﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadFeedFlex(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;

			Excel.Range rngCap = null;
			Excel.Range rngAct = null;

			UInt32 r = 0;

			const UInt32 cCap = 7;
			const UInt32 cAct = 8;

			string cT08_03 = (includeSpace) ? "Table 8-3" : "Table8-3";
			wks = wkb.Worksheets[cT08_03];

			object[] itm = new object[6]
			{
				new object[2] { "Ethane", 21 },
				new object[2] { "ProLPG", 22 },
				new object[2] { "Butane", 23 },
				new object[2] { "NRC", 24 },
				new object[2] { "Diesel", 25 },
				new object[2] { "HeavyGasoil", 26 }
			};

			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				foreach (object[] s in itm)
				{
					try
					{
						r = Convert.ToUInt32(s[1]);
						rngCap = wks.Cells[r, cCap];
						rngAct = wks.Cells[r, cAct];

						//if ((RangeHasValue(rngCap) && ReturnFloat(rngCap) > 0.0f) || (RangeHasValue(rngAct) && ReturnFloat(rngAct) > 0.0f))
						if (RangeHasValue(rngCap) || RangeHasValue(rngAct))
						{
							using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_FeedFlex]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@Refnum		CHAR (9)
								cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

								//@FeedProdID	VARCHAR (20)
								cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = Convert.ToString(s[0]);

								//@PlantCap   REAL         = NULL
								if (RangeHasValue(rngCap)) { cmd.Parameters.Add("@PlantCap", SqlDbType.Float).Value = ReturnFloat(rngCap); }

								//@ActCap     REAL         = NULL
								if (RangeHasValue(rngAct)) { cmd.Parameters.Add("@ActCap", SqlDbType.Float).Value = ReturnFloat(rngAct); }

								cmd.ExecuteNonQuery();
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFeedFlex", Refnum, wkb, wks, rngCap, r, cCap, "[stgFact].[Insert_FeedFlex]", ex);
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFeedFlex", Refnum, wkb, wks, rngAct, r, cAct, "[stgFact].[Insert_FeedFlex]", ex);
					}
				}
				cn.Close();
			}
			rngCap = null;
			rngAct = null;
			wks = null;
		}
	}
}