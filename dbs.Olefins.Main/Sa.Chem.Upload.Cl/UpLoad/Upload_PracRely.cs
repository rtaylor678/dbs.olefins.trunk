﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracRely(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 6;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					string cT06_06 = (includeSpace) ? "Table 6-6" : "Table6-6";
					wks = wkb.Worksheets[cT06_06];

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracRely]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@CompPhil		CHAR (1)	= NULL,
						r = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@CompPhil", SqlDbType.Char, 1).Value = ReturnString(rng, 1);
							
						//@RBICurPcnt		REAL		= NULL,
						//@RBIFutPcnt		REAL		= NULL,
						//@RBIMethPcnt		REAL		= NULL,

						//@RTCGComp		REAL		= NULL,
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@RTCGComp", SqlDbType.Float).Value = ReturnFloat(rng);

						//@RTCGTurb		REAL		= NULL,
						r = 19;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@RTCGTurb", SqlDbType.Float).Value = ReturnFloat(rng);

						//@RTC3Comp		REAL		= NULL,
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@RTC3Comp", SqlDbType.Float).Value = ReturnFloat(rng);

						//@RTC3Turb		REAL		= NULL,
						r = 21;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@RTC3Turb", SqlDbType.Float).Value = ReturnFloat(rng);

						//@RTC2Comp		REAL		= NULL,
						r = 22;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@RTC2Comp", SqlDbType.Float).Value = ReturnFloat(rng);

						//@RTC2Turb		REAL		= NULL,
						r = 23;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@RTC2Turb", SqlDbType.Float).Value = ReturnFloat(rng);


						//@FreqVib		REAL		= NULL,
						r = 33;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@FreqVib", SqlDbType.Float).Value = ReturnFloat(rng);

						//@FreqComp		REAL		= NULL,
						r = 34;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@FreqComp", SqlDbType.Float).Value = ReturnFloat(rng);
						
						//@FreqTurb		REAL		= NULL,
						r = 35;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@FreqTurb", SqlDbType.Float).Value = ReturnFloat(rng);
						
						//@FreqStat		REAL		= NULL,
						r = 36;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@FreqStat", SqlDbType.Float).Value = ReturnFloat(rng);

						//@CompLife		INT			= NULL
						string cT06_05 = (includeSpace) ? "Table 6-5" : "Table6-5";
						wks = wkb.Worksheets[cT06_05];

						r = 23;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@CompLife", SqlDbType.Int).Value = ReturnUShort(rng);

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCompressors", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Compressors]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}