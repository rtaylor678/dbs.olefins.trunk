﻿
Imports System.Data.SqlClient
Imports Microsoft.Data.ConnectionUI

Public Class SimulationControlUI

    Private WithEvents timerSimulation As New System.Windows.Forms.Timer

    Private dbConnString As String
    Private Remaining As Double

    Private cl As New QueueSimulations

    Private Sub frmControl_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Me.lvProcessing.Columns(0).Width = 70
        Me.lvProcessing.Columns(1).Width = 70
        Me.lvProcessing.Columns(2).Width = 100
        Me.lvProcessing.Columns(3).Width = 100
        Me.lvProcessing.Columns(4).Width = 100
        Me.lvProcessing.Columns(5).Width = 450
        Me.lvProcessing.Columns(6).Width = 200

        Me.lblCnString.Text = Nothing
        Me.dbConnString = My.Settings.ConnectionString

        Me.EnableButtons(False)

    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        Me.LogInOut()

    End Sub

    Private Sub LogInOut()

        Dim dcd As New Microsoft.Data.ConnectionUI.DataConnectionDialog()
        Dim dcs As New DataConnectionConfiguration(Sa.AssemblyInfo.PathApplication)

        dcs.LoadConfiguration(dcd)

        Dim cnBuilder As New System.Data.SqlClient.SqlConnectionStringBuilder
        cnBuilder.ConnectionString = My.Settings.ConnectionString

        cnBuilder.ApplicationName = Sa.AssemblyInfo.ProductAndVersion
        cnBuilder.WorkstationID = Sa.Performance.MachineName
        cnBuilder.AsynchronousProcessing = True
        cnBuilder.MultipleActiveResultSets = False

        If Not dcd.SelectedDataProvider Is Nothing Then dcd.ConnectionString = cnBuilder.ConnectionString

        DataConnectionDialog.Show(dcd)
        dcs.SaveConfiguration(dcd)

        Me.lblCnString.Text = "Connection String has been set"
        Me.dbConnString = dcd.ConnectionString
        My.Settings.ConnectionString = dcd.ConnectionString

        My.Settings.Save()

        EnableButtons(True)

    End Sub

    Private Sub EnableButtons(ByVal ready As Boolean)

        Dim t As Boolean = Me.timerSimulation.Enabled

        Me.btnLogin.Enabled = Not t

        Me.btnTerminate.Enabled = t

        Me.btnStartStop.Enabled = t
        Me.numInterval.Enabled = t
        Me.numThreadsPyps.Enabled = t
        Me.numThreadsSpsl.Enabled = t
        Me.txtQueuedItemsPyps.Enabled = t
        Me.txtQueuedItemsSpsl.Enabled = t

        If ready Then

            Me.btnStartStop.Enabled = ready
            Me.numInterval.Enabled = ready
            Me.numThreadsPyps.Enabled = ready
            Me.numThreadsSpsl.Enabled = ready
            Me.txtQueuedItemsPyps.Enabled = ready
            Me.txtQueuedItemsSpsl.Enabled = ready

        End If

    End Sub

    Private Sub btnStartStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStartStop.Click

        Dim b As Button = DirectCast(sender, Button)
        If btnStartStop.Text = "Start" Then

            Dim i As System.Int32 = CType(Me.numInterval.Value, System.Int32)
            Me.TimerStartStop(i)
            b.Text = "Stop"
            Me.lblLastQueue.Text = Replace(Me.lblLastQueue.Text, " (Stopped)", "")

        Else

            Me.TimerStartStop()
            b.Text = "Start"
            Me.lblLastQueue.Text = Me.lblLastQueue.Text + " (Stopped)"

        End If

        EnableButtons(Not Me.timerSimulation.Enabled)

    End Sub

    Private Sub TimerStartStop(ByVal i As System.Int32)

        Me.timerSimulation.Interval = i * 1000
        Me.timer_Elapsed(Me.timerSimulation, New System.EventArgs)
        Me.timerSimulation.Start()

    End Sub

    Private Sub TimerStartStop()

        Me.timerSimulation.Stop()

    End Sub

    Private Sub Timer_Elapsed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerSimulation.Tick

        Dim q As String = Nothing      ' QueueID
        Dim m As String = Nothing      ' SimModelId
        Dim r As String = Nothing      ' Refnum
        Dim f As String = Nothing      ' FactorSetId
        Dim s As String = "Queue"      ' Status
        Dim d As String = Nothing      ' Detail

        Dim t As String = DateTimeOffset.Now.ToString()
        Dim lvi As System.Windows.Forms.ListViewItem

        d = "Queueing Items, Please Wait"
        lvi = New System.Windows.Forms.ListViewItem({q, m, r, f, s, d, t}, 0)
        Me.lvProcessing.Items.Insert(0, lvi)

        Dim i As System.Int32 = Me.QueueSims()

        d = "Queued " + i.ToString + " Simulations"
        lvi = New System.Windows.Forms.ListViewItem({q, m, r, f, s, d, t}, 0)
        Me.lvProcessing.Items.Insert(0, lvi)

        Me.lblLastQueue.Text = "Last queue at " + t

    End Sub

    Private Function QueueSims() As System.Int32

        Dim p As System.Int32 = CType(Me.numThreadsPyps.Value, System.Int32)
        Dim s As System.Int32 = CType(Me.numThreadsSpsl.Value, System.Int32)

        Dim i As System.Int32 = Me.cl.QueueSimulations(Me.dbConnString, Me.lvProcessing, p, s, False)

        Return i

    End Function

    Private Sub StopSimulations(sender As System.Object, e As System.EventArgs) Handles btnTerminate.Click

        Me.cl.StopSimulations()
        Me.btnStartStop_Click(Me.btnStartStop, e)

    End Sub

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click

        Me.lvProcessing.Items.Clear()

    End Sub

    Private Sub numInterval_ValueChanged(sender As System.Object, e As System.EventArgs) Handles numInterval.ValueChanged

        Me.timerSimulation.Interval = CType(Me.numInterval.Value * 1000, System.Int32)

    End Sub

    Private Sub btnTerminateExcel_Click(sender As System.Object, e As System.EventArgs) Handles btnTerminateExcel.Click

        Dim i As System.Int32 = 0

        For Each p As System.Diagnostics.Process In System.Diagnostics.Process.GetProcesses

            If p.ProcessName = "EXCEL" Then

                p.Kill()
                i = i + 1

            End If

        Next p

        MessageBox.Show(i.ToString() + " Excel applications have been termianted.", Sa.AssemblyInfo.Title, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, False)

    End Sub

    Private Sub btnClearFolder_Click(sender As System.Object, e As System.EventArgs) Handles btnClearFolder.Click

        Dim PathEngines As String = Sa.AssemblyInfo.SystemDrive + "PyrolysisSimulations\"

        If Sa.FileOps.DeleteFolder(PathEngines) Then

            MessageBox.Show("Simulation files have been deleted: " + ControlChars.CrLf + PathEngines, Sa.AssemblyInfo.Title, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, False)

        End If

    End Sub

End Class