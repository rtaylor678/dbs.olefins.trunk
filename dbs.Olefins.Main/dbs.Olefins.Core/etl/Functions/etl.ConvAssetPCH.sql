﻿CREATE FUNCTION etl.ConvAssetPCH
(
	  @Refnum		VARCHAR(25)
)
RETURNS VARCHAR(4)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN
	
	SET @Refnum = RTRIM(LTRIM(@Refnum));

	DECLARE @AssetId	VARCHAR(3) = '';

	DECLARE @StudyId	VARCHAR(4) = [etl].[ConvRefnumStudy](@Refnum);

	SET @Refnum = CASE WHEN RIGHT(@Refnum, 1) IN ('U', 'P') THEN LEFT(@Refnum, LEN(@Refnum) -1) ELSE @Refnum END;

	SET @AssetId = RIGHT(@Refnum, LEN(@Refnum) - (CHARINDEX(@StudyId, @Refnum) + LEN(@StudyId) - 1))

	RETURN @AssetId;

END;