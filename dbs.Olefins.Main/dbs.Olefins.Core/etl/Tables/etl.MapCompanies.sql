﻿CREATE TABLE [etl].[MapCompanies]
(
	[Id]				INT					NOT	NULL	IDENTITY(1, 1) NOT FOR REPLICATION,

	[Co]				VARCHAR(40)			NOT	NULL	CONSTRAINT [CL_MapCompanies_Co]					CHECK([Co] <> ''),
	[CompanyId]			VARCHAR(42)			NOT	NULL	CONSTRAINT [FK_MapCompanies_Compay_LookUp]		REFERENCES [dim].[Company_LookUp]([CompanyId]),

	[tsModified]		DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapCompanies_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapCompanies_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapCompanies_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapCompanies_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_MapCompanies_tsGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
														CONSTRAINT [UX_MapCompanies_tsGuid]				UNIQUE NONCLUSTERED([tsGuid] ASC),

	CONSTRAINT [PK_MapCompanies]	PRIMARY KEY NONCLUSTERED([Id] ASC),
	CONSTRAINT [UK_MapCompanies]	UNIQUE CLUSTERED([Co] ASC)
);
GO

CREATE TRIGGER [etl].[t_MapCompanies_u]
	ON [etl].[MapCompanies]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [etl].[MapCompanies]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[etl].[MapCompanies].[Id]		= INSERTED.[Id];

END;