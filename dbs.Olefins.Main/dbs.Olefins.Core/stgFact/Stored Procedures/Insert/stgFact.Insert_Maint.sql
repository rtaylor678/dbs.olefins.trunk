﻿CREATE PROCEDURE [stgFact].[Insert_Maint]
(
	@Refnum         VARCHAR (25),
	@ProjectId      VARCHAR (15),

	@RoutMaintMatl  REAL         = NULL,
	@RoutMaintLabor REAL         = NULL,
	@RoutMaintTot   REAL         = NULL,
	@TAMatl         REAL         = NULL,
	@TALabor        REAL         = NULL,
	@TATot          REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Maint]([Refnum], [ProjectId], [RoutMaintMatl], [RoutMaintLabor], [RoutMaintTot], [TAMatl], [TALabor], [TATot])
	VALUES(@Refnum, @ProjectId, @RoutMaintMatl, @RoutMaintLabor, @RoutMaintTot, @TAMatl, @TALabor, @TATot);

END;
