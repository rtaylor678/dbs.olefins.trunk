﻿CREATE PROCEDURE [stgFact].[Insert_FeedSelections]
(
	@Refnum				VARCHAR (25),
	@FeedProdId			VARCHAR (20),

	@Mix				CHAR (1)		= NULL,
	@Blend				CHAR (1)		= NULL,
	@Fractionation		CHAR (1)		= NULL,
	@Hydrotreat			CHAR (1)		= NULL,
	@Purification		CHAR (1)		= NULL,
	@MinSGEst			REAL			= NULL,
	@MaxSGEst			REAL			= NULL,
	@MinSGAct			REAL			= NULL,
	@MaxSGAct			REAL			= NULL,
	@PipelinePct		REAL			= NULL,
	@TruckPct			REAL			= NULL,
	@BargePct			REAL			= NULL,
	@ShipPct			REAL			= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FeedSelections]([Refnum], [FeedProdId], [Mix], [Blend], [Fractionation], [Hydrotreat], [Purification], [MinSGEst], [MaxSGEst], [MinSGAct], [MaxSGAct], [PipelinePct], [TruckPct], [BargePct], [ShipPct])
	VALUES(@Refnum, @FeedProdId, @Mix, @Blend, @Fractionation, @Hydrotreat, @Purification, @MinSGEst, @MaxSGEst, @MinSGAct, @MaxSGAct, @PipelinePct, @TruckPct, @BargePct, @ShipPct);

END;