﻿CREATE PROCEDURE [stgFact].[Delete_ProdQuality]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[ProdQuality]
	WHERE [Refnum] = @Refnum;

END;