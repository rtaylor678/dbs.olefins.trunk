﻿CREATE TABLE [super].[ReplValOtherOffSite] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [OtherDescription] NVARCHAR (128)     NOT NULL,
    [Value]            REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_super_ReplValOtherOffSite_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_super_ReplValOtherOffSite_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_super_ReplValOtherOffSite_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_super_ReplValOtherOffSite_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_super_ReplValOtherOffSite] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC, [OtherDescription] ASC),
    CONSTRAINT [CL_super_ReplValOtherOffSite_OtherDescriptionp] CHECK ([OtherDescription]<>'')
);


GO

CREATE TRIGGER [super].[t_ReplValOtherOffSite_u]
	ON [super].[ReplValOtherOffSite]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [super].[ReplValOtherOffSite]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[super].[ReplValOtherOffSite].Refnum				= INSERTED.Refnum
		AND	[super].[ReplValOtherOffSite].CalDateKey			= INSERTED.CalDateKey
		AND	[super].[ReplValOtherOffSite].OtherDescription		= INSERTED.OtherDescription;
		
END;