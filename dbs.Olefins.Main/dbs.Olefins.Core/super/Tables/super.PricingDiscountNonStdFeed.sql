﻿CREATE TABLE [super].[PricingDiscountNonStdFeed]
(
	[FactorSetId]		VARCHAR(12)			NOT	NULL	CONSTRAINT [FK_PricingDiscountNonStdFeed_FactorSet_LookUp]	FOREIGN KEY([FactorSetId])	REFERENCES [dim].[FactorSet_LookUp]([FactorSetId]),
	[CalDateKey]		INT					NOT	NULL	CONSTRAINT [FK_PricingDiscountNonStdFeed_Calendar_LookUp]	FOREIGN KEY([CalDateKey])	REFERENCES [dim].[Calendar_LookUp]([CalDateKey]),
	[Refnum]			VARCHAR(25)			NOT	NULL,
	[StreamId]			VARCHAR(42)			NOT	NULL	CONSTRAINT [FK_PricingDiscountNonStdFeed_Stream_LookUp]		FOREIGN KEY([StreamId])		REFERENCES [dim].[Stream_LookUp]([StreamId]),
	[Coefficient]		REAL				NOT	NULL	CONSTRAINT [CR_PricingDiscountNonStdFeed_Coefficient]		CHECK ([Coefficient]>=(0.0)),

	[tsModified]		DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModified]		DEFAULT(sysdatetimeoffset()),
	[tsModifiedHost]	NVARCHAR(168)		NOT	NULL	CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModifiedHost]	DEFAULT(host_name()),
	[tsModifiedUser]	NVARCHAR(168)		NOT	NULL	CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModifiedUser]	DEFAULT(suser_sname()),
	[tsModifiedApp]		NVARCHAR(168)		NOT	NULL	CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModifiedApp]		DEFAULT(app_name()),
	
	CONSTRAINT [PK_PricingDiscountNonStdFeed] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
);