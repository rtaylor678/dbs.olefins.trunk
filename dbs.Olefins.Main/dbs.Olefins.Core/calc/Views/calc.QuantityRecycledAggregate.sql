﻿CREATE VIEW [calc].[QuantityRecycledAggregate]
WITH SCHEMABINDING
AS
	SELECT
		r.FactorSetId,
		r.Refnum,
		MAX(r.CalDateKey)		[CalDateKey],
		p.RecycleId,
		SUM(r.Quantity_kMT)		[Quantity_kMT],
		SUM(r._Recycled_kMT)	[Recycled_kMT]
	FROM [calc].[QuantityRecycled]			r
	INNER JOIN calc.PlantRecycles			p
		ON	p.FactorSetId = r.FactorSetId
		AND	p.Refnum = r.Refnum
	GROUP BY
		r.FactorSetId,
		r.Refnum,
		p.RecycleId;