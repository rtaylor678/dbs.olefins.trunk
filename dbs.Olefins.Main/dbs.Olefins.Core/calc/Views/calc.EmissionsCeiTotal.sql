﻿CREATE VIEW [calc].[EmissionsCeiTotal]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	p.CalDateKey,
	p.EmissionsCarbon_MTCO2e	[PlantEmissions_MTCO2e],
	s.EmissionsCarbon_MTCO2e	[StandardEmissions_MTCO2e],
	p.EmissionsCarbon_MTCO2e / s.EmissionsCarbon_MTCO2e * 100.0	[CeiTotal]
FROM calc.EmissionsCarbonPlantAggregate				p
INNER JOIN calc.EmissionsCarbonStandardAggregate	s
	ON	s.FactorSetId	= p.FactorSetId
	AND	s.Refnum		= p.Refnum
	AND	s.CalDateKey	= p.CalDateKey
	AND	s.EmissionsId	= p.EmissionsId
WHERE	p.EmissionsId	= 'TotalEmissions'
	AND	s.EmissionsId	= 'TotalEmissions';
