﻿CREATE PROCEDURE [calc].[Insert_ReplValSuppFeedAdjustment]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		-- SUPPLEMENTAL FEED ADJUSTMENT per SPECIAL PLANT
		SET @ProcedureDesc = CHAR(9) + 'INSERT INTO @ReplValSuppFeedAdjPlant';
		PRINT @ProcedureDesc;

		DECLARE @ReplValSuppFeedAdjPlant TABLE
		(
			[FactorSetId]				VARCHAR (12)	NOT	NULL,
			[Refnum]					VARCHAR (25)	NOT	NULL,
			[CalDateKey]				INT				NOT	NULL,
			[CurrencyRpt]				VARCHAR (4)		NOT	NULL,
			[ComponentId]				VARCHAR (42)	NOT	NULL,
			[SuppEthylene_kMT]			REAL			NOT	NULL,
			[Escalated_ENC_Cur]			REAL			NOT	NULL,
			[RecoveryMultipler]			REAL			NOT	NULL,
			[InflationFactor]			REAL			NOT	NULL,
			[Quantity_kMT]				REAL			NOT	NULL,
			[Superseede]				BIT				NOT	NULL
		)

		INSERT INTO @ReplValSuppFeedAdjPlant(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ComponentId, SuppEthylene_kMT, Escalated_ENC_Cur, RecoveryMultipler, InflationFactor, Quantity_kMT, Superseede)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			enc.ComponentId,
			rgr.Quantity_kMT,
			enc.Escalated_ENC_Cur,
			rgr.RecoveryMultiplier,
			i._InflationFactor,
			POWER(rgr.Quantity_kMT / 200.0, 0.75) * enc.Escalated_ENC_Cur * rgr.RecoveryMultiplier * 1.25 * i._InflationFactor,
			rgr.Superseede
		FROM @fpl											fpl
		INNER JOIN ante.InflationFactor						i
			--ON	i.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, i.FactorSetId) = fpl.DataYear
			AND	i.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN ante.EngrConstEstimate					enc
			--ON	enc.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, enc.FactorSetId) = fpl.DataYear
			AND	enc.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN super.ReplValRecoveryGasUnit				rgr
			ON	rgr.Refnum = fpl.Refnum
			AND	rgr.ComponentId = enc.ComponentId
		WHERE fpl.CalQtr = 4;

		--	INCREASE IN RECOVERY SECTION INVESTMENT DUE TO SUPPLEMENTAL FEED (0.65 POWER FACTOR)
		SET @ProcedureDesc = CHAR(9) + 'INSERT INTO @ReplValSuppRecovInvIncrease';
		PRINT @ProcedureDesc;

		DECLARE @ReplValSuppRecovInvIncrease TABLE
		(
			[FactorSetId]				VARCHAR (12)		NOT	NULL,
			[Refnum]					VARCHAR (25)		NOT	NULL,
			[CalDateKey]				INT					NOT	NULL,
			[CurrencyRpt]				VARCHAR (4)			NOT	NULL,
			[Amount_MCur]				REAL				NOT	NULL
		)

		INSERT INTO @ReplValSuppRecovInvIncrease(FactorSetId, Refnum, CalDateKey, CurrencyRpt, Amount_MCur)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			rfx._Recovery_ISBL * (POWER(
				(c.PyroEthyleneCap_kMT + p._CapacityAvg_kMT + ISNULL([C2H4].Component_kMT, 0.0)) / (c.PyroEthyleneCap_kMT + p._CapacityAvg_kMT - ISNULL([C3H6].Component_kMT, 0.0)),
				CASE WHEN ISNULL(f.Unit_Count, 0.0) > 2 THEN 0.95 ELSE 0.65 END) - 1.0)
		FROM @fpl										fpl
		INNER JOIN inter.ReplValPyroCapacity			c
			ON	c.FactorSetId = fpl.FactorSetId
			AND	c.Refnum = fpl.Refnum
			AND	c.CalDateKey = fpl.Plant_QtrDateKey
		INNER JOIN calc.ReplValPyroFlexibility			rfx
			ON	rfx.FactorSetId = fpl.FactorSetId
			AND	rfx.Refnum = fpl.Refnum
			AND	rfx.CalDateKey = fpl.Plant_QtrDateKey
		INNER JOIN calc.CapacityPlant					p
			ON	p.FactorSetId = fpl.FactorSetId
			AND	p.Refnum = fpl.Refnum
			AND	p.CalDateKey = fpl.Plant_QtrDateKey
			AND	p.StreamId = 'Propylene'
		LEFT OUTER JOIN	inter.YIRSupplemental			[C3H6]
			ON	[C3H6].FactorSetId = fpl.FactorSetId
			AND	[C3H6].Refnum = fpl.Refnum
			AND	[C3H6].CalDateKey = fpl.Plant_QtrDateKey
			AND	[C3H6].ComponentId = 'C3H6'
		LEFT OUTER JOIN	inter.YIRSupplemental			[C2H4]
			ON	[C2H4].FactorSetId = fpl.FactorSetId
			AND	[C2H4].Refnum = fpl.Refnum
			AND	[C2H4].CalDateKey = fpl.Plant_QtrDateKey
			AND	[C2H4].ComponentId = 'C2H4'
		LEFT OUTER JOIN fact.Facilities					f
			ON	f.Refnum = fpl.Refnum
			AND	f.CalDateKey = fpl.Plant_QtrDateKey
			AND	f.FacilityId = 'TowerEthylene'
		WHERE fpl.CalQtr = 4;

		--	STD ESTIMATE OF SUPPL. FEED ADJUSTMENT
		SET @ProcedureDesc = CHAR(9) + 'INSERT INTO @ReplValSuppFeedAdjStd';
		PRINT @ProcedureDesc;

		DECLARE @ReplValSuppFeedAdjStd TABLE
		(
			[FactorSetId]				VARCHAR (12)		NOT	NULL,
			[Refnum]					VARCHAR (25)		NOT	NULL,
			[CalDateKey]				INT					NOT	NULL,
			[Quantity_kMT]				REAL				NOT	NULL
		);

		INSERT INTO @ReplValSuppFeedAdjStd(FactorSetId, Refnum, CalDateKey, Quantity_kMT)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			SUM(CASE WHEN m.StreamId IN ('ConcPropylene', 'DilPropylene', 'ROGPropylene') THEN 0.5 ELSE 1.0 END * m.Component_kMT) * 0.1 * 361.3 / 355.4 * 1.462
		FROM @fpl									fpl
		INNER JOIN calc.CompositionStream			m
			ON	m.FactorSetId = fpl.FactorSetId
			AND	m.Refnum = fpl.Refnum
			AND	m.CalDateKey = fpl.Plant_QtrDateKey
		WHERE	m.SimModelId = 'Plant'
			AND	m.OpCondId = 'PlantFeed'
			AND	m.StreamId IN ('ConcEthylene', 'DilEthylene', 'ROGEthylene', 'ConcPropylene', 'DilPropylene', 'ROGPropylene')
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey;

		SET @ProcedureDesc = CHAR(9) + 'INSERT INTO calc.ReplValSuppFeedAdjustment';
		PRINT @ProcedureDesc;

		INSERT INTO calc.ReplValSuppFeedAdjustment(FactorSetId, Refnum, CalDateKey, CurrencyRpt, Quantity_kMT, Amount_kMT, InflationFactor, SupplementalAdjustment, RGRSuperseede)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			std.Quantity_kMT,
			inv.Amount_MCur,
			i._InflationFactor,
			calc.MaxValue(std.Quantity_kMT, calc.MinValue(std.Quantity_kMT * 3.0, inv.Amount_MCur)) * i._InflationFactor
													[SupplementaAdjustment],
			pnt.Quantity_kMT + 
				CASE pnt.Superseede
					WHEN 0 THEN 0.0
					WHEN 1 THEN inv.Amount_MCur * i._InflationFactor
				END									[RecoveryGasUnitSuperseede]

		FROM @fpl										fpl
		INNER JOIN ante.InflationFactor					i
			--ON	i.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, i.FactorSetId) = fpl.DataYear
			AND	i.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN @ReplValSuppRecovInvIncrease			inv
			ON	inv.FactorSetId = fpl.FactorSetId
			AND	inv.Refnum = fpl.Refnum
			AND	inv.CalDateKey = fpl.Plant_QtrDateKey
			AND	inv.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN @ReplValSuppFeedAdjStd				std
			ON	std.FactorSetId = fpl.FactorSetId
			AND	std.Refnum = fpl.Refnum
			AND	std.CalDateKey = fpl.Plant_QtrDateKey
		LEFT OUTER JOIN @ReplValSuppFeedAdjPlant		pnt
			ON	pnt.FactorSetId = fpl.FactorSetId
			AND	pnt.Refnum = fpl.Refnum
			AND	pnt.CalDateKey = fpl.Plant_QtrDateKey
			AND	pnt.CurrencyRpt = fpl.CurrencyRpt
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;