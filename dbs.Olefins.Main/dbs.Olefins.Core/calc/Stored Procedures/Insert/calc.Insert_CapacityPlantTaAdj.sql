﻿CREATE PROCEDURE calc.Insert_CapacityPlantTaAdj
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.CapacityPlantTaAdj(
			FactorSetId,
			Refnum,
			CalDateKey,
			SchedId,
			StreamId,
			Study_Date,
			Expansion_Date,
			Ann_TurnAroundLoss_Pcnt,
			Stream_kMT,
			StreamPrev_kMT,
			StreamAvg_kMT,
			Capacity_kMT,
			CapacityPrev_kMT,
			CapacityAvg_kMT)
		SELECT 
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			a.SchedId,
			u.StreamId,
			u.Study_Date,
			u.Expansion_Date,
			a.Ann_TurnAroundLoss_Pcnt,
			u.Stream_kMT,
			u._StreamPrev_kMT,
			u._StreamAvg_kMT,
			u.Capacity_kMT,
			u._CapacityPrev_kMT,
			u._CapacityAvg_kMT
		FROM @fpl								tsq
		INNER JOIN calc.CapacityPlant			u
			ON	u.FactorSetId = tsq.FactorSetId
			AND	u.Refnum = tsq.Refnum
			AND	u.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN calc.TaAdjCapacityAggregate	a
			ON	a.FactorSetId = tsq.FactorSetId
			AND	a.Refnum = tsq.Refnum
			AND	a.CalDateKey = tsq.Plant_QtrDateKey;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;