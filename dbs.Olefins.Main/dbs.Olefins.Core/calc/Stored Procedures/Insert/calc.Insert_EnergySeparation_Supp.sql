﻿CREATE PROCEDURE [calc].[Insert_EnergySeparation_Supp]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[EnergySeparation]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [EnergySeparation_BtuLb])
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			q.StreamId,
			SUM(q.Component_kMT * e.EnergySeparation_BtuLb)	[EnergySeparation_BtuLb]
		FROM @fpl												tsq
		INNER JOIN calc.CompositionStream						q
			ON	q.FactorSetId = tsq.FactorSetId
			AND	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND	q.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = tsq.FactorSetId AND d.StreamId = 'SuppTot')
		INNER JOIN ante.EnergySeparation				e
			ON	e.FactorSetId = tsq.FactorSetId
			AND	e.ComponentId = q.ComponentId
		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			q.StreamId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;