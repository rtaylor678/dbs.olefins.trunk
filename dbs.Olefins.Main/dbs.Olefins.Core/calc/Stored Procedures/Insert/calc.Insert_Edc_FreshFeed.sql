﻿CREATE PROCEDURE [calc].[Insert_Edc_FreshFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			k.EdcId,
			q.StreamDescription,
			k.Value * SUM(q.Quantity_kMT) / r.FeedDivisor [kEdc]
		FROM @fpl										tsq
		INNER JOIN fact.Quantity						q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND q.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = tsq.FactorSetId aND d.StreamId = 'FreshPyroFeed')
		INNER JOIN calc.EdcFeedDivisor					r
			ON	r.FactorSetId = tsq.FactorSetId
			AND	r.Refnum = tsq.Refnum
			AND	r.CalDateKey = tsq.Plant_AnnDateKey
		INNER JOIN ante.EdcCoefficients					k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.EdcId = q.StreamId
		WHERE	tsq.[FactorSet_AnnDateKey]	< 20130000
			AND	r.FeedDivisor				<> 0.0
		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			k.EdcId,
			q.StreamDescription,
			k.Value,
			r.FeedDivisor
		HAVING SUM(q.Quantity_kMT) IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;