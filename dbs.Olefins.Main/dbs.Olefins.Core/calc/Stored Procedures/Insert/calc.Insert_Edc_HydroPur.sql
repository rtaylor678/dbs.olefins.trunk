﻿CREATE PROCEDURE [calc].[Insert_Edc_HydroPur]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			k.EdcId,
			k.EdcId,
			k.Value * f.Unit_Count * ABS(q.Quantity_kMT) / 1000.0 *
				CASE
					WHEN k.FactorSetId IN (2007, 2009, 2011) THEN 2204.6 * 379.0 / 2.0 / 365.0 / (8.0 - 7.0 * calc.ConvMoleWeight('H2', c.Component_WtPcnt, 'M') / 100.0)
					ELSE c.Component_WtPcnt
					END															[kEdc]
		FROM @fpl										tsq
		INNER JOIN	fact.FacilitiesCount				f	WITH (NOEXPAND)
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
			AND	f.FacilityId IN ('HydroPurCryogenic', 'HydroPurMembrane', 'HydroPurPSA')
			AND	f.Unit_Count > 0
		INNER JOIN fact.StreamQuantityAggregate			q	WITH (NOEXPAND)
			ON	q.FactorSetId = tsq.FactorSetId
			AND	q.Refnum = tsq.Refnum
			AND	q.StreamId = 'Hydrogen'
		INNER JOIN fact.CompositionQuantity				c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = q.StreamId
			AND c.Component_WtPcnt > 0.0
		INNER JOIN ante.EdcCoefficients					k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.EdcId = f.FacilityId
		WHERE tsq.[FactorSet_AnnDateKey] < 20130000
			AND	f.Unit_Count		IS NOT NULL
			AND q.Quantity_kMT		IS NOT NULL
			AND	c.Component_WtPcnt	IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	BEGIN TRY

		DECLARE @HydroPur	VARCHAR(42);

		SELECT TOP 1
			@HydroPur = f.[FacilityId]
		FROM @fpl							fpl
		INNER JOIN [dim].[Facility_Bridge]	b
			ON	b.[FactorSetId]	= fpl.[FactorSetId]
			AND	b.[FacilityId]	= 'HydroPur'
		INNER JOIN [fact].[Facilities]		f
			ON	f.[Refnum]		= fpl.[Refnum]
			AND	f.[FacilityId]	= b.[DescendantId]
			AND	f.Unit_Count	> 0
		WHERE fpl.[CalQtr]		= 4
			AND	fpl.[FactorSet_QtrDateKey]	> 20130000
		ORDER BY b.[Hierarchy] ASC;

		INSERT INTO [calc].[Edc]([FactorSetId], [Refnum], [CalDateKey], [EdcId], [EdcDetail], [kEdc])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			@HydroPur,
			@HydroPur,
			(423.3 + 49.33 * 3.0) * 1.1 * ABS(sqa.[Quantity_kMT]) * c.[Component_WtPcnt] / u.[_UtilizationStream_Pcnt] / 365.0 * k.[Value]
		FROM @fpl										fpl
		INNER JOIN [fact].[StreamQuantityAggregate]		sqa
			ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
			AND	sqa.[Refnum]			= fpl.[Refnum]
			AND	sqa.[StreamId]			= 'Hydrogen'
			AND	sqa.[Quantity_kMT]		IS NOT NULL
		INNER JOIN [calc].[CompositionStream]			c
			ON	c.[FactorSetId]			= fpl.[FactorSetId]
			AND	c.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			AND	c.[Refnum]				= fpl.[Refnum]
			AND	c.[StreamId]			= sqa.[StreamId]
			AND	c.ComponentId			= 'H2'
			AND	c.[Component_WtPcnt]	> 40.0
		INNER JOIN [calc].[CapacityUtilization]			u
			ON	u.[FactorSetId]			= fpl.[FactorSetId]
			AND	u.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			AND	u.[Refnum]				= fpl.[Refnum]
			AND	u.[SchedId]				= 'P'
			AND	u.[ComponentId]			= 'ProdOlefins'
		INNER JOIN [ante].[EdcCoefficients]				k
			ON	k.[FactorSetId]			= fpl.[FactorSetId]
			AND	k.[EdcId]				= @HydroPur
		WHERE	fpl.[CalQtr]				= 4
			AND	fpl.[FactorSet_QtrDateKey]	> 20130000
			AND	u.[_UtilizationStream_Pcnt]	<> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;