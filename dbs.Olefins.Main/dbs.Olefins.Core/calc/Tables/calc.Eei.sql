﻿CREATE TABLE [calc].[Eei] (
    [FactorSetId]             VARCHAR (12)       NOT NULL,
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [SimModelId]              VARCHAR (12)       NOT NULL,
    [OpCondId]                VARCHAR (12)       NOT NULL,
    [RecycleId]               INT                NOT NULL,
    [Simulation_MBTU]         REAL               NOT NULL,
    [Separation_MBTU]         REAL               NULL,
    [_TotalModel_MBTU]        AS                 (CONVERT([real],[Simulation_MBTU]+isnull([Separation_MBTU],(0.0)),(1))) PERSISTED NOT NULL,
    [ComponentId]             VARCHAR (42)       NOT NULL,
    [Production_kMT]          REAL               NOT NULL,
    [Simulation_kMT]          REAL               NOT NULL,
    [Supplemental_kMT]        REAL               NULL,
    [_TotalModel_kMT]         AS                 (CONVERT([real],[Simulation_kMT]+isnull([Supplemental_kMT],(0.0)),(1))),
    [Actual_MBTU_kMT]         REAL               NOT NULL,
    [_Model_Energy_MBTU_kMT]  AS                 (CONVERT([real],((([Simulation_MBTU]+isnull([Separation_MBTU],(0.0)))/([Simulation_kMT]+isnull([Supplemental_kMT],(0.0))))/(2.2046))*(1000000.0),(1))) PERSISTED NOT NULL,
    [_Eei_NoSeparationEnergy] AS                 (CONVERT([real],[Actual_MBTU_kMT]/((([Simulation_MBTU]/([Simulation_kMT]+isnull([Supplemental_kMT],(0.0))))/(2.2046))*(10000.0)),(1))) PERSISTED NOT NULL,
    [_Eei]                    AS                 (CONVERT([real],[Actual_MBTU_kMT]/(((([Simulation_MBTU]+isnull([Separation_MBTU],(0.0)))/([Simulation_kMT]+isnull([Supplemental_kMT],(0.0))))/(2.2046))*(10000.0)),(1))) PERSISTED NOT NULL,
    [_IsEei_Bit]              AS                 (CONVERT([bit],case when [OpCondId]='OS25' AND [ComponentId]='ProdHVC' then (1) else (0) end,(0))) PERSISTED NOT NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_Eei_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_Eei_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_Eei_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_Eei_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_Eei] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC, [RecycleId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_Eei_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_Eei_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_Eei_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_Eei_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_Eei_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_calc_Eei_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [calc].[t_Eei_u]
    ON [calc].[Eei]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [calc].[Eei]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[Eei].FactorSetId		= INSERTED.FactorSetId
		AND calc.[Eei].Refnum			= INSERTED.Refnum
		AND calc.[Eei].CalDateKey		= INSERTED.CalDateKey
		AND calc.[Eei].SimModelId		= INSERTED.SimModelId
		AND calc.[Eei].OpCondId			= INSERTED.OpCondId
		AND calc.[Eei].RecycleId		= INSERTED.RecycleId
		AND calc.[Eei].ComponentId		= INSERTED.ComponentId;
	
END