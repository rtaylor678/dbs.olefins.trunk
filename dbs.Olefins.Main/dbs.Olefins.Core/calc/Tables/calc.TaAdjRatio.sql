﻿CREATE TABLE [calc].[TaAdjRatio] (
    [FactorSetId]             VARCHAR (12)       NOT NULL,
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [StreamId]                VARCHAR (42)       NOT NULL,
    [ComponentId]             VARCHAR (42)       NULL,
    [OppLossId]               VARCHAR (42)       NULL,
    [Stream_kMT]              REAL               NOT NULL,
    [Component_kMT]           REAL               NULL,
    [OppLoss_kMT]             REAL               NULL,
    [_TaAdj_Feed_Ratio]       AS                 (CONVERT([real],case when (isnull([Stream_kMT],(0.0))-isnull([OppLoss_kMT],(0.0)))<>(0.0) then isnull([Stream_kMT],(0.0))/(isnull([Stream_kMT],(0.0))-isnull([OppLoss_kMT],(0.0)))  end,(1))) PERSISTED,
    [_TaAdj_Production_Ratio] AS                 (CONVERT([real],case when [Component_kMT]<>(0.0) then (isnull([Component_kMT],(0.0))+isnull([OppLoss_kMT],(0.0)))/[Component_kMT]  end,(1))) PERSISTED,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_TaAdjRatio_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_TaAdjRatio_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_TaAdjRatio_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_TaAdjRatio_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_TaAdjRatio] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_TaAdjRatio_Component_kMT] CHECK ([Component_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjRatio_Feed_Ratio] CHECK ([_TaAdj_Feed_Ratio]>=(1.0)),
    CONSTRAINT [CR_calc_TaAdjRatio_OppLoss_kMT] CHECK ([OppLoss_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjRatio_Production_Ratio] CHECK ([_TaAdj_Production_Ratio]>=(1.0)),
    CONSTRAINT [CR_calc_TaAdjRatio_Stream_kMT] CHECK ([Stream_kMT]>=(0.0)),
    CONSTRAINT [FK_calc_TaAdjRatio_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_TaAdjRatio_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_TaAdjRatio_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_TaAdjRatio_OpLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_calc_TaAdjRatio_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_TaAdjRatio_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_TaAdjRatio_u
	ON  calc.TaAdjRatio
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.TaAdjRatio
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.TaAdjRatio.FactorSetId		= INSERTED.FactorSetId
		AND calc.TaAdjRatio.Refnum			= INSERTED.Refnum
		AND calc.TaAdjRatio.CalDateKey		= INSERTED.CalDateKey;
			
END