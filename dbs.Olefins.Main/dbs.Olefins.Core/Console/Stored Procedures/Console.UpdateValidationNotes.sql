﻿CREATE PROCEDURE [Console].[UpdateValidationNotes]

	@Refnum varchar(18),
	@NoteType varchar(20),
	@Note text,
	@UpdatedBy varchar(3)

AS
BEGIN

	declare @OldNote varchar(MAX)
	select @OldNote = Note COLLATE SQL_Latin1_General_CP1_CS_AS 
	from Val.Notes where Refnum = @Refnum and NoteType = @NoteType order by Updated desc
	
	if not exists(select * from Val.Notes where Refnum = @Refnum and NoteType = @NoteType and 
	Note COLLATE SQL_Latin1_General_CP1_CS_AS like @Note)
		INSERT INTO Val.Notes (RefNum, NoteType, Note, UpdatedBy, Updated) Values(@Refnum,'Validation',@Note, @UpdatedBy, GETDATE())

END
