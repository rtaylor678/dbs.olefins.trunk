﻿CREATE PROCEDURE [Console].[GetCompanyPassword]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SELECT
		[t].[CompanyPassWord]
	FROM
		[cons].[TSort]	[t]
	WHERE	[t].[SmallRefnum]	= @Refnum
		OR	[t].[Refnum]		= @Refnum;

END