﻿CREATE TYPE [dbo].[RefnumList] AS TABLE (
    [Refnum] VARCHAR (25) NOT NULL,
    PRIMARY KEY CLUSTERED ([Refnum] ASC));

