﻿CREATE VIEW dbo.Furnaces
AS
SELECT
		[GroupId]	= [t].[Refnum],
	[t].[Refnum],
	[f].[FurnNo],
	[f].[FurnFeed],
	[f].[DTDecoke],
	[f].[DTTLEClean],
	[f].[DTMinor],
	[f].[DTMajor],
	[f].[STDT],
	[f].[DTStandby],
	[f].[TotTimeOff],
	[f].[TADownDays],
	[f].[OthDownDays],
	[f].[FurnAvail],
	[f].[AdjFurnOnstream],
	[f].[FeedQtyKMTA],
	[f].[FeedCapMTD],
	[f].[OperDays],
	[f].[ActCap],
	[f].[AdjFurnUtil],
	[f].[FurnSlowD],
	[f].[YearRetubed],
	[f].[RetubeCost],
	[f].[RetubeCostMatl],
	[f].[RetubeCostLabor],
	[f].[OtherMajorCost],
	[f].[TotCost],
	[f].[RetubeInterval],
	[f].[OnStreamDays],
	[f].[FuelCons],
	[f].[FuelType],
	[f].[StackOxy],
	[f].[ArchDraft],
	[f].[StackTemp],
	[f].[CrossTemp],
	[f].[FurnFeedBreak],
	[f].[AnnRetubeCostMatlMT],
	[f].[AnnRetubeCostLaborMT],
	[f].[AnnRetubeCostMT],
	[f].[OtherMajorCostMT],
	[f].[TotCostMT],
	[f].[FuelTypeYN],
	[f].[FuelTypeFO],
	[f].[FuelTypeFG],

	[p].[CurrencyRpt],
	[p].[MaintMajor],
	[p].[MaintRetubeLabor],
	[p].[MaintRetubeMatl],
	[MaintTot]	= COALESCE([p].[MaintMajor], 0.0) +
					COALESCE([p].[MaintRetubeLabor], 0.0) +
					COALESCE([p].[MaintRetubeMatl], 0.0),
	[cf].[_Ann_MaintRetubeTot_Cur],
	[cf].[_InflAdjAnn_MaintRetubeTot_Cur],

	[FuelConsRatio]	= CASE WHEN [f].[FeedQtyKMTA] <> 0.0 THEN [f].[FuelCons] * 1000.0 / [f].[FeedQtyKMTA] END

FROM
	[dbo].[TSort]				[t]
INNER JOIN
	[stgFact].[FurnRely]		[f]
		ON	[f].[Refnum]		= [t].[SmallRefnum]
LEFT OUTER JOIN (
	SELECT	
		[rpf].[Refnum],
		[rpf].[FurnId],
		[rpf].[AccountId],
		[rpf].[CurrencyRpt],
		[rpf].[Amount_Cur]
	FROM
		[fact].[ReliabilityPyroFurnOpEx]	[rpf]
	) [u]
	PIVOT
	(
		MAX([u].[Amount_Cur]) FOR [u].[AccountId] IN
		(
			[MaintMajor],
			[MaintRetubeLabor],
			[MaintRetubeMatl]
		)
	) [p]
		ON	[p].[Refnum]	= [t].[Refnum]
		AND	[p].[FurnId]	= [f].[FurnNo]
LEFT OUTER JOIN
	[calc].[Furnaces]						[cf]
		ON	[cf].[Refnum]		= [t].[Refnum]
		AND	[cf].[FurnId]		= [f].[FurnNo]
		AND	[cf].[FactorSetId]	= LEFT([cf].[Refnum], 4);