﻿CREATE TABLE [dim].[FeedStockDistillation_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [DistillationId] VARCHAR (4)         NOT NULL,
    [ParentId]       VARCHAR (4)         NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_FeedStockDistillation_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedStockDistillation_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistillation_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistillation_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistillation_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedStockDistillation_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [DistillationId] ASC),
    CONSTRAINT [CR_FeedStockDistillation_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_FeedStockDistillation_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_FeedStockDistillation_Parent_LookUp_FeedStockDistillation] FOREIGN KEY ([DistillationId]) REFERENCES [dim].[FeedStockDistillation_LookUp] ([DistillationId]),
    CONSTRAINT [FK_FeedStockDistillation_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[FeedStockDistillation_LookUp] ([DistillationId]),
    CONSTRAINT [FK_FeedStockDistillation_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[FeedStockDistillation_Parent] ([FactorSetId], [DistillationId])
);


GO

CREATE TRIGGER [dim].[t_FeedStockDistillation_Parent_u]
ON [dim].[FeedStockDistillation_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedStockDistillation_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedStockDistillation_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[FeedStockDistillation_Parent].[DistillationId]	= INSERTED.[DistillationId];

END;