﻿CREATE TABLE [dim].[ReliabilityOppLoss_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [OppLossId]      VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_ReliabilityOppLoss_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityOppLoss_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityOppLoss_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityOppLoss_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_ReliabilityOppLoss_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityOppLoss_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [OppLossId] ASC),
    CONSTRAINT [CR_ReliabilityOppLoss_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ReliabilityOppLoss_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Parent_LookUp_OppLoss] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ReliabilityOppLoss_Parent] ([FactorSetId], [OppLossId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityOppLoss_Parent_u]
ON [dim].[ReliabilityOppLoss_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityOppLoss_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityOppLoss_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityOppLoss_Parent].[OppLossId]		= INSERTED.[OppLossId];

END;