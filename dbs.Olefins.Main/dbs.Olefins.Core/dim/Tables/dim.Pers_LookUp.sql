﻿CREATE TABLE [dim].[Pers_LookUp] (
    [PersId]         AS                 (coalesce([PersIdPri],'')+coalesce([PersIdSec],'')) PERSISTED NOT NULL,
    [PersIdPri]      VARCHAR (12)       NOT NULL,
    [PersIdSec]      VARCHAR (22)       NOT NULL,
    [PersName]       NVARCHAR (84)      NOT NULL,
    [PersDetail]     NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Pers_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Pers_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Pers_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Pers_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Pers_LookUp] PRIMARY KEY CLUSTERED ([PersId] ASC),
    CONSTRAINT [CL_Pers_LookUp_PersDetail] CHECK ([PersDetail]<>''),
    CONSTRAINT [CL_Pers_LookUp_PersName] CHECK ([PersName]<>''),
    CONSTRAINT [FK_Pers_LookUp_PersPri_LookUp] FOREIGN KEY ([PersIdPri]) REFERENCES [dim].[PersPri_LookUp] ([PersPriId]),
    CONSTRAINT [FK_Pers_LookUp_PersSec_LookUp] FOREIGN KEY ([PersIdSec]) REFERENCES [dim].[PersSec_LookUp] ([PersSecId])
);


GO

CREATE TRIGGER [dim].[t_Pers_LookUp_u]
	ON [dim].[Pers_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Pers_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Pers_LookUp].[PersId]		= INSERTED.[PersId];
		
END;
