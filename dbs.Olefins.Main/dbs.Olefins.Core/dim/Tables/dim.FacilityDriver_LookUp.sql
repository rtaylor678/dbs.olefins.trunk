﻿CREATE TABLE [dim].[FacilityDriver_LookUp] (
    [DriverId]       VARCHAR (42)       NOT NULL,
    [DriverName]     NVARCHAR (84)      NOT NULL,
    [DriverDetail]   NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilityDriver_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FacilityDriver_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FacilityDriver_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FacilityDriver_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilityDriver_LookUp] PRIMARY KEY CLUSTERED ([DriverId] ASC),
    CONSTRAINT [CL_FacilityDriver_LookUp_DriverDetail] CHECK ([DriverDetail]<>''),
    CONSTRAINT [CL_FacilityDriver_LookUp_DriverId] CHECK ([DriverId]<>''),
    CONSTRAINT [CL_FacilityDriver_LookUp_DriverName] CHECK ([DriverName]<>''),
    CONSTRAINT [UK_FacilityDriver_LookUp_DriverDetail] UNIQUE NONCLUSTERED ([DriverDetail] ASC),
    CONSTRAINT [UK_FacilityDriver_LookUp_DriverName] UNIQUE NONCLUSTERED ([DriverName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FacilityDriver_LookUp_u]
	ON [dim].[FacilityDriver_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FacilityDriver_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FacilityDriver_LookUp].[DriverId]		= INSERTED.[DriverId];
		
END;
