﻿CREATE TABLE [dim].[ReplacementValue_Parent] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [ReplacementValueId] VARCHAR (42)        NOT NULL,
    [ParentId]           VARCHAR (42)        NOT NULL,
    [Operator]           CHAR (1)            CONSTRAINT [DF_ReplacementValue_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReplacementValue_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReplacementValue_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReplacementValue_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReplacementValue_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReplacementValue_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ReplacementValueId] ASC),
    CONSTRAINT [CR_ReplacementValue_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ReplacementValue_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReplacementValue_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ReplacementValue_LookUp] ([ReplacementValueId]),
    CONSTRAINT [FK_ReplacementValue_Parent_LookUp_ReplacementValue] FOREIGN KEY ([ReplacementValueId]) REFERENCES [dim].[ReplacementValue_LookUp] ([ReplacementValueId]),
    CONSTRAINT [FK_ReplacementValue_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ReplacementValue_Parent] ([FactorSetId], [ReplacementValueId])
);


GO

CREATE TRIGGER [dim].[t_ReplacementValue_Parent_u]
ON [dim].[ReplacementValue_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReplacementValue_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReplacementValue_Parent].[FactorSetId]			= INSERTED.[FactorSetId]
		AND	[dim].[ReplacementValue_Parent].[ReplacementValueId]	= INSERTED.[ReplacementValueId];

END;