﻿CREATE PROCEDURE [dim].[Update_Parent]
(
	@SchemaName				VARCHAR(48)	= 'dim',
	@TableName				VARCHAR(48),

	@MethodologyIdName		VARCHAR(48)	= 'MethodologyId',
	@PrimeIdName			VARCHAR(48),
	@ParentIdName			VARCHAR(48)	= 'ParentId',
	@SortKeyName			VARCHAR(48)	= 'SortKey',
	@HierarchyName			VARCHAR(48)	= 'Hierarchy'
)
AS
BEGIN

SET NOCOUNT ON;

	DECLARE @SQL	VARCHAR(MAX) =
	'WITH cte(MethodologyId, PrimeId, ParentId, Hierarchy) AS
	(
		SELECT
			a.[' + @MethodologyIdName + '],
			a.[' + @PrimeIdName + '],
			a.[' + @ParentIdName + '],
			SYS.HIERARCHYID::Parse(''/'' + CAST(ROW_NUMBER() OVER(PARTITION BY a.[' + @MethodologyIdName + '] ORDER BY a.[' + @SortKeyName + ']) AS VARCHAR) + ''/'')
		FROM [' + @SchemaName + '].[' + @TableName + '] a
		WHERE a.[' + @PrimeIdName + '] = a.[' + @ParentIdName + ']
		UNION ALL
		SELECT
			a.[' + @MethodologyIdName + '],
			a.[' + @PrimeIdName + '],
			a.[' + @ParentIdName + '],
			SYS.HIERARCHYID::Parse(c.Hierarchy.ToString() + CAST(ROW_NUMBER() OVER(PARTITION BY a.[' + @MethodologyIdName + '] ORDER BY a.[' + @SortKeyName + ']) AS VARCHAR) + ''/'')
		FROM [' + @SchemaName + '].[' + @TableName + '] a
		INNER JOIN cte c
			ON	c.[MethodologyId] = a.[' + @MethodologyIdName + ']
			AND	c.[PrimeId] = a.[' + @ParentIdName + ']
		WHERE a.[' + @PrimeIdName + '] <> a.[' + @ParentIdName + ']
	)
	UPDATE a
	SET a.[' + @HierarchyName + '] = c.[Hierarchy]
	FROM [' + @SchemaName + '].[' + @TableName + '] a
	INNER JOIN cte c
		ON	c.[MethodologyId] = a.[' + @MethodologyIdName + ']
		AND	c.[PrimeId] = a.[' + @PrimeIdName + ']
	WHERE a.[' + @HierarchyName + '] <> c.[Hierarchy];';

	EXECUTE(@SQL);

END;