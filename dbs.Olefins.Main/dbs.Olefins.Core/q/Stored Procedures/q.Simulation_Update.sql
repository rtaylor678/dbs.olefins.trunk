﻿
CREATE PROCEDURE q.Simulation_Update(
	  @tsQueued				DATETIMEOFFSET(7)	= NULL
	, @tsStart				DATETIMEOFFSET(7)	= NULL
	, @tsComplete			DATETIMEOFFSET(7)	= NULL
	, @ItemsQueued			INT					= NULL
	, @ItemsProcessed		INT					= NULL
	, @ErrorId				VARCHAR(12)			= NULL
	----------------------------------------------
	, @QueueId_init			BIGINT
	)
AS
	UPDATE q.Simulation
	SET	  tsQueued			= ISNULL(@tsQueued		, tsQueued)
		, tsStart			= ISNULL(@tsStart		, tsStart)
		, tsComplete		= ISNULL(@tsComplete	, tsComplete)
		, ItemsQueued		= ISNULL(@ItemsQueued	, ItemsQueued)
		, ItemsProcessed	= ISNULL(@ItemsProcessed, ItemsProcessed)
		, ErrorId			= ISNULL(@ErrorId		, ErrorId)
	WHERE QueueId = @QueueId_init;
