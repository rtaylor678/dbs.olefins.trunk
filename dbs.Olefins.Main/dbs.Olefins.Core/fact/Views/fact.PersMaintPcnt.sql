﻿CREATE VIEW [fact].[PersMaintPcnt]
WITH SCHEMABINDING
AS
SELECT
	a.[FactorSetId],
	a.[Refnum],
	a.[CalDateKey],
	a.[MeasureId],
	a.[OccMaintPlant] / (a.[OccSubTotal] - a.[OccMaintTa]) * 100.0	[OccMaint_Pcnt],
	a.[MpsMaintPlant] / (a.[MpsPlantSite] - a.[MpsMaintTa]) * 100.0	[MpsMaint_Pcnt]
FROM [fact].[PersAggregatePivot]	a
WHERE	a.[OccSubTotal]  - a.[OccMaintTa] <> 0.0
	AND	a.[MpsPlantSite] - a.[MpsMaintTa] <> 0.0;