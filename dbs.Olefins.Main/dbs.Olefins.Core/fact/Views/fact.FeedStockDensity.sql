﻿CREATE VIEW fact.FeedStockDensity
WITH SCHEMABINDING
AS
SELECT
	y.FactorSetId,
	t.Refnum,
	t.CalDateKey,
	p.StreamId,
	p.StreamDescription,
	COALESCE(MAX(p.Density_SG), SUM(d.Density_SG * k.Component_WtPcnt) / 100.00) [Density_SG]
FROM fact.TSortClient								t
INNER JOIN ante.FactorSetsInStudyYear				y
	ON	y._StudyYear = t._DataYear
INNER JOIN fact.FeedStockCrackingParameters			p
	ON	p.Refnum = t.Refnum
	AND	p.CalDateKey = t.CalDateKey
LEFT OUTER JOIN fact.CompositionQuantity			k
	ON	k.Refnum = t.Refnum
	AND	k.CalDateKey = t.CalDateKey
	AND	k.StreamId = p.StreamId
	AND	k.StreamDescription = p.StreamDescription
LEFT OUTER JOIN ante.ComponentDensity				d
	ON	d.FactorSetId = y.FactorSetId
	AND	d.ComponentId = k.ComponentId
GROUP BY
	y.FactorSetId,
	t.Refnum,
	t.CalDateKey,
	p.StreamId,
	p.StreamDescription;