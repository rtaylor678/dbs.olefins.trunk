﻿CREATE PROCEDURE [fact].[Select_PolyQuantity]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 12-2 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AssetId],
			[TrainId]		= RIGHT([f].[AssetId], 2),
		[f].[StreamId],
		[f].[Quantity_kMT],
		[n].[StreamName]
	FROM
		[fact].[PolyQuantity]			[f]
	LEFT OUTER JOIN
		[fact].[PolyQuantityNameOther]	[n]
			ON	[n].[Refnum]		= [f].[Refnum]
			AND	[n].[CalDateKey]	= [f].[CalDateKey]
			AND	[n].[AssetId]		= [f].[AssetId]
			AND	[n].[StreamId]		= [f].[StreamId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;