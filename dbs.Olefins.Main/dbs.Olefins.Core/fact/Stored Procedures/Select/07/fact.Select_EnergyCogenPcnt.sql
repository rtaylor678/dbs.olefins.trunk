﻿CREATE PROCEDURE [fact].[Select_EnergyCogenPcnt]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 7
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[FacilityId],
		[f].[CogenEnergy_Pcnt]
	FROM
		[fact].[EnergyCogenPcnt]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;