﻿CREATE PROCEDURE [fact].[Select_ReliabilityPyroFurnPlantLimit]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-5
	SELECT
		[r].[Refnum],
		[r].[CalDateKey],
		[r].[PlantFurnLimit_Pcnt]
	FROM
		[fact].[ReliabilityPyroFurnPlantLimit]			[r]
	WHERE
		[r].[Refnum]	= @Refnum;

END;