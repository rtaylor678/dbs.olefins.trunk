﻿CREATE PROCEDURE [fact].[Select_ReliabilityCriticalPath]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-2
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[FacilityId],
		[f].[CritPath_Bit],
		[CritPath_X] = CASE WHEN [f].[CritPath_Bit] = 1 THEN 'X' END,
		[n].[FacilityName]
	FROM
		[fact].[ReliabilityCritPath]			[f]
	LEFT OUTER JOIN
		[fact].[ReliabilityCritPathNameOther]	[n]
			ON	[n].[Refnum]		= [f].[Refnum]
			AND	[n].[CalDateKey]	= [f].[CalDateKey]
			AND	[n].[FacilityId]	= [f].[FacilityId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;