﻿CREATE PROCEDURE [fact].[Select_ReliabilityDTPredictiveMaint]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-6
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[CauseId],
		[f].[Frequency_Days]
	FROM
		[fact].[ReliabilityDTPredMaint]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;