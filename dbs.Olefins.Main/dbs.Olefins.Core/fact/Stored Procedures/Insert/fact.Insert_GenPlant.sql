﻿CREATE PROCEDURE [fact].[Insert_GenPlant]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlant(Refnum, CalDateKey
			, EthyleneCarrier_Bit, ExtractButadiene_Bit, ExtractAromatics_Bit, MTBE_Bit, Isobutylene_Bit
			, IsobutyleneHighPurity_Bit, Butene1_Bit, Isoprene_Bit, CycloPentadiene_Bit, OtherC5_Bit
			, PolyPlantShare_Bit, OlefinsDerivatives_Bit, IntegrationRefinery_Bit
			, CapCreepAnn_Pcnt
			, LocationFactor
			, OSIMPrep_Hrs)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		
			, etl.ConvBit(p.EtyCarrier)
			, etl.ConvBit(p.ButaExtract)
			, etl.ConvBit(p.AromExtract)
			, etl.ConvBit(p.MTBE)
			, etl.ConvBit(p.IsoMTBE)
			, etl.ConvBit(p.IsoHighPurity)
			, etl.ConvBit(p.Butene1)
			, etl.ConvBit(p.Isoprene)
			, etl.ConvBit(p.Cyclopent)
			, etl.ConvBit(p.OthC5)
			, etl.ConvBit(o.PolyPlantShare)
			, etl.ConvBit(o.Tbl1205)
			, etl.ConvBit(o.IntegrationRefine)
			
			, p.CapCreep			* 100.0
			, p.LocFactor
			, p.PrepHrs
		FROM stgFact.Prac p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		LEFT OUTER JOIN stgFact.PracOrg o ON o.Refnum = p.Refnum
		WHERE	(etl.ConvBit(p.EtyCarrier) IS NOT NULL
			OR	etl.ConvBit(p.ButaExtract) IS NOT NULL
			OR	etl.ConvBit(p.AromExtract) IS NOT NULL
			OR	etl.ConvBit(p.MTBE) IS NOT NULL
			OR	etl.ConvBit(p.IsoMTBE) IS NOT NULL
			OR	etl.ConvBit(p.IsoHighPurity) IS NOT NULL
			OR	etl.ConvBit(p.Butene1) IS NOT NULL
			OR	etl.ConvBit(p.Isoprene) IS NOT NULL
			OR	etl.ConvBit(p.Cyclopent) IS NOT NULL
			OR	etl.ConvBit(p.OthC5) IS NOT NULL
			OR	etl.ConvBit(o.PolyPlantShare) IS NOT NULL
			OR	etl.ConvBit(o.Tbl1205) IS NOT NULL
			OR	etl.ConvBit(o.IntegrationRefine) IS NOT NULL
			OR	p.CapCreep > 0.0
			OR	p.LocFactor IS NOT NULL
			OR	p.PrepHrs IS NOT NULL)
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;