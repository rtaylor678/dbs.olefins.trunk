﻿CREATE PROCEDURE [fact].[Insert_TSortClient]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[TSortClient]
		(
			[Refnum],
			[CalDateKey],
			[PlantCompanyName],
			[PlantAssetName],
			[UomId],
			[CurrencyFcn],
			[CurrencyRpt]
		)
		SELECT
			[Refnum]			= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
			[CalDateKey]		= [etl].[ConvDateKey]([t].[StudyYear]),

			[PlantCompanyName]	= RTRIM(LTRIM(COALESCE([t].[CoName],	[t].[Co],	[t].[CompanyId], N'————'))),
			[PlantAssetName]	= RTRIM(LTRIM(COALESCE([t].[PlantName], [t].[Loc],	[t].[PlantLoc], N'————'))),

			[UonId]			= CASE WHEN COALESCE([t].[UOM], 1) = 1
								THEN 'US'
								ELSE 'Metric'
								END,
			
			[CurrencyFcn]	= [k].[CurrencyId],

			[CurrencyRpt]	= 'USD'

		FROM 
			[stgFact].[TSort]				[t]

		LEFT OUTER JOIN
			[cons].[SubscriptionsAssets]	[s]
				ON	CHARINDEX([t].[Refnum], [s].[Refnum], 1) = 3
				AND [s].[Refnum]			= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId)

		LEFT OUTER JOIN
			[cons].[Assets]					[a]
				ON	[a].[AssetId]			= [s].[AssetId]

		LEFT OUTER JOIN
			[ante].[CountriesCurrencies]	[k]
				ON	[k].[CountryId]			= [a].[CountryId]
				AND [k].[InflationUse]		= 1
				AND [k].[CurrencyId]		NOT IN ('BRCK', 'RUR', 'ROLK')

		LEFT OUTER JOIN
			[fact].[TSortClient]			[x]
				ON	[x].[Refnum]			= @Refnum

		WHERE	[t].[Refnum]				= @sRefnum
			AND	[x].[Refnum]				IS NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;