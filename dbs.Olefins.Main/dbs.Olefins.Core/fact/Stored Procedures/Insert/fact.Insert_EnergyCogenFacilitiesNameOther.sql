﻿CREATE PROCEDURE [fact].[Insert_EnergyCogenFacilitiesNameOther]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.EnergyCogenFacilitiesNameOther(Refnum, CalDateKey, FacilityId, FacilityName)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvFacilityId(e.EnergyType)						[FacilityId]
			, e.OthDesc
		FROM stgFact.EnergyQnty e
		INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
		WHERE e.EnergyType = 'Other'
			AND	e.OthDesc IS NOT NULL
			AND (e.UnitCnt <> 0 OR e.Amount <> 0.0 OR e.Efficiency <> 0.0)
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;