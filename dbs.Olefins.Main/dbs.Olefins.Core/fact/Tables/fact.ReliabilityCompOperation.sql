﻿CREATE TABLE [fact].[ReliabilityCompOperation] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [Operation_Years] TINYINT            NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityCompOperation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompOperation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompOperation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompOperation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityCompOperation] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReliabilityCompOperation_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityCompOperation_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityCompOperation_u]
	ON [fact].[ReliabilityCompOperation]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityCompOperation]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityCompOperation].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityCompOperation].CalDateKey	= INSERTED.CalDateKey;

END;