﻿CREATE PROCEDURE [cons].[Insert_TSortSolomon]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [cons].[TSortSolomon]
		(
			[Refnum],
			[CalDateKey],
			[FactorSetId]
		)
		SELECT
			[Refnum]			= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
			[CalDateKey]		= [etl].[ConvDateKey]([t].[StudyYear]),
			[FactorSetId]		= [t].[StudyYear]
		FROM
			[stgFact].[TSort]			[t]
		LEFT OUTER JOIN
			[cons].[TSortSolomon]		[s]
				ON	CHARINDEX(RTRIM(LTRIM([t].[Refnum])), RTRIM(LTRIM([s].[Refnum])), 1) = 3
		WHERE	[t].[Refnum]	= @sRefnum
			AND	[s].[Refnum]	IS NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;