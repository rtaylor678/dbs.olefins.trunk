﻿CREATE VIEW [cons].[TSort]
WITH SCHEMABINDING
AS
SELECT
		[Refnum]			= RTRIM(LTRIM([t].[Refnum])),
		[SmallRefnum]		= SUBSTRING(RTRIM(LTRIM([t].[Refnum])), 3, LEN(RTRIM(LTRIM([t].[Refnum]))) - 2),

	[t].[CalDateKey],

		[Co]				= RTRIM(LTRIM([c].[CompanyNameReport])),
		[Loc]				= RTRIM(LTRIM([s].[AssetNameReport])),
		[CoLoc]				= RTRIM(LTRIM([c].[CompanyNameReport])) + ' - ' + RTRIM(LTRIM([s].[AssetNameReport])),

		[StudyYear]			= [t].[_StudyYear],
	[t].[Consultant],
	[t].[FactorSetId],
	[s].[StudyId],

	--	Asset

	[s].[AssetId],
	[s].[AssetNameReport],
	[a].[AssetName],
	[a].[AssetDetail],
	[s].[SubscriberAssetName],
	[s].[SubscriberAssetDetail],
		[AssetPassWord]				= [s].[PassWord_PlainText],

		[Region]					= [g].[RegionId],
	[g].[RegionId],
	[e].[EconRegionId],
	[a].[CountryId],
	[l].[CountryName],
	[a].[StateName],

	[a].[Longitude_Deg],
	[a].[Latitude_Deg],
		[GeogLoc_WGS84]				= [a].[_GeogLoc_WGS84],

	[s].[ScenarioId],
	[s].[ScenarioName],
	[s].[ScenarioDetail],

	--	Company

	[c].[CompanyId],
	[c].[CompanyNameReport],
	[k].[CompanyName],
	[k].[CompanyDetail],
	[c].[SubscriberCompanyName],
	[c].[SubscriberCompanyDetail],
		[CompanyPassWord]			= [c].[PassWord_PlainText],

	[t].[Comments],
	[t].[ContactCode],
	[t].[RefDirectory]

FROM
	[cons].[TSortSolomon]			[t]

INNER JOIN
	[cons].[SubscriptionsAssets]	[s]
		ON	[s].[Refnum]			= [t].[Refnum]
		AND	[s].[CalDateKey]		= [t].[CalDateKey]
		AND	[s].[Active]			= 1

INNER JOIN
	[cons].[Assets]					[a]
		ON	[a].[AssetId]			= [s].[AssetId]

INNER JOIN
	[dim].[Country_LookUp]			[l]
		ON	[l].[CountryId]			= [a].[CountryId]

INNER JOIN
	[ante].[CountryEconRegion]		[e]
		ON	[e].[FactorSetId]		= [t].[FactorSetId]
		AND	[e].[CountryId]			= [a].[CountryId]

INNER JOIN
	[ante].[CountryRegion_Geo]		[g]
		ON	[g].[CountryId]			= [a].[CountryId]
		
INNER JOIN
	[cons].[SubscriptionsCompanies]	[c]
		ON	[c].[CompanyId]			= [s].[CompanyId]
		AND	[c].[CalDateKey]		= [t].[CalDateKey]
		AND	[c].[StudyId]			= [s].[StudyId]
		AND	[c].[Active]			= 1

INNER JOIN
	[dim].[Company_LookUp]			[k]
		ON	[k].[CompanyId]			= [c].[CompanyId]

WHERE
	[t].[Active]					= 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UX_cons_TSort]
ON [cons].[TSort]
(
	[Refnum] ASC
);
GO