﻿


CREATE              PROC [reports].[spPyrolysis](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DELETE FROM reports.Pyrolysis WHERE GroupId = @GroupId
Insert into reports.Pyrolysis(GroupId
, Scenario
, Currency
, SimModelId
, DataType
, C2H4_OSOP
, C3H6_OSOP
, OthHVChem_OSOP
, H2_OSOP
, C2H2_OSOP
, C4H6_OSOP
, C6H6_OSOP
, C2H4_MS25
, C3H6_MS25
, OthHVChem_MS25
, H2_MS25
, C4H6_MS25
, C6H6_MS25
, C2H4_OS25
, C3H6_OS25
, OthHVChem_OS25
, H2_OS25
, C4H6_OS25
, C6H6_OS25
, C2H4YIR_MS25
, C2H4YIR_OS25
, C2H4YIR_OSOP
, ProdHVCYIR_MS25
, ProdHVCYIR_OS25
, ProdHVCYIR_OSOP
, PyroPV_Act_CurrPerMT
, PyroPV_MS25_CurrPerMT
, PyroPV_OS25_CurrPerMT
, PyroPV_OSOP_CurrPerMT
, ActPPV_PcntMS25
, ActPPV_PcntOS25
, ActPPV_PcntOSOP
, SeverityIndex
, FurnConstructionYear
, FurnResidenceTime
)

SELECT GroupId = @GroupId
, Scenario				= p.Scenario
, Currency				= p.Currency
, SimModelId			= p.SimModelId
, DataType				= p.DataType
, C2H4_OSOP				= [$(DbGlobal)].dbo.WtAvg(C2H4_OSOP, d.FreshPyroFeed_kMT)
, C3H6_OSOP				= [$(DbGlobal)].dbo.WtAvg(C3H6_OSOP, d.FreshPyroFeed_kMT)
, OthHVChem_OSOP		= [$(DbGlobal)].dbo.WtAvg(OthHVChem_OSOP, d.FreshPyroFeed_kMT)
, H2_OSOP				= [$(DbGlobal)].dbo.WtAvg(H2_OSOP, d.FreshPyroFeed_kMT)
, C2H2_OSOP				= [$(DbGlobal)].dbo.WtAvg(C2H2_OSOP, d.FreshPyroFeed_kMT)
, C4H6_OSOP				= [$(DbGlobal)].dbo.WtAvg(C4H6_OSOP, d.FreshPyroFeed_kMT)
, C6H6_OSOP				= [$(DbGlobal)].dbo.WtAvg(C6H6_OSOP, d.FreshPyroFeed_kMT)
, C2H4_MS25				= [$(DbGlobal)].dbo.WtAvg(C2H4_MS25, d.FreshPyroFeed_kMT)
, C3H6_MS25				= [$(DbGlobal)].dbo.WtAvg(C3H6_MS25, d.FreshPyroFeed_kMT)
, OthHVChem_MS25		= [$(DbGlobal)].dbo.WtAvg(OthHVChem_MS25, d.FreshPyroFeed_kMT)
, H2_MS25				= [$(DbGlobal)].dbo.WtAvg(H2_MS25, d.FreshPyroFeed_kMT)
, C4H6_MS25				= [$(DbGlobal)].dbo.WtAvg(C4H6_MS25, d.FreshPyroFeed_kMT)
, C6H6_MS25				= [$(DbGlobal)].dbo.WtAvg(C6H6_MS25, d.FreshPyroFeed_kMT)
, C2H4_OS25				= [$(DbGlobal)].dbo.WtAvg(C2H4_OS25, d.FreshPyroFeed_kMT)
, C3H6_OS25				= [$(DbGlobal)].dbo.WtAvg(C3H6_OS25, d.FreshPyroFeed_kMT)
, OthHVChem_OS25		= [$(DbGlobal)].dbo.WtAvg(OthHVChem_OS25, d.FreshPyroFeed_kMT)
, H2_OS25				= [$(DbGlobal)].dbo.WtAvg(H2_OS25, d.FreshPyroFeed_kMT)
, C4H6_OS25				= [$(DbGlobal)].dbo.WtAvg(C4H6_OS25, d.FreshPyroFeed_kMT)
, C6H6_OS25				= [$(DbGlobal)].dbo.WtAvg(C6H6_OS25, d.FreshPyroFeed_kMT)
, C2H4YIR_MS25			= [$(DbGlobal)].dbo.WtAvg(C2H4YIR_MS25, d.EthyleneDiv_kMT)
, C2H4YIR_OS25			= [$(DbGlobal)].dbo.WtAvg(C2H4YIR_OS25, d.EthyleneDiv_kMT)
, C2H4YIR_OSOP			= [$(DbGlobal)].dbo.WtAvg(C2H4YIR_OSOP, d.EthyleneDiv_kMT)
, ProdHVCYIR_MS25		= [$(DbGlobal)].dbo.WtAvg(ProdHVCYIR_MS25, d.HvcProd_kMT)
, ProdHVCYIR_OS25		= [$(DbGlobal)].dbo.WtAvg(ProdHVCYIR_OS25, d.HvcProd_kMT)
, ProdHVCYIR_OSOP		= [$(DbGlobal)].dbo.WtAvg(ProdHVCYIR_OSOP, d.HvcProd_kMT)
, PyroPV_Act_CurrPerMT	= [$(DbGlobal)].dbo.WtAvg(PyroPV_Act_CurrPerMT, d.FreshPyroFeed_kMT)
, PyroPV_MS25_CurrPerMT = [$(DbGlobal)].dbo.WtAvg(PyroPV_MS25_CurrPerMT, d.FreshPyroFeed_kMT)
, PyroPV_OS25_CurrPerMT = [$(DbGlobal)].dbo.WtAvg(PyroPV_OS25_CurrPerMT, d.FreshPyroFeed_kMT)
, PyroPV_OSOP_CurrPerMT = [$(DbGlobal)].dbo.WtAvg(PyroPV_OSOP_CurrPerMT, d.FreshPyroFeed_kMT)
, ActPPV_PcntMS25		= [$(DbGlobal)].dbo.WtAvg(ActPPV_PcntMS25, d.FreshPyroFeed_kMT)
, ActPPV_PcntOS25		= [$(DbGlobal)].dbo.WtAvg(ActPPV_PcntOS25, d.FreshPyroFeed_kMT)
, ActPPV_PcntOSOP		= [$(DbGlobal)].dbo.WtAvg(ActPPV_PcntOSOP, d.FreshPyroFeed_kMT)
, SeverityIndex			= CASE WHEN [$(DbGlobal)].dbo.WtAvg(C2H4_MS25, d.FreshPyroFeed_kMT) > 0 THEN
							[$(DbGlobal)].dbo.WtAvg(C2H4_OS25, d.FreshPyroFeed_kMT) / [$(DbGlobal)].dbo.WtAvg(C2H4_MS25, d.FreshPyroFeed_kMT) * 100.0 END
, FurnConstructionYear	= [$(DbGlobal)].dbo.WtAvg(FurnConstructionYear, d.FreshPyroFeed_kMT)
, FurnResidenceTime		= [$(DbGlobal)].dbo.WtAvg(FurnResidenceTime, d.FreshPyroFeed_kMT)
FROM @MyGroup r JOIN dbo.Pyrolysis p on p.Refnum=r.Refnum
JOIN dbo.Divisors d on d.Refnum=p.Refnum
Group by p.Currency, p.Scenario, p.SimModelId, p.DataType


SET NOCOUNT OFF

















