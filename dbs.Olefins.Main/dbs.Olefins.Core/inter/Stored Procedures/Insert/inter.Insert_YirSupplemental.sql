﻿CREATE PROCEDURE [inter].[Insert_YirSupplemental]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Map	TABLE
		(
			[ComponentID_Nat]	VARCHAR(42)			NOT	NULL	CHECK([ComponentID_Nat] <> ''),	-- Natural
			[ComponentID_Bal]	VARCHAR(42)			NOT	NULL	CHECK([ComponentID_Bal] <> ''),	-- Balance

			PRIMARY KEY CLUSTERED ([ComponentID_Nat] ASC)
		);

		INSERT INTO @Map([ComponentID_Nat], [ComponentID_Bal])
		VALUES
			('C2H4', 'C2H6'),
			('C3H6', 'C3H8'),
			('C4H6', 'C4H8'),
			('C6H6', 'PyroGasoline');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.YIRSupplemental (Concentrated)';
		PRINT @ProcedureDesc;

		DECLARE @ConcBasis TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0),
			[Recovered_kMT]			REAL				NOT	NULL	CHECK([Recovered_kMT] >= 0.0),
			[_Balance_kMT]			AS CONVERT(REAL, ROUND([Component_kMT] - COALESCE([Recovered_kMT], 0.0), 5))
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @ConcBasis([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT], [Recovered_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			c.[ComponentId],
			SUM(c.[Component_kMT]),
			SUM(c.[Component_kMT] * ISNULL(r.[Recovered_WtPcnt], 100.0)) / 100.0
		FROM @fpl											fpl
		INNER JOIN [dim].[Stream_Bridge]					b
			ON	b.[FactorSetId]			= fpl.[FactorSetId]
			AND	b.[StreamId]			= 'SuppConc'
		INNER JOIN [calc].[CompositionStream]				c
			ON	c.[FactorSetId]			= fpl.[FactorSetId]
			AND c.[Refnum]				= fpl.[Refnum]
			AND	c.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			AND	c.[StreamId]			= b.[DescendantId]
		LEFT OUTER JOIN [fact].[QuantitySuppRecovery]		r
			ON	r.[Refnum]				= fpl.[Refnum]
			AND	r.CalDateKey			= fpl.[Plant_AnnDateKey]
			AND	r.[StreamId]			= c.[StreamId]
			AND	r.[StreamDescription]	= c.[StreamDescription]
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			c.[ComponentId];

		DECLARE @ConcTemp TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0)
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @ConcTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			b.[Recovered_kMT]
		FROM @ConcBasis		b
		WHERE b.[Component_kMT] > 0.0;

		INSERT INTO @ConcTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			m.[ComponentID_Bal],
			b.[_Balance_kMT]
		FROM @ConcBasis		b
		INNER JOIN @Map		m
			ON	m.[ComponentID_Nat] = b.[ComponentId]
		WHERE b.[_Balance_kMT] > 0.0;

		DECLARE @Concentrated TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0)
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @Concentrated([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			SUM(b.[Component_kMT])
		FROM @ConcTemp				b
		GROUP BY
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId]
		HAVING SUM(b.[Component_kMT]) > 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.YIRSupplemental (Refinery Off Gas)';
		PRINT @ProcedureDesc;

		DECLARE @RogBasis TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0),
			[Recovered_kMT]			REAL				NOT	NULL	CHECK([Recovered_kMT] >= 0.0),
			[_Balance_kMT]			AS CONVERT(REAL, ROUND([Component_kMT] - COALESCE([Recovered_kMT], 0.0), 5))
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @RogBasis([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT], [Recovered_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			c.[ComponentId],
			SUM(c.[Component_kMT]),
			SUM(c.[Component_kMT] * ISNULL(r.[Recovered_WtPcnt], 100.0)) / 100.0
		FROM @fpl											fpl
		INNER JOIN [dim].[Stream_Bridge]					b
			ON	b.[FactorSetId]			= fpl.[FactorSetId]
			AND	b.[StreamId]			= 'SuppRog'
		INNER JOIN [calc].[CompositionStream]				c
			ON	c.[FactorSetId]			= fpl.[FactorSetId]
			AND c.[Refnum]				= fpl.[Refnum]
			AND	c.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			AND	c.[StreamId]			= b.[DescendantId]
		LEFT OUTER JOIN [fact].[QuantitySuppRecovery]		r
			ON	r.[Refnum]				= fpl.[Refnum]
			AND	r.CalDateKey			= fpl.[Plant_AnnDateKey]
			AND	r.[StreamId]			= c.[StreamId]
			AND	r.[StreamDescription]	= c.[StreamDescription]
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			c.[ComponentId];

		DECLARE @RogTemp TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0)
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @RogTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			b.[Recovered_kMT]
		FROM @RogBasis		b
		WHERE b.[Component_kMT] > 0.0;

		INSERT INTO @RogTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			m.[ComponentID_Bal],
			b.[_Balance_kMT]
		FROM @RogBasis		b
		INNER JOIN @Map		m
			ON	m.[ComponentID_Nat] = b.[ComponentId]
		WHERE b.[_Balance_kMT] > 0.0;

		DECLARE @RefOffGas TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0)
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @RefOffGas([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			SUM(b.[Component_kMT])
		FROM @RogTemp				b
		GROUP BY
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId]
		HAVING SUM(b.[Component_kMT]) > 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.YIRSupplemental (Dilute)';
		PRINT @ProcedureDesc;

		DECLARE @DilBasis TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0),
			[Recovered_kMT]			REAL				NOT	NULL	CHECK([Recovered_kMT] >= 0.0),
			[_Balance_kMT]			AS CONVERT(REAL, ROUND([Component_kMT] - COALESCE([Recovered_kMT], 0.0), 5))
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @DilBasis([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT], [Recovered_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			
			CASE c.[ComponentId]
				WHEN 'PyroGasOil' THEN 'PyroFuelOil'
				ELSE c.[ComponentId]
				END,
			
			SUM(c.[Component_kMT]),
			SUM(c.[Component_kMT] * ISNULL(r.[Recovered_WtPcnt], 100.0)) / 100.0
		FROM @fpl											fpl
		INNER JOIN [dim].[Stream_Bridge]					b
			ON	b.[FactorSetId]			= fpl.[FactorSetId]
			AND	b.[StreamId]			IN ('SuppDil', 'SuppMisc')
			AND	b.[DescendantId]		<> 'SuppOther'
		INNER JOIN [calc].[CompositionStream]				c
			ON	c.[FactorSetId]			= fpl.[FactorSetId]
			AND c.[Refnum]				= fpl.[Refnum]
			AND	c.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			AND	c.[StreamId]			= b.[DescendantId]
		LEFT OUTER JOIN [fact].[QuantitySuppRecovery]		r
			ON	r.[Refnum]				= fpl.[Refnum]
			AND	r.CalDateKey			= fpl.[Plant_AnnDateKey]
			AND	r.[StreamId]			= c.[StreamId]
			AND	r.[StreamDescription]	= c.[StreamDescription]
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			c.[ComponentId];

		DECLARE @DilTemp TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0)
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @DilTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			b.[Recovered_kMT]
		FROM @DilBasis		b
		WHERE b.[Component_kMT] > 0.0;

		INSERT INTO @DilTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			m.[ComponentID_Bal],
			b.[_Balance_kMT]
		FROM @DilBasis		b
		INNER JOIN @Map		m
			ON	m.[ComponentID_Nat] = b.[ComponentId]
		WHERE b.[_Balance_kMT] > 0.0;

		DECLARE @Dilute TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0)
								
		--	PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
		);

		INSERT INTO @Dilute([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			SUM(b.[Component_kMT])
		FROM @DilTemp				b
		GROUP BY
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId]
		HAVING SUM(b.[Component_kMT]) > 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.YIRSupplemental (Aggregations)';
		PRINT @ProcedureDesc;

		DECLARE @SuppTemp TABLE
		(
			[FactorSetId]			VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]				VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]			INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[ComponentId]			VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
	
			[Component_kMT]			REAL				NOT	NULL	CHECK([Component_kMT] >= 0.0)
		);

		INSERT INTO @SuppTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			b.[Component_kMT]
		FROM @Concentrated		b;

		INSERT INTO @SuppTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			b.[Component_kMT]
		FROM @RefOffGas			b;

		INSERT INTO @SuppTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			b.[FactorSetId],
			b.[Refnum],
			b.[CalDateKey],
			b.[ComponentId],
			b.[Component_kMT]
		FROM @Dilute			b;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.YIRSupplemental (Aggregations - SuppOther)';
		PRINT @ProcedureDesc;

		INSERT INTO @SuppTemp([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			b.[ComponentId],
			SUM(b.[Component_kMT])
		FROM	@fpl											fpl
		INNER JOIN calc.[CompositionStream]						b
			ON	b.[FactorSetId]	= fpl.[FactorSetId]
			AND b.[Refnum]		= fpl.[Refnum]
			AND	b.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND b.[StreamId]	= 'SuppOther'
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			b.[ComponentId];

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.YIRSupplemental';
		PRINT @ProcedureDesc;

		INSERT INTO [inter].[YIRSupplemental]([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Component_kMT], [Component_WtPcnt])
		SELECT
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[Component_kMT],
			t.[Component_kMT] / SUM(t.[Component_kMT]) OVER(PARTITION BY t.[FactorSetId], t.[Refnum], t.[CalDateKey]) * 100.0
		FROM (
			SELECT
				b.[FactorSetId],
				b.[Refnum],
				b.[CalDateKey],
				b.[ComponentId],
				SUM(b.[Component_kMT])		[Component_kMT]
			FROM @SuppTemp							b
			GROUP BY
				b.[FactorSetId],
				b.[Refnum],
				b.[CalDateKey],
				b.[ComponentId]
			) t
		WHERE t.[Component_kMT] <> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;