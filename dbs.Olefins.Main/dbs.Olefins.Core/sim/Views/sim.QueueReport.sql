﻿CREATE VIEW [sim].[QueueReport]
WITH SCHEMABINDING
AS
SELECT
	[a].[QueueId],
	[a].[FactorSetId],
	[a].[Refnum],
	[a].[ModelId],
	[a].[Notes],
	[a].[Enqueued],
	[b].[Start],
	[b].[ItemsQueued],
	[c].[Complete],
	[c].[ItemsProcessed],
	[c].[ErrorCount]
FROM
	[sim].[QueueAdd]		[a]
LEFT OUTER JOIN
	[sim].[QueueBegin]		[b]
		ON	[b].[QueueId]	= [a].[QueueId]
LEFT OUTER JOIN
	[sim].[QueueComplete]	[c]
		ON	[c].[QueueId]	= [a].[QueueId]
