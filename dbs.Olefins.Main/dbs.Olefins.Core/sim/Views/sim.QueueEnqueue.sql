﻿CREATE VIEW [sim].[QueueEnqueue]
WITH SCHEMABINDING
AS
SELECT
	[a].[QueueId],
	[a].[FactorSetId],
	[a].[Refnum],
	[a].[ModelId],
	[a].[Notes],
	[a].[Enqueued]
FROM
	[sim].[QueueAdd]	[a]
LEFT OUTER JOIN
	[sim].[QueueBegin]	[b]
		ON	[b].[QueueId]	= [a].[QueueId]
WHERE	[b].[QueueId]	IS NULL;