﻿CREATE PROCEDURE [sim].[Insert_Temp_YieldCompositionStream]
(
	@FactorSetId		VARCHAR(12),
	@Refnum				VARCHAR(25),
	@ModelId			VARCHAR(12),
	@OpCondId			VARCHAR(12),
	@StreamId			VARCHAR(42),
	@StreamDescription	VARCHAR(256),
	@RecycleId			INT,
	@ErrorId			VARCHAR(12),
	@ComponentId		VARCHAR(42),
	@Component_WtPcnt	REAL
)
AS
BEGIN

	INSERT INTO [sim].[Temp_YieldCompositionStream]
	(
		[FactorSetId],
		[Refnum],
		[ModelId],
		[OpCondId],
		[StreamId],
		[StreamDescription],
		[RecycleId],
		[ErrorId],
		[ComponentId],
		[Component_WtPcnt]
	)
	VALUES
	(
		@FactorSetId,
		@Refnum,
		@ModelId,
		@OpCondId,
		@StreamId,
		@StreamDescription,
		@RecycleId,
		@ErrorId,
		@ComponentId,
		@Component_WtPcnt
	);

	RETURN 0;

END;