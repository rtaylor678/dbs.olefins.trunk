﻿CREATE VIEW [gap].[AnalysisResultsDiv]
WITH SCHEMABINDING
AS
SELECT
	g.[FactorSetId],
	g.[GroupId],
	g.[TargetId],
	g.[GroupProdCap_kMT],
	g.[BenchProdCap_kMT],
	g.[GapId],
	g.[CurrencyId],
	g.[GapAmount_Cur],
	[GapAmount_Div] = COALESCE(g.[DivAmount_Cur], g.[GapAmount_Cur] / CASE WHEN d.[GapId] IS NOT NULL THEN g.[BenchProdCap_kMT] ELSE g.[GroupProdCap_kMT] END * 1000.0)
FROM [gap].[AnalysisResults]				g
LEFT OUTER JOIN [dim].[Gap_Bridge]			d
	ON 	d.[FactorSetId]		= g.[FactorSetId]
	AND	d.[DescendantId]	= g.[GapId]
	AND	d.[GapId]			IN ('FeedProdPrices', 'NetCashMargin');
