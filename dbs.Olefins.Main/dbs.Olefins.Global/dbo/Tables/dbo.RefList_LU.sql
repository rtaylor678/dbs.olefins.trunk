﻿CREATE TABLE [dbo].[RefList_LU] (
    [RefListNo]   [dbo].[RefListNo] IDENTITY (1, 1) NOT NULL,
    [ListName]    VARCHAR (30)      NOT NULL,
    [CreateDate]  SMALLDATETIME     CONSTRAINT [DF_RefineryLi_CreateDate14__13] DEFAULT (getdate()) NOT NULL,
    [Owner]       VARCHAR (5)       NULL,
    [JobNo]       VARCHAR (10)      NULL,
    [Description] VARCHAR (100)     NULL,
    CONSTRAINT [PK___13__13] PRIMARY KEY CLUSTERED ([RefListNo] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqueRefListName] UNIQUE NONCLUSTERED ([ListName] ASC) WITH (FILLFACTOR = 90)
);


GO

/****** Object:  Trigger dbo.RefListLUModify    Script Date: 4/18/2003 4:35:20 PM ******/
/****** Object:  Trigger dbo.RefListLUModify    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE TRIGGER RefListLUModify ON dbo.RefList_LU 
FOR UPDATE,DELETE 
AS
DECLARE @ListName varchar(30), @UName varchar(30), @UGroup varchar(30)
SELECT @UName = User_Name(), @UGroup = name 
FROM sysusers
WHERE uid = (SELECT gid from sysusers
		WHERE name = User_Name())
IF NOT EXISTS (SELECT * FROM deleted
	WHERE Owner <> @UName AND Owner IS NOT NULL)
OR
	@UName = 'dbo'
OR
	@UGroup IN ('developer')
	
	DELETE FROM RefList
	WHERE RefListNo IN
		(SELECT DISTINCT RefListNo FROM deleted
		WHERE RefListNo NOT IN
			(SELECT DISTINCT RefListNo FROM inserted))
ELSE
BEGIN
	SELECT @ListName = ListName FROM deleted
	WHERE Owner <> @UName AND Owner IS NOT NULL
 
	RAISERROR ('Insufficient rights to modify the list %s.  Transaction rolled back.',
		16, -1, @ListName)
	ROLLBACK TRANSACTION
END

