﻿CREATE TABLE [dbo].[Project_LU] (
    [ProjectID]   VARCHAR (15) NOT NULL,
    [Location]    CHAR (7)     NULL,
    [SortOrder]   TINYINT      NULL,
    [Description] VARCHAR (60) NULL,
    CONSTRAINT [PK___19__14] PRIMARY KEY CLUSTERED ([ProjectID] ASC)
);

