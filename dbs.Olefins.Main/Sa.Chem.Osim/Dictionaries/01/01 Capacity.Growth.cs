﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Capacity
		{
			internal class Growth
			{
				internal void CustomLoad(SqlConnection cn, Excel.Workbook wkb, string tabNamePrefix, string refnum, bool forceIn, bool IsFullLoad)
				{
					string sheetName = tabNamePrefix + Tabs.T01_02;

					if (wkb.SheetExists(sheetName))
					{
						Excel.Worksheet wks = null;
						Excel.Range rng = null;

						int r = 0;
						int c = 0;
						int cYear = 5;

						int vYear;
						int studyYearDbs;
						int syd;

						wks = wkb.Worksheets[sheetName];

						Dictionary<int, int> rowYear = new Dictionary<int, int>();

						for (int i = 11; i <= 32; i++)
						{
							if (IsFullLoad || i <= 21)
							{
								rng = wks.Cells[i, cYear];
								if (int.TryParse(rng.Text, out vYear))
								{
									rowYear.Add(vYear, i);
								}
							}
						}

						using (SqlCommand cmd = new SqlCommand("[fact].[Select_CapacityGrowth]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

							using (SqlDataReader rdr = cmd.ExecuteReader())
							{
								while (rdr.Read())
								{
									studyYearDbs = rdr.GetInt32(rdr.GetOrdinal("Date_Year"));

									if (rowYear.TryGetValue(studyYearDbs, out r))
									{
										syd = rdr.GetInt16(rdr.GetOrdinal("StudyYearDifference"));
										c = (rdr.GetSqlString(rdr.GetOrdinal("StreamId")) == "Ethylene") ? 7 : 8;

										if (syd >= 0)
										{
											XL.SetCellValue(rdr, "Prod_kMT", wks, r, c, forceIn);

											if (c == 7)
											{
												XL.SetCellValue(rdr, "Capacity_kMT", wks, r, c + 2, forceIn);
											}
										}
										else
										{
											XL.SetCellValue(rdr, "Capacity_kMT", wks, r, c, forceIn);
										}
									}
								}
							}
						}
					}
				}

				private static RangeReference year = new RangeReference(Tabs.T01_02, 0, 5, SqlDbType.Int);

				private static RangeReference pastProdEthylene = new RangeReference(Tabs.T01_02, 0, 7, SqlDbType.Float);
				private static RangeReference pastProdPropylene = new RangeReference(Tabs.T01_02, 0, 8, SqlDbType.Float);
				private static RangeReference pastCapEthylene = new RangeReference(Tabs.T01_02, 0, 9, SqlDbType.Float);

				private static RangeReference futureCapEthylene = new RangeReference(Tabs.T01_02, 0, 7, SqlDbType.Float);
				private static RangeReference futureCapPropylene = new RangeReference(Tabs.T01_02, 0, 8, SqlDbType.Float);

				internal class Current
				{
					internal class Upload : TransferData.IUploadMultiple
					{
						public string StoredProcedure
						{
							get
							{
								return "[stgFact].[Insert_CapGrowth]";
							}
						}

						public Dictionary<string, RangeReference> Parameters
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("@Year", Growth.year);
								d.Add("@EthylProdn", Growth.pastProdEthylene);
								d.Add("@PropylProdn", Growth.pastProdPropylene);
								d.Add("@EthylCap", Growth.pastCapEthylene);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								for (int i = 11; i <= 23; i++)
								{
									d.Add("I" + i.ToString(), new RangeReference(Tabs.T01_02, i, 0));
								}

								return d;
							}
						}
					}
				}

				internal class Future
				{
					internal class Upload : TransferData.IUploadMultiple
					{
						public string StoredProcedure
						{
							get
							{
								return "[stgFact].[Insert_CapGrowth]";
							}
						}

						public Dictionary<string, RangeReference> Parameters
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("@Year", Growth.year);
								d.Add("@EthylProdn", Growth.futureCapEthylene);
								d.Add("@PropylProdn", Growth.futureCapPropylene);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								for (int i = 30; i <= 32; i++)
								{
									d.Add("I" + i.ToString(), new RangeReference(Tabs.T01_02, i, 0));
								}

								return d;
							}
						}
					}
				}
			}
		}
	}
}