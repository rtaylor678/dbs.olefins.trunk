﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal partial class Models
			{
				private static RangeReference Implemented_YN = new RangeReference(Tabs.T11, 0, 7, SqlDbType.VarChar);
				private static RangeReference LocationID = new RangeReference(Tabs.T11, 0, 8, SqlDbType.VarChar);

				private static RangeReference LP = new RangeReference(Tabs.T11, 91, 0, SqlDbType.VarChar);
				private static RangeReference Scheduling = new RangeReference(Tabs.T11, 92, 0, SqlDbType.VarChar);
				private static RangeReference LPMultiPlant = new RangeReference(Tabs.T11, 93, 0, SqlDbType.VarChar);
				private static RangeReference LPMultiPeriod = new RangeReference(Tabs.T11, 94, 0, SqlDbType.VarChar);
				private static RangeReference PricingTrieredFeed = new RangeReference(Tabs.T11, 95, 0, SqlDbType.VarChar);
				private static RangeReference PricingTieredProd = new RangeReference(Tabs.T11, 96, 0, SqlDbType.VarChar);
				private static RangeReference NonLP = new RangeReference(Tabs.T11, 97, 0, SqlDbType.VarChar);
				private static RangeReference SpreadSheet = new RangeReference(Tabs.T11, 98, 0, SqlDbType.VarChar);
				private static RangeReference Margin = new RangeReference(Tabs.T11, 99, 0, SqlDbType.VarChar);
				private static RangeReference CaseStudies = new RangeReference(Tabs.T11, 100, 0, SqlDbType.VarChar);
				private static RangeReference BreakEven = new RangeReference(Tabs.T11, 101, 0, SqlDbType.VarChar);
				private static RangeReference YPS = new RangeReference(Tabs.T11, 102, 0, SqlDbType.VarChar);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_FeedSelModels]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "ModelId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Implemented_YN", Models.Implemented_YN);
							d.Add("LocationID", Models.LocationID);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("LP", Models.LP);
							d.Add("Scheduling", Models.Scheduling);
							d.Add("LPMultiPlant", Models.LPMultiPlant);
							d.Add("LPMultiPeriod", Models.LPMultiPeriod);
							d.Add("PricingTrieredFeed", Models.PricingTrieredFeed);
							d.Add("PricingTieredProd", Models.PricingTieredProd);
							d.Add("NonLP", Models.NonLP);
							d.Add("SpreadSheet", Models.SpreadSheet);
							d.Add("Margin", Models.Margin);
							d.Add("CaseStudies", Models.CaseStudies);
							d.Add("BreakEven", Models.BreakEven);
							d.Add("YPS", Models.YPS);

							return d;
						}
					}
				}
			}
		}
	}
}