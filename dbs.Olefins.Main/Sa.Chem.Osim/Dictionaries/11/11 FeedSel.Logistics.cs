﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal class Logistics
			{
				private static RangeReference LPG = new RangeReference(Tabs.T11, 0, 5, SqlDbType.Float, 100.0);
				private static RangeReference Condensate = new RangeReference(Tabs.T11, 0, 6, SqlDbType.Float, 100.0);
				private static RangeReference Naphtha = new RangeReference(Tabs.T11, 0, 7, SqlDbType.Float, 100.0);
				private static RangeReference LiqHeavy = new RangeReference(Tabs.T11, 0, 8, SqlDbType.Float, 100.0);

				private static RangeReference Pipeline = new RangeReference(Tabs.T11, 77, 0, SqlDbType.Float, 100.0);
				private static RangeReference RailTruck = new RangeReference(Tabs.T11, 78, 0, SqlDbType.Float, 100.0);
				private static RangeReference Barge = new RangeReference(Tabs.T11, 79, 0, SqlDbType.Float, 100.0);
				private static RangeReference Ship = new RangeReference(Tabs.T11, 80, 0, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownloadIntersection
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_FeedSelLogistics]";
						}
					}

					public string ValueColumn
					{
						get
						{
							return "Delivery_Pcnt";
						}
					}

					public string RowLookUpColumn
					{
						get
						{
							return "FacilityId";
						}
					}

					public string ColLookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public Dictionary<string, RangeReference> Cols
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("LPG", Logistics.LPG);
							d.Add("Condensate", Logistics.Condensate);
							d.Add("Naphtha", Logistics.Naphtha);
							d.Add("LiqHeavy", Logistics.LiqHeavy);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Rows
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Pipeline", Logistics.Pipeline);
							d.Add("RailTruck", Logistics.RailTruck);
							d.Add("Barge", Logistics.Barge);
							d.Add("Ship", Logistics.Ship);

							return d;
						}
					}
				}
			}
		}
	}
}