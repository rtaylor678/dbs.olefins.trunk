﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal partial class Model
			{
				internal class Penalty
				{
					private static RangeReference Penalty_YN = new RangeReference(Tabs.T11, 0, 10, SqlDbType.VarChar);

					private static RangeReference FeedSlateChange = new RangeReference(Tabs.T11, 156, 10, SqlDbType.VarChar);
					private static RangeReference Reliability = new RangeReference(Tabs.T11, 157, 10, SqlDbType.VarChar);
					private static RangeReference Envrionment = new RangeReference(Tabs.T11, 158, 10, SqlDbType.VarChar);
					private static RangeReference FurnRunLength = new RangeReference(Tabs.T11, 159, 10, SqlDbType.VarChar);
					private static RangeReference Maintenance = new RangeReference(Tabs.T11, 160, 10, SqlDbType.VarChar);
					private static RangeReference Inventory = new RangeReference(Tabs.T11, 161, 10, SqlDbType.VarChar);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_FeedSelModelPenalty]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return "PenaltyID";
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Penalty_YN", Penalty.Penalty_YN);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("FeedSlateChange", Penalty.FeedSlateChange);
								d.Add("Reliability", Penalty.Reliability);
								d.Add("Envrionment", Penalty.Envrionment);
								d.Add("FurnRunLength", Penalty.FurnRunLength);
								d.Add("Maintenance", Penalty.Maintenance);
								d.Add("Inventory", Penalty.Inventory);

								return d;
							}
						}
					}
				}
			}
		}
	}
}