﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal partial class Practices
			{
				private static RangeReference Mix_Y = new RangeReference(Tabs.T11, 51, 0, SqlDbType.VarChar);
				private static RangeReference Blend_Y = new RangeReference(Tabs.T11, 53, 0, SqlDbType.VarChar);
				private static RangeReference Fractionation_Y = new RangeReference(Tabs.T11, 54, 0, SqlDbType.VarChar);
				private static RangeReference HydroTreat_Y = new RangeReference(Tabs.T11, 55, 0, SqlDbType.VarChar);
				private static RangeReference Purification_Y = new RangeReference(Tabs.T11, 57, 0, SqlDbType.VarChar);

				private static RangeReference MinConsidDensity_SG = new RangeReference(Tabs.T11, 62, 0, SqlDbType.Float);
				private static RangeReference MaxConsidDensity_SG = new RangeReference(Tabs.T11, 63, 0, SqlDbType.Float);
				private static RangeReference MinActualDensity_SG = new RangeReference(Tabs.T11, 64, 0, SqlDbType.Float);
				private static RangeReference MaxActualDensity_SG = new RangeReference(Tabs.T11, 65, 0, SqlDbType.Float);

				private static RangeReference LPG = new RangeReference(Tabs.T11, 0, 8);
				private static RangeReference Condensate = new RangeReference(Tabs.T11, 0, 9);
				private static RangeReference Naphtha = new RangeReference(Tabs.T11, 0, 10);
				private static RangeReference LiqHeavy = new RangeReference(Tabs.T11, 0, 11);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_FeedSelPractices]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Mix_Y", Practices.Mix_Y);
							d.Add("Blend_Y", Practices.Blend_Y);
							d.Add("Fractionation_Y", Practices.Fractionation_Y);
							d.Add("HydroTreat_Y", Practices.HydroTreat_Y);
							d.Add("Purification_Y", Practices.Purification_Y);

							d.Add("MinConsidDensity_SG", Practices.MinConsidDensity_SG);
							d.Add("MaxConsidDensity_SG", Practices.MaxConsidDensity_SG);
							d.Add("MinActualDensity_SG", Practices.MinActualDensity_SG);
							d.Add("MaxActualDensity_SG", Practices.MaxActualDensity_SG);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("LPG", Practices.LPG);
							d.Add("Condensate", Practices.Condensate);
							d.Add("Naphtha", Practices.Naphtha);
							d.Add("LiqHeavy", Practices.LiqHeavy);

							return d;
						}
					}
				}
			}
		}
	}
}