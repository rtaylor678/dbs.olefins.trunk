﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Maintenance
		{
			internal class OnSite
			{
				private static RangeReference MaintMaterial_Cur = new RangeReference(Tabs.T10_01, 0, 5, SqlDbType.Float);
				private static RangeReference MaintLabor_Cur = new RangeReference(Tabs.T10_01, 0, 6, SqlDbType.Float);
				private static RangeReference TaMaterial_Cur = new RangeReference(Tabs.T10_01, 0, 7, SqlDbType.Float);
				private static RangeReference TaLabor_Cur = new RangeReference(Tabs.T10_01, 0, 8, SqlDbType.Float);

				private static RangeReference PyroFurnMajOth = new RangeReference(Tabs.T10_01, 33, 0);
				private static RangeReference PyroFurnMinOth = new RangeReference(Tabs.T10_01, 34, 0);
				private static RangeReference PyroFurnTLE = new RangeReference(Tabs.T10_01, 35, 0);
				private static RangeReference CompGas = new RangeReference(Tabs.T10_01, 36, 0);
				private static RangeReference Pump = new RangeReference(Tabs.T10_01, 37, 0);
				private static RangeReference Exchanger = new RangeReference(Tabs.T10_01, 38, 0);
				private static RangeReference TowerCool = new RangeReference(Tabs.T10_01, 39, 0);
				private static RangeReference BoilFiredSteam = new RangeReference(Tabs.T10_01, 40, 0);
				private static RangeReference Vessel = new RangeReference(Tabs.T10_01, 41, 0);
				private static RangeReference PipeOnSite = new RangeReference(Tabs.T10_01, 42, 0);
				private static RangeReference InstrAna = new RangeReference(Tabs.T10_01, 43, 0);
				private static RangeReference PCComputers = new RangeReference(Tabs.T10_01, 44, 0);
				private static RangeReference ElecGen = new RangeReference(Tabs.T10_01, 45, 0);
				private static RangeReference OtherOnSite = new RangeReference(Tabs.T10_01, 47, 0);

				private static RangeReference Store = new RangeReference(Tabs.T10_01, 49, 0);
				private static RangeReference Flare = new RangeReference(Tabs.T10_01, 50, 0);
				private static RangeReference Enviro = new RangeReference(Tabs.T10_01, 51, 0);
				private static RangeReference OtherOffSite = new RangeReference(Tabs.T10_01, 52, 0);
				private static RangeReference TurnAroundServices = new RangeReference(Tabs.T10_01, 54, 0);

				private static RangeReference OverHead = new RangeReference(Tabs.T10_01, 56, 0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_Maint]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "FacilityId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("MaintMaterial_Cur", OnSite.MaintMaterial_Cur);
							d.Add("MaintLabor_Cur", OnSite.MaintLabor_Cur);
							d.Add("TaMaterial_Cur", OnSite.TaMaterial_Cur);
							d.Add("TaLabor_Cur", OnSite.TaLabor_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("PyroFurnMajOth", OnSite.PyroFurnMajOth);
							d.Add("PyroFurnMinOth", OnSite.PyroFurnMinOth);
							d.Add("PyroFurnTLE", OnSite.PyroFurnTLE);
							d.Add("CompGas", OnSite.CompGas);
							d.Add("Pump", OnSite.Pump);
							d.Add("Exchanger", OnSite.Exchanger);
							d.Add("TowerCool", OnSite.TowerCool);
							d.Add("BoilFiredSteam", OnSite.BoilFiredSteam);
							d.Add("Vessel", OnSite.Vessel);
							d.Add("PipeOnSite", OnSite.PipeOnSite);
							d.Add("InstrAna", OnSite.InstrAna);
							d.Add("PCComputers", OnSite.PCComputers);
							d.Add("ElecGen", OnSite.ElecGen);
							d.Add("OtherOnSite", OnSite.OtherOnSite);

							d.Add("Store", OnSite.Store);
							d.Add("Flare", OnSite.Flare);
							d.Add("Enviro", OnSite.Enviro);
							d.Add("OtherOffSite", OnSite.OtherOffSite);
							d.Add("TurnAroundServices", OnSite.TurnAroundServices);

							d.Add("OverHead", OnSite.OverHead);

							return d;
						}
					}
				}
			}
		}
	}
}