﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Prod
			{
				internal class Quantity
				{
					private static RangeReference Q1_kMT = new RangeReference(string.Empty, 0, 3, SqlDbType.Float);
					private static RangeReference Q2_kMT = new RangeReference(string.Empty, 0, 4, SqlDbType.Float);
					private static RangeReference Q3_kMT = new RangeReference(string.Empty, 0, 5, SqlDbType.Float);
					private static RangeReference Q4_kMT = new RangeReference(string.Empty, 0, 6, SqlDbType.Float);

					private static RangeReference StreamDesc = new RangeReference(string.Empty, 0, 2, SqlDbType.VarChar);
					private static RangeReference Recovered_WtPcnt = new RangeReference(string.Empty, 0, 8, SqlDbType.Float, 100.0);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_StreamsProdQuantity]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return new Streams().LookUpColumn;
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Q1_kMT", Quantity.Q1_kMT);
								d.Add("Q2_kMT", Quantity.Q2_kMT);
								d.Add("Q3_kMT", Quantity.Q3_kMT);
								d.Add("Q4_kMT", Quantity.Q4_kMT);
								d.Add("StreamDesc", Quantity.StreamDesc);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Hydrogen", Streams.Hydrogen);
								d.Add("Methane", Streams.Methane);
								d.Add("Acetylene", Streams.Acetylene);
								d.Add("EthylenePG", Streams.EthylenePG);
								d.Add("EthyleneCG", Streams.EthyleneCG);
								d.Add("PropylenePG", Streams.PropylenePG);
								d.Add("PropyleneCG", Streams.PropyleneCG);
								d.Add("PropyleneRG", Streams.PropyleneRG);
								d.Add("PropaneC3Resid", Streams.PropaneC3Resid);

								d.Add("Butadiene", Streams.Butadiene);
								d.Add("IsoButylene", Streams.IsoButylene);
								d.Add("Isobutylene", Streams.Isobutylene);
								d.Add("C4Oth", Streams.C4Oth);

								d.Add("Benzene", Streams.Benzene);
								d.Add("PyroGasoline", Streams.PyroGasoline);

								d.Add("PyroGasOil", Streams.PyroGasOil);
								d.Add("PyroFuelOil", Streams.PyroFuelOil);
								d.Add("AcidGas", Streams.AcidGas);

								d.Add("PPFC", Streams.PPFC);

								d.Add("ProdOther1", Streams.ProdOther1);
								d.Add("ProdOther2", Streams.ProdOther2);

								d.Add("LossFlareVent", Streams.LossFlareVent);
								d.Add("LossOther", Streams.LossOther);
								d.Add("LossMeasure", Streams.LossMeasure);

								return d;
							}
						}
					}
				}
			}
		}
	}
}