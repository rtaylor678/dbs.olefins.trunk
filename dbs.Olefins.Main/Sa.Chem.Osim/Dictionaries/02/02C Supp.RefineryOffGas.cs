﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Supp
			{
				internal class RefineryOffGas
				{
					private static RangeReference CompGasInlet_X = new RangeReference(Tabs.T02_C, 53, 3, SqlDbType.VarChar);
					private static RangeReference CompGasDischarge_X = new RangeReference(Tabs.T02_C, 54, 3, SqlDbType.VarChar);
					private static RangeReference C2Recovery_X = new RangeReference(Tabs.T02_C, 55, 3, SqlDbType.VarChar);
					private static RangeReference C3Recovery_X = new RangeReference(Tabs.T02_C, 56, 3, SqlDbType.VarChar);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_ROGEntryPoint]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return string.Empty;
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("CompGasInlet_X", RefineryOffGas.CompGasInlet_X);
								d.Add("CompGasDischarge_X", RefineryOffGas.CompGasDischarge_X);
								d.Add("C2Recovery_X", RefineryOffGas.C2Recovery_X);
								d.Add("C3Recovery_X", RefineryOffGas.C3Recovery_X);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								return new Dictionary<string, RangeReference>();
							}
						}
					}
				}
			}
		}
	}
}