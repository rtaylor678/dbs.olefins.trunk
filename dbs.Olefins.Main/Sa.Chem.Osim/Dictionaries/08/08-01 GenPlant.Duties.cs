﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class Duties
			{
				private static RangeReference Naphtha = new RangeReference(Tabs.T08_01, 39, 0);
				private static RangeReference LiqHeavy = new RangeReference(Tabs.T08_01, 40, 0);
				private static RangeReference FeedOther = new RangeReference(Tabs.T08_01, 41, 0);
				private static RangeReference EthylenePG = new RangeReference(Tabs.T08_01, 42, 0);
				private static RangeReference PropylenePG = new RangeReference(Tabs.T08_01, 43, 0);
				private static RangeReference Butadiene = new RangeReference(Tabs.T08_01, 44, 0);
				private static RangeReference Benzene = new RangeReference(Tabs.T08_01, 45, 0);

				private static RangeReference Amount_Cur = new RangeReference(Tabs.T08_01, 0, 8, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantDuties]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Amount_Cur", Duties.Amount_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Naphtha", Duties.Naphtha);
							d.Add("LiqHeavy", Duties.LiqHeavy);
							d.Add("FeedOther", Duties.FeedOther);
							d.Add("EthylenePG", Duties.EthylenePG);
							d.Add("PropylenePG", Duties.PropylenePG);
							d.Add("Butadiene", Duties.Butadiene);
							d.Add("Benzene", Duties.Benzene);

							return d;
						}
					}
				}
			}
		}
	}
}