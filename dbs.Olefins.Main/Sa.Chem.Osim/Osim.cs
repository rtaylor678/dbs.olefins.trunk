﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem
{
	public partial class Osim
	{
		private const int maxUpload = 10;
		private const int maxDownload = 3;

		public static void Create(string pathOsimTemplate, List<RefnumPair> refnums, bool PopulateAll)
		{
			Parallel.ForEach(refnums,
				new ParallelOptions { MaxDegreeOfParallelism = 3 },
				rp => { Sa.Chem.Osim.Create(pathOsimTemplate, rp, PopulateAll); }
				);
		}

		public static void Create(string pathOsimTemplate, RefnumPair rp, bool PopulateAll)
		{
			Excel.Application xla = XL.NewExcelApplication(false);
			Excel.Workbook wkb = XL.OpenWorkbook_ReadOnly(xla, pathOsimTemplate);

			Excel.Worksheet wks = wkb.Worksheets["PlantName"];
			wks.Range["D45"].Value = System.IO.File.GetLastWriteTime(pathOsimTemplate).ToString();

			if (string.IsNullOrEmpty(rp.refnumHistory))
			{
				Sa.Chem.Osim.PopulateWorkbook(wkb, rp.refnumCurrent);
			}
			else
			{
				Sa.Chem.Osim.PopulateWorkbook(wkb, rp, PopulateAll);
			}

			//string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Temp\\";
			//string name = rp.refnumCurrent + ((string.IsNullOrEmpty(rp.refnumHistory)) ? string.Empty : (" (" + rp.refnumHistory + ")")) + " " + Common.GetDateTimeStamp() + ".xlsm";

			SiteInfo siteInfo = GetSiteInfo(rp.refnumCurrent);
			string path = @"\\Dallas2\data\Data\STUDY\Olefins\" + siteInfo.studyYear.ToString() + @"\Plants\" + siteInfo.smallRefnum + @"\";
			string name = "OSIM" + siteInfo.studyYear.ToString() + " " + siteInfo.coLoc + ".xlsm";

			System.IO.Directory.CreateDirectory(path);

			xla.Calculation = Excel.XlCalculation.xlCalculationAutomatic;

			if (System.IO.File.Exists(path+name))
			{
				System.IO.File.Delete(path + name);
			}

			wkb.SaveAs(path + name);

			XL.CloseExcel(ref xla, ref wkb);

			wkb = null;
			xla = null;
		}

		private static void PopulateWorkbook(Excel.Workbook wkb, string refnumCurrent)
		{
			using (SqlConnection cn = new SqlConnection(Common.cnString()))
			{
				cn.Open();

				Excel.Application xla = wkb.Parent;

				TransferData.Download(cn, refnumCurrent, wkb, string.Empty, new Osim.PlantName.Download(), false);

				xla.CalculateFullRebuild();

				double forexRateCurrent = XL.GetForex(wkb.Worksheets[Osim.Tabs.T04_Current].Cells[5, 6]);

				PopulateAllTabs(cn, Tabs.Current, wkb, refnumCurrent, forexRateCurrent, false);

				xla.CalculateFullRebuild();

				cn.Close();
			}
		}

		private static void PopulateWorkbook(Excel.Workbook wkb, RefnumPair rp, bool PopulateAll = false)
		{
			using (SqlConnection cnPartial = new SqlConnection(Common.cnString()))
			{
				using (SqlConnection cnAllTabs = new SqlConnection(Common.cnString()))
				{
					cnPartial.Open();
					cnAllTabs.Open();

					Excel.Application xla = wkb.Parent;

					TransferData.Download(cnPartial, rp.refnumCurrent, wkb, string.Empty, new Osim.PlantName.Download(), false);

					xla.CalculateFullRebuild();

					double forexRateCurrent = XL.GetForex(wkb.Worksheets[Osim.Tabs.T04_Current].Cells[5, 6]);
					double forexRateHistory = XL.GetForex(wkb.Worksheets[Osim.Tabs.T04_History].Cells[5, 6]);

					Parallel.Invoke
					(
						new ParallelOptions { MaxDegreeOfParallelism = 2 },
						() =>
							{
								if (PopulateAll)
								{
									PopulateAllTabs(cnPartial, Tabs.Current, wkb, rp.refnumHistory, forexRateHistory, true);
								}
								else
								{
									PopulatePartial(cnPartial, Tabs.Current, wkb, rp.refnumHistory, forexRateHistory, false);
								}
							},
						() => { PopulateAllTabs(cnAllTabs, Tabs.History, wkb, rp.refnumHistory, forexRateHistory, true); }
					);

					xla.CalculateFullRebuild();

					string MacroName = string.Empty;

					MacroName = "appData_Entry.CheckEntry_Workbook";
					wkb.Application.Run("'" + wkb.Name + "'!" + MacroName, false);
					
					MacroName = "xlWorkbook.PrepareWorkbookForClient";
					wkb.Application.Run("'" + wkb.Name + "'!" + MacroName);

					cnPartial.Close();
					cnAllTabs.Close();
				}
			}
		}

		private static void PopulateAllTabs(SqlConnection cn, string tabNamePrefix, Excel.Workbook wkb, string refnum, double forexRate, bool forceIn)
		{
			Parallel.Invoke
			(
				new ParallelOptions { MaxDegreeOfParallelism = maxDownload },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Capacity.Aggregate.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Capacity.Attributes.Download(), forceIn); },
				() => { new Osim.Capacity.Growth().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn, true); },
				() => { new Osim.Facilities().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Light.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Liquid.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Feed.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Composition.Other.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Quantity.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Recycled.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.RefineryOffGas.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Value.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Composition.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Composition.Other.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Ppfc.Composition.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Quantity.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Value.Download(), forceIn); },

				() => { new Osim.Streams.Prod.Ppfc.LHValue().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.OpEx.Download(), forexRate, forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Pers.General.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Pers.Absence.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Attributes.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.LostProduction.History(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.LostProduction.Current(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Availability.History(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Availability.Current(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.CriticalPath.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Train.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.RunLength.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Furnace.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.FurnaceLimited.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.CompressorEvents.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Turbomachinery.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.PredictiveMaint.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Cogen.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Cogen.Facilities.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Cogen.EnergyPercent.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Composition.Download(), forceIn); },

				() => { new Osim.Energy.Electric().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },
				() => { new Osim.Energy.Steam().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.General.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Inventory.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Duties.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Integration.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Logistics.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.EnergyEfficiency.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.FeedFlex.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.CapEx.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.CapAdded.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Environment.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Energy.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.General.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.Controllers.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.Existence.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.OnLinePcnt.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.OnSite.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.Accuracy.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.Routine.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.OpEx.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.Pers.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.General.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Logistics.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Model.Penalty.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Model.Plan.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Models.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.NonDiscretionary.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Personnel.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Practices.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.Facilities.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.OpEx.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.Quantity.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.QuantityDescription.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.CapEx.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.Energy.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.Misc.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.OpEx.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.Quantity.Download(), forceIn); }
			);
		}

		private static void PopulatePartial(SqlConnection cn, string tabNamePrefix, Excel.Workbook wkb, string refnum, double forexRate, bool forceIn)
		{
			Parallel.Invoke
			(
				new ParallelOptions { MaxDegreeOfParallelism = maxDownload },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Capacity.Aggregate.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Capacity.Attributes.Download(), forceIn); },
				() => { new Osim.Capacity.Growth().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn, false); },
				() => { new Osim.Facilities().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Light.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Liquid.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Feed.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Composition.Other.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Quantity.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Recycled.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.RefineryOffGas.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Supp.Value.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Composition.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Composition.Other.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Ppfc.Composition.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Quantity.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Streams.Prod.Value.Download(), forceIn); },

				//() => { new Osim.Streams.Prod.Ppfc.LHValue().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.OpEx.Download(), forexRate, forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Pers.General.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Pers.Absence.Download(), forceIn); },

				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Attributes.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.LostProduction.History(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.LostProduction.Current(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Availability.History(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Availability.Current(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.CriticalPath.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Train.Download(), forceIn); },
				() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.RunLength.Download(), forceIn); }
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Furnace.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.FurnaceLimited.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.CompressorEvents.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.Turbomachinery.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Reliability.PredictiveMaint.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Cogen.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Cogen.Facilities.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Cogen.EnergyPercent.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Energy.Composition.Download(), forceIn); },

				//() => { new Osim.Energy.Electric().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },
				//() => { new Osim.Energy.Steam().CustomLoad(cn, wkb, tabNamePrefix, refnum, forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.General.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Inventory.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Duties.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Integration.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Logistics.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.EnergyEfficiency.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.FeedFlex.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.CapEx.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.CapAdded.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Environment.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.GenPlant.Energy.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.General.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.Controllers.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.Existence.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Apc.OnLinePcnt.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.OnSite.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.Accuracy.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.Routine.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.OpEx.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Maintenance.Pers.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.General.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Logistics.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Model.Penalty.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Model.Plan.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Models.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.NonDiscretionary.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Personnel.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.FeedSelection.Practices.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.Facilities.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.OpEx.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.Quantity.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Polymer.QuantityDescription.Download(), forceIn); },

				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.CapEx.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.Energy.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.Misc.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.OpEx.Download(), forceIn); },
				//() => { TransferData.Download(cn, refnum, wkb, tabNamePrefix, new Osim.Metathesis.Quantity.Download(), forceIn); }
			);
		}

		public static void Upload(Excel.Workbook wkb, string refnum)
		{
			using (SqlConnection cn = new SqlConnection(Common.cnString()))
			{
				cn.Open();

				TransferData.Delete(cn, "[stgFact].[Delete_OlefinsData]", refnum);


				Parallel.Invoke
				(
					new ParallelOptions { MaxDegreeOfParallelism = maxUpload },

					//	Plant Name
					() => { TransferData.Upload(cn, refnum, wkb, string.Empty, new Osim.PlantName.Upload()); },

					//	Table 1-1
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Capacity.Upload()); },

					//	Table 1-2
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Capacity.Growth.Current.Upload()); },
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Capacity.Growth.Future.Upload()); },

					//	Table 1-3
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Facilities.Upload()); },
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Facilities.CoolingWater.Upload()); },

					//	Table 2A-1, 2A-2, 2B
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Streams.Feed.Upload()); },

					//	Table 2A-1, Table 2B
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Streams.Light.Upload()); },

					//	Table 2A-2
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Streams.Liquid.Upload()); },

					//	Table 2C
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Streams.Supp.Quantity.Upload()); },
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Streams.Supp.Composition.Other.Upload()); },

					//	Table 2C (Recycle and ROG), 5A, 5B
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Misc.Upload()); },

					//	Table 3


					//	Table 4
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.OpEx.Upload()); },

					//	Table 6-1
					() => { TransferData.Upload(cn, refnum, wkb, Tabs.Current, new Osim.Reliability.LostProduction.Upload()); }

				);

				cn.Close();
			}
		}

		private static SiteInfo GetSiteInfo(string refnum)
		{
			SiteInfo siteInfo = new SiteInfo();

			using (SqlConnection cn = new SqlConnection(Common.cnString()))
			{
				using (SqlCommand cmd = new SqlCommand("[fact].[Select_PlantName]", cn))
				{
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

					cn.Open();

					using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
					{
						RangeReference r = new RangeReference();

						while (rdr.Read())
						{
							string smallRefnum = rdr.GetString(rdr.GetOrdinal("SmallRefnum"));
							string coLoc = rdr.GetString(rdr.GetOrdinal("CoLoc"));
							int studyYear = rdr.GetInt16(rdr.GetOrdinal("StudyYear"));

							siteInfo = new SiteInfo(refnum, smallRefnum, coLoc, studyYear);
						}
					}
					cn.Close();
				}
			}
			return siteInfo;
		}
	}
}