﻿CREATE TABLE [xls].[FeedClass] (
    [Refnum]         VARCHAR (12)       NOT NULL,
    [FeedClassId]    INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedClass_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FeedClass_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FeedClass_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FeedClass_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedClass] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_FeedClass_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [FK_FeedClass_FeedClassId] FOREIGN KEY ([FeedClassId]) REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])
);


GO

CREATE TRIGGER [xls].[t_FeedClass_u]
	ON [xls].[FeedClass]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FeedClass]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FeedClass].[Refnum]		= INSERTED.[Refnum];

END;