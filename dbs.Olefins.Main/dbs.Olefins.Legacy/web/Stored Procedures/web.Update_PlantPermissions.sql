﻿CREATE PROCEDURE [web].[Update_PlantPermissions]
(
	@PlantPermissionId		INT	= NULL,
	@PlantId				INT	= NULL,
	@UserId					INT	= NULL,
	@Active					BIT	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LoginId	INT = @UserId;
	DECLARE @JoinId		INT = @PlantPermissionId;

	IF EXISTS (SELECT TOP 1 1 FROM [auth].[JoinPlantSubmission] jps WHERE jps.[JoinId] = @JoinId)
	BEGIN
		EXECUTE [auth].[Insert_JoinPlantLogin] @PlantId, @LoginId;
	END
	ELSE
	BEGIN
		EXECUTE [auth].[Update_JoinPlantLogin] @JoinId, @PlantId, @LoginId, @Active;
	END;

	SELECT @JoinId;
	RETURN @JoinId;

END;