﻿CREATE TABLE [audit].[Log_Fact] (
    [LogChanges_Bit]    BIT                CONSTRAINT [DF_Log_Fact_LogChanges] DEFAULT ((1)) NOT NULL,
    [TruncateData_Days] INT                CONSTRAINT [DF_Log_Fact_TruncateData] DEFAULT ((-1)) NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Log_Fact_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_Log_Fact_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_Log_Fact_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_Log_Fact_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Log_Fact] PRIMARY KEY CLUSTERED ([LogChanges_Bit] DESC)
);


GO

CREATE TRIGGER [audit].[Log_Fact_LogChanges]
ON [audit].[Log_Fact]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [audit].[Log_Fact]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE [audit].[Log_Fact].[LogChanges_Bit]	= INSERTED.[LogChanges_Bit];

END;
