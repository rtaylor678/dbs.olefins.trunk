﻿CREATE PROCEDURE [auth].[Update_JoinPlantSubmission]
(
	@JoinId			INT	= NULL,
	@PlantId		INT	= NULL,
	@SubmissionId	INT	= NULL,

	@Active			BIT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	UPDATE	[auth].[JoinPlantSubmission]
	SET		[Active]	= @Active
	OUTPUT	[INSERTED].[JoinId] INTO @Id([Id])
	WHERE	([JoinId]	= @JoinId OR ([PlantId]	= @PlantId	AND	[SubmissionId]	= @SubmissionId))
		AND(@Active		IS NOT NULL)
		AND(@Active		<> COALESCE(@Active, [Active]));

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
			  COALESCE(', @JoinId:'			+ CONVERT(VARCHAR, @JoinId),		'')
			+ COALESCE(', @PlantId:'		+ CONVERT(VARCHAR, @PlantId),		'')
			+ COALESCE(', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId),	'')
			+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),		'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;