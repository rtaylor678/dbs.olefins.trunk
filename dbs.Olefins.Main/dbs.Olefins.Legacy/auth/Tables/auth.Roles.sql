﻿CREATE TABLE [auth].[Roles] (
    [RoleId]         INT                IDENTITY (1, 1) NOT NULL,
    [RoleTag]        VARCHAR (42)       NOT NULL,
    [RoleName]       VARCHAR (84)       NOT NULL,
    [RoleDetail]     VARCHAR (256)      NOT NULL,
    [RoleLevel]      INT                NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_Roles_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Roles_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Roles_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Roles_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Roles_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleId] ASC),
    CONSTRAINT [CL_Roles_RoleDetail] CHECK ([RoleDetail]<>''),
    CONSTRAINT [CL_Roles_RoleName] CHECK ([RoleName]<>''),
    CONSTRAINT [CL_Roles_RoleTag] CHECK ([RoleTag]<>''),
    CONSTRAINT [CR_Roles_RoleLevel] CHECK ([RoleLevel]>=(0)),
    CONSTRAINT [UK_Roles_RoleDetail] UNIQUE NONCLUSTERED ([RoleDetail] ASC),
    CONSTRAINT [UK_Roles_RoleName] UNIQUE NONCLUSTERED ([RoleName] ASC),
    CONSTRAINT [UK_Roles_RoleTag] UNIQUE NONCLUSTERED ([RoleTag] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Roles_Active]
    ON [auth].[Roles]([RoleId] ASC)
    INCLUDE([RoleTag], [RoleName], [RoleDetail], [RoleLevel]) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_Roles_u]
	ON [auth].[Roles]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[Roles]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[Roles].[RoleId]	= INSERTED.[RoleId];

END;