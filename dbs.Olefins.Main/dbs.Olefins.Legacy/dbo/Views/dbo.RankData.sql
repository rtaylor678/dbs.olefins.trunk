﻿












CREATE     view [dbo].[RankData] as
Select c.Refnum, t.CoLoc, t.StudyYear,
UnadjC2Util,
TAAdjC2Util,
TAAdjC2C3Util,
UnAdjC2C3Util,
ePYPS.BTULbHVChem,
ePYPS.BTUuEDC,
HVChemTEPmt,
EDCTotWHr,
HVChemTotWHrmt,
PersCostTA_MTHVC,
PersCostTA_kEDC,
MaintCostIndex,
MaintCostEDC,
NewMaintEffInd,
NewMaintEffEDC,
ReliabInd,
yPYPS.PyroC2YPcnt,
yPYPS.PyroHVCYPcnt,
yPYPS.YIRTPSC2,
yPYPS.YIRTPSHVC,
yPYPS.YIRTHVC,
yPYPS.YIRTC2,
yPYPS.SIC2,
yPYPS.PValPcntOS,
PValPcntMSPYPS = yPYPS.PValPcntMS,
PValPcntMSSPSL = ySPSL.PValPcntMS,
PValPcntOSPPYPS = yPYPS.PValPcntOSP,
PValPcntOSPSPSL = ySPSL.PValPcntOSP,
HVChemCash,
UEDCCash,
EDCNEOpex,
TotHVChemExp,
MfgC2,
MfgC2C3,
MfgHVC,
MargnNetC2,
MargnNetC2C3,
MargnNetHVC,
GrossROI = Gross,
NetROI = Net,
TAAdjNet,
HVChemNEOpex,
yPYPS.Loss_PctTotalFreshFeed,
TotalLoss_PctFreshFeed = CASE WHEN qTotProd.AnnFeedProd = 0 then 0 ELSE qSTLoss.AnnFeedProd/qTotProd.AnnFeedProd*100 end,
HVChemDiv, EthylCapKMTA, TotReplVal,
NewEEIHVChemPYPS = ePYPS.NewEEIHVChem,
NewEEIHVChemSPSL = eSPSL.NewEEIHVChem ,
EII,
kEDC,
kUEDC, 
StdEnergy
From CapUtil c
INNER JOIN TSort t on t.Refnum=c.Refnum
INNER JOIN Production p on p.Refnum=t.Refnum
INNER JOIN Capacity cap on cap.Refnum=t.Refnum
 
INNER JOIN EEI ePYPS on ePYPS.Refnum=t.Refnum AND ePYPS.Model = 'PYPS'
LEFT outer JOIN EEI eSPSL on eSPSL.Refnum=t.Refnum AND eSPSL.Model = 'SPSL'
 
INNER JOIN EII on EII.Refnum=t.Refnum
INNER JOIN Productivity pd on pd.Refnum=t.Refnum
INNER JOIN PersCost pc on pc.Refnum=t.Refnum
INNER JOIN MaintInd m on m.Refnum=t.Refnum
 
INNER JOIN Yield yPYPS on yPYPS.Refnum=t.Refnum AND yPYPS.Model = 'PYPS'
left outer JOIN Yield ySPSL on ySPSL.Refnum=t.Refnum AND ySPSL.Model = 'SPSL'
 
INNER JOIN UnitOpex u on u.Refnum=t.Refnum
INNER JOIN ROI r on r.Refnum=t.Refnum
INNER JOIN Replacement rep on rep.Refnum=t.Refnum
INNER JOIN QuantitySTCalc qSTLoss on qSTLoss.Refnum=t.Refnum AND qSTLoss.Category = 'Loss'
INNER JOIN QuantityTotCalc qTotProd on qTotProd.Refnum=t.Refnum AND qTotProd.[Type] = 'Prod'
INNER JOIN EDC ON EDC.Refnum = t.Refnum


 
 







