﻿CREATE View ClientProdCost as
Select	p.Refnum,
	TotFDCost, 
	TotCashOPEX = TotCash/1000, TotProdRev, 
	EthProdCost = CASE WHEN (TotFDCost+TotProdRev) = 0 THEN NULL ELSE TotFDCost + TotCash/1000 -TotProdRev END,
	EthProdCostMT = CASE WHEN (TotFDCost+TotProdRev) = 0 THEN NULL ELSE (TotFDCost + TotCash/1000 -TotProdRev)*1000000/(C2Div*1e3) END,
	C2Div
from Prac p JOIN FinancialTot f on f.Refnum=p.Refnum
JOIN Production d on d.Refnum=p.Refnum
