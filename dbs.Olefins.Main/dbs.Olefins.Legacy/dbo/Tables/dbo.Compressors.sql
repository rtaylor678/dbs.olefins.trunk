﻿CREATE TABLE [dbo].[Compressors] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [EventNo]    TINYINT        NOT NULL,
    [Compressor] VARCHAR (5)    NULL,
    [EventType]  CHAR (8)       NULL,
    [Duration]   REAL           NULL,
    [Component]  CHAR (5)       NULL,
    [Cause]      TINYINT        NULL,
    [EventYear]  INT            NULL,
    [Comments]   VARCHAR (MAX)  NULL,
    [CompLife]   TINYINT        NULL,
    CONSTRAINT [PK_Compressors_1__10] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EventNo] ASC)
);

