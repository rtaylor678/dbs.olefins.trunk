﻿CREATE TABLE [dbo].[Maint] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [ProjectID]      VARCHAR (15)   NOT NULL,
    [RoutMaintMatl]  REAL           NULL,
    [RoutMaintLabor] REAL           NULL,
    [RoutMaintTot]   REAL           NULL,
    [TAMatl]         REAL           NULL,
    [TALabor]        REAL           NULL,
    [TATot]          REAL           NULL,
    CONSTRAINT [PK___18__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ProjectID] ASC)
);


GO

/****** Object:  Trigger dbo.UpdateMaintTot    Script Date: 4/18/2003 4:32:55 PM ******/

/****** Object:  Trigger dbo.UpdateMaintTot    Script Date: 12/28/2001 7:34:25 AM ******/
CREATE TRIGGER UpdateMaintTot ON dbo.Maint 
FOR INSERT,UPDATE
AS

UPDATE Maint
SET 	RoutMaintTot = ISNULL(i.RoutMaintMatl, 0) + ISNULL(i.RoutMaintLabor, 0),
	TATot = ISNULL(i.TAMatl, 0) + ISNULL(i.TALabor, 0)
FROM Maint INNER JOIN inserted i ON Maint.Refnum = i.Refnum AND Maint.ProjectID = i.ProjectID

