﻿CREATE TABLE [dbo].[Environ] (
    [Refnum] [dbo].[Refnum] NOT NULL,
    [Emis]   REAL           NULL,
    [Waste]  REAL           NULL,
    [NOx]    REAL           NULL,
    [Water]  REAL           NULL,
    CONSTRAINT [PK___9__14] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

