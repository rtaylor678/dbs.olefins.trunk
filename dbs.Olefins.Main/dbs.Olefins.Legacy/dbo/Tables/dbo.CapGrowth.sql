﻿CREATE TABLE [dbo].[CapGrowth] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [Year]        INT            NOT NULL,
    [EthylProdn]  REAL           NULL,
    [PropylProdn] REAL           NULL,
    [EthylCap]    REAL           NULL,
    CONSTRAINT [PK___2__19] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Year] ASC)
);

