﻿CREATE TABLE [dbo].[OutputCheckTableIDs] (
    [TableID]     INT               IDENTITY (1, 1) NOT NULL,
    [Study]       VARCHAR (5)       NOT NULL,
    [StudyYear]   [dbo].[StudyYear] NOT NULL,
    [OutputTab]   VARCHAR (25)      NOT NULL,
    [Workbook]    VARCHAR (400)     NOT NULL,
    [Worksheet]   VARCHAR (50)      NOT NULL,
    [RowIDColumn] SMALLINT          NOT NULL,
    [ColumnIDRow] SMALLINT          NOT NULL,
    CONSTRAINT [PK_OutputCheckTableIDs] PRIMARY KEY CLUSTERED ([Study] ASC, [StudyYear] ASC, [OutputTab] ASC),
    CONSTRAINT [IX_OutputCheckTableIDs] UNIQUE NONCLUSTERED ([TableID] ASC)
);

