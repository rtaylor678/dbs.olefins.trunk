﻿CREATE PROCEDURE [dbo].[Insert_Facilities]
(
	@SubmissionId					INT				= NULL,

	--	Submissions
	@SubmissionName					NVARCHAR(42)	= NULL,
	@DateBeg						DATE			= NULL,
	@DateEnd						DATE			= NULL,
	@SubmissionComment				NVARCHAR(MAX)	= NULL,

	--	Capacity
	@Ethylene_StreamDay_MTSD		FLOAT			= NULL,
	@Propylene_StreamDay_MTSD		FLOAT			= NULL,
	@Olefins_StreamDay_MTSD			FLOAT			= NULL,

	--	Facility Count
	@FracFeed_UnitCount				INT				= NULL,
	@BoilHP_UnitCount				INT				= NULL,
	@BoilLP_UnitCount				INT				= NULL,
	@ElecGen_UnitCount				INT				= NULL,
	@HydroPurCryogenic_UnitCount	INT				= NULL,
	@HydroPurPSA_UnitCount			INT				= NULL,
	@HydroPurMembrane_UnitCount		INT				= NULL,
	@TowerPyroGasHT_UnitCount		INT				= NULL,
	@Trains_UnitCount				INT				= NULL,
	
	--	Facility - Boilers
	@BoilHP_Rate_kLbHr				FLOAT			= NULL,
	@BoilLP_Rate_kLbHr				FLOAT			= NULL,
	
	--	Facility - Electricity Generation
	@Capacity_MW					FLOAT			= NULL,

	--	Facility - FracFeed
	@FracFeed_Quantity_kBSD			FLOAT			= NULL,
	@FracFeed_StreamId				INT				= NULL,
	@FracFeed_Throughput_kMT		FLOAT			= NULL,
	@FracFeed_Density_SG			FLOAT			= NULL,

	--	Facility - HydroTreater
	@HT_Quantity_kBSD				FLOAT			= NULL,
	@HT_HydroTreaterTypeId			INT				= NULL,
	@HT_Pressure_PSIg				FLOAT			= NULL,
	@HT_Processed_kMT				FLOAT			= NULL,
	@HT_Processed_Pcnt				FLOAT			= NULL,
	@HT_Density_SG					FLOAT			= NULL
)
AS
BEGIN

	DECLARE	@ErrorCount		INT;

	--	Submissions
	EXECUTE	@SubmissionId	= [stage].[Insert_Submission] @SubmissionName, @DateBeg, @DateEnd;
	EXECUTE	@ErrorCount		= [stage].[Insert_SubmissionComments] @SubmissionId, @SubmissionComment;

	--	Capacity
	IF(@Ethylene_StreamDay_MTSD > 0.0)
	BEGIN
		DECLARE	@EthyleneId		INT = [dim].[Return_StreamId]('Ethylene');
		EXECUTE	@ErrorCount		= [stage].[Insert_Capacity] @SubmissionId, @EthyleneId, NULL, @Ethylene_StreamDay_MTSD, NULL;
	END

	IF	(COALESCE(@Olefins_StreamDay_MTSD, 0.0) > @Ethylene_StreamDay_MTSD)
	AND (COALESCE(@Propylene_StreamDay_MTSD, 0.0) <> COALESCE(@Olefins_StreamDay_MTSD, 0.0) - @Ethylene_StreamDay_MTSD)
	BEGIN
		SET @Propylene_StreamDay_MTSD = @Olefins_StreamDay_MTSD - @Ethylene_StreamDay_MTSD;
	END;

	IF(@Propylene_StreamDay_MTSD >= 0.0)
	BEGIN
		DECLARE	@PropyleneId	INT = [dim].[Return_StreamId]('Propylene');
		EXECUTE	@ErrorCount		= [stage].[Insert_Capacity] @SubmissionId, @PropyleneId, NULL, @Propylene_StreamDay_MTSD, NULL;
	END;

	--	Facility
	DECLARE @FacilityId		INT;

	IF(@FracFeed_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('FracFeed');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @FracFeed_UnitCount;

		IF(@FracFeed_Quantity_kBSD >= 0.0 AND @FracFeed_StreamId >= 1 AND @FracFeed_Throughput_kMT >= 0.0 AND @FracFeed_Density_SG >= 0.0)
		BEGIN
			EXECUTE	@ErrorCount		= [stage].[Insert_FacilitiesFractionator] @SubmissionId, @FacilityId, @FracFeed_Quantity_kBSD, @FracFeed_StreamId, @FracFeed_Throughput_kMT, @FracFeed_Density_SG;
		END;

	END;

	IF(@BoilHP_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('BoilHP');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @BoilHP_UnitCount;

		IF(@BoilHP_Rate_kLbHr >= 0.0)
		BEGIN
			EXECUTE	@ErrorCount		= [stage].[Insert_FacilitiesBoilers] @SubmissionId, @FacilityId, @BoilHP_Rate_kLbHr, NULL;
		END;

	END;

	IF(@BoilLP_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('BoilLP');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @BoilLP_UnitCount;

		IF(@BoilLP_Rate_kLbHr >= 0.0)
		BEGIN
			EXECUTE	@ErrorCount		= [stage].[Insert_FacilitiesBoilers] @SubmissionId, @FacilityId, @BoilLP_Rate_kLbHr, NULL;
		END;

	END;

	IF(@ElecGen_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('ElecGen');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @ElecGen_UnitCount;

		IF(@Capacity_MW >= 0.0)
		BEGIN
			EXECUTE	@ErrorCount		= [stage].[Insert_FacilitiesElecGeneration] @SubmissionId, @FacilityId, @Capacity_MW;
		END;

	END;

	IF(@HydroPurCryogenic_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('HydroPurCryogenic');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @HydroPurCryogenic_UnitCount;
	END;

	IF(@HydroPurPSA_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('HydroPurPSA');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @HydroPurPSA_UnitCount;
	END;

	IF(@HydroPurMembrane_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('HydroPurMembrane');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @HydroPurMembrane_UnitCount;
	END;

	IF(@TowerPyroGasHT_UnitCount >= 0)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('TowerPyroGasHT');
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @TowerPyroGasHT_UnitCount;

		IF(@HT_Quantity_kBSD >= 0.0 AND @HT_HydroTreaterTypeId > 0 AND @HT_Processed_Pcnt >= 0.0 AND @HT_Processed_Pcnt <= 100.0 AND @HT_Density_SG >= 0.0
			AND (@HT_Pressure_PSIg >= 0.0 OR @HT_Pressure_PSIg IS NULL)
			AND (@HT_Processed_kMT >= 0.0 OR @HT_Processed_kMT IS NULL)
			)
		BEGIN
			EXECUTE	@ErrorCount		= [stage].[Insert_FacilitiesHydroTreater] @SubmissionId, @FacilityId, @HT_Quantity_kBSD, @HT_HydroTreaterTypeId, @HT_Pressure_PSIg, @HT_Processed_kMT, @HT_Processed_Pcnt, @HT_Density_SG;
		END;

	END;

	IF(@Trains_UnitCount >= 1)
	BEGIN
		SET @FacilityId = [dim].[Return_FacilityId]('TowerPyroGasHT');
		EXECUTE	@ErrorCount		= [stage].[Insert_FacilitiesTrains] @SubmissionId, @Trains_UnitCount;
	END;

	RETURN @SubmissionId;

END;
