﻿
/****** Object:  Stored Procedure dbo.procLoadDDObjects    Script Date: 4/18/2003 4:32:54 PM ******/

/****** Object:  Stored Procedure dbo.procLoadDDObjects    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.procLoadDDObjects    Script Date: 04/13/2000 8:32:49 AM ******/
CREATE PROCEDURE procLoadDDObjects AS
DELETE FROM DD_Relationships
WHERE NOT EXISTS (SELECT o.* FROM sysobjects o, DD_Objects d
	WHERE o.name = d.ObjectName
	AND (d.ObjectID = DD_Relationships.ObjectA OR d.ObjectID = DD_Relationships.ObjectB))
DELETE FROM DD_Columns
WHERE NOT EXISTS (SELECT o.* FROM sysobjects o, DD_Objects d
	WHERE o.name = d.ObjectName AND d.ObjectID = DD_Columns.ObjectID)
DELETE FROM DD_Objects
WHERE NOT EXISTS (SELECT * FROM sysobjects
	 WHERE name = DD_Objects.ObjectName)
INSERT INTO DD_Objects (ObjectName)
SELECT name FROM sysobjects
WHERE type in ('U', 'V')
 AND name NOT IN (SELECT ObjectName FROM DD_Objects)
