﻿CREATE PROCEDURE [Console].[GetConsoleSettings] 
	
	@StudyType nvarchar(20)
	
AS
BEGIN
	SELECT * FROM [Console].[Console] WHERE StudyType = @StudyType
END
