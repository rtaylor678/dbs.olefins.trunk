﻿CREATE TABLE [Console].[RefnumStudyCodes] (
    [StudyCode]     VARCHAR (3) NOT NULL,
    [Study]         VARCHAR (3) NOT NULL,
    [StudyCodeSize] AS          (len(rtrim([StudyCode]))),
    CONSTRAINT [PK_RefnumStudyCodes] PRIMARY KEY CLUSTERED ([StudyCode] ASC)
);

