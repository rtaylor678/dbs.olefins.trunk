﻿CREATE TABLE [ante].[NicenessRanges] (
    [MethodologyId]  INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [HistoricalMin]  FLOAT (53)         NOT NULL,
    [HistoricalMax]  FLOAT (53)         NOT NULL,
    [YieldMin]       FLOAT (53)         NOT NULL,
    [YieldMax]       FLOAT (53)         NOT NULL,
    [EnergyMin]      FLOAT (53)         NOT NULL,
    [EnergyMax]      FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessRanges_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_NicenessRanges_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_NicenessRanges_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_NicenessRanges_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_NicenessRanges] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC),
    CONSTRAINT [CR_NicenessRanges_EnergyMax_MinIncl_0.0] CHECK ([EnergyMax]>=(0.0)),
    CONSTRAINT [CR_NicenessRanges_EnergyMin_MinIncl_0.0] CHECK ([EnergyMin]>=(0.0)),
    CONSTRAINT [CR_NicenessRanges_HistoricalMax_MinIncl_0.0] CHECK ([HistoricalMax]>=(0.0)),
    CONSTRAINT [CR_NicenessRanges_HistoricalMin_MinIncl_0.0] CHECK ([HistoricalMin]>=(0.0)),
    CONSTRAINT [CR_NicenessRanges_YieldMax_MinIncl_0.0] CHECK ([YieldMax]>=(0.0)),
    CONSTRAINT [CR_NicenessRanges_YieldMin_MinIncl_0.0] CHECK ([YieldMin]>=(0.0)),
    CONSTRAINT [CV_NicenessRanges_Energy] CHECK ([EnergyMin]<=[EnergyMax]),
    CONSTRAINT [CV_NicenessRanges_Historical] CHECK ([HistoricalMin]<=[HistoricalMax]),
    CONSTRAINT [CV_NicenessRanges_Yield] CHECK ([YieldMin]<=[YieldMax]),
    CONSTRAINT [FK_NicenessRanges_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_NicenessRanges_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_NicenessRanges_u]
	ON [ante].[NicenessRanges]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessRanges]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[NicenessRanges].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[NicenessRanges].[StreamId]		= INSERTED.[StreamId];

END;