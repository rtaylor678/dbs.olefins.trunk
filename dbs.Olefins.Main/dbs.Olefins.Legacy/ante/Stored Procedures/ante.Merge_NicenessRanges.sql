﻿CREATE PROCEDURE [ante].[Merge_NicenessRanges]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[NicenessRanges] AS Target
	USING
	(
		VALUES
		(@MethodologyId, [dim].[Return_StreamId]('Condensate'), 0.924886048301329, 1.02939776622654, 0.95, 1.05, 0.99, 1.01),
		(@MethodologyId, [dim].[Return_StreamId]('HeavyNGL'), 0.924886048301329, 1.02939776622654, 0.95, 1.05, 0.99, 1.01),
		(@MethodologyId, [dim].[Return_StreamId]('NaphthaLt'), 0.945620391640057, 1.0424774710542, 0.95, 1.05, 0.99, 1.01),
		(@MethodologyId, [dim].[Return_StreamId]('Raffinate'), 0.945620391640057, 1.0424774710542, 0.95, 1.05, 0.99, 1.01),
		(@MethodologyId, [dim].[Return_StreamId]('NaphthaFr'), 0.957707923676346, 1.02884158170874, 0.95, 1.05, 0.99, 1.01),
		(@MethodologyId, [dim].[Return_StreamId]('NaphthaHv'), 0.857970999181025, 1.06728778453785, 0.95, 1.05, 0.99, 1.01),
		(@MethodologyId, [dim].[Return_StreamId]('Diesel'), 0.971597340962019, 1.06875708046953, 0.99, 1.01, 1, 1),
		(@MethodologyId, [dim].[Return_StreamId]('GasOilHv'), 0.964610330890017, 1.05379260871715, 0.99, 1.01, 1, 1),
		(@MethodologyId, [dim].[Return_StreamId]('GasOilHt'), 0.952412345400265, 1.0359841042563, 0.99, 1.01, 1, 1)
	)
	AS Source([MethodologyId], [StreamId], [HistoricalMin], [HistoricalMax], [YieldMin], [YieldMax], [EnergyMin], [EnergyMax])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[HistoricalMin]	= Source.[HistoricalMin],
		Target.[HistoricalMax]	= Source.[HistoricalMax],
		Target.[YieldMin]		= Source.[YieldMin],
		Target.[YieldMax]		= Source.[YieldMax],
		Target.[EnergyMin]		= Source.[EnergyMin],
		Target.[EnergyMax]		= Source.[EnergyMax]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [HistoricalMin], [HistoricalMax], [YieldMin], [YieldMax], [EnergyMin], [EnergyMax])
		VALUES([MethodologyId], [StreamId], [HistoricalMin], [HistoricalMax], [YieldMin], [YieldMax], [EnergyMin], [EnergyMax])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;