﻿CREATE PROCEDURE [ante].[Merge_StdEnergyFractionator]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[StdEnergyFractionator] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_StreamId('EPMix'),		60.0),
		(@MethodologyId, dim.Return_StreamId('LPG'),		50.0),
		(@MethodologyId, dim.Return_StreamId('Naphtha'),	55.0),
		(@MethodologyId, dim.Return_StreamId('Condensate'), 55.0),
		(@MethodologyId, dim.Return_StreamId('LiqHeavy'),	55.0)
	)
	AS Source([MethodologyId], [StreamId], [Energy_kBtuBbl])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[Energy_kBtuBbl]	= Source.[Energy_kBtuBbl]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [Energy_kBtuBbl])
		VALUES([MethodologyId], [StreamId], [Energy_kBtuBbl])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;