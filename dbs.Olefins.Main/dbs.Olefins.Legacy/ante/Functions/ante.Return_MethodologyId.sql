﻿CREATE FUNCTION [ante].[Return_MethodologyId]
(
	@MethodologyTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN;

	DECLARE @Id	INT;

	SELECT @Id = l.[MethodologyId]
	FROM [ante].[Methodology]	l
	WHERE l.[MethodologyTag] = @MethodologyTag;

	RETURN @Id;

END;