﻿CREATE FUNCTION [fact].[Get_Facilities]
(
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		f.[SubmissionId],
		f.[FacilityId],
		f.[Unit_Count]
	FROM [fact].[Facilities]			f
	WHERE	f.[SubmissionId] = @SubmissionId
);