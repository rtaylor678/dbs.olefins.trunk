﻿CREATE View [Val].[RefineryGrade] as
Select s.Refnum, s.CoLoc, s.Version, Score = SUM(Score)
, Grade = SUM(CASE WHEN Grade > 100 then 100 ELSE Grade END*WtFactor)/SUM(WtFactor)
, NumReds = Sum(NumReds)
, NumBlues = Sum(NumBlues)
, NumTeals = Sum(NumTeals)
, NumGreens = Sum(NumGreens)
, PercentRed = CASE WHEN Sum(NumReds+NumBlues+NumTeals+NumGreens) > 0 THEN Sum(NumReds)/Sum(NumReds+NumBlues+NumTeals+NumGreens) * 100 ELSE 0 END
, VersionTime = MIN(s.VersionTime)
From Val.SectionGrade s
Group by s.Refnum, s.CoLoc, s.Version
