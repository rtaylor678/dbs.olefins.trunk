﻿CREATE TABLE [Val].[Versions] (
    [Refnum]      [dbo].[RefListName] NOT NULL,
    [Version]     SMALLINT            NOT NULL,
    [VersionTime] DATETIME2 (7)       NOT NULL,
    CONSTRAINT [PK_Versions] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Version] ASC)
);

