﻿CREATE TABLE [calc].[HvcProductionDivisor] (
    [MethodologyId]      INT                NOT NULL,
    [SubmissionId]       INT                NOT NULL,
    [HvcProdDivisor_kMT] FLOAT (53)         NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_HvcProductionDivisor_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_HvcProductionDivisor_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_HvcProductionDivisor_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_HvcProductionDivisor_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_HvcProductionDivisor] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_HvcProductionDivisor_HvcProdDivisor_kMT_MinIncl_0.0] CHECK ([HvcProdDivisor_kMT]>=(0.0)),
    CONSTRAINT [FK_HvcProductionDivisor_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_HvcProductionDivisor_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_HvcProductionDivisor_u]
	ON [calc].[HvcProductionDivisor]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[HvcProductionDivisor]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[HvcProductionDivisor].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[HvcProductionDivisor].[SubmissionId]	= INSERTED.[SubmissionId];

END;