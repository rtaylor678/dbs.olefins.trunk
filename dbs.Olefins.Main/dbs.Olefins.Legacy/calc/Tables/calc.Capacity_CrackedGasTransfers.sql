﻿CREATE TABLE [calc].[Capacity_CrackedGasTransfers] (
    [MethodologyId]          INT                NOT NULL,
    [SubmissionId]           INT                NOT NULL,
    [Capacity_Ann_kMT]       FLOAT (53)         NOT NULL,
    [Utilization_Pcnt]       FLOAT (53)         NOT NULL,
    [_CapacityInfer_Ann_kMT] AS                 (CONVERT([float],([Capacity_Ann_kMT]/[Utilization_Pcnt])*(100.0),0)) PERSISTED NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (128)     CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (128)     CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (128)     CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Capacity_CrackedGasTransfers] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_Capacity_CrackedGasTransfers_Capacity_Ann_kMT_MinIncl_0.0] CHECK ([Capacity_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_Capacity_CrackedGasTransfers_Utilization_Pcnt_MinIncl_0.0] CHECK ([Utilization_Pcnt]>=(0.0)),
    CONSTRAINT [FK_Capacity_CrackedGasTransfers_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Capacity_CrackedGasTransfers_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_Capacity_CrackedGasTransfers_u]
	ON [calc].[Capacity_CrackedGasTransfers]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[Capacity_CrackedGasTransfers]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[Capacity_CrackedGasTransfers].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[Capacity_CrackedGasTransfers].[SubmissionId]	= INSERTED.[SubmissionId];

END;