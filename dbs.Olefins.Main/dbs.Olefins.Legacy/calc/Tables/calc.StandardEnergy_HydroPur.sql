﻿CREATE TABLE [calc].[StandardEnergy_HydroPur] (
    [MethodologyId]          INT                NOT NULL,
    [SubmissionId]           INT                NOT NULL,
    [H2Pur_kScfYear]         FLOAT (53)         NOT NULL,
    [H2Pur_kScfDay]          FLOAT (53)         NOT NULL,
    [StandardEnergy_MBtuDay] FLOAT (53)         NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_HydroPur_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_HydroPur] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_StandardEnergy_HydroPur_H2Pur_kScfDay_MinIncl_0.0] CHECK ([H2Pur_kScfDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_HydroPur_H2Pur_kScfYear_MinIncl_0.0] CHECK ([H2Pur_kScfYear]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_HydroPur_StandardEnergy_MBtuDay_MinIncl_0.0] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [FK_StandardEnergy_HydroPur_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StandardEnergy_HydroPur_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_StandardEnergy_HydroPur_u]
	ON [calc].[StandardEnergy_HydroPur]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_HydroPur]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_HydroPur].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_HydroPur].[SubmissionId]		= INSERTED.[SubmissionId];

END;