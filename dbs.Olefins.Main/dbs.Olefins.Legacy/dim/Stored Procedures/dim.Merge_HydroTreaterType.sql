﻿CREATE PROCEDURE [dim].[Merge_HydroTreaterType]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [dim].[HydroTreaterType_LookUp] AS Target
	USING
	(
		VALUES
			('All', 'All Hydrotreater Types', 'All Hydrotreater Types'),
			('B', 'Both', 'Both Selective Saturation and Desulfurization'),
			('DS', 'Desulfurization', 'Desulfurization'),
			('SS', 'Selective Saturation', 'Selective Saturation'),
			('None', 'No Hydrotreater Selected', 'No Hydrotreater Selected')
	)
	AS Source([HydroTreaterTypeTag], [HydroTreaterTypeName], [HydroTreaterTypeDetail])
	ON	Target.[HydroTreaterTypeTag]		= Source.[HydroTreaterTypeTag]
	WHEN MATCHED THEN UPDATE SET
		Target.[HydroTreaterTypeName]		= Source.[HydroTreaterTypeName],
		Target.[HydroTreaterTypeDetail]		= Source.[HydroTreaterTypeDetail]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([HydroTreaterTypeTag], [HydroTreaterTypeName], [HydroTreaterTypeDetail])
		VALUES([HydroTreaterTypeTag], [HydroTreaterTypeName], [HydroTreaterTypeDetail])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	MERGE INTO [dim].[HydroTreaterType_Parent] AS Target
	USING
	(
		SELECT
			m.MethodologyId,
			l.HydroTreaterTypeId,
			p.HydroTreaterTypeId,
			t.Operator,
			t.SortKey,
			'/'
		FROM (VALUES
			('All', 'All', '+', 1),
			('B', 'All', '+', 2),
			('DS', 'All', '+', 3),
			('SS', 'All', '+', 4),
			('None', 'None', '+', 5)
			)	t(HydroTreaterTypeTag, ParentTag, Operator, SortKey)
		INNER JOIN [dim].[HydroTreaterType_LookUp]	l
			ON	l.HydroTreaterTypeTag = t.HydroTreaterTypeTag
		INNER JOIN [dim].[HydroTreaterType_LookUp]	p
			ON	p.HydroTreaterTypeTag = t.ParentTag
		INNER JOIN [ante].[Methodology]				m
			ON	m.[MethodologyTag] = '2013'
	)
	AS Source([MethodologyId], [HydroTreaterTypeId], [ParentId], [Operator], [SortKey], [Hierarchy])
	ON	Target.[MethodologyId]		= Source.[MethodologyId]
	AND	Target.[HydroTreaterTypeId]	= Source.[HydroTreaterTypeId]
	WHEN MATCHED THEN UPDATE SET
		Target.[ParentId]			= Source.[ParentId],
		Target.[Operator]			= Source.[Operator],
		Target.[SortKey]			= Source.[SortKey],
		Target.[Hierarchy]			= Source.[Hierarchy]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [HydroTreaterTypeId], [ParentId], [Operator], [SortKey], [Hierarchy])
		VALUES([MethodologyId], [HydroTreaterTypeId], [ParentId], [Operator], [SortKey], [Hierarchy])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	EXECUTE dim.Update_Parent 'dim', 'HydroTreaterType_Parent', 'MethodologyId', 'HydroTreaterTypeId', 'ParentId', 'SortKey', 'Hierarchy';
	EXECUTE dim.Merge_Bridge 'dim', 'HydroTreaterType_Parent', 'dim', 'HydroTreaterType_Bridge', 'MethodologyId', 'HydroTreaterTypeId', 'SortKey', 'Hierarchy', 'Operator';

END;