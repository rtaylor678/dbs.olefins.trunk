﻿CREATE PROCEDURE [dim].[Merge_Operator]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [dim].[Operator] AS Target
	USING
	(
	VALUES
		('*'),
		('+'),
		('-'),
		('/'),
		('~')
	)
	AS Source([Operator])
	ON	Target.[Operator]			= Source.[Operator]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Operator])
		VALUES([Operator])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;