﻿using System;
using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace Sa.Chem.Simulation.Ux
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();

		private void runLoop_Click(object sender, EventArgs e)
		{
			AllocConsole();

			using (Pyps.Controller p = new Pyps.Controller())
			{
				p.RunQueue();
			}

			Console.WriteLine();
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}
		
		private void runOnefile_Click(object sender, EventArgs e)
		{
			AllocConsole();

			using (Pyps.Controller p = new Pyps.Controller())
			{
				p.RunFeed();
			}

			Console.WriteLine();
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}

		private void backgroundWorkers_Click(object sender, EventArgs e)
		{
			AllocConsole();

			using (Pyps.Controller p = new Pyps.Controller())
			{
				p.RunQueue();
			}

			Console.WriteLine();
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}

		private void processParallel_Click(object sender, EventArgs e)
		{
			PypsParallel p = new PypsParallel(listBoxProcessing, label1);
			p.AddToList(listBoxAdd);
			p.ParallelList();
		}

		private void backGround_Click(object sender, EventArgs e)
		{
			AllocConsole();

			//PypsMT.Controller pMtc = new PypsMT.Controller();
			//pMtc.RunQueue();

			Console.WriteLine();
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}

		private void listBoxProcessing_ItemsChanged(object sender, ControlEventArgs e)
		{
			label1.Text = listBoxProcessing.Items.Count.ToString();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			const string pathRoot = @"C:\CPA_20130129\dbs.Olefins.Trunk\Development\dbs.Olefins.20150505.Pyps64\Sa.Chem.Simulation.Ux\bin\Debug\";

			string pathFolder = pathRoot + "Engine.Pyps";
			string pathFile = pathRoot + "Engine.Pyps.HashContents";

			Sa.FileOps.CreateFolderHashFile(pathFolder, pathFile);

			List<string> badFiles;
			bool ok = Sa.FileOps.VerifyFolderContents(pathFile, pathFolder, out badFiles);

			if (ok)
			{
			}
		}
	}
}
