﻿DECLARE	@FactorSetId			VARCHAR(12) = '2009';
DECLARE	@GroupId				VARCHAR(25) = '2013PCH162';
DECLARE	@TargetId				VARCHAR(25) = '13PCHc:BASFP09';

SELECT * FROM [cons].[RefList] [rl] WHERE  [rl].[UserGroup] IN ('BASFP09', 'BASFP11', 'BASFP13');

SELECT * FROM [reports].[ReportGroups] [rg] WHERE [rg].[ListID] = '13PCHc' AND [rg].[UserGroup] IN ('BASFP09', 'BASFP11', 'BASFP13');

SELECT * FROM [gap].[Peers]	[p]
WHERE [p].[TargetId] IN (SELECT [rg].[GroupId] FROM [reports].[ReportGroups] [rg] WHERE [rg].[ListID] = '13PCHc' AND [rg].[UserGroup] IN ('BASFP09', 'BASFP11', 'BASFP13'))
	ORDER BY [p].[TargetId] ASC, [p].[GroupId];

--SELECT * FROM [cons].[RefList] [rl] WHERE  [rl].[Refnum] = @GroupId;
--SELECT * FROM [reports].[ReportGroups] [rg] WHERE  [rg].[GroupID] = @TargetId;
--SELECT * FROM [gap].[Peers]	[p] WHERE [p].[GroupId] = @GroupId;

--EXECUTE   [Olefins].[reports].[spALL] @TargetId;
--EXECUTE [Olefins13].[reports].[spALL] @TargetId;
--EXECUTE   [Olefins].[calc].[CalculateGroup] @FactorSetId, @TargetId;
--EXECUTE [Olefins13].[calc].[CalculateGroup] '2009', '13PCHc:BASFP09';

--EXECUTE [gap].[ProcessGaps] @GroupId, @TargetId, 'Olefins13';

--SELECT * FROM [Olefins].[gap].[AnalysisResultsAggregate]	[ara]
--WHERE	[ara].[GroupId]		= @GroupId
--	AND	[ara].[TargetId]	= '13PCHc:BASFP13'
--	AND	[ara].[GapId]		IN ('UnPlanned_DT', 'UnPlanned_SD', 'NOC', 'CapUtil', 'Loss', 'PyroYieldPerf', 'ProdYield');

SELECT DISTINCT
	[ppsa].[Refnum]
FROM [Olefins13].[calc].[PricingPlantStreamAggregate] [ppsa]
WHERE [ppsa].[Refnum] IN ('13PCHc:BASFP09:USA', '13PCHc:BASFP11:USA', '13PCHc:BASFP13:USA')

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

		BEGIN

		SET @ProcedureDesc = CHAR(9) + '@Disposition Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @RecycleValue		TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Ethane]			REAL	NULL,
			[DilEthane]			REAL	NULL,
			[ROGEthane]			REAL	NULL,
			[Propane]			REAL	NULL,
			[DilPropane]		REAL	NULL,
			[ROGPropane]		REAL	NULL,
			[Butane]			REAL	NULL,
			[DilButane]			REAL	NULL,
			[ROGC4Plus]			REAL	NULL,

			[EthRec]			AS CASE WHEN [Ethane] > 0.0 THEN [Ethane] ELSE CASE WHEN [DilEthane] > 0.0 THEN [DilEthane] ELSE CASE WHEN [ROGEthane] > 0.0 THEN [ROGEthane] ELSE 0.0 END END END
								PERSISTED NOT NULL,
			[ProRec]			AS CASE WHEN [Propane] > 0.0 THEN [Propane] ELSE CASE WHEN [DilPropane] > 0.0 THEN [DilPropane] ELSE CASE WHEN [ROGPropane] > 0.0 THEN [ROGPropane] ELSE 0.0 END END END
								PERSISTED NOT NULL,
			[ButRec]			AS CASE WHEN [Butane] > 0.0 THEN [Butane] ELSE CASE WHEN [DilButane] > 0.0 THEN [DilButane] ELSE CASE WHEN [ROGC4Plus] > 0.0 THEN [ROGC4Plus] ELSE 0.0 END END END
								PERSISTED NOT NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @RecycleValue([GroupId], [Ethane], [DilEthane], [ROGEthane], [Propane], [DilPropane], [ROGPropane], [Butane], [DilButane], [ROGC4Plus])
		SELECT
			[GroupId]	= @TargetId,

			[BenchEthane]		= CASE WHEN bV.Ethane		> 0.0 THEN bV.Ethane		ELSE gV.Ethane END,
			[BenchDilEthane]	= CASE WHEN bV.DilEthane	> 0.0 THEN bV.DilEthane		ELSE gV.DilEthane END,
			[BenchRogEthane]	= CASE WHEN bV.ROGEthane	> 0.0 THEN bV.ROGEthane		ELSE gV.ROGEthane END,

			[BenchPropane]		= CASE WHEN bV.Propane		> 0.0 THEN bV.Propane		ELSE gV.Propane END,
			[BenchDilPropane]	= CASE WHEN bV.DilPropane	> 0.0 THEN bV.DilPropane	ELSE gV.DilPropane END,
			[BenchRogPropane]	= CASE WHEN bV.ROGPropane	> 0.0 THEN bV.ROGPropane	ELSE gV.ROGPropane END,

			[BenchButane]		= CASE WHEN bV.Butane		> 0.0 THEN bV.Butane		ELSE gV.Butane END,
			[BenchDilButane]	= CASE WHEN bV.DilButane	> 0.0 THEN bV.DilButane		ELSE gV.DilButane END,
			[BenchRogButane]	= CASE WHEN bV.ROGC4Plus	> 0.0 THEN bV.ROGC4Plus		ELSE gV.ROGC4Plus END

		FROM			[Olefins].[reports].[Yield]			gV
		LEFT OUTER JOIN	[Olefins13].[reports].[Yield]		bV
			ON	bV.[GroupId]	= @TargetId
			AND	bV.[DataType]	= '$PerMT'
		WHERE	gV.[GroupId]	= @GroupId
			AND	gV.[DataType]	= '$PerMT';

		INSERT INTO @RecycleValue([GroupId], [Ethane], [DilEthane], [ROGEthane], [Propane], [DilPropane], [ROGPropane], [Butane], [DilButane], [ROGC4Plus])
		SELECT
			gV.[GroupId],

			[GroupEthane]		= gV.Ethane,
			[GroupDilEthane]	= gV.DilEthane,
			[GroupRogEthane]	= gV.ROGEthane,

			[GroupPropane]		= gV.Propane,
			[GroupDilPropane]	= gV.DilPropane,
			[GroupRogPropane]	= gV.ROGPropane,

			[GroupButane]		= gV.Butane,
			[GroupDilButane]	= gV.DilButane,
			[GroupRogButane]	= gV.ROGC4Plus

		FROM	[Olefins].[reports].[Yield]			gV
		WHERE	gV.[GroupId]	= @GroupId
			AND	gV.[DataType]	= '$PerMT';

		SET @ProcedureDesc = CHAR(9) + '@Disposition Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @Disposition		TABLE
		(
			[GroupId]	VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),

			[C14]		REAL		NOT	NULL,
			[C15]		REAL		NOT	NULL,
			[C16]		REAL		NOT	NULL,
			[C17]		REAL		NOT	NULL,
			[C18]		REAL		NOT	NULL,
			[C19]		REAL		NOT	NULL,
			[C20]		REAL		NOT	NULL,
			[C21]		REAL		NOT	NULL,
			[C22]		REAL		NOT	NULL,
			[C23]		REAL		NOT	NULL,
			[C24]		REAL		NOT	NULL,
			[C25]		REAL		NOT	NULL,
			[C26]		REAL		NOT	NULL,
			[C27]		REAL		NOT	NULL,
			[C28]		REAL		NOT	NULL,
			[C29]		REAL		NOT	NULL,
			[C30]		REAL		NOT	NULL,
			[C31]		REAL		NOT	NULL,
			[C32]		REAL		NOT	NULL,
			[C33]		REAL		NOT	NULL,
			[C34]		REAL		NOT	NULL,
			[C35]		REAL		NOT	NULL,
			[C192]		REAL		NOT	NULL,
			[C197]		REAL		NOT	NULL,
			[C201]		REAL		NOT	NULL,
			[C207]		REAL		NOT	NULL,
			[C211]		REAL		NOT	NULL,
			[C219]		REAL		NOT	NULL,
			[C227]		REAL		NOT	NULL,
			[C231]		REAL		NOT	NULL,
			[C236]		REAL		NOT	NULL,
			[C241]		REAL		NOT	NULL,
			[C246]		REAL		NOT	NULL,
			[C252]		REAL		NOT	NULL,
			[C257]		REAL		NOT	NULL,
			[C262]		REAL		NOT	NULL,
			[D193]		REAL		NOT	NULL,
			[D194]		REAL		NOT	NULL,
			[D195]		REAL		NOT	NULL,
			[D198]		REAL		NOT	NULL,
			[D199]		REAL		NOT	NULL,
			[D208]		REAL		NOT	NULL,
			[D220]		REAL		NOT	NULL,
			[D228]		REAL		NOT	NULL,
			[D242]		REAL		NOT	NULL,
			[D253]		REAL		NOT	NULL,
			[D258]		REAL		NOT	NULL,
			[D263]		REAL		NOT	NULL,
			[D202]		REAL		NOT	NULL,
			[D209]		REAL		NOT	NULL,
			[D212]		REAL		NOT	NULL,
			[D221]		REAL		NOT	NULL,
			[D229]		REAL		NOT	NULL,
			[D232]		REAL		NOT	NULL,
			[D237]		REAL		NOT	NULL,
			[D254]		REAL		NOT	NULL,
			[D259]		REAL		NOT	NULL,
			[D203]		REAL		NOT	NULL,
			[D213]		REAL		NOT	NULL,
			[D222]		REAL		NOT	NULL,
			[D233]		REAL		NOT	NULL,
			[D255]		REAL		NOT	NULL,
			[D260]		REAL		NOT	NULL,
			[D204]		REAL		NOT	NULL,
			[D214]		REAL		NOT	NULL,
			[D234]		REAL		NOT	NULL,
			[D238]		REAL		NOT	NULL,
			[D205]		REAL		NOT	NULL,
			[D215]		REAL		NOT	NULL,
			[D239]		REAL		NOT	NULL,
			[D243]		REAL		NOT	NULL,
			[D216]		REAL		NOT	NULL,
			[D223]		REAL		NOT	NULL,
			[D244]		REAL		NOT	NULL,
			[D247]		REAL		NOT	NULL,
			[D217]		REAL		NOT	NULL,
			[D224]		REAL		NOT	NULL,
			[D248]		REAL		NOT	NULL,
			[D225]		REAL		NOT	NULL,
			[D249]		REAL		NOT	NULL,
			[D250]		REAL		NOT	NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @Disposition([GroupId], 
			[C14], [C15], [C16], [C17], [C18], [C19], [C20], [C21], [C22], [C23], [C24], [C25], [C26], [C27], [C28], [C29], [C30], [C31], [C32], [C33], [C34], [C35],
			[C192], [C197], [C201], [C207], [C211], [C219], [C227], [C231], [C236], [C241], [C246], [C252], [C257], [C262],
			[D193], [D198], [D199], [D208], [D220], [D228], [D237], [D242], [D253], [D258], [D263],
			[D194], [D202], [D209], [D212], [D221], [D229], [D232], [D254], [D259],
			[D195], [D203], [D213], [D222], [D233], [D255], [D260],
			[D204], [D214], [D234], [D238],
			[D205], [D215], [D239], [D243],
			[D216], [D223], [D244], [D247],
			[D217], [D224], [D248],
			[D225], [D249],
			[D250]
		)
		SELECT
			i.[GroupId],

			i.[C14], i.[C15], i.[C16], i.[C17], i.[C18], i.[C19], i.[C20], i.[C21], i.[C22], i.[C23], i.[C24], i.[C25], i.[C26], i.[C27], i.[C28], i.[C29], i.[C30], i.[C31], i.[C32], i.[C33], i.[C34], i.[C35],
			i.[C192], i.[C197], i.[C201], i.[C207], i.[C211], i.[C219], i.[C227], i.[C231], i.[C236], i.[C241], i.[C246], i.[C252], i.[C257], i.[C262],
			i.[D193], i.[D198], i.[D199], i.[D208], i.[D220], i.[D228], i.[D237], i.[D242], i.[D253], i.[D258], i.[D263],
			i.[D194], i.[D202], i.[D209], i.[D212], i.[D221], i.[D229], i.[D232], i.[D254], i.[D259],
			i.[D195], i.[D203], i.[D213], i.[D222], i.[D233], i.[D255], i.[D260],
			i.[D204], i.[D214], i.[D234], i.[D238],
			i.[D205], i.[D215], i.[D239], i.[D243],
			i.[D216], i.[D223], i.[D244], i.[D247],
			i.[D217], i.[D224], i.[D248],
			i.[D225], i.[D249],

			--	[D250]	= C246-D247-D248-D249
			[D250]	= i.[C246] - i.[D247] - i.[D248] - i.[D249]

		FROM (
			SELECT
				h.[GroupId],

				h.[C14], h.[C15], h.[C16], h.[C17], h.[C18], h.[C19], h.[C20], h.[C21], h.[C22], h.[C23], h.[C24], h.[C25], h.[C26], h.[C27], h.[C28], h.[C29], h.[C30], h.[C31], h.[C32], h.[C33], h.[C34], h.[C35],
				h.[C192], h.[C197], h.[C201], h.[C207], h.[C211], h.[C219], h.[C227], h.[C231], h.[C236], h.[C241], h.[C246], h.[C252], h.[C257], h.[C262],
				h.[D193], h.[D198], h.[D199], h.[D208], h.[D220], h.[D228], h.[D237], h.[D242], h.[D253], h.[D258], h.[D263],
				h.[D194], h.[D202], h.[D209], h.[D212], h.[D221], h.[D229], h.[D232], h.[D254], h.[D259],
				h.[D195], h.[D203], h.[D213], h.[D222], h.[D233], h.[D255], h.[D260],
				h.[D204], h.[D214], h.[D234], h.[D238],
				h.[D205], h.[D215], h.[D239], h.[D243],
				h.[D216], h.[D223], h.[D244], h.[D247],
				h.[D217], h.[D224], h.[D248],

				--	[D225]	= C219-SUM(D220:D224)
				[D225]	= h.[C219] - h.[D220] - h.[D221] - h.[D222] - h.[D223] - h.[D224],

				--	[D249]	= MIN(C246-D247-D248,C28-D253-D259)
				[D249]	= [Olefins].[calc].[MinValue](h.[C246] - h.[D247] - h.[D248], h.[C28] - h.[D253] - h.[D259])

			FROM (
				SELECT
					g.[GroupId],

					g.[C14], g.[C15], g.[C16], g.[C17], g.[C18], g.[C19], g.[C20], g.[C21], g.[C22], g.[C23], g.[C24], g.[C25], g.[C26], g.[C27], g.[C28], g.[C29], g.[C30], g.[C31], g.[C32], g.[C33], g.[C34], g.[C35],
					g.[C192], g.[C197], g.[C201], g.[C207], g.[C211], g.[C219], g.[C227], g.[C231], g.[C236], g.[C241], g.[C246], g.[C252], g.[C257], g.[C262],
					g.[D193], g.[D198], g.[D199], g.[D208], g.[D220], g.[D228], g.[D237], g.[D242], g.[D253], g.[D258], g.[D263],
					g.[D194], g.[D202], g.[D209], g.[D212], g.[D221], g.[D229], g.[D232], g.[D254], g.[D259],
					g.[D195], g.[D203], g.[D213], g.[D222], g.[D233], g.[D255], g.[D260],
					g.[D204], g.[D214], g.[D234], g.[D238],
					g.[D205], g.[D215], g.[D239], g.[D243],
					g.[D216], g.[D223], g.[D244], g.[D247],

					--	[D217]	= C211-SUM(D212:D216)
					[D217]	= g.[C211] - g.[D212] - g.[D213] - g.[D214] - g.[D215] - g.[D216],

					--	[D224]	= MIN(C219-D220-D221-D222-D223,C31-D194-D198-D204-D216)
					[D224]	= [Olefins].[calc].[MinValue](g.[C219] - g.[D220] - g.[D221] - g.[D222] - g.[D223], g.[C31] - g.[D194] - g.[D198] - g.[D204] - g.[D216]),

					--	[D248]	= MIN(C246-D247,C26-D242)
					[D248]	= [Olefins].[calc].[MinValue](g.[C246] - g.[D247], g.[C26] - g.[D242])

				FROM (
					SELECT
						f.[GroupId],

						f.[C14], f.[C15], f.[C16], f.[C17], f.[C18], f.[C19], f.[C20], f.[C21], f.[C22], f.[C23], f.[C24], f.[C25], f.[C26], f.[C27], f.[C28], f.[C29], f.[C30], f.[C31], f.[C32], f.[C33], f.[C34], f.[C35],
						f.[C192], f.[C197], f.[C201], f.[C207], f.[C211], f.[C219], f.[C227], f.[C231], f.[C236], f.[C241], f.[C246], f.[C252], f.[C257], f.[C262],
						f.[D193], f.[D198], f.[D199], f.[D208], f.[D220], f.[D228], f.[D237], f.[D242], f.[D253], f.[D258], f.[D263],
						f.[D194], f.[D202], f.[D209], f.[D212], f.[D221], f.[D229], f.[D232], f.[D254], f.[D259],
						f.[D195], f.[D203], f.[D213], f.[D222], f.[D233], f.[D255], f.[D260],
						f.[D204], f.[D214], f.[D234], f.[D238],
						f.[D205], f.[D215], f.[D239], f.[D243],

						--	[D216]	= MIN(C211-D212-D213-D214-D215,C31-D194-D198-D204)
						[D216]	= [Olefins].[calc].[MinValue](f.[C211] - f.[D212] - f.[D213] - f.[D214 ] - f.[D215], f.[C31] - f.[D194] - f.[D198] - f.[D204]),

						--	[D223]	= MIN(C219-D220-D221-D222,C22-D215)
						[D223]	= [Olefins].[calc].[MinValue](f.[C219] - f.[D220] - f.[D221] - f.[D222], f.[C22] - f.[D215]),

						--	[D244]	= C241-D242-D243
						[D244]	= f.[C241] - f.[D242] - f.[D243],

						--	[D247]	= MIN(C246,C27-D229-D234-D243)
						[D247]	= [Olefins].[calc].[MinValue](f.[C246], f.[C27] - f.[D229] - f.[D234] - f.[D243])

					FROM (
						SELECT
							e.[GroupId],

							e.[C14], e.[C15], e.[C16], e.[C17], e.[C18], e.[C19], e.[C20], e.[C21], e.[C22], e.[C23], e.[C24], e.[C25], e.[C26], e.[C27], e.[C28], e.[C29], e.[C30], e.[C31], e.[C32], e.[C33], e.[C34], e.[C35],
							e.[C192], e.[C197], e.[C201], e.[C207], e.[C211], e.[C219], e.[C227], e.[C231], e.[C236], e.[C241], e.[C246], e.[C252], e.[C257], e.[C262],
							e.[D193], e.[D198], e.[D199], e.[D208], e.[D220], e.[D228], e.[D237], e.[D242], e.[D253], e.[D258], e.[D263],
							e.[D194], e.[D202], e.[D209], e.[D212], e.[D221], e.[D229], e.[D232], e.[D254], e.[D259],
							e.[D195], e.[D203], e.[D213], e.[D222], e.[D233], e.[D255], e.[D260],
							e.[D204], e.[D214], e.[D234], e.[D238],

							--	[D205]	= C201-SUM(D202:D204)
							[D205]	= e.[C201] - e.[D202] - e.[D203] - e.[D204],

							--	[D215]	= MIN(C211-D212-D213-D214,C22)
							[D215]	= [Olefins].[calc].[MinValue](e.[C211] - e.[D212] - e.[D213] - e.[D214], e.[C22]),

							--	[D239]	= C236-D237-D238
							[D239]	= e.[C236] - e.[D237] - e.[D238],

							--	[D243]	= MIN(C241-D242,C27-D229-D234)
							[D243]	= [Olefins].[calc].[MinValue](e.[C241] - e.[D242], e.[C27] - e.[D229] - e.[D234])

						FROM (
							SELECT
								d.[GroupId],

								d.[C14], d.[C15], d.[C16], d.[C17], d.[C18], d.[C19], d.[C20], d.[C21], d.[C22], d.[C23], d.[C24], d.[C25], d.[C26], d.[C27], d.[C28], d.[C29], d.[C30], d.[C31], d.[C32], d.[C33], d.[C34], d.[C35],
								d.[C192], d.[C197], d.[C201], d.[C207], d.[C211], d.[C219], d.[C227], d.[C231], d.[C236], d.[C241], d.[C246], d.[C252], d.[C257], d.[C262],
								d.[D193], d.[D198], d.[D199], d.[D208], d.[D220], d.[D228], d.[D237], d.[D242], d.[D253], d.[D258], d.[D263],
								d.[D194], d.[D202], d.[D209], d.[D212], d.[D221], d.[D229], d.[D232], d.[D254], d.[D259],
								d.[D195], d.[D203], d.[D213], d.[D222], d.[D233], d.[D255], d.[D260],
						
								--	[D204]	= IF(C31-D194-D198>=C201-D202-D203,C201-D202-D203,C31-D194-D198)
								[D204]	=	CASE WHEN d.[C31] - d.[D194] - d.[D198] >= d.[C201] - d.[D202] - d.[D203]
											THEN d.[C201] - d.[D202] - d.[D203]
											ELSE d.[C31] - d.[D194] - d.[D198]
											END,

								--	[D214]	= MIN(C211-D212-D213,D222*0.01)
								[D214]	= [Olefins].[calc].[MinValue](d.[C211] - d.[D212] - d.[D213], d.[D222] * 0.01),

								--	[D234]	= C231-D232-D233
								[D234]	= d.[C231] - d.[D232] - d.[D233],

								--	[G238]	= MIN(C236-D237,C24-D233)
								[D238]	= [Olefins].[calc].[MinValue](d.[C236] - d.[D237], d.[C24] - d.[D233])

							FROM (
								SELECT
									c.[GroupId],

									c.[C14], c.[C15], c.[C16], c.[C17], c.[C18], c.[C19], c.[C20], c.[C21], c.[C22], c.[C23], c.[C24], c.[C25], c.[C26], c.[C27], c.[C28], c.[C29], c.[C30], c.[C31], c.[C32], c.[C33], c.[C34], c.[C35],
									c.[C192], c.[C197], c.[C201], c.[C207], c.[C211], c.[C219], c.[C227], c.[C231], c.[C236], c.[C241], c.[C246], c.[C252], c.[C257], c.[C262],
									c.[D193], c.[D198], c.[D199], c.[D208], c.[D220], c.[D228], c.[D237], c.[D242], c.[D253], c.[D258], c.[D263],
									c.[D194], c.[D202], c.[D209], c.[D212], c.[D221], c.[D229], c.[D232], c.[D254], c.[D259],

									--	[D195]	= C192-D193-D194
									[D195]	=	c.[C192] - c.[D193] - c.[D194],

									--	[D203]	= IF(C201=0,0,D209*0.001)
									[D203] = CASE WHEN c.[C201] = 0.0 THEN 0.0 ELSE c.[D209] * 0.001 END,

									--	[D213]	= MIN(C211-D212,D221/L20*(1-L20))
									[D213]	= [Olefins].[calc].[MinValue](c.[C211] - c.[D212], CASE WHEN c.[L20] <> 0.0 THEN c.[D221] / c.[L20] * (1.0 - c.[L20]) ELSE 0.0 END),

									--	[D222]	= MIN(C219-D220-D221,C19*0.99)
									[D222]	= [Olefins].[calc].[MinValue](c.[C219] - c.[D220] - c.[D221], c.[C19] * 0.99),

									--	[D233]	= MIN(C231-D232,C24)
									[D233]	= [Olefins].[calc].[MinValue](c.[C231] - c.[D232], c.[C24]),

									--	[D255]	= C252-D253-D254
									[D255]	= c.[C252] - c.[D253] - c.[D254],

									--	[D260]	= C257-D258-D259
									[D260]	= c.[C257] - c.[D258] - c.[D259]

								FROM (
									SELECT
										b.[GroupId],

										b.[C14], b.[C15], b.[C16], b.[C17], b.[C18], b.[C19], b.[C20], b.[C21], b.[C22], b.[C23], b.[C24], b.[C25], b.[C26], b.[C27], b.[C28], b.[C29], b.[C30], b.[C31], b.[C32], b.[C33], b.[C34], b.[C35],
										b.[C192], b.[C197], b.[C201], b.[C207], b.[C211], b.[C219], b.[C227], b.[C231], b.[C236], b.[C241], b.[C246], b.[C252], b.[C257], b.[C262],
										b.[D193], b.[D198], b.[D199], b.[D208], b.[D220], b.[D228], b.[D237], b.[D242], b.[D253], b.[D258], b.[D263],

										--	[D194]	= IF(C14>=C192,0,IF(C31>=C192+C197-D193,C192-D193,0))
										[D194]	=	CASE WHEN b.[C14] >= b.[C192]
													THEN 0.0
													ELSE
														CASE WHEN b.[C31] >= b.[C192] + b.[C197] - b.[D193]
														THEN b.[C192] - b.[D193]
														ELSE 0.0
														END
													END,

										--	[D202]	= IF(C201=0,0,D208/L18*(1-L18))
										[D202] = CASE WHEN b.[C201] = 0.0 OR b.[L18] = 0.0 THEN 0.0 ELSE b.[D208] / b.[L18] * (1.0 - b.[L18]) END,
								
										--	[D209]	= C207-D208
										[D209]	= b.[C207] - b.[D208],

										--	[D212]	= MIN(C211,D220/L21*(1-L21))
										[D212]	= [Olefins].[calc].[MinValue](b.[C211], CASE WHEN b.[L21] <> 0.0 THEN b.[D220] / b.[L21] * (1.0 - b.[L21]) ELSE 0.0 END),

										--	[D221]	= MIN(C219-D220,C20*L20)
										[D221]	= [Olefins].[calc].[MinValue](b.[C219] - b.[D220], b.[C20] * b.[L20]),

										--	[D229]	= C227-D228
										[D229]	= b.[C227] - b.[D228],

										--	[D232]	= MIN(C231,C25-D228)
										[D232]	= [Olefins].[calc].[MinValue](b.[C231], b.[C25] - b.[D228]),

										--	[D254]	= MIN(C252-D253,C29-D258)
										[D254]	= [Olefins].[calc].[MinValue](b.[C252] - b.[D253], b.[C29] - b.[D258]),

										--	[D259]	= MIN(C257-D258,C28-D253)
										[D259]	= [Olefins].[calc].[MinValue](b.[C257] - b.[D258], b.[C28] - b.[D253]),

										--b.[L18],
										b.[L20]
										--b.[L21]

									FROM (
										SELECT
											a.[GroupId],

											a.[C14], a.[C15], a.[C16], a.[C17], a.[C18], a.[C19], a.[C20], a.[C21], a.[C22], a.[C23], a.[C24], a.[C25], a.[C26], a.[C27], a.[C28], a.[C29], a.[C30], a.[C31], a.[C32], a.[C33], a.[C34], a.[C35],
											a.[C192], a.[C197], a.[C201], a.[C207], a.[C211], a.[C219], a.[C227], a.[C231], a.[C236], a.[C241], a.[C246], a.[C252], a.[C257], a.[C262],

											--	[D193]	= IF(C14>=C192,C192,C14)
											[D193]	=	CASE WHEN a.[C14] >= a.[C192]
														THEN a.[C192]
														ELSE a.[C14]
														END,

											--	[D198]	= IF(C31>=C197,C197,C31)
											[D198]	=	CASE WHEN a.[C31] >= a.[C197]
														THEN a.[C197]
														ELSE a.[C31]
														END,

											--	[D199]	= IF(C31>=C197,0,C197-C31)
											[D199]	=	CASE WHEN a.[C31] >= a.[C197]
														THEN 0.0
														ELSE a.[C197] - a.[C31]
														END,

											--	[D208]	= MIN(C207,C18*L18)
											[D208]	= [Olefins].[calc].[MinValue](a.[C207], a.[C18] * COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0) / 100.0),

											--	[D220]	= MIN(C219,C21*L21)
											[D220]	= [Olefins].[calc].[MinValue](a.[C219], a.[C21] * COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0) / 100.0),

											--	[D228]	= MIN(C227,C25)
											[D228]	= [Olefins].[calc].[MinValue](a.[C227], a.[C25]),

											--	[D237]	= MIN(C236,C24)
											[D237]	= [Olefins].[calc].[MinValue](a.[C236], a.[C24]),

											--	[D242]	= MIN(C241,C26)
											[D242]	= [Olefins].[calc].[MinValue](a.[C241], a.[C26]),

											--	[D253]	= MIN(C252,C28)
											[D253]	= [Olefins].[calc].[MinValue](a.[C252], a.[C28]),

											--	[D258]	= MIN(C257,C29)
											[D258]	= [Olefins].[calc].[MinValue](a.[C257], a.[C29]),

											--	[D263]	= F262
											[D263]	= a.[C262],

											[L18]	= COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0)	/ 100.0,
											[L20]	= COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0)	/ 100.0,
											[L21]	= COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0)	/ 100.0

										FROM (
											SELECT
												y.[GroupId],
												[TargetId] = @TargetId,
												y.[DataType],
												[C14]	= y.[Hydrogen],
												[C15]	= y.[Methane],
												[C16]	= y.[Acetylene],
												[C17]	= y.[EthylenePG],
												[C18]	= y.[EthyleneCG],
												[C19]	= y.[PropylenePG],
												[C20]	= y.[PropyleneCG],
												[C21]	= y.[PropyleneRG],
												[C22]	= y.[PropaneC3Resid],
												[C23]	= y.[Butadiene],
												[C24]	= y.[IsoButylene],
												[C25]	= y.[C4Oth],
												[C26]	= y.[Benzene],
												[C27]	= y.[PyroGasoline],
												[C28]	= y.[PyroGasOil],
												[C29]	= y.[PyroFuelOil],
												[C30]	= y.[AcidGas],
												[C31]	= y.[PPFC],
												[C32]	= y.[ProdOther1],
												[C33]	= y.[ProdOther2],
												[C34]	= y.[LossFlareVent],
												[C35]	= y.[LossMeasure] + y.[LossOther],

												[C192]	= 						+ y.[DilHydrogen]	+ y.[ROGHydrogen],	
												[C197]	=						+ y.[DilMethane]	+ y.[ROGMethane],
												[C201]	=						+ y.[DilEthane]		+ y.[ROGEthane]		- y.[RecSuppEthane],
												[C207]	=	y.[ConcEthylene]	+ y.[DilEthylene]	+ y.[ROGEthylene],
												[C211]	=						+ y.[DilPropane]	+ y.[ROGPropane]	- y.[RecSuppPropane],
												[C219]	=	y.[ConcPropylene]	+ y.[DilPropylene]	+ y.[ROGPropylene],
												[C227]	=						+ y.[DilButane]		+ y.[ROGC4Plus]		- y.[RecSuppButane],
												[C231]	=						+ y.[DilButylene],
												[C236]	=	y.[ConcButadiene]	+ y.[DilButadiene],
												[C241]	=	y.[ConcBenzene]		+ y.[DilBenzene],
												[C246]	=						+ y.[DilMogas],		/*PyroGasoline*/
												[C252]	=	y.[SuppGasOil],
												[C257]	=	y.[SuppWashOil],
												[C262]	=	y.[SuppOther]		+ y.[ROGInerts]
											FROM	[Olefins].[reports].[Yield]					y
											WHERE	y.[GroupId]		= @GroupId
												AND	y.[DataType]	= 'kMT'
											
											UNION ALL
											
											SELECT
												y.[GroupId],
												[TargetId] = @TargetId,
												y.[DataType],
												[C14]	= y.[Hydrogen],
												[C15]	= y.[Methane],
												[C16]	= y.[Acetylene],
												[C17]	= y.[EthylenePG],
												[C18]	= y.[EthyleneCG],
												[C19]	= y.[PropylenePG],
												[C20]	= y.[PropyleneCG],
												[C21]	= y.[PropyleneRG],
												[C22]	= y.[PropaneC3Resid],
												[C23]	= y.[Butadiene],
												[C24]	= y.[IsoButylene],
												[C25]	= y.[C4Oth],
												[C26]	= y.[Benzene],
												[C27]	= y.[PyroGasoline],
												[C28]	= y.[PyroGasOil],
												[C29]	= y.[PyroFuelOil],
												[C30]	= y.[AcidGas],
												[C31]	= y.[PPFC],
												[C32]	= y.[ProdOther1],
												[C33]	= y.[ProdOther2],
												[C34]	= y.[LossFlareVent],
												[C35]	= y.[LossMeasure] + y.[LossOther],

												[C192]	= 						+ y.[DilHydrogen]	+ y.[ROGHydrogen],	
												[C197]	=						+ y.[DilMethane]	+ y.[ROGMethane],
												[C201]	=						+ y.[DilEthane]		+ y.[ROGEthane]		- y.[RecSuppEthane],
												[C207]	=	y.[ConcEthylene]	+ y.[DilEthylene]	+ y.[ROGEthylene],
												[C211]	=						+ y.[DilPropane]	+ y.[ROGPropane]	- y.[RecSuppPropane],
												[C219]	=	y.[ConcPropylene]	+ y.[DilPropylene]	+ y.[ROGPropylene],
												[C227]	=						+ y.[DilButane]		+ y.[ROGC4Plus]		- y.[RecSuppButane],
												[C231]	=						+ y.[DilButylene],
												[C236]	=	y.[ConcButadiene]	+ y.[DilButadiene],
												[C241]	=	y.[ConcBenzene]		+ y.[DilBenzene],
												[C246]	=						+ y.[DilMogas],		/*PyroGasoline*/
												[C252]	=	y.[SuppGasOil],
												[C257]	=	y.[SuppWashOil],
												[C262]	=	y.[SuppOther]		+ y.[ROGInerts]
											FROM	[Olefins13].[reports].[Yield]					y
											WHERE	y.[GroupId]		= @TargetId
												AND	y.[DataType]	= 'kMT'
											) a		--	1
										LEFT OUTER JOIN	[Olefins].[reports].[GapProductPurity]		gp
											ON	gp.[GroupId]	= a.[GroupId]
										LEFT OUTER JOIN	[Olefins13].[reports].[GapProductPurity]	bp
											ON	bp.[GroupId]	= a.[TargetId]
										) b			--	2
									) c				--	3
								) d					--	4
							) e						--	5
						) f							--	6
					) g								--	7
				) h									--	8
			) i;									--	9

		SET @ProcedureDesc = CHAR(9) + '@DispositionSupp Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @DispositionSupp	TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[StreamId]			VARCHAR(42)	NOT	NULL	CHECK([StreamId] <> ''),
			[Quantity_kMT]		REAL		NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [StreamId] ASC)
		);

		INSERT INTO @DispositionSupp([GroupId], [StreamId], [Quantity_kMT])
		SELECT
			p.[GroupId],
			p.[StreamId],
			p.[Quantity_kMT]
		FROM (
			SELECT
				d.[GroupId],
				[Hydrogen]			= d.[D193],
				[Methane]			= d.[D195] + d.[D199] + d.[D205] + d.[D217] + d.[D225],
				[Acetylene]			= CONVERT(REAL, 0.0),
				[EthylenePG]		= d.[D203] + d.[D209],
				[EthyleneCG]		= d.[D202] + d.[D208],
				[PropylenePG]		= d.[D214] + d.[D222],
				[PropyleneCG]		= d.[D213] + d.[D221],
				[PropyleneRG]		= d.[D212] + d.[D220],
				[PropaneC3Resid]	= d.[D215] + d.[D223],
				[Butadiene]			= d.[D237],
				[IsoButylene]		= d.[D233] + d.[D238],
				[C4Oth]				= d.[D228] + d.[D232] + d.[D239],
				[Benzene]			= d.[D242] + d.[D248],
				[PyroGasoline]		= d.[D229] + d.[D234] + d.[D243] + d.[D247],
				[PyroGasOil]		= d.[D244] + d.[D249] + d.[D253] + d.[D259],
				[PyroFuelOil]		= d.[D254] + d.[D258],
				[AcidGas]			= CONVERT(REAL, 0.0),
				[PPFC]				= d.[D194] + d.[D198] + d.[D204] + d.[D216] + d.[D224] + d.[D250] + d.[D255] + d.[D260] + d.[D263],
				[ProdOther1]		= CONVERT(REAL, 0.0),
				[ProdOther2]		= CONVERT(REAL, 0.0),
				[LossFlareVent]		= CONVERT(REAL, 0.0),
				[LossOther]			= CONVERT(REAL, 0.0)
			FROM @Disposition	d
			) u
			UNPIVOT (
				[Quantity_kMT] FOR [StreamId] IN
				(
				[Hydrogen],
				[Methane],
				[Acetylene],
				[EthylenePG],
				[EthyleneCG],
				[PropylenePG],
				[PropyleneCG],
				[PropyleneRG],
				[PropaneC3Resid],
				[Butadiene],
				[IsoButylene],
				[C4Oth],
				[Benzene],
				[PyroGasoline],
				[PyroGasOil],
				[PyroFuelOil],
				[AcidGas],
				[PPFC],
				[ProdOther1],
				[ProdOther2],
				[LossFlareVent],
				[LossOther]
				)
			) p;

		SET @ProcedureDesc = CHAR(9) + '@Production Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @Production			TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[StreamId]			VARCHAR(42)	NOT	NULL	CHECK([StreamId] <> ''),
			[Quantity_kMT]		REAL			NULL,
			[Amount_Cur]		REAL		NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [StreamId] ASC)
		);

		INSERT INTO @Production([GroupId], [StreamId], [Quantity_kMT], [Amount_Cur])
		SELECT
			d.[GroupId],
			d.[StreamId],
			d.[Quantity_kMT],
			d.[Amount_Cur]
		FROM (
			SELECT
				CASE y.[DataType]
					WHEN '$PerMT'	THEN 'Amount_Cur'
					WHEN 'kMT'		THEN 'Quantity_kMT'
					END	[DataType],
				y.[GroupId],
				y.[Hydrogen],
				y.[Methane],
				y.[Acetylene],
				y.[EthylenePG],
				y.[EthyleneCG],
				y.[PropylenePG],
				y.[PropyleneCG],
				y.[PropyleneRG],
				y.[PropaneC3Resid],
				y.[Butadiene],
				y.[IsoButylene],
				y.[C4Oth],
				y.[Benzene],
				y.[PyroGasoline],
				y.[PyroGasOil],
				y.[PyroFuelOil],
				y.[AcidGas],
				y.[PPFC],
				y.[ProdOther1],
				y.[ProdOther2],
				y.[LossFlareVent],
				[LossOther] = y.[LossOther] + y.[LossMeasure]
			FROM [Olefins].[reports].[Yield]		y
			WHERE y.[GroupId] = @GroupId

			UNION ALL

			SELECT
				CASE y.[DataType]
					WHEN '$PerMT'	THEN 'Amount_Cur'
					WHEN 'kMT'		THEN 'Quantity_kMT'
					END	[DataType],
				y.[GroupId],
				y.[Hydrogen],
				y.[Methane],
				y.[Acetylene],
				y.[EthylenePG],
				y.[EthyleneCG],
				y.[PropylenePG],
				y.[PropyleneCG],
				y.[PropyleneRG],
				y.[PropaneC3Resid],
				y.[Butadiene],
				y.[IsoButylene],
				y.[C4Oth],
				y.[Benzene],
				y.[PyroGasoline],
				y.[PyroGasOil],
				y.[PyroFuelOil],
				y.[AcidGas],
				y.[PPFC],
				y.[ProdOther1],
				y.[ProdOther2],
				y.[LossFlareVent],
				[LossOther] = y.[LossOther] + y.[LossMeasure]
			FROM [Olefins13].[reports].[Yield]		y
			WHERE y.[GroupId] = @TargetId
			) p
			UNPIVOT (
				[Value]	FOR [StreamId] IN
				(
				[Hydrogen],
				[Methane],
				[Acetylene],
				[EthylenePG],
				[EthyleneCG],
				[PropylenePG],
				[PropyleneCG],
				[PropyleneRG],
				[PropaneC3Resid],
				[Butadiene],
				[IsoButylene],
				[C4Oth],
				[Benzene],
				[PyroGasoline],
				[PyroGasOil],
				[PyroFuelOil],
				[AcidGas],
				[PPFC],
				[ProdOther1],
				[ProdOther2],
				[LossFlareVent],
				[LossOther]
				)
			) u
			PIVOT (
				MAX(u.[Value]) FOR u.[DataType] IN
				(
					[Quantity_kMT],
					[Amount_Cur]
				)
			) d;

		SET @ProcedureDesc = CHAR(9) + '@ProdSupp Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @ProdSupp			TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[StreamId]			VARCHAR(42)	NOT	NULL	CHECK([StreamId] <> ''),
			[Production_kMT]	REAL		NOT	NULL,
			[Supplemental_kMT]	REAL		NOT	NULL,
			[ProdSupp_kMT]		REAL		NOT	NULL,

			[Amount_Cur]		REAL		NOT	NULL,

			[Production_Cur]	AS [Production_kMT]		* [Amount_Cur] / 1000.0
								PERSISTED	NOT	NULL,
			[Supplemental_Cur]	AS [Supplemental_kMT]	* [Amount_Cur] / 1000.0
								PERSISTED	NOT	NULL,
			[ProdSupp_Cur]		AS [ProdSupp_kMT]		* [Amount_Cur] / 1000.0
								PERSISTED	NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [StreamId] ASC)
		);

		INSERT INTO @ProdSupp([GroupId], [StreamId], [Production_kMT], [Supplemental_kMT], [ProdSupp_kMT], [Amount_Cur])
		SELECT
			d.[GroupId],
			d.[StreamId],
			[Production_kMT]	= p.[Quantity_kMT],
			[Supplemental_kMT]	= d.[Quantity_kMT] -
				CASE d.StreamId
					WHEN 'EthylenePG'	THEN dis.D203
					WHEN 'EthyleneCG'	THEN dis.D202
					WHEN 'PropylenePG'	THEN dis.D214
					WHEN 'PropyleneCG'	THEN dis.D213
					WHEN 'PropyleneRG'	THEN dis.D212
					ELSE 0.0
				END,
			[ProdSupp_kMT]		= d.[Quantity_kMT],
			p.[Amount_Cur]
		FROM @DispositionSupp			d
		INNER JOIN @Production			p
			ON	p.[GroupId]		= d.[GroupId]
			AND	p.[StreamId]	= d.[StreamId]
		INNER JOIN @Disposition			dis
			ON	dis.[GroupId]		= d.[GroupId];

		SET @ProcedureDesc = CHAR(9) + '@ProdSuppAggregate Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @ProdSuppAggregate	TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Production_kMT]		REAL		NOT	NULL,
			[AvgProduction_Cur]		AS 	[Production_Cur] / [Production_kMT] * 1000.0
									PERSISTED	NOT	NULL,
			[Production_Cur]		REAL		NOT	NULL,

			[Supplemental_kMT]		REAL		NOT	NULL,
			[AvgSupplemental_Cur]	AS 	CASE
										WHEN [Supplemental_kMT] <> 0.0
										THEN [Supplemental_Cur] / [Supplemental_kMT] * 1000.0
										ELSE 0.0
										END
									PERSISTED	NOT	NULL,
			[Supplemental_Cur]		REAL		NOT	NULL,

			[ProdPyro_kMT]			AS [Production_kMT] - [Supplemental_kMT]
									PERSISTED	NOT	NULL,
			[ProdPyro_Cur]			AS [Production_Cur] - [Supplemental_Cur]
									PERSISTED	NOT	NULL,
			[ProdPyro_KmtCur]		AS ([Production_Cur] - [Supplemental_Cur]) / ([Production_kMT] - [Supplemental_kMT]) * 1000.0
									PERSISTED	NOT	NULL,

			[ActProdPyro_Val]		AS [Production_Cur] / ([Production_kMT] - [Supplemental_kMT]) * 1000.0
									PERSISTED	NOT	NULL,

			[ProdHvc_kMT]			REAL		NOT	NULL,
			[ProdSupp_kMT]			REAL		NOT	NULL,
			[ProdSupp_Cur]			REAL		NOT	NULL,

			[ProductionPyro_kMT]	AS [Production_kMT] - [ProdSupp_kMT]
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @ProdSuppAggregate([GroupId], [Production_kMT], [Production_Cur], [Supplemental_kMT], [Supplemental_Cur], [ProdHvc_kMT], [ProdSupp_kMT], [ProdSupp_Cur])
		SELECT
			ps.[GroupId],
			[Production_kMT]	= SUM(ps.[Production_kMT]),
			[Production_Cur]	= SUM(ps.[Production_Cur]),

			[Supplemental_kMT]	= SUM(ps.[Supplemental_kMT]),
			[Supplemental_Cur]	= SUM(ps.[Supplemental_Cur]),

			[ProdHvc_kMT]		= SUM(CASE WHEN b.[DescendantId] IS NOT NULL THEN ps.[Supplemental_kMT]
				ELSE 0.0
				END),

			[ProdSupp_kMT]		= SUM(ps.[ProdSupp_kMT]),
			[ProdSupp_Cur]		= SUM(ps.[ProdSupp_Cur])
		FROM @ProdSupp										ps
		LEFT OUTER JOIN [Olefins].[dim].[Stream_Bridge]		b
			ON	b.[FactorSetId]		= @FactorSetId
			AND	b.[StreamId]		= 'ProdHvc'
			AND	b.[DescendantId]	= ps.[StreamId]
		GROUP BY
			ps.[GroupId];	

		END;

		BEGIN

		SET @ProcedureDesc = CHAR(9) + '@CapUtil Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @CapUtil	TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL,

			[GroupProdCap_kMT]		REAL		NOT	NULL,
			[GroupSuppCap_kMT]		REAL		NOT	NULL,
			[GroupPyroCap_kMT]		AS			([GroupProdCap_kMT] - [GroupSuppCap_kMT])
									PERSISTED	NOT	NULL,
			[GroupCapability_Pcnt]	REAL		NOT	NULL,

			[GroupProdCpby_kMT]		AS			([GroupProdCap_kMT] / [GroupCapability_Pcnt] * 100.0)
									PERSISTED	NOT	NULL,
			[GroupPyroCpby_kMT]		AS			(([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0)
									PERSISTED	NOT	NULL,
			[GroupSuppCpby_kMT]		AS			([GroupSuppCap_kMT] / [GroupCapability_Pcnt] * 100.0)
									PERSISTED	NOT	NULL,

			[GroupPyroCpby_Pcnt]	AS			([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / ([GroupProdCap_kMT])
									PERSISTED	NOT	NULL,
			[GroupSuppCpby_Pcnt]	AS			([GroupSuppCap_kMT]) / ([GroupProdCap_kMT])
									PERSISTED	NOT	NULL,

			[GroupOlefinsCpby_kMT]	REAL		NOT	NULL,
			[GroupEthyleneCpby_kMT]	REAL		NOT	NULL,

			[TargetId]				VARCHAR(25)		NULL,
			[BenchProdCap_kMT]		REAL			NULL,
			[BenchPyroCap_kMT]		AS			([BenchProdCap_kMT] - [BenchSuppCap_kMT])
									PERSISTED,
			[BenchSuppCap_kMT]		REAL			NULL,
			[BenchCapability_Pcnt]	REAL			NULL,
			[BenchProdCpby_kMT]		AS			([BenchProdCap_kMT] / [BenchCapability_Pcnt] * 100.0)
									PERSISTED,
			[BenchPyroCpby_kMT]		AS			(([BenchProdCap_kMT] - [BenchSuppCap_kMT]) / [BenchCapability_Pcnt] * 100.0)
									PERSISTED,
			[BenchSuppCpby_kMT]		AS			([BenchSuppCap_kMT] / [BenchCapability_Pcnt] * 100.0)
									PERSISTED,

			[BenchPyroCpby_Pcnt]	AS			([BenchProdCap_kMT] - [BenchSuppCap_kMT]) / ([BenchProdCap_kMT])
									PERSISTED,
			[BenchSuppCpby_Pcnt]	AS			([BenchSuppCap_kMT]) / ([BenchProdCap_kMT])
									PERSISTED,

			[BenchOlefinsCpby_kMT]	REAL			NULL,
			[BenchEthyleneCpby_kMT]	REAL			NULL,

			[HvcProd_Ratio]			AS [GroupProdCap_kMT] / [GroupCapability_Pcnt] / [BenchProdCap_kMT] * [BenchCapability_Pcnt]
									PERSISTED,
			[Olefins_Ratio]			AS [GroupOlefinsCpby_kMT] / [BenchOlefinsCpby_kMT]
									PERSISTED,

			[DiffCapability_Pcnt]	AS ([GroupCapability_Pcnt] - [BenchCapability_Pcnt])
									PERSISTED,
			[EnergyCap_Ratio]		AS ([BenchCapability_Pcnt] - [GroupCapability_Pcnt]) * (([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt]) / ([BenchProdCap_kMT] - [BenchSuppCap_kMT])
									PERSISTED,

			[PyroCapUtil_Ratio]		AS ([GroupCapability_Pcnt] - [BenchCapability_Pcnt]) * 
									(([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0) -- [GroupPyroCpby_kMT]
									/ 
									([BenchProdCap_kMT] - [BenchSuppCap_kMT])    -- BenchPyroCap
									/ 100.0
									PERSISTED,

			[SuppCapUtil_Ratio]		AS CASE WHEN [BenchSuppCap_kMT] <> 0.0 THEN
									([GroupCapability_Pcnt] - [BenchCapability_Pcnt]) * 
									([GroupSuppCap_kMT] / [GroupCapability_Pcnt] * 100.0) -- [GroupSuppCpby_kMT] 
									/ 
									[BenchSuppCap_kMT] 
									/ 100.0
									ELSE
										0.0
									END
									PERSISTED,

			[OlefinsCpby_Ratio]		AS	([BenchOlefinsCpby_kMT] - [GroupOlefinsCpby_kMT]) / [BenchOlefinsCpby_kMT]
									PERSISTED,

			[PyroProd_Ratio]		AS	(
											(([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0)
										-	(([BenchProdCap_kMT] - [BenchSuppCap_kMT]) / [BenchCapability_Pcnt] * 100.0)
										)
										/ 
										(
											([GroupProdCap_kMT] / [GroupCapability_Pcnt] * 100.0)
										-	([BenchProdCap_kMT] / [BenchCapability_Pcnt] * 100.0)
										)
									PERSISTED,

			[SuppProd_Ratio]		AS	(
											(([GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0)
										-	(([BenchSuppCap_kMT]) / [BenchCapability_Pcnt] * 100.0)
										)
										/ 
										(
											([GroupProdCap_kMT] / [GroupCapability_Pcnt] * 100.0)
										-	([BenchProdCap_kMT] / [BenchCapability_Pcnt] * 100.0)
										)
									PERSISTED,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @CapUtil
		(
			[GroupId], [GroupProdCap_kMT], [GroupSuppCap_kMT], [GroupCapability_Pcnt], [GroupOlefinsCpby_kMT], [GroupEthyleneCpby_kMT],
			[TargetId], [BenchProdCap_kMT], [BenchSuppCap_kMT], [BenchCapability_Pcnt], [BenchOlefinsCpby_kMT], [BenchEthyleneCpby_kMT]
		)
		SELECT
			gc.[GroupId],
			gc.[HvcProd_kMT],
			COALESCE(gs.[ProdHvc_kMT], 0.0),
			gc.[OlefinsCpbyUtil_pcnt],
			gc.[OlefinsCpbyAvg_kMT],
			gc.[EthyleneCpbyAvg_kMT],

			bc.[GroupId],
			bc.[HvcProd_kMT],
			COALESCE(bs.[ProdHvc_kMT], 0.0),
			bc.[OlefinsCpbyUtil_pcnt],
			bc.[OlefinsCpbyAvg_kMT],
			bc.[EthyleneCpbyAvg_kMT]

		FROM		[Olefins].[reports].[CapUtil]			gc
		INNER JOIN	[Olefins].[reports].[Pyrolysis]			gp
			ON	gp.[GroupId]	=	gc.[GroupId]
			AND	gp.[DataType]	=	'SuppkMT'
			AND	gp.[Currency]	=	'USD'
			AND	gp.[Scenario]	=	'Basis'
			AND	gp.[SimModelId]	=	'Plant'
		LEFT OUTER JOIN @ProdSuppAggregate					gs
			ON	gs.[GroupId]	=	gc.[GroupId]

		LEFT OUTER JOIN [Olefins13].[reports].[CapUtil]		bc
			ON	bc.[GroupId]	=	@TargetId
			AND	bc.[GroupId]	<>	gc.[GroupId]
		LEFT OUTER JOIN @ProdSuppAggregate					bs
			ON	bs.[GroupId]	=	bc.[GroupId]

		WHERE	gc.[GroupId]	=	@GroupId;

		INSERT INTO @CapUtil
		(
			[GroupId], [GroupProdCap_kMT], [GroupSuppCap_kMT], [GroupCapability_Pcnt], [GroupOlefinsCpby_kMT], [GroupEthyleneCpby_kMT]
		)
		SELECT
			gc.[GroupId],
			gc.[HvcProd_kMT],
			COALESCE(gs.[ProdHvc_kMT], 0.0),
			gc.[OlefinsCpbyUtil_pcnt],
			gc.[OlefinsCpbyAvg_kMT],
			gc.[EthyleneCpbyAvg_kMT]

		FROM		[Olefins13].[reports].[CapUtil]			gc
		INNER JOIN	[Olefins13].[reports].[Pyrolysis]		gp
			ON	gp.[GroupId]	=	gc.[GroupId]
			AND	gp.[DataType]	=	'SuppkMT'
			AND	gp.[Currency]	=	'USD'
			AND	gp.[Scenario]	=	'Basis'
			AND	gp.[SimModelId]	=	'Plant'
		LEFT OUTER JOIN @ProdSuppAggregate					gs
			ON	gs.[GroupId]	=	gc.[GroupId]
		WHERE	gc.[GroupId]	=	@TargetId;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9		->	 934 (GAM!B34)	-> Pyrolysis Capacity (GAM!B13)(Insert Complete)';			--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F10		->	1046 (GAM!B46)	-> Supplemental Capacity (Insert Complete)';				--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F12:G12	->	1200			-> Energy Prices (Insert Complete)';						--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F15		->	1500			-> Energy Intensity (Insert Complete)';						--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F48:G49	->	4700			-> Energy Efficiency and Mix (Insert Complete)';
		PRINT @ProcedureDesc;

		BEGIN

		DECLARE @Energy	TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Currency]				VARCHAR(4)	NOT	NULL	CHECK([Currency] <> ''),
			[Scenario]				VARCHAR(24)	NOT	NULL	CHECK([Scenario] <> ''),
			[AccountId]				VARCHAR(24)	NOT	NULL	CHECK([AccountId] <> ''),
			[Amount_Cur]			REAL		NOT	NULL,
			[Energy_MBTU]			REAL		NOT	NULL,
			[Energy_MBtuHvc]		REAL		NOT	NULL,

			[Rate_CurMBtu]			AS CONVERT(REAL,
									CASE
									WHEN [Energy_MBTU] = 0.0
									THEN
										0.0
									ELSE
										[Amount_Cur] / [Energy_MBTU] * 1000.0
									END)
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC, [Currency] ASC, [AccountId] ASC)
		);

		INSERT INTO @Energy([GroupId], [Currency], [Scenario], [AccountId], [Amount_Cur], [Energy_MBTU], [Energy_MBtuHvc])
		SELECT
			o.[GroupID],
			o.[Currency],
			eAdj.[Scenario],
			cur.[AccountId],
			cur.[Amount_Cur],
			egy.[Energy_MBTU],
			hvc.[Energy_MBTU]
		FROM		[Olefins].[reports].[OPEX]					o
		INNER JOIN	[Olefins].[reports].[EnergyRaw]				eAdj
			ON	eAdj.[GroupID]		= o.[GroupID]
			AND	eAdj.[DataType]		= 'MBTU'

		INNER JOIN	[Olefins].[reports].[EnergyRaw]				eHvc
			ON	eHvc.[GroupID]		= o.[GroupID]
			AND	eHvc.[DataType]		= 'kBtuPerHVCMt'
			AND	eHvc.[Scenario]		= eAdj.[Scenario]

		CROSS APPLY (VALUES
			('PurElec',	o.[PurElec]),
			('PurSteam', o.[PurSteam]),
			('PurFuelGas', o.[PurFuelGas] + o.[PurLiquid] + o.[PPFCFuelGas] + o.[PPFCOther]),
			('ExpSteam', o.[ExportSteam]),
			('ExpElectric', o.[ExportElectric])
			) cur([AccountId], [Amount_Cur])

		CROSS APPLY (VALUES
			('PurElec', eAdj.[PurElec]),
			('PurSteam', eAdj.[PurSteamSHP] + eAdj.[PurSteamHP] + eAdj.[PurSteamIP] + eAdj.[PurSteamLP]),
			('PurFuelGas',  eAdj.[PurFuelNatural] + eAdj.[PurOther] + eAdj.[PPFC]),
			('ExpSteam', eAdj.[ExportSteamSHP] + eAdj.[ExportSteamHP] + eAdj.[ExportSteamIP] + eAdj.[ExportSteamLP]),
			('ExpElectric', eAdj.[ExportElectric])
			) egy([EnergyId], [Energy_MBTU])

		CROSS APPLY (VALUES
			('PurElec', eHvc.[PurElec]),
			('PurSteam', eHvc.[PurSteamSHP] + eHvc.[PurSteamHP] + eHvc.[PurSteamIP] + eHvc.[PurSteamLP]),
			('PurFuelGas',  eHvc.[PurFuelNatural] + eHvc.[PurOther] + eHvc.[PPFC]),
			('ExpSteam', eHvc.[ExportSteamSHP] + eHvc.[ExportSteamHP] + eHvc.[ExportSteamIP] + eHvc.[ExportSteamLP]),
			('ExpElectric', eHvc.[ExportElectric])
			) hvc([EnergyId], [Energy_MBTU])

		WHERE	cur.[AccountId]	= egy.[EnergyId]
			AND	cur.[AccountId]	= hvc.[EnergyId]
			AND	o.[GroupID]		= @GroupId
			AND	o.[DataType]	= 'ADJ';

		INSERT INTO @Energy([GroupId], [Currency], [Scenario], [AccountId], [Amount_Cur], [Energy_MBTU], [Energy_MBtuHvc])
		SELECT
			o.[GroupID],
			o.[Currency],
			eAdj.[Scenario],
			cur.[AccountId],
			cur.[Amount_Cur],
			egy.[Energy_MBTU],
			hvc.[Energy_MBTU]
		FROM		[Olefins13].[reports].[OPEX]				o
		INNER JOIN	[Olefins13].[reports].[EnergyRaw]			eAdj
			ON	eAdj.[GroupID]		= o.[GroupID]
			AND	eAdj.[DataType]		= 'MBTU'

		INNER JOIN	[Olefins13].[reports].[EnergyRaw]			eHvc
			ON	eHvc.[GroupID]		= o.[GroupID]
			AND	eHvc.[DataType]		= 'kBtuPerHVCMt'
			AND	eHvc.[Scenario]		= eAdj.[Scenario]

		CROSS APPLY (VALUES
			('PurElec',	o.[PurElec]),
			('PurSteam', o.[PurSteam]),
			('PurFuelGas', o.[PurFuelGas] + o.[PurLiquid] + o.[PPFCFuelGas] + o.[PPFCOther]),
			('ExpSteam', o.[ExportSteam]),
			('ExpElectric', o.[ExportElectric])
			) cur([AccountId], [Amount_Cur])

		CROSS APPLY (VALUES
			('PurElec', eAdj.[PurElec]),
			('PurSteam', eAdj.[PurSteamSHP] + eAdj.[PurSteamHP] + eAdj.[PurSteamIP] + eAdj.[PurSteamLP]),
			('PurFuelGas',  eAdj.[PurFuelNatural] + eAdj.[PurOther] + eAdj.[PPFC]),
			('ExpSteam', eAdj.[ExportSteamSHP] + eAdj.[ExportSteamHP] + eAdj.[ExportSteamIP] + eAdj.[ExportSteamLP]),
			('ExpElectric', eAdj.[ExportElectric])
			) egy([EnergyId], [Energy_MBTU])

		CROSS APPLY (VALUES
			('PurElec', eHvc.[PurElec]),
			('PurSteam', eHvc.[PurSteamSHP] + eHvc.[PurSteamHP] + eHvc.[PurSteamIP] + eHvc.[PurSteamLP]),
			('PurFuelGas',  eHvc.[PurFuelNatural] + eHvc.[PurOther] + eHvc.[PPFC]),
			('ExpSteam', eHvc.[ExportSteamSHP] + eHvc.[ExportSteamHP] + eHvc.[ExportSteamIP] + eHvc.[ExportSteamLP]),
			('ExpElectric', eHvc.[ExportElectric])
			) hvc([EnergyId], [Energy_MBTU])

		WHERE	cur.[AccountId]	= egy.[EnergyId]
			AND	cur.[AccountId]	= hvc.[EnergyId]
			AND	o.[GroupID]		= @TargetId
			AND	o.[DataType]	= 'ADJ';

		DECLARE @EnergyAdj	TABLE
		(
			[DataType]				VARCHAR(4)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Currency]				VARCHAR(4)	NOT	NULL	CHECK([Currency] <> ''),
			[AccountId]				VARCHAR(24)	NOT	NULL	CHECK([AccountId] <> ''),
			[Amount_Cur]			REAL		NOT	NULL,
			[Energy_MBTU]			REAL		NOT	NULL,
			[Rate_CurMBtu]			REAL		NOT	NULL,

			[EEI_PYPS]				REAL		NOT	NULL,
			[NetEnergyCons_MBTU]	REAL		NOT	NULL,
			[StmEnergyCons_MBTU]	REAL		NOT	NULL,	/*	Stream Energy Consumption	*/

			[StreamAmount_Cur]		AS CONVERT(REAL, [Amount_Cur]	* [StmEnergyCons_MBTU] / [NetEnergyCons_MBTU])
									PERSISTED	NOT	NULL,
			[StreamEnergy_MBTU]		AS CONVERT(REAL, [Energy_MBTU]	* [StmEnergyCons_MBTU] / [NetEnergyCons_MBTU] / 1000.0)
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([DataType] ASC, [GroupId] ASC, [Currency] ASC, [AccountId] ASC)
		);

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Pyro',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			(en.[NetEnergyCons] - (en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0))
		FROM @Energy												ec
		INNER JOIN		[Olefins].[reports].[EnergyRaw]				en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[Olefins].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @GroupId;

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Supp',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0
		FROM @Energy												ec
		INNER JOIN		[Olefins].[reports].[EnergyRaw]				en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[Olefins].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @GroupId;

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Pyro',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			(en.[NetEnergyCons] - (en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0))
		FROM @Energy												ec
		INNER JOIN		[Olefins13].[reports].[EnergyRaw]			en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[Olefins13].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @TargetId;

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Supp',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0
		FROM @Energy												ec
		INNER JOIN		[Olefins13].[reports].[EnergyRaw]			en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[Olefins13].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @TargetId;

		--INSERT INTO [Olefins].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		--SELECT
		--	@FactorSetId,
		--	t.[GroupId],
		--	t.[TargetId],
		--	t.[GroupProdCap_kMT],
		--	t.[BenchProdCap_kMT],
		--	[GapId] = CASE x.[GapId]
		--		WHEN 'EnergyCap'
		--		THEN t.[DataType] + '_' +  x.[GapId]
		--		ELSE x.[GapId]
		--		END,
		--	t.[Currency],
		--	t.[DataType],
		--	SUM(x.[GapAmount_Cur])
		--FROM (
		--	SELECT
		--		cu.[GroupId],
		--		cu.[TargetId],
		--		cu.[GroupProdCap_kMT],
		--		cu.[BenchProdCap_kMT],
		--		gE.[AccountId],
		--		gE.[Currency],
		--		gE.[DataType],

		--		[GroupStreamAmount_Cur]	= gE.[StreamAmount_Cur],	--	Cost
		--		[BenchStreamAmount_Cur]	= bE.[StreamAmount_Cur],	--	Cost

		--		[GroupRate_CurMBtu]		= gE.[Rate_CurMBtu],		--	Price
		--		[BenchRate_CurMBtu]		= bE.[Rate_CurMBtu],		--	Price

		--		[EnergyCap] = 
		--			cu.[BenchCapability_Pcnt] / 100.0 *
		--			CASE gE.DataType
		--			WHEN 'Pyro'
		--			THEN (cu.[BenchPyroCpby_kMT] - cu.[GroupPyroCpby_kMT]) / cu.[BenchPyroCap_kMT]
		--			WHEN 'Supp'
		--			THEN
		--				CASE
		--				WHEN cu.[BenchSuppCap_kMT] = 0.0
		--				THEN 0.0
		--				ELSE (cu.[BenchSuppCpby_kMT] - cu.[GroupSuppCpby_kMT]) / cu.[BenchSuppCap_kMT]
		--				END
		--			END,

		--		[EnergyEff]	= (bE.[EEI_PYPS] / gE.[EEI_PYPS] - 1.0),
	
		--		[EnergyPrc]	=
		--			bE.[StreamEnergy_MBTU] *
		--			CASE gE.[DataType]
		--			WHEN 'Pyro'
		--			THEN (cu.[GroupPyroCap_kMT] / cu.[BenchPyroCap_kMT])
		--			WHEN 'Supp'
		--			THEN
		--				CASE WHEN  cu.[BenchSuppCap_kMT] = 0.0
		--				THEN 0.0
		--				ELSE (cu.[GroupSuppCap_kMT] / cu.[BenchSuppCap_kMT])
		--				END
		--			END,

		--		[EnergyMix]	=
		--			CASE ge.[DataType]
		--			WHEN 'Pyro'
		--			THEN (cu.[GroupPyroCap_kMT] / cu.[BenchPyroCap_kMT])
		--			WHEN 'Supp'
		--			THEN
		--				CASE WHEN cu.[BenchSuppCap_kMT] = 0.0
		--				THEN 0.0
		--				ELSE (cu.[GroupSuppCap_kMT] / cu.[BenchSuppCap_kMT])
		--				END
		--			END
		--			* be.[StmEnergyCons_MBTU]
		--			* (CASE WHEN bE.[StmEnergyCons_MBTU] = 0.0 THEN 0.0 ELSE bE.[StreamEnergy_MBTU] / bE.[StmEnergyCons_MBTU] END 
		--				- CASE WHEN gE.[StmEnergyCons_MBTU] = 0.0 THEN 0.0 ELSE gE.[StreamEnergy_MBTU] / gE.[StmEnergyCons_MBTU] END),

		--		[EnergyInt] =
		--			CASE WHEN gE.[StmEnergyCons_MBTU] = 0.0
		--			THEN 0.0
		--			ELSE (be.[StmEnergyCons_MBTU] / gE.[StmEnergyCons_MBTU])
		--			END
		--			*
		--			CASE gE.[DataType]
		--			WHEN 'Pyro'
		--			THEN (cu.[GroupPyroCap_kMT] / cu.[BenchPyroCap_kMT])
		--			WHEN 'Supp'
		--			THEN
		--				CASE WHEN cu.[BenchSuppCap_kMT] = 0.0
		--				THEN 0.0
		--				ELSE (cu.[GroupSuppCap_kMT] / cu.[BenchSuppCap_kMT])
		--				END
		--			END
		--			 - (bE.[EEI_PYPS] / gE.[EEI_PYPS])

		--	FROM @CapUtil			cu
		--	INNER JOIN @EnergyAdj	gE
		--		ON	gE.[GroupId]	= cu.[GroupId]
		--	INNER JOIN @EnergyAdj	bE
		--		ON	bE.[GroupId]	= cu.[TargetId]
		--		AND	bE.[DataType]	= gE.[DataType]
		--		AND	bE.[AccountId]	= gE.[AccountId]
		--		AND	bE.[Currency]	= gE.[Currency]
		--	WHERE	cu.[TargetId]	IS NOT NULL
		--	) t
		--CROSS APPLY( VALUES
		--	('EnergyCap',	t.[EnergyCap] * t.[BenchStreamAmount_Cur] / 1000.0),
		--	('EnergyEff',	t.[EnergyEff] * t.[GroupStreamAmount_Cur] / 1000.0),
		--	('EnergyPrc',	t.[EnergyPrc] * (t.[BenchRate_CurMBtu] - t.[GroupRate_CurMBtu]) / 1000.0),
		--	('EnergyMix',	t.[EnergyMix] * t.[GroupRate_CurMBtu] / 1000.0),
		--	('EnergyInt',	t.[EnergyInt] * t.[GroupStreamAmount_Cur] / 1000.0)
		--	) x ([GapId], [GapAmount_Cur])
		--GROUP BY
		--	t.[GroupId],
		--	t.[TargetId],
		--	t.[GroupProdCap_kMT],
		--	t.[BenchProdCap_kMT],
		--	x.[GapId],
		--	t.[Currency],
		--	t.[DataType]
		--HAVING SUM(x.[GapAmount_Cur]) IS NOT NULL;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F52:G54	->	5100			-> Other Variable Costs (Insert Complete)'
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	 935 (GAM!B35)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	1047 (GAM!D47)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		BEGIN

		DECLARE @OVC	TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),

			[Prod_kMT]				REAL			NOT	NULL,
	
			[Stream_kMT]			REAL			NOT	NULL,
			[StreamCapUtil_Pcnt]	REAL			NOT	NULL,
			[StreamCapacity_kMT]	AS CONVERT(REAL, [Stream_kMT] / [StreamCapUtil_Pcnt] * 100.0)
									PERSISTED		NOT	NULL,

			[StreamChemicals_Cur]	REAL			NOT	NULL,
			[StreamCatalysts_Cur]	REAL			NOT	NULL,
			[StreamNeUtilCost_Cur]	REAL			NOT	NULL,
			[StreamOtherVol_Cur]	REAL			NOT	NULL,
	
			[Stream_Cur]			AS CONVERT(REAL, [StreamChemicals_Cur] + [StreamCatalysts_Cur] + [StreamNeUtilCost_Cur] + [StreamOtherVol_Cur])
									PERSISTED		NOT	NULL,
			PRIMARY KEY CLUSTERED([GroupId]	ASC, [Currency] ASC, [StreamId] ASC)
		);

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'FreshPyroFeed',
			h.[GroupProdCap_kMT],

			h.[GroupPyroCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! M5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! O5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! P5]
		FROM @CapUtil								h
		INNER JOIN [Olefins].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @GroupId;

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'FreshPyroFeed',
			h.[GroupProdCap_kMT],

			h.[GroupPyroCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! M5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! O5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! P5]
		FROM @CapUtil								h
		INNER JOIN [Olefins13].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @TargetId;

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'SuppTot',
			h.[GroupProdCap_kMT],
			h.[GroupSuppCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! W5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Y5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Z5]
		FROM @CapUtil								h
		INNER JOIN [Olefins].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @GroupId;

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'SuppTot',
			h.[GroupProdCap_kMT],
			h.[GroupSuppCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! W5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Y5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Z5]
		FROM @CapUtil								h
		INNER JOIN [Olefins13].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @TargetId;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F52:G54	->	5100			-> Other Variable Costs (Insert Complete)'
		PRINT @ProcedureDesc;

		--INSERT INTO [Olefins].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		--SELECT
		--	@FactorSetId,
		--	u.[GroupId],
		--	u.[TargetId],
		--	u.[GroupProdCap_kMT],
		--	u.[BenchProdCap_kMT],
		--	u.[AccountId],
		--	u.[Currency],
		--	u.[StreamId],
		--	u.[Gap]
		--FROM (
		--	SELECT
		--		g.[GroupId],
		--		[TargetId] = b.[GroupId],
		--		[GroupProdCap_kMT] = g.[Prod_kMT],
		--		[BenchProdCap_kMT] = b.[Prod_kMT],
		--		g.[Currency],
		--		g.[StreamId],
		--		g.[Prod_kMT],
				
		--		[Chemicals]		=
		--			(
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamChemicals_Cur]	/ b.[Stream_kMT]
		--			END
		--			-
		--			CASE
		--			WHEN g.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE g.[StreamChemicals_Cur]	/ g.[Stream_kMT]
		--			END
		--			)
		--			* g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0,
				
		--		[Catalysts]		=
		--			(
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamCatalysts_Cur]	/ b.[Stream_kMT]
		--			END
		--			-
		--			CASE
		--			WHEN g.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE g.[StreamCatalysts_Cur]	/ g.[Stream_kMT]
		--			END
		--			) * g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0,
				
		--		[NeUtilCost]	=
		--			(
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamNeUtilCost_Cur]	/ b.[Stream_kMT]
		--			END
		--			-
		--			CASE
		--			WHEN g.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE g.[StreamNeUtilCost_Cur]	/ g.[Stream_kMT]
		--			END
		--			)
		--			* g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0,
		--		[OtherVol]		=
		--			(
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamOtherVol_Cur]	/ b.[Stream_kMT]
		--			END
		--			-
		--			CASE
		--			WHEN g.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE g.[StreamOtherVol_Cur]	/ g.[Stream_kMT]
		--			END
		--			)
		--			* g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0
		--	FROM		@OVC		g
		--	INNER JOIN	@OVC		b
		--		ON	b.[StreamId]	= g.[StreamId]
		--		AND	b.[Currency]	= g.[Currency]
		--	WHERE	g.[GroupId]		= @GroupId
		--		AND	b.[GroupId]		= @TargetId
		--	) p
		--	UNPIVOT ([Gap] FOR [AccountId] IN (
		--		[Chemicals],
		--		[Catalysts],
		--		[NeUtilCost],
		--		[OtherVol]
		--		)
		--	) u;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	 935 (GAM!B35)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	1047 (GAM!D47)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		--INSERT INTO [Olefins].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		--SELECT
		--	@FactorSetId,
		--	u.[GroupId],
		--	u.[TargetId],
		--	u.[GroupProdCap_kMT],
		--	u.[BenchProdCap_kMT],
		--	CASE u.[StreamId] 
		--		WHEN 'FreshPyroFeed'	THEN 'Pyro_'
		--		WHEN 'SuppTot'			THEN 'Supp_'
		--		END + u.[AccountId],
		--	u.[Currency],
		--	u.[StreamId],
		--	u.[Gap]
		--FROM (
		--	SELECT
		--		g.[GroupId],
		--		[TargetId] = b.[GroupId],
		--		[GroupProdCap_kMT] = g.[Prod_kMT],
		--		[BenchProdCap_kMT] = b.[Prod_kMT],
		--		g.[Currency],
		--		g.[StreamId],
		--		g.[Prod_kMT],

		--		[Chemicals]		=
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamChemicals_Cur]	/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
		--			END,

		--		[Catalysts]		=
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamCatalysts_Cur]	/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
		--			END,

		--		[NeUtilCost]	=
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamNeUtilCost_Cur]	/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
		--			END,

		--		[OtherVol]		=
		--			CASE
		--			WHEN b.[Stream_kMT] = 0.0
		--			THEN 0.0
		--			ELSE b.[StreamOtherVol_Cur]		/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
		--			END

		--	FROM		@OVC		g
		--	INNER JOIN	@OVC		b
		--		ON	b.[StreamId]	= g.[StreamId]
		--		AND	b.[Currency]	= g.[Currency]
		--	WHERE	g.[GroupId]		= @GroupId
		--		AND	b.[GroupId]		= @TargetId
		--	) p
		--	UNPIVOT ([Gap] FOR [AccountId] IN (
		--		[Chemicals],
		--		[Catalysts],
		--		[NeUtilCost],
		--		[OtherVol]
		--		)
		--	) u;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F19:G22	->	1800			-> Capability Utilization (Insert Complete)';
		PRINT @ProcedureDesc;

		BEGIN

		--	GapAnalysisMatrix!B32	-> @PyroCapUtil
		DECLARE @VarOpex			TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[PyroCapUtil_Energy]	REAL			NOT	NULL,
			[PyroCapUtil_Other]		REAL			NOT	NULL,
			[PyroCapUtil]			AS CONVERT(REAL, [PyroCapUtil_Energy] + [PyroCapUtil_Other])
									PERSISTED		NOT	NULL,
			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [StreamId] ASC, [Currency] ASC)
		);

		INSERT INTO @VarOpex([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamId], [Currency], [PyroCapUtil_Energy], [PyroCapUtil_Other])
		SELECT
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			eo.[StreamId],
			ea.[Currency],
			[GAM!B32] =
				CASE eo.[StreamId]
				WHEN 'FreshPyroFeed'	THEN   SUM(ea.[StreamAmount_Cur] * cu.[EnergyCap_Ratio])	/ 1000.0
				WHEN 'SuppTot'			THEN - SUM(ea.[StreamAmount_Cur] * cu.[SuppCapUtil_Ratio])	/ 1000.0
				END,
			[GAM!B33] =
				CASE eo.[StreamId]
				WHEN 'FreshPyroFeed'	THEN - eo.[Stream_Cur] * cu.[PyroCapUtil_Ratio]
				WHEN 'SuppTot'			THEN - eo.[Stream_Cur] * cu.[SuppCapUtil_Ratio]
				END
		FROM @CapUtil			cu
		INNER JOIN @OVC			eo
			ON	eo.[GroupId]	= cu.[TargetId]
		INNER JOIN @EnergyAdj	ea
			ON	ea.[GroupId]	= cu.[TargetId]
			AND	ea.[Currency]	= eo.[Currency]
		WHERE	ea.[DataType]	= CASE eo.[StreamId]
								WHEN 'FreshPyroFeed'	THEN 'Pyro'
								WHEN 'SuppTot'			THEN 'Supp'
								END
		GROUP BY
			cu.[GroupId],
			cu.[TargetId],
			eo.[StreamId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			cu.[PyroCapUtil_Ratio],
			cu.[SuppCapUtil_Ratio],
			eo.[Stream_Cur],
			ea.[Currency];

		--	GapAnalysisMatrix!B89:E92	-> @ReliabilityOppLoss (Only Percent Elements)
		DECLARE @ReliabilityOppLoss		TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[GroupEthyleneCpby_kMT]	REAL			NOT	NULL,
			[GroupPyroCpby_kMT]		REAL			NOT	NULL,

			[DtLoss_Pcnt]			REAL				NULL,
			[SdLoss_Pcnt]			REAL				NULL,
			[TotLoss_Pcnt]			REAL			NOT	NULL,
			[GapUtilization_Pcnt]	REAL			NOT	NULL,

			[DtLoss_Pcnt_Pcnt]		AS CONVERT(REAL, [DTLoss_Pcnt]	/ [GapUtilization_Pcnt])
									PERSISTED,
			[SdLoss_Pcnt_Pcnt]		AS CONVERT(REAL, [SdLoss_Pcnt]	/ [GapUtilization_Pcnt])
									PERSISTED,
			[TotLoss_Pcnt_Pcnt]		AS CONVERT(REAL, [TotLoss_Pcnt]	/ [GapUtilization_Pcnt])
									PERSISTED		NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC, [AccountId] ASC)
		);

		INSERT INTO @ReliabilityOppLoss([GroupId], [TargetId], [AccountId], [GroupEthyleneCpby_kMT], [GroupPyroCpby_kMT], [DTLoss_Pcnt], [SDLoss_Pcnt], [TotLoss_Pcnt], [GapUtilization_Pcnt])
		SELECT
			p.[GroupId],
			p.[TargetId],
			p.[AccountId],
			p.[GroupEthyleneCpby_kMT],
			p.[GroupPyroCpby_kMT],
			p.[DTLoss_Pcnt],
			p.[SDLoss_Pcnt],
			p.[TotLoss_Pcnt],
			p.[GapUtilization_Pcnt]
		FROM (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				cu.[GroupEthyleneCpby_kMT],
				cu.[GroupPyroCpby_kMT],
				[DataType]		= REPLACE(gRol.[DataType], '_MT', '_Pcnt'),

				[TurnAround]	= bRol.[TurnAround]	/ cu.[BenchEthyleneCpby_kMT] / 10.0 - COALESCE(gRol.[TurnAround], 0.0)	/ cu.[GroupEthyleneCpby_kMT] / 10.0,
				[UnPlanned]		= bRol.[UnPlanned]	/ cu.[BenchEthyleneCpby_kMT] / 10.0 - COALESCE(gRol.[UnPlanned], 0.0)	/ cu.[GroupEthyleneCpby_kMT] / 10.0,

				[GapUtilization_Pcnt] = cu.[GroupCapability_Pcnt] - cu.[BenchCapability_Pcnt]
			FROM @CapUtil												cu
			INNER JOIN [Olefins].[reports].[GapReliabilityOppLoss]		gRol
				ON	gRol.[GroupID]	= cu.[GroupId]
				AND	gRol.[DataType]	IN ('DTLoss_MT', 'SDLoss_MT', 'TotLoss_MT')
			INNER JOIN [Olefins13].[reports].[GapReliabilityOppLoss]	bRol
				ON	bRol.[GroupID]	= cu.[TargetId]
				AND	bRol.[DataType]	= gRol.[DataType]
			WHERE	cu.[TargetId]	IS NOT NULL
			) t
			UNPIVOT (
				[Utilization_Pcnt] FOR [AccountId] IN
				(
					[TurnAround],
					[UnPlanned]
				)
			) u
			PIVOT (
				MAX([Utilization_Pcnt]) FOR [DataType] IN
				(
					[DTLoss_Pcnt],
					[SDLoss_Pcnt],
					[TotLoss_Pcnt]
				)
			) p;

		INSERT INTO @ReliabilityOppLoss([GroupId], [TargetId], [AccountId], [GroupEthyleneCpby_kMT], [GroupPyroCpby_kMT], [DTLoss_Pcnt], [TotLoss_Pcnt], [GapUtilization_Pcnt])
		SELECT
			rol.[GroupId],
			rol.[TargetId],
			'NOC',
			rol.[GroupEthyleneCpby_kMT],
			rol.[GroupPyroCpby_kMT],
			rol.[GapUtilization_Pcnt] - SUM(rol.[TotLoss_Pcnt]),
			rol.[GapUtilization_Pcnt] - SUM(rol.[TotLoss_Pcnt]),
			rol.[GapUtilization_Pcnt]
		FROM @ReliabilityOppLoss	rol
		GROUP BY
			rol.[GroupId],
			rol.[TargetId],
			rol.[GroupEthyleneCpby_kMT],
			rol.[GroupPyroCpby_kMT],
			rol.[GapUtilization_Pcnt];

		--	GapAnalysisMatrix!E89:E92	-> @ReliabilityOppLoss
		DECLARE @GapVarOpex				TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamID]				VARCHAR(48)		NOT	NULL	CHECK([StreamID] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[Gap_DtLoss]			REAL				NULL,
			[Gap_SdLoss]			REAL				NULL,
			[Gap_TotLoss]			REAL			NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC, [StreamID] ASC, [AccountId] ASC)
		);

		INSERT INTO @GapVarOpex([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamID], [Currency], [AccountId], [Gap_DtLoss], [Gap_SdLoss], [Gap_TotLoss])
		SELECT
			rol.[GroupId],
			rol.[TargetId],
			pcu.[GroupProdCap_kMT],
			pcu.[BenchProdCap_kMT],
			pcu.[StreamID],
			pcu.[Currency],
			rol.[AccountId],
			[Gap_DtLoss]	= pcu.[PyroCapUtil] * rol.[DtLoss_Pcnt_Pcnt],
			[Gap_SdLoss]	= pcu.[PyroCapUtil] * rol.[SdLoss_Pcnt_Pcnt],
			[Gap_TotLoss]	= pcu.[PyroCapUtil] * rol.[TotLoss_Pcnt_Pcnt]
		FROM @ReliabilityOppLoss	rol
		INNER JOIN @VarOpex			pcu
			ON	pcu.[GroupId]	= rol.[GroupId]
			AND	pcu.[TargetId]	= rol.[TargetId];

		--	GMSummary!{E,H,K}{156,161,166}	-> @YieldMargins
		DECLARE @TargetCountry	CHAR(3);
		DECLARE @StandardCalc	CHAR(4) = 'slmn';

		SELECT @TargetCountry = t.[CountryID]
		FROM [Olefins].[fact].[TSort] t
		WHERE t.[Refnum] = @GroupId;

		DECLARE @YieldMargins			TABLE
		(
			[DataType]		VARCHAR(4)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]		VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),

			[ProdLoss]		REAL		NOT	NULL,
			[FreshPyroFeed]	REAL		NOT	NULL,
			[FeedSupp]		REAL		NOT	NULL,
			[PlantFeed]		REAL		NOT	NULL,
/**/		[Recycle]		REAL		NOT	NULL,

			[FreshRec]		AS CONVERT(REAL, [FreshPyroFeed] + [Recycle])
							PERSISTED	NOT	NULL,
			[ProdPyro]		AS CONVERT(REAL, [ProdLoss] - [ProdSupp])
							PERSISTED	NOT	NULL,

/**/		[ProdSupp]		REAL		NOT	NULL,

			[_GMPyro]		AS CONVERT(REAL, [ProdLoss] - [ProdSupp] - [FreshPyroFeed] - [Recycle])
							PERSISTED	NOT	NULL,

			[_GMSupp]		AS CONVERT(REAL, [ProdSupp] - [FeedSupp] + [Recycle])
							PERSISTED	NOT	NULL,

			[_GM]			AS CONVERT(REAL, [ProdLoss] - [PlantFeed])
							PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([DataType] ASC, [GroupId] ASC)
		);

		INSERT INTO @YieldMargins([DataType], [GroupId], [FreshPyroFeed], [FeedSupp], [PlantFeed], [ProdLoss], [ProdSupp], [Recycle])
		SELECT
			[DataType]	= @StandardCalc,
			p.[GroupId],
			p.[FreshPyroFeed],
			p.[SuppTot],
			p.[PlantFeed],
			p.[ProdLoss],
			COALESCE(s.[ProdSupp_Cur], 0.0),
			p.[EthRec] + p.[ProRec] + p.[ButRec]
		FROM (
			SELECT
				t.[GroupID],
				t.[StreamId],
				[Value_Cur]	= t.[Quantity_kMT] * t.[Amount_Cur] / 1000.0
			FROM (
				SELECT
					[DataType] =
						CASE y.[DataType]
						WHEN '$PerMT'	THEN 'Amount_Cur'
						WHEN 'kMT'		THEN 'Quantity_kMT'
						END,
					y.[GroupID],
					y.[FreshPyroFeed],
					y.[SuppTot],
					y.[PlantFeed],
					y.[Prod],
					y.[Loss],
					y.[ProdLoss],
					[EthRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppEthane]	ELSE r.[EthRec] END,
					[ProRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppPropane]	ELSE r.[ProRec] END,
					[ButRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppButane]	ELSE r.[ButRec] END
				FROM [Olefins].[reports].[Yield]		y
				INNER JOIN @RecycleValue				r
					ON	r.[GroupId]	= y.[GroupId]
				WHERE	y.[GroupID]	= @GroupId
					AND	y.[DataType] IN ('kMT', '$PerMT')

				UNION ALL

				SELECT
					[DataType] =
						CASE y.[DataType]
						WHEN '$PerMT'	THEN 'Amount_Cur'
						WHEN 'kMT'		THEN 'Quantity_kMT'
						END,
					y.[GroupID],
					y.[FreshPyroFeed],
					y.[SuppTot],
					y.[PlantFeed],
					y.[Prod],
					y.[Loss],
					y.[ProdLoss],
					[EthRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppEthane]	ELSE r.[EthRec] END,
					[ProRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppPropane]	ELSE r.[ProRec] END,
					[ButRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppButane]	ELSE r.[ButRec] END
				FROM [Olefins13].[reports].[Yield]		y
				INNER JOIN @RecycleValue				r
					ON	r.[GroupId]	= y.[GroupId]
				WHERE	y.[GroupID]	= @TargetId
					AND	y.[DataType] IN ('kMT', '$PerMT')
				) p
				UNPIVOT (
					[Value] FOR [StreamId] IN
					(
						[FreshPyroFeed],
						[SuppTot],
						[PlantFeed],
						[Prod],
						[Loss],
						[ProdLoss],
						[EthRec],
						[ProRec],
						[ButRec]
					)
				) a
				PIVOT (
					MAX(a.[Value]) FOR a.[DataType] IN
					(
						[Amount_Cur],
						[Quantity_kMT]
					)
				) t
			) v
			PIVOT (
				MAX(v.[Value_Cur]) FOR v.[StreamId] IN
				(
					[FreshPyroFeed],
					[SuppTot],
					[PlantFeed],
					[Prod],
					[Loss],
					[ProdLoss],
					[EthRec],
					[ProRec],
					[ButRec]
				)
			) p
		LEFT OUTER JOIN @ProdSuppAggregate	s
			ON	s.[GroupId] = p.[GroupID];

		DECLARE @Recycle	REAL;

		SELECT
			@Recycle = COALESCE(SUM(rYield.[Quantity_kMT] * x.[Sel_Calc_Amount_Cur]) / 1000.0, 0.0)
		FROM (
			SELECT
				[GroupId] = pps.[REfnum],
				pps.[StreamID],
				pps.[Avg_Calc_Amount_Cur]
			FROM [Olefins13].[calc].[PricingPlantStreamAggregate]			pps
			WHERE pps.[Refnum]			= @TargetId + ':' + @TargetCountry
				AND pps.[StreamID]		IN ('Ethane', 'DilEthane', 'ROGEthane', 'Propane', 'DilPropane', 'ROGPropane', 'Butane', 'DilButane', 'ROGC4Plus')
				AND	pps.[Quantity_kMT]	> 0.0
			) ppsa
			PIVOT (
				MAX(ppsa.[Avg_Calc_Amount_Cur]) FOR ppsa.[StreamID] IN (
					[Ethane], [DilEthane], [ROGEthane], [Propane], [DilPropane], [ROGPropane], [Butane], [DilButane], [ROGC4Plus]
				)
			) p
		CROSS APPLY( VALUES
			('RecSuppEthane',	COALESCE(p.[Ethane],	p.[DilEthane],	p.[ROGEthane])),
			('RecSuppPropane',	COALESCE(p.[Propane],	p.[DilPropane],	p.[ROGPropane])),
			('RecSuppButane',	COALESCE(p.[Butane],	p.[DilButane],	p.[ROGC4Plus]))
			) x([Rec_StreamID], [Sel_Calc_Amount_Cur])
		INNER JOIN (
			SELECT
				ty.[GroupID],
				ty.[RecSuppEthane],
				ty.[RecSuppPropane],
				ty.[RecSuppButane]
			FROM [Olefins13].[reports].[Yield] tY
			WHERE	tY.[GroupID]	= @TargetId
				AND tY.[DataType]	= 'kMT'
			) u
			UNPIVOT (
				[Quantity_kMT] FOR [Rec_StreamID] IN (
					[RecSuppEthane],
					[RecSuppPropane],
					[RecSuppButane]
				)
			) rYield
			ON	rYield.[Rec_StreamID]	= x.[Rec_StreamID]
			AND	rYield.[Quantity_kMT]	> 0.0;





--		INSERT INTO @YieldMargins([DataType], [GroupId], [FreshPyroFeed], [FeedSupp], [PlantFeed], [ProdLoss], [ProdSupp], [Recycle])
		SELECT
			[DataType]	= @TargetCountry,
			[GroupId]	= @TargetId,
			p.[FreshPyroFeed],
			[SuppTot]	= COALESCE(p.[SuppTot], 0.0),
			p.[PlantFeed],
			p.[ProdLoss],
			[ProdSupp]	= COALESCE(s.[ProdSupp], 0.0),

			[Recycle]	= @Recycle
		FROM (
			SELECT
				[GroupId] = pps.[Refnum],
				pps.[StreamID],
				[Stream_Value_Cur] = pps.[Stream_Value_Cur] / 1000.0
			FROM [Olefins13].[calc].[PricingPlantStreamAggregate]	pps
			WHERE pps.[Refnum] = @TargetId + ':' + @TargetCountry
			AND	pps.[StreamID] IN ('FreshPyroFeed', 'SuppTot', 'PlantFeed', 'ProdLoss')
			) u
			PIVOT (MAX(u.[Stream_Value_Cur]) FOR u.[StreamID] IN(
				[FreshPyroFeed],
				[SuppTot],
				[PlantFeed],
				[ProdLoss]
				)
			) p
		LEFT OUTER JOIN (
			SELECT
				[GroupId]	= pps.[Refnum],
				[ProdSupp]	= SUM(d.[Quantity_kMT] * pps.[Avg_Calc_Amount_Cur]) / 1000.0
			FROM [Olefins13].[calc].[PricingPlantStreamAggregate]			pps
			INNER JOIN @DispositionSupp					d
				ON	d.[GroupId] + ':' + @TargetCountry	= pps.[Refnum]
				AND	d.[StreamId]		= pps.[StreamID]
				AND	d.[Quantity_kMT]	<> 0.0
			WHERE	pps.[Refnum] = @TargetId + ':' + @TargetCountry
			GROUP BY
				pps.[Refnum]
			) s
			ON	s.[GroupId]	= p.[GroupId];

		--	GapAnalysisMatrix!D83,D101	-> @GM
		DECLARE @GM	TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[PyroCapUtil]			REAL			NOT	NULL,
			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [StreamId] ASC, [Currency] ASC)
		);

		INSERT INTO @GM([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamId], [Currency], [PyroCapUtil])
		SELECT
			u.[GroupId],
			u.[TargetId],
			u.[GroupProdCap_kMT],
			u.[BenchProdCap_kMT],
			u.[StreamId],
			'USD',
			u.[PyroCapUtil]
		FROM (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				cu.[GroupProdCap_kMT],
				cu.[BenchProdCap_kMT],
				[FreshPyroFeed]	= ym._GMPyro * cu.PyroCapUtil_Ratio, 
				[SuppTot]		= ym._GMSupp * cu.SuppCapUtil_Ratio
			FROM @CapUtil				cu
			INNER JOIN @YieldMargins	ym
				ON	ym.[GroupId]		= cu.[TargetId]
				AND	ym.[DataType]		= @TargetCountry
			) t
		UNPIVOT (
			[PyroCapUtil] FOR [StreamId] IN (
				[FreshPyroFeed],
				[SuppTot]
			)
		) u;

		DECLARE @GapGM			TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamID]				VARCHAR(48)		NOT	NULL	CHECK([StreamID] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[Gap_DtLoss]			REAL				NULL,
			[Gap_SdLoss]			REAL				NULL,
			[Gap_TotLoss]			REAL			NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC, [StreamID] ASC, [AccountId] ASC)
		);

		--	GapAnalysisMatrix!E89:E100	-> @ReliabilityOppLoss
		INSERT INTO @GapGM([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamID], [Currency], [AccountId], [Gap_DtLoss], [Gap_SdLoss], [Gap_TotLoss])
		SELECT
			rol.[GroupId],
			rol.[TargetId],
			pcu.[GroupProdCap_kMT],
			pcu.[BenchProdCap_kMT],
			pcu.[StreamID],
			'USD',
			rol.[AccountId],
			[Gap_DtLoss]	= pcu.[PyroCapUtil] * rol.[DtLoss_Pcnt_Pcnt],
			[Gap_SdLoss]	= pcu.[PyroCapUtil] * rol.[SdLoss_Pcnt_Pcnt],
			[Gap_TotLoss]	= pcu.[PyroCapUtil] * rol.[TotLoss_Pcnt_Pcnt]
		FROM @ReliabilityOppLoss	rol
		INNER JOIN @GM				pcu
			ON	pcu.[GroupId]	= rol.[GroupId]
			AND	pcu.[TargetId]	= rol.[TargetId];

		--	GapAnalysisMatrix!F89:F92	->
		DECLARE @CapacilityUtilization	TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL				NULL,
			[BenchProdCap_kMT]		REAL				NULL,

			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),

			[GapTot_DtLoss]			REAL				NULL,
			[GapTot_SdLoss]			REAL				NULL,
			[GapTot_TotLoss]		REAL			NOT	NULL,

			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [StreamId] ASC, [AccountId] ASC, [Currency] ASC)
		);

		INSERT INTO @CapacilityUtilization([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamId], [AccountId], [Currency], [GapTot_DtLoss], [GapTot_SdLoss], [GapTot_TotLoss])
		SELECT
			g.[GroupId],
			g.[TargetId],
			v.[GroupProdCap_kMT],
			v.[BenchProdCap_kMT],
			g.[StreamId],
			g.[AccountId],
			v.[Currency],
			[GapTot_DtLoss]		= COALESCE(g.[Gap_DtLoss], 0.0)		+ COALESCE(v.[Gap_DtLoss], 0.0),
			[GapTot_SdLoss]		= COALESCE(g.[Gap_SdLoss], 0.0)		+ COALESCE(v.[Gap_SdLoss], 0.0),
			[GapTot_TotLoss]	= COALESCE(g.[Gap_TotLoss], 0.0)	+ COALESCE(v.[Gap_TotLoss], 0.0)
		FROM @GapGM				g
		INNER JOIN @GapVarOpex	v
			ON	v.[GroupId]		= g.[GroupId]
			AND	v.[TargetId]	= g.[TargetId]
			AND	v.[StreamId]	= g.[StreamId]
			AND	v.[AccountId]	= g.[AccountId];

		--INSERT INTO [Olefins].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		--SELECT
		--	@FactorSetId,
		--	t.[GroupId],
		--	t.[TargetId],
		--	t.[GroupProdCap_kMT],
		--	t.[BenchProdCap_kMT],
		--	t.[AccountId] + '_' + t.[DataType],
		--	t.[Currency],
		--	t.[DataType],
		--	t.[GapAmount_Cur]
		--FROM (
		--	SELECT
		--		cut.[GroupId],
		--		cut.[TargetId],
		--		cut.[GroupProdCap_kMT],
		--		cut.[BenchProdCap_kMT],
		--		cut.[AccountId],
		--		cut.[Currency],
		--		[DT] = SUM(cut.[GapTot_DtLoss]),
		--		[SD] = SUM(cut.[GapTot_SdLoss])
		--	FROM @CapacilityUtilization	cut
		--	GROUP BY
		--		cut.[GroupId],
		--		cut.[TargetId],
		--		cut.[GroupProdCap_kMT],
		--		cut.[BenchProdCap_kMT],
		--		cut.[AccountId],
		--		cut.[Currency]
		--	) u
		--	UNPIVOT (
		--		[GapAmount_Cur] FOR [DataType] IN (
		--			[DT],
		--			[SD]
		--		)
		--	) t;

		END;


