﻿SELECT
	[a].[FactorSetID],
	[a].[Refnum],
	[a].[OpCondID],
	[a].[StreamID],
	[a].[StreamDescription],
	[a].[RecycleID],
	[a].[ComponentID],
	[a].[Component_WtPcnt],
	[-]	= '->',
	[b].[Component_WtPcnt],
	[ErrorAbsolute] = ROUND(ABS([a].[Component_WtPcnt] - [b].[Component_WtPcnt]), 5),
	[ErrorRelative] = ROUND(ABS([a].[Component_WtPcnt] - [b].[Component_WtPcnt]) / [a].[Component_WtPcnt] * 100.0, 2)
	--[b].[FactorSetID],
	--[b].[Refnum],
	--[b].[OpCondID],
	--[b].[StreamID],
	--[b].[StreamDescription],
	--[b].[RecycleID],
	--[b].[ComponentID]
FROM (
	SELECT
		[ycs].*
	FROM
		[sim].[YieldCompositionStream]		[ycs]
	INNER JOIN
		[cons].[TSortSolomon]				[tss]
			ON	[tss].[Refnum]			= [ycs].[Refnum]
			AND	[tss].[FactorSetId]		= [ycs].[FactorSetID]
	INNER JOIN
		[reports].[GroupPlants]				[grp]
			ON	[grp].[Refnum]			= [ycs].[Refnum]
	WHERE	[ycs].[SimModelID]			= 'PYPS'
		AND	[grp].[GroupID]				= '13PCH'
		AND	[ycs].[FactorSetID]			= '2013'
		--AND	[ycs].[Refnum]				= '2013PCH003'
		AND	[ycs].[ComponentId]			= 'C2H4'
		AND	[ycs].[OpCondId]			= 'OSOP'
		AND	[ycs].[StreamID]			IN ('Ethane', 'Propane')
	) a
FULL OUTER JOIN
 (
	SELECT
		[ycs].*
	FROM
		[sim].[Temp_YieldCompositionStream]	[ycs]
	INNER JOIN
		[cons].[TSortSolomon]				[tss]
			ON	[tss].[Refnum]			= [ycs].[Refnum]
			AND	[tss].[FactorSetId]		= [ycs].[FactorSetID]
	INNER JOIN
		[reports].[GroupPlants]				[grp]
			ON	[grp].[Refnum]			= [ycs].[Refnum]
	WHERE	[ycs].[ModelID]				= 'PYPS'
		AND	[grp].[GroupID]				= '13PCH'
		AND	[ycs].[FactorSetID]			= '2013'
		--AND	[ycs].[Refnum]				= '2013PCH003'
		AND	[ycs].[ComponentId]			= 'C2H4'
		AND	[ycs].[OpCondId]			= 'OSOP'
		AND	[ycs].[StreamID]			IN ('Ethane', 'Propane')
	) b
	ON	[b].[FactorSetID]		= [a].[FactorSetID]
	AND	[b].[Refnum]			= [a].[Refnum]
	AND	[b].[ModelId]			= [a].[SimModelID]
	AND	[b].[OpCondId]			= [a].[OpCondID]
	AND	[b].[StreamId]			= [a].[StreamID]
	AND	[b].[StreamDescription]	= [a].[StreamDescription]
	AND	[b].[ComponentId]		= [a].[ComponentID]
	AND	[b].[RecycleId]			= [a].[RecycleId]
--WHERE	ABS([a].[Component_WtPcnt] - [b].[Component_WtPcnt]) / [a].[Component_WtPcnt] > 1.0
--	OR	ABS([a].[Component_WtPcnt] - [b].[Component_WtPcnt]) / [a].[Component_WtPcnt] * 100.0 > 10.0
ORDER BY
	ABS([a].[Component_WtPcnt] - [b].[Component_WtPcnt])	DESC



DECLARE @FactorSetID	VARCHAR(4)	=	'2013';
DECLARE @Refnum			VARCHAR(10)	=	'2013PCH053';
SELECT * FROM sim.PypsSimulationInput(@FactorSetID, @Refnum);
--EXECUTE [sim].[Select_SimulationInputPyps] @FactorSetID, @Refnum;

SELECT * FROM [sim].[Temp_YieldCompositionStream] [ycs] WHERE [ycs].[FactorSetId] = @FactorSetID AND [ycs].[Refnum] = @Refnum
AND	[ycs].[OpCondId]	= 'OS25'
AND	[ycs].[StreamId]	= 'Raffinate';


SELECT * FROM fact.CompositionLiquid l WHERE l.Refnum = '2013PCH053'

/*
DELETE FROM [sim].[Temp_YieldCompositionStream];
DELETE FROM [sim].[Temp_EnergyConsumptionStream];
*/
GO


ALTER PROCEDURE [sim].[Select_List]
AS
BEGIN

	SELECT
		[RowsRemaining] = CONVERT(VARCHAR(5), COUNT(1) OVER() + 1 - ROW_NUMBER() OVER(ORDER BY [sa].[Refnum])),
		[sa].[Refnum],
		[FactorSetId]	= CONVERT(CHAR(4), [sa].[_StudyYear])
	FROM
		[cons].[SubscriptionsAssets]	[sa]
	WHERE	[sa].[CompanyID] <> 'Solomon'
		AND	[sa].[Refnum] LIKE '2013PCH%'
		--AND	[sa].[Refnum] = '2013PCH16B'
	ORDER BY
		[sa].[Refnum] ASC;

	RETURN 0;

END;
GO
