﻿Create VIEW [reports].LTTrends AS
Select
  GroupId = r.GroupId
, RV_MUS				= [$(DbGlobal)].dbo.WtAvg(lt.RV_MUS, 1.0)
, MaintIndexRV			= [$(DbGlobal)].dbo.WtAvg(lt.MaintIndexRV, lt.RV_MUS)
, RelInd				= [$(DbGlobal)].dbo.WtAvg(lt.RelInd, lt.RV_MUS)
FROM reports.GroupPlants r JOIN dbo.LTTrends lt on lt.Refnum=r.Refnum
GROUP by r.GroupId
UNION
Select
  GroupId = lt.Refnum
, RV_MUS
, MaintIndexRV
, RelInd
FROM dbo.LTTrends lt;
