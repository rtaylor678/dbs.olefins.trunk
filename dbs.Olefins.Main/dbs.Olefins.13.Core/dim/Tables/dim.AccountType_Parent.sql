﻿CREATE TABLE [dim].[AccountType_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [AccountTypeId]  VARCHAR (12)        NOT NULL,
    [ParentId]       VARCHAR (12)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_AccountType_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_AccountType_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_AccountType_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_AccountType_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_AccountType_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_AccountType_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [AccountTypeId] ASC),
    CONSTRAINT [CR_AccountType_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_AccountType_Parent_FactorSetLu] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_AccountType_Parent_LookUp_Accounts] FOREIGN KEY ([AccountTypeId]) REFERENCES [dim].[AccountType_LookUp] ([AccountTypeId]),
    CONSTRAINT [FK_AccountType_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[AccountType_LookUp] ([AccountTypeId]),
    CONSTRAINT [FK_AccountType_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[AccountType_Parent] ([FactorSetId], [AccountTypeId])
);


GO

CREATE TRIGGER [dim].[t_AccountType_Parent_u]
ON [dim].[AccountType_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[AccountType_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[AccountType_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[AccountType_Parent].[AccountTypeId]	= INSERTED.[AccountTypeId];

END;