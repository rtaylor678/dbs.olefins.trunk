﻿CREATE TABLE [dim].[FeedSelModel_LookUp] (
    [ModelId]        VARCHAR (42)       NOT NULL,
    [ModelName]      NVARCHAR (84)      NOT NULL,
    [ModelDetail]    NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelModel_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FeedSelModel_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FeedSelModel_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelModel_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedSelModel_LookUp] PRIMARY KEY CLUSTERED ([ModelId] ASC),
    CONSTRAINT [CL_FeedSelModel_LookUp_ModelDetail] CHECK ([ModelDetail]<>''),
    CONSTRAINT [CL_FeedSelModel_LookUp_ModelId] CHECK ([ModelId]<>''),
    CONSTRAINT [CL_FeedSelModel_LookUp_ModelName] CHECK ([ModelName]<>''),
    CONSTRAINT [UK_FeedSelModel_LookUp_ModelDetail] UNIQUE NONCLUSTERED ([ModelDetail] ASC),
    CONSTRAINT [UK_FeedSelModel_LookUp_ModelName] UNIQUE NONCLUSTERED ([ModelName] ASC)
);


GO


CREATE TRIGGER [dim].[t_FeedSelModel_LookUp_u]
	ON [dim].[FeedSelModel_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FeedSelModel_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedSelModel_LookUp].[ModelId]		= INSERTED.[ModelId];
		
END;
