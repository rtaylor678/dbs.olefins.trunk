﻿CREATE TABLE [dim].[SimOpCond_LookUp] (
    [OpCondId]       VARCHAR (12)       NOT NULL,
    [OpCondName]     NVARCHAR (84)      NOT NULL,
    [OpCondDetail]   NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_SimOpCond_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_SimOpCond_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_SimOpCond_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_SimOpCond_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SimOpCond_LookUp] PRIMARY KEY CLUSTERED ([OpCondId] ASC),
    CONSTRAINT [CL_SimOpCond_LookUp_SimOpCondDetail] CHECK ([OpCondDetail]<>''),
    CONSTRAINT [CL_SimOpCond_LookUp_SimOpCondId] CHECK ([OpCondId]<>''),
    CONSTRAINT [CL_SimOpCond_LookUp_SimOpCondName] CHECK ([OpCondName]<>''),
    CONSTRAINT [UK_SimOpCond_LookUp_SimOpCondDetail] UNIQUE NONCLUSTERED ([OpCondDetail] ASC),
    CONSTRAINT [UK_SimOpCond_LookUp_SimOpCondName] UNIQUE NONCLUSTERED ([OpCondName] ASC)
);


GO
CREATE TRIGGER [dim].[t_SimOpCond_LookUp_u]
	ON [dim].[SimOpCond_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[SimOpCond_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[SimOpCond_LookUp].[OpCondId]		= INSERTED.[OpCondId];
		
END;