﻿CREATE ROLE [Datagrp]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Datagrp', @membername = N'DMR';


GO
EXECUTE sp_addrolemember @rolename = N'Datagrp', @membername = N'PRD';


GO
EXECUTE sp_addrolemember @rolename = N'Datagrp', @membername = N'MGV';

