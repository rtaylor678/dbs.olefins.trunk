﻿CREATE TABLE [stgFact].[SimulationSeverity_Pyps] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [SeverityBasis]  VARCHAR (12)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_SimulationSeverity_Pyps_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_SimulationSeverity_Pyps_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_SimulationSeverity_Pyps_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_SimulationSeverity_Pyps_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SimulationSeverity_Pyps] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_SimulationSeverity_Pyps_u]
	ON [stgFact].[SimulationSeverity_Pyps]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[SimulationSeverity_Pyps]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[SimulationSeverity_Pyps].Refnum		= INSERTED.Refnum
		AND	[stgFact].[SimulationSeverity_Pyps].StreamId	= INSERTED.StreamId;

END;