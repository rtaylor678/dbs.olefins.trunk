﻿CREATE PROCEDURE [stgFact].[Delete_Maint]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Maint]
	WHERE [Refnum] = @Refnum;

END;