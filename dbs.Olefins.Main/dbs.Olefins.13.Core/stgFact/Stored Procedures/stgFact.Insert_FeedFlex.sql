﻿CREATE PROCEDURE [stgFact].[Insert_FeedFlex]
(
	@Refnum     VARCHAR (25),
	@FeedProdID VARCHAR (20),

	@PlantCap   REAL         = NULL,
	@ActCap     REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FeedFlex]([Refnum], [FeedProdID], [PlantCap], [ActCap])
	VALUES(@Refnum, @FeedProdID, @PlantCap, @ActCap);

END;