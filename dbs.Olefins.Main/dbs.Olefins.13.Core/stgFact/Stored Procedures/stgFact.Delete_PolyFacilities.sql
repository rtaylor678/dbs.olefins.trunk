﻿CREATE PROCEDURE [stgFact].[Delete_PolyFacilities]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PolyFacilities]
	WHERE [Refnum] = @Refnum;

END;