﻿CREATE PROCEDURE [stgFact].[Delete_PracControls]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracControls]
	WHERE [Refnum] = @Refnum;

END;