﻿CREATE TABLE [Console].[ValidationFiles] (
    [StudyYear]   SMALLINT       NULL,
    [Study]       VARCHAR (3)    NULL,
    [Description] NVARCHAR (100) NULL,
    [Link]        NVARCHAR (100) NULL
);

