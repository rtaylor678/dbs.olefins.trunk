﻿CREATE TABLE [ante].[MapComponentToInput] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [OSIM]           INT                NULL,
    [SPSL]           INT                NULL,
    [PYPS]           INT                NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapComponentToInput_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MapComponentToInput_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MapComponentToInput_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MapComponentToInput_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MapComponentToInput] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_MapComponentToInput_OSIM] CHECK ([OSIM]>=(0)),
    CONSTRAINT [CR_MapComponentToInput_PYPS] CHECK ([PYPS]>=(0)),
    CONSTRAINT [CR_MapComponentToInput_SPSL] CHECK ([SPSL]>=(0)),
    CONSTRAINT [FK_MapComponentToInput_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_MapComponentToInput_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER ante.t_MapComponentToInput_u
ON [ante].[MapComponentToInput]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapComponentToInput]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[MapComponentToInput].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[MapComponentToInput].[ComponentId]	= INSERTED.[ComponentId];

END;
