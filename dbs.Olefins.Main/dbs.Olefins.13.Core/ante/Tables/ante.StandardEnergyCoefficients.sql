﻿CREATE TABLE [ante].[StandardEnergyCoefficients] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [StandardEnergyId] VARCHAR (42)       NOT NULL,
    [Value]            REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyCoefficients_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyCoefficients_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyCoefficients_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyCoefficients_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyCoefficients] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StandardEnergyId] ASC),
    CONSTRAINT [FK_StandardEnergyCoefficients_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergyCoefficients_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId])
);


GO

CREATE TRIGGER [ante].[t_CalcStandardEnergyCoefficients_u]
	ON [ante].[StandardEnergyCoefficients]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [ante].[StandardEnergyCoefficients]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[StandardEnergyCoefficients].FactorSetId			= INSERTED.FactorSetId
		AND	[ante].[StandardEnergyCoefficients].StandardEnergyId	= INSERTED.StandardEnergyId;

END;