﻿CREATE TABLE [ante].[EmissionsStdEnergyDistribution] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [EmissionsId]    VARCHAR (42)       NOT NULL,
    [Energy_Pcnt]    REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsStdEnergyDistribution_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EmissionsStdEnergyDistribution_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EmissionsStdEnergyDistribution_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EmissionsStdEnergyDistribution_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EmissionsStdEnergyDistribution] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EmissionsId] ASC),
    CONSTRAINT [FK_EmissionsStdEnergyDistribution_Emissions_LookUp] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsStdEnergyDistribution_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_EmissionsStdEnergyDistribution_u]
	ON [ante].[EmissionsStdEnergyDistribution]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EmissionsStdEnergyDistribution]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EmissionsStdEnergyDistribution].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[EmissionsStdEnergyDistribution].EmissionsId	= INSERTED.EmissionsId;

END;