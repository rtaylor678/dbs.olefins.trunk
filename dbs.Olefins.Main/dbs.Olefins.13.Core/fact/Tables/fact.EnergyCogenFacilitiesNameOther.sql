﻿CREATE TABLE [fact].[EnergyCogenFacilitiesNameOther] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [FacilityName]   NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyCogenFacilitiesNameOther_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenFacilitiesNameOther_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenFacilitiesNameOther_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenFacilitiesNameOther_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyCogenFacilitiesNameOther] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_EnergyCogenFacilitiesNameOther_FacilityName] CHECK ([FacilityName]<>''),
    CONSTRAINT [FK_EnergyCogenFacilitiesNameOther_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyCogenFacilitiesNameOther_EnergyCogenFacilities] FOREIGN KEY ([Refnum], [FacilityId], [CalDateKey]) REFERENCES [fact].[EnergyCogenFacilities] ([Refnum], [FacilityId], [CalDateKey]),
    CONSTRAINT [FK_EnergyCogenFacilitiesNameOther_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_EnergyCogenFacilitiesNameOther_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergyCogenFacilitiesNameOther_u]
	ON [fact].[EnergyCogenFacilitiesNameOther]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyCogenFacilitiesNameOther]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyCogenFacilitiesNameOther].Refnum		= INSERTED.Refnum
		AND [fact].[EnergyCogenFacilitiesNameOther].FacilityId	= INSERTED.FacilityId
		AND [fact].[EnergyCogenFacilitiesNameOther].CalDateKey	= INSERTED.CalDateKey;

END;