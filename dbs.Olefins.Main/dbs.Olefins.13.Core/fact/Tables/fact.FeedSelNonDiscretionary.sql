﻿CREATE TABLE [fact].[FeedSelNonDiscretionary] (
    [Refnum]             VARCHAR (25)       NOT NULL,
    [CalDateKey]         INT                NOT NULL,
    [StreamId]           VARCHAR (42)       NOT NULL,
    [Affiliated_Pcnt]    REAL               NULL,
    [NonAffiliated_Pcnt] REAL               NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelNonDiscretionary_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_FeedSelNonDiscretionary_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_FeedSelNonDiscretionary_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_FeedSelNonDiscretionary_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSelNonDiscretionary] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FeedSelNonDiscretionary_Affiliated_Pcnt] CHECK ([Affiliated_Pcnt]>=(0.0) AND [Affiliated_Pcnt]<=(100.0)),
    CONSTRAINT [CR_FeedSelNonDiscretionary_NonAffiliated_Pcnt] CHECK ([NonAffiliated_Pcnt]>=(0.0) AND [NonAffiliated_Pcnt]<=(100.0)),
    CONSTRAINT [FK_FeedSelNonDiscretionary_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSelNonDiscretionary_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FeedSelNonDiscretionary_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSelNonDiscretionary_u]
	ON [fact].[FeedSelNonDiscretionary]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSelNonDiscretionary]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSelNonDiscretionary].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSelNonDiscretionary].StreamId	= INSERTED.StreamId
		AND [fact].[FeedSelNonDiscretionary].CalDateKey	= INSERTED.CalDateKey;

END;