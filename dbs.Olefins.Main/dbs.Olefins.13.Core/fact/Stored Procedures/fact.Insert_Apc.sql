﻿CREATE PROCEDURE [fact].[Insert_Apc]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Apc(Refnum, CalDateKey
			-- PracAdv
			, Loop_Count		
			, ClosedLoopAPC_Pcnt
			, ApcMonitorId
			, ApcAge_Years
		
			, OffLineLinearPrograms_Bit
			, OffLineNonLinearSimulators_Bit
			, OffLineNonLinearInput_Bit
			, OnLineClosedLoop_Bit
		
			, OffLineComparison_Days
			, OnLineComparison_Days
			, PriceUpdateFreq_Days
		
			, ClosedLoopModelCokingPred_Bit

			, OptimizationOffLine_Mnths
			, OptimizationPlanFreq_Mnths
			, OptimizationOnLineOper_Mnths
			, OnLineInput_Count
			, OnLineVariable_Count
			, OnLineOperation_Pcnt
			, OnLinePointCycles_Count
		
			, PyroFurnModeledIndependent_Bit
			, OnLineAutoModeSwitch_Bit
		
			-- PracMPC
			, DynamicResponse_Count
			, StepTestSimple_Days
			, StepTestComplex_Days
			, StepTestUpdateFreq_Mnths
			, AdaptiveAlgorithm_Count
			, EmbeddedLP_Count
			, MaxVariablesManipulated_Count
			, MaxVariablesConstraint_Count
			, MaxVariablesControlled_Count
			)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
		
			-- PracAdv
			, a.Tbl9001							-- Loops
			, a.Tbl9002 * 100.0					-- PcntClosedLoop
			, a.Tbl9003							-- ApcMonitorId
			, a.Tbl9004							-- ApcAgeYears

			, etl.ConvBit(a.Tbl9005)
			, etl.ConvBit(a.Tbl9006)
			, etl.ConvBit(a.Tbl9007)
			, etl.ConvBit(a.Tbl9008)

			, a.Tbl9009
			, a.Tbl9009a

			, CASE WHEN t.StudyYear = 1997 THEN a.Tbl9010 END		[PriceUpdateFreqDays]
			, CASE WHEN t.StudyYear <> 1997 THEN
					etl.ConvBit(a.Tbl9010)
				END													[ClosedLoopModelCokingPred]

			, a.Tbl9011
			, a.Tbl9012
			, a.Tbl9013
			, a.Tbl9014
			, a.Tbl9015
			, a.Tbl9016 * 100.0
			, a.Tbl9017
		
			, etl.ConvBit(a.Tbl9018)
			, etl.ConvBit(a.Tbl9019)
		
			-- PracMPC
			, p.Tbl9029
			, p.Tbl9030
			, p.Tbl9031
			, p.Tbl9032
			, p.Tbl9033
			, p.Tbl9034
			, p.Tbl9035
			, p.Tbl9036
			, p.Tbl9037
		
		FROM stgFact.PracAdv a
		INNER JOIN stgFact.PracMPC p ON p.Refnum = a.Refnum
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;