﻿CREATE TABLE [dbo].[Yield] (
    [Refnum]         VARCHAR (25)  NOT NULL,
    [DataType]       VARCHAR (8)   NOT NULL,
    [Ethane]         REAL          NULL,
    [EPMix]          REAL          NULL,
    [Propane]        REAL          NULL,
    [LPG]            REAL          NULL,
    [Butane]         REAL          NULL,
    [FeedLtOther]    REAL          NULL,
    [NaphthaLt]      REAL          NULL,
    [NaphthaFr]      REAL          NULL,
    [NaphthaHv]      REAL          NULL,
    [Condensate]     REAL          NULL,
    [HeavyNGL]       REAL          NULL,
    [Raffinate]      REAL          NULL,
    [Diesel]         REAL          NULL,
    [GasOilHv]       REAL          NULL,
    [GasOilHt]       REAL          NULL,
    [FeedLiqOther]   REAL          NULL,
    [FreshPyroFeed]  REAL          NULL,
    [ConcEthylene]   REAL          NULL,
    [ConcPropylene]  REAL          NULL,
    [ConcButadiene]  REAL          NULL,
    [ConcBenzene]    REAL          NULL,
    [ROGHydrogen]    REAL          NULL,
    [ROGMethane]     REAL          NULL,
    [ROGEthane]      REAL          NULL,
    [ROGEthylene]    REAL          NULL,
    [ROGPropylene]   REAL          NULL,
    [ROGPropane]     REAL          NULL,
    [ROGC4Plus]      REAL          NULL,
    [ROGInerts]      REAL          NULL,
    [DilHydrogen]    REAL          NULL,
    [DilMethane]     REAL          NULL,
    [DilEthane]      REAL          NULL,
    [DilEthylene]    REAL          NULL,
    [DilPropane]     REAL          NULL,
    [DilPropylene]   REAL          NULL,
    [DilButane]      REAL          NULL,
    [DilButylene]    REAL          NULL,
    [DilButadiene]   REAL          NULL,
    [DilBenzene]     REAL          NULL,
    [DilMogas]       REAL          NULL,
    [SuppWashOil]    REAL          NULL,
    [SuppGasOil]     REAL          NULL,
    [SuppOther]      REAL          NULL,
    [SuppTot]        REAL          NULL,
    [PlantFeed]      REAL          NULL,
    [Hydrogen]       REAL          NULL,
    [Methane]        REAL          NULL,
    [Acetylene]      REAL          NULL,
    [EthylenePG]     REAL          NULL,
    [EthyleneCG]     REAL          NULL,
    [PropylenePG]    REAL          NULL,
    [PropyleneCG]    REAL          NULL,
    [PropyleneRG]    REAL          NULL,
    [PropaneC3Resid] REAL          NULL,
    [Butadiene]      REAL          NULL,
    [IsoButylene]    REAL          NULL,
    [C4Oth]          REAL          NULL,
    [Benzene]        REAL          NULL,
    [PyroGasoline]   REAL          NULL,
    [PyroGasOil]     REAL          NULL,
    [PyroFuelOil]    REAL          NULL,
    [AcidGas]        REAL          NULL,
    [PPFC]           REAL          NULL,
    [ProdOther]      REAL          NULL,
    [Prod]           REAL          NULL,
    [LossFlareVent]  REAL          NULL,
    [LossOther]      REAL          NULL,
    [LossMeasure]    REAL          NULL,
    [Loss]           REAL          NULL,
    [ProdLoss]       REAL          NULL,
    [FeedProdLoss]   REAL          NULL,
    [FeedLiqOther1]  REAL          NULL,
    [FeedLiqOther2]  REAL          NULL,
    [FeedLiqOther3]  REAL          NULL,
    [ProdOther1]     REAL          NULL,
    [ProdOther2]     REAL          NULL,
    [RecSuppEthane]  REAL          NULL,
    [RecSuppPropane] REAL          NULL,
    [RecSuppButane]  REAL          NULL,
    [Recycle]        REAL          NULL,
    [tsModified]     SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Yield] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC) WITH (FILLFACTOR = 70)
);

