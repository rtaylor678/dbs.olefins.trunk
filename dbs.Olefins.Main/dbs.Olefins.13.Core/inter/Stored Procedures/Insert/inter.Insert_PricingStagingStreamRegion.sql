﻿CREATE PROCEDURE inter.Insert_PricingStagingStreamRegion
(
	@FactorSetId			VARCHAR(42) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingStreamRegion (ASIA, EUR, NA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  u.FactorSetId										[FactorSetId]
			, u.RegionId										[RegionId]
			, u.CalDateKey										[CalDateKey]
			, 'USD'												[CurrencyRpt]
			, u.StreamId
			, u.Amount_Cur										[Amount_Cur]
			, CASE u.StreamId
				WHEN 'ConcButadiene'	THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.02224	ELSE 0.02497	END
				WHEN 'DilButadiene'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.02224	ELSE 0.02497	END

				WHEN 'ConcBenzene'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.05576	ELSE 0.06256	END
				WHEN 'ConcEthylene'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.05576	ELSE 0.06256	END

				WHEN 'DilButylene'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.03320	ELSE 0.03725	END
				WHEN 'ConcPropylene'	THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END

				WHEN 'DilButane'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'DilEthane'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'DilPropane'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'SuppOther'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'DilMoGas'			THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				END												[Adj_MatlBal_Cur]

		FROM (
			SELECT
				  l.FactorSetId
				, CONVERT(INT, CONVERT(CHAR(4), t.[Year]) +
					CASE t.PeriodID
						WHEN 'Qtr1' THEN '0331'
						WHEN 'Qtr2' THEN '0630'
						WHEN 'Qtr3' THEN '0930'
						WHEN 'Qtr4' THEN '1231'
					END)										[CalDateKey]
				, CASE t.Region
					WHEN 'Asia'	THEN 'ASIA'
					WHEN 'NWE'	THEN 'EUR'
					WHEN 'USGC'	THEN 'NA'
					END											[RegionId]

				-- Fresh Pyro Feed
				, t.Ethane										[Ethane]
				, t.Propane										[Propane]
				, t.LPG											[LPG]
				, t.MIXEDBUTANES								[Butane]
				, t.LtNaphtha									[LiqC5C6]
				, t.Raffinate									[Raffinate]
				, t.REFORM_NAPHTHA								[Naphtha]
				, t.ATMO_GASOIL									[Diesel]
				, t.GasOil										[GasOilHv]
				, t.ATMO_GASOIL									[GasOilHt]
				, t.NRC											[Condensate]

				-- Products
				, t.Hydrogen									[Hydrogen]
				, t.Acetylene									[Acetylene]
				, t.Ethylene									[Ethylene]
				, t.Propylene									[PropylenePG]
				, t.PropyleneCG									[PropyleneCG]
				, t.PROPYLENE_REFY								[PropyleneRG]
				, t.Butadiene									[Butadiene]
				, t.Benzene										[Benzene]
				, t.PYROGASOLINE_HY								[PyroGasOilHt]
				, t.PYROGASOLINE_UT								[PyroGasOilUt]

				-- Errata
				, t.METHANE										[Methane]
				, t.NATURAL_GAS									[PricingAdj]

				-- Stream Calcs (Not Reported)
				, t.Benzene										[ConcBenzene]

				, t.PYROGASOLINE_HY								[DilBenzene]
				, t.Butadiene									[DilButadiene]
				, t.MIXEDBUTANES								[DilButane]
				, t.BUTYLENE_ALKY								[DilButylene]
				, t.Ethane										[DilEthane]

				, t.PYROGASOLINE_UT								[DilMoGas]
	
				-- Plant Calcs (Not Reported)
				, t.Ethylene									[ConcEthylene]
				, t.PROPYLENE_REFY								[ConcPropylene]
				, t.Butadiene									[ConcButadiene]
				, t.Propane										[DilPropane]

				, t.Butane										[C4Oth]
				, t.HeavyNGL									[HeavyNGL]
				, t.BUTYLENE_ALKY								[IsoButylene]
				, t.PyFuelOil									[PyroFuelOil]
				, t.PyGasoil									[PyroGasOil]

				, t.PyFuelOil									[SuppGasOil]
				, t.PyGasoil									[SuppWashOil]

				, t.EPMix										[EPMix]

				-- Product Streams
				, t.Ethylene									[EthylenePG]
				, t.Ethylene * 0.95								[EthyleneCG]

				, t.Ethane										[PPFCEthane]
				, t.Propane										[PPFCPropane]
				, t.MIXEDBUTANES								[PPFCButane]
				, t.PYROGASOLINE_UT								[PPFCPyroNaphtha]
				, t.PyGasoil									[PPFCPyroGasOil]
				, t.PyFuelOil									[PPFCPyroFuelOil]

			FROM stgFact.Pricing				t
			INNER JOIN dim.FactorSet_LookUp		l
				ON	l.[FactorSetYear] = t.[Year]
			WHERE l.FactorSetId = @FactorSetId
			) p
			UNPIVOT(
			Amount_Cur FOR StreamId IN (
				-- Fresh Pyro Feed
				  [Ethane]
				, [Propane]
				, [LPG]
				, [Butane]
				, [LiqC5C6]
				, [Raffinate]
				, [Naphtha]
				, [Diesel]
				, [GasOilHv]
				, [GasOilHt]
				, [Condensate]

				-- Products
				, [Hydrogen]
				, [Acetylene]
				, [Ethylene]
				, [PropylenePG]
				, [PropyleneCG]
				, [PropyleneRG]
				, [Butadiene]
				, [Benzene]
				, [PyroGasOilHt]
				, [PyroGasOilUt]

				-- Errata
				, [Methane]
				, [PricingAdj]

				-- Stream Calcs (Not Reported)
				, [ConcBenzene]

				, [DilBenzene]
				, [DilButadiene]
				, [DilButane]
				, [DilButylene]
				, [DilEthane]

				, [DilMoGas]

				-- Plant Calcs (Not Reported)
				, [ConcEthylene]
				, [ConcPropylene]
				, [ConcButadiene]
				, [DilPropane]

				, [C4Oth]
				, [HeavyNGL]
				, [IsoButylene]
				, [PyroFuelOil]
				, [PyroGasOil]
				, [SuppGasOil]
				, [SuppWashOil]

				, [EPMix]

				-- Product Streams
				, [EthylenePG]
				, [EthyleneCG]

				, [PPFCEthane]
				, [PPFCPropane]
				, [PPFCButane]
				, [PPFCPyroNaphtha]
				, [PPFCPyroGasOil]
				, [PPFCPyroFuelOil]
				)
			) u
		WHERE NOT (u.StreamId = 'Diesel' AND u.CalDateKey >= 20090000);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingStreamRegion (MEA: Methane, Ethane, EPMix)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur)
		SELECT
			  l.FactorSetId
			, 'MEA'
			, c.CalDateKey
			, 'USD'
			, p.StreamId
			, p.Amount_Cur
		FROM dim.FactorSet_LookUp			l
		INNER JOIN dim.Calendar_LookUp		c
			ON	c.CalYear = l.[FactorSetYear]
		, (
		SELECT
			  55.0	[Methane]
			, 70.0	[Ethane]
			, 70.0	[EPMix]
			, 70.0	[DilEthane]
			, 70.0	[PPFCEthane]
			) u
			UNPIVOT (
			Amount_Cur FOR StreamId IN (
				  Methane
				, Ethane
				, EPMix
				, DilEthane
				, PPFCEthane
				)
			) p
		WHERE l.FactorSetId = @FactorSetId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, 'N/A';

		RETURN ERROR_NUMBER();

	END CATCH;

END;