﻿CREATE PROCEDURE [inter].[Delete_Pricing]
(
	@FactorSetId			VARCHAR(12) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		EXECUTE [inter].[Delete_PricingBasisCompositionRegion]		@FactorSetId;
		EXECUTE [inter].[Delete_PricingBasisStreamRegion]			@FactorSetId;
		EXECUTE [inter].[Delete_PricingCompositionCountry]			@FactorSetId;
		EXECUTE [inter].[Delete_PricingCompositionLocation]			@FactorSetId;
		EXECUTE [inter].[Delete_PricingCompositionRegion]			@FactorSetId;
		EXECUTE [inter].[Delete_PricingPrimaryCompositionCountry]	@FactorSetId;
		EXECUTE [inter].[Delete_PricingPrimaryCompositionRegion]	@FactorSetId;
		EXECUTE [inter].[Delete_PricingPrimaryStreamCountry]		@FactorSetId;
		EXECUTE [inter].[Delete_PricingPrimaryStreamRegion]			@FactorSetId;
		EXECUTE [inter].[Delete_PricingSecondaryStreamCountry]		@FactorSetId;
		EXECUTE [inter].[Delete_PricingSecondaryStreamRegion]		@FactorSetId;
		EXECUTE [inter].[Delete_PricingStagingCompositionRegion]	@FactorSetId;
		EXECUTE [inter].[Delete_PricingStagingStreamRegion]			@FactorSetId;
		EXECUTE [inter].[Delete_PricingStreamCountry]				@FactorSetId;
		EXECUTE [inter].[Delete_PricingStreamLocation]				@FactorSetId;
		EXECUTE [inter].[Delete_PricingStreamRegion]				@FactorSetId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, 'N/A';

		RETURN ERROR_NUMBER();

	END CATCH;

END;