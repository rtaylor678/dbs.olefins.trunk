﻿CREATE PROCEDURE [cons].[Insert_Assets_Polymer]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO cons.Assets(AssetIdPri, AssetIdSec, AssetName, AssetDetail, CountryId)
		SELECT DISTINCT
			  etl.ConvAssetPCH(f.Refnum)				[AssetIdPri]
			, 'T' + CONVERT(VARCHAR(2), f.PlantId)		[AssetIdSec]
			, MIN(f.Name)
			, MIN(f.Name + ISNULL(' (' + f.City, '') + CASE WHEN LEN(f.[State]) >= 1 THEN ISNULL(', ' + f.[State], '') ELSE '' END + CASE WHEN LEN(f.City) >= 2 THEN ')' ELSE '' END)
			, MIN(etl.ConvCountryID(f.Refnum, ISNULL(f.Country, t.PlantCountry)))
		FROM stgFact.PolyFacilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE f.Name <> '0'
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			AND etl.ConvAssetPCH(f.Refnum) + '-' + 'T' + CONVERT(VARCHAR(2), f.PlantId)	NOT IN (SELECT AssetId FROM cons.Assets)
		GROUP BY
			  etl.ConvAssetPCH(f.Refnum)
			, 'T' + CONVERT(VARCHAR(2), f.PlantId);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;