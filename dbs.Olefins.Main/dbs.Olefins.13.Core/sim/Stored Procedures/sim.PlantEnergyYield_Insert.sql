﻿CREATE PROCEDURE [sim].[PlantEnergyYield_Insert]
(
	@FactorSetId				VARCHAR(12)		= NULL,
	@Refnum						VARCHAR(25)		= NULL,
	@SimModelId					VARCHAR(12)		= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @RecycleId	TINYINT;

	SELECT @RecycleId = pr.RecycleId
	FROM calc.PlantRecycles	pr
	WHERE	pr.FactorSetId = @FactorSetId
		AND	pr.Refnum = @Refnum;

	IF @SimModelId = 'SPSL'
	BEGIN

	-------------------------------------------------------------------
	--	Recycle Handler
	
	DECLARE @iRecycle TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
		[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
		[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
		[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),
		[StreamId]				VARCHAR (42)	NOT	NULL	CHECK(LEN([StreamId]) > 0),

		[FreshPyroFeed_kMT]		FLOAT (53)		NOT	NULL	CHECK([FreshPyroFeed_kMT] >= 0.0),
		[FeedBalance_kMT]		FLOAT (53)		NOT	NULL	CHECK([FeedBalance_kMT] >= 0.0),
		[AggregateRecycle_kMT]	FLOAT (53)		NOT	NULL	CHECK([AggregateRecycle_kMT] >= 0.0)	DEFAULT(0.0),
		[Recycle_kMT]			FLOAT (53)		NOT	NULL	CHECK([Recycle_kMT] >= 0.0)		DEFAULT(0.0),

		CHECK([FeedBalance_kMT]			<= [FreshPyroFeed_kMT]),
		CHECK([AggregateRecycle_kMT]	<= [FreshPyroFeed_kMT]),
		PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [OpCondId] ASC, [StreamId] ASC)
	);

	INSERT INTO @iRecycle(FactorSetId, Refnum, CalDateKey, OpCondId, StreamId, FreshPyroFeed_kMT, FeedBalance_kMT)
	SELECT
		pr.FactorSetId,
		q.Refnum,
		MAX(q.CalDateKey),
		l.OpCondId,
		ro.StreamId,
		SUM(q.Quantity_kMT)	[FreshPyroFeed_kMT],
		SUM(q.Quantity_kMT)	[Balance_kMT]
	FROM fact.Quantity								q
	INNER JOIN calc.PlantRecycles					pr
		ON	pr.Refnum = q.Refnum
	INNER JOIN ante.RecycleOperation				ro
		ON	ro.FactorSetId = pr.FactorSetId
		AND	ro.RecycleId <= pr.RecycleId
		AND calc.GeometricContains(ro.RecycleId, pr.RecycleId) = 1
	CROSS JOIN dim.SimOpCond_LookUp					l
	--WHERE	q.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('FreshPyroFeed', 0, '') d)
	WHERE	q.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = pr.FactorSetId and d.StreamId = 'FreshPyroFeed')
		AND	l.OpCondId <> 'PlantFeed'
		AND	q.Refnum = @Refnum
		AND	pr.FactorSetId = @FactorSetId
	GROUP BY
		pr.FactorSetId,
		q.Refnum,
		l.OpCondId,
		ro.StreamId

	-------------------------------------------------------------------
	--	Prepare Once-through Simulation Yield Results

	DECLARE @iComposition TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
		[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
		[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
		[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
		[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),

		[FreshPyroFeed_kMT]		FLOAT (53)		NOT	NULL	CHECK([FreshPyroFeed_kMT] >= 0.0),

		[ComponentId]			VARCHAR (42)	NOT	NULL	CHECK(LEN([ComponentId]) > 0),
		[Component_WtPcnt]		AS CAST([Component_kMT] / [FreshPyroFeed_kMT] * 100.0 AS FLOAT (53))
								PERSISTED		NOT	NULL	CHECK([Component_WtPcnt] >= 0.0 AND [Component_WtPcnt] <= 100.0),
		[Component_kMT]			FLOAT (53)		NOT	NULL	CHECK([Component_kMT] >= 0.0),

		CHECK([Component_kMT] <= [FreshPyroFeed_kMT]),
		PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC, [ComponentId] ASC)
	);

	INSERT INTO @iComposition(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, FreshPyroFeed_kMT, ComponentId, Component_kMT)
	SELECT
		sy.FactorSetId,
		sy.Refnum,
		sy.CalDateKey,
		sy.SimModelId,
		sy.OpCondId,
		SUM(SUM(q.Quantity_kMT * sy.Component_WtPcnt) / 100.0) OVER(PARTITION BY sy.FactorSetId, sy.Refnum, sy.CalDateKey, sy.SimModelId, sy.OpCondId)	[FreshPyroFeed_kMT],
		sy.ComponentId,
		SUM(q.Quantity_kMT * sy.Component_WtPcnt) / 100.0																								[Composition_kMT]
	FROM sim.YieldCompositionStream						sy
	INNER JOIN fact.Quantity							q
		ON	q.Refnum = sy.Refnum
		AND	q.StreamId = sy.StreamId
		AND	q.StreamDescription = sy.StreamDescription
	WHERE	sy.Refnum = @Refnum
		AND	sy.FactorSetId = @FactorSetId
		AND	sy.SimModelId = @SimModelId
		--AND	sy.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('FreshPyroFeed', 0, '') d)
		AND	sy.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = sy.FactorSetId and d.StreamId = 'FreshPyroFeed')
	GROUP BY
		sy.FactorSetId,
		sy.Refnum,
		sy.CalDateKey,
		sy.SimModelId,
		sy.OpCondId,
		sy.ComponentId;

	-------------------------------------------------------------------
	--	Calculate Recycle

	DECLARE @RecycleProcess TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
		[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
		[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
		[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
		[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),
		[StreamId]				VARCHAR (42)	NOT	NULL	CHECK(LEN([StreamId]) > 0),
		[ComponentId]			VARCHAR (42)	NOT	NULL	CHECK(LEN([ComponentId]) > 0),

		[Recycle_WtPcnt]		FLOAT (53)			NULL	CHECK([Recycle_WtPcnt] >= 0.0 AND [Recycle_WtPcnt] <= 100.0),
		[Yield_WtPcnt]			FLOAT (53) 			NULL	CHECK([Yield_WtPcnt] >= 0.0 AND [Yield_WtPcnt] <= 100.0),
		[Change_WtPcnt]			AS CAST(ISNULL([Yield_WtPcnt], 0.0) - ISNULL([Recycle_WtPcnt], 0.0) AS FLOAT (53))
								PERSISTED		NOT	NULL,
		PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC, [ComponentId] ASC)
	);

	INSERT INTO @RecycleProcess(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, ComponentId, Recycle_WtPcnt, Yield_WtPcnt)
	SELECT
		pr.FactorSetId,
		pr.Refnum,
		pr.CalDateKey,
		sy.SimModelId,
		sy.OpCondId,
		ro.StreamId,
		sy.ComponentId,
		cr.Component_WtPcnt		[Recycle_WtPcnt],
		sy.Component_WtPcnt		[Yield_WtPcnt]
	FROM calc.PlantRecycles								pr
	INNER JOIN ante.RecycleOperation					ro
		ON	ro.FactorSetId = pr.FactorSetId
		AND	ro.RecycleId <= pr.RecycleId
		AND calc.GeometricContains(ro.RecycleId, pr.RecycleId) = 1
	LEFT OUTER JOIN sim.YieldCompositionStream			sy
		ON	sy.FactorSetId = pr.FactorSetId
		AND	sy.Refnum = pr.Refnum
		AND	sy.CalDateKey = pr.CalDateKey
		AND	sy.StreamId = ro.StreamId
	LEFT OUTER JOIN fact.CompositionQuantityAggregate	cr
		ON	cr.FactorSetId = pr.FactorSetId
		AND	cr.Refnum = pr.Refnum
		AND	cr.CalDateKey = pr.CalDateKey
		AND	cr.StreamId = ro.StreamId
		AND	cr.ComponentId = sy.ComponentId
	WHERE	pr.Refnum = @Refnum
		AND	pr.FactorSetId = @FactorSetId
		AND	sy.SimModelId = @SimModelId

	-------------------------------------------------------------------
	--	Process Recycle Oprations

	DECLARE @Iterations			TINYINT = 0;
	DECLARE @MaxIterations		TINYINT = 100;

	DECLARE	@MinBalance_kMT		FLOAT (53) = 0.000001;
	DECLARE	@DeltaBalance_kMT	FLOAT (53) = @MinBalance_kMT;

	WHILE @DeltaBalance_kMT >= @MinBalance_kMT AND @Iterations <= @MaxIterations
	BEGIN

		UPDATE iRec
		SET
			iRec.FeedBalance_kMT = iRec.FeedBalance_kMT - i.AggregateRecycle_kMT,
			iRec.AggregateRecycle_kMT = iRec.AggregateRecycle_kMT + i.Recycle_kMT,
			iRec.Recycle_kMT = i.Recycle_kMT,
			@DeltaBalance_kMT = i.AggregateRecycle_kMT / iRec.FeedBalance_kMT
		FROM @iRecycle	iRec
		INNER JOIN (
			SELECT
				i.FactorSetId,
				i.Refnum,
				i.CalDateKey,
				i.SimModelId,
				i.OpCondId,
				i.FeedBalance_kMT,
				SUM(SUM(i.Recycle_kMT)) OVER(PARTITION BY i.FactorSetId, i.Refnum, i.CalDateKey, i.SimModelId, i.OpCondId, i.FeedBalance_kMT)	[AggregateRecycle_kMT],
				i.StreamId,
				SUM(i.Recycle_kMT)		[Recycle_kMT]
			FROM (
				SELECT
					i.FactorSetId,
					i.Refnum,
					i.CalDateKey,
					i.SimModelId,
					i.OpCondId,
					i.FeedBalance_kMT,
					i.StreamId,
					i.IndependentRecycle_kMT *	MIN(i.RecycleNormalization)
						OVER(PARTITION BY i.FactorSetId, i.Refnum, i.CalDateKey, i.SimModelId, i.OpCondId, i.StreamId)	[Recycle_kMT]
				FROM (
					SELECT
						i.FactorSetId,
						i.Refnum,
						i.CalDateKey,
						i.SimModelId,
						i.OpCondId,
						i.FeedBalance_kMT,
						i.StreamId,
						i.IndependentRecycle_kMT,
						i.Component_kMT / SUM(i.IndependentRecycle_kMT)
							OVER(PARTITION BY i.FactorSetId, i.Refnum, i.CalDateKey, i.SimModelId, i.OpCondId, i.ComponentId)
											[RecycleNormalization]
					FROM (
						SELECT
							iComp.FactorSetId,
							iComp.Refnum,
							iComp.CalDateKey,
							iComp.SimModelId,
							iComp.OpCondId,
							iRec.FeedBalance_kMT,
							iComp.ComponentId,
							iComp.Component_kMT,
							rp.StreamId,
							iRec.FeedBalance_kMT * rp.Recycle_WtPcnt / 100.0 *
							MIN(iComp.Component_kMT / iRec.FeedBalance_kMT / CASE WHEN rp.Recycle_WtPcnt <> 0.0 THEN rp.Recycle_WtPcnt END * 100.0)
								OVER(PARTITION BY iComp.FactorSetId, iComp.Refnum, iComp.CalDateKey, iComp.SimModelId, iComp.OpCondId, rp.StreamId)		[IndependentRecycle_kMT]
						FROM @iComposition					iComp
						INNER JOIN @iRecycle				iRec
							ON	iRec.FactorSetId = iComp.FactorSetId
							AND	iRec.Refnum = iComp.Refnum
							AND	iRec.CalDateKey = iComp.CalDateKey
							AND	iRec.OpCondId = iComp.OpCondId
						INNER JOIN @RecycleProcess			rp
							ON	rp.FactorSetId = iComp.FactorSetId
							AND	rp.Refnum = iComp.Refnum
							AND	rp.CalDateKey = iComp.CalDateKey
							AND rp.SimModelId = iComp.SimModelId
							AND	rp.OpCondId = iComp.OpCondId
							AND	rp.ComponentId = iComp.ComponentId
							AND	rp.StreamId = iRec.StreamId
						WHERE rp.Recycle_WtPcnt IS NOT NULL
					) i
				) i
			) i
			GROUP BY
				i.FactorSetId,
				i.Refnum,
				i.CalDateKey,
				i.SimModelId,
				i.OpCondId,
				i.FeedBalance_kMT,
				i.StreamId
			) i
			ON	i.FactorSetId = iRec.FactorSetId
			AND	i.Refnum = iRec.Refnum
			AND	i.CalDateKey = iRec.CalDateKey
			AND	i.OpCondId = iRec.OpCondId
			AND	i.StreamId = iRec.StreamId

		UPDATE iComp
		SET iComp.Component_kMT = i.Component_kMT
		FROM @iComposition	iComp
		INNER JOIN (
			SELECT
				iComp.FactorSetId,
				iComp.Refnum,
				iComp.CalDateKey,
				iComp.OpCondId,
				rp.SimModelId,
				rp.ComponentId,
				CASE WHEN	iComp.Component_kMT + SUM(rp.Change_WtPcnt * iRec.Recycle_kMT / 100.0) > 0
					THEN	iComp.Component_kMT + SUM(rp.Change_WtPcnt * iRec.Recycle_kMT / 100.0)
					ELSE	2.23E-300
					END		[Component_kMT]
			FROM @iComposition					iComp
			INNER JOIN @iRecycle				iRec
				ON	iRec.FactorSetId = iComp.FactorSetId
				AND	iRec.Refnum = iComp.Refnum
				AND	iRec.CalDateKey = iComp.CalDateKey
				AND	iRec.OpCondId = iComp.OpCondId
			INNER JOIN @RecycleProcess			rp
				ON	rp.FactorSetId = iComp.FactorSetId
				AND	rp.Refnum = iComp.Refnum
				AND	rp.CalDateKey = iComp.CalDateKey
				AND rp.SimModelId = iComp.SimModelId
				AND	rp.OpCondId = iComp.OpCondId
				AND	rp.ComponentId = iComp.ComponentId
				AND	rp.StreamId = iRec.StreamId
			GROUP BY
				iComp.FactorSetId,
				iComp.Refnum,
				iComp.CalDateKey,
				iComp.OpCondId,
				rp.SimModelId,
				rp.ComponentId,
				iComp.Component_kMT
			) i
		ON	i.FactorSetId = iComp.FactorSetId
		AND	i.Refnum = iComp.Refnum
		AND	i.CalDateKey = iComp.CalDateKey
		AND	i.SimModelId = iComp.SimModelId
		AND	i.OpCondId = iComp.OpCondId
		AND	i.ComponentId = iComp.ComponentId

		IF @@ERROR <> 0 BREAK;

		SET @Iterations = @Iterations + 1

	END

	-----------------------------------------------------------------
	-----------------------------------------------------------------
	--	Insert Plant Yield
		
	INSERT INTO sim.YieldCompositionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId, Component_kMT, Component_WtPcnt)
	SELECT
		i.FactorSetId,
		i.Refnum,
		i.CalDateKey,
		i.SimModelId,
		i.OpCondId,
		r.RecycleId,
		i.ComponentId,
		i.Component_kMT,
		i.Component_WtPcnt
	FROM @iComposition i
	INNER JOIN calc.PlantRecycles		r
		ON	r.FactorSetId = i.FactorSetId
		AND	r.Refnum = i.Refnum
		AND	r.CalDateKey = i.CalDateKey

	-----------------------------------------------------------------
	--	Insert Plant Energy Consumption
	
	INSERT INTO sim.EnergyConsumptionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, Energy_kCalC2H4)
	SELECT
		e.FactorSetId,
		e.Refnum,
		e.CalDateKey,
		e.SimModelId,
		e.OpCondId,
		pr.RecycleId,
		SUM(e.SimEnergy_kCalkg * 
			CASE WHEN e.StreamId IN('EthRec', 'ProRec', 'ButRec')
			THEN COALESCE(iRec.AggregateRecycle_kMT, 0.0) + COALESCE(rSup.SuppRecycled_kMT, 0.0)
			ELSE q.Tot_kMT
			END
			* [C2H4].Component_WtPcnt) / 
		SUM(
			CASE WHEN e.StreamId IN('EthRec', 'ProRec', 'ButRec')
			THEN COALESCE(iRec.AggregateRecycle_kMT, 0.0) + COALESCE(rSup.SuppRecycled_kMT, 0.0)
			ELSE q.Tot_kMT
			END * [C2H4].Component_WtPcnt)
	FROM sim.EnergyConsumptionStream					e
	INNER JOIN calc.PlantRecycles						pr
		ON	pr.FactorSetId = e.FactorSetId
		AND	pr.Refnum = e.Refnum
		AND	pr.CalDateKey = e.CalDateKey
	INNER JOIN sim.YieldCompositionStream				[C2H4]
		ON	[C2H4].Refnum = e.Refnum
		AND	[C2H4].FactorSetId = e.FactorSetId
		AND	[C2H4].SimModelId = e.SimModelId
		AND	[C2H4].OpCondId = e.OpCondId
		AND	[C2H4].StreamId = e.StreamId
		AND	[C2H4].StreamDescription = e.StreamDescription
		AND	[C2H4].ComponentId = 'C2H4'
	INNER JOIN fact.QuantityPivot						q
		ON	q.Refnum = e.Refnum
		AND	q.CalDateKey = e.CalDateKey
		AND	q.StreamId = e.StreamId
		AND	q.StreamDescription = e.StreamDescription
	LEFT OUTER JOIN @iRecycle							iRec
		ON	iRec.FactorSetId = e.FactorSetId
		AND	iRec.Refnum = e.Refnum
		AND	iRec.CalDateKey = e.CalDateKey
		AND	iRec.OpCondId = e.OpCondId
		AND	iRec.StreamId = e.StreamId
	--LEFT OUTER JOIN fact.QuantitySupplementalRecycled	rSup
	--	ON	rSup.Refnum = e.Refnum
	--	AND	rSup.StreamId = e.StreamId
	LEFT OUTER JOIN fact.QuantitySupplementalRecycledAggregate(@Refnum, @RecycleId)	rSup
		ON	rSup.Refnum = e.Refnum
		AND	rSup.CalDateKey = e.CalDateKey
		AND	rSup.StreamId = e.StreamId
	WHERE	e.FactorSetId = @FactorSetId
		AND	e.Refnum = @Refnum
		AND	e.SimModelId = @SimModelId
	GROUP BY
		e.FactorSetId,
		e.Refnum,
		e.CalDateKey,
		e.SimModelId,
		e.OpCondId,
		pr.RecycleId;

	END;

	--IF @SimModelId = 'SPSL_20130710'
	--BEGIN

	---------------------------------------------------------------------
	----	Recycle Handler
	
	--DECLARE @iRecycle TABLE
	--(
	--	[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
	--	[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
	--	[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
	--	[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),
	--	[StreamId]				VARCHAR (42)	NOT	NULL	CHECK(LEN([StreamId]) > 0),

	--	[FreshPyroFeed_kMT]		FLOAT (53)		NOT	NULL	CHECK([FreshPyroFeed_kMT] >= 0.0),
	--	[FeedBalance_kMT]		FLOAT (53)		NOT	NULL	CHECK([FeedBalance_kMT] >= 0.0),
	--	[AggregateRecycle_kMT]	FLOAT (53)		NOT	NULL	CHECK([AggregateRecycle_kMT] >= 0.0)	DEFAULT(0.0),
	--	[Recycle_kMT]			FLOAT (53)		NOT	NULL	CHECK([Recycle_kMT] >= 0.0)		DEFAULT(0.0),

	--	CHECK([FeedBalance_kMT]			<= [FreshPyroFeed_kMT]),
	--	CHECK([AggregateRecycle_kMT]	<= [FreshPyroFeed_kMT]),
	--	PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [OpCondId] ASC, [StreamId] ASC)
	--);

	--INSERT INTO @iRecycle(FactorSetId, Refnum, CalDateKey, OpCondId, StreamId, FreshPyroFeed_kMT, FeedBalance_kMT)
	--SELECT
	--	pr.FactorSetId,
	--	q.Refnum,
	--	MAX(q.CalDateKey),
	--	l.OpCondId,
	--	ro.StreamId,
	--	SUM(q.Quantity_kMT)	[FreshPyroFeed_kMT],
	--	SUM(q.Quantity_kMT)	[Balance_kMT]
	--FROM fact.Quantity								q
	--INNER JOIN calc.PlantRecycles						pr
	--	ON	pr.Refnum = q.Refnum
	--INNER JOIN ante.RecycleOperation				ro
	--	ON	ro.FactorSetId = pr.FactorSetId
	--	AND	ro.RecycleId <= pr.RecycleId
	--	AND calc.GeometricContains(ro.RecycleId, pr.RecycleId) = 1
	--CROSS JOIN dim.SimOpCondLu						l
	--WHERE	q.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('FreshPyroFeed', 0, '') d)
	--	AND	l.OpCondId <> 'PlantFeed'
	--	AND	q.Refnum = @Refnum
	--	AND	pr.FactorSetId = @FactorSetId
	--GROUP BY
	--	pr.FactorSetId,
	--	q.Refnum,
	--	l.OpCondId,
	--	ro.StreamId

	---------------------------------------------------------------------
	----	Prepare Once-through Simulation Yield Results

	--DECLARE @iComposition TABLE
	--(
	--	[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
	--	[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
	--	[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
	--	[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
	--	[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),

	--	[FreshPyroFeed_kMT]		FLOAT (53)		NOT	NULL	CHECK([FreshPyroFeed_kMT] >= 0.0),

	--	[ComponentId]			VARCHAR (42)	NOT	NULL	CHECK(LEN([ComponentId]) > 0),
	--	[Component_WtPcnt]		AS CAST([Component_kMT] / [FreshPyroFeed_kMT] * 100.0 AS FLOAT (53))
	--							PERSISTED		NOT	NULL	CHECK([Component_WtPcnt] >= 0.0 AND [Component_WtPcnt] <= 100.0),
	--	[Component_kMT]			FLOAT (53)		NOT	NULL	CHECK([Component_kMT] >= 0.0),

	--	CHECK([Component_kMT] <= [FreshPyroFeed_kMT]),
	--	PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC, [ComponentId] ASC)
	--);

	--INSERT INTO @iComposition(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, FreshPyroFeed_kMT, ComponentId, Component_kMT)
	--SELECT
	--	sy.FactorSetId,
	--	sy.Refnum,
	--	sy.CalDateKey,
	--	sy.SimModelId,
	--	sy.OpCondId,
	--	SUM(SUM(q.Quantity_kMT * sy.Component_WtPcnt) / 100.0) OVER(PARTITION BY sy.FactorSetId, sy.Refnum, sy.CalDateKey, sy.SimModelId, sy.OpCondId)	[FreshPyroFeed_kMT],
	--	sy.ComponentId,
	--	SUM(q.Quantity_kMT * sy.Component_WtPcnt) / 100.0																								[Composition_kMT]
	--FROM sim.YieldCompositionStream						sy
	--INNER JOIN fact.Quantity							q
	--	ON	q.Refnum = sy.Refnum
	--	AND	q.StreamId = sy.StreamId
	--	AND	q.StreamDescription = sy.StreamDescription
	--WHERE	sy.Refnum = @Refnum
	--	AND	sy.FactorSetId = @FactorSetId
	--	AND	sy.SimModelId = @SimModelId
	--	AND	sy.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('FreshPyroFeed', 0, '') d)
	--GROUP BY
	--	sy.FactorSetId,
	--	sy.Refnum,
	--	sy.CalDateKey,
	--	sy.SimModelId,
	--	sy.OpCondId,
	--	sy.ComponentId;

	---------------------------------------------------------------------
	----	Calculate Recycle

	--DECLARE @RecycleProcess TABLE
	--(
	--	[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
	--	[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
	--	[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
	--	[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
	--	[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),
	--	[StreamId]				VARCHAR (42)	NOT	NULL	CHECK(LEN([StreamId]) > 0),
	--	[ComponentId]			VARCHAR (42)	NOT	NULL	CHECK(LEN([ComponentId]) > 0),

	--	[Recycle_WtPcnt]		FLOAT (53)			NULL	CHECK([Recycle_WtPcnt] >= 0.0 AND [Recycle_WtPcnt] <= 100.0),
	--	[Yield_WtPcnt]			FLOAT (53) 			NULL	CHECK([Yield_WtPcnt] >= 0.0 AND [Yield_WtPcnt] <= 100.0),
	--	[Change_WtPcnt]			AS CAST(ISNULL([Yield_WtPcnt], 0.0) - ISNULL([Recycle_WtPcnt], 0.0) AS FLOAT (53))
	--							PERSISTED		NOT	NULL,
	--	PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC, [ComponentId] ASC)
	--);

	--INSERT INTO @RecycleProcess(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, ComponentId, Recycle_WtPcnt, Yield_WtPcnt)
	--SELECT
	--	pr.FactorSetId,
	--	pr.Refnum,
	--	pr.CalDateKey,
	--	sy.SimModelId,
	--	sy.OpCondId,
	--	ro.StreamId,
	--	sy.ComponentId,
	--	cr.Component_WtPcnt		[Recycle_WtPcnt],
	--	sy.Component_WtPcnt		[Yield_WtPcnt]
	--FROM calc.PlantRecycles								pr
	--INNER JOIN ante.RecycleOperation					ro
	--	ON	ro.FactorSetId = pr.FactorSetId
	--	AND	ro.RecycleId <= pr.RecycleId
	--	AND calc.GeometricContains(ro.RecycleId, pr.RecycleId) = 1
	--LEFT OUTER JOIN sim.YieldCompositionStream			sy
	--	ON	sy.FactorSetId = pr.FactorSetId
	--	AND	sy.Refnum = pr.Refnum
	--	AND	sy.CalDateKey = pr.CalDateKey
	--	AND	sy.StreamId = ro.StreamId
	--LEFT OUTER JOIN fact.CompositionQuantityAggregate	cr
	--	ON	cr.Refnum = pr.Refnum
	--	AND	cr.CalDateKey = pr.CalDateKey
	--	AND	cr.StreamId = ro.StreamId
	--	AND	cr.ComponentId = sy.ComponentId
	--WHERE	pr.Refnum = @Refnum
	--	AND	pr.FactorSetId = @FactorSetId
	--	AND	sy.SimModelId = @SimModelId

	---------------------------------------------------------------------
	----	Process Recycle Oprations

	--DECLARE @Iterations			TINYINT = 0;
	--DECLARE @MaxIterations		TINYINT = 100;

	--DECLARE	@MinBalance_kMT		FLOAT (53) = 0.000001;
	--DECLARE	@DeltaBalance_kMT	FLOAT (53) = @MinBalance_kMT;

	--WHILE @DeltaBalance_kMT >= @MinBalance_kMT AND @Iterations <= @MaxIterations
	--BEGIN

	--	UPDATE iRec
	--	SET
	--		iRec.FeedBalance_kMT = iRec.FeedBalance_kMT - i.AggregateRecycle_kMT,
	--		iRec.AggregateRecycle_kMT = iRec.AggregateRecycle_kMT + i.Recycle_kMT,
	--		iRec.Recycle_kMT = i.Recycle_kMT,
	--		@DeltaBalance_kMT = i.AggregateRecycle_kMT / iRec.FeedBalance_kMT
	--	FROM @iRecycle	iRec
	--	INNER JOIN (
	--		SELECT
	--			i.FactorSetId,
	--			i.Refnum,
	--			i.CalDateKey,
	--			i.SimModelId,
	--			i.OpCondId,
	--			i.FeedBalance_kMT,
	--			SUM(SUM(i.Recycle_kMT)) OVER(PARTITION BY i.FactorSetId, i.Refnum, i.CalDateKey, i.SimModelId, i.OpCondId, i.FeedBalance_kMT)	[AggregateRecycle_kMT],
	--			i.StreamId,
	--			SUM(i.Recycle_kMT)		[Recycle_kMT]
	--		FROM (
	--			SELECT
	--				i.FactorSetId,
	--				i.Refnum,
	--				i.CalDateKey,
	--				i.SimModelId,
	--				i.OpCondId,
	--				i.FeedBalance_kMT,
	--				i.StreamId,
	--				i.IndependentRecycle_kMT *	MIN(i.RecycleNormalization)
	--					OVER(PARTITION BY i.FactorSetId, i.Refnum, i.CalDateKey, i.SimModelId, i.OpCondId, i.StreamId)	[Recycle_kMT]
	--			FROM (
	--				SELECT
	--					i.FactorSetId,
	--					i.Refnum,
	--					i.CalDateKey,
	--					i.SimModelId,
	--					i.OpCondId,
	--					i.FeedBalance_kMT,
	--					i.StreamId,
	--					i.IndependentRecycle_kMT,
	--					i.Component_kMT / SUM(i.IndependentRecycle_kMT)
	--						OVER(PARTITION BY i.FactorSetId, i.Refnum, i.CalDateKey, i.SimModelId, i.OpCondId, i.ComponentId)
	--										[RecycleNormalization]
	--				FROM (
	--					SELECT
	--						iComp.FactorSetId,
	--						iComp.Refnum,
	--						iComp.CalDateKey,
	--						iComp.SimModelId,
	--						iComp.OpCondId,
	--						iRec.FeedBalance_kMT,
	--						iComp.ComponentId,
	--						iComp.Component_kMT,
	--						rp.StreamId,
	--						iRec.FeedBalance_kMT * rp.Recycle_WtPcnt / 100.0 *
	--						MIN(iComp.Component_kMT / iRec.FeedBalance_kMT / CASE WHEN rp.Recycle_WtPcnt <> 0.0 THEN rp.Recycle_WtPcnt END * 100.0)
	--							OVER(PARTITION BY iComp.FactorSetId, iComp.Refnum, iComp.CalDateKey, iComp.SimModelId, iComp.OpCondId, rp.StreamId)		[IndependentRecycle_kMT]
	--					FROM @iComposition					iComp
	--					INNER JOIN @iRecycle				iRec
	--						ON	iRec.FactorSetId = iComp.FactorSetId
	--						AND	iRec.Refnum = iComp.Refnum
	--						AND	iRec.CalDateKey = iComp.CalDateKey
	--						AND	iRec.OpCondId = iComp.OpCondId
	--					INNER JOIN @RecycleProcess			rp
	--						ON	rp.FactorSetId = iComp.FactorSetId
	--						AND	rp.Refnum = iComp.Refnum
	--						AND	rp.CalDateKey = iComp.CalDateKey
	--						AND rp.SimModelId = iComp.SimModelId
	--						AND	rp.OpCondId = iComp.OpCondId
	--						AND	rp.ComponentId = iComp.ComponentId
	--						AND	rp.StreamId = iRec.StreamId
	--					WHERE rp.Recycle_WtPcnt IS NOT NULL
	--				) i
	--			) i
	--		) i
	--		GROUP BY
	--			i.FactorSetId,
	--			i.Refnum,
	--			i.CalDateKey,
	--			i.SimModelId,
	--			i.OpCondId,
	--			i.FeedBalance_kMT,
	--			i.StreamId
	--		) i
	--		ON	i.FactorSetId = iRec.FactorSetId
	--		AND	i.Refnum = iRec.Refnum
	--		AND	i.CalDateKey = iRec.CalDateKey
	--		AND	i.OpCondId = iRec.OpCondId
	--		AND	i.StreamId = iRec.StreamId

	--	UPDATE iComp
	--	SET iComp.Component_kMT = i.Component_kMT
	--	FROM @iComposition	iComp
	--	INNER JOIN (
	--		SELECT
	--			iComp.FactorSetId,
	--			iComp.Refnum,
	--			iComp.CalDateKey,
	--			iComp.OpCondId,
	--			rp.SimModelId,
	--			rp.ComponentId,
	--			CASE WHEN	iComp.Component_kMT + SUM(rp.Change_WtPcnt * iRec.Recycle_kMT / 100.0) > 0
	--				THEN	iComp.Component_kMT + SUM(rp.Change_WtPcnt * iRec.Recycle_kMT / 100.0)
	--				ELSE	2.23E-300
	--				END		[Component_kMT]
	--		FROM @iComposition					iComp
	--		INNER JOIN @iRecycle				iRec
	--			ON	iRec.FactorSetId = iComp.FactorSetId
	--			AND	iRec.Refnum = iComp.Refnum
	--			AND	iRec.CalDateKey = iComp.CalDateKey
	--			AND	iRec.OpCondId = iComp.OpCondId
	--		INNER JOIN @RecycleProcess			rp
	--			ON	rp.FactorSetId = iComp.FactorSetId
	--			AND	rp.Refnum = iComp.Refnum
	--			AND	rp.CalDateKey = iComp.CalDateKey
	--			AND rp.SimModelId = iComp.SimModelId
	--			AND	rp.OpCondId = iComp.OpCondId
	--			AND	rp.ComponentId = iComp.ComponentId
	--			AND	rp.StreamId = iRec.StreamId
	--		GROUP BY
	--			iComp.FactorSetId,
	--			iComp.Refnum,
	--			iComp.CalDateKey,
	--			iComp.OpCondId,
	--			rp.SimModelId,
	--			rp.ComponentId,
	--			iComp.Component_kMT
	--		) i
	--	ON	i.FactorSetId = iComp.FactorSetId
	--	AND	i.Refnum = iComp.Refnum
	--	AND	i.CalDateKey = iComp.CalDateKey
	--	AND	i.SimModelId = iComp.SimModelId
	--	AND	i.OpCondId = iComp.OpCondId
	--	AND	i.ComponentId = iComp.ComponentId

	--	IF @@ERROR <> 0 BREAK;

	--	SET @Iterations = @Iterations + 1

	--END

	-------------------------------------------------------------------
	-------------------------------------------------------------------
	----	Insert Plant Yield
		
	--INSERT INTO sim.YieldCompositionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId, Component_kMT, Component_WtPcnt)
	--SELECT
	--	i.FactorSetId,
	--	i.Refnum,
	--	i.CalDateKey,
	--	i.SimModelId,
	--	i.OpCondId,
	--	r.RecycleId,
	--	i.ComponentId,
	--	i.Component_kMT,
	--	i.Component_WtPcnt
	--FROM @iComposition i
	--INNER JOIN calc.PlantRecycles		r
	--	ON	r.FactorSetId = i.FactorSetId
	--	AND	r.Refnum = i.Refnum
	--	AND	r.CalDateKey = i.CalDateKey

	-------------------------------------------------------------------
	----	Insert Plant Energy Consumption
	
	--INSERT INTO sim.EnergyConsumptionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, Energy_kCalC2H4)
	--SELECT
	--	e.FactorSetId,
	--	e.Refnum,
	--	e.CalDateKey,
	--	e.SimModelId,
	--	e.OpCondId,
	--	pr.RecycleId,
	--	SUM(e.SimEnergy_kCalkg * 
	--		CASE WHEN e.StreamId IN('EthRec', 'ProRec', 'ButRec')
	--		THEN COALESCE(iRec.AggregateRecycle_kMT, 0.0) + COALESCE(rSup.SuppRecycled_kMT, 0.0)
	--		ELSE q.Tot_kMT
	--		END
	--		* [C2H4].Component_WtPcnt) / 
	--	SUM(
	--		CASE WHEN e.StreamId IN('EthRec', 'ProRec', 'ButRec')
	--		THEN COALESCE(iRec.AggregateRecycle_kMT, 0.0) + COALESCE(rSup.SuppRecycled_kMT, 0.0)
	--		ELSE q.Tot_kMT
	--		END * [C2H4].Component_WtPcnt)
	--FROM sim.EnergyConsumptionStream					e
	--INNER JOIN calc.PlantRecycles						pr
	--	ON	pr.FactorSetId = e.FactorSetId
	--	AND	pr.Refnum = e.Refnum
	--	AND	pr.CalDateKey = e.CalDateKey
	--INNER JOIN sim.YieldCompositionStream				[C2H4]
	--	ON	[C2H4].Refnum = e.Refnum
	--	AND	[C2H4].FactorSetId = e.FactorSetId
	--	AND	[C2H4].SimModelId = e.SimModelId
	--	AND	[C2H4].OpCondId = e.OpCondId
	--	AND	[C2H4].StreamId = e.StreamId
	--	AND	[C2H4].StreamDescription = e.StreamDescription
	--	AND	[C2H4].ComponentId = 'C2H4'
	--INNER JOIN fact.QuantityPivot						q
	--	ON	q.Refnum = e.Refnum
	--	AND	q.CalDateKey = e.CalDateKey
	--	AND	q.StreamId = e.StreamId
	--	AND	q.StreamDescription = e.StreamDescription
	--LEFT OUTER JOIN @iRecycle							iRec
	--	ON	iRec.FactorSetId = e.FactorSetId
	--	AND	iRec.Refnum = e.Refnum
	--	AND	iRec.CalDateKey = e.CalDateKey
	--	AND	iRec.OpCondId = e.OpCondId
	--	AND	iRec.StreamId = e.StreamId
	----LEFT OUTER JOIN fact.QuantitySupplementalRecycled	rSup
	----	ON	rSup.Refnum = e.Refnum
	----	AND	rSup.StreamId = e.StreamId
	--LEFT OUTER JOIN fact.QuantitySupplementalRecycledAggregate(@Refnum, @RecycleId)	rSup
	--	ON	rSup.Refnum = e.Refnum
	--	AND	rSup.CalDateKey = e.CalDateKey
	--	AND	rSup.StreamId = e.StreamId
	--WHERE	e.FactorSetId = @FactorSetId
	--	AND	e.Refnum = @Refnum
	--	AND	e.SimModelId = @SimModelId
	--GROUP BY
	--	e.FactorSetId,
	--	e.Refnum,
	--	e.CalDateKey,
	--	e.SimModelId,
	--	e.OpCondId,
	--	pr.RecycleId;

	--END;

	IF @SimModelId = 'PYPS'
	BEGIN

	DECLARE @C4Limits TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
		[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
		[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
		[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
		[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),
		[ComponentId]			VARCHAR (42)	NOT	NULL	CHECK(LEN([ComponentId]) > 0),
		[Component_kMT]			FLOAT			NOT	NULL	CHECK([Component_kMT] >= 0.0),

		PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC, [ComponentId] ASC)
	)

	INSERT INTO @C4Limits(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, ComponentId, Component_kMT)
	SELECT
		ycs.FactorSetId,
		ycs.Refnum,
		ycs.CalDateKey,
		ycs.SimModelId,
		ycs.OpCondId,
		ycs.ComponentId,
		SUM(CASE WHEN ycs.StreamId IN ('EthRec', 'ProRec')
			THEN sr.SuppRecycled_kMT
			ELSE q.Quantity_kMT
			END * ycs.Component_WtPcnt) / 100.0		[Component_kMT]
	FROM sim.YieldCompositionStream						ycs
	LEFT OUTER JOIN fact.Quantity						q
		ON	q.Refnum = ycs.Refnum
		AND	q.StreamId = ycs.StreamId
		AND	q.StreamDescription = ycs.StreamDescription
	LEFT OUTER JOIN fact.QuantitySupplementalRecycled	sr
		ON	sr.Refnum = ycs.Refnum
		AND	sr.CalDateKey = ycs.CalDateKey
		AND	sr.StreamId = ycs.StreamId
		AND	sr.ComponentId = ycs.ComponentId
	--WHERE	ycs.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('FreshPyroFeed|Recycle', 0, '|') d)
	WHERE	ycs.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = ycs.FactorSetId AND d.StreamId IN ('FreshPyroFeed', 'Recycle'))
		AND	ycs.StreamId <> 'ButRec'
		AND	ycs.ComponentId IN ('C4H6', 'C4H8', 'C4H10')
		AND	ycs.Refnum = @Refnum
		AND	ycs.FactorSetId = @FactorSetId
		AND	ycs.SimModelId = @SimModelId
	GROUP BY
		ycs.FactorSetId,
		ycs.Refnum,
		ycs.CalDateKey,
		ycs.SimModelId,
		ycs.OpCondId,
		ycs.ComponentId;

	DECLARE @C4Effluent TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
		[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
		[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
		[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
		[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),

		[C4_kMT]				FLOAT (53)		NOT	NULL	CHECK([C4_kMT] >= 0.0),

		PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC)
	)

	INSERT INTO @C4Effluent(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, C4_kMT)
	SELECT
		[C4].FactorSetId,
		[C4].Refnum,
		[C4].CalDateKey,
		[C4].SimModelId,
		[C4].OpCondId,
		calc.MinValue(SUM([C4].Component_kMT) - COALESCE(SUM(sr.SuppRecycled_kMT), 0.0), q.Tot_kMT)
	FROM  @C4Limits										[C4]
	LEFT OUTER JOIN fact.QuantitySupplementalRecycled	sr
		ON	sr.Refnum = [C4].Refnum
		AND	sr.CalDateKey = [C4].CalDateKey
		AND	sr.ComponentId = [C4].ComponentId
		AND	sr.StreamId = 'ButRec'
	INNER JOIN fact.QuantityPivot				q
		ON	q.Refnum = [C4].Refnum
		AND	q.CalDateKey = [C4].CalDateKey
		AND	q.StreamId = 'ButRec'
	GROUP BY
		[C4].FactorSetId,
		[C4].Refnum,
		[C4].CalDateKey,
		[C4].SimModelId,
		[C4].OpCondId,
		q.Tot_kMT

	DECLARE @ButRecycle TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
		[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
		[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
		[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
		[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),
		[StreamId]				VARCHAR (42)	NOT	NULL	CHECK(LEN([StreamId]) > 0),
		[RecycleId]				TINYINT			NOT	NULL,
		[ComponentId]			VARCHAR (42)	NOT	NULL	CHECK(LEN([ComponentId]) > 0),

		[Component_WtPcnt]		FLOAT (53)		NOT	NULL,	--	CHECK([Component_WtPcnt] >= 0.0 AND [Component_WtPcnt] <= 100.0),
		[Component_kMT]			FLOAT (53)		NOT	NULL,	--	CHECK([Component_kMT] >= 0.0),

		PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC, [ComponentId] ASC)
	)

	INSERT INTO @ButRecycle(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, RecycleId, ComponentId, Component_WtPcnt, Component_kMT)
	SELECT
		eff.FactorSetId,
		eff.Refnum,
		eff.CalDateKey,
		eff.SimModelId,
		eff.OpCondId,
		ycs.StreamId,
		ycs.RecycleId,
		ycs.ComponentId,
		ycs.Component_WtPcnt,
		eff.C4_kMT * ycs.Component_WtPcnt / 100.0	[Component_kMT]
	FROM @C4Effluent							eff
	INNER JOIN sim.YieldCompositionStream		ycs
		ON	ycs.FactorSetId = eff.FactorSetId
		AND	ycs.Refnum = eff.Refnum
		AND	ycs.CalDateKey = eff.CalDateKey
		AND	ycs.SimModelId = eff.SimModelId
		AND	ycs.OpCondId = eff.OpCondId
	WHERE	ycs.StreamId = 'ButRec'

	DECLARE @NetYield TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK(LEN([FactorSetId]) > 0),
		[Refnum]				VARCHAR (18)	NOT	NULL	CHECK(LEN([Refnum]) > 0),
		[CalDateKey]			INT				NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
		[SimModelId]			VARCHAR (4)		NOT	NULL	CHECK(LEN([SimModelId]) > 0),
		[OpCondId]				VARCHAR (12)	NOT	NULL	CHECK(LEN([OpCondId]) > 0),
		[ComponentId]			VARCHAR (42)	NOT	NULL	CHECK(LEN([ComponentId]) > 0),
		[NetYield_kMT]			FLOAT (53)		NOT	NULL,
		PRIMARY KEY CLUSTERED([FactorSetId] DESC, [Refnum] DESC, [CalDateKey] DESC, [SimModelId] ASC, [OpCondId] ASC, [ComponentId] ASC)
	)

	INSERT INTO @NetYield(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, ComponentId, NetYield_kMT)
	SELECT
		eff.FactorSetId,
		eff.Refnum,
		eff.CalDateKey,
		eff.SimModelId,
		eff.OpCondId,
		lim.ComponentId,
		- calc.MinValue(eff.C4_kMT, lim.Component_kMT)
	FROM @C4Effluent		eff
	INNER JOIN  @C4Limits	lim
		ON	lim.FactorSetId = eff.FactorSetId
		AND	lim.Refnum = eff.Refnum
		AND	lim.CalDateKey = eff.CalDateKey
		AND	lim.SimModelId = eff.SimModelId
		AND	lim.OpCondId = eff.OpCondId
	WHERE lim.ComponentId = 'C4H10'

	INSERT INTO @NetYield(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, ComponentId, NetYield_kMT)
	SELECT
		eff.FactorSetId,
		eff.Refnum,
		eff.CalDateKey,
		eff.SimModelId,
		eff.OpCondId,
		lim.ComponentId,
		- calc.MinValue(lim.Component_kMT, eff.C4_kMT + ny.NetYield_kMT)
	FROM @C4Effluent		eff
	INNER JOIN  @C4Limits	lim
		ON	lim.FactorSetId = eff.FactorSetId
		AND	lim.Refnum = eff.Refnum
		AND	lim.CalDateKey = eff.CalDateKey
		AND	lim.SimModelId = eff.SimModelId
		AND	lim.OpCondId = eff.OpCondId
	INNER JOIN @NetYield	ny
		ON	ny.FactorSetId = eff.FactorSetId
		AND	ny.Refnum = eff.Refnum
		AND	ny.CalDateKey = eff.CalDateKey
		AND	ny.SimModelId = eff.SimModelId
		AND	ny.OpCondId = eff.OpCondId
		AND	ny.ComponentId = 'C4H10'
	WHERE lim.ComponentId = 'C4H8'

	INSERT INTO @NetYield(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, ComponentId, NetYield_kMT)
	SELECT
		eff.FactorSetId,
		eff.Refnum,
		eff.CalDateKey,
		eff.SimModelId,
		eff.OpCondId,
		'C4H6',
		eff.C4_kMT + SUM(ny.NetYield_kMT)
	FROM @C4Effluent		eff
	INNER JOIN @NetYield	ny
		ON	ny.FactorSetId = eff.FactorSetId
		AND	ny.Refnum = eff.Refnum
		AND	ny.CalDateKey = eff.CalDateKey
		AND	ny.SimModelId = eff.SimModelId
		AND	ny.OpCondId = eff.OpCondId
		AND	ny.ComponentId IN('C4H10', 'C4H8')
	GROUP BY
		eff.FactorSetId,
		eff.Refnum,
		eff.CalDateKey,
		eff.SimModelId,
		eff.OpCondId,
		eff.C4_kMT

	INSERT INTO @NetYield(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, ComponentId, NetYield_kMT)
	SELECT
		eff.FactorSetId,
		eff.Refnum,
		eff.CalDateKey,
		eff.SimModelId,
		eff.OpCondId,
		[C2H4].ComponentId,
		[C2H4].Component_kMT * SUM(ny.NetYield_kMT) / (SUM([C4].Component_kMT) - eff.C4_kMT)
	FROM @C4Effluent							eff
	INNER JOIN @ButRecycle						[C2H4]
		ON	[C2H4].FactorSetId = eff.FactorSetId
		AND	[C2H4].Refnum = eff.Refnum
		AND	[C2H4].CalDateKey = eff.CalDateKey
		AND	[C2H4].SimModelId = eff.SimModelId
		AND	[C2H4].OpCondId = eff.OpCondId
		AND	[C2H4].StreamId = 'ButRec'
		AND	[C2H4].ComponentId NOT IN ('C4H6', 'C4H8', 'C4H10')
	LEFT OUTER JOIN @ButRecycle					[C4]
		ON	[C4].FactorSetId = eff.FactorSetId
		AND	[C4].Refnum = eff.Refnum
		AND	[C4].CalDateKey = eff.CalDateKey
		AND	[C4].SimModelId = eff.SimModelId
		AND	[C4].OpCondId = eff.OpCondId
		AND	[C4].StreamId = 'ButRec'
		AND	[C4].ComponentId IN ('C4H6', 'C4H8', 'C4H10')
	LEFT OUTER JOIN @NetYield					ny
		ON	ny.FactorSetId = eff.FactorSetId
		AND	ny.Refnum = eff.Refnum
		AND	ny.CalDateKey = eff.CalDateKey
		AND	ny.SimModelId = eff.SimModelId
		AND	ny.OpCondId = eff.OpCondId
		AND	ny.ComponentId = [C4].ComponentId
	GROUP BY
		eff.FactorSetId,
		eff.Refnum,
		eff.CalDateKey,
		eff.SimModelId,
		eff.OpCondId,
		eff.C4_kMT,
		[C2H4].StreamId,
		[C2H4].RecycleId,
		[C2H4].ComponentId,
		[C2H4].Component_kMT

	-----------------------------------------------------------------
	-----------------------------------------------------------------
	--	Insert Plant Yield

	INSERT INTO sim.YieldCompositionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId, Component_WtPcnt, Component_kMT)
	SELECT
		ycs.FactorSetId,
		ycs.Refnum,
		ycs.CalDateKey,
		ycs.SimModelId,
		ycs.OpCondId,
		ycs.RecycleId,
		ycs.ComponentId,
		ROUND(SUM(COALESCE(sr.SuppRecycled_kMT, q.Quantity_kMT) * ycs.Component_WtPcnt) / 100.0 + COALESCE(ny.NetYield_kMT, 0.0), 6) /
		SUM(SUM(COALESCE(sr.SuppRecycled_kMT, q.Quantity_kMT) * ycs.Component_WtPcnt) / 100.0 + COALESCE(ny.NetYield_kMT, 0.0))
			OVER (PARTITION BY ycs.FactorSetId, ycs.Refnum, ycs.CalDateKey, ycs.SimModelId, ycs.OpCondId) * 100.0		[Component_WtPcnt],
		ROUND(SUM(COALESCE(sr.SuppRecycled_kMT, q.Quantity_kMT) * ycs.Component_WtPcnt) / 100.0 + COALESCE(ny.NetYield_kMT, 0.0), 6)	[Component_kMT]
	FROM sim.YieldCompositionStream						ycs
	LEFT OUTER JOIN fact.Quantity						q
		ON	q.Refnum = ycs.Refnum
		AND	q.StreamId = ycs.StreamId
		AND	q.StreamDescription = ycs.StreamDescription
		AND	q.StreamId NOT IN ('EthRec', 'ProRec', 'ButRec')
	LEFT OUTER JOIN fact.QuantitySupplementalRecycledAggregate(@Refnum, @RecycleId)	sr
		ON	sr.Refnum = ycs.Refnum
		AND	sr.CalDateKey = ycs.CalDateKey
		AND	sr.StreamId = ycs.StreamId
	LEFT OUTER JOIN @NetYield							ny
		ON	ny.FactorSetId = ycs.FactorSetId
		AND	ny.Refnum = ycs.Refnum
		AND	ny.CalDateKey = ycs.CalDateKey
		AND	ny.SimModelId = ycs.SimModelId
		AND	ny.OpCondId = ycs.OpCondId
		AND	ny.ComponentId = ycs.ComponentId
	--WHERE	ycs.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('FreshPyroFeed|Recycle', 0, '|') d)
	WHERE	ycs.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = ycs.FactorSetId AND d.StreamId IN ('FreshPyroFeed', 'Recycle'))
		AND	ycs.Refnum = @Refnum
		AND	ycs.FactorSetId = @FactorSetId
		AND	ycs.SimModelId = @SimModelId
	GROUP BY
		ycs.FactorSetId,
		ycs.Refnum,
		ycs.CalDateKey,
		ycs.SimModelId,
		ycs.OpCondId,
		ycs.RecycleId,
		ycs.ComponentId,
		ny.NetYield_kMT;

	-----------------------------------------------------------------
	--	Insert Plant Energy Consumption

	INSERT INTO sim.EnergyConsumptionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, Energy_kCalC2H4)
	SELECT
		ycs.FactorSetId,
		ycs.Refnum,
		ycs.CalDateKey,
		ycs.SimModelId,
		ycs.OpCondId,
		ycs.RecycleId,
		SUM(COALESCE(ny.NetYield_kMT, q.Tot_kMT * ycs.Component_WtPcnt / 100.0, sr.SuppRecycled_kMT * ycs.Component_WtPcnt / 100.0) * ecs.SimEnergy_kCalkg) /
		SUM(COALESCE(q.Tot_kMT, sr.SuppRecycled_kMT) * ycs.Component_WtPcnt / 100.0)
	FROM sim.YieldCompositionStream						ycs
	LEFT OUTER JOIN fact.QuantityPivot					q
		ON	q.Refnum = ycs.Refnum
		AND	q.CalDateKey = ycs.CalDateKey
		AND	q.StreamId = ycs.StreamId
		AND	q.StreamDescription = ycs.StreamDescription
		AND	q.StreamId NOT IN ('EthRec', 'ProRec', 'ButRec')
	--LEFT OUTER JOIN fact.QuantitySupplementalRecycled	sr
	--	ON	sr.Refnum = ycs.Refnum
	--	AND	sr.CalDateKey = ycs.CalDateKey
	--	AND	sr.StreamId = ycs.StreamId
	--	AND	sr.ComponentId = ycs.ComponentId
	LEFT OUTER JOIN fact.QuantitySupplementalRecycledAggregate(@Refnum, @RecycleId)	sr
		ON	sr.Refnum = ycs.Refnum
		AND	sr.CalDateKey = ycs.CalDateKey
		AND	sr.StreamId = ycs.StreamId
	INNER JOIN sim.EnergyConsumptionStream				ecs
		ON	ecs.FactorSetId = ycs.FactorSetId
		AND	ecs.Refnum = ycs.Refnum
		AND	ecs.CalDateKey = ycs.CalDateKey
		AND	ecs.SimModelId = ycs.SimModelId
		AND	ecs.OpCondId = ycs.OpCondId
		AND	ecs.StreamId = ycs.StreamId
		AND	ecs.StreamDescription = ycs.StreamDescription
	LEFT OUTER JOIN @NetYield							ny
		ON	ny.FactorSetId = ycs.FactorSetId
		AND	ny.Refnum = ycs.Refnum
		AND	ny.CalDateKey = ycs.CalDateKey
		AND	ny.SimModelId = ycs.SimModelId
		AND	ny.OpCondId = ycs.OpCondId
		AND	ny.ComponentId = ycs.ComponentId
		AND	ycs.StreamId = 'ButRec'
	--WHERE	ycs.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('FreshPyroFeed|Recycle', 0, '|') d)
	WHERE	ycs.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = ycs.FactorSetId AND d.StreamId IN ('FreshPyroFeed', 'Recycle'))
		AND	ycs.ComponentId = 'C2H4'
		AND	ycs.Refnum = @Refnum
		AND	ycs.FactorSetId = @FactorSetId
		AND	ycs.SimModelId = @SimModelId
	GROUP BY
		ycs.FactorSetId,
		ycs.Refnum,
		ycs.CalDateKey,
		ycs.SimModelId,
		ycs.OpCondId,
		ycs.RecycleId

	END;

END;