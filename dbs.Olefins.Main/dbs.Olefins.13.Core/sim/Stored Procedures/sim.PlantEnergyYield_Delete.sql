﻿CREATE PROCEDURE [sim].[PlantEnergyYield_Delete]
(
	@FactorSetId	VARCHAR(12)	= NULL,
	@Refnum			VARCHAR(25)	= NULL,
	@SimModelId		VARCHAR(12)	= NULL
)
AS
BEGIN

	DELETE FROM sim.EnergyConsumptionPlant
	WHERE	FactorSetId		= @FactorSetId
		AND	Refnum			= @Refnum
		AND	SimModelId		= @SimModelId;

	DELETE FROM sim.YieldCompositionPlant
	WHERE	FactorSetId		= @FactorSetId
		AND	Refnum			= @Refnum	
		AND	SimModelId		= @SimModelId;
		
END