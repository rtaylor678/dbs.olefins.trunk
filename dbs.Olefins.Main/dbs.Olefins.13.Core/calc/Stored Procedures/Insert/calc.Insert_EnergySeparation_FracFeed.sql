﻿CREATE PROCEDURE [calc].[Insert_EnergySeparation_FracFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[EnergySeparation]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [EnergySeparation_BtuLb])
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			f.FacilityId,
			ISNULL(f.FeedProcessed_kMT, 0.0) * 
			(
			(55.0 * 1000.0 / 42.0) / (f.FeedDensity_SG * 8.34)
			)									[EnergySeparation_BtuLb]
		FROM @fpl												tsq
		INNER JOIN fact.FacilitiesFeedFrac						f
			ON	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr = 4
			AND	f.FeedDensity_SG > 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;