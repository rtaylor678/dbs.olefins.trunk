﻿CREATE PROCEDURE [calc].[Insert_Efficiency_Hydrotreater]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Efficiency([FactorSetId], [Refnum], [CalDateKey], [EfficiencyId], [UnitId], [EfficiencyStandard])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			wCdu.[EfficiencyId],
			uef.[UnitId],
			uef.[Coefficient]
			* POWER(
				[calc].[MaxValue](
					[calc].[MinValue](
						uef.[ValueMax],
						fht.[FeedRate_kBsd] / CONVERT(REAL, fc.[Unit_Count]) * 1000.0
						),
					uef.[ValueMin]
					),
				uef.[Exponent])
			* fht.[FeedRate_kBsd]
			* wCdu.[Coefficient]
		FROM @fpl										fpl
		INNER JOIN ante.UnitEfficiencyWWCdu					wCdu
			ON	wCdu.[FactorSetId]	= fpl.[FactorSetId]

		LEFT OUTER JOIN [fact].[FacilitiesHydroTreat]		ht
			ON	ht.[Refnum]			= fpl.[Refnum]
			AND	ht.[CalDateKey]		= fpl.[Plant_AnnDateKey]

		INNER JOIN (
			VALUES
				('B', 'TowerPyroGasB'),
				('DS', 'TowerPyroGasDS'),
				('SS', 'TowerPyroGasSS')
			)											map ([HtType], [UnitId])
			ON	map.[HtType]		= COALESCE(ht.[HTTypeID], 'SS')
		INNER JOIN ante.UnitEfficiencyFactors			uef
			ON	uef.[FactorSetId]	= fpl.[FactorSetId]
			AND	uef.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	uef.[UnitId]		= map.[UnitId]

		INNER JOIN [fact].[Facilities]					fc
			ON	fc.[Refnum]			= fpl.[Refnum]
			AND	fc.[CalDateKey]		= fpl.[Plant_AnnDateKey]
			AND	fc.[FacilityId]		= 'TowerPyroGasHT'
			AND	fc.[Unit_Count]		> 0
		INNER JOIN [fact].[FacilitiesHydroTreat]		fht
			ON	fht.[Refnum]		= fpl.[Refnum]
			AND	fht.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	fht.[FeedRate_kBsd]	> 0.0

		WHERE	fpl.CalQtr			= 4;
		
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;