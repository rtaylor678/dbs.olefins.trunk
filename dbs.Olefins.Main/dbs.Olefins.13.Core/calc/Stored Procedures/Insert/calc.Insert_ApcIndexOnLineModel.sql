﻿CREATE PROCEDURE [calc].[Insert_ApcIndexOnLineModel]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ApcIndexOnLineModel]([FactorSetId], [Refnum], [CalDateKey], [ApcId], [OnLine_Pcnt], [Mpc_Int], [Mpc_Value], [Abs_Mpc_Value])
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			w.ApcId,
			a.OnLineOperation_Pcnt												[OnLine_Pcnt],
			a.OnLineClosedLoop_Bit												[Mpc_Int],
			w.Mpc_Value,
			100.0 - (COALESCE(ia.[_Abs_Mpc_Value], 0.0) + COALESCE(so.[_Abs_Mpc_Value], 0.0) + COALESCE(ip.[_Abs_Mpc_Value], 0.0))
		FROM @fpl												tsq
		INNER JOIN calc.ApcAbsenceCorrection					c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_QtrDateKey
		INNER JOIN ante.ApcWeighting							w
			ON	w.FactorSetId	= tsq.FactorSetId
			AND	w.ApcId			= 'ApcOnLineClosedLoop'
		INNER JOIN fact.Apc										a
			ON	a.Refnum		= tsq.Refnum

		LEFT OUTER JOIN calc.ApcIndexPyroFurnIndivid			ip
			ON	ip.FactorSetId	= tsq.FactorSetId
			AND	ip.Refnum		= tsq.Refnum
			AND	ip.CalDateKey	= tsq.Plant_QtrDateKey
		
		LEFT OUTER JOIN (
			SELECT
				t.FactorSetId,
				t.Refnum,
				t.CalDateKey,
				SUM(t._Abs_Mpc_Value)			[_Abs_Mpc_Value]
			FROM calc.ApcIndexApplications	t
			GROUP BY
				t.FactorSetId,
				t.Refnum,
				t.CalDateKey				
			)													ia
			ON	ia.FactorSetId	= tsq.FactorSetId
			AND	ia.Refnum		= tsq.Refnum
			AND	ia.CalDateKey	= tsq.Plant_QtrDateKey
		
		LEFT OUTER JOIN (
			SELECT
				t.FactorSetId,
				t.Refnum,
				t.CalDateKey,
				SUM(t._Abs_Mpc_Value)			[_Abs_Mpc_Value]
			FROM calc.ApcIndexSteamOther	t
			GROUP BY
				t.FactorSetId,
				t.Refnum,
				t.CalDateKey				
			)													so
			ON	so.FactorSetId	= tsq.FactorSetId
			AND	so.Refnum		= tsq.Refnum
			AND	so.CalDateKey	= tsq.Plant_QtrDateKey

		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;