﻿CREATE PROCEDURE [calc].[Insert_Pers_MaintTa_AnnRetube]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PersMaintTa (FactorSetId, Refnum, CalDateKey, PersId, InflAdjAnn_Company_Hrs, InflAdjAnn_Contract_Hrs)
		SELECT
			a.FactorSetId,
			a.Refnum,
			a.CalDateKey,
			REPLACE(p.PersId, 'MaintTa', 'MaintTaAnnRetube')				[PersId],
			a.InflAdjAnn_MaintLabor_Hrs	* p.Ann_Company_Pcnt	/ 100.0 	[InflAdjAnn_Company_Hrs],
			a.InflAdjAnn_MaintLabor_Hrs	* p.Ann_Contract_Pcnt	/ 100.0		[InflAdjAnn_Contract_Hrs]
		FROM @fpl										tsq
		INNER JOIN calc.FurnacesAggregate				a
			ON	a.FactorSetId = tsq.FactorSetId
			AND	a.Refnum = tsq.Refnum
			AND	a.CalDateKey = tsq.Plant_QtrDateKey
			AND	a.CurrencyRpt = tsq.CurrencyRpt
			AND	a.InflAdjAnn_MaintRetube_Cur IS NOT NULL
		INNER JOIN calc.TaAdjPers						p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END