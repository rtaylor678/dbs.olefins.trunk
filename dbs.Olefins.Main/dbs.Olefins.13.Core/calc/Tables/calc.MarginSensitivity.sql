﻿CREATE TABLE [calc].[MarginSensitivity] (
    [FactorSetId]                 VARCHAR (12)       NOT NULL,
    [Refnum]                      VARCHAR (25)       NOT NULL,
    [CalDateKey]                  INT                NOT NULL,
    [CurrencyRpt]                 VARCHAR (4)        NOT NULL,
    [MarginAnalysis]              VARCHAR (42)       NOT NULL,
    [MarginId]                    VARCHAR (42)       NOT NULL,
    [Amount_Cur]                  REAL               NOT NULL,
    [Amount_SensEDC]              REAL               NULL,
    [Amount_SensProd_MT]          REAL               NULL,
    [Amount_SensProd_Cents]       REAL               NULL,
    [TaAdj_Amount_Cur]            REAL               NOT NULL,
    [TAAdj_Amount_SensEDC]        REAL               NULL,
    [TaAdj_Amount_SensProd_MT]    REAL               NULL,
    [TAAdj_Amount_SensProd_Cents] REAL               NULL,
    [tsModified]                  DATETIMEOFFSET (7) CONSTRAINT [DF_MarginSensitivity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]              NVARCHAR (168)     CONSTRAINT [DF_MarginSensitivity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]              NVARCHAR (168)     CONSTRAINT [DF_MarginSensitivity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]               NVARCHAR (168)     CONSTRAINT [DF_MarginSensitivity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_MarginSensitivity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MarginAnalysis] ASC, [MarginId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_MarginSensitivity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_MarginSensitivity_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_MarginSensitivity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_MarginSensitivity_Margin_LookUp] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_calc_MarginSensitivity_Stream_LookUp] FOREIGN KEY ([MarginAnalysis]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_MarginSensitivity_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_MarginSensitivity_CashProductionCost]
    ON [calc].[MarginSensitivity]([MarginId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [MarginAnalysis], [Amount_Cur], [TaAdj_Amount_Cur]);


GO
CREATE TRIGGER [calc].[t_MarginSensitivity_u]
	ON [calc].[MarginSensitivity]
	AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.MarginSensitivity
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MarginSensitivity.FactorSetId		= INSERTED.FactorSetId
		AND	calc.MarginSensitivity.Refnum			= INSERTED.Refnum
		AND calc.MarginSensitivity.CalDateKey		= INSERTED.CalDateKey
		AND calc.MarginSensitivity.CurrencyRpt		= INSERTED.CurrencyRpt
		AND calc.MarginSensitivity.MarginAnalysis	= INSERTED.MarginAnalysis
		AND calc.MarginSensitivity.MarginId			= INSERTED.MarginId;

END