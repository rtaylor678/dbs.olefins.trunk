﻿CREATE TABLE [calc].[EnergySeparation] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [StandardEnergyId]       VARCHAR (42)       NOT NULL,
    [EnergySeparation_BtuLb] REAL               NOT NULL,
    [_EnergySeparation_MBtu] AS                 (CONVERT([real],[EnergySeparation_BtuLb]*(2.2046),(0))) PERSISTED NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_EnergySeparation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_EnergySeparation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_EnergySeparation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_EnergySeparation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergySeparation] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StandardEnergyId] ASC, [CalDateKey] ASC),
    CHECK ([EnergySeparation_BtuLb]>=(0.0)),
    CONSTRAINT [FK_EnergySeparation_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergySeparation_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_EnergySeparation_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_EnergySeparation_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [calc].[t_EnergySeparation_u]
   ON  [calc].[EnergySeparation]
   AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [calc].[EnergySeparation]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[calc].[EnergySeparation].[FactorSetId]			= INSERTED.[FactorSetId]
		AND [calc].[EnergySeparation].[Refnum]				= INSERTED.[Refnum]
		AND [calc].[EnergySeparation].[StandardEnergyId]	= INSERTED.[StandardEnergyId]
		AND [calc].[EnergySeparation].[CalDateKey]			= INSERTED.[CalDateKey];
				
END;