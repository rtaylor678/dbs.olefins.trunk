﻿
CREATE VIEW calc.PersCostAggregate
WITH SCHEMABINDING
AS
SELECT
	c.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	c.CurrencyRpt,
	
	SUM(c.Amount_Cur)				[Amount_Cur],
	SUM(c.TaLabor_Cur)				[TaLaborAmount_Cur],
	SUM(c._Ann_Amount_Cur)			[Ann_Amount_Cur],
	SUM(c._TaAdjAnn_Amount_Cur)		[TaAdjAnn_Amount_Cur]
	
FROM calc.PersCost c
GROUP BY
	c.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	c.CurrencyRpt;