﻿
CREATE VIEW [calc].[EnergySeparationAggregate]
WITH SCHEMABINDING
AS
SELECT
	e.[FactorSetId],
	e.[Refnum],
	e.[CalDateKey],
	b.[StandardEnergyId],
	SUM(CASE b.[DescendantOperator]
		WHEN '+' THEN + e.[EnergySeparation_BtuLb]
		WHEN '-' THEN - e.[EnergySeparation_BtuLb]
		ELSE 0.0
		END)							[EnergySeparation_BtuLb],
	SUM(CASE b.[DescendantOperator]
		WHEN '+' THEN + e.[_EnergySeparation_MBtu]
		WHEN '-' THEN - e.[_EnergySeparation_MBtu]
		ELSE 0.0
		END)							[EnergySeparation_MBtu],
	COUNT_BIG(*)						[IndexItems]
FROM [calc].[EnergySeparation]				e
INNER JOIN [dim].[StandardEnergy_Bridge]	b
	ON	b.[FactorSetId] = e.[FactorSetId]
	AND	b.[DescendantId] = e.[StandardEnergyId]
GROUP BY
	e.[FactorSetId],
	e.[Refnum],
	e.[CalDateKey],
	b.[StandardEnergyId];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_EnergySeparationAggregate]
    ON [calc].[EnergySeparationAggregate]([FactorSetId] ASC, [Refnum] ASC, [StandardEnergyId] ASC, [CalDateKey] ASC);

