﻿CREATE VIEW [calc].[LossIndex_EthyleneFuel]
WITH SCHEMABINDING
AS
SELECT
	cs.FactorSetId,
	cs.Refnum,
	cs.SimModelId,
	cs.OpCondId,
	MAX(cs.CalDateKey)	[CalDateKey],
	cs.Component_WtPcnt								[EthyleneFuel_WtPcnt],
	SUM(cs.Component_kMT) / ABS(qp.Quantity_kMT) * 100.0	[Loss_EthyleneFuel_Prod_WtPcnt]
FROM calc.CompositionStream					cs
INNER JOIN fact.StreamQuantityAggregate		qp WITH (NOEXPAND)
	ON	qp.FactorSetId = cs.FactorSetId
	AND	qp.Refnum = cs.Refnum
	AND	qp.StreamId = 'EthylenePG'
WHERE	cs.StreamId = 'PPFC'
	AND cs.ComponentId = 'C2H4'
GROUP BY
	cs.FactorSetId,
	cs.Refnum,
	cs.SimModelId,
	cs.OpCondId,
	cs.Component_WtPcnt,
	qp.Quantity_kMT;