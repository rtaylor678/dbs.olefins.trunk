﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MetaOpEx(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string accountId;
			RangeReference rr;

			Dictionary<string, RangeReference> metaOpEx = MetaOpExDictionary();
			Dictionary<string, int> metaCol = MetaColDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MetaOpEx]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("AccountID"));

								if (metaOpEx.TryGetValue(accountId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;

									foreach (KeyValuePair<string, int> entry in metaCol)
									{
										c = entry.Value;
										AddRangeValues(wks, r, c, rdr, entry.Key);
									}
								}
							}
						}
					}
					cn.Close();
				};
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MetaOpEx", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MetaOpEx]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> MetaOpExDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("MetaNonVol", new RangeReference(T13_01, 46, 0));
			d.Add("MetaSTVol", new RangeReference(T13_01, 47, 0));

			return d;
		}
	}
}