﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void EnergyCogen(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			const int c = 8;

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "7"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_EnergyCogen]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								r = 53;
								AddRangeValues(wks, r, c, rdr, "HasCogen_YN");

								r = 68;
								AddRangeValues(wks, r, c, rdr, "Thermal_Pcnt", 100.0);
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "EnergyCogen", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_EnergyCogen]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}