﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityPyroFurn(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			int furnId;

			Dictionary<string, int> relFurn = RelFurnRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-4"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityPyroFurn]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								furnId = rdr.GetInt32(rdr.GetOrdinal("FurnID"));
								
								c = furnId + 2;

								foreach (KeyValuePair<string, int> entry in relFurn)
								{
									r = entry.Value;
									if (r == 40)
									{
										AddRangeValues(wks, r, c, rdr, entry.Key, 100.0);
									}
									else
									{
										AddRangeValues(wks, r, c, rdr, entry.Key);
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityPyroFurn", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityPyroFurn]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> RelFurnRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("StreamId", 6);

			d.Add("MaintDecoke_Days", 8);
			d.Add("MaintTLE_Days", 9);
			d.Add("MaintMinor_Days", 10);
			d.Add("MaintMajor_Days", 11);
			d.Add("StandByDT_Days", 13);

			//d.Add("PeriodDays", 0);
			//d.Add("PlantOutages", 0);
			//d.Add("TurnAround", 0);

			d.Add("FeedQty_kMT", 24);
			d.Add("FeedCap_MTd", 25);

			d.Add("FuelTypeID", 33);
			d.Add("FuelCons_kMT", 32);
			d.Add("StackOxygen_Pcnt", 34);
			d.Add("ArchDraft_H20", 35);
			d.Add("StackTemp_C", 36);
			d.Add("CrossTemp_C", 37);

			d.Add("Retubed_Year", 39);
			d.Add("Retubed_Pcnt", 40);
			d.Add("RetubeInterval_Mnths", 41);
			//d.Add("RetubeInterval_Years", 0);

			//d.Add("CurrencyRpt", 0);
			d.Add("MaintRetubeMatl_Cur", 44);
			d.Add("MaintRetubeLabor_Cur", 45);
			d.Add("MaintMajor_Cur", 46);

			return d;
		}
	}
}