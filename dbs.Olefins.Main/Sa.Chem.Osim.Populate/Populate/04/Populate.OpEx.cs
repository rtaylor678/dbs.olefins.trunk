﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void OpEx(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int cAmount = 6;
			int cName = 3;

			string accountId;
			double forex;

			Dictionary<string, int> OpExRow = OpExRowDictionary();
			
			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "4"];

				forex = wks.Cells[5, 6].Value2;

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_OpEx]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("AccountID"));

								if (OpExRow.TryGetValue(accountId, out r))
								{
									rng = wks.Cells[r, cAmount];

									if (!rng.Locked)
									{
										rng.Value2 = (double)rdr.GetValue(rdr.GetOrdinal("Amount_Cur")) * forex;
									}

									AddRangeValues(wks, r, cName, rdr, "AccountName");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "OpEx", refnum, wkb, wks, rng, (UInt32)r, (UInt32)cAmount, "[fact].[Select_OpEx]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> OpExRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("OccSal", 10);
			d.Add("OCCSal", 10);
			d.Add("MpsSal", 11);
			d.Add("MPSSal", 11);
			d.Add("OccBen", 13);
			d.Add("OCCBen", 13);
			d.Add("MpsBen", 14);
			d.Add("MPSBen", 14);

			d.Add("MaintMatl", 15);
			d.Add("MaintContractLabor", 16);
			d.Add("MaintContractMatl", 17);
			d.Add("MaintEquip", 18);

			d.Add("ContractTechSvc", 19);
			d.Add("Envir", 20);
			d.Add("ContractServOth", 21);
			d.Add("NonMaintEquipRent", 22);
			d.Add("Tax", 23);
			d.Add("Insur", 24);

			d.Add("NVE1", 26);
			d.Add("NVE2", 27);
			d.Add("NVE3", 28);
			d.Add("NVE4", 29);
			d.Add("NVE5", 30);
			d.Add("NVE6", 31);
			d.Add("NVE7", 32);
			d.Add("NVE8", 33);
			d.Add("NVE9", 34);

			d.Add("TaAccrual", 37);
			d.Add("TaCurrExp", 38);
			
			d.Add("ChemBFWCW", 42);
			d.Add("ChemWW", 43);
			d.Add("ChemCaustic", 44);
			d.Add("ChemPretreatment", 45);
			d.Add("ChemAntiFoul", 46);
			d.Add("ChemOthProc", 47);
			d.Add("ChemAdd", 48);
			d.Add("ChemOth", 49);

			d.Add("Catalysts", 50);
			d.Add("Royalties", 51);

			d.Add("PurElec", 53);
			d.Add("PurSteam", 54);
			d.Add("PurCoolingWater", 55);
			d.Add("PurOth", 56);

			d.Add("PurFuelGas", 58);
			d.Add("PurLiquid", 59);
			d.Add("PurSolid", 60);

			d.Add("PPFCFuelGas", 62);
			d.Add("PPFCOther", 63);

			d.Add("ExportSteam", 65);
			d.Add("ExportElectric", 66);

			d.Add("VE1", 68);
			d.Add("VE2", 69);
			d.Add("VE3", 70);
			d.Add("VE4", 71);
			d.Add("VE5", 72);
			d.Add("VE6", 73);
			d.Add("VE7", 74);
			d.Add("VE8", 75);
			d.Add("VE9", 76);

			d.Add("GANonPers", 80);

			return d;
		}

		private Dictionary<string, string> CurrencyCountryDictionary()
		{
			Dictionary<string, string> d = new Dictionary<string, string>();

			d.Add("ARS", "ARGENTINA");
			d.Add("AUD", "AUSTRALIA");
			//d.Add("EUR", "AUSTRIA");
			//d.Add("EUR", "BELGIUM");
			d.Add("BRL", "BRAZIL");
			d.Add("CAD", "CANADA");
			d.Add("CLP", "CHILE");
			d.Add("COP", "COLOMBIA");
			d.Add("CZK", "CZECH REP");
			d.Add("EURO", "EURO");
			//d.Add("EURO", "FINLAND");
			//d.Add("EURO", "FRANCE");
			//d.Add("EURO", "GERMANY");
			d.Add("HUF ", "HUNGARY");
			d.Add("INR", "INDIA");
			d.Add("ILS ", "ISRAEL");
			//d.Add("EURO", "ITALY");
			d.Add("IDR", "INDONESIA");
			d.Add("JPY", "JAPAN");
			d.Add("KRW", "SOUTH KOREA");
			d.Add("KWD", "KUWAIT");
			d.Add("MYR", "MALAYSIA");
			d.Add("MXV", "MEXICO");
			//d.Add("EUR", "NETHERLANDS");
			d.Add("NOK", "NORWAY");
			d.Add("PLN", "POLAND");
			//d.Add("EUR", "PORTUGAL");
			d.Add("CNY", "PRC CHINA");
			d.Add("QAR", "QATAR");
			d.Add("RON", "ROMANIA");
			d.Add("RUB", "RUSSIA");
			d.Add("SAR", "SAUDI ARABIA");
			d.Add("SGD", "SINGAPORE");
			d.Add("SKK", "SLOVAKIA");
			d.Add("ZAR", "SOUTH AFRICA");
			//d.Add("EUR", "SPAIN");
			d.Add("SEK", "SWEDEN");
			d.Add("TWD", "TAIWAN");
			d.Add("THB", "THAILAND");
			d.Add("TRY", "TURKEY");
			d.Add("AED", "UAE");
			d.Add("GBP", "UNITED KINGDOM");
			d.Add("USD", "USA");

			return d;
		}
	}
}