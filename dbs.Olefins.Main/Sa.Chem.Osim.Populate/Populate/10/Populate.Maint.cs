﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void Maint(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string facilityId;
			RangeReference rr;

			Dictionary<string, RangeReference> maint = MaintDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_Maint]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								facilityId = rdr.GetString(rdr.GetOrdinal("FacilityID"));

								if (maint.TryGetValue(facilityId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;

									c = 5;
									AddRangeValues(wks, r, c, rdr, "MaintMaterial_Cur");

									c = 6;
									AddRangeValues(wks, r, c, rdr, "MaintLabor_Cur");

									c = 7;
									AddRangeValues(wks, r, c, rdr, "TaMaterial_Cur");

									c = 8;
									AddRangeValues(wks, r, c, rdr, "TaLabor_Cur");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "Maint", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_Maint]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> MaintDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("PyroFurnMajOth", new RangeReference(T10_01, 33, 0));
			d.Add("PyroFurnMinOth", new RangeReference(T10_01, 34, 0));
			d.Add("PyroFurnTLE", new RangeReference(T10_01, 35, 0));
			d.Add("CompGas", new RangeReference(T10_01, 36, 0));
			d.Add("Pump", new RangeReference(T10_01, 37, 0));
			d.Add("Exchanger", new RangeReference(T10_01, 38, 0));
			d.Add("TowerCool", new RangeReference(T10_01, 39, 0));
			d.Add("BoilFiredSteam", new RangeReference(T10_01, 40, 0));
			d.Add("Vessel", new RangeReference(T10_01, 41, 0)); 
			d.Add("PipeOnSite", new RangeReference(T10_01, 42, 0));
			d.Add("InstrAna", new RangeReference(T10_01, 43, 0));
			d.Add("PCComputers", new RangeReference(T10_01, 44, 0));
			d.Add("ElecGen", new RangeReference(T10_01, 45, 0));
			d.Add("OtherOnSite", new RangeReference(T10_01, 47, 0));

			d.Add("Store", new RangeReference(T10_01, 49, 0));
			d.Add("Flare", new RangeReference(T10_01, 50, 0));
			d.Add("Enviro", new RangeReference(T10_01, 51, 0));
			d.Add("OtherOffSite", new RangeReference(T10_01, 52, 0));
			d.Add("TurnAroundServices", new RangeReference(T10_01, 54, 0));

			d.Add("OverHead", new RangeReference(T10_01,56, 0));
			
			return d;
		}
	}
}