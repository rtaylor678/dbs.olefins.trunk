﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class Integration
			{
				private static RangeReference FreshPyroFeed = new RangeReference(Tabs.T08_01, 51, 0);
				private static RangeReference Ethylene = new RangeReference(Tabs.T08_01, 53, 0);
				private static RangeReference Propylene = new RangeReference(Tabs.T08_01, 54, 0);
				private static RangeReference Prod = new RangeReference(Tabs.T08_01, 55, 0);

				private static RangeReference Integration_Pcnt = new RangeReference(Tabs.T08_01, 0, 8, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantCommIntegration]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Integration_Pcnt", Integration.Integration_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("FreshPyroFeed", Integration.FreshPyroFeed);
							d.Add("Ethylene", Integration.Ethylene);
							d.Add("Propylene", Integration.Propylene);
							d.Add("Prod", Integration.Prod);

							return d;
						}
					}
				}
			}
		}
	}
}