﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Prod
			{
				internal partial class Composition
				{
					private static RangeReference H2Mol_Pcnt = new RangeReference(Tabs.T03, 9, 8, SqlDbType.Float, 100.0);
					private static RangeReference CH4Mol_Pcnt = new RangeReference(Tabs.T03, 11, 8, SqlDbType.Float, 100.0);

					private static RangeReference CH4 = new RangeReference(Tabs.T03, 0, 8, SqlDbType.Float, 100.0);
					private static RangeReference C2H4 = new RangeReference(Tabs.T03, 0, 8, SqlDbType.Float, 100.0);
					private static RangeReference C3H6 = new RangeReference(Tabs.T03, 0, 8, SqlDbType.Float, 100.0);
					private static RangeReference C3H8 = new RangeReference(Tabs.T03, 0, 8, SqlDbType.Float, 100.0);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_StreamsProdComposition]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return new Streams().LookUpColumn;
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("CH4", Composition.CH4);
								d.Add("C2H4", Composition.C2H4);
								d.Add("C3H6", Composition.C3H6);
								d.Add("C3H8", Composition.C3H8);

								d.Add("H2Mol_Pcnt", Composition.H2Mol_Pcnt);
								d.Add("CH4Mol_Pcnt", Composition.H2Mol_Pcnt);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Hydrogen", Streams.Hydrogen);
								d.Add("Methane", Streams.Methane);
								d.Add("EthylenePG", Streams.EthylenePG);
								d.Add("EthyleneCG", Streams.EthyleneCG);
								d.Add("PropylenePG", Streams.PropylenePG);
								d.Add("PropyleneCG", Streams.PropyleneCG);
								d.Add("PropyleneRG", Streams.PropyleneRG);
								d.Add("PropaneC3Resid", Streams.PropaneC3Resid);

								return d;
							}
						}
					}
				}
			}
		}
	}
}