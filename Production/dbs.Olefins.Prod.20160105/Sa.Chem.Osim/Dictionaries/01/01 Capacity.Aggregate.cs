﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Capacity
		{
			internal class Aggregate
			{
				private static RangeReference columnCapacity = new RangeReference(Tabs.T01_01, 0, 6, SqlDbType.Float);
				private static RangeReference columnCapacitySd = new RangeReference(Tabs.T01_01, 0, 8, SqlDbType.Float);
				private static RangeReference columnRecord = new RangeReference(Tabs.T01_01, 0, 9, SqlDbType.Float);

				private static RangeReference Ethylene = new RangeReference(Tabs.T01_01, 10, 0, SqlDbType.Float);
				private static RangeReference Propylene = new RangeReference(Tabs.T01_01, 11, 0, SqlDbType.Float);
				private static RangeReference ProdOlefins = new RangeReference(Tabs.T01_01, 12, 0, SqlDbType.Float);
				private static RangeReference FreshPyroFeed = new RangeReference(Tabs.T01_01, 13, 0, SqlDbType.Float);
				private static RangeReference Recycle = new RangeReference(Tabs.T01_01, 14, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_CapacityAggregate]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("AbsCapacity_kMT", Aggregate.columnCapacity);
							d.Add("AbsStream_MTd", Aggregate.columnCapacitySd);
							d.Add("AbsRecord_MTd", Aggregate.columnRecord);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Ethylene", Aggregate.Ethylene);
							d.Add("Propylene", Aggregate.Propylene);
							d.Add("ProdOlefins", Aggregate.ProdOlefins);
							d.Add("FreshPyroFeed", Aggregate.FreshPyroFeed);
							d.Add("Recycle", Aggregate.Recycle);

							return d;
						}
					}
				}
			}
		}
	}
}