﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Maintenance
		{
			internal class Pers
			{
				private static RangeReference Company_Hrs = new RangeReference(Tabs.T10_02, 0, 7, SqlDbType.Float);

				private static RangeReference OccMaint = new RangeReference(Tabs.T10_02, 20, 7, SqlDbType.Float);
				private static RangeReference MpsMaint = new RangeReference(Tabs.T10_02, 22, 7, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MaintPers]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "PersId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Company_Hrs", Pers.Company_Hrs);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("OccMaint", Pers.OccMaint);
							d.Add("MpsMaint", Pers.MpsMaint);

							return d;
						}
					}
				}
			}
		}
	}
}