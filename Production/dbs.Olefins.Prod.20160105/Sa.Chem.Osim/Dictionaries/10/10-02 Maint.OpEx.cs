﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Maintenance
		{
			internal class OpEx
			{
				private static RangeReference AmountPrev_Cur = new RangeReference(Tabs.T10_02, 0, 7, SqlDbType.Float);

				private static RangeReference MaintMatl = new RangeReference(Tabs.T10_02, 15, 7, SqlDbType.Float);
				private static RangeReference MaintContractLabor = new RangeReference(Tabs.T10_02, 16, 7, SqlDbType.Float);
				private static RangeReference MaintContractMatl = new RangeReference(Tabs.T10_02, 17, 7, SqlDbType.Float);
				private static RangeReference MaintEquip = new RangeReference(Tabs.T10_02, 18, 7, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MaintOpExAggregate]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "AccountId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("AmountPrev_Cur", OpEx.AmountPrev_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("MaintMatl", OpEx.MaintMatl);
							d.Add("MaintContractLabor", OpEx.MaintContractLabor);
							d.Add("MaintContractMatl", OpEx.MaintContractMatl);
							d.Add("MaintEquip", OpEx.MaintEquip);

							return d;
						}
					}
				}
			}
		}
	}
}