﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class Train
			{
				private static RangeReference Train1 = new RangeReference(Tabs.T06_03, 0, 6);
				private static RangeReference Train2 = new RangeReference(Tabs.T06_03, 0, 7);
				private static RangeReference Train3 = new RangeReference(Tabs.T06_03, 0, 8);

				private static RangeReference Capacity_Pcnt = new RangeReference(Tabs.T06_03, 6, 0, SqlDbType.Float, 100.0);
				private static RangeReference PlannedInterval_Mnths = new RangeReference(Tabs.T06_03, 7, 0, SqlDbType.Float);
				private static RangeReference PlannedDownTime_Hrs = new RangeReference(Tabs.T06_03, 8, 0, SqlDbType.Float);
				private static RangeReference Interval_Mnths = new RangeReference(Tabs.T06_03, 9, 0, SqlDbType.Float);
				private static RangeReference DownTime_Hrs = new RangeReference(Tabs.T06_03, 10, 0, SqlDbType.Float);
				private static RangeReference TurnAroundCost_MCur = new RangeReference(Tabs.T06_03, 11, 0, SqlDbType.Float);
				private static RangeReference TurnAround_ManHrs = new RangeReference(Tabs.T06_03, 15, 0, SqlDbType.Float);
				private static RangeReference TurnAround_Date = new RangeReference(Tabs.T06_03, 18, 0, SqlDbType.Date);

				private static RangeReference MiniTA_YN = new RangeReference(Tabs.T06_03, 20, 0, SqlDbType.VarChar);
				private static RangeReference MiniTA_DownTime_Hrs = new RangeReference(Tabs.T06_03, 21, 0, SqlDbType.Float);
				private static RangeReference MiniTA_Cost_MCur = new RangeReference(Tabs.T06_03, 22, 0, SqlDbType.Float);
				private static RangeReference MiniTA_ManHrs = new RangeReference(Tabs.T06_03, 23, 0, SqlDbType.Float);
				private static RangeReference MiniTA_Date = new RangeReference(Tabs.T06_03, 24, 0, SqlDbType.Date);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityTrainTurnaround]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "TTrainId";
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("T1", Train.Train1);
							d.Add("T2", Train.Train2);
							d.Add("T3", Train.Train3);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Capacity_Pcnt", Train.Capacity_Pcnt);
							d.Add("PlannedInterval_Mnths", Train.PlannedInterval_Mnths);
							d.Add("PlannedDownTime_Hrs", Train.PlannedDownTime_Hrs);
							d.Add("Interval_Mnths", Train.Interval_Mnths);
							d.Add("DownTime_Hrs", Train.DownTime_Hrs);
							d.Add("TurnAroundCost_MCur", Train.TurnAroundCost_MCur);
							d.Add("TurnAround_ManHrs", Train.TurnAround_ManHrs);
							d.Add("TurnAround_Date", Train.TurnAround_Date);

							d.Add("MiniTA_YN", Train.MiniTA_YN);
							d.Add("MiniTA_DownTime_Hrs", Train.MiniTA_DownTime_Hrs);
							d.Add("MiniTA_Cost_MCur", Train.MiniTA_Cost_MCur);
							d.Add("MiniTA_ManHrs", Train.MiniTA_ManHrs);
							d.Add("MiniTA_Date", Train.MiniTA_Date);

							return d;
						}
					}
				}
			}
		}
	}
}