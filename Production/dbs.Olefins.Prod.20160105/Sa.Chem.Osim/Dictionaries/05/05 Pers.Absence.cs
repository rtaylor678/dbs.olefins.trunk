﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Pers
		{
			internal class Absence
			{
				private static RangeReference OccSubTotal = new RangeReference(Tabs.T05_A, 42, 0, SqlDbType.Float);
				private static RangeReference MpsSubTotal = new RangeReference(Tabs.T05_B, 44, 0, SqlDbType.Float);

				private static RangeReference Absence_Pcnt = new RangeReference(string.Empty, 0, 7, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_PersonnelAbsence]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "PersID";
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("OccSubTotal", Absence.OccSubTotal);
							d.Add("MpsSubTotal", Absence.MpsSubTotal);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Absence_Pcnt", Absence.Absence_Pcnt);

								return d;
							}
						}
					}
				}
			}
		}
	}
}