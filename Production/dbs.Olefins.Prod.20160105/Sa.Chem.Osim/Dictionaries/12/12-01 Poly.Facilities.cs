﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Polymer
		{
			internal partial class Facilities
			{
				private static RangeReference T1 = new RangeReference(Tabs.T12_01, 0, 4);
				private static RangeReference T2 = new RangeReference(Tabs.T12_01, 0, 5);
				private static RangeReference T3 = new RangeReference(Tabs.T12_01, 0, 6);
				private static RangeReference T4 = new RangeReference(Tabs.T12_01, 0, 7);
				private static RangeReference T5 = new RangeReference(Tabs.T12_01, 0, 8);
				private static RangeReference T6 = new RangeReference(Tabs.T12_01, 0, 9);

				private static RangeReference PolyName = new RangeReference(Tabs.T12_01, 6, 0, SqlDbType.VarChar);
				private static RangeReference PolyCity = new RangeReference(Tabs.T12_01, 7, 0, SqlDbType.VarChar);
				private static RangeReference PolyState = new RangeReference(Tabs.T12_01, 8, 0, SqlDbType.VarChar);
				private static RangeReference PolyCountry = new RangeReference(Tabs.T12_01, 9, 0, SqlDbType.VarChar);

				private static RangeReference Stream_MTd = new RangeReference(Tabs.T12_01, 10, 0, SqlDbType.Float);
				private static RangeReference Capacity_kMT = new RangeReference(Tabs.T12_01, 11, 0, SqlDbType.Float);
				private static RangeReference ReactorTrains_Count = new RangeReference(Tabs.T12_01, 12, 0, SqlDbType.Int);
				private static RangeReference StartUp_Year = new RangeReference(Tabs.T12_01, 13, 0, SqlDbType.Int);
				private static RangeReference PolymerFamilyID = new RangeReference(Tabs.T12_01, 14, 0, SqlDbType.VarChar);
				private static RangeReference PolymerTechID = new RangeReference(Tabs.T12_01, 15, 0, SqlDbType.VarChar);

				private static RangeReference PrimeResin_Pcnt = new RangeReference(Tabs.T12_01, 16, 0, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_PolyFacilities]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "TrainId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("PolyName", Facilities.PolyName);
							d.Add("PolyCity", Facilities.PolyCity);
							d.Add("PolyState", Facilities.PolyState);
							d.Add("PolyCountry", Facilities.PolyCountry);

							d.Add("Stream_MTd", Facilities.Stream_MTd);
							d.Add("Capacity_kMT", Facilities.Capacity_kMT);
							d.Add("ReactorTrains_Count", Facilities.ReactorTrains_Count);
							d.Add("StartUp_Year", Facilities.StartUp_Year);
							d.Add("PolymerFamilyID", Facilities.PolymerFamilyID);
							d.Add("PolymerTechID", Facilities.PolymerTechID);

							d.Add("PrimeResin_Pcnt", Facilities.PrimeResin_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("T1", Facilities.T1);
							d.Add("T2", Facilities.T2);
							d.Add("T3", Facilities.T3);
							d.Add("T4", Facilities.T4);
							d.Add("T5", Facilities.T5);
							d.Add("T6", Facilities.T6);

							return d;
						}
					}
				}
			}
		}
	}
}