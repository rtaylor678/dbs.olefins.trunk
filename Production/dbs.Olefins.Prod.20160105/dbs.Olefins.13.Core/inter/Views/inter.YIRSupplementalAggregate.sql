﻿CREATE VIEW [inter].[YIRSupplementalAggregate]
WITH SCHEMABINDING
AS
SELECT
	s.[FactorSetId],
	s.[Refnum],
	s.[CalDateKey],
	d.[ComponentId],
	SUM(
		CASE d.[DescendantOperator]
		WHEN '+' THEN + s.[Component_kMT]
		WHEN '-' THEN - s.[Component_kMT]
		ELSE 0.0
		END)								[Component_kMT],
	SUM(
		CASE d.[DescendantOperator]
		WHEN '+' THEN + s.[Component_WtPcnt]
		WHEN '-' THEN - s.[Component_WtPcnt]
		ELSE 0.0
		END)								[Component_WtPcnt],
	COUNT_BIG(*)							[IndexItems]
FROM [dim].[Component_Bridge]					d
INNER JOIN [inter].[YIRSupplemental]			s
	ON	s.[FactorSetId] = d.[FactorSetId]
	AND	s.[ComponentId] = d.[DescendantId]
GROUP BY
	s.[FactorSetId],
	s.[Refnum],
	s.[CalDateKey],
	d.[ComponentId];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_YIRSupplementalAggregate]
    ON [inter].[YIRSupplementalAggregate]([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC);

