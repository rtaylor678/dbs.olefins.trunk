﻿






CREATE              PROC [dbo].[spReliabilityCalc](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON


DELETE FROM dbo.ReliabilityCalc WHERE Refnum = @Refnum
Insert into dbo.ReliabilityCalc (Refnum
, CalDateKey
, DataType
, EthyleneCpbyAvg_kMT
, TurnAround
, OperError
, IntFeedInterrupt
, FurnaceProcess
, FFP
, Transition
, AcetyleneConv
, ProcessOther
, Compressor
, OtherRotating
, FurnaceMech
, Corrosion
, ElecDist
, ControlSys
, OtherFailure
, ExtElecFailure
, ExtOthUtilFailure
, IntElecFailure
, IntSteamFailure
, IntOthUtilFailure
, UnPlanned
, PlantRelated
, FeedMix
, Severity
, Demand
, ExtFeedInterrupt
, CapProject
, Strikes
, OtherNonOp
, OtherTot
, LostProdOpp
, EQF
, FFPCause1
, FFPCause2
, FFPCause3
, FFPCause4
, FFPCauses
, FFPCauseBalacnce
, NOC
, ProcessOther1
, ProcessOther2
, ProcessOther3
, ProcessOther4
, ProcessOtherBalance
, PRO
, UTF)
SELECT pvt.Refnum, pvt.CalDateKey, DataType, EthyleneCpbyAvg_kMT= d.EthyleneCpbyAvg_kMT,
    ISNULL(TurnAround,0), ISNULL(OperError,0), ISNULL(IntFeedInterrupt,0), ISNULL(FurnaceProcess,0), ISNULL(FFP,0),
    ISNULL(Transition,0), ISNULL(AcetyleneConv,0), ISNULL(ProcessOther,0), ISNULL(Compressor,0), ISNULL(OtherRotating,0),
    ISNULL(FurnaceMech,0), ISNULL(Corrosion,0), ISNULL(ElecDist,0), ISNULL(ControlSys,0),
	ISNULL(OtherFailure,0), ISNULL(ExtElecFailure,0), ISNULL(ExtOthUtilFailure,0), ISNULL(IntElecFailure,0),
	ISNULL(IntSteamFailure,0), ISNULL(IntOthUtilFailure,0), ISNULL(UnPlanned,0), ISNULL(PlantRelated,0),
	ISNULL(FeedMix,0), ISNULL(Severity,0), ISNULL(Demand,0), ISNULL(ExtFeedInterrupt,0), 
	ISNULL(CapProject,0), ISNULL(Strikes,0), ISNULL(OtherNonOp,0),
	OtherTot = ISNULL(FeedMix,0) + ISNULL(Severity,0) + ISNULL(Demand,0) + ISNULL(ExtFeedInterrupt,0) + ISNULL(CapProject,0) + ISNULL(Strikes,0) + ISNULL(OtherNonOp,0),
	ISNULL(LostProdOpp,0), ISNULL(EQF,0),
	ISNULL(FFPCause1,0), ISNULL(FFPCause2,0), ISNULL(FFPCause3,0), ISNULL(FFPCause4,0), ISNULL(FFPCauses,0), ISNULL(FFPCauseBalacnce,0),
	ISNULL(NOC,0),
	ISNULL(ProcessOther1,0), ISNULL(ProcessOther2,0), ISNULL(ProcessOther3,0), ISNULL(ProcessOther4,0), ISNULL(ProcessOtherBalance,0),
	ISNULL(PRO,0), ISNULL(UTF,0)
FROM (
SELECT r.FactorSetId, r.Refnum, r.CalDateKey, r.OppLossId, p.DataType, p.Value
FROM calc.ReliabilityOpLossAnnualizedAggregate  r with (NOEXPAND) CROSS APPLY (
      VALUES ('Count', r.IndexItems)
            , ('DTLossMT', r.DownTimeLoss_MT)
            , ('SDLossMT', r.SlowDownLoss_MT)
            , ('TotLossMT', r.TotLoss_MT)
      ) p(DataType, Value)) src
PIVOT (SUM(src.Value) FOR OppLossId IN (OperError,IntFeedInterrupt,FurnaceProcess,FFP,Transition,AcetyleneConv,ProcessOther,Compressor,OtherRotating,FurnaceMech,Corrosion,ElecDist,ControlSys,
OtherFailure,ExtElecFailure,ExtOthUtilFailure,IntElecFailure,IntSteamFailure,IntOthUtilFailure,UnPlanned,PlantRelated,FeedMix,Severity,Demand,ExtFeedInterrupt,CapProject,Strikes,OtherNonOp,
LostProdOpp,EQF,FFPCause1,FFPCause2,FFPCause3,FFPCause4,FFPCauses,FFPCauseBalacnce,NOC,ProcessOther1,ProcessOther2,ProcessOther3,ProcessOther4,ProcessOtherBalance,PRO,TurnAround,UTF)) pvt
JOIN dbo.Divisors d on d.Refnum=pvt.Refnum
WHERE pvt.Refnum = @Refnum and pvt.FactorSetId=@FactorSetId
SET NOCOUNT OFF

















