﻿


CREATE              PROC [dbo].[spYield](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON
DECLARE @Currency as varchar(5)
SELECT @Currency = 'USD'

--DECLARE @FreshPyroFeed real, @SuppTot real, @PlantFeed real, @Prod real, @Loss real, @ProdLoss real, @FeedProdLoss real
--SELECT 
--  @FreshPyroFeed = SUM(CASE WHEN q.StreamId = 'FreshPyroFeed' THEN q.Tot_kMT ELSE 0 END) 
--, @SuppTot = SUM(CASE WHEN q.StreamId = 'SuppTot' THEN q.Tot_kMT ELSE 0 END) 
--, @PlantFeed = SUM(CASE WHEN q.StreamId = 'PlantFeed' THEN q.Tot_kMT ELSE 0 END) 
--, @Prod = -SUM(CASE WHEN q.StreamId = 'Prod' THEN q.Tot_kMT ELSE 0 END) 
--, @Loss = -SUM(CASE WHEN q.StreamId = 'Loss' THEN q.Tot_kMT ELSE 0 END) 
--, @ProdLoss = -SUM(CASE WHEN q.StreamId = 'ProdLoss' THEN q.Tot_kMT ELSE 0 END) 
--, @FeedProdLoss = SUM(CASE WHEN q.StreamId = 'FeedProdLoss' THEN q.Tot_kMT ELSE 0 END) 
--FROM calc.PricingPlantStreamAggregate q 
--WHERE q.Refnum=@Refnum and q.FactorSetId = @FactorSetId
--AND q.StreamId in ('FreshPyroFeed','SuppTot','PlantFeed','Prod','Loss','ProdLoss','FeedProdLoss')


DECLARE	@C2H6	REAL;
DECLARE	@C3H8	REAL;
DECLARE	@C4H10	REAL;

SELECT
	@C2H6	= p.[C2H6],
	@C3H8	= p.[C3H8],
	@C4H10	= p.[C4H10]
FROM (
	SELECT
		qr.[FactorSetId],
		qr.[Refnum],
		qr.[ComponentId],
		qr.[_Recycled_kMT]
	FROM [calc].[QuantityRecycled]	qr
	WHERE	qr.[FactorSetId]	= @FactorSetId
		AND	qr.[Refnum]			= @Refnum
	) u
	PIVOT (
		SUM(u.[_Recycled_kMT]) FOR u.[ComponentId] IN
		(
			[C2H6],
			[C3H8],
			[C4H10]
		)
	) p;

DELETE FROM dbo.Yield WHERE Refnum = @Refnum
Insert into dbo.Yield (Refnum
, DataType
, Ethane
, EPMix
, Propane
, LPG
, Butane
, FeedLtOther
, NaphthaLt
, NaphthaFr
, NaphthaHv
, Condensate
, HeavyNGL
, Raffinate
, Diesel
, GasOilHv
, GasOilHt
, FeedLiqOther
, FreshPyroFeed
, ConcEthylene
, ConcPropylene
, ConcButadiene
, ConcBenzene
, ROGHydrogen
, ROGMethane
, ROGEthane
, ROGEthylene
, ROGPropylene
, ROGPropane
, ROGC4Plus
, ROGInerts
, DilHydrogen
, DilMethane
, DilEthane
, DilEthylene
, DilPropane
, DilPropylene
, DilButane
, DilButylene
, DilButadiene
, DilBenzene
, DilMogas
, SuppWashOil
, SuppGasOil
, SuppOther
, SuppTot
, PlantFeed
, Hydrogen
, Methane
, Acetylene
, EthylenePG
, EthyleneCG
, PropylenePG
, PropyleneCG
, PropyleneRG
, PropaneC3Resid
, Butadiene
, IsoButylene
, C4Oth
, Benzene
, PyroGasoline
, PyroGasOil
, PyroFuelOil
, AcidGas
, PPFC
, ProdOther
, Prod
, LossFlareVent
, LossOther
, LossMeasure
, Loss
, ProdLoss
, FeedProdLoss
, FeedLiqOther1, FeedLiqOther2, FeedLiqOther3
, ProdOther1, ProdOther2
, [RecSuppEthane], [RecSuppPropane], [RecSuppButane], [Recycle]
)

SELECT Refnum = @Refnum
, DataType					= 'kMT'
, Ethane			= SUM(CASE WHEN q.StreamId = 'Ethane' THEN q.Quantity_kMT ELSE 0 END) 
, EPMix				= SUM(CASE WHEN q.StreamId = 'EPMix' THEN q.Quantity_kMT ELSE 0 END) 
, Propane			= SUM(CASE WHEN q.StreamId = 'Propane' THEN q.Quantity_kMT ELSE 0 END) 
, LPG				= SUM(CASE WHEN q.StreamId = 'LPG' THEN q.Quantity_kMT ELSE 0 END) 
, Butane			= SUM(CASE WHEN q.StreamId = 'Butane' THEN q.Quantity_kMT ELSE 0 END) 
, FeedLtOther		= SUM(CASE WHEN q.StreamId = 'FeedLtOther' THEN q.Quantity_kMT ELSE 0 END) 
, NaphthaLt			= SUM(CASE WHEN q.StreamId = 'NaphthaLt' THEN q.Quantity_kMT ELSE 0 END) 
, NaphthaFr			= SUM(CASE WHEN q.StreamId = 'NaphthaFr' THEN q.Quantity_kMT ELSE 0 END) 
, NaphthaHv			= SUM(CASE WHEN q.StreamId = 'NaphthaHv' THEN q.Quantity_kMT ELSE 0 END) 
, Condensate		= SUM(CASE WHEN q.StreamId = 'Condensate' THEN q.Quantity_kMT ELSE 0 END) 
, HeavyNGL			= SUM(CASE WHEN q.StreamId = 'HeavyNGL' THEN q.Quantity_kMT ELSE 0 END) 
, Raffinate			= SUM(CASE WHEN q.StreamId = 'Raffinate' THEN q.Quantity_kMT ELSE 0 END) 
, Diesel			= SUM(CASE WHEN q.StreamId = 'Diesel' THEN q.Quantity_kMT ELSE 0 END) 
, GasOilHv			= SUM(CASE WHEN q.StreamId = 'GasOilHv' THEN q.Quantity_kMT ELSE 0 END) 
, GasOilHt			= SUM(CASE WHEN q.StreamId = 'GasOilHt' THEN q.Quantity_kMT ELSE 0 END) 
, FeedLiqOther		= SUM(CASE WHEN q.StreamId = 'FeedLiqOther' THEN q.Quantity_kMT ELSE 0 END) 
, FreshPyroFeed		= SUM(CASE WHEN q.StreamId = 'FreshPyroFeed' THEN q.Quantity_kMT ELSE 0 END) 
, ConcEthylene		= SUM(CASE WHEN q.StreamId = 'ConcEthylene' THEN q.Quantity_kMT ELSE 0 END) 
, ConcPropylene		= SUM(CASE WHEN q.StreamId = 'ConcPropylene' THEN q.Quantity_kMT ELSE 0 END) 
, ConcButadiene		= SUM(CASE WHEN q.StreamId = 'ConcButadiene' THEN q.Quantity_kMT ELSE 0 END) 
, ConcBenzene		= SUM(CASE WHEN q.StreamId = 'ConcBenzene' THEN q.Quantity_kMT ELSE 0 END) 
, ROGHydrogen		= SUM(CASE WHEN q.StreamId = 'ROGHydrogen' THEN q.Quantity_kMT ELSE 0 END) 
, ROGMethane		= SUM(CASE WHEN q.StreamId = 'ROGMethane' THEN q.Quantity_kMT ELSE 0 END) 
, ROGEthane			= SUM(CASE WHEN q.StreamId = 'ROGEthane' THEN q.Quantity_kMT ELSE 0 END) 
, ROGEthylene		= SUM(CASE WHEN q.StreamId = 'ROGEthylene' THEN q.Quantity_kMT ELSE 0 END) 
, ROGPropylene		= SUM(CASE WHEN q.StreamId = 'ROGPropylene' THEN q.Quantity_kMT ELSE 0 END) 
, ROGPropane		= SUM(CASE WHEN q.StreamId = 'ROGPropane' THEN q.Quantity_kMT ELSE 0 END) 
, ROGC4Plus			= SUM(CASE WHEN q.StreamId = 'ROGC4Plus' THEN q.Quantity_kMT ELSE 0 END) 
, ROGInerts			= SUM(CASE WHEN q.StreamId = 'ROGInerts' THEN q.Quantity_kMT ELSE 0 END) 
, DilHydrogen		= SUM(CASE WHEN q.StreamId = 'DilHydrogen' THEN q.Quantity_kMT ELSE 0 END) 
, DilMethane		= SUM(CASE WHEN q.StreamId = 'DilMethane' THEN q.Quantity_kMT ELSE 0 END) 
, DilEthane			= SUM(CASE WHEN q.StreamId = 'DilEthane' THEN q.Quantity_kMT ELSE 0 END) 
, DilEthylene		= SUM(CASE WHEN q.StreamId = 'DilEthylene' THEN q.Quantity_kMT ELSE 0 END) 
, DilPropane		= SUM(CASE WHEN q.StreamId = 'DilPropane' THEN q.Quantity_kMT ELSE 0 END) 
, DilPropylene		= SUM(CASE WHEN q.StreamId = 'DilPropylene' THEN q.Quantity_kMT ELSE 0 END) 
, DilButane			= SUM(CASE WHEN q.StreamId = 'DilButane' THEN q.Quantity_kMT ELSE 0 END) 
, DilButylene		= SUM(CASE WHEN q.StreamId = 'DilButylene' THEN q.Quantity_kMT ELSE 0 END) 
, DilButadiene		= SUM(CASE WHEN q.StreamId = 'DilButadiene' THEN q.Quantity_kMT ELSE 0 END) 
, DilBenzene		= SUM(CASE WHEN q.StreamId = 'DilBenzene' THEN q.Quantity_kMT ELSE 0 END) 
, DilMogas			= SUM(CASE WHEN q.StreamId = 'DilMogas' THEN q.Quantity_kMT ELSE 0 END) 
, SuppWashOil		= SUM(CASE WHEN q.StreamId = 'SuppWashOil' THEN q.Quantity_kMT ELSE 0 END) 
, SuppGasOil		= SUM(CASE WHEN q.StreamId = 'SuppGasOil' THEN q.Quantity_kMT ELSE 0 END) 
, SuppOther			= SUM(CASE WHEN q.StreamId = 'SuppOther' THEN q.Quantity_kMT ELSE 0 END) 
, SuppTot			= SUM(CASE WHEN q.StreamId = 'SuppTot' THEN q.Quantity_kMT ELSE 0 END) 
, PlantFeed			= SUM(CASE WHEN q.StreamId = 'PlantFeed' THEN q.Quantity_kMT ELSE 0 END) 
, Hydrogen			= -SUM(CASE WHEN q.StreamId = 'Hydrogen' THEN q.Quantity_kMT ELSE 0 END) 
, Methane			= -SUM(CASE WHEN q.StreamId = 'Methane' THEN q.Quantity_kMT ELSE 0 END) 
, Acetylene			= -SUM(CASE WHEN q.StreamId = 'Acetylene' THEN q.Quantity_kMT ELSE 0 END) 
, EthylenePG		= -SUM(CASE WHEN q.StreamId = 'EthylenePG' THEN q.Quantity_kMT ELSE 0 END) 
, EthyleneCG		= -SUM(CASE WHEN q.StreamId = 'EthyleneCG' THEN q.Quantity_kMT ELSE 0 END) 
, PropylenePG		= -SUM(CASE WHEN q.StreamId = 'PropylenePG' THEN q.Quantity_kMT ELSE 0 END) 
, PropyleneCG		= -SUM(CASE WHEN q.StreamId = 'PropyleneCG' THEN q.Quantity_kMT ELSE 0 END) 
, PropyleneRG		= -SUM(CASE WHEN q.StreamId = 'PropyleneRG' THEN q.Quantity_kMT ELSE 0 END) 
, PropaneC3Resid	= -SUM(CASE WHEN q.StreamId = 'PropaneC3Resid' THEN q.Quantity_kMT ELSE 0 END) 
, Butadiene			= -SUM(CASE WHEN q.StreamId = 'Butadiene' THEN q.Quantity_kMT ELSE 0 END) 
, IsoButylene		= -SUM(CASE WHEN q.StreamId = 'IsoButylene' THEN q.Quantity_kMT ELSE 0 END) 
, C4Oth				= -SUM(CASE WHEN q.StreamId = 'C4Oth' THEN q.Quantity_kMT ELSE 0 END) 
, Benzene			= -SUM(CASE WHEN q.StreamId = 'Benzene' THEN q.Quantity_kMT ELSE 0 END) 
, PyroGasoline		= -SUM(CASE WHEN q.StreamId = 'PyroGasoline' THEN q.Quantity_kMT ELSE 0 END) 
, PyroGasOil		= -SUM(CASE WHEN q.StreamId = 'PyroGasOil' THEN q.Quantity_kMT ELSE 0 END) 
, PyroFuelOil		= -SUM(CASE WHEN q.StreamId = 'PyroFuelOil' THEN q.Quantity_kMT ELSE 0 END) 
, AcidGas			= -SUM(CASE WHEN q.StreamId = 'AcidGas' THEN q.Quantity_kMT ELSE 0 END) 
, PPFC				= -SUM(CASE WHEN q.StreamId = 'PPFC' THEN q.Quantity_kMT ELSE 0 END) 
, ProdOther			= -SUM(CASE WHEN q.StreamId = 'ProdOther' THEN q.Quantity_kMT ELSE 0 END) 
, Prod				= -SUM(CASE WHEN q.StreamId = 'Prod' THEN q.Quantity_kMT ELSE 0 END) 
, LossFlareVent		= -SUM(CASE WHEN q.StreamId = 'LossFlareVent' THEN q.Quantity_kMT ELSE 0 END) 
, LossOther			= -SUM(CASE WHEN q.StreamId = 'LossOther' THEN q.Quantity_kMT ELSE 0 END) 
, LossMeasure		= -SUM(CASE WHEN q.StreamId = 'LossMeasure' THEN q.Quantity_kMT ELSE 0 END) 
, Loss				= -SUM(CASE WHEN q.StreamId = 'Loss' THEN q.Quantity_kMT ELSE 0 END) 
, ProdLoss			= -SUM(CASE WHEN q.StreamId = 'ProdLoss' THEN q.Quantity_kMT ELSE 0 END) 
, FeedProdLoss		= -SUM(CASE WHEN q.StreamId = 'FeedProdLoss' THEN q.Quantity_kMT ELSE 0 END) 
, FeedLiqOther1		= SUM(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 1)%' THEN q.Quantity_kMT ELSE 0 END) 
, FeedLiqOther2		= SUM(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 2)%' THEN q.Quantity_kMT ELSE 0 END) 
, FeedLiqOther3		= SUM(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 3)%' THEN q.Quantity_kMT ELSE 0 END) 
, ProdOther1		= -SUM(CASE WHEN q.StreamId = 'ProdOther' AND q.StreamDescription like '%(Other Prod 1)%' THEN q.Quantity_kMT ELSE 0 END) 
, ProdOther2		= -SUM(CASE WHEN q.StreamId = 'ProdOther' AND q.StreamDescription like '%(Other Prod 2)%' THEN q.Quantity_kMT ELSE 0 END)
, @C2H6
, @C3H8
, @C4H10
, COALESCE(@C2H6, 0.0) + COALESCE(@C3H8, 0.0) + COALESCE(@C4H10, 0.0)
FROM calc.PricingPlantStreamAggregate q WHERE q.Refnum=@Refnum and q.FactorSetId = @FactorSetId

DECLARE @Mult real
Select @Mult = [$(DbGlobal)].dbo.UnitsConv(1.0, 'KMT', 'MLB')

Insert into dbo.Yield (Refnum
, DataType
, Ethane
, EPMix
, Propane
, LPG
, Butane
, FeedLtOther
, NaphthaLt
, NaphthaFr
, NaphthaHv
, Condensate
, HeavyNGL
, Raffinate
, Diesel
, GasOilHv
, GasOilHt
, FeedLiqOther
, FreshPyroFeed
, ConcEthylene
, ConcPropylene
, ConcButadiene
, ConcBenzene
, ROGHydrogen
, ROGMethane
, ROGEthane
, ROGEthylene
, ROGPropylene
, ROGPropane
, ROGC4Plus
, ROGInerts
, DilHydrogen
, DilMethane
, DilEthane
, DilEthylene
, DilPropane
, DilPropylene
, DilButane
, DilButylene
, DilButadiene
, DilBenzene
, DilMogas
, SuppWashOil
, SuppGasOil
, SuppOther
, SuppTot
, PlantFeed
, Hydrogen
, Methane
, Acetylene
, EthylenePG
, EthyleneCG
, PropylenePG
, PropyleneCG
, PropyleneRG
, PropaneC3Resid
, Butadiene
, IsoButylene
, C4Oth
, Benzene
, PyroGasoline
, PyroGasOil
, PyroFuelOil
, AcidGas
, PPFC
, ProdOther
, Prod
, LossFlareVent
, LossOther
, LossMeasure
, Loss
, ProdLoss
, FeedProdLoss
, FeedLiqOther1, FeedLiqOther2, FeedLiqOther3
, ProdOther1, ProdOther2
, [RecSuppEthane], [RecSuppPropane], [RecSuppButane], [Recycle]
)
SELECT Refnum = @Refnum
, DataType					= 'MLb'
, Ethane =  Ethane * @Mult
, EPMix =  EPMix * @Mult
, Propane =  Propane * @Mult
, LPG =  LPG * @Mult
, Butane =  Butane * @Mult
, FeedLtOther =  FeedLtOther * @Mult
, NaphthaLt =  NaphthaLt * @Mult
, NaphthaFr =  NaphthaFr * @Mult
, NaphthaHv =  NaphthaHv * @Mult
, Condensate =  Condensate * @Mult
, HeavyNGL =  HeavyNGL * @Mult
, Raffinate =  Raffinate * @Mult
, Diesel =  Diesel * @Mult
, GasOilHv =  GasOilHv * @Mult
, GasOilHt =  GasOilHt * @Mult
, FeedLiqOther =  FeedLiqOther * @Mult
, FreshPyroFeed =  FreshPyroFeed * @Mult
, ConcEthylene =  ConcEthylene * @Mult
, ConcPropylene =  ConcPropylene * @Mult
, ConcButadiene =  ConcButadiene * @Mult
, ConcBenzene =  ConcBenzene * @Mult
, ROGHydrogen =  ROGHydrogen * @Mult
, ROGMethane =  ROGMethane * @Mult
, ROGEthane =  ROGEthane * @Mult
, ROGEthylene =  ROGEthylene * @Mult
, ROGPropylene =  ROGPropylene * @Mult
, ROGPropane =  ROGPropane * @Mult
, ROGC4Plus =  ROGC4Plus * @Mult
, ROGInerts =  ROGInerts * @Mult
, DilHydrogen =  DilHydrogen * @Mult
, DilMethane =  DilMethane * @Mult
, DilEthane =  DilEthane * @Mult
, DilEthylene =  DilEthylene * @Mult
, DilPropane =  DilPropane * @Mult
, DilPropylene =  DilPropylene * @Mult
, DilButane =  DilButane * @Mult
, DilButylene =  DilButylene * @Mult
, DilButadiene =  DilButadiene * @Mult
, DilBenzene =  DilBenzene * @Mult
, DilMogas =  DilMogas * @Mult
, SuppWashOil =  SuppWashOil * @Mult
, SuppGasOil =  SuppGasOil * @Mult
, SuppOther =  SuppOther * @Mult
, SuppTot =  SuppTot * @Mult
, PlantFeed =  PlantFeed * @Mult
, Hydrogen =  Hydrogen * @Mult
, Methane =  Methane * @Mult
, Acetylene =  Acetylene * @Mult
, EthylenePG =  EthylenePG * @Mult
, EthyleneCG =  EthyleneCG * @Mult
, PropylenePG =  PropylenePG * @Mult
, PropyleneCG =  PropyleneCG * @Mult
, PropyleneRG =  PropyleneRG * @Mult
, PropaneC3Resid =  PropaneC3Resid * @Mult
, Butadiene =  Butadiene * @Mult
, IsoButylene =  IsoButylene * @Mult
, C4Oth =  C4Oth * @Mult
, Benzene =  Benzene * @Mult
, PyroGasoline =  PyroGasoline * @Mult
, PyroGasOil =  PyroGasOil * @Mult
, PyroFuelOil =  PyroFuelOil * @Mult
, AcidGas =  AcidGas * @Mult
, PPFC =  PPFC * @Mult
, ProdOther =  ProdOther * @Mult
, Prod =  Prod * @Mult
, LossFlareVent =  LossFlareVent * @Mult
, LossOther =  LossOther * @Mult
, LossMeasure =  LossMeasure * @Mult
, Loss =  Loss * @Mult
, ProdLoss =  ProdLoss * @Mult
, FeedProdLoss =  FeedProdLoss * @Mult
, FeedLiqOther1 =  FeedLiqOther1 * @Mult
, FeedLiqOther2 =  FeedLiqOther2 * @Mult
, FeedLiqOther3 =  FeedLiqOther3 * @Mult
, ProdOther1 =  ProdOther1 * @Mult
, ProdOther2 =  ProdOther2 * @Mult
, [RecSuppEthane] =  [RecSuppEthane] * @Mult
, [RecSuppPropane] =  [RecSuppPropane] * @Mult
, [RecSuppButane] =  [RecSuppButane] * @Mult
, [Recycle] =  [Recycle] * @Mult
FROM dbo.Yield where Refnum = @Refnum and DataType = 'kMT'


Insert into dbo.Yield (Refnum
, DataType
, Ethane
, EPMix
, Propane
, LPG
, Butane
, FeedLtOther
, NaphthaLt
, NaphthaFr
, NaphthaHv
, Condensate
, HeavyNGL
, Raffinate
, Diesel
, GasOilHv
, GasOilHt
, FeedLiqOther
, FreshPyroFeed
, ConcEthylene
, ConcPropylene
, ConcButadiene
, ConcBenzene
, ROGHydrogen
, ROGMethane
, ROGEthane
, ROGEthylene
, ROGPropylene
, ROGPropane
, ROGC4Plus
, ROGInerts
, DilHydrogen
, DilMethane
, DilEthane
, DilEthylene
, DilPropane
, DilPropylene
, DilButane
, DilButylene
, DilButadiene
, DilBenzene
, DilMogas
, SuppWashOil
, SuppGasOil
, SuppOther
, SuppTot
, PlantFeed
, Hydrogen
, Methane
, Acetylene
, EthylenePG
, EthyleneCG
, PropylenePG
, PropyleneCG
, PropyleneRG
, PropaneC3Resid
, Butadiene
, IsoButylene
, C4Oth
, Benzene
, PyroGasoline
, PyroGasOil
, PyroFuelOil
, AcidGas
, PPFC
, ProdOther
, Prod
, LossFlareVent
, LossOther
, LossMeasure
, Loss
, ProdLoss
, FeedProdLoss
, FeedLiqOther1, FeedLiqOther2, FeedLiqOther3
, ProdOther1, ProdOther2
)

SELECT Refnum = @Refnum
, DataType					= '$PerMT'
, Ethane			= SUM(CASE WHEN q.StreamId = 'Ethane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, EPMix				= SUM(CASE WHEN q.StreamId = 'EPMix' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Propane			= SUM(CASE WHEN q.StreamId = 'Propane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, LPG				= SUM(CASE WHEN q.StreamId = 'LPG' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Butane			= SUM(CASE WHEN q.StreamId = 'Butane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, FeedLtOther		= SUM(CASE WHEN q.StreamId = 'FeedLtOther' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, NaphthaLt			= SUM(CASE WHEN q.StreamId = 'NaphthaLt' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, NaphthaFr			= SUM(CASE WHEN q.StreamId = 'NaphthaFr' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, NaphthaHv			= SUM(CASE WHEN q.StreamId = 'NaphthaHv' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Condensate		= SUM(CASE WHEN q.StreamId = 'Condensate' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, HeavyNGL			= SUM(CASE WHEN q.StreamId = 'HeavyNGL' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Raffinate			= SUM(CASE WHEN q.StreamId = 'Raffinate' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Diesel			= SUM(CASE WHEN q.StreamId = 'Diesel' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, GasOilHv			= SUM(CASE WHEN q.StreamId = 'GasOilHv' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, GasOilHt			= SUM(CASE WHEN q.StreamId = 'GasOilHt' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, FeedLiqOther		= [$(DbGlobal)].dbo.WtAvg(q.Avg_Calc_Amount_Cur, CASE WHEN q.StreamId = 'FeedLiqOther' AND q.Quantity_kMT <> 0 THEN q.Quantity_kMT ELSE 0 END) 
, FreshPyroFeed		= SUM(CASE WHEN q.StreamId = 'FreshPyroFeed' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ConcEthylene		= SUM(CASE WHEN q.StreamId = 'ConcEthylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ConcPropylene		= SUM(CASE WHEN q.StreamId = 'ConcPropylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ConcButadiene		= SUM(CASE WHEN q.StreamId = 'ConcButadiene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ConcBenzene		= SUM(CASE WHEN q.StreamId = 'ConcBenzene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGHydrogen		= SUM(CASE WHEN q.StreamId = 'ROGHydrogen' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGMethane		= SUM(CASE WHEN q.StreamId = 'ROGMethane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGEthane			= SUM(CASE WHEN q.StreamId = 'ROGEthane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGEthylene		= SUM(CASE WHEN q.StreamId = 'ROGEthylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGPropylene		= SUM(CASE WHEN q.StreamId = 'ROGPropylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGPropane		= SUM(CASE WHEN q.StreamId = 'ROGPropane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGC4Plus			= SUM(CASE WHEN q.StreamId = 'ROGC4Plus' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ROGInerts			= SUM(CASE WHEN q.StreamId = 'ROGInerts' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilHydrogen		= SUM(CASE WHEN q.StreamId = 'DilHydrogen' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilMethane		= SUM(CASE WHEN q.StreamId = 'DilMethane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilEthane			= SUM(CASE WHEN q.StreamId = 'DilEthane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilEthylene		= SUM(CASE WHEN q.StreamId = 'DilEthylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilPropane		= SUM(CASE WHEN q.StreamId = 'DilPropane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilPropylene		= SUM(CASE WHEN q.StreamId = 'DilPropylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilButane			= SUM(CASE WHEN q.StreamId = 'DilButane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilButylene		= SUM(CASE WHEN q.StreamId = 'DilButylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilButadiene		= SUM(CASE WHEN q.StreamId = 'DilButadiene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilBenzene		= SUM(CASE WHEN q.StreamId = 'DilBenzene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, DilMogas			= SUM(CASE WHEN q.StreamId = 'DilMogas' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, SuppWashOil		= SUM(CASE WHEN q.StreamId = 'SuppWashOil' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, SuppGasOil		= SUM(CASE WHEN q.StreamId = 'SuppGasOil' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, SuppOther			= SUM(CASE WHEN q.StreamId = 'SuppOther' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, SuppTot			= SUM(CASE WHEN q.StreamId = 'SuppTot' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PlantFeed			= SUM(CASE WHEN q.StreamId = 'PlantFeed' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Hydrogen			= SUM(CASE WHEN q.StreamId = 'Hydrogen' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Methane			= SUM(CASE WHEN q.StreamId = 'Methane' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Acetylene			= SUM(CASE WHEN q.StreamId = 'Acetylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, EthylenePG		= SUM(CASE WHEN q.StreamId = 'EthylenePG' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, EthyleneCG		= SUM(CASE WHEN q.StreamId = 'EthyleneCG' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PropylenePG		= SUM(CASE WHEN q.StreamId = 'PropylenePG' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PropyleneCG		= SUM(CASE WHEN q.StreamId = 'PropyleneCG' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PropyleneRG		= SUM(CASE WHEN q.StreamId = 'PropyleneRG' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PropaneC3Resid	= SUM(CASE WHEN q.StreamId = 'PropaneC3Resid' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Butadiene			= SUM(CASE WHEN q.StreamId = 'Butadiene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, IsoButylene		= SUM(CASE WHEN q.StreamId = 'IsoButylene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, C4Oth				= SUM(CASE WHEN q.StreamId = 'C4Oth' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Benzene			= SUM(CASE WHEN q.StreamId = 'Benzene' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PyroGasoline		= SUM(CASE WHEN q.StreamId = 'PyroGasoline' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PyroGasOil		= SUM(CASE WHEN q.StreamId = 'PyroGasOil' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PyroFuelOil		= SUM(CASE WHEN q.StreamId = 'PyroFuelOil' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, AcidGas			= SUM(CASE WHEN q.StreamId = 'AcidGas' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, PPFC				= SUM(CASE WHEN q.StreamId = 'PPFC' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ProdOther			= [$(DbGlobal)].dbo.WtAvg(q.Avg_Calc_Amount_Cur, CASE WHEN q.StreamId = 'ProdOther' AND q.Quantity_kMT <> 0 THEN q.Quantity_kMT ELSE 0 END) 
, Prod				= SUM(CASE WHEN q.StreamId = 'Prod' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, LossFlareVent		= SUM(CASE WHEN q.StreamId = 'LossFlareVent' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, LossOther			= SUM(CASE WHEN q.StreamId = 'LossOther' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, LossMeasure		= SUM(CASE WHEN q.StreamId = 'LossMeasure' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, Loss				= SUM(CASE WHEN q.StreamId = 'Loss' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ProdLoss			= SUM(CASE WHEN q.StreamId = 'ProdLoss' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, FeedProdLoss		= SUM(CASE WHEN q.StreamId = 'FeedProdLoss' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, FeedLiqOther1		= SUM(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 1)%' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, FeedLiqOther2		= SUM(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 2)%' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, FeedLiqOther3		= SUM(CASE WHEN q.StreamId = 'FeedLiqOther' AND q.StreamDescription like '%(Other Feed 3)%' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ProdOther1		= SUM(CASE WHEN q.StreamId = 'ProdOther' AND q.StreamDescription like '%(Other Prod 1)%' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
, ProdOther2		= SUM(CASE WHEN q.StreamId = 'ProdOther' AND q.StreamDescription like '%(Other Prod 2)%' THEN q.Avg_Calc_Amount_Cur ELSE 0 END) 
FROM calc.PricingPlantStreamAggregate q WHERE q.Refnum=@Refnum and q.FactorSetId=@FactorSetId and q.ScenarioId = 'Basis' and q.CurrencyRpt = 'USD'
SET NOCOUNT OFF
