﻿
CREATE VIEW dbo.[MaintPersAggregate]
AS
SELECT
c.Refnum
,AccountId				= ISNULL(c.PersId, 'Tot')
,StraightTimePrev_Hrs		= SUM(p.StraightTime_Hrs)
,TotCompPrev_Hrs			= SUM(p.StraightTime_Hrs) + ISNULL(SUM(p.OverTime_Hrs),0)
,StraightTime_Hrs			= SUM(c.StraightTime_Hrs)
,TotComp_Hrs				= SUM(c.StraightTime_Hrs) + ISNULL(SUM(c.OverTime_Hrs),0)
,StraightTime_Avg_Hrs		= (SUM(p.StraightTime_Hrs) + SUM(c.StraightTime_Hrs)) / 2.0
,TotComp_Avg_Hrs			= (SUM(p.StraightTime_Hrs) + SUM(c.StraightTime_Hrs)+ ISNULL(SUM(p.OverTime_Hrs),0) + ISNULL(SUM(c.OverTime_Hrs),0)) / 2.0
								
FROM fact.PersAggregate		c	WITH (NOEXPAND)
INNER JOIN fact.Pers		p
	ON	p.Refnum = c.Refnum
	AND p.PersId = c.PersId
	AND	p.CalDateKey = (c.CalDateKey - 10000)
INNER JOIN dbo.TSort t on t.Refnum=p.Refnum and c.FactorSetId=t.FactorSetId
GROUP BY
	c.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	p.CalDateKey,
	ROLLUP(c.PersId);
