﻿CREATE VIEW fact.MaintPersAggregate
WITH SCHEMABINDING
AS
SELECT
	c.FactorSetId,
	c.Refnum,
	ISNULL(c.PersId, 'Tot')	[AccountId],
	p.CalDateKey			[CalDateKeyPrev],
	SUM(p.StraightTime_Hrs)	[StraightTimePrev_Hrs],
	c.CalDateKey,
	SUM(c.StraightTime_Hrs)	[StraightTime_Hrs],
	(SUM(p.StraightTime_Hrs) + SUM(c.StraightTime_Hrs)) / 2.0
								[StraightTime_Avg_Hrs]
FROM fact.PersAggregate		c	WITH (NOEXPAND)
INNER JOIN fact.Pers		p
	ON	p.Refnum = c.Refnum
	AND p.PersId = c.PersId
	AND	p.CalDateKey = (c.CalDateKey - 10000)
GROUP BY
	c.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	p.CalDateKey,
	ROLLUP(c.PersId);
