﻿
CREATE VIEW fact.MaintExpenseAggregate
WITH SCHEMABINDING
AS
SELECT
	  m.Refnum
	, m.CalDateKey
	, ISNULL(m.AccountId, 'Maint')	[AccountId]
	, SUM(CompMaintHours_Pcnt)		[CompMaintHours_Pcnt]
FROM fact.MaintExpense m
GROUP BY
	  m.Refnum
	, m.CalDateKey
	, ROLLUP(m.AccountId);
