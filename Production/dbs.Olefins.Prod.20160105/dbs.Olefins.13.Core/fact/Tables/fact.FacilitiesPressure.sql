﻿CREATE TABLE [fact].[FacilitiesPressure] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [Pressure_PSIg]  REAL               NULL,
    [Rate_kLbHr]     REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesPressure_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FacilitiesPressure_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FacilitiesPressure_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FacilitiesPressure_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FacilitiesPressure] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FacilitiesPressure_Pressure_PSIg] CHECK ([Pressure_PSIg]>=(0.0)),
    CONSTRAINT [CR_FacilitiesPressure_Rate_kLbHr] CHECK ([Rate_kLbHr]>=(0.0)),
    CONSTRAINT [FK_FacilitiesPressure_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FacilitiesPressure_Facilities] FOREIGN KEY ([Refnum], [FacilityId], [CalDateKey]) REFERENCES [fact].[Facilities] ([Refnum], [FacilityId], [CalDateKey]),
    CONSTRAINT [FK_FacilitiesPressure_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesPressure_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_FacilitiesPressure_Calc_Edc]
    ON [fact].[FacilitiesPressure]([Rate_kLbHr] ASC)
    INCLUDE([Refnum], [CalDateKey], [FacilityId]);


GO

CREATE TRIGGER [fact].[t_FacilitiesPressure_u]
	ON [fact].[FacilitiesPressure]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesPressure]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FacilitiesPressure].Refnum		= INSERTED.Refnum
		AND [fact].[FacilitiesPressure].FacilityId	= INSERTED.FacilityId
		AND [fact].[FacilitiesPressure].CalDateKey	= INSERTED.CalDateKey;

END;