﻿CREATE TABLE [fact].[ApcControllers] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [ApcId]            VARCHAR (42)       NOT NULL,
    [Controller_Count] INT                NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_ApcControllers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_ApcControllers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_ApcControllers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_ApcControllers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ApcControllers] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ApcId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ApcControllers_Controller_Count] CHECK ([Controller_Count]>=(0)),
    CONSTRAINT [FK_ApcControllers_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_ApcControllers_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ApcControllers_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ApcControllers_u]
	ON [fact].[ApcControllers]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ApcControllers]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ApcControllers].Refnum		= INSERTED.Refnum
		AND [fact].[ApcControllers].ApcId		= INSERTED.ApcId
		AND [fact].[ApcControllers].CalDateKey	= INSERTED.CalDateKey;

END;