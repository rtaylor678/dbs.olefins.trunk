﻿CREATE TABLE [fact].[ReliabilityOppLossNameOther] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [OppLossId]      VARCHAR (42)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [OppLossName]    NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityOppLossNameOther_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLossNameOther_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLossNameOther_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLossNameOther_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityOppLossNameOther] PRIMARY KEY CLUSTERED ([Refnum] ASC, [OppLossId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_ReliabilityOppLossNameOther_OppLossName] CHECK ([OppLossName]<>''),
    CONSTRAINT [FK_ReliabilityOppLossNameOther_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityOppLossNameOther_ReliabilityOppLoss] FOREIGN KEY ([Refnum], [StreamId], [OppLossId], [CalDateKey]) REFERENCES [fact].[ReliabilityOppLoss] ([Refnum], [StreamId], [OppLossId], [CalDateKey]),
    CONSTRAINT [FK_ReliabilityOppLossNameOther_ReliabilityOppLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLossNameOther_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReliabilityOppLossNameOther_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityOppLossNameOther_u]
	ON [fact].[ReliabilityOppLossNameOther]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityOppLossNameOther]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityOppLossNameOther].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityOppLossNameOther].OppLossId	= INSERTED.OppLossId
		AND [fact].[ReliabilityOppLossNameOther].StreamId	= INSERTED.StreamId
		AND [fact].[ReliabilityOppLossNameOther].CalDateKey	= INSERTED.CalDateKey;

END;