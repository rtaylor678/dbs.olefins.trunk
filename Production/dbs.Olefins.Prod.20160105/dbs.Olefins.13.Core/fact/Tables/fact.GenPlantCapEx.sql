﻿CREATE TABLE [fact].[GenPlantCapEx] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_Cur]     REAL               NULL,
    [Budget_Cur]     REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantCapEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_GenPlantCapEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_GenPlantCapEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_GenPlantCapEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantCapEx] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantCapEx_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_GenPlantCapEx_Budget_Cur] CHECK ([Budget_Cur]>=(0.0)),
    CONSTRAINT [FK_GenPlantCapEx_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_GenPlantCapEx_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantCapEx_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_GenPlantCapEx_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantCapEx_u]
	ON [fact].[GenPlantCapEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantCapEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantCapEx].Refnum			= INSERTED.Refnum
		AND [fact].[GenPlantCapEx].CurrencyRpt		= INSERTED.CurrencyRpt
		AND [fact].[GenPlantCapEx].AccountId		= INSERTED.AccountId
		AND [fact].[GenPlantCapEx].CalDateKey		= INSERTED.CalDateKey;

END;