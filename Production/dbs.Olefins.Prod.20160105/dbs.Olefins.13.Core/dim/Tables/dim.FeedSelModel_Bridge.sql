﻿CREATE TABLE [dim].[FeedSelModel_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [ModelId]            VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_FeedSelModel_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedSelModel_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_FeedSelModel_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_FeedSelModel_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_FeedSelModel_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedSelModel_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ModelId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_FeedSelModel_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_FeedSelModel_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[FeedSelModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_FeedSelModel_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_FeedSelModel_Bridge_LookUp_ModelId] FOREIGN KEY ([ModelId]) REFERENCES [dim].[FeedSelModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_FeedSelModel_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [ModelId]) REFERENCES [dim].[FeedSelModel_Parent] ([FactorSetId], [ModelId]),
    CONSTRAINT [FK_FeedSelModel_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[FeedSelModel_Parent] ([FactorSetId], [ModelId])
);


GO

CREATE TRIGGER [dim].[t_FeedSelModel_Bridge_u]
ON [dim].[FeedSelModel_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedSelModel_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedSelModel_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[FeedSelModel_Bridge].[ModelId]		= INSERTED.[ModelId]
		AND	[dim].[FeedSelModel_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;