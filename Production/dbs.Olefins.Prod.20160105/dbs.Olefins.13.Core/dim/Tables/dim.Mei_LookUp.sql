﻿CREATE TABLE [dim].[Mei_LookUp] (
    [MeiId]          VARCHAR (42)       NOT NULL,
    [MeiName]        NVARCHAR (84)      NOT NULL,
    [MeiDetail]      NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Mei_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Mei_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Mei_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Mei_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Mei_LookUp] PRIMARY KEY CLUSTERED ([MeiId] ASC),
    CONSTRAINT [CL_Mei_LookUp_MeiDetail] CHECK ([MeiDetail]<>''),
    CONSTRAINT [CL_Mei_LookUp_MeiId] CHECK ([MeiId]<>''),
    CONSTRAINT [CL_Mei_LookUp_MeiName] CHECK ([MeiName]<>''),
    CONSTRAINT [UK_Mei_LookUp_MeiDetail] UNIQUE NONCLUSTERED ([MeiDetail] ASC),
    CONSTRAINT [UK_Mei_LookUp_MeiName] UNIQUE NONCLUSTERED ([MeiName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Mei_LookUp_u]
	ON [dim].[Mei_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Mei_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Mei_LookUp].[MeiId]		= INSERTED.[MeiId];
		
END;
