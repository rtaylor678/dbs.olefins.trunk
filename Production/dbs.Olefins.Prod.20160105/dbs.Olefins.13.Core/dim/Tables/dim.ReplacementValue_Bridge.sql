﻿CREATE TABLE [dim].[ReplacementValue_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [ReplacementValueId] VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ReplacementValue_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReplacementValue_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReplacementValue_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReplacementValue_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReplacementValue_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReplacementValue_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ReplacementValueId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ReplacementValue_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ReplacementValue_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ReplacementValue_LookUp] ([ReplacementValueId]),
    CONSTRAINT [FK_ReplacementValue_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReplacementValue_Bridge_LookUp_ReplacementValue] FOREIGN KEY ([ReplacementValueId]) REFERENCES [dim].[ReplacementValue_LookUp] ([ReplacementValueId]),
    CONSTRAINT [FK_ReplacementValue_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [ReplacementValueId]) REFERENCES [dim].[ReplacementValue_Parent] ([FactorSetId], [ReplacementValueId]),
    CONSTRAINT [FK_ReplacementValue_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ReplacementValue_Parent] ([FactorSetId], [ReplacementValueId])
);


GO

CREATE TRIGGER [dim].[t_ReplacementValue_Bridge_u]
ON [dim].[ReplacementValue_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReplacementValue_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReplacementValue_Bridge].[FactorSetId]			= INSERTED.[FactorSetId]
		AND	[dim].[ReplacementValue_Bridge].[ReplacementValueId]	= INSERTED.[ReplacementValueId]
		AND	[dim].[ReplacementValue_Bridge].[DescendantId]			= INSERTED.[DescendantId];

END;