﻿CREATE TABLE [dim].[Region_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [RegionId]           VARCHAR (5)         NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (5)         NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Region_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Region_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Region_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Region_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Region_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Region_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [RegionId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Region_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Region_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_Region_Bridge_FactorSetLu] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Region_Bridge_LookUp_Region] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_Region_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [RegionId]) REFERENCES [dim].[Region_Parent] ([FactorSetId], [RegionId]),
    CONSTRAINT [FK_Region_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Region_Parent] ([FactorSetId], [RegionId])
);


GO

CREATE TRIGGER [dim].[t_Region_Bridge_u]
ON [dim].[Region_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Region_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Region_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Region_Bridge].[RegionId]	= INSERTED.[RegionId]
		AND	[dim].[Region_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;