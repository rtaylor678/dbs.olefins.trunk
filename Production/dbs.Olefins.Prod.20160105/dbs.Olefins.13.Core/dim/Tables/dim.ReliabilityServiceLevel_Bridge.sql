﻿CREATE TABLE [dim].[ReliabilityServiceLevel_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [ServiceLevelId]     VARCHAR (14)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (14)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ReliabilityServiceLevel_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityServiceLevel_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityServiceLevel_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityServiceLevel_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReliabilityServiceLevel_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityServiceLevel_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ServiceLevelId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ReliabilityServiceLevel_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ReliabilityServiceLevel_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ReliabilityServiceLevel_LookUp] ([ServiceLevelId]),
    CONSTRAINT [FK_ReliabilityServiceLevel_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityServiceLevel_Bridge_LookUp_ServiceLevel] FOREIGN KEY ([ServiceLevelId]) REFERENCES [dim].[ReliabilityServiceLevel_LookUp] ([ServiceLevelId]),
    CONSTRAINT [FK_ReliabilityServiceLevel_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [ServiceLevelId]) REFERENCES [dim].[ReliabilityServiceLevel_Parent] ([FactorSetId], [ServiceLevelId]),
    CONSTRAINT [FK_ReliabilityServiceLevel_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ReliabilityServiceLevel_Parent] ([FactorSetId], [ServiceLevelId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityServiceLevel_Bridge_u]
ON [dim].[ReliabilityServiceLevel_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityServiceLevel_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityServiceLevel_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityServiceLevel_Bridge].[ServiceLevelId]		= INSERTED.[ServiceLevelId]
		AND	[dim].[ReliabilityServiceLevel_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;