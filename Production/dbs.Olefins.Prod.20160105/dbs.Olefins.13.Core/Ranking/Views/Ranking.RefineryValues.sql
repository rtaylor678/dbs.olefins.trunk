﻿










CREATE VIEW [Ranking].[RefineryValues]
AS
SELECT Refnum, RankVariable, Value, PcntFactor = (Select MAX(kEdc) from dbo.GENSUM gs where gs.Refnum = unp.Refnum)
FROM (SELECT [Refnum],[kEdc],[kUEdc],[EthyleneProd_kMT],[PropyleneProd_kMT],[OlefinsProd_kMT],[HvcProd_kMT],[TotalPlantInput_kMT],[PlantFeed_kMT],[FreshPyroFeed_kMT]
      ,[RV_MUS],[RVUSGC_MUS],[TAAdj_kUEDC],[EthyleneCapYE_kMT],[EthyleneCpbyUtil_pcnt],[OlefinsCpbyUtil_pcnt],[EthyleneCpbyUtilTAAdj_pcnt],[OlefinsCpbyUtilTAAdj_pcnt]
      ,[EthyleneCapUtil_pcnt],[OlefinsCapUtil_pcnt],[EthyleneCapUtilTAAdj_pcnt],[OlefinsCapUtilTAAdj_pcnt]
      ,[NetEnergyCons_MBtuperUEDC],[NetEnergyCons_BTUperHVCLb],[NetEnergyCons_GJperHVCMt],[EII],[EEI_SPSL],[EEI_PYPS]
      ,[TotPersEDC],[TotPersHVCMt],PEI, mPEI, nmPEI, [PersCost_CurrPerEDC],[PersCost_CurrPerHVC_MT]
      ,[MaintIndexEDC],[MaintIndexRV],[RelInd], MechAvail, OpAvail, PlantAvail, MechAvailSlow, OpAvailSlow, PlantAvailSlow,[RMeiEDC],[RMeiRV], MEI
      ,[C2H4_OSOP],[Chem_OSOP],[C2H4YIR_MS25_SPSL],[HVCProdYIR_MS25_SPSL],[C2H4YIR_OSOP_SPSL],[HVCProdYIR_OSOP_SPSL],[C2H4YIR_OS25_SPSL],[HVCProdYIR_OS25_SPSL],[SeverityIndex_SPSL]
      ,[C2H4YIR_MS25_PYPS],[HVCProdYIR_MS25_PYPS],[C2H4YIR_OSOP_PYPS],[HVCProdYIR_OSOP_PYPS],[C2H4YIR_OS25_PYPS],[HVCProdYIR_OS25_PYPS],[SeverityIndex_PYPS]
      ,[ActPPV_PcntOS25_SPSL],[ActPPV_PcntMS25_SPSL],[ActPPV_PcntOS25_PYPS],[ActPPV_PcntMS25_PYPS],[PhysLoss_Pcnt]
      ,[NEOpExEDC],NEI, [TotCashOpExUEDC],[TotRefExpUEDC],[NEOpExHVCLb],[TotCashOpExHVCLb],[TotRefExpHVCLb],[NEOpExHVCMt],[TotCashOpExHVCMt],[TotRefExpHVCMt]
      ,[EthyleneProdCostPerUEDC],[OlefinsProdCostPerUEDC],[HVCProdCostPerUEDC],[EthyleneProdCostPerMT],[OlefinsProdCostPerMT],[HVCProdCostPerMT]
      ,[EthyleneProdCentsPerLB],[OlefinsProdCentsPerLB],[HVCProdCentsPerLB]
      ,[NCMPerEthyleneMT],[NCMPerOlefinsMT],[NCMPerHVCMT],[NCMPerEthyleneLB],[NCMPerOlefinsLB],[NCMPerHVCLB]
      ,[GMPerRV],[ROI],[ROITAAdj],[GMPerRVUSGC],[NCMPerRVUSGC],[NCMPerRVUSGCTAAdj]
      ,[ProformaOpExPerUEDC],[ProformaOpExPerHVCMT],[ProformaProdCostPerEthyleneMT],[ProformaProdCostPerHVCMT],[ProformaNCMPerEthyleneMT],[ProformaNCMPerHVCMT]
      ,[ProformaROI],[ProformaOpExPerHVCLb],[ProformaProdCostPerEthyleneLb],[ProformaProdCostPerHVCLb],[ProformaNCMPerEthyleneLb],[ProformaNCMPerHVCLb]
      ,[APCImplIndex],[APCOnLineIndex]
FROM dbo.GENSUM) g
UNPIVOT (Value FOR RankVariable IN ([kEdc],[kUEdc],[EthyleneProd_kMT],[PropyleneProd_kMT],[OlefinsProd_kMT],[HvcProd_kMT],[TotalPlantInput_kMT],[PlantFeed_kMT],[FreshPyroFeed_kMT]
      ,[RV_MUS],[RVUSGC_MUS],[TAAdj_kUEDC],[EthyleneCapYE_kMT],[EthyleneCpbyUtil_pcnt],[OlefinsCpbyUtil_pcnt],[EthyleneCpbyUtilTAAdj_pcnt],[OlefinsCpbyUtilTAAdj_pcnt]
      ,[EthyleneCapUtil_pcnt],[OlefinsCapUtil_pcnt],[EthyleneCapUtilTAAdj_pcnt],[OlefinsCapUtilTAAdj_pcnt]
      ,[NetEnergyCons_MBtuperUEDC],[NetEnergyCons_BTUperHVCLb],[NetEnergyCons_GJperHVCMt],[EII],[EEI_SPSL],[EEI_PYPS]
      ,[TotPersEDC],[TotPersHVCMt],PEI, mPEI, nmPEI, [PersCost_CurrPerEDC],[PersCost_CurrPerHVC_MT]
      ,[MaintIndexEDC],[MaintIndexRV],[RelInd], MechAvail, OpAvail, PlantAvail, MechAvailSlow, OpAvailSlow, PlantAvailSlow, [RMeiEDC],[RMeiRV], MEI
      ,[C2H4_OSOP],[Chem_OSOP],[C2H4YIR_MS25_SPSL],[HVCProdYIR_MS25_SPSL],[C2H4YIR_OSOP_SPSL],[HVCProdYIR_OSOP_SPSL],[C2H4YIR_OS25_SPSL],[HVCProdYIR_OS25_SPSL],[SeverityIndex_SPSL]
      ,[C2H4YIR_MS25_PYPS],[HVCProdYIR_MS25_PYPS],[C2H4YIR_OSOP_PYPS],[HVCProdYIR_OSOP_PYPS],[C2H4YIR_OS25_PYPS],[HVCProdYIR_OS25_PYPS],[SeverityIndex_PYPS]
      ,[ActPPV_PcntOS25_SPSL],[ActPPV_PcntMS25_SPSL],[ActPPV_PcntOS25_PYPS],[ActPPV_PcntMS25_PYPS],[PhysLoss_Pcnt]
      ,[NEOpExEDC],NEI, [TotCashOpExUEDC],[TotRefExpUEDC],[NEOpExHVCLb],[TotCashOpExHVCLb],[TotRefExpHVCLb],[NEOpExHVCMt],[TotCashOpExHVCMt],[TotRefExpHVCMt]
      ,[EthyleneProdCostPerUEDC],[OlefinsProdCostPerUEDC],[HVCProdCostPerUEDC],[EthyleneProdCostPerMT],[OlefinsProdCostPerMT],[HVCProdCostPerMT]
      ,[EthyleneProdCentsPerLB],[OlefinsProdCentsPerLB],[HVCProdCentsPerLB]
      ,[NCMPerEthyleneMT],[NCMPerOlefinsMT],[NCMPerHVCMT],[NCMPerEthyleneLB],[NCMPerOlefinsLB],[NCMPerHVCLB]
      ,[GMPerRV],[ROI],[ROITAAdj],[GMPerRVUSGC],[NCMPerRVUSGC],[NCMPerRVUSGCTAAdj]
      ,[ProformaOpExPerUEDC],[ProformaOpExPerHVCMT],[ProformaProdCostPerEthyleneMT],[ProformaProdCostPerHVCMT],[ProformaNCMPerEthyleneMT],[ProformaNCMPerHVCMT]
      ,[ProformaROI],[ProformaOpExPerHVCLb],[ProformaProdCostPerEthyleneLb],[ProformaProdCostPerHVCLb],[ProformaNCMPerEthyleneLb],[ProformaNCMPerHVCLb]
      ,[APCImplIndex],[APCOnLineIndex])) unp

--select * from GENSUM











