﻿CREATE FUNCTION [stat].[ChiSquared_Probability]
(@DegreesOfFreedom INT, @CStatistic FLOAT (53))
RETURNS FLOAT (53)
AS
 EXTERNAL NAME [Sa.Meta.Numerics].[UserDefinedFunctions].[ChiSquared_Probability]

