﻿CREATE PROCEDURE [Console].[GetCompanyContactInfo] 
	@RefNum varchar(18), 
	@ContactType nvarchar(10),
	@StudyYear nvarchar(4)
	AS
	

BEGIN
IF @ContactType='COORD'  
BEGIN
 

				SELECT FirstName ,
			LastName ,
			ContactType,
			cc.Email ,
			cc.Phone ,
			cc.Fax ,
			cc.MailAddr1 ,
			cc.MailAddr2 ,
			cc.MailAddr3 ,
			MailCity  ,
			MailState ,
			MailZip ,
			MailCountry ,
			StrAddr1,
			StrAddr2,
			StrAdd3,
			StrCity,
			StrState,
			StrZip,
			StrCountry,
			JobTitle,
			SendMethod,
			cc.Password as CompanyPassword,
			SendMethod as CompanySendMethod,
			cc.Comment,
			cc.AltFirstName,
			cc.AltLastName,
			cc.AltEmail,
			null as SANNumber,
			cc.StudyYear
		 FROM CoContactInfo cc right outer join dbo.tsort t on t.contactcode = cc.contactcode and t.StudyYear = cc.StudyYear
			WHERE t.SmallRefnum=@Refnum  and ContactType=@ContactType and cc.StudyYear = @StudyYear 
			END
			ELSE 
			BEGIN
			SET @ContactType = 'ALT'
						SELECT FirstName ,
			LastName ,
			ContactType,
			cc.Email ,
			cc.Phone ,
			cc.Fax ,
			cc.MailAddr1 ,
			cc.MailAddr2 ,
			cc.MailAddr3 ,
			MailCity  ,
			MailState ,
			MailZip ,
			MailCountry ,
			StrAddr1,
			StrAddr2,
			StrAdd3,
			StrCity,
			StrState,
			StrZip,
			StrCountry,
			JobTitle,
			SendMethod,
			cc.PassWord as CompanyPassword,
			SendMethod as CompanySendMethod,
			cc.Comment,
			cc.AltFirstName,
			cc.AltLastName,
			cc.AltEmail,
			null as SANNumber,
			cc.StudyYear 
		 FROM CoContactInfo cc right outer join dbo.tsort t on t.contactcode = cc.contactcode and t.StudyYear = cc.StudyYear
			WHERE t.SmallRefnum=@RefNum  and ContactType=@ContactType
			END
END
