﻿CREATE PROCEDURE [Console].[GetMessageLog]
    @MessageType int,
	@RefNum varchar(18)

AS
BEGIN

	SELECT * FROM dbo.MessageLog
	WHERE (Refnum = @RefNum OR Refnum = dbo.FormatRefNum(@Refnum, 0))
	AND MessageID = @MessageType 
	Order by MessageTime Desc
	
END
