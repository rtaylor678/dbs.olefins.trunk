﻿CREATE PROCEDURE [Console].[GetConsultingOpportunities]

	@RefNum varchar(18),
	@NoteType varchar(20)

AS
BEGIN

	select [Note] from val.Notes  where refnum = @RefNum and NoteType = @NoteType

END
