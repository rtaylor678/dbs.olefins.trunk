﻿CREATE TABLE [ante].[PeerGroupTech] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [PeerGroup]       TINYINT            NOT NULL,
    [LimitLower_Year] SMALLINT           NOT NULL,
    [LimitUpper_Year] SMALLINT           NOT NULL,
    [_LimitRange]     AS                 (((('('+CONVERT([varchar](32),round([LimitLower_Year],(2)),(0)))+', ')+CONVERT([varchar](32),round([LimitUpper_Year],(2)),(0)))+']') PERSISTED NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupTech_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupTech_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupTech_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_PeerGroupTech_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PeerGroupTech] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PeerGroup] ASC),
    CONSTRAINT [CV_PeerGroupTech_Limit] CHECK ([LimitLower_Year]<[LimitUpper_Year]),
    CONSTRAINT [FK_PeerGroupTech_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_PeerGroupTech_u]
	ON [ante].[PeerGroupTech]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PeerGroupTech]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PeerGroupTech].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[PeerGroupTech].PeerGroup	= INSERTED.PeerGroup;

END;