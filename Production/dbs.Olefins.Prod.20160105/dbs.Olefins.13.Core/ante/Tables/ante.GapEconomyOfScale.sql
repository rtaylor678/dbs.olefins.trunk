﻿CREATE TABLE [ante].[GapEconomyOfScale] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [Exponent]       REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_GapEconomyOfScale_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_GapEconomyOfScale_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_GapEconomyOfScale_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_GapEconomyOfScale_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GapEconomyOfScale] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [AccountId] ASC),
    CONSTRAINT [FK_GapEconomyOfScale_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_GapEconomyOfScale_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_GapEconomyOfScale_u]
	ON [ante].[GapEconomyOfScale]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[GapEconomyOfScale]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[GapEconomyOfScale].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[GapEconomyOfScale].[AccountId]		= INSERTED.[AccountId];

END;
