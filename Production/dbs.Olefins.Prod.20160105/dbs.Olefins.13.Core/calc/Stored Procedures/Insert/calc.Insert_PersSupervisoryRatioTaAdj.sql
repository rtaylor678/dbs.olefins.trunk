﻿CREATE PROCEDURE calc.Insert_PersSupervisoryRatioTaAdj
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PersSupervisoryRatioTaAdj(
			FactorSetId, Refnum, CalDateKey, PersIdSec,
			Occ_Count, Mps_Count,
			Occ_Fte, Mps_Fte,
			InflTaAdj_Occ_Fte, InflTaAdj_Mps_Fte
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			o.PersIdSec,
			o.Personnel_Count,
			m.Personnel_Count,
			o.Tot_Fte,
			m.Tot_Fte,
			o.InflTaAdjAnn_Tot_Fte,
			m.InflTaAdjAnn_Tot_Fte
		FROM @fpl												tsq
		INNER JOIN	(
			SELECT 
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey	[CalDateKey],
				l.PersIdSec,
				p.Personnel_Count,
				p.Tot_Fte,
				p.InflTaAdjAnn_Tot_Fte
			FROM @fpl								fpl
			INNER JOIN dim.Pers_Bridge				d
				ON	d.FactorSetId = fpl.FactorSetId
				AND	d.PersId = 'OccSubTotal'
			INNER JOIN dim.Pers_LookUp				l
				ON	l.PersId = d.DescendantId
				AND	l.PersIdPri = 'Occ'
			INNER JOIN calc.PersAggregateAdj		p
				ON	p.FactorSetId = fpl.FactorSetId
				AND	p.Refnum = fpl.Refnum
				AND	p.CalDateKey = fpl.Plant_QtrDateKey
				AND	p.PersId = d.DescendantId
			WHERE	fpl.CalQtr = 4
				AND (p.Personnel_Count		> 0.0
				OR	p.InflTaAdjAnn_Tot_Fte	> 0.0
				OR	p.Tot_Fte				> 0.0)
			)													o
			ON	o.FactorSetId = tsq.FactorSetId
			AND	o.Refnum = tsq.Refnum
			AND	o.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN (
			SELECT 
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey	[CalDateKey],
				l.PersIdSec,
				p.Personnel_Count,
				p.Tot_Fte,
				p.InflTaAdjAnn_Tot_Fte
			FROM @fpl								fpl
			INNER JOIN dim.Pers_Bridge				d
				ON	d.FactorSetId = fpl.FactorSetId
				AND	d.PersId = 'MpsSubTotal'
			INNER JOIN dim.Pers_LookUp				l
				ON	l.PersId = d.DescendantId
				AND	l.PersIdPri = 'Mps'
			INNER JOIN calc.PersAggregateAdj		p
				ON	p.FactorSetId = fpl.FactorSetId
				AND	p.Refnum = fpl.Refnum
				AND	p.CalDateKey = fpl.Plant_QtrDateKey
				AND	p.PersId = d.DescendantId
			WHERE	fpl.CalQtr = 4
				AND (p.Personnel_Count		> 0.0
				OR	p.InflTaAdjAnn_Tot_Fte	> 0.0
				OR	p.Tot_Fte				> 0.0)
			)													m
			ON	m.FactorSetId = tsq.FactorSetId
			AND	m.Refnum = tsq.Refnum
			AND	m.CalDateKey = tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr = 4
			AND	m.PersIdSec = o.PersIdSec;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;