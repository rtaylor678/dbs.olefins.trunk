﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonPlant_Methane]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY
	
		INSERT INTO calc.EmissionsCarbonPlant(FactorSetId, Refnum, CalDateKey, EmissionsId, Quantity_kMT, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			'Methane'					[EmissionsId],
			ABS(c.Quantity_kMT),
			21.0,
			ABS(c.Quantity_kMT) * 21.0	[CEF_MTCO2_MBtu]
		FROM @fpl									fpl
		INNER JOIN fact.StreamQuantityAggregate		c	WITH (NOEXPAND)
			ON	c.FactorSetId	= fpl.FactorSetId
			AND	c.Refnum		= fpl.Refnum
			AND	c.StreamId		= 'Ethylene'
		WHERE	fpl.CalQtr		= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;