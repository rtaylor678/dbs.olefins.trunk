﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonDirect_Standard]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonDirect(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_MBtu, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			'PYPS',
			fpl.Plant_QtrDateKey,
			utl.EmissionsId,
			COALESCE(utl.Quantity_MBtu, 0.0),
			utl.EmissionsCarbon_MTCO2e
		FROM @fpl											fpl
		INNER JOIN calc.EmissionsCarbonStandard				utl
			ON	utl.FactorSetId		= fpl.FactorSetId
			AND	utl.Refnum			= fpl.Refnum
			AND	utl.CalDateKey		= fpl.Plant_QtrDateKey
			AND	utl.EmissionsId		IN (SELECT b.DescendantId FROM dim.EmissionsStandard_Bridge b WHERE b.FactorSetId = fpl.FactorSetId AND b.EmissionsId IN ('ExportUtilities', 'LossFlareVent', 'PlantFeed', 'Methane'))
			AND	utl.EmissionsCarbon_MTCO2e	> 0.0
		WHERE	fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END