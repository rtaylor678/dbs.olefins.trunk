﻿CREATE PROCEDURE [calc].[Insert_ReplacementValue_Boilers]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplacementValue(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReplacementValueId, ReplacementValue)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			i.CurrencyRpt,
			p.FacilityId,
			CASE WHEN p.FacilityId = 'BoilLP' OR (p.FacilityId = 'BoilHP' AND ISNULL(m.ElecGen_MW, 0.0) < 10.0) THEN 1 ELSE 0.2 END * p.Pressure_PSIg * p.Rate_kLbHr * 0.0000266 * 361.3 / 355.4 * i._InflationFactor
		FROM @fpl									fpl
		INNER JOIN ante.InflationFactor				i
			--ON	i.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, i.FactorSetId) = fpl.DataYear
			AND	i.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN	fact.FacilitiesPressure			p
			ON	p.Refnum = fpl.Refnum
			AND	p.CalDateKey = fpl.Plant_QtrDateKey
			AND p.FacilityId IN ('BoilHP', 'BoilLP')
			AND	p.Pressure_PSIg > 0.0
			AND	p.Rate_kLbHr > 0.0
		LEFT OUTER JOIN fact.FacilitiesMisc			m
			ON	m.Refnum = fpl.Refnum
			AND	m.CalDateKey = fpl.Plant_QtrDateKey
			AND	m.ElecGen_MW > 0.0
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;