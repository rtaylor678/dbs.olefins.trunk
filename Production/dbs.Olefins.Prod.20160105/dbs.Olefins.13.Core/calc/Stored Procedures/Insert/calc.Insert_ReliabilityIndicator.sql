﻿CREATE PROCEDURE [calc].[Insert_ReliabilityIndicator]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReliabilityIndicator(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReliabilityIndicator)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			l.Loss_MTYr / c._StreamAvg_kMT / 10.0	[ReliabilityIndicator]
		FROM @fpl										fpl
		INNER JOIN inter.ReliabilityProductLost		l
			ON	l.FactorSetId = fpl.FactorSetId
			AND	l.Refnum = fpl.Refnum
			AND	l.CalDateKey = fpl.Plant_QtrDateKey
		INNER JOIN calc.CapacityPlant					c
			ON	c.FactorSetId = fpl.FactorSetId
			AND c.Refnum = fpl.Refnum
			AND c.CalDateKey = fpl.Plant_QtrDateKey
			AND	c.StreamId = 'Ethylene'
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;