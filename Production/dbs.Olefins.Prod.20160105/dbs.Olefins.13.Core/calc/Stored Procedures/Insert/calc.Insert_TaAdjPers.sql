﻿CREATE PROCEDURE calc.Insert_TaAdjPers
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @PersAggregate TABLE
		(
			[FactorSetId]		VARCHAR (12)	NOT NULL	CHECK([FactorSetId] <> ''),
			[FactorSetDateKey]	INT				NOT NULL	CHECK([FactorSetDateKey] > 19000101 AND [FactorSetDateKey] < 99991231),
			[Refnum]			VARCHAR (25)	NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT				NOT NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),
			[PersId]			VARCHAR (34)	NOT NULL	CHECK([PersId] IN ('OccMaintTa', 'MpsMaintTa')),

			[Company_Hrs]		REAL				NULL	CHECK([Company_Hrs] >= 0.0),
			[Contract_Hrs]		REAL				NULL	CHECK([Contract_Hrs] >= 0.0),
			[Tot_Company_Hrs]	REAL				NULL	CHECK([Tot_Company_Hrs] >= 0.0),
			[Tot_Contract_Hrs]	REAL				NULL	CHECK([Tot_Contract_Hrs] >= 0.0),
			[Tot_Hrs]			REAL				NULL	CHECK([Tot_Hrs] >= 0.0),

			[Company_Pcnt]		AS CASE WHEN [Tot_Hrs]<> 0.0 THEN ISNULL([Company_Hrs], 0.0)									/ [Tot_Hrs] * 100.0 ELSE 0.0 END
								PERSISTED		NOT	NULL	CHECK([Company_Pcnt] >= 0.0),

			[Contract_Pcnt]		AS CASE WHEN [Tot_Hrs]<> 0.0 THEN ISNULL([Contract_Hrs], 0.0)									/ [Tot_Hrs] * 100.0 ELSE 0.0 END
								PERSISTED		NOT	NULL	CHECK([Contract_Pcnt] >= 0.0),

			[Tot_Pcnt]			AS CASE WHEN [Tot_Hrs]<> 0.0 THEN (ISNULL([Company_Hrs], 0.0) + ISNULL([Contract_Hrs], 0.0))	/ [Tot_Hrs] * 100.0 ELSE 0.0 END
								PERSISTED		NOT	NULL	CHECK([Tot_Pcnt] >= 0.0),

			[Tot_Company_Pcnt]	AS CASE WHEN [Tot_Hrs]<> 0.0 THEN ISNULL([Tot_Company_Hrs], 0.0)								/ [Tot_Hrs] * 100.0 ELSE 0.0 END
								PERSISTED		NOT	NULL	CHECK([Tot_Company_Pcnt] >= 0.0),

			[Tot_Contract_Pcnt]	AS CASE WHEN [Tot_Hrs]<> 0.0 THEN ISNULL([Tot_Contract_Hrs], 0.0)								/ [Tot_Hrs] * 100.0 ELSE 0.0 END
								PERSISTED		NOT	NULL	CHECK([Tot_Contract_Pcnt] >= 0.0),

			[Occ_Hrs]			REAL				NULL	CHECK([Occ_Hrs] >= 0.0),

			PRIMARY KEY CLUSTERED([FactorSetId] ASC, [Refnum] ASC, [PersId] ASC, [CalDateKey] DESC)
			);

		INSERT INTO @PersAggregate(
			FactorSetId,
			FactorSetDateKey,
			Refnum,
			CalDateKey,
			PersId,
			Company_Hrs,
			Contract_Hrs,
			Tot_Company_Hrs,
			Tot_Contract_Hrs,
			Tot_Hrs,
			Occ_Hrs
			)
		SELECT
			tsq.FactorSetId,
			tsq.FactorSet_AnnDateKey,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			l.PersId,
			p.Company_Hrs,
			p.Contract_Hrs,
			SUM(p.Company_Hrs) OVER(PARTITION BY
				tsq.FactorSetId,
				tsq.Refnum,
				tsq.FactorSet_AnnDateKey
				)									[Tot_Company_Hrs],
			SUM(p.Contract_Hrs) OVER(PARTITION BY
				tsq.FactorSetId,
				tsq.Refnum,
				tsq.FactorSet_AnnDateKey
				)									[Tot_Contract_Hrs],
			SUM(p.Tot_Hrs) OVER(PARTITION BY
				tsq.FactorSetId,
				tsq.Refnum,
				tsq.FactorSet_AnnDateKey
				)									[Tot_Hrs],
			SUM(CASE WHEN p.PersId = 'OccMaintTa' THEN p.Tot_Hrs ELSE 0.0 END) OVER(PARTITION BY
				tsq.FactorSetId,
				tsq.Refnum,
				tsq.FactorSet_AnnDateKey
				)									[Occ_Hrs]
		FROM @fpl							tsq
		CROSS JOIN (VALUES ('OccMaintTa'), ('MpsMaintTa')) l ([PersId])
		LEFT OUTER JOIN fact.PersAggregate		p
			ON	p.FactorSetId	= tsq.FactorSetId
			AND	p.Refnum		= tsq.Refnum
			AND	p.CalDateKey	= tsq.Plant_QtrDateKey
			AND	p.PersId		= l.PersId
		WHERE	tsq.CalQtr		= 4;

		INSERT INTO calc.TaAdjPers(
			FactorSetId,
			Refnum,
			CalDateKey,

			PersId,
			Ann_Company_Pcnt,
			Ann_Contract_Pcnt)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.CalDateKey,
			t.PersId,
			t.Company_Pcnt,
			CASE t.PersId
				WHEN 'MpsMaintTa' THEN
					CASE WHEN t.Ta = 1 --AND t.Contract_Hrs > 0.0 -- AND ((t.CalDateKey < 20110000 AND t.Contract_Hrs > 0.0) OR t.CalDateKey >= 20110000)
						THEN calc.MaxValue(8.0 - t.Company_Pcnt, t.Contract_Pcnt)
						ELSE CASE WHEN t.Tot_Company_Pcnt < 6.0
							THEN
								8.0 - t.Company_Pcnt
							ELSE
								0.0
							END
						END
				WHEN 'OccMaintTa' THEN 100.0 - t.Tot_Company_Pcnt
				 - SUM(
					CASE WHEN t.PersId = 'MpsMaintTa' THEN
						CASE WHEN t.Ta = 1
							THEN calc.MaxValue(8.0 - t.Company_Pcnt, t.Contract_Pcnt)
							ELSE CASE WHEN t.Tot_Company_Pcnt < 6.0
								THEN
									8.0 - t.Company_Pcnt
								ELSE
									0.0
								END
							END
					END

				 ) OVER (PARTITION BY t.FactorSetId, t.Refnum, t.CalDateKey)
				END		[Contract_Pcnt]
		FROM (
			SELECT
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey,
				p.PersId,
					CASE WHEN y.TurnAroundInStudyYear_Bit = 1 AND p.Company_Hrs > 0.0
					THEN p.Company_Pcnt
					ELSE 
						CASE WHEN p.FactorSetDateKey < 20110000
						THEN 2.0
						ELSE 0.0
						END
					END																	[Company_Pcnt],

				SUM(
					CASE WHEN y.TurnAroundInStudyYear_Bit = 1 AND p.Company_Hrs > 0.0
					THEN p.Company_Pcnt
					ELSE
						CASE WHEN p.FactorSetDateKey < 20110000
						THEN 2.0
						ELSE 0.0
						END
					END
					) OVER (PARTITION BY y.FactorSetId, p.Refnum, p.CalDateKey)			[Tot_Company_Pcnt],

				y.TurnAroundInStudyYear_Bit [Ta],
				p.Contract_Hrs,
				p.Contract_Pcnt
			FROM @PersAggregate				p
			INNER JOIN calc.TaAdjStudyYear	y
				ON	y.FactorSetId = p.FactorSetId
				AND	y.Refnum = p.Refnum
				AND	y.CalDateKey = p.CalDateKey
			INNER JOIN(
				SELECT
					a.FactorSetId,
					a.Refnum,
					a.CalDateKey,
					MIN(a.Ann_TurnAround_ManHrs)	[Ann_TurnAround_ManHrs]
				FROM calc.TaAdjReliability a
				GROUP BY
					a.FactorSetId,
					a.Refnum,
					a.CalDateKey
				)	r
				ON	r.FactorSetId = y.FactorSetId
				AND	r.Refnum = y.Refnum
				AND	r.CalDateKey = y.CalDateKey
			) t;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;