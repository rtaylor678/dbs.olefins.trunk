﻿CREATE PROCEDURE [calc].[Insert_ReliabilityOpLossAnnualized]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ReliabilityOpLossAnnualized]([FactorSetId], [Refnum], [CalDateKey], [OppLossId], [Item_Count], [DownTimeLoss_MT], [SlowDownLoss_MT], [TotLoss_MT], [DownTimeLoss_kMT], [SlowDownLoss_kMT], [TotLoss_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			l.[OppLossId],

			CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2 ELSE 1 END					[Item_Count],

			SUM(
				CASE l.[Operator]
				WHEN '+' THEN + c.[DownTimeLoss_MT]
				WHEN '-' THEN - c.[DownTimeLoss_MT]
				END)	/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END	[DownTimeLoss_MT],
			SUM(
				CASE l.[Operator]
				WHEN '+' THEN + c.[SlowDownLoss_MT]
				WHEN '-' THEN - c.[SlowDownLoss_MT]
				END)	/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END	[SlowDownLoss_MT],
			SUM(
				CASE l.[Operator]
				WHEN '+' THEN + c.[_TotLoss_MT]
				WHEN '-' THEN - c.[_TotLoss_MT]
				END)	/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END	[TotLoss_MT],

			SUM(
				CASE l.[Operator]
				WHEN '+' THEN + c.[_DownTimeLoss_kMT]
				WHEN '-' THEN - c.[_DownTimeLoss_kMT]
				END)	/ CASE WHEN p.TotLoss_MT > 0.0 THEN 2.0 ELSE 1.0 END	[DownTimeLoss_kMT],
			SUM(
				CASE l.[Operator]
				WHEN '+' THEN + c.[_SlowDownLoss_kMT]
				WHEN '-' THEN - c.[_SlowDownLoss_kMT]
				END)	/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END	[SlowDownLoss_kMT],
			SUM(
				CASE l.[Operator]
				WHEN '+' THEN + c.[_TotLoss_kMT]
				WHEN '-' THEN - c.[_TotLoss_kMT]
				END)	/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END	[TotLoss_kMT]

		FROM @fpl													fpl
		INNER JOIN [dim].[ReliabilityOppLoss_Parent]				l
			ON	l.[FactorSetId]	= fpl.[FactorSetId]

		LEFT OUTER JOIN [fact].[ReliabilityOppLoss]					c
			ON	c.[Refnum]		= fpl.[Refnum]
			AND	c.[OppLossId]	= l.[OppLossId]
			AND (c.CalDateKey	= fpl.[Plant_QtrDateKey]
			OR	c.CalDateKey	= fpl.Plant_QtrDateKey - 10000)

		LEFT OUTER JOIN [fact].[ReliabilityOppLossAggregate]		p
			ON	p.[FactorSetId]	= fpl.[FactorSetId]
			AND	p.[Refnum]		= fpl.[Refnum]
			AND	p.[CalDateKey]	= fpl.[Plant_QtrDateKey] - 10000
			AND	p.[TotLoss_MT]	> 0.0
			AND p.[OppLossId]	= 'LostProdOpp'

		WHERE	fpl.[CalQtr]	= 4

		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			l.[OppLossId],
			p.[TotLoss_MT]

		HAVING SUM(
				CASE l.[Operator]
				WHEN '+' THEN + c.[_TotLoss_MT]
				WHEN '-' THEN - c.[_TotLoss_MT]
				END) <> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;