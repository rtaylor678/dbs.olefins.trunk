﻿CREATE TABLE [calc].[MeiUnreliability] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [CurrencyRpt]          VARCHAR (4)        NOT NULL,
    [MarginAnalysis]       VARCHAR (42)       NOT NULL,
    [Margin_CurkMT]        REAL               NOT NULL,
    [Unreliability_Index]  REAL               NOT NULL,
    [Unreliability_PcntRv] REAL               NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_MeiUnreliability_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_MeiUnreliability_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_MeiUnreliability_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_MeiUnreliability_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_MeiUnreliability] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MarginAnalysis] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_MeiUnreliability_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_MeiUnreliability_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_MeiUnreliability_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_MeiUnreliability_Stream_LookUp] FOREIGN KEY ([MarginAnalysis]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_MeiUnreliability_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_MeiUnreliability_u
	ON  calc.MeiUnreliability
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.MeiUnreliability
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MeiUnreliability.FactorSetId		= INSERTED.FactorSetId
		AND calc.MeiUnreliability.Refnum			= INSERTED.Refnum
		AND calc.MeiUnreliability.CalDateKey		= INSERTED.CalDateKey
		AND calc.MeiUnreliability.CurrencyRpt		= INSERTED.CurrencyRpt
		AND calc.MeiUnreliability.MarginAnalysis	= INSERTED.MarginAnalysis;
				
END