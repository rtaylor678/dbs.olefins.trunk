﻿CREATE TABLE [calc].[MaintPersAverage] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PersId]         VARCHAR (34)       NOT NULL,
    [MaintPrev_Hrs]  REAL               NOT NULL,
    [MaintCurr_Hrs]  REAL               NOT NULL,
    [_MaintAvg_Hrs]  AS                 (CONVERT([real],([MaintPrev_Hrs]+[MaintCurr_Hrs])/(2.0),(1))) PERSISTED NOT NULL,
    [Maint_Pcnt]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MaintPersAverage_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MaintPersAverage_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MaintPersAverage_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MaintPersAverage_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MaintPersAverage] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [PersId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MaintPersAverage_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MaintPersAverage_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MaintPersAverage_Pers_LookUp] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_MaintPersAverage_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [calc].[t_MaintPersAverage_u]
	ON [calc].[MaintPersAverage]
	AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.MaintPersAverage
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MaintPersAverage.FactorSetId		= INSERTED.FactorSetId
		AND	calc.MaintPersAverage.Refnum			= INSERTED.Refnum
		AND calc.MaintPersAverage.CalDateKey		= INSERTED.CalDateKey
		AND calc.MaintPersAverage.PersId			= INSERTED.PersId;

END;