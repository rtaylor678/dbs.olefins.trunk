﻿CREATE TABLE [calc].[Efficiency] (
    [FactorSetId]        VARCHAR (12)       NOT NULL,
    [Refnum]             VARCHAR (25)       NOT NULL,
    [CalDateKey]         INT                NOT NULL,
    [UnitId]             VARCHAR (42)       NOT NULL,
    [EfficiencyId]       VARCHAR (42)       NOT NULL,
    [Capacity]           REAL               NULL,
    [EfficiencyStandard] REAL               NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_Efficiency_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_Efficiency_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_Efficiency_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_Efficiency_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Efficiency] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [UnitId] ASC, [EfficiencyId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_Efficiency_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Efficiency_Efficiency_LookUp] FOREIGN KEY ([EfficiencyId]) REFERENCES [dim].[Efficiency_LookUp] ([EfficiencyId]),
    CONSTRAINT [FK_Efficiency_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Efficiency_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum]),
    CONSTRAINT [FK_Efficiency_Unit_LookUp] FOREIGN KEY ([UnitId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId])
);


GO

CREATE TRIGGER [calc].[t_Efficiency_u]
    ON [calc].[Efficiency]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [calc].[Efficiency]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[Efficiency].FactorSetId		= INSERTED.FactorSetId
		AND calc.[Efficiency].Refnum			= INSERTED.Refnum
		AND calc.[Efficiency].[UnitId]			= INSERTED.[UnitId]
		AND calc.[Efficiency].[EfficiencyId]	= INSERTED.[EfficiencyId]
		AND calc.[Efficiency].CalDateKey		= INSERTED.CalDateKey;
	
END;