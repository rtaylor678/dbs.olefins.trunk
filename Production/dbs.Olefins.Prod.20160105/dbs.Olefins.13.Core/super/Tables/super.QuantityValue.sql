﻿CREATE TABLE [super].[QuantityValue] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [Amount_Cur]        REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_super_QuantityValue_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_super_QuantityValue_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_super_QuantityValue_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_super_QuantityValue_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_super_QuantityValue] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_super_QuantityValue_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CL_super_QuantityValue_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_super_QuantityValue_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_super_QuantityValue_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_super_QuantityValue_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_super_QuantityValue_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [super].[t_super_QuantityValue_u]
	ON [super].[QuantityValue]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [super].[QuantityValue]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[super].[QuantityValue].Refnum				= INSERTED.Refnum
		AND	[super].[QuantityValue].CalDateKey			= INSERTED.CalDateKey
		AND	[super].[QuantityValue].StreamId			= INSERTED.StreamId
		AND	[super].[QuantityValue].StreamDescription	= INSERTED.StreamDescription
		AND	[super].[QuantityValue].CurrencyRpt			= INSERTED.CurrencyRpt;
		
END;