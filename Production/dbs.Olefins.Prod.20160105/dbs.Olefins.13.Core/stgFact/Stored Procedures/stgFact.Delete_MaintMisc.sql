﻿CREATE PROCEDURE [stgFact].[Delete_MaintMisc]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[MaintMisc]
	WHERE [Refnum] = @Refnum;

END;