﻿CREATE PROCEDURE [stgFact].[Delete_PracTA]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracTA]
	WHERE [Refnum] = @Refnum;

END;