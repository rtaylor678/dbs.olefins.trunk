﻿
CREATE FUNCTION sim.PypsFeedTypePiano
(
	@FeedType	TINYINT,
	@Piano		TINYINT
)
RETURNS TINYINT
WITH SCHEMABINDING
AS
BEGIN
	
	RETURN ISNULL(@FeedType, 0)
		- CASE WHEN
			ISNULL(@FeedType, 0) = 14
			AND ISNULL(@Piano, 0) = 0	-- and has No PIANO composition
		THEN 1
		ELSE 0
		END;
		
END;