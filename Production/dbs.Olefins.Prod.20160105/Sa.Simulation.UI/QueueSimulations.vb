﻿
''' <summary>
''' Simulation queue.
''' </summary>
''' <remarks>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </remarks>
Friend Class QueueSimulations

    <System.ThreadStatic()> Private Pyps As New Sa.Simulation.Pyps.Engine
    <System.ThreadStatic()> Private Spsl As New Sa.Simulation.Spsl.Engine

    Friend Function QueueSimulations(
        ByVal dbConnString As String,
        ByVal lv As System.Windows.Forms.ListView,
        ByVal PypsThreads As System.Int32,
        ByVal SpslThreads As System.Int32,
        ByVal DeleteFolder As Boolean) As System.Int32

        Dim i As System.Int32 = 0

        Using cn As New System.Data.SqlClient.SqlConnection(dbConnString)

            Using cmd As New System.Data.SqlClient.SqlCommand("q.Simulation_Enqueue", cn)

                cn.Open()

                Using rdr As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)

                    If rdr.HasRows Then

                        While rdr.Read()

                            Select Case UCase(rdr.GetString(rdr.GetOrdinal("SimModelID")))

                                Case "PYPS"
                                    If PypsThreads > 0 Then
                                        Pyps.QueuedItems.Enqueue(Sa.Simulation.Threading.PopulateArgument(dbConnString, rdr, DeleteFolder))
                                        i = i + 1
                                    End If

                                Case "SPSL"
                                    If SpslThreads > 0 Then
                                        Spsl.QueuedItems.Enqueue(Sa.Simulation.Threading.PopulateArgument(dbConnString, rdr, DeleteFolder))
                                        i = i + 1
                                    End If

                            End Select

                            Sa.Simulation.SQL.StoredProcedures.SimQueueUpdate(dbConnString, rdr.GetInt64(rdr.GetOrdinal("QueueID")), DateTimeOffset.Now())

                        End While

                    End If

                End Using

                cn.Close()

            End Using

        End Using

        Pyps.RunSimulation(lv, PypsThreads)
        Spsl.RunSimulation(lv, SpslThreads)

        Return i

    End Function

    Friend Sub StopSimulations()

        Pyps.StopSimulations()
        Spsl.StopSimulations()

    End Sub

End Class
