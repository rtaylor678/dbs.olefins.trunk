﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Chem.UpLoad
{
	public partial class ControlForm : Form
	{
		public ControlForm()
		{
			InitializeComponent();
		}

		private void ControlForm_Load(object sender, EventArgs e)
		{
			Sa.Chem.Osim.Common.PopulateCombo(cboRefnums);
		}

		private void UpLoadOne_Click(object sender, EventArgs e)
		{
			string f = GetUploadFile();
			
			if (f != "")
			{
				uLoad(f);
				MessageBox.Show("File " + f + " Has been Uploaded");
			}
		}

		private void UpLoadFolder_Click(object sender, EventArgs e)
		{
			string p = GetUploadPath();

			if (System.IO.Directory.Exists(p))
			{
				int i = 0;
				foreach (string f in System.IO.Directory.GetFiles(p))
				{
					uLoad(f);
					i++;
				}
				MessageBox.Show(i.ToString() + " Files have been Uploaded");
			}
		}

		private static void uLoad(string FilePath)
		{
			ChemUpLoad u = new ChemUpLoad();
			u.UpLoadChemFile(FilePath);
		}

		private string GetUploadFile()
		{
			string f = string.Empty;

			OpenFileDialog file = new OpenFileDialog();

			file.Filter = "Excel Files (*.xls,*.xlsm)|*.xls;*.xlsm";

			if (file.ShowDialog() == DialogResult.OK)
			{
				f = file.FileName;
			}

			return f;
		}

		private string GetUploadPath()
		{
			string p = string.Empty;

			FolderBrowserDialog path = new FolderBrowserDialog();

			if (path.ShowDialog() == DialogResult.OK)
			{
				p = path.SelectedPath;
			}

			return p;
		}

		private void btnPopulate_Click(object sender, EventArgs e)
		{
			//string pathTemplate = GetUploadFile();

			string pathTemplate = "C:\\Users\\RRH\\Desktop\\OSIM2015 Master.xlsm";

			string currentRefnum = cboRefnums.SelectedItem.ToString();
			
			ChemPopulate o = new ChemPopulate();
			o.OsimPopulate(currentRefnum, true, pathTemplate);
			o = null;

			MessageBox.Show("One OSIM has been created (NOT IMPLEMENTED)");
		}

		private void btnValidatedInput_Click(object sender, EventArgs e)
		{
			const string extension = "xls";
			const string path = "\\\\Dallas2\\data\\Data\\STUDY\\SEEC\\2014\\Correspondence\\14SEEC169\\";
			const string pattern = "OSIM2014SEEC*." + extension;

			List<string> files = new List<string>();

			files = System.IO.Directory
				.GetFiles(path, pattern, System.IO.SearchOption.AllDirectories) //<--- .NET 4.5
				.Where(file => file.ToUpper().Contains("OSIM2014SEEC") && file.ToLower().EndsWith(extension) && !file.ToUpper().Contains("COMPANY") && !file.ToUpper().Contains("UPLOAD"))
				.ToList();

			foreach (string file in files)
			{
				lbFiles.Items.Add(file);
				new Sa.Chem.Osim.VI().MakeVI(file);
			}
		}


		//private void button1_Click(object sender, EventArgs e)
		//{
		//    string p = "\\\\Dallas2\\data\\Data\\STUDY\\Olefins\\2013\\Correspondence\\";

		//    System.Collections.Generic.List<string> Files = new System.Collections.Generic.List<string>();
		//    //Files.Add("C:\\Users\\RRH\\Desktop\\OSIM2013PCH011.xls");

		//    Files.Add(p + "13PCH003\\OSIM2013PCH003.xls");
		//    Files.Add(p + "13PCH003\\PYPS2013PCH003.xls");
		//    Files.Add(p + "13PCH003\\SPSL2013PCH003.xls");

		//    Files.Add(p + "13PCH058\\OSIM2013PCH058.xls");
		//    Files.Add(p + "13PCH058\\PYPS2013PCH058.xls");
		//    Files.Add(p + "13PCH058\\SPSL2013PCH058.xls");

		//    Files.Add(p + "13PCH122\\OSIM2013PCH122.xls");
		//    Files.Add(p + "13PCH122\\PYPS2013PCH122.xls");
		//    Files.Add(p + "13PCH122\\SPSL2013PCH122.xls");

		//    Files.Add(p + "13PCH148\\OSIM2013PCH148.xls");
		//    Files.Add(p + "13PCH148\\PYPS2013PCH148.xls");
		//    Files.Add(p + "13PCH148\\SPSL2013PCH148.xls");

		//    Files.Add(p + "13PCH011\\OSIM2013PCH011.xls");
		//    Files.Add(p + "13PCH011\\PYPS2013PCH011.xls");
		//    Files.Add(p + "13PCH011\\SPSL2013PCH011.xls");

		//    Files.Add(p + "13PCH175\\OSIM2013PCH175.xls");
		//    Files.Add(p + "13PCH175\\PYPS2013PCH175.xls");
		//    Files.Add(p + "13PCH175\\SPSL2013PCH175.xls");

		//    foreach (string f in Files)
		//    {
		//        if (System.IO.File.Exists(f))
		//        {
		//            textBox1.Text = System.IO.Path.GetFileName(f);
		//            ChemUpLoad.UpLoadFile(f);
		//            textBox1.Text = string.Empty;
		//        }
		//    }

		//    MessageBox.Show(Files.Count.ToString() + " have been Uploaded.");

		//}
	}
}
