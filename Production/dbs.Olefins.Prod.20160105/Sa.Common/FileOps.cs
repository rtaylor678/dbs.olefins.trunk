﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Sa
{
    public static class FileOps
    {
		/// <summary>
		/// Deletes a file; verifies the file has been deleted.
		/// </summary>
		/// <param name="PathDelete">Path to file to be deleted.</param>
		/// <returns>Boolean</returns>
		/// <remarks>Loops trying to delete a file.  Necessary because simulation's do not timely close.</remarks>
		public static bool DeleteFile(string path)
		{
			const int Iterations = 0;
			int i = 0;

			while (File.Exists(path) && i <= Iterations)
			{
				GC.WaitForPendingFinalizers();
				GC.Collect();
				GC.WaitForFullGCComplete();

				try
				{
					File.Delete(path);
				}
				catch (IOException exIO)
				{
				}
				catch (UnauthorizedAccessException exUac)
				{
				}
				finally
				{
					i++;
				}
			}

			return (!File.Exists(path));
		}

		public static bool Copyfile(string pathSource, string pathTarget, bool overwrite = true)
		{
			if (File.Exists(pathSource) &&  (pathSource != pathTarget))
			{
				File.Copy(pathSource, pathTarget, overwrite);
			}

			return File.Exists(pathTarget);
		}

		public static void CopyDirectory(string sourceDirectoryPath, string targetDirectoryPath, bool overwrite, bool recursive)
		{
			// Get the subdirectories for the specified directory.
			DirectoryInfo dir = new DirectoryInfo(sourceDirectoryPath);
			DirectoryInfo[] dirs = dir.GetDirectories();

			if (!dir.Exists)
			{
				throw new DirectoryNotFoundException(
					"Source directory does not exist or could not be found: "
					+ sourceDirectoryPath);
			}

			// If the destination directory doesn't exist, create it. 
			if (!Directory.Exists(targetDirectoryPath))
			{
				Directory.CreateDirectory(targetDirectoryPath);
			}

			// Get the files in the directory and copy them to the new location.
			FileInfo[] files = dir.GetFiles();
			foreach (FileInfo file in files)
			{
				string temppath = Path.Combine(targetDirectoryPath, file.Name);
				file.CopyTo(temppath, overwrite);
			}

			// If copying subdirectories, copy them and their contents to new location. 
			if (recursive)
			{
				foreach (DirectoryInfo subdir in dirs)
				{
					string temppath = Path.Combine(targetDirectoryPath, subdir.Name);
					CopyDirectory(subdir.FullName, temppath, overwrite, recursive);
				}
			}
		}

		public static bool IsNetworkPath(string path)
		{
			return (new DriveInfo(path).DriveType == DriveType.Network);
		}

		public static bool IsFileLocked(string path)
		{
			try
			{
				using (Stream stream = new FileStream(path, FileMode.Open))
				{
					stream.Close();
					return false;
				}
			}
			catch (IOException)
			{
				return true;
			}
		}

		#region Wait for Files to Unlock

		public static bool WaitForUnlockedFiles(string path)
		{
			string[] files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);

			int items = files.Length;

			int timeOut_ms = items * (FileOps.IsNetworkPath(path) ? 25 : 10);

			return FileOps.WaitForUnlockedFiles(files, timeOut_ms);
		}

		public static bool WaitForUnlockedFiles(string path, int timeOut_ms)
		{
			string[] files = System.IO.Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
			return FileOps.WaitForUnlockedFiles(files, timeOut_ms);
		}

		private static bool WaitForUnlockedFiles(string[] files, int timeOut_ms)
		{
			bool completed = false;

			CancellationTokenSource source = new CancellationTokenSource(TimeSpan.FromMilliseconds(timeOut_ms));
			CancellationToken token = source.Token;

			try
			{
				ParallelLoopResult plr = Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = files.Length, CancellationToken = token },
					file =>
					{
						while (FileOps.IsFileLocked(file))
						{
						};
					}
				);

				completed = plr.IsCompleted;
			}
			catch (OperationCanceledException)
			{
				completed = false;
			}

			return completed;
		}

		#endregion

		#region Verify Folder Contents

		public static bool VerifyFolderContents(string pathVerificationFile, string pathFolder, out List<string> badFileList)
		{
			string verificationFile = File.ReadAllText(pathVerificationFile);
			string[] verificationLines = verificationFile.Split(Environment.NewLine.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);

			int timeOut_ms = verificationLines.Length * 100 * (FileOps.IsNetworkPath(pathFolder) ? 3 : 1);

			return VerifyFolderContents(verificationLines, pathFolder, timeOut_ms, out badFileList);
		}

		public static bool VerifyFolderContents(string pathVerificationFile, string pathFolder, int timeOut, out List<string> badFileList)
		{
			string verificationFile = File.ReadAllText(pathVerificationFile);
			string[] verificationLines = verificationFile.Split(Environment.NewLine.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);

			return VerifyFolderContents(verificationLines, pathFolder, timeOut, out badFileList);
		}

		private static bool VerifyFolderContents(string[] verificationLines, string pathFolder, int timeOut_ms, out List<string> badFileList)
		{
			bool completed = false;

			CancellationTokenSource source = new CancellationTokenSource(TimeSpan.FromMilliseconds(timeOut_ms));
			CancellationToken token = source.Token;

			List<string> badFiles = new List<string>();

			try
			{
				ParallelLoopResult plr = Parallel.ForEach(verificationLines, new ParallelOptions { MaxDegreeOfParallelism = verificationLines.Length, CancellationToken = token },
					verificationLine =>
					{
						string[] line = verificationLine.Split('|');

						string path = pathFolder + line[0];
						string hash = line[1];

						if (!IsFileCorrect(path, hash))
						{
							badFiles.Add(path);
						}
					}
				);
				completed = plr.IsCompleted;
			}
			catch (OperationCanceledException)
			{
				completed = false;
			}

			badFileList = badFiles;

			return (completed && badFiles.Count == 0);
		}

		public static void CreateFolderHashFile(string pathFolder, string pathFile)
		{
			string[] files = Directory.GetFiles(pathFolder, "*.*", SearchOption.AllDirectories);

			string fileContents = string.Empty;
			string fileHash;
			string relativePath;

			foreach (string qualifiedPath in files)
			{
				relativePath = qualifiedPath.Replace(pathFolder, String.Empty);
				fileHash = HmacSha512Hash(qualifiedPath);

				fileContents = fileContents + relativePath + "|" + fileHash + Environment.NewLine;
			}

			File.WriteAllText(pathFile, fileContents);
		}

		private static string HmacSha512Hash(string fullyQualifiedPath)
		{
			using (Stream stream = new FileStream(fullyQualifiedPath, FileMode.Open))
			{
				using (SHA512Managed sha = new SHA512Managed())
				{
					byte[] hash = sha.ComputeHash(stream);
					return BitConverter.ToString(hash).Replace("-", String.Empty);
				}
			}
		}

		private static bool IsFileCorrect(string fullyQualifiedPath, string hash)
		{
			return File.Exists(fullyQualifiedPath) && (hash == HmacSha512Hash(fullyQualifiedPath));
		}

		#endregion
	}
}