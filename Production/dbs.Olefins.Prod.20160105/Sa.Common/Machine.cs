﻿using System;
using System.Management;

namespace Sa
{
	public static class Machine
	{
		public static bool IsVirtual
		{
			get
			{
				using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * from Win32_ComputerSystem"))
				{
					using (ManagementObjectCollection items = searcher.Get())
					{
						foreach (ManagementObject item in items)
						{
							string manufacturer = item["Manufacturer"].ToString().ToLower();

							if ((manufacturer == "microsoft corporation" && item["Model"].ToString().ToUpperInvariant().Contains("VIRTUAL"))
								|| manufacturer.Contains("vmware")
								|| item["Model"].ToString() == "VirtualBox")
							{
								return true;
							}
						}
					}
				}
				return false;
			}
		}
	}
}
