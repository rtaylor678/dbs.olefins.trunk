﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void Personnel(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string persId;
			string persIdPri;
			string wksName;

			Dictionary<string, int> persRow = PersRowDictionary();
			Dictionary<string, string> persPri = PersPriDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_Personnel]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								persIdPri = rdr.GetString(rdr.GetOrdinal("PersIdPri"));

								if (persPri.TryGetValue(persIdPri, out wksName))
								{
									wks = wkb.Worksheets[tabNamePrefix + wksName];

									persId = rdr.GetString(rdr.GetOrdinal("PersID"));

									if (persRow.TryGetValue(persId, out r))
									{
										AddRangeValues(wks, r, 4, rdr, "Personnel_Count");
										AddRangeValues(wks, r, 5, rdr, "StraightTime_Hrs");

										if (persIdPri == "Occ")
										{
											AddRangeValues(wks, r, 6, rdr, "OverTime_Hrs");
											AddRangeValues(wks, r, 7, rdr, "Contract_Hrs");
										}
										else
										{
											AddRangeValues(wks, r, 6, rdr, "Contract_Hrs");
										}
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "Personnel", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_Personnel]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, string> PersPriDictionary()
		{
			Dictionary<string, string> d = new Dictionary<string, string>();

			d.Add("Occ", "5A");
			d.Add("Mps", "5B");

			return d;
		}

		private Dictionary<string, int> PersRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("MpsProcProc", 9);

			d.Add("MpsMaintRoutine", 12);
			d.Add("MpsMaintTaMaint", 13);
			d.Add("MpsMaintShop", 14);
			d.Add("MpsMaintPlan", 15);
			d.Add("MpsMaintWarehouse", 16);

			d.Add("MpsAdminLab", 19);

			d.Add("MpsTechProc", 22);
			d.Add("MpsTechEcon", 23);
			d.Add("MpsTechRely", 24);
			d.Add("MpsTechInsp", 25);
			d.Add("MpsTechAPC", 26);
			d.Add("MpsTechEnviron", 27);
			d.Add("MpsTechOther", 28);

			d.Add("MpsAdminAcct", 31);
			d.Add("MpsAdminMIS", 32);
			d.Add("MpsAdminPurchase", 33);
			d.Add("MpsAdminSecurity", 34);
			d.Add("MpsAdminOther", 35);

			d.Add("MpsGenPurchase", 38);
			d.Add("MpsGenOther", 39);

			d.Add("OccProcProc", 10);
			d.Add("OccProcUtil", 11);
			d.Add("OccProcOffSite", 12);
			d.Add("OccProcTruckRail", 13);
			d.Add("OccProcMarine", 14);
			d.Add("OccProcRelief", 15);
			d.Add("OccProcTraining", 16);
			d.Add("OccProcOther", 17);

			d.Add("OccMaintRoutine", 21);
			d.Add("OccMaintTaMaint", 22);
			d.Add("OccMaintInsp", 23);
			d.Add("OccMaintShop", 24);
			d.Add("OccMaintPlan", 25);
			d.Add("OccMaintWarehouse", 26);
			d.Add("OccMaintTraining", 27);

			d.Add("OccAdminLab", 31);
			d.Add("OccAdminAcct", 32);
			d.Add("OccAdminPurchase", 33);
			d.Add("OccAdminSecurity", 34);
			d.Add("OccAdminClerical", 35);
			d.Add("OccAdminOther", 36);

			return d;
		}
	}
}