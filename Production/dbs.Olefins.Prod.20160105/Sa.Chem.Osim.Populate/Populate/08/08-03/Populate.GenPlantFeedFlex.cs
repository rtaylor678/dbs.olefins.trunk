﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void GenPlantFeedFlex(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string streamId;

			Dictionary<string, int> feedFlex = GenPlantFeedFlexRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "8-3"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_GenPlantFeedFlex]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));

								if (feedFlex.TryGetValue(streamId, out r))
								{
									c = 7;
									AddRangeValues(wks, r, c, rdr, "Capability_Pcnt", 100.0);

									c = 8;
									AddRangeValues(wks, r, c, rdr, "Demonstrate_Pcnt", 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "GenPlantFeedFlex", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_GenPlantFeedFlex]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> GenPlantFeedFlexRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Ethane", 21);
			d.Add("Propane", 22);
			d.Add("Butane", 23);
			d.Add("LiqLight", 24);
			d.Add("Diesel", 25);
			d.Add("GasOilHv", 26);

			return d;
		}
	}
}