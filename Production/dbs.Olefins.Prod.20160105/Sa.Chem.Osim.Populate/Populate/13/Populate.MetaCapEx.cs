﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MetaCapEx(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			const int r = 59;
			const int c = 8;

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "13"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MetaCapEx]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								AddRangeValues(wks, r, c, rdr, "Amount_Cur");
							}
						}
					}
					cn.Close();
				};
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MetaCapEx", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MetaCapEx]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}