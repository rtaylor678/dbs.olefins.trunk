﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSelLogistics(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string streamId;
			string facilityId;

			Dictionary<string, int> feedSelStream = FeedSelStreamColDictionary();
			Dictionary<string, int> feedSelLogistics = FeedSelLogisticsRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + T11_01];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSelLogistics]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));

								if (feedSelStream.TryGetValue(streamId, out c))
								{
									facilityId = rdr.GetString(rdr.GetOrdinal("FacilityId"));

									if (feedSelLogistics.TryGetValue(facilityId, out r))
									{
										AddRangeValues(wks, r, c, rdr, "Delivery_Pcnt", 100.0);
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSelLogistics", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSelLogistics]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> FeedSelStreamColDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("LPG", 5);
			d.Add("Condensate", 6);
			d.Add("Naphtha", 7);
			d.Add("LiqHeavy", 8);

			return d;
		}

		private Dictionary<string, int> FeedSelLogisticsRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Pipeline", 77);
			d.Add("RailTruck", 78);
			d.Add("Barge", 79);
			d.Add("Ship", 80);

			return d;
		}
	}
}