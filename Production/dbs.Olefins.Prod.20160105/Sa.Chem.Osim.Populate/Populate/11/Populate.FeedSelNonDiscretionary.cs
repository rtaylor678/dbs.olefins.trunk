﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSelNonDiscretionary(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			const int c = 11;

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + T11_01];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSelNonDiscretionary]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								r = 43;
								AddRangeValues(wks, r, c, rdr, "Affiliated_Pcnt", 100.0);

								r = 46;
								AddRangeValues(wks, r, c, rdr, "NonAffiliated_Pcnt", 100.0);
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSelNonDiscretionary", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSelNonDiscretionary]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}