﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void EnergyCogenFacilities(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string facilityId;

			Dictionary<string, int> facilityRow = CoGenTypeRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "7"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_EnergyCogenFacilities]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								facilityId = rdr.GetString(rdr.GetOrdinal("FacilityID"));

								if (facilityRow.TryGetValue(facilityId, out r))
								{
									c = 6;
									AddRangeValues(wks, r, c, rdr, "Unit_Count");

									c = 7;
									AddRangeValues(wks, r, c, rdr, "Capacity_MW");

									c = 8;
									AddRangeValues(wks, r, c, rdr, "Efficiency_Pcnt", 100.0);

									c = 3;
									AddRangeValues(wks, r, c, rdr, "FacilityName");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "EnergyCogenFacilities", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_EnergyCogenFacilities]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> CoGenTypeRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();
			
			d.Add("CogenGT", 57);
			d.Add("CogenST", 58);
			d.Add("CogenCC", 59);
			d.Add("CogenOth", 60);

			return d;
		}
	}
}