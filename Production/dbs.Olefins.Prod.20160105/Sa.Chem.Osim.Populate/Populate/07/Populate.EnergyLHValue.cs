﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void EnergyLHValue(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string accountId;

			Dictionary<string, int> lhvRow = LhvAccountRowDictionary();
			Dictionary<string, int> egyCol = energyColDictionary();

			try
			{
				string uomSuffix = (wkb.Worksheets["Conv"].Cells[2, 2].Value2 == 1) ? "Imp" : "Met";

				wks = wkb.Worksheets[tabNamePrefix + "7"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_EnergyLHValue]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("AccountID"));

								if(lhvRow.TryGetValue(accountId, out r))
								{
									c = 6;
									AddRangeValues(wks, r, c, rdr, "Component_WtPcnt", 100.0);

									foreach (KeyValuePair<string, int> entry in egyCol)
									{
										c = entry.Value;
										AddRangeValues(wks, r, c, rdr, entry.Key + uomSuffix);
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "EnergyLHValue", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_EnergyLHValue]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> LhvAccountRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("PurFuelNatural", 7);
			d.Add("PurEthane", 8);
			d.Add("PurPropane", 9);
			d.Add("PurButane", 10);
			d.Add("PurNaphtha", 11);
			d.Add("PurDistallate", 12);
			d.Add("PurResidual", 13);
			d.Add("PurCoalCoke", 14);

			d.Add("PurSteamSHP", 16);
			d.Add("PurSteamHP", 17);
			d.Add("PurSteamIP", 18);
			d.Add("PurSteamLP", 19);

			d.Add("PurSteam", 21);		// Fuel Used for Purchased Steam Generation, Methane Content, wt %
			
			d.Add("PPFCFuelGas", 27);
			d.Add("PPFCEthane", 28);
			d.Add("PPFCPropane", 29);
			d.Add("PPFCButane", 30);
			d.Add("PPFCPyroNaphtha", 31);
			d.Add("PPFCPyroGasOil", 32);
			d.Add("PPFCPyroFuelOil", 33);

			d.Add("ExportSteamSHP", 40);
			d.Add("ExportSteamHP", 41);
			d.Add("ExportSteamIP", 42);
			d.Add("ExportSteamLP", 43);

			return d;
		}

		private Dictionary<string, int> energyColDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Amount_", 4);
			d.Add("Pressure_", 5);
			d.Add("Temp_", 6);
			d.Add("LHValue_", 7);
			d.Add("Amount_Cur_", 8);

			return d;
		}
	}
}