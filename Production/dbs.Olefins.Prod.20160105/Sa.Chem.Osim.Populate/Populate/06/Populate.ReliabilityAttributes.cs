﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityAttributes(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityAttributes]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								wks = wkb.Worksheets[tabNamePrefix + "6-2"];

								AddRangeValues(wks, 26, 5, rdr, "TimingFactorID");
								AddRangeValues(wks, 42, 6, rdr, "TaPrep_Hrs");
								AddRangeValues(wks, 46, 6, rdr, "TaStartUp_Hrs");

								wks = wkb.Worksheets[tabNamePrefix + "6-3"];

								AddRangeValues(wks, 34, 8, rdr, "FurnTubeLife_Mnths");
								AddRangeValues(wks, 38, 8, rdr, "FurnRetube_Days");
								AddRangeValues(wks, 41, 8, rdr, "FurnRetubeWorkID");

								wks = wkb.Worksheets[tabNamePrefix + "6-4"];

								AddRangeValues(wks, 47, 3, rdr, "PlantFurnLimit_Pcnt", 100.0);
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityAttributes", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityAttributes]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}