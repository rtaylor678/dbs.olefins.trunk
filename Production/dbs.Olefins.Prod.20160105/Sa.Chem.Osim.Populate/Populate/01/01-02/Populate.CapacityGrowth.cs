﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		//	0				1			2			3				4				5					6				7
		//Refnum		CalDateKey	Date_Year	StreamId		Prod_kMT		Capacity_kMT	Utilization_Pcnt	StudyYearDifference
		//2013PCH175	20011231	2001		Ethylene		602.7			620				97.20968			12
		//2013PCH175	20021231	2002		Ethylene		613.8			620				99					11
		//2013PCH175	20031231	2003		Ethylene		579.3			620				93.43549			10
		//2013PCH175	20041231	2004		Ethylene		645.6			712				90.67416			9
		//2013PCH175	20011231	2001		Propylene		301.5			NULL			NULL				12
		//2013PCH175	20021231	2002		Propylene		324				NULL			NULL				11
		//2013PCH175	20031231	2003		Propylene		305.2			NULL			NULL				10
		//2013PCH175	20041231	2004		Propylene		335.4			NULL			NULL				9
	
		public void CapacityGrowth(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;
			int cYear = 5;
			
			int vYear;
			int studyYearDbs;
			int syd;

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "1-2"];

				Dictionary<int, int> rowYear = new Dictionary<int, int>();

				for (int i = 11; i <= 32; i++)
				{
					rng = wks.Cells[i, cYear];
					if (int.TryParse(rng.Text, out vYear))
					{
						rowYear.Add(vYear, i);
					}
				}

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_CapacityGrowth]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								studyYearDbs = rdr.GetInt32(rdr.GetOrdinal("Date_Year"));

								if (rowYear.TryGetValue(studyYearDbs, out r))
								{
									syd = rdr.GetInt16(rdr.GetOrdinal("StudyYearDifference"));
									c = (rdr.GetSqlString(rdr.GetOrdinal("StreamId")) == "Ethylene") ? 7 : 8;

									if (syd >= 0)
									{
										AddRangeValues(wks, r, c, rdr, "Prod_kMT");

										if (c == 7)
										{
											AddRangeValues(wks, r, c + 2, rdr, "Capacity_kMT");
										}
									}
									else
									{
										AddRangeValues(wks, r, c, rdr, "Capacity_kMT");
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "CapacityGrowth", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_CapacityGrowth]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}