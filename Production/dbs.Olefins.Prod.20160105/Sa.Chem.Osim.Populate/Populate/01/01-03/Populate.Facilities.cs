﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void Facilities(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 7;
			int o = 0;

			string facilityId;

			Dictionary<string, int> facilityRow = FacilityRowDictionary();
			Dictionary<string, int> colOffset = FacilityColOffsetDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "1-3"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_Facilities]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								facilityId = rdr.GetString(rdr.GetOrdinal("FacilityId"));

								if (facilityRow.TryGetValue(facilityId, out r))
								{
									if (colOffset.TryGetValue(facilityId, out o))
									{
										//rng = wks.Cells[r, c + o];
										//rng.Value2 = rdr.GetString(rdr.GetOrdinal("HasUnit_YN"));
										AddRangeValues(wks, r, c + o, rdr, "HasUnit_YN");
									}
									else
									{
										//rng = wks.Cells[r, c];
										//rng.Value2 = rdr.GetValue(rdr.GetOrdinal("Unit_Count"));
										AddRangeValues(wks, r, c, rdr, "Unit_Count");
									}

									switch (facilityId)
									{
										case "FracFeed":
											AddRangeValues(wks, r, 9, rdr, "FFFeedRate_kBsd");
											AddRangeValues(wks, r, 12, rdr, "FFStreamId");
											AddRangeValues(wks, r + 1, 9, rdr, "FFFeedProcessed_kMT");
											AddRangeValues(wks, r + 1, 12, rdr, "FFFeedDensity_SG");
											break;

										case "PyroFurn":
											AddRangeValues(wks, r, 9, rdr, "PyroFurnSpare_Pcnt", 100.0);
											break;

										case "CompGas":
											AddRangeValues(wks, r, 9, rdr, "Stages_Count");
											AddRangeValues(wks, r + 1, 9, rdr, "Age_Years");
											AddRangeValues(wks, r + 1, 12, rdr, "Power_BHP");
											AddRangeValues(wks, r + 2, 9, rdr, "CoatingID");
											AddRangeValues(wks, r + 2, 12, rdr, "DriverID");

											break;

										case "TowerDemethanizer":
										case "TowerDeethanizer":
										case "TowerEthylene":
										case "TowerDepropanizer":
										case "TowerPropylene":
											AddRangeValues(wks, r, 9, rdr, "Pressure_PSIg");
											break;

										case "CompRefrigPropylene":
										case "CompRefrigEthylene":
										case "CompRefrigMethane":
											AddRangeValues(wks, r, 9, rdr, "Age_Years");
											AddRangeValues(wks, r, 12, rdr, "Power_BHP");
											AddRangeValues(wks, r + 1, 9, rdr, "CoatingID");
											AddRangeValues(wks, r + 1, 12, rdr, "DriverID");
											break;

										case "BoilHP":
										case "BoilLP":
										case "SteamSuperHeat":
											AddRangeValues(wks, r, 9, rdr, "Pressure_PSIg");
											AddRangeValues(wks, r, 12, rdr, "Rate_KLbHr");
											break;

										case "ElecGenDist":
											AddRangeValues(wks, r, 9, rdr, "ElecGen_MW");
											break;

										case "TowerCool":
											AddRangeValues(wks, r + 1, 9, rdr, "FanHorsepower_bhp");
											AddRangeValues(wks, r + 2, 9, rdr, "PumpHorsepower_bhp");
											AddRangeValues(wks, r + 3, 9, rdr, "FlowRate_TonneDay");
											break;

										case "TowerPyroGasHT":
											AddRangeValues(wks, r, 9, rdr, "HTFeedPressure_PSIg");
											AddRangeValues(wks, r, 12, rdr, "HTFeedRate_kBsd");
											AddRangeValues(wks, r + 1, 9, rdr, "HTTypeID");
											AddRangeValues(wks, r + 2, 9, rdr, "HTFeedProcessed_kMT");
											AddRangeValues(wks, r + 3, 9, rdr, "HTFeedDensity_SG");
											break;

										case "VesselOth1":
										case "VesselOth2":
										case "VesselOth3":
											AddRangeValues(wks, r, 2, rdr, "FacilityName");
											break;
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_Facilities]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> FacilityRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("FracFeed", 6);

			d.Add("PyroFurn", 9);
			d.Add("TowerQuench", 10);
			d.Add("QWaterSys", 11);
			d.Add("TowerGas",12);
			d.Add("DryerProcess", 13);

			d.Add("CompGas", 15);

			d.Add("TowerCaustic", 19);
			d.Add("MethSys", 20);
			d.Add("TowerDemethanizer", 21);
			d.Add("TowerDeethanizer", 22);
			d.Add("Acetyl", 23);
			d.Add("TowerEthylene", 24);
			d.Add("TowerDepropanizer", 25);
			d.Add("TowerPropylene", 26);
			d.Add("TowerDebutanizer", 27);

			d.Add("CompRefrigPropylene", 29);
			d.Add("CompRefrigEthylene", 32);
			d.Add("CompRefrigMethane", 35);

			d.Add("BoilHP", 40);
			d.Add("BoilLP", 41);
			d.Add("SteamSuperHeat", 43);
			d.Add("ElecGenDist", 44);

			d.Add("TowerCool", 46);

			d.Add("HydroPurCryogenic", 52);
			d.Add("HydroPurPSA", 53);
			d.Add("HydroPurMembrane", 54);

			d.Add("StoreFeed", 56);
			d.Add("StoreProdAbove", 57);
			d.Add("StoreProdUnder", 58);
			d.Add("EthyLiq", 59);

			d.Add("TowerPyroGasHT", 61);

			d.Add("PipeStation", 66);
			d.Add("RailTruck", 67);
			d.Add("Marine", 68);
			d.Add("CompAir", 69);
			d.Add("ElecTransf", 70);
			d.Add("Flare", 71);
			d.Add("WaterTreat", 72);
			d.Add("Bldg", 73);
			d.Add("RGR", 74);

			d.Add("VesselOth1", 77);
			d.Add("VesselOth2", 78);
			d.Add("VesselOth3", 79);

			return d;
		}

		private Dictionary<string, int> FacilityColOffsetDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("SteamSuperHeat", -1);
			d.Add("StoreFeed", -1);
			d.Add("StoreProdAbove", -1);
			d.Add("StoreProdUnder", -1);

			d.Add("PipeStation", -1);
			d.Add("RailTruck", -1);
			d.Add("Marine", -1);
			d.Add("CompAir", -1);
			d.Add("ElecTransf", -1);
			d.Add("Flare", -1);
			d.Add("WaterTreat", -1);
			d.Add("Bldg", -1);
			d.Add("RGR", -1);

			return d;
		}

	}
}