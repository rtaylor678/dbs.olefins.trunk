﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;


//	Table 1-3
//*	Cooling water	46-49	(Done; needs C# stgFact upload)

//	Table 2C
//	Composition		59-72	(Done; needs C# stgFact upload)
//	Price			75		(Done; needs C# stgFact upload)

//	Table 4
//*	Cooling water	55		(Done; needs C# stgFact upload)

//	Table 6-3
//*	Mini TA info	21-24	(Done; needs C# stgFact upload)

//	Table 6-4
//*	Percent of tube	40		(Done; needs C# stgFact upload)

//	Table 6-6
//*							(needs all)

//	Table 8-1
//	Feedstock Cost & Product revenue (Deleted)

//	Table 8-3
//	no more future	39-47
//*	Split Cap. Maint and other	46-47


namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		const string T02_A1 = "2A-1";
		const string T02_A2 = "2A-2";
		const string T02_B = "2B";
		const string T02_C = "2C";
		const string T03_01 = "3";


		const string T09_01 = "9-1";
		const string T09_02 = "9-2";
		const string T09_03 = "9-3";
		const string T09_04 = "9-4";
		const string T09_05 = "9-5";
		const string T09_06 = "9-6";
		const string T10_01 = "10-1";
		const string T10_02 = "10-2";
		const string T11_01 = "11";
		const string T12_01 = "12-1";
		const string T12_02 = "12-2";
		const string T13_01 = "13";

		public void InputForm(Excel.Workbook wkb, string tabPrefix, string refnum, string factorSetId)
		{
			this.PlantName(wkb, tabPrefix, refnum);

			Parallel.Invoke
			(
				() => { this.CapacityAggregate(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.CapacityAttributes(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.CapacityGrowth(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.Facilities(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.Streams(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.SupplementalRecycled(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.RogEntryPoint(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.OpEx(wkb, tabPrefix, refnum, factorSetId); },

				///////////////////////////////////

				() => { this.Personnel(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.PersonnelAbsence(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.ReliabilityAttributes(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityCriticalPath(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityOppLoss(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityOppLossAvailability(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityPyroFurn(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityPyroFurnRunLength(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityPyroFurnRunPlantLimit(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityTrainTurnaround(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ReliabilityCompEvents(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.EnergyCogen(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.EnergyCogenFacilities(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.EnergyCogenPcnt(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.EnergyElec(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.EnergyLHValue(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.EnergyComposition(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.GenPlant(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantCapEx(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantCommIntegration(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantDuties(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantEnergy(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantEnergyEfficiency(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantEnvironment(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantExpansion(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantFeedFlex(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantInventory(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.GenPlantLogistics(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.Apc(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ApcControllers(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ApcExistance(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.ApcOnLinePcnt(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.Maint(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MaintAccuracy(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MaintExpense(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MaintOpExAggregate(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MaintPers(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.FeedSel(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.FeedSelLogistics(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.FeedSelModelPenalty(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.FeedSelModelPlan(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.FeedSelModels(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.FeedSelNonDiscretionary(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.FeedSelPersonnel(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.FeedSelPractices(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.PolyFacilities(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.PolyOpEx(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.PolyQuantity(wkb, tabPrefix, refnum, factorSetId); },

				() => { this.MetaCapEx(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MetaEnergy(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MetaOpEx(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MetaQuantity(wkb, tabPrefix, refnum, factorSetId); },
				() => { this.MetaMisc(wkb, tabPrefix, refnum, factorSetId); }
			);

			wkb.ForceFullCalculation = true;
			wkb.Parent.CalculateFullRebuild();
		}

		private void AddRangeValues(Excel.Worksheet wks, int r, int c, SqlDataReader rdr, string col)
		{
			int ord = rdr.GetOrdinal(col);
			Excel.Range rng = wks.Cells[r, c];

			if (!rdr.IsDBNull(ord) && !rng.Locked)
			{
				rng.Value2 = rdr.GetValue(ord);
			}
		}

		private void AddRangeValues(Excel.Worksheet wks, int r, int c, SqlDataReader rdr, string col, double divisor)
		{
			int ord = rdr.GetOrdinal(col);
			Excel.Range rng = wks.Cells[r, c];

			if (!rdr.IsDBNull(ord) && !rng.Locked)
			{
				double tmp = rdr.GetFloat(ord);
				rng.Value2 = tmp / divisor;
			}
		}
    }
}
