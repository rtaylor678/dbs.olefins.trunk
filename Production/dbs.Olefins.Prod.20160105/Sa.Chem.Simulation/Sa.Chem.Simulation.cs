﻿using System;
using System.Collections.Generic;
//using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Sa
{
	public class Asset
	{
		public string factorSetId;
		public string refnum;

		public Asset(string factorSetId, string refnum)
		{
			this.factorSetId = factorSetId;
			this.refnum = refnum;
		}
	}

	public class Assets : List<Asset>
	{
	}

	public class Plant
	{
		public int queueId;
		public string factorSetId;
		public string refnum;
		public bool autoDelete;

		public Plant(int queueId, string factorSetId, string refnum, bool autoDelete)
		{
			this.queueId = queueId;
			this.factorSetId = factorSetId;
			this.refnum = refnum;
			this.autoDelete = autoDelete;
		}
	}

	public class Plants : List<Plant>
	{
	}
}

namespace Sa.Chem
{
	public abstract class Simulation
	{
		public abstract class AController : IDisposable
		{
			#region IDisposable

			private bool disposed = false;

			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			//protected virtual void Dispose(bool disposing)
			private void Dispose(bool disposing)
			{
				if (disposed)
					return;

				if (disposing)
				{
				}

				disposed = true;
			}

			#endregion

			public abstract void RunQueue();
			public abstract void RunPlant(Asset asset);
		}

		public abstract class AEngine : IDisposable
		{
			#region IDisposable

			private bool disposed = false;

			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			protected virtual void Dispose(bool disposing)
			{
				if (disposed)
					return;

				if (disposing)
				{
					if (pathIsolatedDelete) Directory.Delete(this.pathIsolated, true);
				}

				disposed = true;
			}

			#endregion

			private readonly string pathEngine;
			private readonly string pathIsolated;
			private readonly bool pathIsolatedDelete;

			protected AEngine(string pathEngine, string pathIsolated, bool autoDelete)
			{
				this.pathIsolatedDelete = autoDelete;

				this.pathEngine = Environment.CurrentDirectory + "\\" + pathEngine + "\\";
				this.pathIsolated = Environment.CurrentDirectory + "\\" + pathIsolated + "\\";

				//	Isolate Current Simulation Files
				Sa.FileOps.CopyDirectory(this.PathEngine, this.PathIsolated, true, true);
			}

			protected AEngine(string pathEngine, string pathIsolated)
				: this(pathEngine, pathIsolated, false)
			{
			}

			public string PathEngine
			{
				get { return pathEngine; }
			}

			public string PathIsolated
			{
				get { return pathIsolated; }
			}

			protected sealed class ErrorCode
			{
				public const int None = 0;

				public const int SourceFiles = -1;
				public const int Prepare = -2;
				public const int Process = -3;
				public const int Worker = -4;

				public const int Cancel = -97;
				public const int Init = -98;
				public const int Notice = -99;
				public const int Finished = -100;

				public const int ExecuteSimulation = -11;
				public const int Execute = -12;
				public const int ExecuteKill = -13;
				public const int ExecuteWinEx = -14;

				public const int Feed = -101;
				public const int FeedComposition = -102;

				public const int Yield = -201;
				public const int YieldComposition = -202;
				public const int YieldEnergy = -203;
				public const int YieldCompositionEnergy = -204;

				public const int Plant = -301;
				public const int PlantComposition = -302;
				public const int PlantEnergy = -303;
				public const int PlantCompositionEnergy = -304;
			}

			public abstract void SimulateFeed(Feed feed, IInputReader iRdr, IOutputWriter oWtr);

			protected abstract int Execute();

			protected abstract bool VerifyEngine();

			protected abstract void ResetEngine();
		}

		public class Feed
		{
			public readonly string factorSetId;
			public readonly string refnum;
			public readonly string opCondId;
			public readonly string streamId;
			public readonly string streamDescription;
			public readonly int recycleId;

			public readonly int f127;
			public readonly float f130;
			public readonly float f131;
			public readonly float f132;

			public Feed(string factorSetId, string refnum, string opCondId, string streamId, string streamDescription, int recycleId, int f127, float f130, float f131, float f132)
			{
				this.factorSetId = factorSetId;
				this.refnum = refnum;
				this.opCondId = opCondId;
				this.streamId = streamId;
				this.streamDescription = streamDescription;
				this.recycleId = recycleId;

				this.f127 = f127;
				this.f130 = f130;
				this.f131 = f131;
				this.f132 = f132;
			}
		}

		public class Feeds : List<Feed>
		{
		}

		#region Input

		public class InputItem
		{
			public readonly string Column;
			public float FValue;

			public InputItem(string column, float fValue)
			{
				this.Column = column;
				this.FValue = fValue;
			}
		}

		public class InputItems : List<InputItem>
		{
		}

		public class SimInput
		{
			public InputItems InputItems;

			public SimInput(InputItems inputItems)
			{
				this.InputItems = inputItems;
			}
		}

		#endregion

		#region Output

		public class Component
		{
			public readonly string id;
			public readonly float wtPcnt;

			public Component(string id, float wtPcnt)
			{
				this.id = id;
				this.wtPcnt = wtPcnt;
			}
		}

		public class Yield : List<Component>
		{
		}

		public class Energy
		{
			public readonly float amount;

			public Energy(float amount)
			{
				this.amount = amount;
			}
		}

		public class SimResults
		{
			public Yield yield;
			public Energy energy;

			public SimResults(Yield yield, Energy energy)
			{
				this.yield = yield;
				this.energy = energy;
			}
		}

		#endregion

		public interface ILiquidProperties
		{
			bool IsLiquid { get; }
			int IndexIbp { get; }
			int IndexDensity { get; }
			float InitialBoilingPoint { get; }
			float Density { get; }
		}

		public interface IInputReader
		{
			SimInput Read();
			bool Verified();
		}

		public interface IInputWriter
		{
			void Write(SimInput simInput);
			bool Exists();
		}

		public interface IOutputReader
		{
			bool Exists();
			bool Verified();
			SimResults Read(Feed feed);
		}

		public interface IOutputWriter
		{
			void Write(Feed feed, SimResults simResults);
		}
	}
}