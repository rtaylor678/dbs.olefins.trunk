﻿CREATE FUNCTION [fact].[PlantNameHistoryLimit]
(
	@StudyYear	SMALLINT
)
RETURNS TABLE AS RETURN
(
SELECT
	[f].[Refnum],
	[f].[SmallRefnum],
	[f].[AssetId],

	[f].[Co],
	[f].[Loc],
	[f].[CoLoc],

	[f].[NameFull],
	[f].[NameTitle],
	[f].[AddressPOBox],
	[f].[AddressStreet],
	[f].[AddressCity],
	[f].[AddressState],
	[f].[AddressCountryId],

	[f].[AddressPostalCode],
	[f].[NumberVoice],
	[f].[NumberFax],
	[f].[eMail],

	[f].[PricingName],
	[f].[PrincingEMail],
	[f].[DataName],
	[f].[DataEMail],

	[f].[UomId],
	[f].[UomNumber],
	[f].[StudyYear],

		[PreviousRefnum]		= [h].[Refnum],
		[PreviousStudyYear]		= [h].[StudyYear],
		[PreviousForexRate]		= [h].[ForexRate],
		[StudyYearDifference]	= [f].[StudyYear] - [h].[StudyYear],

	[f].[CurrencyFcn],
	[f].[CurrencyRpt],
	[f].[CountryId]
FROM
	[fact].[TSort]				[f]

OUTER APPLY (
	SELECT TOP 1
		[s].[Refnum],
		[s].[StudyYear],
		[x].[ForexRate],
		[s].[UomId]
	FROM
		[fact].[TSort]			[s]
	LEFT OUTER JOIN
		[fact].[ForexRate]		[x]
			ON	[x].[Refnum]	= [s].[Refnum]
	WHERE
			[s].[AssetId]		=	[f].[AssetId]
		AND	[s].[StudyId]		=	[f].[StudyId]
		AND	[s].[Refnum]		<>	[f].[Refnum]
		AND	[s].[StudyYear]		<	[f].[StudyYear]
		AND	[s].[StudyYear]		>=	@StudyYear
		AND	[s].[ScenarioId]	IS NULL
	ORDER BY
		[s].[StudyYear]		DESC
		) [h]
);
GO