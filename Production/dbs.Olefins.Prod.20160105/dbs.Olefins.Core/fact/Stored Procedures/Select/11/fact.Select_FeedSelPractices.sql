﻿CREATE PROCEDURE [fact].[Select_FeedSelPractices]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[Mix_Bit],
			[Mix_Y]					= CASE WHEN [f].[Mix_Bit]			= 1 THEN 'Y' END,
		[f].[Blend_Bit],
			[Blend_Y]				= CASE WHEN [f].[Blend_Bit]			= 1 THEN 'Y' END,
		[f].[Fractionation_Bit],
			[Fractionation_Y]		= CASE WHEN [f].[Fractionation_Bit] = 1 THEN 'Y' END,
		[f].[HydroTreat_Bit],
			[HydroTreat_Y]			= CASE WHEN [f].[HydroTreat_Bit]	= 1 THEN 'Y' END,
		[f].[Purification_Bit],
			[Purification_Y]		= CASE WHEN [f].[Purification_Bit]	= 1 THEN 'Y' END,
		[f].[MinConsidDensity_SG],
		[f].[MaxConsidDensity_SG],
		[f].[MinActualDensity_SG],
		[f].[MaxActualDensity_SG]
	FROM
		[fact].[FeedSelPractices]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;