﻿CREATE PROCEDURE [fact].[Select_FeedSelModelPenalty]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (8)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[PenaltyId],
		[f].[Penalty_Bit],
			[Penalty_YN]	= CASE WHEN [f].[Penalty_Bit] = 1 THEN 'Y' ELSE 'N' END
	FROM
		[fact].[FeedSelModelPenalty]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;