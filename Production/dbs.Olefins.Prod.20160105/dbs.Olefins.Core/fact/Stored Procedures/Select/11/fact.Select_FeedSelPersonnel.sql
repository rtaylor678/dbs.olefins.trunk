﻿CREATE PROCEDURE [fact].[Select_FeedSelPersonnel]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (6)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[PersId],
		[f].[Plant_FTE],
		[f].[Corporate_FTE]
	FROM
		[fact].[FeedSelPersonnel]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;