﻿CREATE PROCEDURE [fact].[Select_ReliabilityCompEvents]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-5
	SELECT
		[r].[Refnum],
		[r].[CalDateKey],
		[r].[EventNo],
			[EEventNo]				= 'E' + CONVERT(VARCHAR(3), [r].[EventNo]),
		[r].[CompressorId],
			[WksCompressorId]		= [f].[Compressor],
		[r].[ServiceLevelId],
		[r].[CompComponentId],
			[WksCompComponentId]	= [x].[Component],
		[r].[EventCauseId],
			[WksEventCauseId]		= [y].[Cause],
		[r].[EventOccur_Year],
		[r].[Duration_Hours],
		[c].[Comments]
	FROM
		[fact].[ReliabilityCompEvents]			[r]
	LEFT OUTER JOIN
		[fact].[ReliabilityCompEventComments]	[c]
			ON	[c].[Refnum]					= [r].[Refnum]
			AND	[c].[EventNo]					= [r].[EventNo]

	LEFT OUTER JOIN(VALUES
		('CG', 'CompGas'),
		('C3', 'CompRefrigPropylene'),
		('C2', 'CompRefrigEthylene'),
		('C1', 'CompRefrigMethane'),
		('C2/C3', 'CompRefrigOlefins'),
		('Other', 'CompOther')
		) [f]([Compressor], [FacilityId])
			ON	[f].[FacilityId]	= [r].[CompressorId]

	LEFT OUTER JOIN(VALUES
		('COMPR', 'Comp'),
		('', 'Interruption'),
		('CW', 'InterruptionCooling'),
		('ELEC', 'InterruptionElectric'),
		('FG', 'InterruptionFuelGas'),
		('STEAM', 'InterruptionSteam'),
		('EM', 'MotorElectric'),
		('HX', 'Exchanger'),
		('GB', 'GearBox'),
		('INSTR', 'Instr'),
		('LUBE', 'Lube'),
		('OTHER', 'CompOther'),
		('COMP', 'PrimaryComponent'),
		('', 'System'),
		('SCS', 'SystemSpeed'),
		('AUX', 'SystemAux'),
		('', 'Turbine'),
		('GT', 'TurbineGas'),
		('ST', 'TurbineSteam'),
		('SEAL', ''),
		('INST', '')
		) [x]([Component], [CompComponentId])
			ON	[x].[CompComponentId]	= [r].[CompComponentId]

	LEFT OUTER JOIN(VALUES
		('1', 'CompressorFouling'),
		('2', 'CompressorBearings'),
		('3', 'CompressorSeals'),
		('4', 'CompressorImpeller'),
		('5', 'PistonSeal'),
		('6', 'BearingThrust'),
		('7', 'CasingLeak'),
		('8', 'OverspeedTrip'),
		('9', 'TurbineBlade'),
		('10', 'TurbineBearing'),
		('11', 'ValveGoverner'),
		('12', 'ValveTT'),
		('13', 'SystemSpeed'),
		('14', 'SystemSurge'),
		('15', 'TurbinePower'),
		('16', 'GearBoxShaft'),
		('17', 'GearBoxBearing'),

		('19', 'GearBoxCoupling'),
		('20', 'GlandSeal'),
		('21', 'LubeSeal'),
		('22', 'SteamCondenser'),
		('23', 'Coolers'),
		('24', 'TurbineDisk'),
		('25', 'RV'),
		('26', 'ProcessPiping'),
		('27', 'FalseTripVibration'),
		('28', 'FalseTripOther'),
		('29', 'MotorElectricFault'),
		('30', 'MotorElectricStarter'),
		('31', 'MotorElectricBearing'),
		('32', 'ProcessUpset'),
		('33', 'OtherCause'),

		('', 'Turbine'),
		('', 'Compressor'),
		('', 'FalseTrip'),
		('', 'GearBox'),
		('', 'GearBoxGears'),
		('', 'MotorElectric'),
		('', 'PrimaryCause'),
		('', 'System'),
		('', 'Valve')

		) [y]([Cause], [EventCauseId])
			ON	[y].[EventCauseId]	= [r].[EventCauseId]
	WHERE
		[r].[Refnum]	= @Refnum;

END;