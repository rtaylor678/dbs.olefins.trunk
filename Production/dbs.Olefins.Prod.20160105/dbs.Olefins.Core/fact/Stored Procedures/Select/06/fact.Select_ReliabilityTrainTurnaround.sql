﻿CREATE PROCEDURE [fact].[Select_ReliabilityTrainTurnaround]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-3
	SELECT
		[a].[Refnum],
		[a].[CalDateKey],
		[a].[TrainId],
			[TTrainId]				= 'T' + CONVERT(CHAR(1), [a].[TrainId]),
		[a].[StreamId],
		[a].[Capacity_Pcnt],
			[PlannedInterval_Mnths]	= [p].[Interval_Mnths],
			[PlannedDownTime_Hrs]	= [p].[DownTime_Hrs],
		[a].[Interval_Mnths],
		[a].[DownTime_Hrs],
		[a].[CurrencyRpt],
		[a].[TurnAroundCost_MCur],
		[a].[TurnAround_ManHrs],
		[a].[TurnAround_Date],
			[MiniTA_YN]				= CASE WHEN([a].[MiniTA_Bit] = 1) THEN 'Yes' ELSE 'No' END,
			[MiniTA_DownTime_Hrs]	= [m].[DownTime_Hrs],
			[MiniTA_Cost_MCur]		= [m].[TurnAroundCost_MCur],
			[MiniTA_ManHrs]			= [m].[TurnAround_ManHrs],
			[MiniTA_Date]			= [m].[TurnAround_Date]
	FROM
		[fact].[ReliabilityTA]		[a]
	INNER JOIN
		[fact].[ReliabilityTA]		[p]
			ON	[p].[Refnum]		= [a].[Refnum]
			AND	[p].[CalDateKey]	= [a].[CalDateKey]
			AND	[p].[TrainId]		= [a].[TrainId]
			AND	[p].[SchedId]		= 'P'
	LEFT OUTER JOIN
		[fact].[ReliabilityTAMini]	[m]
			ON	[m].[Refnum]		= [a].[Refnum]
			AND	[m].[CalDateKey]	= [a].[CalDateKey]
			AND	[m].[TrainId]		= [a].[TrainId]
			AND	[m].[CurrencyRpt]	= [a].[CurrencyRpt]
	WHERE	[a].[SchedId]			= 'A'
		AND	[a].[Refnum]			= @Refnum;

END;