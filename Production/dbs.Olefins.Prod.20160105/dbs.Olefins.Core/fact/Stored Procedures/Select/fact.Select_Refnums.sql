﻿CREATE PROCEDURE [fact].[Select_Refnums]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		[t].[Refnum],
		[t].[PreviousRefnum]
	FROM
		[fact].[PlantNameHistoryLimit](2009)	[t]
	WHERE	[t].[StudyYear]	=	2015;

END;