﻿CREATE PROCEDURE [fact].[Select_MetaCapEx]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 13 (4)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[CurrencyRpt],
		[f].[Amount_Cur]
	FROM
		[fact].[MetaCapEx]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;