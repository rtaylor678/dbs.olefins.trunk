﻿CREATE PROCEDURE [fact].[Select_ApcControllers]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 9-3 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[ApcId],
		[f].[Controller_Count]
	FROM
		[fact].[ApcControllers]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;