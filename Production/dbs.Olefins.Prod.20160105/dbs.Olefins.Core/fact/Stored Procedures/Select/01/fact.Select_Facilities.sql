﻿CREATE PROCEDURE [fact].[Select_Facilities]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 1-3
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[FacilityId],
		[f].[Unit_Count],
			[HasUnit_Bit]	= [f].[_Unit_Bit],
			[HasUnit_YN]	= CASE WHEN [f].[_Unit_Bit] = 1 THEN 'Y' ELSE 'N' END,

		[fc].[Age_Years],
		[fc].[Power_BHP],
		[fc].[CoatingId],
		[fc].[DriverId],
		[fc].[Stages_Count],

			[FFStreamId]			= [ff].[StreamId],
			[FFFeedRate_kBsd]		= [ff].[FeedRate_kBsd],
			[FFFeedProcessed_kMT]	= [ff].[FeedProcessed_kMT],
			[FFFeedDensity_SG]		= [ff].[FeedDensity_SG],

		[ht].[HTTypeId],
			[HTFeedRate_kBsd]		= [ht].[FeedRate_kBsd],
			[HTFeedPressure_PSIg]	= [ht].[FeedPressure_PSIg],
			[HTFeedProcessed_kMT]	= [ht].[FeedProcessed_kMT],
			[HTFeedDensity_SG]		= [ht].[FeedDensity_SG],

		[fn].[FacilityName],

		[fp].[Pressure_PSIg],
		[fp].[Rate_kLbHr],

		[fw].[FanHorsepower_bhp],
		[fw].[PumpHorsepower_bhp],
		[fw].[FlowRate_TonneDay],

			[PyroFurnSpare_Pcnt]	= CASE WHEN [f].[FacilityId] = 'PyroFurn'		THEN [fm].[PyroFurnSpare_Pcnt]	END,
			[ElecGen_MW]			= CASE WHEN [f].[FacilityId] = 'ElecGenDist'	THEN [fm].[ElecGen_MW]			END
	FROM
		[fact].[Facilities]				[f]
	LEFT OUTER JOIN
		[fact].[FacilitiesCompressors]	[fc]
			ON	[fc].[Refnum]		= [f].[Refnum]
			AND	[fc].[FacilityId]	= [f].[FacilityId]
	LEFT OUTER JOIN
		[fact].[FacilitiesFeedFrac]		[ff]
			ON	[ff].[Refnum]		= [f].[Refnum]
			AND	[ff].[FacilityId]	= [f].[FacilityId]
	LEFT OUTER JOIN
		[fact].[FacilitiesHydroTreat]	[ht]
			ON	[ht].[Refnum]		= [f].[Refnum]
			AND	[ht].[FacilityId]	= [f].[FacilityId]
	LEFT OUTER JOIN
		[fact].[FacilitiesNameOther]	[fn]
			ON	[fn].[Refnum]		= [f].[Refnum]
			AND	[fn].[FacilityId]	= [f].[FacilityId]
	LEFT OUTER JOIN
		[fact].[FacilitiesPressure]		[fp]
			ON	[fp].[Refnum]		= [f].[Refnum]
			AND	[fp].[FacilityId]	= [f].[FacilityId]
	LEFT OUTER JOIN
		[fact].[FacilitiesCoolingWater]	[fw]
			ON	[fw].[Refnum]		= [f].[Refnum]
			AND	[fw].[FacilityId]	= [f].[FacilityId]
	LEFT OUTER JOIN
		[fact].[FacilitiesMisc]			[fm]
			ON	[fm].[Refnum]		= [f].[Refnum]
	WHERE
		[f].[Refnum]	= @Refnum;

END;