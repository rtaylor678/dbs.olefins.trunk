﻿CREATE PROCEDURE [fact].[Select_GenPlantEnergy]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-4 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[CoolingWater_MTd],
		[f].[CoolingWaterSupplyTemp_C],
		[f].[CoolingWaterReturnTemp_C],
		[f].[FinFanHeatAbsorb_kBTU],
		[f].[CompGasEfficiency_Pcnt],
		[f].[CalcTypeId],
		[f].[SteamExtratRate_kMTd],
		[f].[SteamCondenseRate_kMTd],
		[f].[SurfaceCondVacuum_InH20],
		[f].[ExchangerHeatDuty_BTU],
		[f].[QuenchHeatMatl_Pcnt],
		[f].[QuenchDilution_Pcnt]
	FROM
		[fact].[GenPlantEnergy]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;