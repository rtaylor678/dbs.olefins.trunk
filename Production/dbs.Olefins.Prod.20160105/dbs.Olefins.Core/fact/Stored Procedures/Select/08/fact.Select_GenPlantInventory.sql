﻿CREATE PROCEDURE [fact].[Select_GenPlantInventory]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-1 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[FacilityId],
		[f].[StorageCapacity_kMT],
		[f].[StorageLevel_Pcnt],
		[YearEndInventory_kMT] = [f].[_YearEndInventory_kMT]
	FROM
		[fact].[GenPlantInventory]  [f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;