﻿CREATE PROCEDURE [fact].[Select_StreamsProdOtherComposition]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		--	Table 2A-1, Table 2A-2, Table 2B, Table 2C, Table 3
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[StreamIdInt]		= CASE WHEN [f].[StreamId] IN ('FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										[f].[StreamId] + SUBSTRING([f].[StreamDescription], LEN([f].[StreamDescription]) - 1, 1)
									ELSE
										[f].[StreamId]
									END,
		[f].[StreamDescription],
			[StreamDesc]		= CASE WHEN [f].[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										LEFT([f].[StreamDescription], LEN([f].[StreamDescription]) - CHARINDEX('(', REVERSE([f].[StreamDescription])))
									END,

		--	Table 2A-1, Table 2B, Table 3
		[CH4]	= COALESCE([c].[CH4],	[s].[CH4]),
		[C2H2]	= COALESCE([c].[C2H2],	[s].[C2H2]),
		[C2H4]	= COALESCE([c].[C2H4],	[s].[C2H4]),
		[C2H6]	= COALESCE([c].[C2H6],	[s].[C2H6]),

		[C3H6]	= COALESCE([c].[C3H6],	[s].[C3H6]),
		[C3H8]	= COALESCE([c].[C3H8],	[s].[C3H8]),

		[C4H6]	= COALESCE([c].[C4H6],	[s].[BUTAD]),
		[C4H8]	= COALESCE([c].[C4H8],	[s].[C4S]),
		[C4H10]	= COALESCE([c].[C4H10],	[s].[C4H10]),

		[C6H6]	= COALESCE([c].[C6H6],	[s].[BZ]),
		[c].[C7H16],
		[c].[C8H18],

		[c].[NBUTA],
		[c].[IBUTA],
		[c].[IB],
		[c].[B1],
		[c].[NC5],
		[c].[IC5],
		[c].[NC6],
		[c].[C6ISO],
		[c].[CO_CO2],

		--	Table 2A-2
		[H2]	= COALESCE([c].[H2], [s].[H2]),
		[c].[S],

		--	Table 3
		[PyroGasoline]	= COALESCE([c].[PyroGasoline],	[s].[PYGAS]),
		[PyroFuelOil]	= COALESCE([c].[PyroFuelOil],	[s].[PYOIL]),
		[Inerts]		= COALESCE([c].[Inerts],		[s].[INERTS])

	FROM
		[fact].[QuantityPivot]					[f]
	LEFT OUTER JOIN
		[fact].[CompositionQuantityPivot]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[StreamId]			= [f].[StreamId]
			AND	[c].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[super].[Composition]					[s]
			ON	[s].[Refnum]			= [f].[Refnum]
			AND	[s].[StreamId]			= [f].[StreamId]
	CROSS APPLY(
		SELECT
			[Amount_Cur] = AVG([q].[Amount_Cur])
		FROM
			[super].[QuantityValue]					[q]
		WHERE	[q].[Refnum]			= [f].[Refnum]
			AND	[q].[StreamId]			= [f].[StreamId]
			AND	[q].[StreamDescription]	= [f].[StreamDescription]
		) [q]
	WHERE	[f].[StreamId]	= 'ProdOther'
		AND	[f].[Refnum]	= @Refnum;

END;