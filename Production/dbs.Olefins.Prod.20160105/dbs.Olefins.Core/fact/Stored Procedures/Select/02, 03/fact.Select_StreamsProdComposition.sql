﻿CREATE PROCEDURE [fact].[Select_StreamsProdComposition]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		--	Table 2A-1, Table 2A-2, Table 2B, Table 2C, Table 3
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[StreamIdInt]		= CASE WHEN [f].[StreamId] IN ('FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										[f].[StreamId] + SUBSTRING([f].[StreamDescription], LEN([f].[StreamDescription]) - 1, 1)
									ELSE
										[f].[StreamId]
									END,

		[CH4]	= CASE WHEN [f].[StreamId] <> 'Methane' THEN COALESCE([c].[CH4],	[s].[CH4]) END,

		[C2H4]	= COALESCE([c].[C2H4],	[s].[C2H4]),
		[C3H6]	= COALESCE([c].[C3H6],	[s].[C3H6]),
		[C3H8]	= COALESCE([c].[C3H8],	[s].[C3H8]),

		[H2Mol_Pcnt]	= CASE WHEN [f].[StreamId] = 'Hydrogen'
					THEN
						[calc].[ConvMoleWeight]('H2',  [c].[H2], 'M')
					END,

		[CH4Mol_Pcnt]	= CASE WHEN [f].[StreamId] = 'Methane'
					THEN
						[calc].[ConvMoleWeight]('CH4',  [c].[CH4], 'M')
					END

	FROM
		[fact].[QuantityPivot]					[f]
	INNER JOIN
		[dim].[Stream_Bridge]					[b]
			ON	[b].[DescendantId]		= [f].[StreamId]
			AND	[b].[FactorSetId]		= '2013'
			AND	[b].[StreamId]			= 'Prod'
	INNER JOIN
		[fact].[CompositionQuantityPivot]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[StreamId]			= [f].[StreamId]
			AND	[c].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[super].[Composition]					[s]
			ON	[s].[Refnum]			= [f].[Refnum]
			AND	[s].[StreamId]			= [f].[StreamId]
	WHERE	[f].[Refnum]	= @Refnum;

END;