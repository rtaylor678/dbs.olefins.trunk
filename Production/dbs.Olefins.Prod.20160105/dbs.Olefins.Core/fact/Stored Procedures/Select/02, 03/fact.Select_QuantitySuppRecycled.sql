﻿CREATE PROCEDURE [fact].[Select_QuantitySuppRecycled]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 2C
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[StreamIdInt]	= [f].[StreamId],
		[f].[ComponentId],
		[f].[Recycled_WtPcnt]
	FROM
		[fact].[QuantitySuppRecycled]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;