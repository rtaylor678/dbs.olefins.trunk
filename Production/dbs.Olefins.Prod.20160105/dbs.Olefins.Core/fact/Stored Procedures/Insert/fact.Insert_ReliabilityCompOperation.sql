﻿CREATE PROCEDURE [fact].[Insert_ReliabilityCompOperation]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[ReliabilityCompOperation]
		(
			[Refnum],
			[CalDateKey],
			[Operation_Years]
		)
		SELECT
				[Refnum]		= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
				[CalDateKey]	= [etl].[ConvDateKey]([t].[StudyYear]),
			[c].[CompLife]
		FROM
			[stgFact].[TSort]		[t]
		INNER JOIN
			[stgFact].[PracRely]	[c]
				ON	[c].[Refnum]	= [t].[Refnum]
		WHERE	[c].[CompLife]		IS NOT NULL
			AND	[t].[Refnum]		= @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;