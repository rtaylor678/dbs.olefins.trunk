﻿CREATE PROCEDURE [fact].[Insert_Quantity]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Quantity(Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT)
		SELECT
			  u.Refnum
			, CONVERT(INT, CONVERT(VARCHAR(4), u.StudyYear) + CONVERT(VARCHAR(4), u.CalDateKey))
																	[CalDateKey]
			, etl.ConvStreamID(u.FeedProdId)						[StreamId]
			, ISNULL(u.StreamDescription, u.FeedProdId)				[StreamDescription]

			, ABS(u.kMT)											[kMT]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)		[Refnum]
				, t.StudyYear
				, q.FeedProdId	
				, etl.ConvStreamDescription(q.FeedProdId, f.OthLiqFeedDESC, q.MiscProd1, q.MiscProd2, q.MiscFeed) [StreamDescription]
				, ISNULL(q.Q1Feed, 0.0)								[0331]
				, ISNULL(q.Q2Feed, 0.0)								[0630]
				, ISNULL(q.Q3Feed, 0.0)								[0930]
				, ISNULL(q.Q4Feed, 0.0)								[1231]
			
			FROM stgFact.Quantity q
			LEFT OUTER JOIN stgFact.FeedQuality f
				ON	f.Refnum = q.Refnum
				AND f.FeedProdId = q.FeedProdId
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = q.Refnum
			WHERE (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
				AND q.FeedProdId <> 'CO & CO2'
				AND t.Refnum = @sRefnum
			) p
			UNPIVOT(
			kMT FOR CalDateKey IN (
				[0331], [0630], [0930], [1231]
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;