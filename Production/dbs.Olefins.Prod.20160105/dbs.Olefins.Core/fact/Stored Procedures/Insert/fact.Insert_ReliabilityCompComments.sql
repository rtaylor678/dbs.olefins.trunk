﻿CREATE PROCEDURE [fact].[Insert_ReliabilityComments]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[ReliabilityCompEventComments]
		(
			[Refnum],
			[CalDateKey],
			[EventNo],
			[Comments]
		)
		SELECT
				[Refnum]		= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
				[CalDateKey]	= [etl].[ConvDateKey]([t].[StudyYear]),
			[c].[EventNo],
			[c].[Comments]
		FROM
			[stgFact].[TSort]				[t]
		INNER JOIN
			[stgFact].[Compressors]			[c]
				ON	[c].[Refnum]			= [t].[Refnum]
		WHERE	[c].[Comments]	<> ''
			AND	[c].[Duration]	IS NOT NULL
			AND	[t].[Refnum]	= @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;