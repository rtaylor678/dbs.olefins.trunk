﻿CREATE TABLE [fact].[FeedStockCrackingParameters] (
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [StreamId]                VARCHAR (42)       NOT NULL,
    [StreamDescription]       NVARCHAR (256)     NOT NULL,
    [CoilOutletPressure_Psia] REAL               NULL,
    [CoilOutletTemp_C]        REAL               NULL,
    [CoilInletPressure_Psia]  REAL               NULL,
    [CoilInletTemp_C]         REAL               NULL,
    [RadiantWallTemp_C]       REAL               NULL,
    [SteamHydrocarbon_Ratio]  REAL               NULL,
    [EthyleneYield_WtPcnt]    REAL               NULL,
    [PropyleneEthylene_Ratio] REAL               NULL,
    [PropyleneMethane_Ratio]  REAL               NULL,
    [FeedConv_WtPcnt]         REAL               NULL,
    [DistMethodId]            VARCHAR (8)        NULL,
    [SulfurContent_ppm]       REAL               NULL,
    [Density_SG]              REAL               NULL,
    [FurnConstruction_Year]   REAL               NULL,
    [ResidenceTime_s]         REAL               NULL,
    [FlowRate_KgHr]           REAL               NULL,
    [Recycle_Bit]             BIT                CONSTRAINT [DF_FeedStockCrackingParameters_Recycle_Bit] DEFAULT ((0)) NOT NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_FeedStockCrackingParameters_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_FeedStockCrackingParameters_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_FeedStockCrackingParameters_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_FeedStockCrackingParameters_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedStockCrackingParameters] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_FeedStockCrackingParameters_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_FeedStockCrackingParameters_CoilInletPressure_Psia] CHECK ([CoilInletPressure_Psia]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_CoilInletTemp_C] CHECK ([CoilInletTemp_C]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_CoilOutletPressure_Psia] CHECK ([CoilOutletPressure_Psia]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_CoilOutletTemp_C] CHECK ([CoilOutletTemp_C]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_Density_SG] CHECK ([Density_SG]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_EthyleneYield_WtPcnt] CHECK ([EthyleneYield_WtPcnt]>=(0.0) AND [EthyleneYield_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_FeedConv_WtPcnt] CHECK ([FeedConv_WtPcnt]>=(0.0) AND [FeedConv_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_FlowRate_KgHrs] CHECK ([FlowRate_KgHr]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_FurnConstruction_Year] CHECK ([FurnConstruction_Year]>=(1900.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_PropylemeEthylene_Ratio] CHECK ([PropyleneEthylene_Ratio]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_PropylemeMethane_Ratio] CHECK ([PropyleneMethane_Ratio]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_RadiantWallTemp_C] CHECK ([RadiantWallTemp_C]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_ResidenceTime_s] CHECK ([ResidenceTime_s]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_SteamHydrocarbon_Ratio] CHECK ([SteamHydrocarbon_Ratio]>=(0.0)),
    CONSTRAINT [CR_FeedStockCrackingParameters_SulfurContent_ppm] CHECK ([SulfurContent_ppm]>=(0.0)),
    CONSTRAINT [FK_FeedStockCrackingParameters_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedStockCrackingParameters_FeedStockDistMethod_LookUp] FOREIGN KEY ([DistMethodId]) REFERENCES [dim].[FeedStockDistMethod_LookUp] ([DistMethodId]),
    CONSTRAINT [FK_FeedStockCrackingParameters_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FeedStockCrackingParameters_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedStockCrackingParameters_u]
	ON [fact].[FeedStockCrackingParameters]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedStockCrackingParameters]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedStockCrackingParameters].Refnum					= INSERTED.Refnum
		AND [fact].[FeedStockCrackingParameters].StreamId				= INSERTED.StreamId
		AND [fact].[FeedStockCrackingParameters].StreamDescription		= INSERTED.StreamDescription
		AND [fact].[FeedStockCrackingParameters].CalDateKey				= INSERTED.CalDateKey;

END;