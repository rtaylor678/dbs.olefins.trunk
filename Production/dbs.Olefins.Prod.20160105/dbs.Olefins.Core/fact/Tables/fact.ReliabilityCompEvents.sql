﻿CREATE TABLE [fact].[ReliabilityCompEvents] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [EventNo]         TINYINT            NOT NULL,
    [CompressorId]    VARCHAR (42)       NOT NULL,
    [ServiceLevelId]  VARCHAR (14)       NULL,
    [CompComponentId] VARCHAR (42)       NULL,
    [EventCauseId]    VARCHAR (42)       NULL,
    [EventOccur_Year] INT                NOT NULL,
    [Duration_Hours]  REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityCompEvents_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompEvents_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompEvents_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompEvents_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityCompEvents] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EventNo] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReliabilityCompEvents_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityCompEvents_Facility_LookUp] FOREIGN KEY ([CompressorId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_ReliabilityCompEvents_ReliabilityCompComponent_LookUp] FOREIGN KEY ([CompComponentId]) REFERENCES [dim].[ReliabilityCompComponent_LookUp] ([CompComponentId]),
    CONSTRAINT [FK_ReliabilityCompEvents_ReliabilityEventCause_LookUp] FOREIGN KEY ([EventCauseId]) REFERENCES [dim].[ReliabilityEventCause_LookUp] ([EventCauseId]),
    CONSTRAINT [FK_ReliabilityCompEvents_ReliabilityServiceLevel_LookUp] FOREIGN KEY ([ServiceLevelId]) REFERENCES [dim].[ReliabilityServiceLevel_LookUp] ([ServiceLevelId]),
    CONSTRAINT [FK_ReliabilityCompEvents_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityCompEvents_u]
	ON [fact].[ReliabilityCompEvents]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityCompEvents]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityCompEvents].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityCompEvents].EventNo		= INSERTED.EventNo
		AND [fact].[ReliabilityCompEvents].CalDateKey	= INSERTED.CalDateKey;

END;