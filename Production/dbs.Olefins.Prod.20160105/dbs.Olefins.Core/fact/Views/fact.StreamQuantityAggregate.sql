﻿CREATE VIEW [fact].[StreamQuantityAggregate]
WITH SCHEMABINDING
AS
SELECT
	b.[FactorSetId],
	q.[Refnum],
	--MAX(q.[CalDateKey])	[CalDateKey],
	b.[StreamId],
	CASE WHEN b.[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'ProdOther') THEN q.[StreamDescription] END	[StreamDescription],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + q.[Quantity_kMT]
		WHEN '-' THEN - q.[Quantity_kMT]
		ELSE 0.0
		END)							[Quantity_kMT],
	COUNT_BIG(*)						[IndexItems]
FROM [dim].[Stream_Bridge]					b
INNER JOIN [fact].[Quantity]				q
	ON	q.[StreamId] = b.[DescendantId]
GROUP BY
	b.[FactorSetId],
	q.[Refnum],
	b.[StreamId],
	CASE WHEN b.[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'ProdOther') THEN q.[StreamDescription] END;
GO
CREATE UNIQUE CLUSTERED INDEX [UX_StreamQuantityAggregate]
    ON [fact].[StreamQuantityAggregate]([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_StreamQuantityAggregate]
    ON [fact].[StreamQuantityAggregate]([StreamId] ASC)
    INCLUDE([FactorSetId], [Refnum], [Quantity_kMT]);

