﻿CREATE VIEW fact.QuantityPivot
WITH SCHEMABINDING
AS
SELECT
	p.Refnum,
	p.CalDateKey,
	p.StreamId,
	p.StreamDescription,
	p.Q1_kMT,
	p.Q2_kMT,
	p.Q3_kMT,
	p.Q4_kMT,
	ISNULL(p.Q1_kMT, 0.0)
	+ ISNULL(p.Q2_kMT, 0.0)
	+ ISNULL(p.Q3_kMT, 0.0)
	+ ISNULL(p.Q4_kMT, 0.0)			[Tot_kMT],
	(ISNULL(p.Q1_kMT, 0.0)
	+ ISNULL(p.Q2_kMT, 0.0)
	+ ISNULL(p.Q3_kMT, 0.0)
	+ ISNULL(p.Q4_kMT, 0.0)) / 4.0	[Avg_kMT]
FROM(
	SELECT
		q.Refnum,
		CONVERT(INT, CONVERT(VARCHAR(4), d.CalYear) + '1231')	[CalDateKey],
		'Q' + CONVERT(CHAR(1), d.CalQtr) + '_kMT'					[CalQtr],
		q.StreamId,
		q.StreamDescription,
		q.Quantity_kMT
	FROM fact.Quantity q
	INNER JOIN dim.Calendar_LookUp d ON d.CalDateKey = q.CalDateKey
	WHERE q.Quantity_kMT > 0.0
	) u
	PIVOT(
	MAX(Quantity_kMT) FOR CalQtr IN (
		[Q1_kMT],
		[Q2_kMT],
		[Q3_kMT],
		[Q4_kMT]
		)
	) p;