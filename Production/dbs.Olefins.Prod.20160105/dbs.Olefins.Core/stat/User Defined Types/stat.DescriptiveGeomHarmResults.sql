﻿CREATE TYPE [stat].[DescriptiveGeomHarmResults] AS TABLE (
    [xCount]  SMALLINT   NOT NULL,
    [gMean]   FLOAT (53) NOT NULL,
    [wgMean]  FLOAT (53) NULL,
    [gStDevP] FLOAT (53) NOT NULL,
    [hMean]   FLOAT (53) NOT NULL,
    [whMean]  FLOAT (53) NULL,
    CHECK ([xCount]>=(1)),
    CHECK ([gStDevP]>=(0.0)));

