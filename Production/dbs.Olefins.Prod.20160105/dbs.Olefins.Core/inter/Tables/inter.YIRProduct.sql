﻿CREATE TABLE [inter].[YIRProduct] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_kMT]    REAL               NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_YIRProduct_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_YIRProduct_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_YIRProduct_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_YIRProduct_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_inter_YIRProduct] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_YIRProduct_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_YIRProduct_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_YIRProduct_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_YIRProduct_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [inter].[t_Inter_YIRProduct_u]
	ON [inter].[YIRProduct]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.YIRProduct
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.YIRProduct.FactorSetId		= INSERTED.FactorSetId
		AND inter.YIRProduct.Refnum				= INSERTED.Refnum
		AND inter.YIRProduct.CalDateKey			= INSERTED.CalDateKey
		AND inter.YIRProduct.ComponentId		= INSERTED.ComponentId;

END;