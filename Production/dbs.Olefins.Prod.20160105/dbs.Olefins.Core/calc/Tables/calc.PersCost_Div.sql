﻿CREATE TABLE [calc].[PersCost_Div] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [AccountId]            VARCHAR (42)       NOT NULL,
    [CurrencyRpt]          VARCHAR (4)        NOT NULL,
    [DivisorId]            VARCHAR (42)       NOT NULL,
    [DivisorField]         VARCHAR (42)       NOT NULL,
    [DivsorAmount]         REAL               NOT NULL,
    [TaLabor_Cur]          REAL               NULL,
    [Amount_Cur]           REAL               NULL,
    [Ann_Amount_Cur]       REAL               NULL,
    [TaAdjAnn_Amount_Cur]  REAL               NULL,
    [_TaLabor_Cur]         AS                 (CONVERT([real],[TaLabor_Cur]/[DivsorAmount],(1))) PERSISTED,
    [_Amount_Cur]          AS                 (CONVERT([real],[Amount_Cur]/[DivsorAmount],(1))) PERSISTED,
    [_Ann_Amount_Cur]      AS                 (CONVERT([real],[Ann_Amount_Cur]/[DivsorAmount],(1))) PERSISTED,
    [_TaAdjAnn_Amount_Cur] AS                 (CONVERT([real],[TaAdjAnn_Amount_Cur]/[DivsorAmount],(1))) PERSISTED,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_PersCost_Div_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_PersCost_Div_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_PersCost_Div_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_PersCost_Div_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Account_LookUpPersCost_Div] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [AccountId] ASC, [DivisorId] ASC, [DivisorField] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_Account_LookUpPersCost_Div_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_calc_PersCost_Div_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PersCost_Div_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_PersCost_Div_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PersCost_Div_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [calc].[t_PersCost_Div_u]
	ON [calc].[PersCost_Div]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.PersCost_Div
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PersCost_Div.FactorSetId	= INSERTED.FactorSetId
		AND calc.PersCost_Div.Refnum		= INSERTED.Refnum
		AND calc.PersCost_Div.CalDateKey	= INSERTED.CalDateKey
		AND calc.PersCost_Div.CalDateKey	= INSERTED.AccountId;
	
END