﻿CREATE TABLE [calc].[StandardEnergyRedCrackedGas] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [StandardEnergyId]       VARCHAR (42)       NOT NULL,
    [StreamId]               VARCHAR (42)       NOT NULL,
    [StreamDescription]      NVARCHAR (256)     NOT NULL,
    [Stream_kMT]             REAL               NOT NULL,
    [Energy_MBTU]            REAL               NOT NULL,
    [StandardEnergy_MBtuDay] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyRedCrackedGas_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyRedCrackedGas_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyRedCrackedGas_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyRedCrackedGas_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyRedCrackedGas] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StandardEnergyId] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_StandardEnergyRedCrackedGas_Energy_MBTU] CHECK ([Energy_MBTU]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyRedCrackedGas_StandardEnergy_MBtuDay] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyRedCrackedGas_Stream_kMT] CHECK ([Stream_kMT]>=(0.0)),
    CONSTRAINT [FK_StandardEnergyRedCrackedGas_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_StandardEnergyRedCrackedGas_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergyRedCrackedGas_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergyRedCrackedGas_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_StandardEnergyRedCrackedGas_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_CalcStandardEnergyRedCrackedGas_u
	ON  calc.StandardEnergyRedCrackedGas
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.StandardEnergyRedCrackedGas
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.StandardEnergyRedCrackedGas.[FactorSetId]			= INSERTED.[FactorSetId]
		AND	calc.StandardEnergyRedCrackedGas.[Refnum]				= INSERTED.[Refnum]
		AND calc.StandardEnergyRedCrackedGas.[StandardEnergyId]		= INSERTED.[StandardEnergyId]
		AND calc.StandardEnergyRedCrackedGas.[StreamId]				= INSERTED.[StreamId]
		AND calc.StandardEnergyRedCrackedGas.[StreamDescription]	= INSERTED.[StreamDescription]
		AND calc.StandardEnergyRedCrackedGas.[CalDateKey]			= INSERTED.[CalDateKey];
				
END;