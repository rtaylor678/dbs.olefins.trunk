﻿CREATE TABLE [calc].[Divisors] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [DivisorId]         VARCHAR (42)       NOT NULL,
    [DivisorField]      VARCHAR (42)       NOT NULL,
    [DivisorValue]      REAL               NOT NULL,
    [DivisorMultiplier] REAL               NOT NULL,
    [_Divisor]          AS                 (CONVERT([real],[DivisorValue]*[DivisorMultiplier],(1))) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Divisors_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_Divisors_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_Divisors_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_Divisors_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_Divisors] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [DivisorId] ASC, [DivisorField] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_Divisors_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_Divisors_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_Divisors_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_Divisors_u
	ON  calc.Divisors
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.Divisors
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.Divisors.FactorSetId		= INSERTED.FactorSetId
		AND calc.Divisors.Refnum			= INSERTED.Refnum
		AND calc.Divisors.CalDateKey		= INSERTED.CalDateKey
		AND calc.Divisors.DivisorId			= INSERTED.DivisorId
		AND calc.Divisors.DivisorField		= INSERTED.DivisorField;
	
END