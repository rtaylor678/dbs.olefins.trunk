﻿CREATE PROCEDURE [calc].[Insert_PeerGroup_Capacity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PeerGroupCapacity(FactorSetId, Refnum, CalDateKey, PeerGroup, Capacity_kMT)
		SELECT
			pg.FactorSetId,
			a.Refnum,
			a.CalDateKey,
			pg.PeerGroup,
			ABS(a.Capacity_kMT)
		FROM	@fpl							fpl
		INNER JOIN	ante.PeerGroupCapacity		pg
			ON	pg.FactorSetId = fpl.FactorSetId
		INNER JOIN fact.CapacityAggregate		a
			ON	a.FactorSetId = fpl.FactorSetId
			AND	a.Refnum = fpl.Refnum
			AND	a.CalDateKey = fpl.Plant_QtrDateKey
			AND	ABS(a.Capacity_kMT) >=  pg.LimitLower_kMT
			AND	ABS(a.Capacity_kMT) <	pg.LimitUpper_kMT
		WHERE	fpl.CalQtr = 4
			AND	a.StreamId = 'ProdOlefins'

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;