﻿CREATE PROCEDURE [calc].[Insert_Divisors_Efficiency]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Divisors(FactorSetId, Refnum, CalDateKey, DivisorId, DivisorField, DivisorValue, DivisorMultiplier)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			e.EfficiencyId,
			e.UnitId,
			e.EfficiencyStandard,
			1.0
		FROM @fpl												fpl
		INNER JOIN calc.EfficiencyAggregate						e
			ON	e.FactorSetId	= fpl.FactorSetId
			AND	e.Refnum		= fpl.Refnum
			AND	e.CalDateKey	= fpl.Plant_QtrDateKey
		WHERE	e.UnitId = 'UnitTot'
			AND	fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;