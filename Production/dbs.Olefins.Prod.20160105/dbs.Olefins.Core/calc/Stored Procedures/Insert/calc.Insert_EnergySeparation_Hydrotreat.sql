﻿CREATE PROCEDURE [calc].[Insert_EnergySeparation_Hydrotreat]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[EnergySeparation]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [EnergySeparation_BtuLb])
		SELECT
			h.FactorSetId,
			h.Refnum,
			h.Plant_QtrDateKey,
			h.FacilityId,
			h.FeedProcessed_kMT *
				(e.StdEnergy_kBTUbbl * 1000.0 / 42.0) /
				(SUM(h.SG) * 8.34)	[EnergySeparation_BtuLb]
		FROM (
			SELECT
				tsq.FactorSetId,
				tsq.Refnum,
				tsq.Plant_QtrDateKey,
				h.FacilityId,
				h.FeedProcessed_kMT,
				h.HTTypeId,
				CASE p.StreamId
					WHEN 'PyroGasoline'	THEN h.FeedDensity_SG
					WHEN 'Benzene'		THEN 0.8845
					END * p.Tot_kMT /
				SUM(p.Tot_kMT) OVER(PARTITION BY
					tsq.FactorSetId,
					tsq.Refnum,
					tsq.Plant_QtrDateKey
					)								[SG]
			FROM @fpl												tsq
			INNER JOIN fact.FacilitiesHydroTreat				h
				ON	h.Refnum = tsq.Refnum
				AND	h.CalDateKey = tsq.Plant_QtrDateKey
			INNER JOIN fact.QuantityPivot			p
				ON	p.Refnum = tsq.Refnum
				AND	p.CalDateKey = tsq.Plant_QtrDateKey
			WHERE p.StreamId IN ('PyroGasoline', 'Benzene')
				AND	tsq.CalQtr = 4
			) h
		INNER JOIN ante.HydroTreaterTypeEnergy	e
			ON	e.FactorSetId = h.FactorSetId
			AND	e.HTTypeId = h.HTTypeId
		GROUP BY
			h.FactorSetId,
			h.Refnum,
			h.Plant_QtrDateKey,
			h.FacilityId,
			h.FeedProcessed_kMT,
			e.StdEnergy_kBTUbbl;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;