﻿CREATE PROCEDURE calc.Insert_FacilitiesCountComplexity
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.FacilitiesCountComplexity (Declarations)';
		PRINT @ProcedureDesc;

		DECLARE @MaxValue			REAL;

		DECLARE @AdjCount TABLE
		(
			FactorSetId			VARCHAR(12)			NOT	NULL	CHECK(FactorSetId <> ''),
			Refnum				VARCHAR(25)			NOT	NULL	CHECK(Refnum <> ''),
			FacilityId			VARCHAR(42)			NOT	NULL	CHECK(FacilityId <> ''),
			Adj_Count			REAL					NULL	CHECK(Adj_Count > 0),
			Replacement_Cnt		INT						NULL,
			PRIMARY KEY CLUSTERED(FactorSetId ASC, Refnum ASC, FacilityId ASC)
		);

		--	HydroPur
		SET @MaxValue = 2.0;

		INSERT INTO @AdjCount(FactorSetId, Refnum, FacilityId, Adj_Count)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.FacilityId,
			t.Unit_Count / t.Tot_Count * @MaxValue	[Adj_Count]
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey				[CalDateKey],
				f.FacilityId,
				CONVERT(REAL, f.Unit_Count, 1)		[Unit_Count],
				CONVERT(REAL, SUM(f.Unit_Count) OVER (PARTITION BY fpl.FactorSetId, fpl.Plant_QtrDateKey, fpl.Refnum), 1)
													[Tot_Count]
			FROM @fpl						fpl
			INNER JOIN fact.Facilities		f
				ON	f.Refnum = fpl.Refnum
				AND	f.CalDateKey = fpl.Plant_QtrDateKey
				AND	f.Unit_Count > 0
			INNER JOIN dim.Facility_Bridge	b
				ON	b.FactorSetId = fpl.FactorSetId
				AND	b.DescendantId = f.FacilityId
				AND	b.FacilityId = 'HydroPur'
			WHERE fpl.CalQtr = 4
			) t
		WHERE t.Tot_Count > @MaxValue;

		--	VesselOth
		SET @MaxValue = 4.0;

		INSERT INTO @AdjCount(FactorSetId, Refnum, FacilityId, Adj_Count)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.FacilityId,
			t.Unit_Count / t.Tot_Count * @MaxValue	[Adj_Count]
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey				[CalDateKey],
				f.FacilityId,
				CONVERT(REAL, f.Unit_Count, 1)		[Unit_Count],
				CONVERT(REAL, SUM(f.Unit_Count) OVER (PARTITION BY fpl.FactorSetId, fpl.Plant_QtrDateKey, fpl.Refnum), 1)
													[Tot_Count]
			FROM @fpl						fpl
			INNER JOIN fact.Facilities		f
				ON	f.Refnum = fpl.Refnum
				AND	f.CalDateKey = fpl.Plant_QtrDateKey
				AND	f.Unit_Count > 0
			INNER JOIN dim.Facility_Bridge	b
				ON	b.FactorSetId = fpl.FactorSetId
				AND	b.DescendantId = f.FacilityId
				AND	b.FacilityId = 'VesselOth'
			WHERE fpl.CalQtr = 4
			) t
		WHERE t.Tot_Count > @MaxValue;

		-- EthyLiq, TowerPyroGasHT, SteamSuperHeat
		SET @MaxValue = 1.0;
	
		INSERT INTO @AdjCount(FactorSetId, Refnum, FacilityId, Adj_Count)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			f.FacilityId,
			@MaxValue
		FROM @fpl						fpl
		INNER JOIN fact.Facilities		f
			ON	f.Refnum = fpl.Refnum
			AND	f.CalDateKey = fpl.Plant_QtrDateKey
			AND	f.FacilityId IN ('TowerPyroGasHT', 'SteamSuperHeat')
			AND	f.Unit_Count > 0
		WHERE fpl.CalQtr = 4;

		INSERT INTO @AdjCount(FactorSetId, Refnum, FacilityId, Replacement_Cnt)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			f.FacilityId,
			@MaxValue
		FROM @fpl						fpl
		INNER JOIN fact.Facilities		f
			ON	f.Refnum = fpl.Refnum
			AND	f.CalDateKey = fpl.Plant_QtrDateKey
			AND	f.FacilityId = 'TowerCool'
			AND	f.Unit_Count > 0
		WHERE fpl.CalQtr = 4;

		INSERT INTO @AdjCount(FactorSetId, Refnum, FacilityId, Adj_Count, Replacement_Cnt)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			f.FacilityId,
			@MaxValue,
			@MaxValue
		FROM @fpl						fpl
		INNER JOIN fact.Facilities		f
			ON	f.Refnum = fpl.Refnum
			AND	f.CalDateKey = fpl.Plant_QtrDateKey
			AND	f.FacilityId = 'EthyLiq'
			AND	f.Unit_Count > 0
		WHERE fpl.CalQtr = 4;

		-- Aggregate Count Complexity
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.FacilitiesCountComplexity';
		PRINT @ProcedureDesc;

		INSERT INTO calc.FacilitiesCountComplexity(FactorSetId, Refnum, CalDateKey, FacilityId, Unit_Count, Unit_Complexity, Adj_Count, Adj_Complexity, Replacement_Wt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			b.FacilityId,
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(f.Unit_Count, 0.0)
				WHEN '-' THEN - ISNULL(f.Unit_Count, 0.0)
				END)										[Unit_Count],

			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN +						CONVERT(REAL, ISNULL(f.Unit_Count, 0.0), 1)		/ ISNULL(c.Complexity_Div, 1.0)
				WHEN '-' THEN -						CONVERT(REAL, ISNULL(f.Unit_Count, 0.0), 1)		/ ISNULL(c.Complexity_Div, 1.0)
				END)										[Unit_Complexity],

			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(a.Adj_Count, CONVERT(REAL, ISNULL(f.Unit_Count, 0.0), 1))
				WHEN '-' THEN - ISNULL(a.Adj_Count, CONVERT(REAL, ISNULL(f.Unit_Count, 0.0), 1))
				END)										[Adj_Count],

			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(a.Adj_Count, CONVERT(REAL, ISNULL(f.Unit_Count, 0.0), 1))	/ ISNULL(c.Complexity_Div, 1.0)
				WHEN '-' THEN - ISNULL(a.Adj_Count, CONVERT(REAL, ISNULL(f.Unit_Count, 0.0), 1))	/ ISNULL(c.Complexity_Div, 1.0)
				END)										[Adj_Complexity],

			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(a.Replacement_Cnt, ISNULL(f.Unit_Count, 0.0)) * ISNULL(c.Replacement_Wt, 0.0)
				WHEN '-' THEN - ISNULL(a.Replacement_Cnt, ISNULL(f.Unit_Count, 0.0)) * ISNULL(c.Replacement_Wt, 0.0)
				END)										[Replacement_Wt]

		FROM @fpl												tsq

		INNER JOIN fact.Facilities								f
			ON	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey

		--	Task 141: Implement Tri-Tables in calculations
		--INNER JOIN dim.BridgeFacilitiesLu						b
		--	ON	b.DescendantId = f.FacilityId
		INNER JOIN dim.Facility_Bridge							b
			ON	b.FactorSetId = tsq.FactorSetId
			AND	b.DescendantId = f.FacilityId

		LEFT OUTER JOIN @AdjCount								a
			ON	a.FactorSetId = tsq.FactorSetId
			AND	a.Refnum = f.Refnum
			AND	a.FacilityId = f.FacilityId
			AND	a.FacilityId = b.DescendantId

		LEFT OUTER JOIN ante.FacilitiesComplexityDivisors		c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.FacilityId = f.FacilityId
			AND	c.FacilityId = b.DescendantId

		WHERE	f.Unit_Count > 0.0
			AND	tsq.CalQtr = 4

		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			b.FacilityId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;