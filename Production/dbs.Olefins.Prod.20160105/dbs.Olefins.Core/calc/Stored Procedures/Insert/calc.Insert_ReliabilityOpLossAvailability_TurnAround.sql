﻿CREATE PROCEDURE [calc].[Insert_ReliabilityOpLossAvailability_TurnAround]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ReliabilityOpLossAvailability]([FactorSetId], [Refnum], [CalDateKey], [OppLossId], [DownTimeLoss_MT], [TotLoss_MT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			'TurnAround'					[OppLossId],
			a.[Ann_FullPlantEquivalentDT_HrsYr] * c.[_StreamAvg_MTd] / 24.0	[DownTimeLoss_kMT],
			a.[Ann_FullPlantEquivalentDT_HrsYr] * c.[_StreamAvg_MTd] / 24.0	[TotLoss_MT]
		FROM @fpl									fpl
		INNER JOIN [calc].[CapacityPlant]			c
			ON	c.[FactorSetId]	= fpl.[FactorSetId]
			AND	c.[Refnum]		= fpl.[Refnum]
			AND	c.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	c.[StreamId]	= 'Ethylene'
		INNER JOIN [calc].[TaAdjCapacityAggregate]	a
			ON	a.[FactorSetId]	= fpl.[FactorSetId]
			AND	a.[Refnum]		= fpl.[Refnum]
			AND	a.[CalDateKey]	= fpl.[Plant_QtrDateKey]	
			AND	a.[SchedId]		= 'A'
		WHERE	fpl.[CalQtr]	= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;