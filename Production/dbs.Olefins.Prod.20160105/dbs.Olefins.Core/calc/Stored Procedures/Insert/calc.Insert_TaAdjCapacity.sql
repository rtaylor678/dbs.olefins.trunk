﻿CREATE PROCEDURE calc.Insert_TaAdjCapacity
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.TaAdjCapacity(
			FactorSetId,
			Refnum,
			CalDateKey,
			TrainId,
			SchedId,
			StreamId,
			Capacity_Pcnt,
			CapacityCalc_Pcnt,
			Interval_Mnths,
			Interval_Years,
			DownTime_Hrs,
			TurnAround_Date,
			TurnAroundInStudyYear_Bit,
			CurrencyRpt,
			TurnAroundCost_MCur,
			TurnAround_ManHrs,
			Ann_DownTime_Hrs,
			Ann_TurnAroundCost_kCur,
			Ann_TurnAround_ManHrs,
			CostPerManHour_Cur,
			TaAdjExRate
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,

			r.TrainId,
			r.SchedId,

			r.StreamId,
			r.Capacity_Pcnt,

			--CASE WHEN COUNT(CASE WHEN r.Capacity_Pcnt > 0.0 THEN 1 END) OVER (PARTITION BY tsq.FactorSetId, r.Refnum, r.CalDateKey, r.SchedId) <> COUNT(1) OVER (PARTITION BY tsq.FactorSetId, r.Refnum, r.CalDateKey, r.SchedId)
			--	OR ABS(SUM(r.Capacity_Pcnt) OVER (PARTITION BY tsq.FactorSetId, r.Refnum, r.CalDateKey, r.SchedId) - 100.0) > .1
			--	THEN
			--		1.0 / CONVERT(REAL, COUNT(1) OVER(PARTITION BY
			--			tsq.FactorSetId,
			--			r.Refnum,
			--			r.CalDateKey,
			--			r.SchedId), 1) * 100.0
			--	ELSE
			--		r.Capacity_Pcnt
			--	END						[CapacityCalc_Pcnt],

			CASE WHEN
					COUNT(CASE WHEN r.Capacity_Pcnt > 0.0 THEN 1 END) OVER(PARTITION BY tsq.FactorSetId, r.Refnum, r.CalDateKey, r.SchedId) = COUNT(r.TrainId) OVER(PARTITION BY tsq.FactorSetId, r.Refnum, r.CalDateKey, r.SchedId)
				AND SUM(r.Capacity_Pcnt) OVER (PARTITION BY tsq.FactorSetId, r.Refnum, r.CalDateKey, r.SchedId) = 100.0
				THEN
					r.Capacity_Pcnt
				ELSE
					100.0 / CONVERT(REAL, COUNT(1) OVER(PARTITION BY tsq.FactorSetId, r.Refnum, r.CalDateKey, r.SchedId), 1)
				END						[CapacityCalc_Pcnt],

			r.Interval_Mnths,
			r._Interval_Years,
			r.DownTime_Hrs,
			r.TurnAround_Date,
			r._TurnAroundInStudyYear_Bit,
			r.CurrencyRpt,
			r.TurnAroundCost_MCur,
			r.TurnAround_ManHrs,
			r._Ann_DownTime_Hrs,
			r._Ann_TurnAroundCost_kCur,
			r._Ann_TurnAround_ManHrs,
			r._CostPerManHour_Cur,

			[$(DbGlobal)].[dbo].[ExchangeRate](tsq.CurrencyRpt, 'USD',
				CASE WHEN YEAR(r.TurnAround_Date) < tsq.StudyYear - 12 THEN tsq.StudyYear - 12 ELSE YEAR(r.TurnAround_Date) END,
				tsq.StudyYear, tsq.CurrencyFcn)				[TaAdjExRate]

		FROM @fpl						tsq
		INNER JOIN fact.ReliabilityTA		r
			ON	r.Refnum = tsq.Refnum
			AND	r.CalDateKey = tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;