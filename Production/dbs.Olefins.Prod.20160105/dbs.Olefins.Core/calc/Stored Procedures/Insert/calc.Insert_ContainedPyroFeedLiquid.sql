﻿CREATE PROCEDURE [calc].[Insert_ContainedPyroFeedLiquid]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ContainedPyroFeedLiquid(FactorSetId, Refnum, CalDateKey, ContainedStreamId, Lt_LightQuantity_kMT, Lt_HeavyQuantity_kMT, Lt_TotalQuantity_kMT, Hv_LightQuantity_kMT, Hv_HeavyQuantity_kMT, Hv_TotalQuantity_kMT)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			d.StreamId															[ContainedStreamId],
			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Light.Quantity_kMT * Light.C5C6Weight_Fraction
				WHEN '-' THEN - Light.Quantity_kMT * Light.C5C6Weight_Fraction
			END)																[Lt_LightQuantity_kMT],

			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Light.Quantity_kMT * Light.HeavyWeight_Fraction
				WHEN '-' THEN - Light.Quantity_kMT * Light.HeavyWeight_Fraction
			END)																[Lt_HeavyQuantity_kMT],

			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Light.Quantity_kMT
				WHEN '-' THEN - Light.Quantity_kMT
			END)																[Lt_TotalQuantity_kMT],

			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Heavy.Quantity_kMT * Heavy.C5C6Weight_Fraction
				WHEN '-' THEN - Heavy.Quantity_kMT * Heavy.C5C6Weight_Fraction
			END)																[Hv_LightQuantity_kMT],
			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Heavy.Quantity_kMT * Heavy.HeavyWeight_Fraction
				WHEN '-' THEN - Heavy.Quantity_kMT * Heavy.HeavyWeight_Fraction
			END)																[Hv_HeavyQuantity_kMT],

			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Heavy.Quantity_kMT
				WHEN '-' THEN - Heavy.Quantity_kMT
			END)																[Hv_TotalQuantity_kMT]

		FROM @fpl												fpl
		INNER JOIN dim.Stream_Bridge							d
			ON	d.FactorSetId = fpl.FactorSetId
		INNER JOIN dim.Stream_Bridge							l
			ON	l.FactorSetId = fpl.FactorSetId
			AND	l.DescendantId = d.StreamId
			AND	l.StreamId = 'Liquid'
		LEFT OUTER JOIN fact.FeedStockLiquidAttributes			Light
			ON	Light.FactorSetId = fpl.FactorSetId
			AND	Light.Refnum = fpl.Refnum
			AND	Light.CalDateKey = fpl.Plant_QtrDateKey
			AND	CASE WHEN Light.StreamId = 'FeedLiqOther' THEN 'LiqLightOther' ELSE Light.StreamId END = d.DescendantId
		LEFT OUTER JOIN fact.FeedStockLiquidAttributes			Heavy
			ON	Heavy.FactorSetId = fpl.FactorSetId
			AND	Heavy.Refnum = fpl.Refnum
			AND	Heavy.CalDateKey = fpl.Plant_QtrDateKey
			AND	CASE WHEN Heavy.StreamId = 'FeedLiqOther' THEN 'LiqHeavyOther' ELSE Heavy.StreamId END = d.DescendantId
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			d.StreamId
		HAVING
			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Light.Quantity_kMT
				WHEN '-' THEN - Light.Quantity_kMT
			END) IS NOT NULL
			OR
			SUM(CASE d.DescendantOperator 
				WHEN '+' THEN	Heavy.Quantity_kMT
				WHEN '-' THEN - Heavy.Quantity_kMT
			END) IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END