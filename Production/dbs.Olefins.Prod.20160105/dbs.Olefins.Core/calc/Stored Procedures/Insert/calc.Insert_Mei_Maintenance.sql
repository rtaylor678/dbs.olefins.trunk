﻿CREATE PROCEDURE [calc].[Insert_Mei_Maintenance]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Mei(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MeiId, Indicator_Index, Indicator_PcntRv)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			'Maintenance',
			mei.Ann_MaintCostIndex_Cur,
			mei.Ann_MaintCostIndex_PcntRv
		FROM @fpl									fpl
		INNER JOIN calc.MeiMaintenanceCostIndex		mei
			ON	mei.FactorSetId = fpl.FactorSetId
			AND	mei.Refnum = fpl.Refnum
			AND	mei.CalDateKey = fpl.Plant_QtrDateKey
			AND	mei.CurrencyRpt = fpl.CurrencyRpt;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END