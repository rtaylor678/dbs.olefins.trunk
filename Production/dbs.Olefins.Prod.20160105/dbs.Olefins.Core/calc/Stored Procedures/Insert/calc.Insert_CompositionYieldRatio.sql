﻿CREATE PROCEDURE [calc].[Insert_CompositionYieldRatio]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.CompositionYieldRatio(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId,
			Component_kMT, Component_WtPcnt,
			Component_Extrap_kMT, Component_Extrap_WtPcnt,
			Component_Supp_kMT,
			Component_Div_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			y.SimModelId,
			y.OpCondId,
			y.RecycleId,
			b.ComponentId,

			SUM(y.Component_kMT),
			SUM(y.Component_WtPcnt),

			SUM(y.Component_Extrap_kMT),
			SUM(y.Component_Extrap_WtPcnt),

			SUM(y.Component_Supp_kMT),
			SUM(y._Component_Div_kMT)

		FROM @fpl												tsq
		INNER JOIN calc.CompositionYieldPlant					y
			ON	y.FactorSetId = tsq.FactorSetId
			AND	y.Refnum = tsq.Refnum
			AND	y.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN dim.Component_Bridge							b
			ON	b.FactorSetId = tsq.FactorSetId
			AND	b.DescendantId = y.ComponentId
		WHERE tsq.CalQtr = 4
		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			y.SimModelId,
			y.OpCondId,
			y.RecycleId,
			b.ComponentId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;