﻿CREATE PROCEDURE calc.Insert_StandardEnergy_2013_Supplemental
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'Plant',
			se.[StandardEnergyId],
			se.[ComponentId],
			SUM(se.[StandardEnergy_MBtuDay]) * 365.0
		FROM @fpl										fpl
		INNER JOIN [calc].[StandardEnergySupplemental]	se
			ON	se.[FactorSetId]	= fpl.[FactorSetId]
			AND	se.[Refnum]			= fpl.[Refnum]
			AND	se.[CalDateKey]		= fpl.[Plant_QtrDateKey]
		WHERE fpl.[FactorSet_AnnDateKey]	> 20130000
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			se.[StandardEnergyId],
			se.[ComponentId];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;