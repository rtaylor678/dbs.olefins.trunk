﻿CREATE PROCEDURE [calc].[Update_ReplacementValue]
(
	@Refnum		VARCHAR (18),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		UPDATE calc.ReplacementValue
		SET ReplacementLocation = rv.ReplacementValue * calc.MaxValue(lf.LocationFactor, 0.95)
		FROM	@fpl						fpl
		INNER JOIN calc.ReplacementValue	rv
			ON	rv.FactorSetId = fpl.FactorSetId
			AND	rv.Refnum = fpl.Refnum
			AND	rv.CalDateKey = fpl.Plant_QtrDateKey
			AND	rv.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN ante.LocationFactor		lf
			--ON	lf.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, lf.FactorSetId) = fpl.DataYear
			AND	lf.CountryId = fpl.CountryId
			AND	lf.StateName IS NULL
		WHERE fpl.CalQtr = 4;

		UPDATE calc.ReplacementValue
		SET ReplacementLocation = rv.ReplacementValue * calc.MaxValue(lf.LocationFactor, 0.95)
		FROM	@fpl						fpl
		INNER JOIN calc.ReplacementValue	rv
			ON	rv.FactorSetId = fpl.FactorSetId
			AND	rv.Refnum = fpl.Refnum
			AND	rv.CalDateKey = fpl.Plant_QtrDateKey
			AND	rv.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN ante.LocationFactor		lf
			--ON	lf.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, lf.FactorSetId) = fpl.DataYear
			AND	lf.CountryId = fpl.CountryId
			AND	lf.StateName = fpl.StateName
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;