﻿CREATE PROCEDURE [stgFact].[Delete_MetaFeedProd]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[MetaFeedProd]
	WHERE [Refnum] = @Refnum;

END;