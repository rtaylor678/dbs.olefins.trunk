﻿CREATE PROCEDURE [stgFact].[Delete_Compressors]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Compressors]
	WHERE [Refnum] = @Refnum;

END;