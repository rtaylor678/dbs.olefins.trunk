﻿CREATE PROCEDURE [stgFact].[Delete_PracRely]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracRely]
	WHERE [Refnum] = @Refnum;

END;