﻿CREATE PROCEDURE [stgFact].[Delete_Inventory]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Inventory]
	WHERE [Refnum] = @Refnum;

END;