﻿CREATE PROCEDURE [stgFact].[Delete_Composition]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Composition]
	WHERE [Refnum] = @Refnum;

END;