﻿CREATE PROCEDURE [stgFact].[Select_UploadTime]
(
	@Refnum					VARCHAR (25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT TOP 1
		[UploadTime]		= CONVERT(DATETIME, [t].[tsModified], 102)
	FROM
		[stgFact].[TSort]	[t]
	WHERE
		[t].[Refnum]		= @Refnum;

END;