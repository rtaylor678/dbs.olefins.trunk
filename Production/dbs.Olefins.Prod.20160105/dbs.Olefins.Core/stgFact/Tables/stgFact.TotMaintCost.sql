﻿CREATE TABLE [stgFact].[TotMaintCost] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [DirMaintMatl]      REAL               NULL,
    [DirMaintLabor]     REAL               NULL,
    [DirTAMatl]         REAL               NULL,
    [DirTALabor]        REAL               NULL,
    [AllocOHMaintMatl]  REAL               NULL,
    [AllocOHMaintLabor] REAL               NULL,
    [AllocOHTAMatl]     REAL               NULL,
    [AllocOHTALabor]    REAL               NULL,
    [TotalMaintMatl]    REAL               NULL,
    [TotalMaintLabor]   REAL               NULL,
    [TotalTAMatl]       REAL               NULL,
    [TotalTALabor]      REAL               NULL,
    [DirMaintTot]       REAL               NULL,
    [DirTATot]          REAL               NULL,
    [AllocOHMaintTot]   REAL               NULL,
    [AllocOHTATot]      REAL               NULL,
    [TotRoutMaint]      REAL               NULL,
    [TotTAMaint]        REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_TotMaintCost_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_TotMaintCost_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_TotMaintCost_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_stgFact_TotMaintCost_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_TotMaintCost] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_TotMaintCost_u]
	ON [stgFact].[TotMaintCost]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[TotMaintCost]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[TotMaintCost].Refnum		= INSERTED.Refnum;

END;