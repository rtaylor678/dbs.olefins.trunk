﻿CREATE TABLE [cons].[TSortSolomon] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [_StudyYear]     AS                 (CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0))) PERSISTED NOT NULL,
    [Consultant]     VARCHAR (18)       NULL,
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [RefDirectory]   VARCHAR (100)      NULL,
    [Comments]       VARCHAR (256)      NULL,
    [ContactCode]    VARCHAR (25)       NULL,
    [Active]         BIT                CONSTRAINT [DF_TSortSolomon_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_TSortSolomon_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_TSortSolomon_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_TSortSolomon_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_TSortSolomon_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_TSortSolomon] PRIMARY KEY CLUSTERED ([Refnum] ASC),
    CONSTRAINT [CL_TSortSolomon_Comments] CHECK ([Comments]<>''),
    CONSTRAINT [CL_TSortSolomon_ContactCode] CHECK ([ContactCode]<>''),
    CONSTRAINT [CL_TSortSolomon_FactorSetId] CHECK ([RefDirectory]<>''),
    CONSTRAINT [CL_TSortSolomon_RefDirectory] CHECK ([RefDirectory]<>''),
    CONSTRAINT [FK_TSortSolomon_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_TSortSolomon_StudyPeriods_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[StudyPeriods_LookUp] ([CalDateKey])
);


GO

CREATE TRIGGER [cons].[t_TSortSolomon_u]
	ON [cons].[TSortSolomon]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[TSortSolomon]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[TSortSolomon].Refnum		= INSERTED.Refnum;

END;