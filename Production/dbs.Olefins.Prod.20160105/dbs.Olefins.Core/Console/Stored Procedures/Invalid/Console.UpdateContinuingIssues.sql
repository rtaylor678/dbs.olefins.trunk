﻿CREATE PROCEDURE [Console].[UpdateContinuingIssues]

	@RefNum varchar(18),
	@Issue text

AS
BEGIN
if exists(select * from Val.Notes where Refnum = @RefNum)
	UPDATE Val.Notes set ContinuingIssues = @Issue where Refnum = @refnum
	ELSE
	INSERT INTO val.Notes (RefNum, ContinuingIssues, Updated) Values(@RefNum,@Issue, GETDATE())

END
