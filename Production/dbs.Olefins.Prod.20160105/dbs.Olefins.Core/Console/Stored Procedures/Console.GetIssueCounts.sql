﻿CREATE PROCEDURE [Console].[GetIssueCounts]
	@Refnum varchar(18),
	@Mode varchar(1)

AS
BEGIN

	SELECT COUNT(IssueId) as Issues FROM Val.Checklist 
	WHERE Refnum = dbo.FormatRefNum('20' + @Refnum,0)
	AND Completed = @Mode or @Mode='A'

END
