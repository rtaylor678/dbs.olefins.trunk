﻿CREATE PROCEDURE sim.SpslOut_Delete
(
	@QueueId			BIGINT			= NULL,
	@FactorSetId		VARCHAR(12)		= NULL,
	
	@Refnum				VARCHAR(25)		= NULL,
	@CalDateKey			INT				= NULL,
	@SimModelId			VARCHAR(12)		= NULL,
	@OpCondId			VARCHAR(12)		= NULL,
	
	@StreamId			VARCHAR(42)		= NULL,
	@StreamDescription	NVARCHAR(256)	= NULL,
	@RecycleId			INT				= NULL,
	@DataSetId			VARCHAR(6)		= NULL,
	@FieldId			VARCHAR(12)		= NULL
)
AS
	DELETE FROM sim.SpslOut
	WHERE	QueueId				= ISNULL(@QueueId,				QueueId)
		AND	FactorSetId			= ISNULL(@FactorSetId,			FactorSetId)
		AND	SimModelId			= ISNULL(@SimModelId,			SimModelId)
		AND	Refnum				= ISNULL(@Refnum,				Refnum)
		AND	(CalDateKey			= ISNULL(@CalDateKey,			CalDateKey)	OR @CalDateKey = 0)
		AND	StreamId			= ISNULL(@StreamId,				StreamId)
		AND	StreamDescription	= ISNULL(@StreamDescription,	StreamDescription)
		AND	OpCondId			= ISNULL(@OpCondId,				OpCondId)
		AND	RecycleId			= ISNULL(@RecycleId,			RecycleId)
		AND	DataSetId			= ISNULL(@DataSetId,			DataSetId)	
		AND	FieldId				= ISNULL(@FieldId,				FieldId);
