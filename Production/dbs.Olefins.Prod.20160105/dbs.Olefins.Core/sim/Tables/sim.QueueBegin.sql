﻿CREATE TABLE [sim].[QueueBegin]
(
	[QueueId]			INT					NOT	NULL	CONSTRAINT [FK_QueueBegin_QueueAdd]				REFERENCES [sim].[QueueAdd]([QueueId]),

	[Start]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_QueueBegin_Start]				DEFAULT(SYSDATETIMEOFFSET()),
	[ItemsQueued]		INT					NOT	NULL,

	[tsModified]		DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_QueueBegin_tsModified]			DEFAULT (SYSDATETIMEOFFSET()),
	[tsModifiedHost]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueBegin_tsModifiedHost]		DEFAULT (HOST_NAME()),
	[tsModifiedUser]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueBegin_tsModifiedUser]		DEFAULT (SUSER_SNAME()),
	[tsModifiedApp]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueBegin_tsModifiedApp]		DEFAULT (APP_NAME()),

	CONSTRAINT [PK_QueueBegin] PRIMARY KEY CLUSTERED([QueueId] ASC)
);