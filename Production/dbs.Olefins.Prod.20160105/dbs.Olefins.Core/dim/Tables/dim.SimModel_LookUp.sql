﻿CREATE TABLE [dim].[SimModel_LookUp] (
    [ModelId]        VARCHAR (12)       NOT NULL,
    [ModelName]      NVARCHAR (84)      NOT NULL,
    [ModelDetail]    NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_SimModel_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_SimModel_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_SimModel_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_SimModel_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SimModel_LookUp] PRIMARY KEY CLUSTERED ([ModelId] ASC),
    CONSTRAINT [CL_SimModel_LookUp_SimModelDetail] CHECK ([ModelDetail]<>''),
    CONSTRAINT [CL_SimModel_LookUp_SimModelId] CHECK ([ModelId]<>''),
    CONSTRAINT [CL_SimModel_LookUp_SimModelName] CHECK ([ModelName]<>''),
    CONSTRAINT [UK_SimModel_LookUp_SimModelDetail] UNIQUE NONCLUSTERED ([ModelDetail] ASC),
    CONSTRAINT [UK_SimModel_LookUp_SimModelName] UNIQUE NONCLUSTERED ([ModelName] ASC)
);


GO

CREATE TRIGGER [dim].[t_SimModel_LookUp_u]
	ON [dim].[SimModel_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[SimModel_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[SimModel_LookUp].[ModelId]		= INSERTED.[ModelId];
		
END;
