﻿CREATE TABLE [dim].[FeedSelPenalty_LookUp] (
    [PenaltyId]      VARCHAR (42)       NOT NULL,
    [PenaltyName]    NVARCHAR (84)      NOT NULL,
    [PenaltyDetail]  NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelPenalty_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FeedSelPenalty_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FeedSelPenalty_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelPenalty_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedSelPenalty_LookUp] PRIMARY KEY CLUSTERED ([PenaltyId] ASC),
    CONSTRAINT [CL_FeedSelPenalty_LookUp_PenaltyDetail] CHECK ([PenaltyDetail]<>''),
    CONSTRAINT [CL_FeedSelPenalty_LookUp_PenaltyId] CHECK ([PenaltyId]<>''),
    CONSTRAINT [CL_FeedSelPenalty_LookUp_PenaltyName] CHECK ([PenaltyName]<>''),
    CONSTRAINT [UK_FeedSelPenalty_LookUp_PenaltyDetail] UNIQUE NONCLUSTERED ([PenaltyDetail] ASC),
    CONSTRAINT [UK_FeedSelPenalty_LookUp_PenaltyName] UNIQUE NONCLUSTERED ([PenaltyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FeedSelPenalty_LookUp_u]
	ON [dim].[FeedSelPenalty_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FeedSelPenalty_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedSelPenalty_LookUp].[PenaltyId]	= INSERTED.[PenaltyId];
		
END;
