﻿CREATE TABLE [dim].[FactorSet_LookUp] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [FactorSetName]   NVARCHAR (84)      NOT NULL,
    [FactorSetDetail] NVARCHAR (256)     NOT NULL,
    [FactorSetYear]   SMALLINT           NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_FactorSet_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_FactorSet_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_FactorSet_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_FactorSet_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FactorSet_LookUp] PRIMARY KEY CLUSTERED ([FactorSetId] ASC),
    CONSTRAINT [CL_FactorSet_LookUp_FactorSetDetail] CHECK ([FactorSetDetail]<>''),
    CONSTRAINT [CL_FactorSet_LookUp_FactorSetId] CHECK ([FactorSetId]<>''),
    CONSTRAINT [CL_FactorSet_LookUp_FactorSetName] CHECK ([FactorSetName]<>''),
    CONSTRAINT [CV_FactorSet_LookUp_FactorSetYear] CHECK ([FactorSetYear]>(1980) AND len([FactorSetYear])=(4)),
    CONSTRAINT [UK_FactorSet_LookUp_FactorSetDetail] UNIQUE NONCLUSTERED ([FactorSetDetail] ASC),
    CONSTRAINT [UK_FactorSet_LookUp_FactorSetName] UNIQUE NONCLUSTERED ([FactorSetName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FactorSet_LookUp_u]
	ON [dim].[FactorSet_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FactorSet_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FactorSet_LookUp].[FactorSetId]		= INSERTED.[FactorSetId];
		
END;
