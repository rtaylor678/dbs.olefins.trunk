﻿
CREATE PROC [Ranking].[RankRefnumList] @Refnums [dbo].[RefnumList] READONLY, @RankVariable sysname, @ForceTiles tinyint,
	@NumTiles tinyint OUTPUT, @DataCount int OUTPUT, @Top2 real OUTPUT, @Tile1Break real OUTPUT, @Tile2Break real OUTPUT, @Tile3Break real OUTPUT, @Btm2 real OUTPUT
AS
SET NOCOUNT ON
DECLARE @HigherIsBetter bit, @MinRangeValue real, @MaxRangeValue real
SELECT @HigherIsBetter = CASE SortOrder WHEN 'DESC' THEN 1 ELSE 0 END, @MinRangeValue = RangeMinValue, @MaxRangeValue = RangeMaxValue
FROM Ranking.RankVariables WHERE RankVariable = @RankVariable

DECLARE @RankData Ranking.RankData
INSERT @RankData(Refnum, RankValue, PcntFactor)
SELECT Refnum, RankValue, PcntFactor
FROM [Ranking].[GetDataToRank](@Refnums, @RankVariable)

DECLARE @RankedData Ranking.RankedData
INSERT @RankedData(Refnum, Rank, Percentile, Tile, Value)
SELECT Refnum, Rank, Percentile, Tile, RankValue
FROM Ranking.RankAndTile(@RankData, @HigherIsBetter, @ForceTiles)

SELECT @NumTiles = NumTiles, @DataCount = DataCount, @Top2 = Top2, @Tile1Break = Tile1Break, @Tile2Break = Tile2Break, @Tile3Break = Tile3Break, @Btm2 = Btm2
FROM Ranking.GetSummary(@RankedData, @MinRangeValue, @MaxRangeValue)

SET NOCOUNT OFF
SELECT * FROM @RankedData ORDER BY Rank