﻿
CREATE FUNCTION etl.ConvFacilityHydroTypeID
(
	@TypeID	VARCHAR(42)
)
RETURNS VARCHAR(2)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(2) =
	CASE @TypeID
		WHEN 'Selective Saturation'	THEN 'SS'
		WHEN 'Both'					THEN 'B'
	END;
	
	RETURN @ResultVar

END