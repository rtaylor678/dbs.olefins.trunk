﻿
CREATE  FUNCTION [dbo].[CoLoc](@Refnum AS varchar(18)) 
RETURNS varchar(50)
AS
BEGIN 
	DECLARE @CoLoc varchar(50)
    	SELECT @CoLoc = CoLoc FROM TSort WHERE Refnum = @Refnum
    	RETURN @CoLoc
END


