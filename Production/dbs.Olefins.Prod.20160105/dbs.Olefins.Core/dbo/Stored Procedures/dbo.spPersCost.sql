﻿


CREATE              PROC [dbo].[spPersCost](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON
DECLARE @Currency as varchar(5)
SELECT @Currency = 'USD'

DELETE FROM dbo.PersCost WHERE Refnum = @Refnum
Insert into dbo.PersCost (Refnum
, Currency
, PersCost_CurrPerEDC
, PersCost_CurrPerHVC_MT)

SELECT Refnum = @Refnum
, Currency = @Currency
, PersCost_CurrPerEDC		= CASE WHEN d.kEdc>0			THEN TaAdjAnn_Amount_Cur / d.kEdc END
, PersCost_CurrPerHVC_MT	= CASE WHEN d.HvcProd_kMT>0	THEN TaAdjAnn_Amount_Cur / d.HvcProd_kMT END
FROM calc.PersCostAggregate p JOIN dbo.Divisors d on p.Refnum=d.Refnum 
WHERE p.CurrencyRpt = @Currency and p.FactorSetId = @FactorSetId and p.Refnum = @Refnum


SET NOCOUNT OFF

--exec spperscost '2011pch115'
--select * from PersCost 















