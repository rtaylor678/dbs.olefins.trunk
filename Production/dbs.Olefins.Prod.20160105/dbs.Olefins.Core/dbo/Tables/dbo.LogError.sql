﻿CREATE TABLE [dbo].[LogError] (
    [ErrorNumber]     INT                NOT NULL,
    [XActState]       SMALLINT           NOT NULL,
    [Refnum]          VARCHAR (55)       NULL,
    [ProcedureSchema] [sysname]          NOT NULL,
    [ProcedureName]   NVARCHAR (128)     NOT NULL,
    [ProcedureLine]   INT                NOT NULL,
    [ProcedureDesc]   NVARCHAR (4000)    NULL,
    [ErrorMessage]    NVARCHAR (4000)    NOT NULL,
    [ErrorSeverity]   INT                NOT NULL,
    [ErrorState]      INT                NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_LogError_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_LogError_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_LogError_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_LogError_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [ts]              ROWVERSION         NOT NULL,
    CONSTRAINT [PK_dbo_LogError] PRIMARY KEY CLUSTERED ([ts] DESC),
    CONSTRAINT [CL_dbo_LogError_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CL_LogError_ErrorMessage] CHECK ([ErrorMessage]<>''),
    CONSTRAINT [CL_LogError_ProcedureDesc] CHECK ([ProcedureDesc]<>''),
    CONSTRAINT [CL_LogError_ProcedureName] CHECK ([ProcedureName]<>''),
    CONSTRAINT [CL_LogError_ProcedureSchema] CHECK ([ProcedureSchema]<>'')
);


GO

CREATE TRIGGER [dbo].[t_LogError_u]
	ON [dbo].[LogError]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dbo].[LogError]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[dbo].[LogError].ts		= INSERTED.ts;
		
END;