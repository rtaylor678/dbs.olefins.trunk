﻿CREATE TABLE [dbo].[Message_LU] (
    [MessageId]   INT           IDENTITY (1, 1) NOT NULL,
    [Severity]    CHAR (1)      NULL,
    [MessageText] VARCHAR (255) NULL,
    [SysAdmin]    BIT           NOT NULL,
    [Consultant]  BIT           NOT NULL,
    [Pricing]     BIT           NOT NULL,
    [Audience4]   BIT           NOT NULL,
    [Audience5]   BIT           NOT NULL,
    [Audience6]   BIT           NOT NULL,
    [Audience7]   BIT           NOT NULL
);

