﻿CREATE TABLE [dbo].[PersCost] (
    [Refnum]                 VARCHAR (25)  NOT NULL,
    [Currency]               VARCHAR (5)   NOT NULL,
    [PersCost_CurrPerEDC]    REAL          NULL,
    [PersCost_CurrPerHVC_MT] REAL          NULL,
    [tsModified]             SMALLDATETIME DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_PersCost] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Currency] ASC) WITH (FILLFACTOR = 70)
);

