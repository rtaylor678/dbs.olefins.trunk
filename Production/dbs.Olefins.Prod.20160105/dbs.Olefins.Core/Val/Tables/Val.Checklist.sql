﻿CREATE TABLE [Val].[Checklist] (
    [Refnum]         CHAR (25)          NOT NULL,
    [IssueId]        CHAR (8)           NOT NULL,
    [IssueTitle]     CHAR (50)          NOT NULL,
    [IssueText]      TEXT               NULL,
    [PostedBy]       CHAR (3)           NOT NULL,
    [PostedTime]     DATETIME           NOT NULL,
    [Completed]      CHAR (1)           NOT NULL,
    [SetBy]          CHAR (3)           NULL,
    [SetTime]        DATETIME           NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Checklist_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Checklist_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Checklist_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Checklist_tsModifiedApp] DEFAULT (app_name()) NOT NULL,

	CONSTRAINT [PK_ValCheckList]	PRIMARY KEY CLUSTERED ([Refnum] ASC, [IssueId] ASC)
);
GO

CREATE TRIGGER [Val].[t_Checklist_u]
ON [Val].[Checklist]
AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [Val].[Checklist]
	SET	tsModified		= sysdatetimeoffset(),
		tsModifiedHost	= host_name(),
		tsModifiedUser	= suser_sname(),
		tsModifiedApp	= app_name()
	FROM INSERTED
	WHERE	[Val].[Checklist].[Refnum]			= INSERTED.[Refnum]
		AND	[Val].[Checklist].[IssueId]			= INSERTED.[IssueId];
END;
