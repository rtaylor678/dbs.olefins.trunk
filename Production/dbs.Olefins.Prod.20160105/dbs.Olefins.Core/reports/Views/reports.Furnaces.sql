﻿CREATE VIEW [reports].[Furnaces] AS
SELECT
	[GroupId]				= [r].[GroupId],
	[FurnFeed]				= CASE WHEN COALESCE([x].[MappedFurnFeed], [f].[FurnFeed]) IS NULL THEN 'All' ELSE 
								COALESCE([x].[MappedFurnFeed], [f].[FurnFeed])
								END,

	[DTDecoke]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[DTDecoke],			[f].[FeedCapMTD]),
	[DTTLEClean]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[DTTLEClean],			[f].[FeedCapMTD]),
	[DTMinor]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[DTMinor],			[f].[FeedCapMTD]),
	[DTMajor]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[DTMajor],			[f].[FeedCapMTD]),
	[STDT]					= [$(DbGlobal)].[dbo].[WtAvg]([f].[STDT],				[f].[FeedCapMTD]),
	[DTStandby]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[DTStandby],			[f].[FeedCapMTD]),
	[TotTimeOff]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[TotTimeOff],			[f].[FeedCapMTD]),
	
	[TADownDays]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[TADownDays],			[f].[FeedCapMTD]),
	[OthDownDays]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[OthDownDays],		[f].[FeedCapMTD]),
	[FurnAvail]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[FurnAvail],			[f].[FeedCapMTD]),
	[AdjFurnOnstream]		= [$(DbGlobal)].[dbo].[WtAvg]([f].[AdjFurnOnstream],	[f].[FeedCapMTD]),
	[FeedQtyKMTA]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[FeedQtyKMTA],		[f].[FeedCapMTD]),
	[FeedCapMTD]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[FeedCapMTD],			[f].[FeedCapMTD]),
	[OperDays]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[OperDays],			[f].[FeedCapMTD]),
	[ActCap]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[ActCap],				[f].[FeedCapMTD]),
	[AdjFurnUtil]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[AdjFurnUtil],		[f].[FeedCapMTD]),
	[FurnSlowD]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[FurnSlowD],			[f].[FeedCapMTD]),
	[FuelCons]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[FuelCons],			[f].[FeedCapMTD]),
	[FuelConsRatio]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[FuelConsRatio],		[f].[FeedCapMTD]),
	[StackOxy]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[StackOxy],			[f].[FeedCapMTD]),
	[ArchDraft]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[ArchDraft],			[f].[FeedCapMTD]),
	[StackTemp]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[StackTemp],			[f].[FeedCapMTD]),
	[CrossTemp]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[CrossTemp],			[f].[FeedCapMTD]),
	[RetubeInterval]		= [$(DbGlobal)].[dbo].[WtAvg]([f].[RetubeInterval],		[f].[FeedCapMTD]),
	[RetubeCostMatl]		= [$(DbGlobal)].[dbo].[WtAvg]([f].[RetubeCostMatl],		[f].[FeedCapMTD]),
	[RetubeCostLabor]		= [$(DbGlobal)].[dbo].[WtAvg]([f].[RetubeCostLabor],	[f].[FeedCapMTD]),
	[OtherMajorCost]		= [$(DbGlobal)].[dbo].[WtAvg]([f].[OtherMajorCost],		[f].[FeedCapMTD]),
	[TotCost]				= [$(DbGlobal)].[dbo].[WtAvg]([f].[TotCost],			[f].[FeedCapMTD]),
	[_Ann_MaintRetubeTot_Cur]			= [$(DbGlobal)].[dbo].[WtAvg]([f].[_Ann_MaintRetubeTot_Cur],			[f].[FeedCapMTD]),
	[_InflAdjAnn_MaintRetubeTot_Cur]	= [$(DbGlobal)].[dbo].[WtAvg]([f].[_InflAdjAnn_MaintRetubeTot_Cur],		[f].[FeedCapMTD]),

	[NumFurnacesPerPlant]	= CONVERT(REAL, COUNT(1)) / CONVERT(REAL, (SELECT COUNT(DISTINCT([g].[Refnum])) FROM [reports].[GroupPlants] [g] WHERE [g].[GroupId] = [r].[GroupId]))
FROM
	[reports].[GroupPlants]		[r]
INNER JOIN
	[dbo].[Furnaces]			[f]
		ON	[f].[Refnum]		= [r].[Refnum]
LEFT OUTER JOIN (VALUES
	('Butane',			'Butane and Other Light'),
	('Other Light',		'Butane and Other Light'),
	('Gasoil<390',		'Other Heavy Liquid'),
	('Gasoil>390',		'Other Heavy Liquid'),
	('Other Liquid',	'Other Heavy Liquid')
	) [x]([FurnFeed], [MappedFurnFeed])
		ON	[x].[FurnFeed]		= [f].[FurnFeed]
GROUP BY [r].[GroupId],
	ROLLUP(COALESCE([x].[MappedFurnFeed], [f].[FurnFeed]));
GO