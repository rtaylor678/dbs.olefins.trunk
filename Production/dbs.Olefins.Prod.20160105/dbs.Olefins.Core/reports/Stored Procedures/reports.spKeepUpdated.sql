﻿CREATE PROC reports.spKeepUpdated (@GroupId varchar(25)) as
DECLARE @Name varchar(40), @qry varchar(200)

DECLARE cGroupID CURSOR
READ_ONLY
FOR 
	SELECT distinct p.GroupId
	from reports.GroupPlants p JOIN reports.ReportGroups r on r.GroupId=p.GroupId
	JOIN reports.GENSUM plt on plt.GroupId=p.Refnum
	JOIN reports.GENSUM grp on grp.GroupId=p.GroupId
	and plt.tsModified > grp.tsModified
	and plt.GroupId = @GroupId
	and KeepUpdated = 'y'
OPEN cGroupID

FETCH NEXT FROM cGroupID INTO @name
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
		SELECT @qry = 'Exec reports.spALL ''' + RTRIM(@Name) + ''''
		print (@qry)
		exec(@qry)
	END
	FETCH NEXT FROM cGroupID INTO @name
END

CLOSE cGroupID
DEALLOCATE cGroupID




