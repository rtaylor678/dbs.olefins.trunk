﻿CREATE PROC [reports].[spInsertKeepUpdated] (@GroupId varchar(25)) as
DECLARE @RecordCount int

SELECT  @RecordCount = COUNT(*)
	from reports.GroupPlants p JOIN reports.ReportGroups r on r.GroupId=p.GroupId
	JOIN reports.GENSUM plt on plt.GroupId=p.Refnum
	JOIN reports.GENSUM grp on grp.GroupId=p.GroupId
	LEFT OUTER JOIN dbo.Queue q on q.Refnum=p.Refnum and q.Program = 'reports.KeepUpdated' -- Make sure it is NOT there
	WHERE plt.tsModified > grp.tsModified
	and KeepUpdated = 'y' and q.Refnum IS NULL
	AND plt.GroupId = @GroupId

IF @RecordCount > 0
BEGIN
	INSERT INTO dbo.Queue (Refnum, Program, Consultant) SELECT @GroupId, 'reports.KeepUpdated', 'DBB'
END;


