﻿

CREATE              PROC [reports].[spYield](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint, @Currency as varchar(5)
SELECT @Currency = 'USD'
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DECLARE @FreshPyroFeed real, @SuppTot real, @PlantFeed real, @Prod real, @Loss real, @ProdLoss real, @FeedProdLoss real

SELECT 
  @FreshPyroFeed = SUM(CASE WHEN q.StreamId = 'FreshPyroFeed' THEN q.Tot_kMT ELSE 0 END) / @RefCount
, @SuppTot = SUM(CASE WHEN q.StreamId = 'SuppTot' THEN q.Tot_kMT ELSE 0 END) / @RefCount
, @PlantFeed = SUM(CASE WHEN q.StreamId = 'PlantFeed' THEN q.Tot_kMT ELSE 0 END) / @RefCount
, @Prod = -SUM(CASE WHEN q.StreamId = 'Prod' THEN q.Tot_kMT ELSE 0 END) / @RefCount
, @Loss = -SUM(CASE WHEN q.StreamId = 'Loss' THEN q.Tot_kMT ELSE 0 END) / @RefCount
, @ProdLoss = -SUM(CASE WHEN q.StreamId = 'ProdLoss' THEN q.Tot_kMT ELSE 0 END) / @RefCount
, @FeedProdLoss = SUM(CASE WHEN q.StreamId = 'FeedProdLoss' THEN q.Tot_kMT ELSE 0 END) / @RefCount
FROM @MyGroup r JOIN fact.QuantityAggregate q on q.Refnum=r.Refnum and @RefCount>0
AND q.StreamId in ('FreshPyroFeed','SuppTot','PlantFeed','Prod','Loss','ProdLoss','FeedProdLoss')


DELETE FROM reports.Yield WHERE GroupId = @GroupId
Insert into reports.Yield (GroupId
, DataType
, Ethane
, EPMix
, Propane
, LPG
, Butane
, FeedLtOther
, NaphthaLt
, NaphthaFr
, NaphthaHv
, Condensate
, HeavyNGL
, Raffinate
, Diesel
, GasOilHv
, GasOilHt
, FeedLiqOther
, FreshPyroFeed
, ConcEthylene
, ConcPropylene
, ConcButadiene
, ConcBenzene
, ROGHydrogen
, ROGMethane
, ROGEthane
, ROGEthylene
, ROGPropylene
, ROGPropane
, ROGC4Plus
, ROGInerts
, DilHydrogen
, DilMethane
, DilEthane
, DilEthylene
, DilPropane
, DilPropylene
, DilButane
, DilButylene
, DilButadiene
, DilBenzene
, DilMogas
, SuppWashOil
, SuppGasOil
, SuppOther
, SuppTot
, PlantFeed
, Hydrogen
, Methane
, Acetylene
, EthylenePG
, EthyleneCG
, PropylenePG
, PropyleneCG
, PropyleneRG
, PropaneC3Resid
, Butadiene
, IsoButylene
, C4Oth
, Benzene
, PyroGasoline
, PyroGasOil
, PyroFuelOil
, AcidGas
, PPFC
, ProdOther
, Prod
, LossFlareVent
, LossOther
, LossMeasure
, Loss
, ProdLoss
, FeedProdLoss
, FeedLiqOther1
, FeedLiqOther2
, FeedLiqOther3
, ProdOther1
, ProdOther2
, RecSuppEthane
, RecSuppPropane
, RecSuppButane
, Recycle
)

SELECT GroupId = @GroupId
, y.DataType
, Ethane = [$(DbGlobal)].dbo.WtAvg(Ethane, 1.0)
, EPMix = [$(DbGlobal)].dbo.WtAvg(EPMix, 1.0)
, Propane = [$(DbGlobal)].dbo.WtAvg(Propane, 1.0)
, LPG = [$(DbGlobal)].dbo.WtAvg(LPG, 1.0)
, Butane = [$(DbGlobal)].dbo.WtAvg(Butane, 1.0)
, FeedLtOther = [$(DbGlobal)].dbo.WtAvg(FeedLtOther, 1.0)
, NaphthaLt = [$(DbGlobal)].dbo.WtAvg(NaphthaLt, 1.0)
, NaphthaFr = [$(DbGlobal)].dbo.WtAvg(NaphthaFr, 1.0)
, NaphthaHv = [$(DbGlobal)].dbo.WtAvg(NaphthaHv, 1.0)
, Condensate = [$(DbGlobal)].dbo.WtAvg(Condensate, 1.0)
, HeavyNGL = [$(DbGlobal)].dbo.WtAvg(HeavyNGL, 1.0)
, Raffinate = [$(DbGlobal)].dbo.WtAvg(Raffinate, 1.0)
, Diesel = [$(DbGlobal)].dbo.WtAvg(Diesel, 1.0)
, GasOilHv = [$(DbGlobal)].dbo.WtAvg(GasOilHv, 1.0)
, GasOilHt = [$(DbGlobal)].dbo.WtAvg(GasOilHt, 1.0)
, FeedLiqOther = [$(DbGlobal)].dbo.WtAvg(FeedLiqOther, 1.0)
, FreshPyroFeed = [$(DbGlobal)].dbo.WtAvg(FreshPyroFeed, 1.0)
, ConcEthylene = [$(DbGlobal)].dbo.WtAvg(ConcEthylene, 1.0)
, ConcPropylene = [$(DbGlobal)].dbo.WtAvg(ConcPropylene, 1.0)
, ConcButadiene = [$(DbGlobal)].dbo.WtAvg(ConcButadiene, 1.0)
, ConcBenzene = [$(DbGlobal)].dbo.WtAvg(ConcBenzene, 1.0)
, ROGHydrogen = [$(DbGlobal)].dbo.WtAvg(ROGHydrogen, 1.0)
, ROGMethane = [$(DbGlobal)].dbo.WtAvg(ROGMethane, 1.0)
, ROGEthane = [$(DbGlobal)].dbo.WtAvg(ROGEthane, 1.0)
, ROGEthylene = [$(DbGlobal)].dbo.WtAvg(ROGEthylene, 1.0)
, ROGPropylene = [$(DbGlobal)].dbo.WtAvg(ROGPropylene, 1.0)
, ROGPropane = [$(DbGlobal)].dbo.WtAvg(ROGPropane, 1.0)
, ROGC4Plus = [$(DbGlobal)].dbo.WtAvg(ROGC4Plus, 1.0)
, ROGInerts = [$(DbGlobal)].dbo.WtAvg(ROGInerts, 1.0)
, DilHydrogen = [$(DbGlobal)].dbo.WtAvg(DilHydrogen, 1.0)
, DilMethane = [$(DbGlobal)].dbo.WtAvg(DilMethane, 1.0)
, DilEthane = [$(DbGlobal)].dbo.WtAvg(DilEthane, 1.0)
, DilEthylene = [$(DbGlobal)].dbo.WtAvg(DilEthylene, 1.0)
, DilPropane = [$(DbGlobal)].dbo.WtAvg(DilPropane, 1.0)
, DilPropylene = [$(DbGlobal)].dbo.WtAvg(DilPropylene, 1.0)
, DilButane = [$(DbGlobal)].dbo.WtAvg(DilButane, 1.0)
, DilButylene = [$(DbGlobal)].dbo.WtAvg(DilButylene, 1.0)
, DilButadiene = [$(DbGlobal)].dbo.WtAvg(DilButadiene, 1.0)
, DilBenzene = [$(DbGlobal)].dbo.WtAvg(DilBenzene, 1.0)
, DilMogas = [$(DbGlobal)].dbo.WtAvg(DilMogas, 1.0)
, SuppWashOil = [$(DbGlobal)].dbo.WtAvg(SuppWashOil, 1.0)
, SuppGasOil = [$(DbGlobal)].dbo.WtAvg(SuppGasOil, 1.0)
, SuppOther = [$(DbGlobal)].dbo.WtAvg(SuppOther, 1.0)
, SuppTot = [$(DbGlobal)].dbo.WtAvg(SuppTot, 1.0)
, PlantFeed = [$(DbGlobal)].dbo.WtAvg(PlantFeed, 1.0)
, Hydrogen = [$(DbGlobal)].dbo.WtAvg(Hydrogen, 1.0)
, Methane = [$(DbGlobal)].dbo.WtAvg(Methane, 1.0)
, Acetylene = [$(DbGlobal)].dbo.WtAvg(Acetylene, 1.0)
, EthylenePG = [$(DbGlobal)].dbo.WtAvg(EthylenePG, 1.0)
, EthyleneCG = [$(DbGlobal)].dbo.WtAvg(EthyleneCG, 1.0)
, PropylenePG = [$(DbGlobal)].dbo.WtAvg(PropylenePG, 1.0)
, PropyleneCG = [$(DbGlobal)].dbo.WtAvg(PropyleneCG, 1.0)
, PropyleneRG = [$(DbGlobal)].dbo.WtAvg(PropyleneRG, 1.0)
, PropaneC3Resid = [$(DbGlobal)].dbo.WtAvg(PropaneC3Resid, 1.0)
, Butadiene = [$(DbGlobal)].dbo.WtAvg(Butadiene, 1.0)
, IsoButylene = [$(DbGlobal)].dbo.WtAvg(IsoButylene, 1.0)
, C4Oth = [$(DbGlobal)].dbo.WtAvg(C4Oth, 1.0)
, Benzene = [$(DbGlobal)].dbo.WtAvg(Benzene, 1.0)
, PyroGasoline = [$(DbGlobal)].dbo.WtAvg(PyroGasoline, 1.0)
, PyroGasOil = [$(DbGlobal)].dbo.WtAvg(PyroGasOil, 1.0)
, PyroFuelOil = [$(DbGlobal)].dbo.WtAvg(PyroFuelOil, 1.0)
, AcidGas = [$(DbGlobal)].dbo.WtAvg(AcidGas, 1.0)
, PPFC = [$(DbGlobal)].dbo.WtAvg(PPFC, 1.0)
, ProdOther = [$(DbGlobal)].dbo.WtAvg(ProdOther, 1.0)
, Prod = [$(DbGlobal)].dbo.WtAvg(Prod, 1.0)
, LossFlareVent = [$(DbGlobal)].dbo.WtAvg(LossFlareVent, 1.0)
, LossOther = [$(DbGlobal)].dbo.WtAvg(LossOther, 1.0)
, LossMeasure = [$(DbGlobal)].dbo.WtAvg(LossMeasure, 1.0)
, Loss = [$(DbGlobal)].dbo.WtAvg(Loss, 1.0)
, ProdLoss = [$(DbGlobal)].dbo.WtAvg(ProdLoss, 1.0)
, FeedProdLoss = [$(DbGlobal)].dbo.WtAvg(FeedProdLoss, 1.0)
, FeedLiqOther1 = [$(DbGlobal)].dbo.WtAvg(FeedLiqOther1, 1.0)
, FeedLiqOther2 = [$(DbGlobal)].dbo.WtAvg(FeedLiqOther2, 1.0)
, FeedLiqOther3 = [$(DbGlobal)].dbo.WtAvg(FeedLiqOther3, 1.0)
, ProdOther1 = [$(DbGlobal)].dbo.WtAvg(ProdOther1, 1.0)
, ProdOther2 = [$(DbGlobal)].dbo.WtAvg(ProdOther2, 1.0)
, RecSuppEthane = [$(DbGlobal)].dbo.WtAvg(RecSuppEthane, 1.0)
, RecSuppPropane = [$(DbGlobal)].dbo.WtAvg(RecSuppPropane, 1.0)
, RecSuppButane = [$(DbGlobal)].dbo.WtAvg(RecSuppButane, 1.0)
, Recycle = [$(DbGlobal)].dbo.WtAvg(Recycle, 1.0)

FROM @MyGroup r JOIN dbo.Yield y on y.Refnum=r.Refnum
WHERE y.[DataType] <> '$PerMT'
GROUP BY
y.DataType



Insert into reports.Yield (GroupId
, DataType
, Ethane
, EPMix
, Propane
, LPG
, Butane
, FeedLtOther
, NaphthaLt
, NaphthaFr
, NaphthaHv
, Condensate
, HeavyNGL
, Raffinate
, Diesel
, GasOilHv
, GasOilHt
, FeedLiqOther
, FreshPyroFeed
, ConcEthylene
, ConcPropylene
, ConcButadiene
, ConcBenzene
, ROGHydrogen
, ROGMethane
, ROGEthane
, ROGEthylene
, ROGPropylene
, ROGPropane
, ROGC4Plus
, ROGInerts
, DilHydrogen
, DilMethane
, DilEthane
, DilEthylene
, DilPropane
, DilPropylene
, DilButane
, DilButylene
, DilButadiene
, DilBenzene
, DilMogas
, SuppWashOil
, SuppGasOil
, SuppOther
, SuppTot
, PlantFeed
, Hydrogen
, Methane
, Acetylene
, EthylenePG
, EthyleneCG
, PropylenePG
, PropyleneCG
, PropyleneRG
, PropaneC3Resid
, Butadiene
, IsoButylene
, C4Oth
, Benzene
, PyroGasoline
, PyroGasOil
, PyroFuelOil
, AcidGas
, PPFC
, ProdOther
, Prod
, LossFlareVent
, LossOther
, LossMeasure
, Loss
, ProdLoss
, FeedProdLoss
, FeedLiqOther1
, FeedLiqOther2
, FeedLiqOther3
, ProdOther1
, ProdOther2
, RecSuppEthane
, RecSuppPropane
, RecSuppButane
, Recycle
)

SELECT GroupId = @GroupId
, v.DataType
, Ethane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Ethane], q.[Ethane]), 0.0)
, EPMix = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[EPMix], q.[EPMix]), 0.0)
, Propane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Propane], q.[Propane]), 0.0)
, LPG = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[LPG], q.[LPG]), 0.0)
, Butane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Butane], q.[Butane]), 0.0)
, FeedLtOther = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[FeedLtOther], q.[FeedLtOther]), 0.0)
, NaphthaLt = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[NaphthaLt], q.[NaphthaLt]), 0.0)
, NaphthaFr = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[NaphthaFr], q.[NaphthaFr]), 0.0)
, NaphthaHv = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[NaphthaHv], q.[NaphthaHv]), 0.0)
, Condensate = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Condensate], q.[Condensate]), 0.0)
, HeavyNGL = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[HeavyNGL], q.[HeavyNGL]), 0.0)
, Raffinate = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Raffinate], q.[Raffinate]), 0.0)
, Diesel = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Diesel], q.[Diesel]), 0.0)
, GasOilHv = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[GasOilHv], q.[GasOilHv]), 0.0)
, GasOilHt = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[GasOilHt], q.[GasOilHt]), 0.0)
, FeedLiqOther = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[FeedLiqOther], q.[FeedLiqOther]), 0.0)
, FreshPyroFeed = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[FreshPyroFeed], q.[FreshPyroFeed]), 0.0)
, ConcEthylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ConcEthylene], q.[ConcEthylene]), 0.0)
, ConcPropylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ConcPropylene], q.[ConcPropylene]), 0.0)
, ConcButadiene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ConcButadiene], q.[ConcButadiene]), 0.0)
, ConcBenzene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ConcBenzene], q.[ConcBenzene]), 0.0)
, ROGHydrogen = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGHydrogen], q.[ROGHydrogen]), 0.0)
, ROGMethane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGMethane], q.[ROGMethane]), 0.0)
, ROGEthane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGEthane], q.[ROGEthane]), 0.0)
, ROGEthylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGEthylene], q.[ROGEthylene]), 0.0)
, ROGPropylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGPropylene], q.[ROGPropylene]), 0.0)
, ROGPropane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGPropane], q.[ROGPropane]), 0.0)
, ROGC4Plus = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGC4Plus], q.[ROGC4Plus]), 0.0)
, ROGInerts = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ROGInerts], q.[ROGInerts]), 0.0)
, DilHydrogen = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilHydrogen], q.[DilHydrogen]), 0.0)
, DilMethane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilMethane], q.[DilMethane]), 0.0)
, DilEthane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilEthane], q.[DilEthane]), 0.0)
, DilEthylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilEthylene], q.[DilEthylene]), 0.0)
, DilPropane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilPropane], q.[DilPropane]), 0.0)
, DilPropylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilPropylene], q.[DilPropylene]), 0.0)
, DilButane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilButane], q.[DilButane]), 0.0)
, DilButylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilButylene], q.[DilButylene]), 0.0)
, DilButadiene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilButadiene], q.[DilButadiene]), 0.0)
, DilBenzene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilBenzene], q.[DilBenzene]), 0.0)
, DilMogas = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[DilMogas], q.[DilMogas]), 0.0)
, SuppWashOil = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[SuppWashOil], q.[SuppWashOil]), 0.0)
, SuppGasOil = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[SuppGasOil], q.[SuppGasOil]), 0.0)
, SuppOther = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[SuppOther], q.[SuppOther]), 0.0)
, SuppTot = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[SuppTot], q.[SuppTot]), 0.0)
, PlantFeed = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PlantFeed], q.[PlantFeed]), 0.0)
, Hydrogen = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Hydrogen], q.[Hydrogen]), 0.0)
, Methane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Methane], q.[Methane]), 0.0)
, Acetylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Acetylene], q.[Acetylene]), 0.0)
, EthylenePG = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[EthylenePG], q.[EthylenePG]), 0.0)
, EthyleneCG = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[EthyleneCG], q.[EthyleneCG]), 0.0)
, PropylenePG = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PropylenePG], q.[PropylenePG]), 0.0)
, PropyleneCG = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PropyleneCG], q.[PropyleneCG]), 0.0)
, PropyleneRG = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PropyleneRG], q.[PropyleneRG]), 0.0)
, PropaneC3Resid = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PropaneC3Resid], q.[PropaneC3Resid]), 0.0)
, Butadiene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Butadiene], q.[Butadiene]), 0.0)
, IsoButylene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[IsoButylene], q.[IsoButylene]), 0.0)
, C4Oth = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[C4Oth], q.[C4Oth]), 0.0)
, Benzene = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Benzene], q.[Benzene]), 0.0)
, PyroGasoline = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PyroGasoline], q.[PyroGasoline]), 0.0)
, PyroGasOil = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PyroGasOil], q.[PyroGasOil]), 0.0)
, PyroFuelOil = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PyroFuelOil], q.[PyroFuelOil]), 0.0)
, AcidGas = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[AcidGas], q.[AcidGas]), 0.0)
, PPFC = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[PPFC], q.[PPFC]), 0.0)
, ProdOther = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ProdOther], q.[ProdOther]), 0.0)
, Prod = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Prod], q.[Prod]), 0.0)
, LossFlareVent = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[LossFlareVent], q.[LossFlareVent]), 0.0)
, LossOther = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[LossOther], q.[LossOther]), 0.0)
, LossMeasure = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[LossMeasure], q.[LossMeasure]), 0.0)
, Loss = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Loss], q.[Loss]), 0.0)
, ProdLoss = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ProdLoss], q.[ProdLoss]), 0.0)
, FeedProdLoss = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[FeedProdLoss], q.[ProdLoss]), 0.0)
, FeedLiqOther1 = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[FeedLiqOther1], q.[FeedLiqOther1]), 0.0)
, FeedLiqOther2 = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[FeedLiqOther2], q.[FeedLiqOther2]), 0.0)
, FeedLiqOther3 = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[FeedLiqOther3], q.[FeedLiqOther3]), 0.0)
, ProdOther1 = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ProdOther1], q.[ProdOther1]), 0.0)
, ProdOther2 = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[ProdOther2], q.[ProdOther2]), 0.0)

, RecSuppEthane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[RecSuppEthane], q.[RecSuppEthane]), 0.0)
, RecSuppPropane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[RecSuppPropane], q.[RecSuppPropane]), 0.0)
, RecSuppButane = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[RecSuppButane], q.[RecSuppButane]), 0.0)
, Recycle = COALESCE([$(DbGlobal)].[dbo].[WtAvg](v.[Recycle], q.[Recycle]), 0.0)

FROM @MyGroup				r
INNER JOIN [dbo].[Yield]	v 
	ON	v.[Refnum] = r.[Refnum]
	AND v.[DataType] = '$PerMT'
INNER JOIN [dbo].[Yield]	q 
	ON	q.[Refnum] = r.[Refnum]
	AND q.[DataType] = 'kMT'
GROUP BY
	v.[DataType];

SET NOCOUNT OFF