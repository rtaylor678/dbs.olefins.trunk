﻿DECLARE @Refnum VARCHAR(25) = '';

INSERT INTO [cons].[RefListLu]
(
	[ListID],
	[ListName],
	[ListDetail],
	[BaseList_Bit],
	[SortKey],
	[Operator],
	[Parent],
	[Hierarchy]
)
VALUES(
	@Refnum,
	@Refnum,
	@Refnum,
	0,
	0,
	'~',
	@Refnum,
	'/'
);

INSERT INTO [cons].[RefList]
(
	[ListID],
	[Refnum],
	[UserGroup]
)
VALUES(
	@Refnum,
	@Refnum,
	0
	);