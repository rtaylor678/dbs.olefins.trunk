﻿CREATE TABLE [xls].[StandardEnergy_HydroPur] (
    [Refnum]                 VARCHAR (12)       NOT NULL,
    [H2Pur_kScfYear]         FLOAT (53)         NOT NULL,
    [H2Pur_kScfDay]          FLOAT (53)         NOT NULL,
    [StandardEnergy_MBtuDay] FLOAT (53)         NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_HydroPur_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_HydroPur] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_StandardEnergy_HydroPur_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_StandardEnergy_HydroPur_H2Pur_kScfDay_MinIncl_0.0] CHECK ([H2Pur_kScfDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_HydroPur_H2Pur_kScfYear_MinIncl_0.0] CHECK ([H2Pur_kScfYear]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_HydroPur_StandardEnergy_MBtuDay_MinIncl_0.0] CHECK ([StandardEnergy_MBtuDay]>=(0.0))
);


GO

CREATE TRIGGER [xls].[t_StandardEnergy_HydroPur_u]
	ON [xls].[StandardEnergy_HydroPur]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StandardEnergy_HydroPur]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StandardEnergy_HydroPur].[Refnum]	= INSERTED.[Refnum];

END;