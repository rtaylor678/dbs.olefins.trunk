﻿CREATE TABLE [dbo].[Capacity] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [EthylCapKMTA]     REAL           NULL,
    [EthylCapMTD]      REAL           NULL,
    [EthylMaxCapMTD]   REAL           NULL,
    [PropylCapKMTA]    REAL           NULL,
    [OlefinsCapMTD]    REAL           NULL,
    [OlefinsMaxCapMTD] REAL           NULL,
    [FurnaceCapKMTA]   REAL           NULL,
    [RecycleCapKMTA]   REAL           NULL,
    [SvcFactor]        REAL           NULL,
    [ExpansionPcnt]    REAL           NULL,
    [ExpansionDate]    DATETIME       NULL,
    [CapAllow]         CHAR (1)       NULL,
    [CapLossPcnt]      REAL           NULL,
    [PlantStartupDate] SMALLINT       NULL,
    CONSTRAINT [PK___1__19] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

