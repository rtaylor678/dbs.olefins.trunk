﻿CREATE TABLE [dbo].[TAAdj] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [AdjFactor]      REAL           NULL,
    [AnnOCCHrs]      REAL           NULL,
    [AnnMPSHrs]      REAL           NULL,
    [AnnContOCCHrs]  REAL           NULL,
    [AnnContMPSHrs]  REAL           NULL,
    [AvgDuration]    REAL           NULL,
    [AvgInterval]    REAL           NULL,
    [AnnAvgDuration] REAL           NULL,
    [TACycleCost]    REAL           NULL,
    [TACycleMH]      REAL           NULL,
    [TAAnnMH]        REAL           NULL,
    [Adj_Exp_Retube] REAL           NULL,
    CONSTRAINT [PK___TAAdj] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

