﻿CREATE TABLE [dbo].[DD_Lookups] (
    [LookupID]    INT           IDENTITY (1, 1) NOT NULL,
    [ObjectName]  VARCHAR (30)  NOT NULL,
    [ColumnName]  VARCHAR (30)  NOT NULL,
    [DescField]   VARCHAR (30)  NOT NULL,
    [Description] VARCHAR (100) NULL,
    CONSTRAINT [PK_DD_Lookups_1__14] PRIMARY KEY CLUSTERED ([LookupID] ASC),
    CONSTRAINT [UniqueLookup] UNIQUE NONCLUSTERED ([ObjectName] ASC)
);

