﻿CREATE TABLE [dbo].[PersCalc] (
    [Refnum]    [dbo].[Refnum] NOT NULL,
    [PersID]    CHAR (15)      NOT NULL,
    [SortKey]   TINYINT        NULL,
    [SectionID] CHAR (4)       NULL,
    [CompEqP]   REAL           NULL,
    [ContEqP]   REAL           NULL,
    [TotEqP]    REAL           NULL,
    [CompWHr]   REAL           NULL,
    [ContWHr]   REAL           NULL,
    [TotWHr]    REAL           NULL,
    CONSTRAINT [PK_PersCalc_1__30] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PersID] ASC)
);

