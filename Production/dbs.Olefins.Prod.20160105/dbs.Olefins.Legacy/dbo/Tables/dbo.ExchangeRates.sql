﻿CREATE TABLE [dbo].[ExchangeRates] (
    [CountryID] SMALLINT     NOT NULL,
    [Country]   VARCHAR (50) NULL,
    [YR_1995]   REAL         NULL,
    [YR_1996]   REAL         NULL,
    [YR_1997]   REAL         NULL,
    [YR_1998]   REAL         NULL,
    [YR_1999]   REAL         NULL,
    [YR_2000]   REAL         NULL,
    [YR_2001]   REAL         NULL,
    [YR_2002]   REAL         NULL,
    [YR_2003]   REAL         NULL,
    CONSTRAINT [PK_ExchangeRates] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);

