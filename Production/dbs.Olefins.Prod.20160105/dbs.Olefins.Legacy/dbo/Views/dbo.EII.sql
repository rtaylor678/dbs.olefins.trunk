﻿












CREATE View [dbo].[EII] as
SELECT Refnum,
EII,
StdEnergy,
PyrolysisFeed,
Compression,
Supplemental,
Auxiliary,
LightFeed,
NaphthatFeed,
HeavyFeed,
DiSuppl,
RefSuppl,
ConcSuppl,
OtherSuppl,
FracFeedKBSD,
HydroCryoCnt,
HydroPSACnt,
HydroMembCnt,
PGasHydroKBSD,
GasCompBHP,
MethCompBHP,
EthyCompBHP,
PropCompBHP,
Purity,
Ethane,
Propane,
Butane,
LPG,
OthLtFeed,
LtNaphtha,
Raffinate,
HeavyNGL,
Condensate,
FRNaphtha,
HeavyNaphtha,
Diesel,
HeavyGasoil,
HTGasoil,
OthLiqFeed1,
OthLiqFeed2,
OthLiqFeed3,
DiHydrogen,
DiMethane,
DiEthane,
DiEthylene,
DiPropylene,
DiPropane,
DiButane,
DiButylenes,
DiBenzene,
DiButadiene,
DiMoGas,
RefHydrogen,
RefMethane,
RefEthane,
RefEthylene,
RefPropylene,
RefPropane,
RefC4Plus,
RefInerts,
ConcEthylene,
ConcPropylene,
ConcBenzene,
ConcButadiene,
GasOil,
WashOil,
OthSpl,
nfFresh,
nfSupp,
nfAuxiliary,
nfReduction,
nfUtilities,
tsCalculated
FROM EIIVersionCalcs
WHERE FactorSet = '2013' and ModelVersion = '2013'

