﻿



CREATE    VIEW dbo.vPracOrg AS
SELECT t.Refnum, 
 TimeFactA=CASE WHEN pta.TimeFact='A' THEN 100 ELSE 0 END,
 TimeFactB=CASE WHEN pta.TimeFact='B' THEN 100 ELSE 0 END,
 TimeFactC=CASE WHEN pta.TimeFact='C' THEN 100 ELSE 0 END,
 TimeFactD=CASE WHEN pta.TimeFact='D' THEN 100 ELSE 0 END,
 TimeFactE=CASE WHEN pta.TimeFact='E' THEN 100 ELSE 0 END,
 TimeFactF=CASE WHEN pta.TimeFact='F' THEN 100 ELSE 0 END,
 CSG=CASE WHEN pta.CGC='X' THEN 100 ELSE 0 END,
 RC=CASE WHEN pta.RC='X' THEN 100 ELSE 0 END,
 TLE=CASE WHEN pta.TLE='X' THEN 100 ELSE 0 END,
 OHX=CASE WHEN pta.OHX='X' THEN 100 ELSE 0 END,
 Tower=CASE WHEN pta.Tower='X' THEN 100 ELSE 0 END, 
 Furnace=CASE WHEN pta.Furnace='X' THEN 100 ELSE 0 END,
 Util=CASE WHEN pta.Util='X' THEN 100 ELSE 0 END,
 CapProj=CASE WHEN pta.CapProj='X' THEN 100 ELSE 0 END,
 Other=CASE WHEN pta.Other='X' THEN 100 ELSE 0 END
FROM TSort t
INNER JOIN PracTA pta ON t.Refnum=pta.Refnum





