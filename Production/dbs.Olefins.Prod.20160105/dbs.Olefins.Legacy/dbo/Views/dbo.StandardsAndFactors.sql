﻿CREATE VIEW [dbo].[StandardsAndFactors]
WITH SCHEMABINDING
AS
SELECT
	p.[SubmissionId],
	z.[SubmissionName]	[Refnum],
	p.[ProcessUnitId],
	l.[ProcessUnitTag],
	l.[ProcessUnitName],
	p.[kEdc],
	p.[StdEnergy],
	p.[PersMaint],
	p.[PersNonMaint],
	p.[Pers],
	p.[Mes],
	p.[NonEnergy]
FROM [calc].[Standards_Pivot_Standard]	p
INNER JOIN [fact].[Submissions]			z
	ON	z.[SubmissionId]	= p.[SubmissionId]
INNER JOIN [dim].[ProcessUnit_LookUp]	l
	ON	l.[ProcessUnitId]	= p.[ProcessUnitId];

