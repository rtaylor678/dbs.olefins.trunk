﻿CREATE PROCEDURE [dbo].[Get_FractionatorFeedStock]
AS
SELECT
	l.[StreamId]	[FeedStockId],
	l.[StreamName]	[FeedStockDesc]
FROM [dim].[Stream_LookUp]	l
WHERE
	l.[StreamId] IN (132, 8, 11, 19, 24, 26);
