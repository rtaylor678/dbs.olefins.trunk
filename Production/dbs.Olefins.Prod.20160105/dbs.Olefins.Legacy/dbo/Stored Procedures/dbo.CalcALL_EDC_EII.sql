﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXECUTE [dbo].[CalcALL_EDC_EII] 2007, '07PCH Rank'
CREATE PROCEDURE [dbo].[CalcALL_EDC_EII] 
	@MethodologyYear smallint,
	@ListName		 varchar(30)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Refnum dbo.Refnum;
	
	BEGIN DECLARE curTemp CURSOR LOCAL FAST_FORWARD FOR
	SELECT	t.Refnum
	FROM	dbo.TSort t
		INNER JOIN dbo._RL r ON r.Refnum = t.Refnum AND r.ListName = @ListName
	ORDER BY t.Refnum ASC	END;
	
	OPEN curTemp;
	FETCH NEXT FROM curTemp INTO @Refnum;
	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXECUTE dbo.CalcEDC @Refnum, @MethodologyYear;
		EXECUTE dbo.CalcEII @Refnum, @MethodologyYear;
		--EXECUTE [dbo].[RefreshNewFactors] @Refnum	-- RRH 20131015: commented for late participants; new factors are still being developed
		
	FETCH NEXT FROM curTemp INTO @Refnum;
	END;

	-- Free variables and memory
	CLOSE curTemp;
	DEALLOCATE curTemp;
	
END








