﻿
/****** Object:  Stored Procedure dbo.spClearRefList    Script Date: 4/18/2003 4:32:54 PM ******/

CREATE PROCEDURE spClearRefList
	@ListName RefListName = NULL,
	@RefListNo RefListNo = NULL
AS
DECLARE @Access smallint
IF @RefListNo IS NULL 
	SELECT @RefListNo = RefListNo
	FROM RefList_LU
	WHERE ListName = @ListName
IF @RefListNo IS NOT NULL
BEGIN
	EXEC @Access = spCheckListOwner @ListNo = @RefListNo
	IF @Access >= 0
		DELETE FROM RefList
		WHERE RefListNo = @RefListNo
	ELSE
		RETURN @Access
END


