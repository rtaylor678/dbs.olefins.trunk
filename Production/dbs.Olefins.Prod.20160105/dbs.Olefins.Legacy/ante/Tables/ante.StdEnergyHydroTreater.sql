﻿CREATE TABLE [ante].[StdEnergyHydroTreater] (
    [MethodologyId]      INT                NOT NULL,
    [HydroTreaterTypeId] INT                NOT NULL,
    [Energy_kBtuBbl]     FLOAT (53)         NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_StdEnergyHydroTreater_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_StdEnergyHydroTreater_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_StdEnergyHydroTreater_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_StdEnergyHydroTreater_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StdEnergyHydroTreater] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC),
    CONSTRAINT [CR_StdEnergyHydroTreater_Energy_kBtuBbl_MinIncl_0.0] CHECK ([Energy_kBtuBbl]>=(0.0)),
    CONSTRAINT [FK_StdEnergyHydroTreater_HydroTreater_LookUp] FOREIGN KEY ([HydroTreaterTypeId]) REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId]),
    CONSTRAINT [FK_StdEnergyHydroTreater_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId])
);


GO

CREATE TRIGGER [ante].[t_StdEnergyHydroTreater_u]
	ON [ante].[StdEnergyHydroTreater]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[StdEnergyHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[StdEnergyHydroTreater].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[StdEnergyHydroTreater].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId];

END;