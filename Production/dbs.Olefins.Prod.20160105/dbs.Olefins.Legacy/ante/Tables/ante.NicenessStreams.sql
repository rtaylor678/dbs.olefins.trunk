﻿CREATE TABLE [ante].[NicenessStreams] (
    [MethodologyId]    INT                NOT NULL,
    [StreamId]         INT                NOT NULL,
    [EmptyComposition] FLOAT (53)         NULL,
    [Divisor]          FLOAT (53)         NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessStreams_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)     CONSTRAINT [DF_NicenessStreams_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)     CONSTRAINT [DF_NicenessStreams_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)     CONSTRAINT [DF_NicenessStreams_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_NicenessStreams] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC),
    CONSTRAINT [CR_NicenessStreams_Divisor_MinIncl_0.0] CHECK ([Divisor]>=(0.0)),
    CONSTRAINT [CR_NicenessStreams_EmptyComposition_MinIncl_0.0] CHECK ([EmptyComposition]>=(0.0)),
    CONSTRAINT [FK_NicenessStreams_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_NicenessStreams_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_NicenessStreams_u]
	ON [ante].[NicenessStreams]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessStreams]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[NicenessStreams].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[NicenessStreams].[StreamId]			= INSERTED.[StreamId];

END;