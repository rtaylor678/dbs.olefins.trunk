﻿CREATE PROCEDURE [ante].[Merge_MapStreamProcessUnit]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapStreamProcessUnit] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_StreamId('Ethylene'),		dim.Return_ProcessUnitId('RedEthylene')),
			(@MethodologyId, dim.Return_StreamId('EthyleneCG'),		dim.Return_ProcessUnitId('RedEthyleneCG')),
			(@MethodologyId, dim.Return_StreamId('Propylene'),		dim.Return_ProcessUnitId('RedPropylene')),
			(@MethodologyId, dim.Return_StreamId('PropyleneCG'),	dim.Return_ProcessUnitId('RedPropyleneCG')),
			(@MethodologyId, dim.Return_StreamId('PropyleneRG'),	dim.Return_ProcessUnitId('RedPropyleneRG')),
			(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ProcessUnitId('RedCrackedGasTrans'))
	)
	AS Source([MethodologyId], [StreamId], [ProcessUnitId])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[ProcessUnitId]	= Source.[ProcessUnitId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [ProcessUnitId])
		VALUES([MethodologyId], [StreamId], [ProcessUnitId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;