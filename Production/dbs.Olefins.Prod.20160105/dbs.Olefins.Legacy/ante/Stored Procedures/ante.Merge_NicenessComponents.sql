﻿CREATE PROCEDURE [ante].[Merge_NicenessComponents]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[NicenessComponents] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_ComponentId('P'), 1.05),
		(@MethodologyId, dim.Return_ComponentId('I'), 1.00),
		(@MethodologyId, dim.Return_ComponentId('A'), 0.67),
		(@MethodologyId, dim.Return_ComponentId('N'), 0.95),
		(@MethodologyId, dim.Return_ComponentId('O'), 0.95)
	)
	AS Source([MethodologyId], [ComponentId], [Coefficient])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[ComponentId]	= Source.[ComponentId]
	WHEN MATCHED THEN UPDATE SET
		Target.[Coefficient]	= Source.[Coefficient]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [ComponentId], [Coefficient])
		VALUES([MethodologyId], [ComponentId], [Coefficient])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;