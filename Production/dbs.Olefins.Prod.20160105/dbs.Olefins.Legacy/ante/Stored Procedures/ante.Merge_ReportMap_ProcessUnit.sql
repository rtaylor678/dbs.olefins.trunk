﻿CREATE PROCEDURE [ante].[Merge_ReportMap_ProcessUnit]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[ReportMap_ProcessUnit] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_ProcessUnitId('Fresh'),					'StdConfigEandP'),
		(@MethodologyId, dim.Return_ProcessUnitId('Supp'),					'SuppFeedsEandP'),
		(@MethodologyId, dim.Return_ProcessUnitId('Auxiliary'),				'Auxiliary'),
		(@MethodologyId, dim.Return_ProcessUnitId('Reduction'),				'Reduction'),
		(@MethodologyId, dim.Return_ProcessUnitId('Utilities'),				'Utilities'),
		(@MethodologyId, dim.Return_ProcessUnitId('Total'),					'Total')
	)
	AS Source([MethodologyId], [ProcessUnitId], [Report_Suffix])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[ProcessUnitId]	= Source.[ProcessUnitId]
	WHEN MATCHED THEN UPDATE SET
		Target.[Report_Suffix]	= Source.[Report_Suffix]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [ProcessUnitId], [Report_Suffix])
		VALUES([MethodologyId], [ProcessUnitId], [Report_Suffix])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;