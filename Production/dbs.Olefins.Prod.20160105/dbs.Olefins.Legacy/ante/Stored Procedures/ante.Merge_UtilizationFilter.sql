﻿CREATE PROCEDURE [ante].[Merge_UtilizationFilter]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[UtilizationFilter] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_StreamId('EthyleneCG'), dim.Return_ComponentId('C2H4')),
		(@MethodologyId, dim.Return_StreamId('EthylenePG'), dim.Return_ComponentId('C2H4')),
		(@MethodologyId, dim.Return_StreamId('PropylenePG'), dim.Return_ComponentId('C3H6')),
		(@MethodologyId, dim.Return_StreamId('PropyleneCG'), dim.Return_ComponentId('C3H6')),
		(@MethodologyId, dim.Return_StreamId('PropyleneRG'), dim.Return_ComponentId('C3H6')),
		(@MethodologyId, dim.Return_StreamId('ProdOther'), dim.Return_ComponentId('C2H4')),
		(@MethodologyId, dim.Return_StreamId('ProdOther'), dim.Return_ComponentId('C3H6'))
	)
	AS Source([MethodologyId], [StreamId], [ComponentId])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	AND	Target.[ComponentId]	= Source.[ComponentId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [ComponentId])
		VALUES([MethodologyId], [StreamId], [ComponentId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;