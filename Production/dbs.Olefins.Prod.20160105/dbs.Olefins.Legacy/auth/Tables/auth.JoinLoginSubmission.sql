﻿CREATE TABLE [auth].[JoinLoginSubmission] (
    [JoinId]         INT                IDENTITY (1, 1) NOT NULL,
    [LoginId]        INT                NOT NULL,
    [SubmissionId]   INT                NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_JoinLoginSubmission_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_JoinLoginSubmission_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_JoinLoginSubmission_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_JoinLoginSubmission_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_JoinLoginSubmission_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_JoinLoginSubmission] PRIMARY KEY CLUSTERED ([JoinId] ASC),
    CONSTRAINT [FK_JoinLoginSubmission_Logins] FOREIGN KEY ([LoginId]) REFERENCES [auth].[Logins] ([LoginId]),
    CONSTRAINT [FK_JoinLoginSubmission_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId]),
    CONSTRAINT [UK_JoinLoginSubmission_Submissions] UNIQUE NONCLUSTERED ([SubmissionId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinLoginSubmission_Active]
    ON [auth].[JoinLoginSubmission]([LoginId] ASC, [SubmissionId] ASC) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_JoinLoginSubmission_u]
	ON [auth].[JoinLoginSubmission]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinLoginSubmission]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinLoginSubmission].[JoinId]		= INSERTED.[JoinId];

END;