﻿CREATE TABLE [dim].[HydroTreaterType_Parent] (
    [MethodologyId]      INT                 NOT NULL,
    [HydroTreaterTypeId] INT                 NOT NULL,
    [ParentId]           INT                 NOT NULL,
    [Operator]           CHAR (1)            CONSTRAINT [DF_HydroTreaterType_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_HydroTreaterType_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_HydroTreaterType_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_HydroTreaterType_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_HydroTreaterType_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_HydroTreaterType_Parent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC),
    CONSTRAINT [FK_HydroTreaterType_Parent_LookUp_HydroTreaterType] FOREIGN KEY ([HydroTreaterTypeId]) REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId]),
    CONSTRAINT [FK_HydroTreaterType_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId]),
    CONSTRAINT [FK_HydroTreaterType_Parent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_HydroTreaterType_Parent_Operator] FOREIGN KEY ([Operator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_HydroTreaterType_Parent_Parent] FOREIGN KEY ([MethodologyId], [HydroTreaterTypeId]) REFERENCES [dim].[HydroTreaterType_Parent] ([MethodologyId], [HydroTreaterTypeId])
);


GO

CREATE TRIGGER [dim].[t_HydroTreaterType_Parent_u]
	ON [dim].[HydroTreaterType_Parent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[HydroTreaterType_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[HydroTreaterType_Parent].[MethodologyId]			= INSERTED.[MethodologyId]
		AND	[dim].[HydroTreaterType_Parent].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId];

END;