﻿CREATE TABLE [dim].[Company_Parent] (
    [MethodologyId]  INT                 NOT NULL,
    [CompanyId]      INT                 NOT NULL,
    [ParentId]       INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Company_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Company_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Company_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Company_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Company_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Company_Parent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [CompanyId] ASC),
    CONSTRAINT [FK_Company_Parent_LookUp_Companys] FOREIGN KEY ([CompanyId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_Company_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_Company_Parent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Company_Parent_Operator] FOREIGN KEY ([Operator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Company_Parent_Parent] FOREIGN KEY ([MethodologyId], [CompanyId]) REFERENCES [dim].[Company_Parent] ([MethodologyId], [CompanyId])
);


GO

CREATE TRIGGER [dim].[t_Company_Parent_u]
	ON [dim].[Company_Parent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Company_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Company_Parent].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Company_Parent].[CompanyId]		= INSERTED.[CompanyId];

END;