﻿CREATE TABLE [calc].[FacilitiesFractionator] (
    [MethodologyId]  INT                NOT NULL,
    [SubmissionId]   INT                NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [FeedStock_BSD]  FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesFractionator_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesFractionator] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_FacilitiesFractionator_FeedStock_BSD_MinIncl_0.0] CHECK ([FeedStock_BSD]>=(0.0)),
    CONSTRAINT [FK_FacilitiesFractionator_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesFractionator_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_FacilitiesFractionator_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_FacilitiesFractionator_u]
	ON [calc].[FacilitiesFractionator]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[FacilitiesFractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[FacilitiesFractionator].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[FacilitiesFractionator].[SubmissionId]	= INSERTED.[SubmissionId];

END;