﻿CREATE PROCEDURE [calc].[Insert_FeedClass]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@FreshPyroFeed			INT		= [dim].[Return_StreamId]('FreshPyroFeed');
	DECLARE	@FreshPyroFeed_kMT		FLOAT;

	SELECT @FreshPyroFeed_kMT = SUM(q.[Quantity_kMT])
	FROM [fact].[StreamQuantity]		q
	INNER JOIN [dim].[Stream_Bridge]	b
		ON	b.[DescendantId]	= q.[StreamId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId
		AND	b.[StreamId]		= @FreshPyroFeed;

	DECLARE	@C2H6					INT = [dim].[Return_ComponentId]('C2H6');
	DECLARE	@C2H6_kMT				FLOAT;

	SELECT @C2H6_kMT = SUM(q.[Component_Dur_kMT])
	FROM [calc].[StreamComposition]		q
	INNER JOIN [dim].[Stream_Bridge]	b
		ON	b.[DescendantId]	= q.[StreamId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId
		AND	q.[ComponentId]		= @C2H6
		AND	b.[StreamId]		= @FreshPyroFeed;

	DECLARE	@C2H4					INT = [dim].[Return_ComponentId]('C2H4');
	DECLARE	@C3H8					INT = [dim].[Return_ComponentId]('C3H8');
	DECLARE	@C3H6					INT = [dim].[Return_ComponentId]('C3H6');
	DECLARE	@NBUTA					INT = [dim].[Return_ComponentId]('NBUTA');
	DECLARE	@IBUTA					INT = [dim].[Return_ComponentId]('IBUTA');
	DECLARE	@B2						INT = [dim].[Return_ComponentId]('B2');
	DECLARE	@B1						INT = [dim].[Return_ComponentId]('B1');
	DECLARE	@C4H6					INT = [dim].[Return_ComponentId]('C4H6');

	DECLARE	@LPG_kMT				FLOAT;

	SELECT @LPG_kMT = SUM(q.[Component_Dur_kMT])
	FROM [calc].[StreamComposition]		q
	INNER JOIN [dim].[Stream_Bridge]	b
		ON	b.[DescendantId]	= q.[StreamId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId
		AND	b.[StreamId]		= @FreshPyroFeed
		AND	q.[ComponentId]		IN (@C2H4, @C3H8, @C3H6, @NBUTA, @IBUTA, @B2, @B1, @C4H6);

	DECLARE	@Liquid					INT		= [dim].[Return_StreamId]('Liquid');
	DECLARE	@Liquid_kMT				FLOAT;

	SELECT @Liquid_kMT = SUM(q.[Quantity_kMT])
	FROM [fact].[StreamQuantity]		q
	INNER JOIN [dim].[Stream_Bridge]	b
		ON	b.[DescendantId]	= q.[StreamId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId
		AND	b.[StreamId]		= @Liquid;

	DECLARE @EpGT390				INT		= [dim].[Return_StreamId]('EpGT390');
	DECLARE @EpGT390_kMT			FLOAT;

	SELECT @EpGT390_kMT = SUM(q.[Quantity_kMT])
	FROM [fact].[StreamQuantity]		q
	INNER JOIN [dim].[Stream_Bridge]	b
		ON	b.[DescendantId]	= q.[StreamId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId
		AND	b.[StreamId]		= @EpGT390;

	DECLARE	@Naphtha				INT		= [dim].[Return_StreamId]('Naphtha');
	DECLARE	@NaphthaDensity_SG		FLOAT;

	SELECT @NaphthaDensity_SG = SUM(q.[Quantity_kMT] * d.[Density_SG]) / SUM(q.[Quantity_kMT])
	FROM [fact].[StreamQuantity]		q
	INNER JOIN [fact].[StreamDensity]	d
		ON	d.[SubmissionId]	= q.[SubmissionId]
		AND	d.[StreamNumber]	= q.[StreamNumber]
	INNER JOIN [dim].[Stream_Bridge]	b
		ON	b.[DescendantId]	= q.[StreamId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId
		AND	b.[StreamId]		= @Naphtha
	GROUP BY
		b.[MethodologyId],
		q.[SubmissionId];

	SET @NaphthaDensity_SG = COALESCE(@NaphthaDensity_SG, 0.0);

	DECLARE	@C2H6_Pcnt				FLOAT	= @C2H6_kMT		/ @FreshPyroFeed_kMT * 100.0;
	DECLARE	@LPG_Pcnt				FLOAT	= @LPG_kMT		/ @FreshPyroFeed_kMT * 100.0;
	DECLARE	@Liquid_Pcnt			FLOAT	= @Liquid_kMT	/ @FreshPyroFeed_kMT * 100.0;
	DECLARE	@EpGT390_Pcnt			FLOAT	= @EpGT390_kMT	/ @FreshPyroFeed_kMT * 100.0;

	DECLARE	@C2H6_Limit				FLOAT	= 85.0;
	DECLARE	@LPG_Limit				FLOAT	= 50.0;
	DECLARE	@Liquid_Limit			FLOAT	= 85.0;
	DECLARE	@EpGT390_Limit			FLOAT	= 25.0;
	DECLARE	@NaphthaDensity_Limit	FLOAT	= 0.7;

	DECLARE @FeedClassTag			VARCHAR(1)		= '1';

	IF	@C2H6_Pcnt > @C2H6_Limit SET @FeedClassTag = '1';
	IF	@LPG_Pcnt > @LPG_Limit AND @C2H6_Pcnt < @C2H6_Limit SET @FeedClassTag = '2';
	IF	(@Liquid_Pcnt > @Liquid_Limit AND @EpGT390_Pcnt < @EpGT390_Limit) OR @NaphthaDensity_SG < @NaphthaDensity_Limit SET @FeedClassTag = '4';
	IF	@FeedClassTag = '-' SET @FeedClassTag = '3'

	IF(DB_NAME() = 'OlefinsOnline')
	INSERT INTO [calc].[FeedClass]([MethodologyId], [SubmissionId], [FeedClassId])
	VALUES (@MethodologyId, @SubmissionId, [dim].[Return_FeedClassId](@FeedClassTag));
	
	IF(DB_NAME() = 'Olefins11Rev') OR (DB_NAME() = 'Olefins')
	INSERT INTO [calc].[FeedClass]([MethodologyId], [SubmissionId], [FeedClassId])
	SELECT
		@MethodologyId,
		z.[SubmissionId],
		[dim].[Return_FeedClassId](CONVERT(VARCHAR, t.FeedClass))
	FROM [fact].[Submissions]	z
	INNER JOIN [dbo].[TSort]	t
		ON	t.[Refnum] = z.[SubmissionName]
	WHERE z.[SubmissionId] = @SubmissionId;
	
	IF(DB_NAME() = 'Sa.Olefins.Calculator.Backend')
	INSERT INTO [calc].[FeedClass]([MethodologyId], [SubmissionId], [FeedClassId])
	SELECT @MethodologyId, @SubmissionId, [dim].[Return_FeedClassId](t.[FeedClassTag])
	FROM (VALUES
		('07PCH004', 3),
		('07PCH005', 2),
		('07PCH006', 4),
		('07PCH007', 4),
		('07PCH010', 4),
		('07PCH011', 3),
		('07PCH012', 3),
		('07PCH013', 4),
		('07PCH015', 2),
		('07PCH016', 3),
		('07PCH018', 2),
		('07PCH01A', 1),
		('07PCH01B', 1),
		('07PCH020', 4),
		('07PCH022', 3),
		('07PCH025', 3),
		('07PCH026', 3),
		('07PCH028', 4),
		('07PCH033', 3),
		('07PCH035', 4),
		('07PCH036', 3),
		('07PCH03A', 4),
		('07PCH03B', 4),
		('07PCH040', 4),
		('07PCH041', 3),
		('07PCH042', 4),
		('07PCH043', 4),
		('07PCH045', 4),
		('07PCH046', 4),
		('07PCH047', 4),
		('07PCH049', 4),
		('07PCH053', 4),
		('07PCH055', 4),
		('07PCH056', 3),
		('07PCH057', 2),
		('07PCH058', 2),
		('07PCH059', 2),
		('07PCH068', 4),
		('07PCH070', 4),
		('07PCH074', 3),
		('07PCH076', 2),
		('07PCH077', 4),
		('07PCH078', 4),
		('07PCH079', 3),
		('07PCH082', 4),
		('07PCH083', 2),
		('07PCH084', 4),
		('07PCH085', 4),
		('07PCH087', 1),
		('07PCH088', 1),
		('07PCH089', 4),
		('07PCH090', 4),
		('07PCH091', 3),
		('07PCH092', 1),
		('07PCH094', 4),
		('07PCH096', 4),
		('07PCH097', 1),
		('07PCH099', 4),
		('07PCH100', 1),
		('07PCH105', 4),
		('07PCH106', 1),
		('07PCH111', 4),
		('07PCH112', 4),
		('07PCH118', 1),
		('07PCH119', 4),
		('07PCH122', 4),
		('07PCH127', 2),
		('07PCH128', 4),
		('07PCH131', 2),
		('07PCH133', 1),
		('07PCH134', 4),
		('07PCH140', 4),
		('07PCH141', 1),
		('07PCH143', 4),
		('07PCH144', 4),
		('07PCH145', 4),
		('07PCH152', 4),
		('07PCH153', 4),
		('07PCH154', 4),
		('07PCH155', 3),
		('07PCH157', 4),
		('07PCH159', 4),
		('07PCH162', 4),
		('07PCH163', 1),
		('07PCH164', 4),
		('07PCH165', 1),
		('07PCH167', 4),
		('07PCH168', 2),
		('07PCH169', 1),
		('07PCH170', 4),
		('07PCH171', 2),
		('07PCH172', 2),
		('07PCH173', 1),
		('07PCH174', 4),
		('07PCH175', 3),
		('07PCH176', 4),
		('07PCH178', 4),
		('07PCH179', 2),
		('07PCH181', 1),
		('07PCH184', 1),
		('07PCH186', 4),
		('07PCH189', 2),
		('07PCH191', 1),
		('07PCH195', 4),
		('07PCH196', 4),
		('07PCH197', 3),
		('07PCH198', 3),
		('07PCH199', 3),
		('07PCH200', 4),
		('07PCH201', 4),
		('07PCH202', 4),
		('07PCH203', 4),
		('07PCH23A', 1),
		('07PCH23B', 2),
		('07PCH23C', 2),
		('07PCH50A', 4),
		('07PCH50B', 4),
		('09PCH053', 4),
		('09PCH105', 4),
		('09PCH122', 4),
		('09PCH128', 4),
		('09PCH131', 2),
		('09PCH134', 4),
		('09PCH144', 4),
		('09PCH149', 4),
		('09PCH152', 4),
		('09PCH153', 4),
		('09PCH154', 4),
		('09PCH159', 4),
		('09PCH167', 4),
		('09PCH168', 2),
		('09PCH175', 3),
		('09PCH178', 3),
		('09PCH197', 4),
		('09PCH198', 3),
		('09PCH209', 3),
		('09PCH210', 2),
		('09PCH216', 4),
		('09PCH003', 4),
		('09PCH010', 4),
		('09PCH013', 4),
		('09PCH015', 1),
		('09PCH020', 4),
		('09PCH025', 3),
		('09PCH026', 3),
		('09PCH028', 4),
		('09PCH033', 3),
		('09PCH042', 4),
		('09PCH043', 4),
		('09PCH045', 4),
		('09PCH046', 4),
		('09PCH047', 4),
		('09PCH049', 4),
		('09PCH055', 4),
		('09PCH056', 3),
		('09PCH068', 4),
		('09PCH070', 3),
		('09PCH074', 3),
		('09PCH075', 4),
		('09PCH077', 4),
		('09PCH078', 4),
		('09PCH079', 3),
		('09PCH084', 4),
		('09PCH085', 4),
		('09PCH094', 4),
		('09PCH095', 4),
		('09PCH096', 4),
		('09PCH111', 4),
		('09PCH112', 4),
		('09PCH115', 4),
		('09PCH126', 4),
		('09PCH164', 4),
		('09PCH199', 3),
		('09PCH208', 3),
		('09PCH211', 4),
		('09PCH214', 4),
		('09PCH50B', 4),
		('09PCH006', 4),
		('09PCH007', 4),
		('09PCH040', 4),
		('09PCH097', 1),
		('09PCH140', 4),
		('09PCH143', 4),
		('09PCH165', 1),
		('09PCH194', 2),
		('09PCH148', 1),
		('09PCH169', 1),
		('09PCH170', 4),
		('09PCH171', 2),
		('09PCH172', 2),
		('09PCH173', 1),
		('09PCH174', 2),
		('09PCH184', 1),
		('09PCH191', 1),
		('09PCH212', 2),
		('09PCH004', 3),
		('09PCH005', 2),
		('09PCH011', 3),
		('09PCH012', 3),
		('09PCH029', 3),
		('09PCH036', 3),
		('09PCH039', 1),
		('09PCH057', 2),
		('09PCH058', 2),
		('09PCH059', 2),
		('09PCH076', 2),
		('09PCH082', 4),
		('09PCH087', 1),
		('09PCH088', 1),
		('09PCH089', 2),
		('09PCH091', 3),
		('09PCH092', 1),
		('09PCH099', 4),
		('09PCH100', 1),
		('09PCH106', 1),
		('09PCH119', 4),
		('09PCH127', 2),
		('09PCH133', 1),
		('09PCH162', 4),
		('09PCH16A', 3),
		('09PCH16B', 3),
		('09PCH23B', 2),
		('09PCH23C', 2),
		('11PCH053', 4),
		('11PCH083', 2),
		('11PCH105', 4),
		('11PCH108', 4),
		('11PCH109', 4),
		('11PCH122', 4),
		('11PCH123', 1),
		('11PCH128', 4),
		('11PCH131', 2),
		('11PCH134', 4),
		('11PCH144', 4),
		('11PCH149', 3),
		('11PCH152', 4),
		('11PCH153', 4),
		('11PCH154', 4),
		('11PCH159', 4),
		('11PCH167', 4),
		('11PCH168', 2),
		('11PCH175', 3),
		('11PCH176', 4),
		('11PCH183', 4),
		('11PCH186', 4),
		('11PCH195', 4),
		('11PCH197', 3),
		('11PCH198', 4),
		('11PCH209', 4),
		('11PCH210', 2),
		('11PCH216', 2),
		('11PCH221', 1),
		('11PCH224', 4),
		('11PCH225', 4),
		('11PCH003', 4),
		('11PCH010', 4),
		('11PCH013', 4),
		('11PCH015', 2),
		('11PCH020', 4),
		('11PCH025', 4),
		('11PCH026', 3),
		('11PCH028', 2),
		('11PCH033', 3),
		('11PCH041', 4),
		('11PCH042', 4),
		('11PCH043', 4),
		('11PCH045', 4),
		('11PCH046', 4),
		('11PCH047', 4),
		('11PCH049', 4),
		('11PCH055', 4),
		('11PCH056', 3),
		('11PCH070', 4),
		('11PCH074', 4),
		('11PCH077', 2),
		('11PCH078', 4),
		('11PCH079', 3),
		('11PCH094', 4),
		('11PCH095', 4),
		('11PCH096', 4),
		('11PCH111', 4),
		('11PCH112', 4),
		('11PCH115', 4),
		('11PCH164', 4),
		('11PCH199', 4),
		('11PCH208', 4),
		('11PCH213', 4),
		('11PCH214', 4),
		('11PCH50A', 4),
		('11PCH50B', 4),
		('11PCH006', 4),
		('11PCH007', 4),
		('11PCH040', 4),
		('11PCH097', 1),
		('11PCH140', 4),
		('11PCH143', 4),
		('11PCH165', 1),
		('11PCH194', 2),
		('11PCH148', 1),
		('11PCH184', 1),
		('11PCH212', 2),
		('11PCH222', 1),
		('11PCH004', 3),
		('11PCH005', 2),
		('11PCH011', 3),
		('11PCH012', 3),
		('11PCH018', 2),
		('11PCH01A', 1),
		('11PCH01B', 1),
		('11PCH022', 3),
		('11PCH029', 3),
		('11PCH036', 3),
		('11PCH039', 1),
		('11PCH057', 2),
		('11PCH058', 2),
		('11PCH059', 2),
		('11PCH076', 2),
		('11PCH082', 2),
		('11PCH087', 1),
		('11PCH088', 1),
		('11PCH089', 3),
		('11PCH091', 3),
		('11PCH092', 1),
		('11PCH099', 2),
		('11PCH100', 1),
		('11PCH106', 1),
		('11PCH118', 1),
		('11PCH119', 2),
		('11PCH127', 2),
		('11PCH133', 1),
		('11PCH162', 3),
		('11PCH16A', 3),
		('11PCH16B', 3),
		('11PCH179', 2),
		('11PCH181', 1),
		('11PCH23A', 1),
		('11PCH23B', 2),
		('11PCH23C', 2)
		) t ([Refnum], [FeedClassTag])
	INNER JOIN [fact].[Submissions]		z
		ON	z.[SubmissionName]	= t.[Refnum]
		AND	z.[SubmissionId]	= @SubmissionId

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
