﻿CREATE TABLE [fact].[StreamDescription] (
    [SubmissionId]      INT                NOT NULL,
    [StreamNumber]      INT                NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_StreamDescription_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_StreamDescription_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_StreamDescription_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_StreamDescription_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamDescription] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC),
    CONSTRAINT [CL_StreamDescription_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [FK_StreamDescription_StreamQuantity] FOREIGN KEY ([SubmissionId], [StreamNumber]) REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber]),
    CONSTRAINT [FK_StreamDescription_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_StreamDescription_u]
	ON [fact].[StreamDescription]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamDescription]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamDescription].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[fact].[StreamDescription].[StreamNumber]	= INSERTED.[StreamNumber];

END;