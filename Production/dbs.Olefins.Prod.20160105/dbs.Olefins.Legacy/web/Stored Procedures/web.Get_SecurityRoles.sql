﻿CREATE PROCEDURE [web].[Get_SecurityRoles]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		r.[RoleId]			[SecurityLevelID],
		r.[RoleLevel]		[SecurityLevel],
		r.[RoleName]		[SecurityLevelDesc]
	FROM [auth].[Get_Roles]()	r;

END;