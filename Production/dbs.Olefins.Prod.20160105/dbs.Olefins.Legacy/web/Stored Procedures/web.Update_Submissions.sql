﻿CREATE PROCEDURE [web].[Update_Submissions]
(
	@SubmissionId	INT,
	@Active			BIT = 0
)
AS
BEGIN

	DECLARE @JoinId	INT;

	EXECUTE @JoinId = [auth].[Update_JoinPlantSubmission] NULL, NULL, @SubmissionId, @Active;

	SELECT @JoinId;
	RETURN @JoinId;

END;