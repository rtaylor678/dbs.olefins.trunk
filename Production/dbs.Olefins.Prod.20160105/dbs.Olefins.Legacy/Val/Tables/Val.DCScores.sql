﻿CREATE TABLE [Val].[DCScores] (
    [Refnum]   CHAR (9)     NOT NULL,
    [Version]  SMALLINT     NOT NULL,
    [Category] VARCHAR (10) NOT NULL,
    [RedH]     REAL         NULL,
    [RedM]     REAL         NULL,
    [RedL]     REAL         NULL,
    [BlueH]    REAL         NULL,
    [BlueM]    REAL         NULL,
    [BlueL]    REAL         NULL,
    [TealH]    REAL         NULL,
    [TealM]    REAL         NULL,
    [TealL]    REAL         NULL,
    [GreenH]   REAL         NULL,
    [GreenM]   REAL         NULL,
    [GreenL]   REAL         NULL,
    CONSTRAINT [PK_DCScores] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Version] ASC, [Category] ASC)
);

