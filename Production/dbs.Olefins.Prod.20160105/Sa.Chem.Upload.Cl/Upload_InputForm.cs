﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Threading.Tasks;

namespace Chem.UpLoad
{
	public class Upload_OSIM: Chem.UpLoad.InputForm
	{
		public void UpLoad(string Refnum, string PathFile)
		{
			string sRefnum = Common.GetRefnumSmall(Refnum);
			uint StudyYear = Common.GetStudyYear(Refnum);

			Delete(sRefnum);
			Insert(PathFile, sRefnum, StudyYear);
		}

		void Insert(string PathFile, string sRefnum, uint StudyYear)
		{
			Excel.Application xla = Common.NewExcelApplication(false);
			Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, PathFile);

			if (VerifyRefnum(wkb, sRefnum))
			{
				bool includeSpace = ((StudyYear >= 2015) && (Common.GetFileType(PathFile) == Common.Osim));

					Parallel.Invoke
					(
						() => { UpLoadTSort(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadCapacity(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadCapGrowth(wkb, sRefnum, includeSpace); },
						() =>
						{
							if (StudyYear >= 2015)
							{
								UpLoadFacilities2015(wkb, sRefnum, StudyYear, includeSpace);
							}
							else
							{
								UpLoadFacilities(wkb, sRefnum, StudyYear, includeSpace);
							}
						},
						() => { UpLoadFeedQuality(wkb, sRefnum, includeSpace); },
						() => { UpLoadQuantity(wkb, sRefnum, includeSpace); },
						() => { UpLoadOpEx(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadOpExMiscDetail(wkb, sRefnum, includeSpace); },
						() => { UpLoadPers(wkb, sRefnum, includeSpace); },
						() => { UpLoadPracTA(wkb, sRefnum, includeSpace); },
						() => { UpLoadTADT(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadPracFurnInfo(wkb, sRefnum, includeSpace); },
						() => { UpLoadFurnRely(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadEnergyQnty(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadPracAdv(wkb, sRefnum, includeSpace); },
						() => { UpLoadPracMPC(wkb, sRefnum, includeSpace); },
						() => { UpLoadPracControls(wkb, sRefnum, includeSpace); },
						() => { UpLoadPracOnlineFact(wkb, sRefnum, includeSpace); },
						() => { UpLoadMaint01(wkb, sRefnum, includeSpace); },
						() => { UpLoadMaintMisc(wkb, sRefnum, includeSpace); },
						() => { UpLoadFeedSelections(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadFeedResources(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadMisc(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadPracOrg(wkb, sRefnum, includeSpace); },
						() => { UpLoadProdQuality(wkb, sRefnum, includeSpace); },
						() => { UpLoadComposition(wkb, sRefnum, includeSpace); },
						() => { UpLoadPrac(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadInventory(wkb, sRefnum, includeSpace); },
						() => { UpLoadDuties(wkb, sRefnum, includeSpace); },
						() => { UpLoadFeedFlex(wkb, sRefnum, includeSpace); },
						() => { UpLoadCapitalExp(wkb, sRefnum, includeSpace); },
						() => { UpLoadEnviron(wkb, sRefnum, includeSpace); },
						() => { UpLoadFeedPlan(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadProdLoss(wkb, sRefnum, StudyYear, includeSpace); },

						() => { UpLoadCompressors(wkb, sRefnum, StudyYear, includeSpace); },
						() => { UpLoadPracRely(wkb, sRefnum, StudyYear, includeSpace); },

						() =>
						{
							if (StudyYear >= 2013)
							{
								UpLoadProdLoss_Availability(wkb, sRefnum, StudyYear, includeSpace);
							}
						}
					);

				Parallel.Invoke
				(
					() => { UpLoadPolyFacilities(wkb, sRefnum, includeSpace); },
					() => { UpLoadPolyOpEx(wkb, sRefnum, includeSpace); },
					() => { UpLoadPolyMatlBalance(wkb, sRefnum, includeSpace); },
					() => { UpLoadMetaFeedProd(wkb, sRefnum, includeSpace); },
					() => { UpLoadMetaOpEx(wkb, sRefnum, includeSpace); },
					() => { UpLoadMetaEnergy(wkb, sRefnum, includeSpace); },
					() => { UpLoadMetaMisc(wkb, sRefnum, includeSpace); }
				);
			}

			Common.CloseExcel(ref xla, ref wkb);
		}
	}
}
