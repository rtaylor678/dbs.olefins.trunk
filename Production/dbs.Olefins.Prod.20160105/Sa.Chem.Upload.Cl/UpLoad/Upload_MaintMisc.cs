﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadMaintMisc(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_MaintMisc]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@RespUnplan       REAL         = NULL
						//@RespPlan         REAL         = NULL
						//@Prevent          REAL         = NULL
						//@Predict          REAL         = NULL
						//@Training         REAL         = NULL

						#region Table6-3

						string cT06_03 = (includeSpace) ? "Table 6-3" : "Table6-3";
						wks = wkb.Worksheets[cT06_03];

						c = 8;

						//@FurnTubeLife     REAL         = NULL
						r = 30;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnTubeLife", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FurnRetubeTime   REAL         = NULL
						r = 34;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnRetubeTime", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FurnRetubeWho    VARCHAR (25) = NULL
						r = 37;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnRetubeWho", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						#endregion

						#region Table10-1

						string cT10_01 = (includeSpace) ? "Table 10-1" : "Table10-1";
						wks = wkb.Worksheets[cT10_01];

						//@Accuracy         CHAR (1)     = NULL
						c = 4;
						r = 63;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Accuracy", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						#endregion

						#region Table10-2 a

						string cT10_02 = (includeSpace) ? "Table 10-2" : "Table10-2";
						wks = wkb.Worksheets[cT10_02];

						c = 7;

						//@MaintMatlPY      REAL         = NULL
						r = 15;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MaintMatlPY", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ContMaintLaborPY REAL         = NULL
						r = 16;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ContMaintLaborPY", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ContMaintMatlPY  REAL         = NULL
						r = 17;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ContMaintMatlPY", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MaintEquipRentPY REAL         = NULL
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MaintEquipRentPY", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OCCMaintST_PY    REAL         = NULL
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OCCMaintST_PY", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MPSMaintST_PY    REAL         = NULL
						r = 22;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MPSMaintST_PY", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Table10-2 b

						c = 9;

						//@CorrEmergMaint   REAL         = NULL
						r = 36;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CorrEmergMaint", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CorrOthMaint     REAL         = NULL
						r = 37;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CorrOthMaint", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PrevCondAct      REAL         = NULL
						r = 39;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PrevCondAct", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PrevAddMaint     REAL         = NULL
						r = 40;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PrevAddMaint", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PrevRoutine      REAL         = NULL
						r = 41;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PrevRoutine", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PrefAddRoutine   REAL         = NULL
						r = 42;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PrefAddRoutine", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMaintMisc", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_MaintMisc]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}