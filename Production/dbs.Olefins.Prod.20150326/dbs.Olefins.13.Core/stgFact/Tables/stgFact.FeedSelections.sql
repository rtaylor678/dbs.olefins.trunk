﻿CREATE TABLE [stgFact].[FeedSelections] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdID]     VARCHAR (20)       NOT NULL,
    [Mix]            CHAR (1)           NULL,
    [Blend]          CHAR (1)           NULL,
    [Fractionation]  CHAR (1)           NULL,
    [Hydrotreat]     CHAR (1)           NULL,
    [Purification]   CHAR (1)           NULL,
    [MinSGEst]       REAL               NULL,
    [MaxSGEst]       REAL               NULL,
    [MinSGAct]       REAL               NULL,
    [MaxSGAct]       REAL               NULL,
    [PipelinePct]    REAL               NULL,
    [TruckPct]       REAL               NULL,
    [BargePct]       REAL               NULL,
    [ShipPct]        REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_FeedSelections_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedSelections_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedSelections_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedSelections_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_FeedSelections] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_FeedSelections_u]
	ON [stgFact].[FeedSelections]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[FeedSelections]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[FeedSelections].Refnum		= INSERTED.Refnum
		AND	[stgFact].[FeedSelections].FeedProdID	= INSERTED.FeedProdID;

END;