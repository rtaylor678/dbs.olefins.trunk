﻿CREATE TABLE [stgFact].[Prac] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [FeedLogCost]       REAL               NULL,
    [EtyLogCost]        REAL               NULL,
    [PryLogCost]        REAL               NULL,
    [AffGasPcnt]        REAL               NULL,
    [AffEtyPcnt]        REAL               NULL,
    [AffPryPcnt]        REAL               NULL,
    [AffOthPcnt]        REAL               NULL,
    [EtyCarrier]        CHAR (1)           NULL,
    [ButaExtract]       CHAR (1)           NULL,
    [AromExtract]       CHAR (1)           NULL,
    [MTBE]              CHAR (1)           NULL,
    [IsoMTBE]           CHAR (1)           NULL,
    [IsoHighPurity]     CHAR (1)           NULL,
    [Butene1]           CHAR (1)           NULL,
    [Isoprene]          CHAR (1)           NULL,
    [Cyclopent]         CHAR (1)           NULL,
    [OthC5]             CHAR (1)           NULL,
    [InletTemp]         REAL               NULL,
    [FurnPreheatPcnt]   REAL               NULL,
    [FurnOutputPcnt]    REAL               NULL,
    [StmPress]          REAL               NULL,
    [TLE2Pcnt]          REAL               NULL,
    [TurbDriver]        CHAR (1)           NULL,
    [FurnTemp]          REAL               NULL,
    [OxyContent]        REAL               NULL,
    [OutletTemp]        REAL               NULL,
    [Deterioration]     REAL               NULL,
    [CapCreep]          REAL               NULL,
    [LocFactor]         REAL               NULL,
    [PrepHrs]           REAL               NULL,
    [AffChmPcnt]        REAL               NULL,
    [SteamReq]          REAL               NULL,
    [AffGasPcntNonDisc] REAL               NULL,
    [NonAffGasPcnt]     REAL               NULL,
    [SteamPcnt]         REAL               NULL,
    [TotFDCost]         REAL               NULL,
    [TotProdRev]        REAL               NULL,
    [CW]                REAL               NULL,
    [CWSupplyTemp]      REAL               NULL,
    [CWReturnTemp]      REAL               NULL,
    [HeatFF]            REAL               NULL,
    [ProcGasCompr]      REAL               NULL,
    [ComprEffDesc]      VARCHAR (40)       NULL,
    [ExtractSteam]      REAL               NULL,
    [CondSteam]         REAL               NULL,
    [SurfCondVac]       REAL               NULL,
    [DutyPPHE]          REAL               NULL,
    [QuenchOilHM]       REAL               NULL,
    [QuenchOilDS]       REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Prac_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_Prac_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_Prac_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_stgFact_Prac_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Prac] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Prac_u]
	ON [stgFact].[Prac]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Prac]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Prac].Refnum		= INSERTED.Refnum;

END;