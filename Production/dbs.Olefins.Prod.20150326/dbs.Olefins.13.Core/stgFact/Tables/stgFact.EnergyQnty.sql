﻿CREATE TABLE [stgFact].[EnergyQnty] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [EnergyType]       VARCHAR (12)       NOT NULL,
    [Amount]           REAL               NULL,
    [PSIG]             REAL               NULL,
    [MBtu]             REAL               NULL,
    [Cost]             REAL               NULL,
    [YN]               VARCHAR (3)        NULL,
    [UnitCnt]          INT                NULL,
    [Efficiency]       REAL               NULL,
    [PurchaseSteamPct] REAL               NULL,
    [ElecPowerPct]     REAL               NULL,
    [ThermalPct]       REAL               NULL,
    [OthDesc]          VARCHAR (100)      NULL,
    [Temperature]      REAL               NULL,
    [Methane_WtPcnt]   REAL               NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_EnergyQnty_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyQnty_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyQnty_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyQnty_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_EnergyQnty] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EnergyType] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_EnergyQnty_u]
	ON [stgFact].[EnergyQnty]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[EnergyQnty]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[EnergyQnty].Refnum		= INSERTED.Refnum;

END;