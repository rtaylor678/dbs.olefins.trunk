﻿CREATE TABLE [stgFact].[PracAdv] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Tbl9001]        REAL               NULL,
    [Tbl9002]        REAL               NULL,
    [Tbl9003]        CHAR (1)           NULL,
    [Tbl9004]        REAL               NULL,
    [Tbl9005]        CHAR (1)           NULL,
    [Tbl9006]        CHAR (1)           NULL,
    [Tbl9007]        CHAR (1)           NULL,
    [Tbl9008]        CHAR (1)           NULL,
    [Tbl9009]        REAL               NULL,
    [Tbl9009a]       REAL               NULL,
    [Tbl9010]        CHAR (1)           NULL,
    [Tbl9011]        REAL               NULL,
    [Tbl9012]        REAL               NULL,
    [Tbl9013]        REAL               NULL,
    [Tbl9014]        INT                NULL,
    [Tbl9015]        INT                NULL,
    [Tbl9016]        REAL               NULL,
    [Tbl9017]        REAL               NULL,
    [Tbl9018]        CHAR (1)           NULL,
    [Tbl9019]        CHAR (1)           NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PracAdv_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracAdv_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracAdv_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracAdv_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PracAdv] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PracAdv_u]
	ON [stgFact].[PracAdv]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PracAdv]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PracAdv].Refnum		= INSERTED.Refnum;

END;