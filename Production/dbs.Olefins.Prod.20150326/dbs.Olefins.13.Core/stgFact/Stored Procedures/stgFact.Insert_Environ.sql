﻿CREATE PROCEDURE [stgFact].[Insert_Environ]
(
    @Refnum VARCHAR (25),

    @Emis   REAL     = NULL,
    @Waste  REAL     = NULL,
    @NOx    REAL     = NULL,
    @Water  REAL     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Environ]([Refnum], [Emis], [Waste], [NOx], [Water])
	VALUES(@Refnum, @Emis, @Waste, @NOx, @Water);

END;