﻿CREATE PROCEDURE [stgFact].[Delete_ProdLoss]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[ProdLoss]
	WHERE [Refnum] = @Refnum;

END;