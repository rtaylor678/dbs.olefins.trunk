﻿CREATE PROCEDURE [stgFact].[Delete_Environ]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Environ]
	WHERE [Refnum] = @Refnum;

END;