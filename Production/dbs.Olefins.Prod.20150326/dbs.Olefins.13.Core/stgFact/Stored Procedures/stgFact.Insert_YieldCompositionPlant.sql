﻿CREATE PROCEDURE [stgFact].[Insert_YieldCompositionPlant]
(
	@Refnum					VARCHAR (25),
	@SimModelId				VARCHAR (12),
	@OpCondId				VARCHAR (12),
	@ComponentId			VARCHAR (42),

	@Component_WtPcnt		REAL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[YieldCompositionPlant]([Refnum], [SimModelId], [OpCondId], [ComponentId], [Component_WtPcnt])
	VALUES(@Refnum, @SimModelId, @OpCondId, @ComponentId, @Component_WtPcnt);

END;