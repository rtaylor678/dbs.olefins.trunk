﻿
CREATE view [dbo].[_RL] as
Select ListName, Refnum, UserGroup, CoLoc = dbo.CoLoc(Refnum), rl.ListId
FROM cons.RefList rl JOIN cons.RefListLu rlu on rl.ListId = rlu.ListId
