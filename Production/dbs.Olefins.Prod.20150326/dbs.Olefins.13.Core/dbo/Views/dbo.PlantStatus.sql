﻿







CREATE view [dbo].[PlantStatus] as
Select t.Refnum, CoLoc, t.Consultant
, OSIMUpload = MAX(CASE WHEN MessageID = 5 THEN MessageTime END) 
, PYPSUpload = MAX(CASE WHEN MessageID = 8 THEN MessageTime END) 
, SPSLUpload = MAX(CASE WHEN MessageID = 9 THEN MessageTime END) 
, Calcs = MAX(CASE WHEN MessageID = 4 THEN MessageTime END) 
, MakeTables = MAX(CASE WHEN MessageID = 11 THEN MessageTime END) 
, reports = MAX(CASE WHEN MessageID = 12 THEN MessageTime END) 
, PTs = MAX(CASE WHEN MessageID = 18 THEN MessageTime END) 
, CTs = MAX(CASE WHEN MessageID = 10 THEN MessageTime END) 
, SumCalc = MAX(CASE WHEN MessageID = 17 THEN MessageTime END) 
From MessageLog m JOIn dbo.TSort t on t.Refnum=m.Refnum
Group by t.Refnum, CoLoc, t.Consultant







