﻿CREATE VIEW calc.EdcFeedDivisor
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	p.FreshPyroFeed		[FreshPyroFeed_kMT],
	p.SuppTot			[SuppTot_kMT],
	ABS(c.Stream_MTd)	[Stream_MTd],
	CASE WHEN (ISNULL(c.Stream_MTd, 0.0) - ISNULL(p.SuppTot, 0.0)) <> 0.0 THEN
		ABS(ISNULL(p.FreshPyroFeed, 0.0) / (ISNULL(c.Stream_MTd, 0.0) + ISNULL(p.SuppTot, 0.0))) * 1000.0 
		END							[FeedDivisor]
FROM (
	SELECT
		b.FactorSetId,
		q.Refnum,
		b.StreamId,
		SUM(
			CASE b.DescendantOperator
			WHEN '+' THEN + q.Quantity_kMT
			WHEN '-' THEN - q.Quantity_kMT
			END)							[Quantity_kMT]
	FROM dim.Stream_Bridge		b
	INNER JOIN fact.Quantity	q
		ON	q.StreamId = b.DescendantId
	WHERE	b.StreamId <> b.DescendantId
		AND(b.StreamId = 'FreshPyroFeed'
		OR	b.StreamId = 'SuppTot' AND b.DescendantId IN ('ConcEthylene', 'ConcPropylene', 'DilEthylene', 'DilPropylene', 'ROGEthylene', 'ROGPropylene'))
	GROUP BY
		b.FactorSetId,
		q.Refnum,
		b.StreamId
	) u
	PIVOT (
	MAX(Quantity_kMT) FOR StreamId IN ([FreshPyroFeed], [SuppTot])
	) p
INNER JOIN fact.CapacityAggregate c
	ON	c.FactorSetId = p.FactorSetId
	AND	c.Refnum = p.Refnum
	AND c.StreamId = 'ProdOlefins'
	AND c.Stream_MTd IS NOT NULL;