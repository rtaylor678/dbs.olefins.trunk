﻿CREATE VIEW calc.ReplacementValueAggregate
WITH SCHEMABINDING
AS
SELECT
	r.FactorSetId,
	r.Refnum,
	r.CalDateKey,
	b.ReplacementValueId,
	r.CurrencyRpt,
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + r.ReplacementValue
		WHEN '-' THEN - r.ReplacementValue
		END)							[ReplacementValue],
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + r.ReplacementLocation
		WHEN '-' THEN - r.ReplacementLocation
		END)							[ReplacementLocation]
FROM calc.ReplacementValue				r
INNER JOIN dim.ReplacementValue_Bridge	b
	ON	b.FactorSetId = r.FactorSetId
	AND	b.DescendantId = r.ReplacementValueId
GROUP BY
	r.FactorSetId,
	r.Refnum,
	r.CalDateKey,
	b.ReplacementValueId,
	r.CurrencyRpt;