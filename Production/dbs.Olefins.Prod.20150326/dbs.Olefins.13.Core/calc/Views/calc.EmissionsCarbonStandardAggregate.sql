﻿CREATE VIEW [calc].[EmissionsCarbonStandardAggregate]
WITH SCHEMABINDING
AS
SELECT
	ecs.FactorSetId,
	ecs.Refnum,
	ecs.SimModelId,
	ecs.CalDateKey,
	b.EmissionsId,

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecs.Quantity_kMT
		WHEN '-' THEN 	ecs.Quantity_kMT
		END)				[Quantity_kMT],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecs.Quantity_MBtu
		WHEN '-' THEN 	ecs.Quantity_MBtu
		END)				[Quantity_MBtu],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecs.EmissionsCarbon_MTCO2e
		WHEN '-' THEN 	ecs.EmissionsCarbon_MTCO2e
		END)/
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ISNULL(ecs.Quantity_MBtu, ecs.Quantity_kMT)
		WHEN '-' THEN 	ISNULL(ecs.Quantity_MBtu, ecs.Quantity_kMT)
		END)				[CefGwp],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecs.EmissionsCarbon_MTCO2e
		WHEN '-' THEN 	ecs.EmissionsCarbon_MTCO2e
		END)				[EmissionsCarbon_MTCO2e]

FROM calc.EmissionsCarbonStandard				ecs
INNER JOIN dim.EmissionsStandard_Bridge			b
	ON	b.FactorSetId	= ecs.FactorSetId
	AND	b.DescendantId	= ecs.EmissionsId
GROUP BY
	ecs.FactorSetId,
	ecs.Refnum,
	ecs.SimModelId,
	ecs.CalDateKey,
	b.EmissionsId;
