﻿CREATE TABLE [calc].[ReliabilityIndicator] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [CurrencyRpt]          VARCHAR (4)        NOT NULL,
    [ReliabilityIndicator] REAL               NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityIndicator_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_ReliabilityIndicator_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_ReliabilityIndicator_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_ReliabilityIndicator_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ReliabilityIndicator] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ReliabilityIndicator_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ReliabilityIndicator_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_ReliabilityIndicator_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ReliabilityIndicator_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ReliabilityIndicator_u
	ON  calc.ReliabilityIndicator
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ReliabilityIndicator
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ReliabilityIndicator.FactorSetId	= INSERTED.FactorSetId
		AND calc.ReliabilityIndicator.Refnum		= INSERTED.Refnum
		AND calc.ReliabilityIndicator.CalDateKey	= INSERTED.CalDateKey
		AND calc.ReliabilityIndicator.CurrencyRpt	= INSERTED.CurrencyRpt;
				
END