﻿CREATE TABLE [calc].[CapacityGrowthAggregate] (
    [FactorSetId]         VARCHAR (12)       NOT NULL,
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [StreamId]            VARCHAR (42)       NOT NULL,
    [Prod_kMT]            REAL               NULL,
    [Capacity_kMT]        REAL               NULL,
    [_Utilization_Pcnt]   AS                 (CONVERT([real],case when [Capacity_kMT]<>(0.0) then ([Prod_kMT]/[Capacity_kMT])*(100.0)  end,(1))) PERSISTED,
    [StudyYearDifference] SMALLINT           NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityGrowthAggregate_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_CapacityGrowthAggregate_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_CapacityGrowthAggregate_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_CapacityGrowthAggregate_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CapacityGrowthAggregate] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CapacityGrowthAggregate_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CapacityGrowthAggregate_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CapacityGrowthAggregate_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_CapacityGrowthAggregate_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_CapacityGrowthAggregate_Projected]
    ON [calc].[CapacityGrowthAggregate]([StudyYearDifference] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Capacity_kMT]);


GO
CREATE TRIGGER calc.t_CapacityGrowthAggregate_u
	ON  calc.[CapacityGrowthAggregate]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[CapacityGrowthAggregate]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[CapacityGrowthAggregate].FactorSetId	= INSERTED.FactorSetId
		AND calc.[CapacityGrowthAggregate].Refnum		= INSERTED.Refnum
		AND calc.[CapacityGrowthAggregate].CalDateKey	= INSERTED.CalDateKey
		AND calc.[CapacityGrowthAggregate].StreamId		= INSERTED.StreamId;
	
END