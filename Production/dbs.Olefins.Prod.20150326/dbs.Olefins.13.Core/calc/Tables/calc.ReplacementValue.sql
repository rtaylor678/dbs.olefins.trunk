﻿CREATE TABLE [calc].[ReplacementValue] (
    [FactorSetId]         VARCHAR (12)       NOT NULL,
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [CurrencyRpt]         VARCHAR (4)        NOT NULL,
    [ReplacementValueId]  VARCHAR (42)       NOT NULL,
    [ReplacementValue]    REAL               NOT NULL,
    [ReplacementLocation] REAL               NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_ReplacementValue_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_ReplacementValue_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_ReplacementValue_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_ReplacementValue_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ReplacementValue] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [ReplacementValueId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ReplacementValue_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ReplacementValue_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_ReplacementValue_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ReplacementValue_ReplacementValue_LookUp] FOREIGN KEY ([ReplacementValueId]) REFERENCES [dim].[ReplacementValue_LookUp] ([ReplacementValueId]),
    CONSTRAINT [FK_calc_ReplacementValue_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ReplacementValue_u
	ON  calc.ReplacementValue
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ReplacementValue
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ReplacementValue.FactorSetId			= INSERTED.FactorSetId
		AND calc.ReplacementValue.Refnum				= INSERTED.Refnum
		AND calc.ReplacementValue.CalDateKey			= INSERTED.CalDateKey
		AND calc.ReplacementValue.CurrencyRpt			= INSERTED.CurrencyRpt
		AND calc.ReplacementValue.ReplacementValueId	= INSERTED.ReplacementValueId;
			
END