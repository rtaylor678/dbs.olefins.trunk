﻿CREATE TABLE [calc].[CapacityProductionRatio] (
    [FactorSetId]              VARCHAR (12)       NOT NULL,
    [Refnum]                   VARCHAR (25)       NOT NULL,
    [CalDateKey]               INT                NOT NULL,
    [Ethylene_kMT]             REAL               NOT NULL,
    [Propylene_kMT]            REAL               NULL,
    [OLEProd_kMT]              REAL               NOT NULL,
    [HVC_kMT]                  REAL               NULL,
    [FreshPyro_kMT]            REAL               NULL,
    [FreshSupp_kMT]            REAL               NULL,
    [_EthylenePropylene_Ratio] AS                 (CONVERT([real],case when [Propylene_kMT]<>(0.0) then [Ethylene_kMT]/[Propylene_kMT]  end,(1))) PERSISTED,
    [_PropyleneEthylene_Ratio] AS                 (CONVERT([real],case when [Ethylene_kMT]<>(0.0) then [Propylene_kMT]/[Ethylene_kMT]  end,(1))) PERSISTED,
    [_EthyleneOLEProd_Ratio]   AS                 (CONVERT([real],case when [OLEProd_kMT]<>(0.0) then [Ethylene_kMT]/[OLEProd_kMT]  end,(1))) PERSISTED,
    [_OLEProdEthylene_Ratio]   AS                 (CONVERT([real],case when [Ethylene_kMT]<>(0.0) then [OLEProd_kMT]/[Ethylene_kMT]  end,(1))) PERSISTED,
    [_EthyleneFresh_Ratio]     AS                 (CONVERT([real],case when [FreshSupp_kMT]<>(0.0) then [Ethylene_kMT]/[FreshSupp_kMT]  end,(1))) PERSISTED,
    [_FreshPyroEthylene_Ratio] AS                 (CONVERT([real],case when [Ethylene_kMT]<>(0.0) then [FreshPyro_kMT]/[Ethylene_kMT]  end,(1))) PERSISTED,
    [_EthyleneFreshSupp_Ratio] AS                 (CONVERT([real],case when [FreshSupp_kMT]<>(0.0) then [Ethylene_kMT]/[FreshSupp_kMT]  end,(1))) PERSISTED,
    [_FreshSuppEthylene_Ratio] AS                 (CONVERT([real],case when [Ethylene_kMT]<>(0.0) then [FreshSupp_kMT]/[Ethylene_kMT]  end,(1))) PERSISTED,
    [EthyleneLoss_kMT]         REAL               NULL,
    [_PropyleneLoss_kMT]       AS                 (CONVERT([real],case when [Propylene_kMT]<>(0.0) then ([Propylene_kMT]/[Ethylene_kMT])*[EthyleneLoss_kMT]  end,(1))) PERSISTED,
    [_OLEProdLoss_kMT]         AS                 (CONVERT([real],case when [Propylene_kMT]<>(0.0) then ([Propylene_kMT]/[Ethylene_kMT])*[EthyleneLoss_kMT]  end,(1))+isnull([EthyleneLoss_kMT],(0.0))) PERSISTED,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityProductionRatio_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_CapacityProductionRatio_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_CapacityProductionRatio_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_CapacityProductionRatio_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CapacityProductionRatio] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CapacityProductionRatio_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CapacityProductionRatio_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CapacityProductionRatio_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_CapacityProductionRatio_u
	ON  calc.CapacityProductionRatio
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.CapacityProductionRatio
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.CapacityProductionRatio.FactorSetId	= INSERTED.FactorSetId
		AND calc.CapacityProductionRatio.Refnum			= INSERTED.Refnum
		AND calc.CapacityProductionRatio.CalDateKey		= INSERTED.CalDateKey;
				
END