﻿CREATE PROCEDURE [calc].[Insert_PlantRecycles]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PlantRecycles([FactorSetId], [Refnum], [CalDateKey], [RecycleId])
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey			[CalDateKey],
			ISNULL(SUM(ro.RecycleId), 0)	[RecycleId]
		FROM @fpl							fpl
		LEFT OUTER JOIN (
			SELECT
				q.Refnum,
				c.CalDateKey,
				q.StreamId
			FROM fact.Quantity					q
			INNER JOIN fact.CompositionQuantity	c
				ON	c.Refnum = q.Refnum
				AND	c.StreamId = q.StreamId
			WHERE	q.StreamId IN ('ButRec', 'EthRec', 'ProRec')
				AND	q.Quantity_kMT > 0.0
				AND	c.Component_WtPcnt > 0.0
			GROUP BY
				q.Refnum,
				c.CalDateKey,
				q.StreamId
			HAVING 	SUM(q.Quantity_kMT * c.Component_WtPcnt) > 0.0
				AND	ABS(100.0 * COUNT(DISTINCT q.Quantity_kMT) - SUM(c.Component_WtPcnt)) / 100.0 / COUNT(DISTINCT q.Quantity_kMT) <= 10.0
			)	q
			ON	q.Refnum = fpl.Refnum
			AND	q.CalDateKey = fpl.Plant_QtrDateKey
		LEFT OUTER JOIN ante.RecycleOperation		ro
			ON	ro.FactorSetId = fpl.FactorSetId
			AND	ro.StreamId = q.StreamId
		WHERE	fpl.CalQtr = 4
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;