﻿CREATE PROCEDURE calc.Insert_PersAggregateAdj
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY
		
		INSERT INTO calc.PersAggregateAdj(FactorSetId, Refnum, CalDateKey, PersId,
			Personnel_Count, StraightTime_Hrs, OverTime_Hrs, Company_Hrs, Contract_Hrs, Tot_Hrs, Company_Fte, Contract_Fte, Tot_Fte,
			InflTaAdjAnn_Company_Hrs, InflTaAdjAnn_Contract_Hrs, InflTaAdjAnn_Tot_Hrs, InflTaAdjAnn_Company_Fte, InflTaAdjAnn_Contract_Fte, InflTaAdjAnn_Tot_Fte,
			WorkWeek_Hrs,
			Contract_InflTaAdjAnnPcnt
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			b.PersId,

			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p.Personnel_Count
				WHEN '-' THEN - p.Personnel_Count
				END)																[Personnel_Count],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p.StraightTime_Hrs
				WHEN '-' THEN - p.StraightTime_Hrs
				END)																[StraightTime_Hrs],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p.OverTime_Hrs
				WHEN '-' THEN - p.OverTime_Hrs
				END)																[OverTime_Hrs],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p._Company_Hrs
				WHEN '-' THEN - p._Company_Hrs
				END)																[Company_Hrs],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p.Contract_Hrs
				WHEN '-' THEN - p.Contract_Hrs
				END)																[Contract_Hrs],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p._Tot_Hrs
				WHEN '-' THEN - p._Tot_Hrs
				END)																[Tot_Hrs],

			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p._Company_Fte
				WHEN '-' THEN - p._Company_Fte
				END)																[Company_Fte],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p._Contract_Fte
				WHEN '-' THEN - p._Contract_Fte
				END)																[Contract_Fte],
			
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + p._Tot_Fte
				WHEN '-' THEN - p._Tot_Fte
				END)																[Tot_Fte],

			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(m.InflAdjAnn_Company_Hrs, p._Company_Hrs)
				WHEN '-' THEN - ISNULL(m.InflAdjAnn_Company_Hrs, p._Company_Hrs)
				END)																[InflTaAdjAnn_Company_Hrs],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(m.InflAdjAnn_Contract_Hrs, p.Contract_Hrs)
				WHEN '-' THEN - ISNULL(m.InflAdjAnn_Contract_Hrs, p.Contract_Hrs)
				END)																[InflTaAdjAnn_Contract_Hrs],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(m._InflAdjAnn_Tot_Hrs, p._Tot_Hrs)
				WHEN '-' THEN -	ISNULL(m._InflAdjAnn_Tot_Hrs, p._Tot_Hrs)
				END)																[InflTaAdjAnn_Tot_Hrs],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(m._InflAdjAnn_Company_Fte, p._Company_Fte)
				WHEN '-' THEN - ISNULL(m._InflAdjAnn_Company_Fte, p._Company_Fte)
				END)																[InflTaAdjAnn_Company_Fte],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(m._InflAdjAnn_Contract_Fte, p._Contract_Fte)
				WHEN '-' THEN - ISNULL(m._InflAdjAnn_Contract_Fte, p._Contract_Fte)
				END)																[InflTaAdjAnn_Contract_Fte],
			SUM(
				CASE b.DescendantOperator
				WHEN '+' THEN + ISNULL(m._InflAdjAnn_Tot_Fte, p._Tot_Fte)
				WHEN '-' THEN - ISNULL(m._InflAdjAnn_Tot_Fte, p._Tot_Fte)
				END)																[InflTaAdjAnn_Tot_Fte],
			
			CASE WHEN
					SUM(
					CASE b.DescendantOperator
					WHEN '+' THEN + p.Personnel_Count
					WHEN '-' THEN - p.Personnel_Count
					END) <> 0.0
				THEN
					SUM(
					CASE b.DescendantOperator
					WHEN '+' THEN + ISNULL(m.InflAdjAnn_Company_Hrs, p._Company_Hrs)
					WHEN '-' THEN - ISNULL(m.InflAdjAnn_Company_Hrs, p._Company_Hrs)
					END)
					/
					SUM(
					CASE b.DescendantOperator
					WHEN '+' THEN + p.Personnel_Count
					WHEN '-' THEN - p.Personnel_Count
					END)
					/
					52.0
				END																	[WorkWeek_Hrs],
			
			CASE WHEN
					SUM(
					CASE b.DescendantOperator
					WHEN '+' THEN + ISNULL(m._InflAdjAnn_Tot_Fte, p._Tot_Fte)
					WHEN '-' THEN - ISNULL(m._InflAdjAnn_Tot_Fte, p._Tot_Fte)
					END) <> 0.0
				THEN
					SUM(
					CASE b.DescendantOperator
					WHEN '+' THEN + ISNULL(m._InflAdjAnn_Contract_Fte, p._Contract_Fte)
					WHEN '-' THEN - ISNULL(m._InflAdjAnn_Contract_Fte, p._Contract_Fte)
					END)
					/
					SUM(
					CASE b.DescendantOperator
					WHEN '+' THEN + ISNULL(m._InflAdjAnn_Tot_Fte, p._Tot_Fte)
					WHEN '-' THEN - ISNULL(m._InflAdjAnn_Tot_Fte, p._Tot_Fte)
					END)
				END	* 100.0															[Contract_InflTaAdjAnnPcnt]

		FROM @fpl											tsq
		--	Task 141: Implement Tri-Tables in calculations
		--CROSS JOIN dim.PersLuDescendants(NULL, 1, ',')	b
		INNER JOIN dim.Pers_Bridge							b
			ON	b.FactorSetId = tsq.FactorSetId
		LEFT OUTER JOIN fact.Pers							p
			ON	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
			AND	p.PersId = b.DescendantId

		LEFT OUTER JOIN calc.PersMaintTa					m
			ON	m.FactorSetId = tsq.FactorSetId
			AND	m.Refnum = tsq.Refnum
			AND	m.CalDateKey = tsq.Plant_QtrDateKey
			AND	m.PersId = b.DescendantId

		WHERE	tsq.CalQtr = 4

		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			b.PersId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;