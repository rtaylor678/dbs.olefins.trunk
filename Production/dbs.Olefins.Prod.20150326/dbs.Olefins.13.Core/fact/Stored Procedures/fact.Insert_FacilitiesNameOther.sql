﻿CREATE PROCEDURE [fact].[Insert_FacilitiesNameOther]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesNameOther(Refnum, CalDateKey, FacilityId, FacilityName)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'VesselOth1'
			, f.OthName1
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE f.OthCnt1 > 0 AND f.OthName1 IS NOT NULL
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

		INSERT INTO fact.FacilitiesNameOther(Refnum, CalDateKey, FacilityId, FacilityName)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'VesselOth2'
			, f.OthName2
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE f.OthCnt2 > 0 AND f.OthName2 IS NOT NULL
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

		INSERT INTO fact.FacilitiesNameOther(Refnum, CalDateKey, FacilityId, FacilityName)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'VesselOth3'
			, f.OthName3
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE f.OthCnt3 > 0 AND f.OthName3 IS NOT NULL
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;