﻿CREATE TABLE [fact].[EnergySteam] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [Amount_kMT]     REAL               NOT NULL,
    [Pressure_Barg]  REAL               NOT NULL,
    [Temp_C]         REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EnergySteam_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EnergySteam_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EnergySteam_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EnergySteam_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergySteam] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergySteam_Amount_kMT] CHECK ([Amount_kMT]>=(0.0)),
    CONSTRAINT [CR_EnergySteam_Pressure_Barg] CHECK ([Pressure_Barg]>=(0.0)),
    CONSTRAINT [CR_EnergySteam_Temp_C] CHECK ([Temp_C]>=(0.0)),
    CONSTRAINT [FK_EnergySteam_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_EnergySteam_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergySteam_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergySteam_u]
	ON [fact].[EnergySteam]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergySteam]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergySteam].Refnum		= INSERTED.Refnum
		AND [fact].[EnergySteam].AccountId	= INSERTED.AccountId
		AND [fact].[EnergySteam].CalDateKey	= INSERTED.CalDateKey;

END;