﻿CREATE VIEW fact.MaintAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	m.CurrencyRpt,
	b.FacilityId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + m.MaintMaterial_Cur
		WHEN '-' THEN - m.MaintMaterial_Cur
		END)								[MaintMaterial_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + m.MaintLabor_Cur
		WHEN '-' THEN - m.MaintLabor_Cur
		END)								[MaintLabor_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + (ISNULL(m.MaintMaterial_Cur, 0.0) + ISNULL(m.MaintLabor_Cur, 0.0))
		WHEN '-' THEN - (ISNULL(m.MaintMaterial_Cur, 0.0) + ISNULL(m.MaintLabor_Cur, 0.0))
		END)								[Maint_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + m.TaMaterial_Cur
		WHEN '-' THEN - m.TaMaterial_Cur
		END)								[TaMaterial_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + m.TaLabor_Cur
		WHEN '-' THEN - m.TaLabor_Cur
		END)								[TaLabor_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + (ISNULL(m.TaMaterial_Cur, 0.0) + ISNULL(m.TaLabor_Cur, 0.0))
		WHEN '-' THEN - (ISNULL(m.TaMaterial_Cur, 0.0) + ISNULL(m.TaLabor_Cur, 0.0))
		END)								[TA_Cur],
		
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + (ISNULL(m.MaintMaterial_Cur, 0.0) + ISNULL(m.TaMaterial_Cur, 0.0))
		WHEN '-' THEN - (ISNULL(m.MaintMaterial_Cur, 0.0) + ISNULL(m.TaMaterial_Cur, 0.0))
		END)								[TotMaterial_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + (ISNULL(m.MaintLabor_Cur, 0.0) + ISNULL(m.TaLabor_Cur, 0.0))
		WHEN '-' THEN - (ISNULL(m.MaintLabor_Cur, 0.0) + ISNULL(m.TaLabor_Cur, 0.0))
		END)								[TotLabor_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + (ISNULL(m.MaintMaterial_Cur, 0.0) + ISNULL(m.MaintLabor_Cur, 0.0) + ISNULL(m.TaMaterial_Cur, 0.0) + ISNULL(m.TaLabor_Cur, 0.0))
		WHEN '-' THEN - (ISNULL(m.MaintMaterial_Cur, 0.0) + ISNULL(m.MaintLabor_Cur, 0.0) + ISNULL(m.TaMaterial_Cur, 0.0) + ISNULL(m.TaLabor_Cur, 0.0))
		END)								[Tot_Cur]

FROM dim.Facility_Bridge	b
INNER JOIN fact.Maint		m
	ON	m.FacilityId = b.DescendantId
GROUP BY
	b.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	m.CurrencyRpt,
	b.FacilityId;