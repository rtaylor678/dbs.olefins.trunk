﻿CREATE TABLE [ante].[C2YieldPercent] (
    [FactorSetId]        VARCHAR (12)       NOT NULL,
    [ComponentId]        VARCHAR (42)       NOT NULL,
    [YieldEstimate_Pcnt] REAL               NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_C2YieldPercent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_C2YieldPercent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_C2YieldPercent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_C2YieldPercent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_C2YieldPercent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_C2YieldPercent_YieldEstimate] CHECK ([YieldEstimate_Pcnt]>=(0.0) AND [YieldEstimate_Pcnt]<=(100.0)),
    CONSTRAINT [FK_C2YieldPercent_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_C2YieldPercent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_C2YieldPercent_u]
	ON [ante].[C2YieldPercent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].C2YieldPercent
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[C2YieldPercent].FactorSetId		= INSERTED.FactorSetId
		AND	[ante].[C2YieldPercent].ComponentId		= INSERTED.ComponentId;

END;