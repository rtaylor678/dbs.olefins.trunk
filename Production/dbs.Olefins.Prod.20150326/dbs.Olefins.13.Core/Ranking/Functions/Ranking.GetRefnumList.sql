﻿CREATE FUNCTION [Ranking].[GetRefnumList](@ListId varchar(42), @RankBreak sysname, @BreakValue varchar(12))
RETURNS @refnums TABLE (Refnum varchar(18) NOT NULL)
AS
BEGIN
	IF @RankBreak = 'TotalList'
		INSERT @refnums
		SELECT DISTINCT Refnum
		FROM cons.RefList
		WHERE ListId = @ListId
	ELSE IF @RankBreak = 'UserGroup'
		INSERT @refnums
		SELECT Refnum
		FROM cons.RefList
		WHERE ListId = @ListId AND UserGroup = @BreakValue
	ELSE
		INSERT @refnums
		SELECT Refnum 
		FROM Ranking.RefineryBreakValues
		WHERE RankBreak = @RankBreak AND BreakValue = @BreakValue AND Refnum IN (SELECT Refnum FROM cons.RefList WHERE ListId = @ListId)

	RETURN
END
