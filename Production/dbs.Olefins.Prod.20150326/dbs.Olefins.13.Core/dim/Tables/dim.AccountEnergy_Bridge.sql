﻿CREATE TABLE [dim].[AccountEnergy_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [AccountId]          VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_AccountEnergy_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_AccountEnergy_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_AccountEnergy_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_AccountEnergy_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_AccountEnergy_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_AccountEnergy_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] DESC, [AccountId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_AccountEnergy_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_AccountEnergy_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_AccountEnergy_Bridge_FactorSetLu] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_AccountEnergy_Bridge_LookUp_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_AccountEnergy_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [AccountId]) REFERENCES [dim].[AccountEnergy_Parent] ([FactorSetId], [AccountId]),
    CONSTRAINT [FK_AccountEnergy_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[AccountEnergy_Parent] ([FactorSetId], [AccountId])
);


GO

CREATE TRIGGER [dim].[t_AccountEnergy_Bridge_u]
ON [dim].[AccountEnergy_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[AccountEnergy_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[AccountEnergy_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[AccountEnergy_Bridge].[AccountId]		= INSERTED.[AccountId]
		AND	[dim].[AccountEnergy_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
