﻿CREATE TABLE [dim].[ReliabilityCompComponent_LookUp] (
    [CompComponentId]     VARCHAR (42)       NOT NULL,
    [CompComponentName]   NVARCHAR (84)      NOT NULL,
    [CompComponentDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityCompComponent_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompComponent_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompComponent_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompComponent_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]        ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReliabilityCompComponent_LookUp] PRIMARY KEY CLUSTERED ([CompComponentId] ASC),
    CONSTRAINT [CL_ReliabilityCompComponent_LookUp_CompComponentDetail] CHECK ([CompComponentDetail]<>''),
    CONSTRAINT [CL_ReliabilityCompComponent_LookUp_CompComponentId] CHECK ([CompComponentId]<>''),
    CONSTRAINT [CL_ReliabilityCompComponent_LookUp_CompComponentName] CHECK ([CompComponentName]<>''),
    CONSTRAINT [UK_ReliabilityCompComponent_LookUp_CompComponentDetail] UNIQUE NONCLUSTERED ([CompComponentDetail] ASC),
    CONSTRAINT [UK_ReliabilityCompComponent_LookUp_CompComponentName] UNIQUE NONCLUSTERED ([CompComponentName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReliabilityCompComponent_LookUp_u]
	ON [dim].[ReliabilityCompComponent_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReliabilityCompComponent_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityCompComponent_LookUp].[CompComponentId]		= INSERTED.[CompComponentId];
		
END;
