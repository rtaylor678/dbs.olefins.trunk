﻿CREATE TABLE [inter].[ReplValPyroCapacity] (
    [FactorSetId]         VARCHAR (12)       NOT NULL,
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [CurrencyRpt]         VARCHAR (4)        NOT NULL,
    [PyroEthyleneCap_kMT] REAL               NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_ReplValPyroCapacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroCapacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroCapacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroCapacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReplValPyroCapacity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReplValPyroCapacity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReplValPyroCapacity_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_ReplValPyroCapacity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER inter.t_ReplValPyroCapacity_u
	ON inter.ReplValPyroCapacity
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.ReplValPyroCapacity
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.ReplValPyroCapacity.FactorSetId	= INSERTED.FactorSetId
		AND inter.ReplValPyroCapacity.Refnum			= INSERTED.Refnum
		AND inter.ReplValPyroCapacity.CalDateKey		= INSERTED.CurrencyRpt
		AND inter.ReplValPyroCapacity.CalDateKey		= INSERTED.CalDateKey;

END;