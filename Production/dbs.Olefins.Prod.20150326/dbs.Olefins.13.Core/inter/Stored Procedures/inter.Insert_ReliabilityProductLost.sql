﻿CREATE PROCEDURE inter.Insert_ReliabilityProductLost
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO inter.ReliabilityProductLost(FactorSetId, Refnum, CalDateKey, StreamId, SchedId, Loss_MTYr)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			cp.StreamId,
			tac.SchedId,
			ISNULL(rl.TotLoss_MT, 0.0) +
			(SUM(tac._Ann_FullPlantEquivalentDT_HrsYr) * cp._StreamAvg_MTd / 24.0) +
			CASE WHEN ta.Interval_Years <> 0.0
				THEN calc.MaxValue(ISNULL(lc.SlowDownLoss_MT, 0.0), ISNULL(lp.SlowDownLoss_MT, 0.0)) / ta.Interval_Years
				ELSE 0.0
				END
		FROM @fpl											fpl

		LEFT OUTER JOIN calc.[ReliabilityOpLossAnnualizedAggregate]	rl	WITH (NOEXPAND)
			ON	rl.FactorSetId = fpl.FactorSetId
			AND	rl.Refnum = fpl.Refnum
			AND	rl.OppLossId = 'UnPlanned'
	
		INNER JOIN calc.TaAdjCapacity						tac
			ON	tac.FactorSetId = fpl.FactorSetId
			AND	tac.Refnum = fpl.Refnum
			AND	tac.CalDateKey = fpl.Plant_QtrDateKey
			AND tac.SchedId = 'A'
			AND	tac.StreamId = 'Ethylene'
		INNER JOIN calc.CapacityPlant						cp
			ON	cp.FactorSetId = fpl.FactorSetId
			AND cp.Refnum = fpl.Refnum
			AND cp.CalDateKey = fpl.Plant_QtrDateKey
			AND	cp.StreamId = tac.StreamId

		LEFT OUTER JOIN calc.TaAdjCapacityAggregate			ta
			ON	ta.FactorSetId = fpl.FactorSetId
			AND	ta.Refnum = fpl.Refnum
			AND	ta.CalDateKey = fpl.Plant_QtrDateKey
			AND	ta.CurrencyRpt = fpl.CurrencyRpt
			AND	ta.SchedId = tac.SchedId
			AND	ta.StreamId = tac.StreamId
		LEFT OUTER JOIN fact.ReliabilityOppLoss				lc
			ON	lc.Refnum = fpl.Refnum
			AND	lc.CalDateKey = fpl.Plant_QtrDateKey
			AND	lc.StreamId = tac.StreamId
			AND	lc.OppLossId = 'TurnAround'
		LEFT OUTER JOIN fact.ReliabilityOppLoss				lp
			ON	lp.Refnum = fpl.Refnum
			AND	lp.CalDateKey = fpl.Plant_QtrDateKey - 10000
			AND	lp.StreamId = tac.StreamId
			AND	lp.OppLossId = 'TurnAround'

		WHERE	fpl.CalQtr = 4

		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			cp.StreamId,
			cp._StreamAvg_MTd,
			tac.SchedId,
			rl.TotLoss_MT,
			ta.Interval_Years,
			lc.SlowDownLoss_MT,
			lp.SlowDownLoss_MT;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;