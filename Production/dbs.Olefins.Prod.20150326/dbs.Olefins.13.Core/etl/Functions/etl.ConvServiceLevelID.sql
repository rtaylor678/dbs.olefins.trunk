﻿CREATE FUNCTION [etl].[ConvServiceLevelID]
(
	@ServiceLevelID	NVARCHAR(8)
)
RETURNS NVARCHAR(14)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(14) =
	CASE UPPER(LEFT(@ServiceLevelID, 1))
		WHEN 'O'	THEN 'Outage'
		WHEN 'S'	THEN 'SlowDown'
		WHEN 'D'	THEN 'DownTime'
	END
		
	RETURN @ResultVar;
	
END