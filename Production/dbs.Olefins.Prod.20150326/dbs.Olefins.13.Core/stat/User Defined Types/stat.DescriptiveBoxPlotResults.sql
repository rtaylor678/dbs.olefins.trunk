﻿CREATE TYPE [stat].[DescriptiveBoxPlotResults] AS TABLE (
    [xCount]     SMALLINT   NOT NULL,
    [_BoxLowerE] AS         (CONVERT([float],[BoxLower]-([BoxUpper]-[BoxLower])*(3.0),0)) PERSISTED NOT NULL,
    [_BoxLowerM] AS         (CONVERT([float],[BoxLower]-([BoxUpper]-[BoxLower])*(1.5),0)) PERSISTED NOT NULL,
    [BoxLower]   FLOAT (53) NOT NULL,
    [BoxMedian]  FLOAT (53) NOT NULL,
    [BoxUpper]   FLOAT (53) NOT NULL,
    [_BoxUpperM] AS         (CONVERT([float],[BoxUpper]+([BoxUpper]-[BoxLower])*(1.5),0)) PERSISTED NOT NULL,
    [_BoxUpperE] AS         (CONVERT([float],[BoxUpper]+([BoxUpper]-[BoxLower])*(3.0),0)) PERSISTED NOT NULL,
    [_BoxIMR]    AS         (CONVERT([float],[BoxUpper]-[BoxLower],0)) PERSISTED NOT NULL,
    [_BoxWidth]  AS         (CONVERT([float],sqrt([xCount]),0)) PERSISTED NOT NULL,
    [_BoxNotch]  AS         (CONVERT([float],([BoxUpper]-[BoxLower])/sqrt([xCount]),0)) PERSISTED NOT NULL,
    CHECK ([xCount]>=(1)),
    CHECK ([_BoxIMR]>=(0.0)),
    CHECK ([BoxLower]<=[BoxUpper]),
    CHECK ([BoxMedian]>=[BoxLower] AND [BoxMedian]<=[BoxUpper]));

