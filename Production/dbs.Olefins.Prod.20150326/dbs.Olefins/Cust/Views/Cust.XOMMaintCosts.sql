﻿






/*

This view is for long term trends for the XOM custon Followup work

DBB 14Aug14
*/

CREATE view [Cust].[XOMMaintCosts] AS
Select Refnum = LEFT(t.StudyYear, 2) + Rtrim(t.Refnum), 
t.StudyYear,
ByProdBasis,
DivFactor, 
(r.TotReplVal *1000 * MaintCostIndex * (CASE WHEN t.StudyYear <=1993 THEN 100 ELSE 1 END) / 100 / DivFactor) as TotMaintCost,
(r.TotReplVal *1000 * MaintCostIndex  * (CASE WHEN t.StudyYear<=1993 THEN 100 ELSE 1 END) / 100 - f.AnnTAExp) / DivFactor as RoutMaintCost,
 f.AnnTAExp / DivFactor as AnnTAMaintCost
FROM  [$(DbOlefinsLegacy)].dbo.ByProd b INNER JOIN  [$(DbOlefinsLegacy)].dbo.FinancialTot f on f.Refnum=b.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.MaintInd m on m.Refnum=f.Refnum
INNER JOIN  [$(DbOlefinsLegacy)].dbo.Replacement r on r.Refnum=f.Refnum
INNER JOIN  [$(DbOlefinsLegacy)].dbo.TSort t on t.Refnum=f.Refnum  
where t.StudyYear < 2007

UNION

Select o.Refnum, 
g.StudyYear, 
DataType = CASE d.DataType WHEN 'ETHDiv_MT' THEN 'C2' WHEN 'OLEDiv_MT' THEN 'OLE' WHEN 'HVC_MT' THEN 'HVC' ELSE d.DataType END, 
d.DivisorValue, 
TotMaintCost = m.MaintIndexEDC * g.kEdc / d.DivisorValue,
RoutMaintCost = m.RoutIndexEDC * g.kEdc / d.DivisorValue,
AnnTAMaintCost = m.TAIndexEDC * g.kEdc / d.DivisorValue
from [$(DatabaseName)].dbo.OpEx o JOIN [$(DatabaseName)].dbo.GENSUM g on g.Refnum=o.Refnum and o.DataType = 'ADJ'
JOIN [$(DatabaseName)].dbo.Maint m on m.Refnum=g.Refnum
Join [$(DatabaseName)].dbo.DivisorsUnPivot d on d.Refnum=o.Refnum and d.DataType in ('ETHDiv_MT','OLEDiv_MT','HVC_MT', 'EDC', 'UEDC', 'OLECpby_MT')
where g.StudyYear >= 2007

--Select * from Olefins.dbo.Maint where Refnum = '2011PCH115'
--Select * from cust.XOMMaintCosts where Refnum = '2011PCH115'









