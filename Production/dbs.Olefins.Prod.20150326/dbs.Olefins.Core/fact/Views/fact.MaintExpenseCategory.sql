﻿CREATE VIEW [fact].[MaintExpenseCategory]
WITH SCHEMABINDING
AS
SELECT
	ma.FactorSetId,
	ma.Refnum,
	ma.CalDateKey,
	ma.CurrencyRpt,
	me.AccountId,
	ma.Maint_Cur * me.CompMaintHours_Pcnt / 100.0	[MaintExp_Cur]
FROM fact.MaintAggregate			ma
INNER JOIN fact.MaintExpense		me
	ON	me.Refnum = ma.Refnum
	AND	me.CalDateKey = ma.CalDateKey
WHERE	ma.FacilityId = 'TotUnitCnt';
