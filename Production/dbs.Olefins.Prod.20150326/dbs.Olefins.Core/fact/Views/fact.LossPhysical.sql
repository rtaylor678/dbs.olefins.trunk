﻿CREATE VIEW [fact].[LossPhysical]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	SUM(l.Quantity_kMT) / p.Quantity_kMT * 100.0 [ProductLoss_Pcnt]
FROM fact.StreamQuantityAggregate			p	WITH (NOEXPAND)
INNER JOIN fact.StreamQuantityAggregate		l	WITH (NOEXPAND)
	ON	l.FactorSetId = p.FactorSetId
	AND	l.Refnum = p.Refnum
	AND	l.StreamId IN ('LossFlareVent' ,'LossOther')
WHERE p.StreamId = 'ProdLoss'
	AND	p.Quantity_kMT <> 0.0
GROUP BY
	p.FactorSetId,
	p.Refnum,
	p.Quantity_kMT;