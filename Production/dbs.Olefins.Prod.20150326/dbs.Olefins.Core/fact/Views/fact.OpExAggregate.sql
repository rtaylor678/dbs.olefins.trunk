﻿CREATE VIEW [fact].[OpExAggregate]
WITH SCHEMABINDING
AS
SELECT
	b.[FactorSetId],
	o.[Refnum],
	o.[CalDateKey],
	o.[CurrencyRpt],
	b.[AccountId],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + o.[Amount_Cur]
		WHEN '-' THEN - o.[Amount_Cur]
		ELSE 0.0
		END)							[Amount_Cur],
	COUNT_BIG(*)						[IndexItems]
FROM [dim].[Account_Bridge]		b
INNER JOIN [fact].[OpEx]		o
	ON	o.[AccountId] = b.[DescendantId]
GROUP BY
	b.[FactorSetId],
	o.[Refnum],
	o.[CalDateKey],
	o.[CurrencyRpt],
	b.[AccountId];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_fact_OpExAggregate]
    ON [fact].[OpExAggregate]([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC);

