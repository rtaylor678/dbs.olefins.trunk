﻿CREATE PROCEDURE [fact].[Insert_GenPlantDuties]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantDuties(Refnum, CalDateKey, AccountId, StreamId, CurrencyRpt, Amount_Cur)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'DutiesImport'
			, etl.ConvStreamID(
					CASE d.FeedProdID
					WHEN 'GasOil'		THEN 'LiqHeavy'
					WHEN 'FRNaphtha'	THEN 'Naphtha'
					ELSE --'FeedOther'
						CASE WHEN (d.FeedProdID = 'HEAVY FEEDS' AND d.Refnum = '01PCH013') OR d.FeedProdID = '0'
						THEN 'FeedOther' 
						ELSE d.FeedProdID
						END
					END
					) 
			, 'USD'
			, d.ImpTx
		FROM stgFact.Duties d
		INNER JOIN stgFact.TSort t ON t.Refnum = d.Refnum
		WHERE	d.ImpTx IS NOT NULL
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;