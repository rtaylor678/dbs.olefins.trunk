﻿CREATE TYPE [Ranking].[RankData] AS TABLE (
    [Refnum]     VARCHAR (25) NOT NULL,
    [RankValue]  REAL         NOT NULL,
    [PcntFactor] REAL         NOT NULL,
    PRIMARY KEY CLUSTERED ([Refnum] ASC));

