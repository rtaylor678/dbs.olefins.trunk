﻿CREATE TABLE [ante].[MapFeedClass] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [PeerGroup]        TINYINT            NOT NULL,
    [StreamId]         VARCHAR (42)       NOT NULL,
    [EdcId]            VARCHAR (42)       NOT NULL,
    [SuppEdcId]        VARCHAR (42)       NOT NULL,
    [UnitId]           VARCHAR (42)       NOT NULL,
    [CompUnitId]       VARCHAR (42)       NOT NULL,
    [SuppUnitId]       VARCHAR (42)       NOT NULL,
    [FeedFracStreamId] VARCHAR (42)       NOT NULL,
    [Energy_kBtuBBL]   FLOAT (53)         NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_MapFeedClass_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_MapFeedClass_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_MapFeedClass_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_MapFeedClass_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MapFeedClass] PRIMARY KEY CLUSTERED ([FactorSetId] DESC, [PeerGroup] ASC),
    CONSTRAINT [CR_MapFeedClass_PeerGroup] CHECK ([PeerGroup]=(5) OR [PeerGroup]=(4) OR [PeerGroup]=(3) OR [PeerGroup]=(2) OR [PeerGroup]=(1)),
    CONSTRAINT [FK_MapFeedClass_CompUnit_LookUp] FOREIGN KEY ([CompUnitId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId]),
    CONSTRAINT [FK_MapFeedClass_Edc_LookUp] FOREIGN KEY ([EdcId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_MapFeedClass_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MapFeedClass_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_MapFeedClass_SuppEdc_LookUp] FOREIGN KEY ([SuppEdcId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_MapFeedClass_SuppUnit_LookUp] FOREIGN KEY ([SuppUnitId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId]),
    CONSTRAINT [FK_MapFeedClass_Unit_LookUp] FOREIGN KEY ([UnitId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId])
);


GO

CREATE TRIGGER [ante].[t_MapFeedClass_u]
	ON [ante].[MapFeedClass]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFeedClass]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[MapFeedClass].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[MapFeedClass].[PeerGroup]	= INSERTED.[PeerGroup];

END;