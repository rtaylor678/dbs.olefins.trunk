﻿CREATE PROCEDURE [Console].[UpdateConsultingOpportunities]

	@RefNum varchar(18),
	@Issue text

AS
BEGIN
if exists(select * from Val.Notes where Refnum = @RefNum)
	UPDATE Val.Notes set ConsultingOpportunities = @Issue where Refnum = @refnum
	ELSE
	INSERT INTO val.Notes (RefNum, ConsultingOpportunities, Updated) Values(@RefNum,@Issue, GETDATE())

END
