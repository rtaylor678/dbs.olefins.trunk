﻿CREATE PROCEDURE [Console].[GetContactEmails] 
	@RefNum varchar(18)
AS
BEGIN
	SELECT 'Company' as [EmailType], FirstName + ' ' + LastName as [Name], cc.Email,cc.Password, JobTitle as Title
	FROM dbo.CoContactInfo cc INNER JOIN TSort t on cc.ContactCode = t.ContactCode and cc.StudyYear = t.StudyYear
	
	Where t.SmallRefnum = dbo.FormatRefNum(@RefNum,0)

	UNION

	SELECT 'Plant' as [EmailType], cc.NameFull as [Name], cc.email as Email,null, cc.nametitle as Title
	FROM dbo.ContactInfo cc INNER JOIN TSort t on cc.Refnum = t.Refnum 
	
	Where t.SmallRefnum = dbo.FormatRefNum(@RefNum,0) and ContactTypeId='COORD'

	UNION
	
	SELECT 'Plant' as [EmailType], cc.NameFull as [Name], cc.email as Email,null, cc.nametitle as Title
	FROM dbo.ContactInfo cc INNER JOIN TSort t on cc.Refnum = t.Refnum 
	
	Where t.SmallRefnum = dbo.FormatRefNum(@RefNum,0) and ContactTypeId='PRICINGCOORD'
	
	UNION 
	
	SELECT 'Plant' as [EmailType], cc.NameFull as [Name], cc.email as Email,null, cc.nametitle as Title
	FROM dbo.ContactInfo cc INNER JOIN TSort t on cc.Refnum = t.Refnum 
	
	Where t.SmallRefnum = dbo.FormatRefNum(@RefNum,0) and ContactTypeId='DATACOORD'
	
	
END
