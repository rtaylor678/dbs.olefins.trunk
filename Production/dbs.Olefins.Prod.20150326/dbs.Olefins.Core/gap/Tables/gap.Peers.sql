﻿CREATE TABLE [gap].[Peers] (
    [GroupId]        VARCHAR (25)       NOT NULL,
    [TargetId]       VARCHAR (25)       NOT NULL,
    [PeerNumber]     TINYINT            CONSTRAINT [DF_Peers_PeerNumber_0] DEFAULT ((0)) NOT NULL,
    [PeerDetail]     VARCHAR (348)      NULL,
    [DatabaseName]   NVARCHAR (128)     NOT NULL,
    [InOutPut]       BIT                CONSTRAINT [DF_Peers_InOutPut_1] DEFAULT ((1)) NOT NULL,
    [Title1]         VARCHAR (20)       NULL,
    [Title2]         VARCHAR (20)       NULL,
    [Title3]         VARCHAR (20)       NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Peers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Peers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Peers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Peers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Peers] PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC),
    CONSTRAINT [CK_Peers_GroupId_EmptyString] CHECK ([GroupId]<>''),
    CONSTRAINT [CK_Peers_PeerDetail_DatabaseName] CHECK ([DatabaseName]<>''),
    CONSTRAINT [CK_Peers_PeerDetail_EmptyString] CHECK ([PeerDetail]<>''),
    CONSTRAINT [CK_Peers_PeerDetail_Title1_EmptyString] CHECK ([Title1]<>''),
    CONSTRAINT [CK_Peers_PeerDetail_Title2_EmptyString] CHECK ([Title2]<>''),
    CONSTRAINT [CK_Peers_PeerDetail_Title3_EmptyString] CHECK ([Title3]<>''),
    CONSTRAINT [CK_Peers_TargetId_EmptyString] CHECK ([TargetId]<>'')
);


GO

CREATE TRIGGER [gap].[t_Peers_u]
    ON [gap].[Peers]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [gap].[Peers]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[gap].[Peers].[GroupId]		= INSERTED.[GroupId]
		AND [gap].[Peers].[TargetId]	= INSERTED.[TargetId];

END;