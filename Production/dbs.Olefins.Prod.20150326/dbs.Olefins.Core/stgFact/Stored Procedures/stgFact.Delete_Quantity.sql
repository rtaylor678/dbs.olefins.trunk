﻿CREATE PROCEDURE [stgFact].[Delete_Quantity]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Quantity]
	WHERE [Refnum] = @Refnum;

END;