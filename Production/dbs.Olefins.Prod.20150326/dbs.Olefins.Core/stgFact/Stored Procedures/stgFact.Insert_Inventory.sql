﻿CREATE PROCEDURE [stgFact].[Insert_Inventory]
(
	@Refnum		VARCHAR (25),
	@TankType	VARCHAR (2),

	@KMT		REAL	= NULL,
	@YEIL		REAL	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Inventory]([Refnum], [TankType], [KMT], [YEIL])
	VALUES(@Refnum, @TankType, @KMT, @YEIL);

END;