﻿CREATE PROCEDURE [stgFact].[Delete_MetaEnergy]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[MetaEnergy]
	WHERE [Refnum] = @Refnum;

END;