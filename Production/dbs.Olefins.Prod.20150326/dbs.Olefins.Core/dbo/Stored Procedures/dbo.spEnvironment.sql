﻿






CREATE              PROC [dbo].[spEnvironment](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON


DELETE FROM dbo.Environment WHERE Refnum = @Refnum
Insert into dbo.Environment (Refnum
, Emissions_MT			
, Emissions_PcntInput    
, Emissions_MTPerEDC   
, WasteHazMat_MT		
, WasteHazMat_PcntInput
, WasteHazMat_MTPerEDC  
, NOx_LbPerMBTU			
, NOX_MTperGJ			
, TotPyroFuel_MBTU		
, WasteWater_MTd		
, WasteWater_PerInput   
, WasteWater_MTPerEDC 
, EstDirectCO2E_kMT		
, EstTotCO2E_kMT		
, CeiDirect				
, CeiTotal				
, StdDirectCO2E_kMT	
, StdTotCO2E_kMT)

SELECT d.Refnum
, Emissions_MT			= g.Emissions_MTd
, Emissions_PcntInput   = g.Emissions_MTd/d.TotalPlantInput_kMT/1000.0 * 100.0
, Emissions_MTPerEDC    = g.Emissions_MTd/d.kEdc
, WasteHazMat_MT		= g.WasteHazMat_MTd
, WasteHazMat_PcntInput	= g.WasteHazMat_MTd/d.TotalPlantInput_kMT/1000.0 * 100.0
, WasteHazMat_MTPerEDC  = g.WasteHazMat_MTd/d.kEdc
, NOx_LbPerMBTU			= g.NOx_LbMBtu
, NOX_MTperGJ			= [$(DbGlobal)].dbo.UnitsConv(g.NOx_LbMBtu, 'LB/MBTU', 'MT/GJ')
, TotPyroFuel_MBTU		= ce.TotPyroFuelMBTU
, WasteWater_MTd		= g.WasteWater_MTd
, WasteWater_PerInput   = g.WasteWater_MTd/d.TotalPlantInput_kMT/1000.0 * d.NumDays
, WasteWater_MTPerEDC   = g.WasteWater_MTd/d.kEdc * d.NumDays 
, EstDirectCO2E_kMT		= edir.EmissionsCarbon_MTCO2e / 1000.0
, EstTotCO2E_kMT		= etot.EmissionsCarbon_MTCO2e / 1000.0
, CeiDirect				= cdir.CeiDirect
, CeiTotal				= ctot.CeiTotal
, StdDirectCO2E_kMT		= sdir.EmissionsCarbon_MTCO2e / 1000.0
, StdTotCO2E_kMT		= stot.EmissionsCarbon_MTCO2e / 1000.0
FROM dbo.Divisors d
JOIN calc.EmissionsCarbonPlantAggregate edir on edir.EmissionsId='DirectEmissions' and edir.Refnum=d.Refnum  
JOIN calc.EmissionsCarbonPlantAggregate etot on etot.EmissionsId='NetEmissions' and etot.Refnum=d.Refnum and etot.FactorSetId=edir.FactorSetId
JOIN calc.EmissionsCeiDirect cdir on cdir.Refnum=d.Refnum and cdir.FactorSetId=edir.FactorSetId
JOIN calc.EmissionsCeiTotal ctot on ctot.Refnum=d.Refnum and ctot.FactorSetId=edir.FactorSetId
JOIN calc.EmissionsCarbonDirectAggregate sdir on sdir.EmissionsId='TotalEmissions' and sdir.Refnum=d.Refnum and sdir.FactorSetId=edir.FactorSetId and sdir.SimModelId = 'PYPS'
JOIN calc.EmissionsCarbonStandardAggregate stot on stot.EmissionsId='TotalEmissions' and stot.Refnum=d.Refnum and stot.FactorSetId=edir.FactorSetId and stot.SimModelId = 'PYPS'
JOIN dbo.EnergyCalc e on e.Refnum=d.Refnum and e.DataType = 'MBTU' and e.AccountId = 'EnergyBalance'
JOIN (Select Refnum, FactorSetId, TotPyroFuelMBTU = SUM(Quantity_MBtu) FROM calc.EmissionsCarbonPlant Where EmissionsId in ('CO2PPFCFuelGas','CO2PurFuelNatural') GROUP BY Refnum, FactorSetId) ce
	on ce.Refnum=d.Refnum and ce.FactorSetId = edir.FactorSetId
LEFT JOIN fact.GenPlantEnvironment g on g.Refnum=d.Refnum
WHERE edir.FactorSetId = @FactorSetId and d.Refnum = @Refnum
SET NOCOUNT OFF

















