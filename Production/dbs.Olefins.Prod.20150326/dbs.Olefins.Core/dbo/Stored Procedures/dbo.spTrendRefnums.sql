﻿
CREATE PROCEDURE [dbo].[spTrendRefnums](@Refnum varchar(25))
AS
DECLARE @RefnumP1 varchar(25), @RefnumP2 varchar(25), @RefnumP3 varchar(25)
DECLARE @SYP1 int, @SYP2 int, @SYP3 int
DECLARE @AssetId varchar(3), @StudyYear int, @StudyId varchar(4)
SET NOCOUNT ON

DELETE FROM dbo.TrendRefnums WHERE Refnum = @Refnum

SELECT @AssetId = AssetId, @StudyYear = StudyYear, @StudyId = RTRIM(StudyId) FROM dbo.TSort WHERE Refnum = @Refnum

DECLARE cRefs CURSOR LOCAL FAST_FORWARD

FOR SELECT Refnum, StudyYear FROM dbo.TSort 
	WHERE StudyYear <= @StudyYear AND AssetId = @AssetId AND Refnum <> @Refnum AND RTRIM(StudyId) = @StudyId
	ORDER BY StudyYear DESC, Refnum ASC
OPEN cRefs
DECLARE @i tinyint, @Ref varchar(25), @Year int, @LastYear int, @UseRef bit
SELECT @i = 0, @LastYear = 0
FETCH NEXT FROM cRefs INTO @Ref, @Year
WHILE @@FETCH_STATUS = 0 AND (@i < 3)
BEGIN
	SELECT @UseRef = 0
	IF LEN(RTRIM(@Refnum)) = 10
	BEGIN
	        --This is a non-pro forma (no character at the end of Refnum)
		IF LEN(RTRIM(@Ref)) = 10 
			SET @UseRef = 1
		ELSE IF @LastYear <> @Year And @Year <> @StudyYear
		--   You know, if we don't have any other submissions for this year, we will take a proforma!
			SET @UseRef = 1
	END
    ELSE
		SET @UseRef = 1
		
	IF @UseRef = 1
	BEGIN
		SELECT @i = @i + 1, @LastYear = @Year 
		IF @i = 1 BEGIN
			SET @RefnumP1 = @Ref
			SET @SYP1 = @Year
		END
		ELSE IF @i = 2 BEGIN
			SET @RefnumP2 = @Ref
			SET @SYP2 = @Year
		END
		ELSE IF @i = 3 BEGIN
			SET @RefnumP3 = @Ref
			SET @SYP3 = @Year
		END
	END
	FETCH NEXT FROM cRefs INTO @Ref, @Year
END

CLOSE cRefs
DEALLOCATE cRefs

INSERT dbo.TrendRefnums (Refnum, RefnumP1, RefnumP2, RefnumP3, StudyYear, StudyYearP1, StudyYearP2, StudyYearP3)
VALUES (RTrim(@Refnum), RTrim(@RefnumP1), RTrim(@RefnumP2), RTrim(@RefnumP3), @StudyYear, @SYP1, @SYP2, @SYP3)


