﻿CREATE TABLE [dim].[Region_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [RegionId]       VARCHAR (5)         NOT NULL,
    [ParentId]       VARCHAR (5)         NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Region_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Region_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Region_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Region_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Region_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Region_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [RegionId] ASC),
    CONSTRAINT [CR_Region_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Region_Parent_FactorSetLu] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Region_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_Region_Parent_LookUp_Region] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_Region_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Region_Parent] ([FactorSetId], [RegionId])
);


GO

CREATE TRIGGER [dim].[t_Region_Parent_u]
ON [dim].[Region_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Region_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Region_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Region_Parent].[RegionId]	= INSERTED.[RegionId];

END;