﻿CREATE TABLE [calc].[PeerGroupCapacity] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PeerGroup]      TINYINT            NOT NULL,
    [Capacity_kMT]   REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupCapacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupCapacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupCapacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_PeerGroupCapacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PeerGroupCapacity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PeerGroupCapacity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PeerGroupCapacity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PeerGroupCapacity_PeerGroup] FOREIGN KEY ([FactorSetId], [PeerGroup]) REFERENCES [ante].[PeerGroupCapacity] ([FactorSetId], [PeerGroup]),
    CONSTRAINT [FK_calc_PeerGroupCapacity_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_PeerGroupCapacity_u
ON  calc.PeerGroupCapacity
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE calc.PeerGroupCapacity
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PeerGroupCapacity.FactorSetId		= INSERTED.FactorSetId
		AND calc.PeerGroupCapacity.Refnum			= INSERTED.Refnum
		AND calc.PeerGroupCapacity.CalDateKey		= INSERTED.CalDateKey;
		
END;