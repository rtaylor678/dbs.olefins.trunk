﻿CREATE TABLE [calc].[Roi] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [MarginAnalysis] VARCHAR (42)       NOT NULL,
    [MarginId]       VARCHAR (42)       NOT NULL,
    [Roi]            REAL               NOT NULL,
    [TaAdj_Roi]      REAL               NOT NULL,
    [Loc_Roi]        REAL               NOT NULL,
    [TaAdj_Loc_Roi]  REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Roi_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Roi_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Roi_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Roi_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_Roi] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MarginAnalysis] ASC, [MarginId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_Roi_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_Roi_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_Roi_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_Roi_Margin_LookUp] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_calc_Roi_Stream_LookUp] FOREIGN KEY ([MarginAnalysis]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_Roi_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [calc].[t_Roi_u]
    ON [calc].[Roi]
    AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.Roi
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.Roi.FactorSetId		= INSERTED.FactorSetId
		AND	calc.Roi.Refnum				= INSERTED.Refnum
		AND calc.Roi.CalDateKey			= INSERTED.CalDateKey
		AND calc.Roi.CurrencyRpt		= INSERTED.CurrencyRpt
		AND calc.Roi.MarginAnalysis		= INSERTED.MarginAnalysis
		AND calc.Roi.MarginId			= INSERTED.MarginId;

END