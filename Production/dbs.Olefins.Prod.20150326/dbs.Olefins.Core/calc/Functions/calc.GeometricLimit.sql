﻿CREATE FUNCTION [calc].[GeometricLimit]
(
	@GeometricNumber	int, 
	@PartialAggregate	int
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @rtn	INT;
	DECLARE @base	INT = 2;
	
	DECLARE @k	INT = (Log(@GeometricNumber) / Log(@base));

	WHILE calc.GeometricContains(@GeometricNumber, @PartialAggregate) = 0 AND @GeometricNumber >= @PartialAggregate
	BEGIN

		SET @k = Log(@GeometricNumber) / Log(@base);
		SET @GeometricNumber = POWER(@base, @k - 1);

	END;

	RETURN @GeometricNumber;

END;