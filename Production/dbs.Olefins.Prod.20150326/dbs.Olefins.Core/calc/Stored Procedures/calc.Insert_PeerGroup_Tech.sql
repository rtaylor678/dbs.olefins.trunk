﻿CREATE PROCEDURE [calc].[Insert_PeerGroup_Tech]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PeerGroupTech(FactorSetId, Refnum, CalDateKey, PeerGroup, Tech_Year)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.CalDateKey,
			pg.PeerGroup,
			t.Tech_Year
		FROM (
			SELECT
				t.FactorSetId,
				t.Refnum,
				t.Plant_QtrDateKey		[CalDateKey],
				t.StudyYear -
				ROUND(
				0.3 * (t.StudyYear - c.StartUp_Year) +
				0.4 * (t.StudyYear - ROUND(f.FurnConstruction_Year, 0)) +
				0.3 * (SUM(k.Age_Years * k.Power_BHP) / SUM(k.Power_BHP))
				, 0)	[Tech_Year]

			FROM @fpl								t 
			INNER JOIN fact.CapacityAttributes		c
				ON	c.Refnum = t.Refnum
				AND	c.CalDateKey = t.Plant_QtrDateKey
			INNER JOIN fact.QuantityFeedAttributes	f
				ON	f.FactorSetId = t.FactorSetId
				AND	f.Refnum = t.Refnum
			INNER JOIN fact.FacilitiesCompressors	k
				ON	k.Refnum = t.Refnum
				AND	k.CalDateKey = t.Plant_QtrDateKey
				AND	k.FacilityId IN ('CompGas', 'CompRefrigPropylene', 'CompRefrigEthylene')
			WHERE	t.CalQtr = 4
			GROUP BY
				t.FactorSetId,
				t.Refnum,
				t.Plant_QtrDateKey,
				t.StudyYear,
				c.StartUp_Year,
				f.FurnConstruction_Year
			) t
		INNER JOIN ante.PeerGroupTech	pg
			ON	pg.FactorSetId = t.FactorSetId
			AND	pg.LimitLower_Year <	t.Tech_Year
			AND	pg.LimitUpper_Year >=	t.Tech_Year;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;