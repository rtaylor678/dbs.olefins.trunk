﻿CREATE PROCEDURE [calc].[Insert_ContainedPyroFeedLight]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ContainedPyroFeedLight(FactorSetId, Refnum, CalDateKey, ContainedComponentID, Feed_kMT, Recycled_kMT, Total_kMT)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			c.ContainedComponentID,
			s.Component_kMT,
			r.Recycled_kMT,
			ISNULL(s.Component_kMT, 0.0) + ISNULL(r.Recycled_kMT, 0.0)	[Total_kMT]
		FROM @fpl									fpl
		CROSS JOIN (VALUES ('C2+'), ('C3+'), ('C4+')) c(ContainedComponentID)
		LEFT OUTER JOIN (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey				[CalDateKey],
				CASE
					WHEN s.ComponentId IN ('CH4', 'C2H4', 'C2H6')	THEN 'C2+'
					WHEN s.ComponentId IN ('C3H6', 'C3H8')			THEN 'C3+'
					ELSE												 'C4+'
					END							[ContainedComponentID],
				SUM(s.Component_kMT)			[Component_kMT]
			FROM @fpl							fpl
			INNER JOIN calc.CompositionStream	s
				ON	s.FactorSetId = fpl.FactorSetId
				AND	s.Refnum = fpl.Refnum
				AND	s.CalDateKey = fpl.Plant_QtrDateKey
			INNER JOIN dim.Stream_Bridge		b
				ON	b.FactorSetId = s.FactorSetId
				AND	b.DescendantId = s.StreamId
				AND	b.StreamId = 'Light'
			WHERE	s.SimModelId = 'Plant'
				AND	s.OpCondId = 'PlantFeed'
				AND	s.ComponentId NOT IN ('CO_CO2', 'S')
			GROUP BY
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey,
				CASE
					WHEN s.ComponentId IN ('CH4', 'C2H4', 'C2H6')	THEN 'C2+'
					WHEN s.ComponentId IN ('C3H6', 'C3H8')			THEN 'C3+'
					ELSE												'C4+'
					END
			HAVING SUM(s.Component_kMT) <> 0.0
			)	s
			ON	s.FactorSetId = fpl.FactorSetId
			AND	s.Refnum = fpl.Refnum
			AND	s.CalDateKey = fpl.Plant_QtrDateKey
			AND s.[ContainedComponentID] = c.[ContainedComponentID]
		LEFT OUTER JOIN (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey				[CalDateKey],
				CASE
					WHEN r.ComponentId IN ('CH4', 'C2H4', 'C2H6')	THEN 'C2+'
					WHEN r.ComponentId IN ('C3H6', 'C3H8')			THEN 'C3+'
					ELSE												'C4+'
					END							[ContainedComponentID],
				SUM(r._Recycled_kMT)			[Recycled_kMT]
			FROM @fpl									fpl
			INNER JOIN calc.QuantityRecycled			r
				ON	r.FactorSetId = fpl.FactorSetId
				AND	r.Refnum = fpl.Refnum
				AND	r.CalDateKey = fpl.Plant_QtrDateKey
			INNER JOIN dim.Stream_Bridge				b
				ON	b.FactorSetId = fpl.FactorSetId
				AND	b.DescendantId = r.StreamId
				AND	b.StreamId = 'SuppTot'
			GROUP BY
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey,
				CASE
					WHEN r.ComponentId IN ('CH4', 'C2H4', 'C2H6')	THEN 'C2+'
					WHEN r.ComponentId IN ('C3H6', 'C3H8')			THEN 'C3+'
					ELSE												'C4+'
					END
			HAVING SUM(r._Recycled_kMT) <> 0.0
			)	r
			ON	r.FactorSetId = fpl.FactorSetId
			AND	r.Refnum = fpl.Refnum
			AND	r.CalDateKey = fpl.Plant_QtrDateKey
			AND	r.[ContainedComponentID] = c.[ContainedComponentID]
		WHERE	fpl.CalQtr = 4
			AND	ISNULL(s.Component_kMT, 0.0) + ISNULL(r.Recycled_kMT, 0.0) <> 0.0

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END