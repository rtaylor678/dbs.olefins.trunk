﻿CREATE PROCEDURE [calc].[Insert_MaintPersAverage]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.MaintPersAverage(FactorSetId, Refnum, CalDateKey, PersId, MaintPrev_Hrs, MaintCurr_Hrs, Maint_Pcnt)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.Plant_QtrDateKey,
			t.AccountId,
			t.Prev_Hrs,
			t.Curr_Hrs,
			(t.Prev_Hrs + t.Curr_Hrs) / 2.0 / t.AvgTot_Hrs * 100.0
		FROM (
			SELECT
				tsq.FactorSetId,
				tsq.Refnum,
				tsq.Plant_QtrDateKey,
				mp.AccountId,
				COALESCE(mp.StraightTimePrev_Hrs, 0.0)	+ COALESCE(p.InflTaAdjAnn_Company_Hrs, 0.0)	[Prev_Hrs],
				COALESCE(pa.Company_Hrs, 0.0)			+ COALESCE(p.InflTaAdjAnn_Company_Hrs, 0.0)	[Curr_Hrs],

				SUM(
					(COALESCE(mp.StraightTimePrev_Hrs, 0.0) + COALESCE(pa.Company_Hrs, 0.0)) / 2.0
						+ COALESCE(p.InflTaAdjAnn_Company_Hrs, 0.0)
					) OVER(PARTITION BY tsq.FactorSetId, tsq.Refnum, tsq.Plant_QtrDateKey)			[AvgTot_Hrs]

			FROM @fpl									tsq
			LEFT OUTER JOIN fact.MaintPersAggregate		mp
				ON	mp.FactorSetId		= tsq.FactorSetId
				AND	mp.Refnum			= tsq.Refnum
				AND	mp.CalDateKey		= tsq.Plant_QtrDateKey
				AND mp.AccountId		IN ('MpsMaint', 'OccMaint')
			LEFT OUTER JOIN fact.PersAggregate			pa	WITH (NOEXPAND)
				ON	pa.FactorSetId		= tsq.FactorSetId
				AND	pa.Refnum			= tsq.Refnum
				AND	pa.CalDateKey		= tsq.Plant_QtrDateKey
				AND pa.PersId			= mp.AccountId + 'Plant'
			LEFT OUTER JOIN calc.PersAggregateAdj		p
				ON	p.FactorSetId		= tsq.FactorSetId
				AND	p.Refnum			= tsq.Refnum
				AND	p.CalDateKey		= tsq.Plant_QtrDateKey
				AND	p.PersId			= mp.AccountId + 'TaAnnRetube'
			WHERE	tsq.CalQtr = 4
			) t
		WHERE	t.AvgTot_Hrs <> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;