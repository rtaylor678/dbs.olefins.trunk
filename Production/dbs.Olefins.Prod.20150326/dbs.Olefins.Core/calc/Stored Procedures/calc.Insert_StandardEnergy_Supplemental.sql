﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Supplemental]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			m.ModelId,
			q.StreamId,
			q.StreamDescription,
			k.Value * q.Tot_kMT		[StandardEnergy]
		FROM @fpl												tsq
		INNER JOIN fact.QuantityPivot							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN dim.Stream_Bridge							b
			ON	b.FactorSetId = tsq.FactorSetId
			AND	b.DescendantId = q.StreamId
			AND	 b.StreamId = 'SuppTot'
		INNER JOIN ante.StandardEnergyCoefficients				k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.StandardEnergyId = q.StreamId
		INNER JOIN dim.SimModel_LookUp							m
			ON	m.ModelId	<>	'Plant'
		WHERE	k.Value > 0.0
			AND	q.Tot_kMT > 0.0
			AND	tsq.FactorSet_AnnDateKey	<= 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;