﻿CREATE PROCEDURE calc.Insert_DivisorsFeed
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Losses_WtPcnt_Default		REAL = 0.25;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @MapSuppToRec (Supplemental)';
		PRINT @ProcedureDesc;

		DECLARE @MapSuppToRec TABLE(
			StreamID_Supp			VARCHAR(42)		NOT	NULL	CHECK(LEN(StreamID_Supp) > 0),
			StreamID_SuppRec		VARCHAR(42)		NOT	NULL	CHECK(LEN(StreamID_SuppRec) > 0),
			ComponentId				VARCHAR(42)		NOT	NULL	CHECK(LEN(ComponentId) > 0),
			PRIMARY KEY CLUSTERED(StreamID_Supp ASC)
			);

		INSERT INTO @MapSuppToRec(StreamID_Supp, StreamID_SuppRec, ComponentId)
		VALUES('DilEthane'	, 'SuppToRecEthane'		, 'C2H6')
			, ('ROGEthane'	, 'SuppToRecEthane'		, 'C2H6')
			, ('DilPropane'	, 'SuppToRecPropane'	, 'C3H8')
			, ('ROGPropane'	, 'SuppToRecPropane'	, 'C3H8')
			, ('DilButane'	, 'SuppToRecButane'		, 'C4H10')
			, ('ROGC4Plus'	, 'SuppToRecButane'		, 'C4H10');

		DECLARE @DivisorsFeed TABLE(
			FactorSetId						VARCHAR(12)			NOT NULL	CHECK(FactorSetId <> ''),
			Refnum							VARCHAR(25)			NOT NULL	CHECK(Refnum <> ''),
			CalDateKey						INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			StreamId						VARCHAR(42)			NOT	NULL	CHECK(StreamId <> ''),
			StreamDescription				VARCHAR(256)		NOT	NULL	CHECK(StreamDescription <> ''),
		
			IsLiquid						BIT					NOT	NULL,
			StreamQuantity_kMT				REAL				NOT	NULL	CHECK(StreamQuantity_kMT >= 0.0),
		
			ComponentId						VARCHAR(42)			NOT	NULL	CHECK(ComponentId <> ''),
			Component_WtPcnt				REAL				NOT	NULL	CHECK(Component_WtPcnt >= 0.0 AND Component_WtPcnt <= 100.0),
		
			Recycle_WtPcnt					REAL					NULL	CHECK(Recycle_WtPcnt >= 0.0 AND Recycle_WtPcnt <= 100.0),
		
			_FeedPlant_kMT					AS CONVERT(REAL, [StreamQuantity_kMT] * CASE WHEN NOT ([IsLiquid] = 1 AND [ComponentId] IN('H2', 'S'))		THEN	[Component_WtPcnt]	END	/ 100.0										, 1)
											PERSISTED,
			_FeedPyrolysis_kMT				AS CONVERT(REAL, [StreamQuantity_kMT] * CASE WHEN NOT ([IsLiquid] = 1 AND [ComponentId] IN('H2', 'S'))		THEN	[Recycle_WtPcnt]	END	/ 100.0										, 1)
											PERSISTED,
			_FeedAnalysis_kMT				AS CONVERT(REAL, [StreamQuantity_kMT] * CASE WHEN [ComponentId] NOT IN('H2', 'S', 'CO_CO2', 'CO', 'CO2')	THEN	[Recycle_WtPcnt]	END	/ 100.0										, 1)
											PERSISTED,
			_FeedH2_kMT						AS CONVERT(REAL, [StreamQuantity_kMT] * CASE WHEN [IsLiquid] = 0 AND [ComponentId] = 'H2'					THEN	[Recycle_WtPcnt]	END	/ 100.0										, 1)
											PERSISTED,
			_FeedInerts_kMT					AS CONVERT(REAL, [StreamQuantity_kMT] * CASE WHEN [ComponentId] IN ('S', 'CO_CO2', 'CO', 'CO2')				THEN	[Recycle_WtPcnt]	END	/ 100.0										, 1)
											PERSISTED,
										
			Losses_WtPcnt					REAL				NOT	NULL	CHECK([Losses_WtPcnt] >= 0.0 AND [Losses_WtPcnt] <= 100.0),
																	
			_Losses_kMT						AS CONVERT(REAL, [StreamQuantity_kMT] * CASE WHEN [ComponentId] NOT IN('H2', 'S', 'CO_CO2', 'CO', 'CO2')	THEN	[Recycle_WtPcnt]	END	/ 100.0 *		   [Losses_WtPcnt]  / 100.0	, 1)
											PERSISTED,

			PRIMARY KEY CLUSTERED(FactorSetId DESC, Refnum DESC, CalDateKey DESC, StreamId ASC, StreamDescription ASC, ComponentId ASC)
			);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @DivisorsFeed (Light)';
		PRINT @ProcedureDesc;

		INSERT INTO @DivisorsFeed(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, IsLiquid, StreamQuantity_kMT, ComponentId, Component_WtPcnt, Recycle_WtPcnt, Losses_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			0,
			q.Quantity_kMT,
			c.ComponentId,
			c.Component_WtPcnt,
			c.Component_WtPcnt,
			@Losses_WtPcnt_Default
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN calc.[CompositionStream]					c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = q.StreamId
			AND	c.StreamDescription = q.StreamDescription
			AND	c.SimModelId = 'Plant'
			AND	c.OpCondId = 'PlantFeed'
			AND	c.RecycleId = 0
		WHERE	q.StreamId IN (SELECT b.DescendantId FROM dim.Stream_Bridge b WHERE b.FactorSetId = tsq.FactorSetId AND b.StreamId = 'Light');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @DivisorsFeed (Liquid)';
		PRINT @ProcedureDesc;

		INSERT INTO @DivisorsFeed(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, IsLiquid, StreamQuantity_kMT, ComponentId, Component_WtPcnt, Recycle_WtPcnt, Losses_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			1,
			q.Quantity_kMT,
			'PIANO',
			100.0,
			100.0,
			@Losses_WtPcnt_Default
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND	q.StreamId IN (SELECT b.DescendantId FROM dim.Stream_Bridge b WHERE b.FactorSetId = tsq.FactorSetId AND b.StreamId = 'Liquid');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @DivisorsFeed (Liquid - Inerts)';
		PRINT @ProcedureDesc;

		INSERT INTO @DivisorsFeed(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, IsLiquid, StreamQuantity_kMT, ComponentId, Component_WtPcnt, Recycle_WtPcnt, Losses_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			1,
			q.Quantity_kMT,
			c.ComponentId,
			c.Component_WtPcnt,
			c.Component_WtPcnt,
			@Losses_WtPcnt_Default
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN calc.[CompositionStream]					c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = q.StreamId
			AND	c.StreamDescription = q.StreamDescription
			AND	c.SimModelId = 'Plant'
			AND	c.OpCondId = 'PlantFeed'
			AND	c.ComponentId NOT IN ('P', 'I', 'A', 'N', 'O')
		WHERE	q.StreamId IN (SELECT b.DescendantId FROM dim.Stream_Bridge b WHERE b.FactorSetId = tsq.FactorSetId AND b.StreamId = 'Liquid');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @DivisorsFeed (Supplemental)';
		PRINT @ProcedureDesc;

		INSERT INTO @DivisorsFeed(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, IsLiquid, StreamQuantity_kMT, ComponentId, Component_WtPcnt, Recycle_WtPcnt, Losses_WtPcnt)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			0,
			q.Quantity_kMT,
			c.ComponentId,
			c.Component_WtPcnt,
			r.Recycled_WtPcnt,
			ISNULL(l._Loss_Pcnt, @Losses_WtPcnt_Default)
		FROM	@fpl										tsq
		INNER JOIN fact.Quantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN calc.[CompositionStream]				c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = q.StreamId
			AND	c.StreamDescription = q.StreamDescription
			AND	c.SimModelId = 'Plant'
			AND	c.OpCondId = 'PlantFeed'
			AND	c.RecycleId = 0
		LEFT OUTER JOIN @MapSuppToRec						m
			ON	m.StreamID_Supp = q.StreamId
		LEFT OUTER JOIN fact.QuantitySuppRecycled			r
			ON	r.Refnum = tsq.Refnum
			AND	r.StreamId = m.StreamID_SuppRec
			AND	r.ComponentId = r.ComponentId
		LEFT OUTER JOIN inter.DivisorsProduction			l
			ON	l.FactorSetId = tsq.FactorSetId
			AND	l.Refnum = tsq.Refnum
			AND	l.ComponentId = 'C2H4'
		WHERE	q.StreamId IN (SELECT b.DescendantId FROM dim.Stream_Bridge b WHERE b.FactorSetId = tsq.FactorSetId AND b.StreamId = 'SuppTot');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.DivisorsFeed';
		PRINT @ProcedureDesc;

		INSERT INTO calc.DivisorsFeed(FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription,
			FeedPlant_kMT, FeedPyrolysis_kMT, 
			FeedH2_kMT, FeedInerts_kMT, FeedAnalysis_kMT,
			
			Losses_kMT,
			FeedPlantWithLosses_kMT,
			FeedPyrolysis_WithLosses_kMT,
			FeedAnalysisWithLosses_kMT,
			TaAdj_Feed_Ratio
		)
		SELECT
			f.FactorSetId,
			f.Refnum,
			MAX(f.CalDateKey),
			b.StreamId,
			CASE WHEN b.StreamId IN ('FeedLtOther', 'FeedLiqOther') THEN f.StreamDescription END,

			SUM(f._FeedPlant_kMT)				[FeedPlant_kMT],
			SUM(f._FeedPyrolysis_kMT)			[FeedPyrolysis_kMT],

			SUM(f._FeedH2_kMT)					[FeedH2_kMT],
			SUM(f._FeedInerts_kMT)				[FeedInerts_kMT],
			SUM(f._FeedPyrolysis_kMT) - COALESCE(SUM(f._FeedH2_kMT), 0.0) - COALESCE(SUM(f._FeedInerts_kMT), 0.0)	[FeedAnalysis_kMT],

			SUM(f._Losses_kMT)					[Losses_kMT],

			SUM(f._FeedPlant_kMT)																					- ISNULL(SUM(f._Losses_kMT), 0.0)	[FeedPlant_WithLosses_kMT],
			SUM(f._FeedPyrolysis_kMT)																				- ISNULL(SUM(f._Losses_kMT), 0.0)	[FeedPyrolysis_WithLosses_kMT],
			SUM(f._FeedPyrolysis_kMT) - COALESCE(SUM(f._FeedH2_kMT), 0.0) - COALESCE(SUM(f._FeedInerts_kMT), 0.0)	- ISNULL(SUM(f._Losses_kMT), 0.0)	[FeedAnalysis_WithLosses_kMT],

			r._TaAdj_Production_Ratio
		FROM @DivisorsFeed							f
		INNER JOIN dim.Stream_Bridge				b
			ON	b.FactorSetId	= f.FactorSetId
			AND	b.DescendantId	= f.StreamId
			AND	b.StreamId		IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = f.FactorSetId AND d.StreamId = 'PlantFeed')
		LEFT OUTER JOIN calc.TaAdjRatio				r
			ON	r.FactorSetId	= f.FactorSetId
			AND	r.Refnum		= f.Refnum
			AND	r.StreamId		= 'Ethylene'
			AND	r.ComponentId	= 'C2H4'
		GROUP BY
			f.FactorSetId,
			f.Refnum,
			b.StreamId,
			CASE WHEN b.StreamId IN ('FeedLtOther', 'FeedLiqOther') THEN f.StreamDescription END,
			r._TaAdj_Production_Ratio;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;