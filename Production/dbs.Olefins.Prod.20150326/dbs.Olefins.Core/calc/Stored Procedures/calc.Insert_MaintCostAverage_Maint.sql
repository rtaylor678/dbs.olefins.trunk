﻿CREATE PROCEDURE [calc].[Insert_MaintCostAverage_Maint]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.MaintCostAverage(FactorSetId, Refnum, CalDateKey, CurrencyRpt, AccountId, MaintPrev_Cur, MaintCurr_Cur)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			m.AccountId,
			m.AmountPrev_Cur,
			m.Amount_Cur
		FROM @fpl						fpl
		INNER JOIN fact.MaintOpExAggregate		m
			ON	m.Refnum			= fpl.Refnum
			AND	m.CalDateKey		= fpl.Plant_QtrDateKey
			AND	m.CurrencyRpt		= fpl.CurrencyRpt
			AND	m.AccountId			<> 'Maint'
			AND(m.AmountPrev_Cur	<> 0.0
			OR	m.Amount_Cur		<> 0.0)
		WHERE	fpl.CalQtr		= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;