﻿CREATE PROCEDURE [calc].[Insert_NicenessFeed_SupplementalEP]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessFeed]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [NicenessYield_kMT], [NicenessEnergy_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			m.[StreamNiceness],
			SUM(q.Quantity_kMT * r.Recycled_WtPcnt) / 100.0,
			SUM(q.Quantity_kMT * r.Recycled_WtPcnt) / 100.0,
			SUM(q.Quantity_kMT * r.Recycled_WtPcnt) / 100.0
		FROM @fpl									fpl
		INNER JOIN [ante].[MapRecyle]				m
			ON	m.[FactorSetId]		= fpl.[FactorSetId]
		INNER JOIN [fact].[Quantity]				q
			ON	q.[Refnum]		= fpl.[Refnum]
			AND	q.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	q.[StreamId]	= m.[StreamId]
			AND	q.[StreamId]	IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId IN ('SuppDil', 'SuppROG'))
		INNER JOIN [fact].[QuantitySuppRecycled]	r
			ON	r.[Refnum]		= fpl.[Refnum]
			AND	r.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	r.[StreamId]	= m.[RecycleId]
		WHERE	fpl.[FactorSet_AnnDateKey]	> 20130000
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			m.[StreamNiceness];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;