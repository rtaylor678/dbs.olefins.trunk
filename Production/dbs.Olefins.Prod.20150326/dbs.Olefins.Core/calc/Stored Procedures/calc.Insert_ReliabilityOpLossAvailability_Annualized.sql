﻿CREATE PROCEDURE [calc].[Insert_ReliabilityOpLossAvailability_Annualized]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ReliabilityOpLossAvailability]([FactorSetId], [Refnum], [CalDateKey], [OppLossId], [DownTimeLoss_MT], [SlowDownLoss_MT], [TotLoss_MT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ri.[OppLossId],
			ri.[DownTimeLoss_MT],
			ri.[SlowDownLoss_MT],
			ri.[TotLoss_MT]
		FROM @fpl										fpl
		INNER JOIN [calc].[ReliabilityOpLossAnnualized]	ri
			ON	ri.[FactorSetId]	= fpl.[FactorSetId]
			AND	ri.[Refnum]			= fpl.[Refnum]
			AND	ri.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	ri.[OppLossId]		IN (SELECT d.DescendantId FROM [dim].[ReliabilityOppLoss_Availability_Bridge] d WHERE d.[FactorSetId] = fpl.[FactorSetId] AND d.[OppLossId] = 'LostProdOpp')
			AND	ri.[OppLossId]		<> 'TurnAround'
		WHERE	fpl.[CalQtr]		= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;