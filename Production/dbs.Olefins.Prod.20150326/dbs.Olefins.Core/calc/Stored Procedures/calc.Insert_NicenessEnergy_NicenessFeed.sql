﻿CREATE PROCEDURE [calc].[Insert_NicenessEnergy_NicenessFeed]
(
	@Refnum		VARCHAR (18),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessEnergy]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [Stream_Pcnt], [NicenessYield_kMT], [NicenessYield_Pcnt], [NicenessYield_Tot], [NicenessEnergy_kMT], [NicenessEnergy_Pcnt], [NicenessEnergy_Tot])
		SELECT
			nf.[FactorSetId],
			nf.[Refnum],
			nf.[CalDateKey],
			nf.[StreamId],
			SUM(nf.[Stream_kMT])											[Stream_kMT],
			SUM(nf.[Stream_kMT])
				/ SUM(SUM(nf.[Stream_kMT]))			OVER() * 100.0			[Stream_Pcnt],

			SUM(nf.[NicenessYield_kMT])										[NicenessYield_kMT],
			SUM(nf.[NicenessYield_kMT])
				/ SUM(SUM(nf.[NicenessYield_kMT]))	OVER() * 100.0			[NicenessYield_Pcnt],
			SUM(nf.[NicenessYield_kMT])
				/ SUM(SUM(nf.[NicenessYield_kMT]))	OVER() * co.[Yield]		[NicenessYield_Tot],

			SUM(nf.[NicenessEnergy_kMT])									[NicenessEnergy_kMT],
			SUM(nf.[NicenessEnergy_kMT])
				/ SUM(SUM(nf.[NicenessEnergy_kMT]))	OVER() * 100.0			[NicenessEnergy_Pcnt],
			SUM(nf.[NicenessEnergy_kMT])
				/ SUM(SUM(nf.[NicenessEnergy_kMT]))	OVER() * co.[Energy]	[NicenessEnergy_Tot]
		FROM [calc].[NicenessFeed]								nf
		LEFT OUTER JOIN [ante].[NicenessModelCoefficients]		co
			ON	co.FactorSetId	= nf.FactorSetId
			AND	co.StreamId		= nf.StreamId
		GROUP BY
			nf.[FactorSetId],
			nf.[Refnum],
			nf.[CalDateKey],
			nf.[StreamId],
			co.[Yield],
			co.[Energy];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;