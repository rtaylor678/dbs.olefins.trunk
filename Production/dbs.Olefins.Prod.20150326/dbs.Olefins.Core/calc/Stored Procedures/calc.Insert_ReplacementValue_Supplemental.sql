﻿CREATE PROCEDURE [calc].[Insert_ReplacementValue_Supplemental]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplacementValue(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReplacementValueId, ReplacementValue)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey, 
			fpl.CurrencyRpt,
			'FeedSupplemetal',
			f._SuppFeedAdjustment
		FROM @fpl										fpl
		INNER JOIN calc.ReplValSuppFeedAdjustment		f
			ON	f.FactorSetId = fpl.FactorSetId
			AND	f.Refnum = fpl.Refnum
			AND	f.CalDateKey = fpl.Plant_QtrDateKey
		WHERE	fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;