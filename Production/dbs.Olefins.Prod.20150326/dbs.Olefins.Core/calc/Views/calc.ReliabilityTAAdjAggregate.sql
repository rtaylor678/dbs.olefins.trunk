﻿
CREATE VIEW calc.ReliabilityTAAdjAggregate
WITH SCHEMABINDING
AS
SELECT
	r.FactorSetId,
	r.Refnum,
	r.CalDateKey,
	r.CurrencyRpt,
	r.CurrencyFcn,
	SUM(r.Ann_TurnAroundCost_kCur)			[Ann_TurnAroundCost_kCur],
	SUM(r._InflAdjAnn_TurnAroundCost_kCur)	[InflAdjAnn_TurnAroundCost_kCur],
	SUM(r.Ann_TurnAround_ManHrs)				[Ann_TurnAround_ManHrs]
FROM calc.TaAdjReliability r
GROUP BY
	r.FactorSetId,
	r.Refnum,
	r.CalDateKey,
	r.CurrencyRpt,
	r.CurrencyFcn;