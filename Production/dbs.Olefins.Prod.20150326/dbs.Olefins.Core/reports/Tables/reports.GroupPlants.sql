﻿CREATE TABLE [reports].[GroupPlants] (
    [GroupId] VARCHAR (25) NOT NULL,
    [Refnum]  VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_Report_GroupPlants] PRIMARY KEY CLUSTERED ([GroupId] ASC, [Refnum] ASC)
);

