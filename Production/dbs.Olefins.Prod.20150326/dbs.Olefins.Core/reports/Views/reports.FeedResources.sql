﻿
--select * from FeedResources where Refnum = '2013PCH003'
--select * from [reports].FeedResources where groupid = '13pch'

CREATE VIEW [reports].[FeedResources] AS
Select
  GroupId = r.GroupId
, MinParaffin = [$(DbGlobal)].dbo.WtAvgNZ(MinParaffin, 1.0)
, MaxVaporPresFRS = [$(DbGlobal)].dbo.WtAvgNZ(MaxVaporPresFRS, 1.0)
, Use_LP_PC = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_LP_PC = 'Y' OR Lvl_LP_PC IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LP_PC_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_LP_PC = 'P' OR Lvl_LP_PC IS NULL) AND Use_LP_PC = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LP_PC_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LP_PC = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LP_PC_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LP_PC = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_LP_Main = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_LP_Main = 'Y' OR Lvl_LP_Main IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LP_Main_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_LP_Main = 'P' OR Lvl_LP_Main IS NULL) AND Use_LP_Main = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LP_Main_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LP_Main = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LP_Main_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LP_Main = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_LPMultiPlant = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_LPMultiPlant = 'Y' OR Lvl_LPMultiPlant IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LPMultiPlant_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_LPMultiPlant = 'P' OR Lvl_LPMultiPlant IS NULL) AND Use_LPMultiPlant = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LPMultiPlant_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LPMultiPlant = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LPMultiPlant_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LPMultiPlant = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_LPMultiPeriod = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_LPMultiPeriod = 'Y' OR Lvl_LPMultiPeriod IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LPMultiPeriod_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_LPMultiPeriod = 'P' OR Lvl_LPMultiPeriod IS NULL) AND Use_LPMultiPeriod = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LPMultiPeriod_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LPMultiPeriod = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_LPMultiPeriod_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_LPMultiPeriod = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_TierData = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_TierData = 'Y' OR Lvl_TierData IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_TierData_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_TierData = 'P' OR Lvl_TierData IS NULL) AND Use_TierData = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_TierData_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_TierData = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_TierData_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_TierData = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_TierProduct = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_TierProduct = 'Y' OR Lvl_TierProduct IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_TierProduct_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_TierProduct = 'P' OR Lvl_TierProduct IS NULL) AND Use_TierProduct = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_TierProduct_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_TierProduct = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_TierProduct_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_TierProduct = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_NonLP = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_NonLP = 'Y' OR Lvl_NonLP IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_NonLP_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_NonLP = 'P' OR Lvl_NonLP IS NULL) AND Use_NonLP = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_NonLP_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_NonLP = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_NonLP_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_NonLP = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_Spread = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_Spread = 'Y' OR Lvl_Spread IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_Spread_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_Spread = 'P' OR Lvl_Spread IS NULL) AND Use_Spread = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_Spread_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_Spread = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_Spread_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_Spread = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_MarginData = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_MarginData = 'Y' OR Lvl_MarginData IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_MarginData_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_MarginData = 'P' OR Lvl_MarginData IS NULL) AND Use_MarginData = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_MarginData_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_MarginData = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_MarginData_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_MarginData = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_AltFeed = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_AltFeed = 'Y' OR Lvl_AltFeed IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_AltFeed_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_AltFeed = 'P' OR Lvl_AltFeed IS NULL) AND Use_AltFeed = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_AltFeed_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_AltFeed = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_AltFeed_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_AltFeed = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_BEAltFeed = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_BEAltFeed = 'Y' OR Lvl_BEAltFeed IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_BEAltFeed_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_BEAltFeed = 'P' OR Lvl_BEAltFeed IS NULL) AND Use_BEAltFeed = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_BEAltFeed_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_BEAltFeed = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_BEAltFeed_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_BEAltFeed = 'B' THEN 100.0 ELSE 0 END, 1.0)

, Use_YieldPredict = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_YieldPredict = 'Y' OR Lvl_YieldPredict IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_YieldPredict_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_YieldPredict = 'P' OR Lvl_YieldPredict IS NULL) AND Use_YieldPredict = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_YieldPredict_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_YieldPredict = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_YieldPredict_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_YieldPredict = 'B' THEN 100.0 ELSE 0 END, 1.0)

, YieldPatternCnt = [$(DbGlobal)].dbo.WtAvgNZ(YieldPatternCnt, 1.0)
, YieldPatternLPCnt = [$(DbGlobal)].dbo.WtAvgNZ(YieldPatternLPCnt, 1.0)
, YieldGroupCnt = [$(DbGlobal)].dbo.WtAvgNZ(YieldGroupCnt, 1.0)
, Plant_PPCnt = [$(DbGlobal)].dbo.WtAvgNZ(Plant_PPCnt, 1.0)
, Corp_PPCnt = [$(DbGlobal)].dbo.WtAvgNZ(Corp_PPCnt, 1.0)
, Plant_3PlusCnt = [$(DbGlobal)].dbo.WtAvgNZ(Plant_3PlusCnt, 1.0)
, Corp_3PlusCnt = [$(DbGlobal)].dbo.WtAvgNZ(Corp_3PlusCnt, 1.0)
, Plant_BuyerCnt = [$(DbGlobal)].dbo.WtAvgNZ(Plant_BuyerCnt, 1.0)
, Corp_BuyerCnt = [$(DbGlobal)].dbo.WtAvgNZ(Corp_BuyerCnt, 1.0)
, Plant_DataCnt = [$(DbGlobal)].dbo.WtAvgNZ(Plant_DataCnt, 1.0)
, Corp_DataCnt = [$(DbGlobal)].dbo.WtAvgNZ(Corp_DataCnt, 1.0)
, Plant_Logistic = [$(DbGlobal)].dbo.WtAvgNZ(Plant_Logistic, 1.0)
, Corp_Logistic = [$(DbGlobal)].dbo.WtAvgNZ(Corp_Logistic, 1.0)

, Plant_Decision = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Plant_Decision IS NOT NULL THEN 100.0 ELSE 0 END, Case when (Plant_Decision IS Null AND  Corp_Decision IS Null) then 0.0 else 1.0 end)
, Corp_Decision = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Corp_Decision IS NOT NULL THEN 100.0 ELSE 0 END, Case when (Plant_Decision IS Null AND  Corp_Decision IS Null) then 0.0 else 1.0 end)

, PlanTimeDays = [$(DbGlobal)].dbo.WtAvgNZ(PlanTimeDays, 1.0)
, FeedSlateChg = [$(DbGlobal)].dbo.WtAvg(CASE WHEN FeedSlateChg = 'Y' THEN 100.0 ELSE 0.0 END, 1.0)
, Reliab = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Reliab = 'Y' THEN 100.0 ELSE 0.0 END, 1.0)
, Environ = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Environ = 'Y' THEN 100.0 ELSE 0.0 END, 1.0)
, Furnace = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Furnace = 'Y' THEN 100.0 ELSE 0.0 END, 1.0)
, Maint = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Maint = 'Y' THEN 100.0 ELSE 0.0 END, 1.0)
, Interest = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Interest = 'Y' THEN 100.0 ELSE 0.0 END, 1.0)
, EffFeedChg = [$(DbGlobal)].dbo.WtAvg(EffFeedChg, 1.0)
, FeedPurchase = [$(DbGlobal)].dbo.WtAvg(FeedPurchase, 1.0)
, YPS_Lummus = [$(DbGlobal)].dbo.WtAvg(YPS_Lummus, CASE WHEN YPS_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, YPS_SPYRO = [$(DbGlobal)].dbo.WtAvg(YPS_SPYRO, CASE WHEN YPS_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, YPS_SelfDev = [$(DbGlobal)].dbo.WtAvg(YPS_SelfDev, CASE WHEN YPS_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, YPS_Other = [$(DbGlobal)].dbo.WtAvg(YPS_Other, CASE WHEN YPS_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, LP_Aspen = [$(DbGlobal)].dbo.WtAvg(LP_Aspen, CASE WHEN LP_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Honeywell = [$(DbGlobal)].dbo.WtAvg(LP_Honeywell, CASE WHEN LP_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Haverly = [$(DbGlobal)].dbo.WtAvg(LP_Haverly, CASE WHEN LP_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_SelfDev = [$(DbGlobal)].dbo.WtAvg(LP_SelfDev, CASE WHEN LP_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Other = [$(DbGlobal)].dbo.WtAvg(LP_Other, CASE WHEN LP_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, LP_RowCnt = [$(DbGlobal)].dbo.WtAvgNZ(LP_RowCnt, 1.0)
, LP_Recur_Yes = [$(DbGlobal)].dbo.WtAvg(LP_Recur_Yes, CASE WHEN LP_Recur_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Recur_No = [$(DbGlobal)].dbo.WtAvg(LP_Recur_No, CASE WHEN LP_Recur_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Recur_NotSure = [$(DbGlobal)].dbo.WtAvg(LP_Recur_NotSure, CASE WHEN LP_Recur_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Recur_RPT = [$(DbGlobal)].dbo.WtAvg(LP_Recur_RPT, 1.0)
, LP_DeltaBase_Yes = [$(DbGlobal)].dbo.WtAvg(LP_DeltaBase_Yes, CASE WHEN LP_DeltaBase_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_DeltaBase_No = [$(DbGlobal)].dbo.WtAvg(LP_DeltaBase_No, CASE WHEN LP_DeltaBase_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_DeltaBase_NotSure = [$(DbGlobal)].dbo.WtAvg(LP_DeltaBase_NotSure, CASE WHEN LP_DeltaBase_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_DeltaBase_RPT = [$(DbGlobal)].dbo.WtAvg(LP_DeltaBase_RPT, 1.0)
, LP_MixInt_Yes = [$(DbGlobal)].dbo.WtAvg(LP_MixInt_Yes, CASE WHEN LP_MixInt_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_MixInt_No = [$(DbGlobal)].dbo.WtAvg(LP_MixInt_No, CASE WHEN LP_MixInt_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_MixInt_NotSure = [$(DbGlobal)].dbo.WtAvg(LP_MixInt_NotSure, CASE WHEN LP_MixInt_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_MixInt_RPT = [$(DbGlobal)].dbo.WtAvg(LP_MixInt_RPT, 1.0)
, LP_Online_Yes = [$(DbGlobal)].dbo.WtAvg(LP_Online_Yes, CASE WHEN LP_Online_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Online_No = [$(DbGlobal)].dbo.WtAvg(LP_Online_No, CASE WHEN LP_Online_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, LP_Online_NotSure = [$(DbGlobal)].dbo.WtAvg(LP_Online_NotSure, CASE WHEN LP_Online_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, LP_Online_RPT = [$(DbGlobal)].dbo.WtAvg(LP_Online_RPT, 1.0)

, BeyondCrack_None = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_None, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, BeyondCrack_C4 = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_C4, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, BeyondCrack_C5 = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_C5, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, BeyondCrack_Pygas = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_Pygas, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, BeyondCrack_BTX = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_BTX, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, BeyondCrack_OthChem = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_OthChem, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, BeyondCrack_OthRefine = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_OthRefine, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, BeyondCrack_Oth = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_Oth, CASE WHEN BeyondCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, Data_LPSpecial = [$(DbGlobal)].dbo.WtAvg(Data_LPSpecial, CASE WHEN Data_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Data_Econ = [$(DbGlobal)].dbo.WtAvg(Data_Econ, CASE WHEN Data_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Data_FeedSupply = [$(DbGlobal)].dbo.WtAvg(Data_FeedSupply, CASE WHEN Data_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Data_Market = [$(DbGlobal)].dbo.WtAvg(Data_Market, CASE WHEN Data_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Data_TechSrvs = [$(DbGlobal)].dbo.WtAvg(Data_TechSrvs, CASE WHEN Data_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Data_Oper = [$(DbGlobal)].dbo.WtAvg(Data_Oper, CASE WHEN Data_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Data_Oth = [$(DbGlobal)].dbo.WtAvg(Data_Oth, CASE WHEN Data_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, Dev_LPSpecial = [$(DbGlobal)].dbo.WtAvg(Dev_LPSpecial, CASE WHEN Dev_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Dev_Econ = [$(DbGlobal)].dbo.WtAvg(Dev_Econ, CASE WHEN Dev_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Dev_FeedSupply = [$(DbGlobal)].dbo.WtAvg(Dev_FeedSupply, CASE WHEN Dev_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Dev_Market = [$(DbGlobal)].dbo.WtAvg(Dev_Market, CASE WHEN Dev_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Dev_TechSrvs = [$(DbGlobal)].dbo.WtAvg(Dev_TechSrvs, CASE WHEN Dev_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Dev_Oper = [$(DbGlobal)].dbo.WtAvg(Dev_Oper, CASE WHEN Dev_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Dev_Oth = [$(DbGlobal)].dbo.WtAvg(Dev_Oth, CASE WHEN Dev_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, Valid_LPSpecial = [$(DbGlobal)].dbo.WtAvg(Valid_LPSpecial, CASE WHEN Valid_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Valid_Econ = [$(DbGlobal)].dbo.WtAvg(Valid_Econ, CASE WHEN Valid_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Valid_FeedSupply = [$(DbGlobal)].dbo.WtAvg(Valid_FeedSupply, CASE WHEN Valid_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Valid_Market = [$(DbGlobal)].dbo.WtAvg(Valid_Market, CASE WHEN Valid_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Valid_TechSrvs = [$(DbGlobal)].dbo.WtAvg(Valid_TechSrvs, CASE WHEN Valid_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Valid_Oper = [$(DbGlobal)].dbo.WtAvg(Valid_Oper, CASE WHEN Valid_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Valid_Oth = [$(DbGlobal)].dbo.WtAvg(Valid_Oth, CASE WHEN Valid_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, Constraint_LPSpecial = [$(DbGlobal)].dbo.WtAvg(Constraint_LPSpecial, CASE WHEN Constraint_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Constraint_Econ = [$(DbGlobal)].dbo.WtAvg(Constraint_Econ, CASE WHEN Constraint_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Constraint_FeedSupply = [$(DbGlobal)].dbo.WtAvg(Constraint_FeedSupply, CASE WHEN Constraint_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Constraint_Market = [$(DbGlobal)].dbo.WtAvg(Constraint_Market, CASE WHEN Constraint_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Constraint_TechSrvs = [$(DbGlobal)].dbo.WtAvg(Constraint_TechSrvs, CASE WHEN Constraint_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Constraint_Oper = [$(DbGlobal)].dbo.WtAvg(Constraint_Oper, CASE WHEN Constraint_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, Constraint_Oth = [$(DbGlobal)].dbo.WtAvg(Constraint_Oth, CASE WHEN Constraint_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, FeedPrice_LPSpecial = [$(DbGlobal)].dbo.WtAvg(FeedPrice_LPSpecial, CASE WHEN FeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, FeedPrice_Econ = [$(DbGlobal)].dbo.WtAvg(FeedPrice_Econ, CASE WHEN FeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, FeedPrice_FeedSupply = [$(DbGlobal)].dbo.WtAvg(FeedPrice_FeedSupply, CASE WHEN FeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, FeedPrice_Market = [$(DbGlobal)].dbo.WtAvg(FeedPrice_Market, CASE WHEN FeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, FeedPrice_TechSrvs = [$(DbGlobal)].dbo.WtAvg(FeedPrice_TechSrvs, CASE WHEN FeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, FeedPrice_Oper = [$(DbGlobal)].dbo.WtAvg(FeedPrice_Oper, CASE WHEN FeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, FeedPrice_Oth = [$(DbGlobal)].dbo.WtAvg(FeedPrice_Oth, CASE WHEN FeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, ProdPrice_LPSpecial = [$(DbGlobal)].dbo.WtAvg(ProdPrice_LPSpecial, CASE WHEN ProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, ProdPrice_Econ = [$(DbGlobal)].dbo.WtAvg(ProdPrice_Econ, CASE WHEN ProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, ProdPrice_FeedSupply = [$(DbGlobal)].dbo.WtAvg(ProdPrice_FeedSupply, CASE WHEN ProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, ProdPrice_Market = [$(DbGlobal)].dbo.WtAvg(ProdPrice_Market, CASE WHEN ProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, ProdPrice_TechSrvs = [$(DbGlobal)].dbo.WtAvg(ProdPrice_TechSrvs, CASE WHEN ProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, ProdPrice_Oper = [$(DbGlobal)].dbo.WtAvg(ProdPrice_Oper, CASE WHEN ProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, ProdPrice_Oth = [$(DbGlobal)].dbo.WtAvg(ProdPrice_Oth, CASE WHEN ProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0

, PPCom_1 = [$(DbGlobal)].dbo.WtAvg(PPCom_1, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_2 = [$(DbGlobal)].dbo.WtAvg(PPCom_2, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_3 = [$(DbGlobal)].dbo.WtAvg(PPCom_3, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_4 = [$(DbGlobal)].dbo.WtAvg(PPCom_4, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_5 = [$(DbGlobal)].dbo.WtAvg(PPCom_5, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_6 = [$(DbGlobal)].dbo.WtAvg(PPCom_6, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_7 = [$(DbGlobal)].dbo.WtAvg(PPCom_7, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_8 = [$(DbGlobal)].dbo.WtAvg(PPCom_8, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPCom_9 = [$(DbGlobal)].dbo.WtAvg(PPCom_9, CASE WHEN PPCom_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_1 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_1, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_2 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_2, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_3 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_3, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_4 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_4, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_5 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_5, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_6 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_6, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_7 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_7, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_8 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_8, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedPrice_9 = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_9, CASE WHEN PPFeedPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_1 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_1, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_2 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_2, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_3 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_3, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_4 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_4, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_5 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_5, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_6 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_6, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_7 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_7, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_8 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_8, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedCrack_9 = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_9, CASE WHEN PPFeedCrack_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_1 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_1, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_2 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_2, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_3 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_3, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_4 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_4, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_5 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_5, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_6 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_6, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_7 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_7, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_8 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_8, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPFeedAvail_9 = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_9, CASE WHEN PPFeedAvail_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_1 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_1, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_2 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_2, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_3 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_3, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_4 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_4, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_5 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_5, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_6 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_6, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_7 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_7, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_8 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_8, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdPrice_9 = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_9, CASE WHEN PPProdPrice_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_1 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_1, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_2 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_2, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_3 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_3, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_4 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_4, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_5 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_5, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_6 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_6, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_7 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_7, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_8 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_8, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPProdDemand_9 = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_9, CASE WHEN PPProdDemand_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_1 = [$(DbGlobal)].dbo.WtAvg(PPStatus_1, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_2 = [$(DbGlobal)].dbo.WtAvg(PPStatus_2, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_3 = [$(DbGlobal)].dbo.WtAvg(PPStatus_3, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_4 = [$(DbGlobal)].dbo.WtAvg(PPStatus_4, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_5 = [$(DbGlobal)].dbo.WtAvg(PPStatus_5, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_6 = [$(DbGlobal)].dbo.WtAvg(PPStatus_6, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_7 = [$(DbGlobal)].dbo.WtAvg(PPStatus_7, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_8 = [$(DbGlobal)].dbo.WtAvg(PPStatus_8, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPStatus_9 = [$(DbGlobal)].dbo.WtAvg(PPStatus_9, CASE WHEN PPStatus_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_1 = [$(DbGlobal)].dbo.WtAvg(PPTools_1, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_2 = [$(DbGlobal)].dbo.WtAvg(PPTools_2, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_3 = [$(DbGlobal)].dbo.WtAvg(PPTools_3, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_4 = [$(DbGlobal)].dbo.WtAvg(PPTools_4, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_5 = [$(DbGlobal)].dbo.WtAvg(PPTools_5, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_6 = [$(DbGlobal)].dbo.WtAvg(PPTools_6, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_7 = [$(DbGlobal)].dbo.WtAvg(PPTools_7, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_8 = [$(DbGlobal)].dbo.WtAvg(PPTools_8, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPTools_9 = [$(DbGlobal)].dbo.WtAvg(PPTools_9, CASE WHEN PPTools_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_1 = [$(DbGlobal)].dbo.WtAvg(PPExper_1, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_2 = [$(DbGlobal)].dbo.WtAvg(PPExper_2, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_3 = [$(DbGlobal)].dbo.WtAvg(PPExper_3, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_4 = [$(DbGlobal)].dbo.WtAvg(PPExper_4, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_5 = [$(DbGlobal)].dbo.WtAvg(PPExper_5, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_6 = [$(DbGlobal)].dbo.WtAvg(PPExper_6, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_7 = [$(DbGlobal)].dbo.WtAvg(PPExper_7, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_8 = [$(DbGlobal)].dbo.WtAvg(PPExper_8, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, PPExper_9 = [$(DbGlobal)].dbo.WtAvg(PPExper_9, CASE WHEN PPExper_RPT > 0.0 THEN 1.0 ELSE 0.0 END) * 100.0
, ExeFeedPurchase = [$(DbGlobal)].dbo.WtAvg(ExeFeedPurchase, 1.0)
, YPS_RPT = [$(DbGlobal)].dbo.WtAvg(YPS_RPT, 1.0)
, LP_RPT = [$(DbGlobal)].dbo.WtAvg(LP_RPT, 1.0)
, BeyondCrack_RPT = [$(DbGlobal)].dbo.WtAvg(BeyondCrack_RPT, 1.0)
, Data_RPT = [$(DbGlobal)].dbo.WtAvg(Data_RPT, 1.0)
, Dev_RPT = [$(DbGlobal)].dbo.WtAvg(Dev_RPT, 1.0)
, Valid_RPT = [$(DbGlobal)].dbo.WtAvg(Valid_RPT, 1.0)
, Constraint_RPT = [$(DbGlobal)].dbo.WtAvg(Constraint_RPT, 1.0)
, FeedPrice_RPT = [$(DbGlobal)].dbo.WtAvg(FeedPrice_RPT, 1.0)
, ProdPrice_RPT = [$(DbGlobal)].dbo.WtAvg(ProdPrice_RPT, 1.0)
, PPCom_RPT = [$(DbGlobal)].dbo.WtAvg(PPCom_RPT, 1.0)
, PPFeedPrice_RPT = [$(DbGlobal)].dbo.WtAvg(PPFeedPrice_RPT, 1.0)
, PPFeedCrack_RPT = [$(DbGlobal)].dbo.WtAvg(PPFeedCrack_RPT, 1.0)
, PPFeedAvail_RPT = [$(DbGlobal)].dbo.WtAvg(PPFeedAvail_RPT, 1.0)
, PPProdPrice_RPT = [$(DbGlobal)].dbo.WtAvg(PPProdPrice_RPT, 1.0)
, PPProdDemand_RPT = [$(DbGlobal)].dbo.WtAvg(PPProdDemand_RPT, 1.0)
, PPProdStatus_RPT = [$(DbGlobal)].dbo.WtAvg(PPProdStatus_RPT, 1.0)
, PPExper_RPT = [$(DbGlobal)].dbo.WtAvg(PPExper_RPT, 1.0)
, PPTools_RPT = [$(DbGlobal)].dbo.WtAvg(PPTools_RPT, 1.0)
, PPStatus_RPT = [$(DbGlobal)].dbo.WtAvg(PPStatus_RPT, 1.0)
, Use_ScheduleModel = [$(DbGlobal)].dbo.WtAvg(CASE WHEN (Use_ScheduleModel = 'Y' OR Lvl_ScheduleModel IS NOT NULL) THEN 100.0 ELSE 0 END, 1.0)
, Lvl_ScheduleModel_P = [$(DbGlobal)].dbo.WtAvg(CASE WHEN ((Lvl_ScheduleModel = 'P' OR Lvl_ScheduleModel IS NULL) AND Use_ScheduleModel = 'Y') THEN 100.0 ELSE 0 END, 1.0)
, Lvl_ScheduleModel_C = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_ScheduleModel = 'C' THEN 100.0 ELSE 0 END, 1.0)
, Lvl_ScheduleModel_B = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Lvl_ScheduleModel = 'B' THEN 100.0 ELSE 0 END, 1.0)


FROM reports.GroupPlants r JOIN dbo.FeedResources f on f.Refnum=r.Refnum
GROUP by r.GroupId


