﻿
CREATE PROCEDURE [reports].[Insert_GapProductPurity]
(
	@GroupId	VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @MyGroup TABLE 
		(
			[GroupId]	VARCHAR(25)	NOT NULL,
			[Refnum]	VARCHAR(25)	NOT NULL
			PRIMARY KEY CLUSTERED ([Refnum] ASC)
		);

		INSERT @MyGroup ([GroupId], [Refnum])
		SELECT
			gp.[GroupId],
			gp.[Refnum]
		FROM [reports].[GroupPlants]	gp
		WHERE gp.[GroupId] = @GroupId;

		INSERT INTO [reports].[GapProductPurity]
		(
			[GroupId],
			[Hydrogen_WtPcnt],
			[Methane_WtPcnt],
			[EthyleneCG_WtPcnt],
			[EthylenePG_WtPcnt],
			[PropyleneCG_WtPcnt],
			[PropylenePG_WtPcnt],
			[PropyleneRG_WtPcnt],
			[PropaneC3Resid_WtPcnt]
		)
		SELECT
			g.[GroupId],
			[$(DbGlobal)].[dbo].[WtAvgNN]([Hydrogen_WtPcnt],			y.Hydrogen),
			[$(DbGlobal)].[dbo].[WtAvgNN]([Methane_WtPcnt],			y.Methane),
			[$(DbGlobal)].[dbo].[WtAvgNN]([EthyleneCG_WtPcnt],			y.EthyleneCG),
			[$(DbGlobal)].[dbo].[WtAvgNN]([EthylenePG_WtPcnt],			y.EthylenePG),
			[$(DbGlobal)].[dbo].[WtAvgNN]([PropyleneCG_WtPcnt],		y.PropyleneCG),
			[$(DbGlobal)].[dbo].[WtAvgNN]([PropylenePG_WtPcnt],		y.PropylenePG),
			[$(DbGlobal)].[dbo].[WtAvgNN]([PropyleneRG_WtPcnt],		y.PropyleneRG),
			[$(DbGlobal)].[dbo].[WtAvgNN]([PropaneC3Resid_WtPcnt],		y.PropaneC3Resid)
		FROM @MyGroup								g
		INNER JOIN [dbo].[GapProductPurity]			d
			ON	d.[Refnum]	= g.[Refnum]
		INNER JOIN dbo.Yield						y
			ON	y.Refnum	= g.Refnum
			AND y.DataType = 'kMT'
		GROUP BY
			g.[GroupId];

	END TRY
	BEGIN CATCH

		SET @GroupId = 'G:' + @GroupId;
		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
