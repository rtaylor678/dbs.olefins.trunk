﻿
CREATE PROCEDURE [reports].[Insert_GapEnergySeparation]
(
	@GroupId	VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @MyGroup TABLE 
		(
			[GroupId]	VARCHAR(25)	NOT NULL,
			[Refnum]	VARCHAR(25)	NOT NULL
			PRIMARY KEY CLUSTERED ([Refnum] ASC)
		);

		INSERT @MyGroup ([GroupId], [Refnum])
		SELECT
			gp.[GroupId],
			gp.[Refnum]
		FROM [reports].[GroupPlants]	gp
		WHERE gp.[GroupId] = @GroupId;

		INSERT INTO [reports].[GapEnergySeparation]
		(
			[GroupId],
			[DataType],
			[StandardEnergyTot],
			[PlantFeed],
			[FreshPyroFeed],
			[SuppTot],
			[SuppConc],
			[ConcEthylene],
			[ConcPropylene],
			[ConcBenzene],
			[ConcButadiene],
			[SuppROG],
			[ROGHydrogen],
			[ROGMethane],
			[ROGEthane],
			[ROGEthylene],
			[ROGPropane],
			[ROGPropylene],
			[ROGC4Plus],
			[ROGInerts],
			[SuppDil],
			[DilHydrogen],
			[DilMethane],
			[DilEthane],
			[DilEthylene],
			[DilPropane],
			[DilPropylene],
			[DilBenzene],
			[DilButadiene],
			[DilButane],
			[DilButylene],
			[DilMoGas],
			[SuppMisc],
			[SuppGasOil],
			[SuppWashOil],
			[SuppOther],
			[Vessel],
			[FracFeed],
			[TowerPyroGasHT],
			[Benzene],
			[PyroGasoline],
			[HydroPur],
			[Reduction],
			[RedPropylene],
			[RedPropyleneCG],
			[RedPropyleneRG],
			[RedEthylene],
			[RedEthyleneCG],
			[RedCrackedGasTrans]
		)
		SELECT
			g.[GroupId],
			d.[DataType],
			[$(DbGlobal)].[dbo].[WtAvg](d.[StandardEnergyTot], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[PlantFeed], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FreshPyroFeed], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppTot], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppConc], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ConcEthylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ConcPropylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ConcBenzene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ConcButadiene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppROG], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGHydrogen], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGMethane], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGEthane], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGEthylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGPropane], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGPropylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGC4Plus], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ROGInerts], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppDil], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilHydrogen], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilMethane], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilEthane], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilEthylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilPropane], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilPropylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilBenzene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilButadiene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilButane], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilButylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[DilMoGas], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppMisc], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppGasOil], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppWashOil], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[SuppOther], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Vessel], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FracFeed], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[TowerPyroGasHT], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Benzene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[PyroGasoline], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[HydroPur], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Reduction], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[RedPropylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[RedPropyleneCG], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[RedPropyleneRG], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[RedEthylene], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[RedEthyleneCG], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[RedCrackedGasTrans], 1.0)
		FROM @MyGroup							g
		INNER JOIN [dbo].[GapEnergySeparation]	d
			ON	d.[Refnum]	= g.[Refnum]
		GROUP BY
			g.[GroupId],
			d.[DataType];

	END TRY
	BEGIN CATCH

		SET @GroupId = 'G:' + @GroupId;
		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
