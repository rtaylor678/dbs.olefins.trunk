﻿CREATE TABLE [q].[Simulation] (
    [QueueId]        BIGINT             IDENTITY (1, 1) NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [SimModelId]     VARCHAR (12)       NOT NULL,
    [tsCreated]      DATETIMEOFFSET (7) CONSTRAINT [DF_qSimulation_tsCreated] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsQueued]       DATETIMEOFFSET (7) NULL,
    [tsStart]        DATETIMEOFFSET (7) NULL,
    [tsComplete]     DATETIMEOFFSET (7) NULL,
    [ItemsQueued]    INT                NULL,
    [ItemsProcessed] INT                NULL,
    [ErrorID]        VARCHAR (12)       NULL,
    [Notes]          VARCHAR (MAX)      NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_qSimulation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_qSimulation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_qSimulation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_qSimulation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_qSimulation] PRIMARY KEY CLUSTERED ([QueueId] ASC),
    CONSTRAINT [FK_qSimulation_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_qSimulation_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId])
);


GO

CREATE TRIGGER [q].[t_Simulation_u]
	ON [q].[Simulation]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [q].[Simulation]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE [q].[Simulation].QueueId = INSERTED.QueueId;

END;