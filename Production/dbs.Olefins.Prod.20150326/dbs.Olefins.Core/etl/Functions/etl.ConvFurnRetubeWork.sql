﻿
CREATE FUNCTION etl.ConvFurnRetubeWork
(
	@FurnRetubeWorkID	NVARCHAR(24)
)
RETURNS NVARCHAR(24)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(24) =
	CASE @FurnRetubeWorkID
		WHEN 'company  employees'			THEN 'Company'
		WHEN 'company employee'				THEN 'Company'
		WHEN 'company employees'			THEN 'Company'
		WHEN 'company employes'				THEN 'Company'
		WHEN 'company'						THEN 'Company'
		WHEN 'contractor'					THEN 'General'
		WHEN 'Contractors'					THEN 'General'
		WHEN 'EMP/SPCLITYCONTR'				THEN 'CompSpec'
		WHEN 'employees'					THEN 'Company'
		WHEN 'G.C.'							THEN 'General'
		WHEN 'GC'							THEN 'General'
		WHEN 'Gen Contr'					THEN 'General'
		WHEN 'gen. contr.'					THEN 'General'
		WHEN 'GEN.CONT'						THEN 'General'
		WHEN 'Gen/specialty contr'			THEN 'GenSpec'
		WHEN 'Gen/specialty contrac'		THEN 'GenSpec'
		WHEN 'General & specialty contr'	THEN 'GenSpec'
		WHEN 'general constractor'			THEN 'General'
		WHEN 'general cont.'				THEN 'General'
		WHEN 'general contractor'			THEN 'General'
		WHEN 'general contractors'			THEN 'General'
		WHEN 'GENERAL'						THEN 'General'
		WHEN 'Nova/ Contract'				THEN 'CompGen'
		WHEN 'Own + contractors'			THEN 'CompGen'
		WHEN 'SC'							THEN 'Speciality'
		WHEN 'SP. CONT'						THEN 'Speciality'
		WHEN 'spec cont'					THEN 'Speciality'
		WHEN 'SPECIAL CONTRACTORS'			THEN 'Speciality'
		WHEN 'speciality    contractors'	THEN 'Speciality'
		WHEN 'speciality contractor'		THEN 'Speciality'
		WHEN 'speciality contractors'		THEN 'Speciality'
		WHEN 'SPECIALITY CONTRACTTOR'		THEN 'Speciality'
		WHEN 'Speciality'					THEN 'Speciality'
		WHEN 'Specialty Contractor'			THEN 'Speciality'
		WHEN 'Specialty Contractors'		THEN 'Speciality'
		WHEN 'specialty'					THEN 'Speciality'
		ELSE NULL
	END;
	
	RETURN @ResultVar;
	
END;