﻿CREATE FUNCTION [etl].[ValidateString]
(
	@Old				NVARCHAR(MAX),
	@New				NVARCHAR(MAX),
	@AllowNullToText	BIT
)
RETURNS BIT
AS
BEGIN

	DECLARE @r BIT = 0;

	IF (UPPER(RTRIM(LTRIM(@Old))) = UPPER(RTRIM(LTRIM(@New))))
		OR (@AllowNullToText = 0 AND @Old IS NULL AND @New IS NULL)
		OR (@AllowNullToText = 1 AND @Old IS NULL)
		SET @r = 1;
	ELSE
		SET @r = 0;

	RETURN @r;

END