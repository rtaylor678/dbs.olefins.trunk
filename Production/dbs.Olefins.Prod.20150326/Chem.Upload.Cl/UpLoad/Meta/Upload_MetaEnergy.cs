﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadMetaEnergy(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			const UInt32 r = 53;
			UInt32 c = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table13"];

				rng = wks.Range["F53:I53"];
				if (RangeHasValue(rng))
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_MetaEnergy]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum				CHAR (9)
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@EnergyType			CHAR (12)
					cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = "IEC";

					//@Qtr1			REAL		= NULL,
					c = 6;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr1", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@Qtr2			REAL		= NULL,
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr2", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@Qtr3			REAL		= NULL,
					c = 8;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr3", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@Qtr4			REAL		= NULL,
					c = 9;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr4", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@Total		REAL		= NULL
					c = 10;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Total", SqlDbType.Float).Value = ReturnFloat(rng); }

					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMetaEnergy", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_MetaEnergy]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
