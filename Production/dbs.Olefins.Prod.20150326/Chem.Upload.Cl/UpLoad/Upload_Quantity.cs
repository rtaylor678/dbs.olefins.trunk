﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadQuantity(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;
			UInt32 cBeg = 0;
			UInt32 cEnd = 0;

			object[] itm;

			string sht = string.Empty;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());
			cn.Open();

			#region Fresh Feeds

			itm = new object[3] 
			{
				new object[3] { "Table2A-1", 3, 8 },
				new object[3] { "Table2A-2", 3, 14 },
				new object[3] { "Table2B", 3, 5 }
			};

			foreach (object[] s in itm)
			{
				sht = (string)s[0];

				wks = wkb.Worksheets[sht];

				cBeg = Convert.ToUInt32(s[1]);
				cEnd = Convert.ToUInt32(s[2]);

				for (c = cBeg; c <= cEnd; c++)
				{
					try
					{
						r = 36;

						if (wks.Name == "Table2A-2") { r++; };

						if ((double?)wks.Cells[r, c].Value +
							(double?)wks.Cells[++r, c].Value +
							(double?)wks.Cells[++r, c].Value +
							(double?)wks.Cells[++r, c].Value +
							(double?)wks.Cells[++r, c].Value > 0.0f)
						{
							SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Quantity]", cn);
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum				CHAR (9),
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID	VARCHAR (20),
							r = 4;
							rng = wks.Cells[r, c];
							cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = ConvertFeedProdId(rng, c);

							#region Quantity

							r = 36;

							if (wks.Name == "Table2A-2") { r++; };

							//@Q1Feed			REAL	= NULL,
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q1Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Q2Feed			REAL	= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q2Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Q3Feed			REAL	= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q3Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Q4Feed			REAL	= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q4Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@AnnFeedProd		REAL	= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@AnnFeedProd", SqlDbType.Float).Value = ReturnFloat(rng); }

							#endregion

							#region Misc Feed Name

							//@MiscFeed    VARCHAR (35) = NULL,
							r = 4;
							rng = wks.Cells[r, c];
							if ((wks.Name == "Table2A-1" && c == 8) || (wks.Name == "Table2A-2" && (c == 12 || c == 13 || c == 14)) && RangeHasValue(rng))
							{ cmd.Parameters.Add("@MiscFeed", SqlDbType.VarChar, 35).Value = ReturnString(rng, 35); }

							#endregion

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadQuantity (Fresh Feeds)", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Quantity]", ex);
					}
				}
			}

			rng = null;
			wks = null;
			itm = null;

			#endregion

			#region Supplemental Feeds and Products

			itm = new object[4]
			{
				new object[5] { "Table2C", 10, 38, 3, 8 },
				new object[5] { "Table3", 9, 31, 4, 8 },
				new object[5] { "Table3", 37, 38, 4, 8 },
				new object[5] { "Table3", 40, 42, 4, 8 }
			};

			foreach (object[] s in itm)
			{
				sht = (string)s[0];

				wks = wkb.Worksheets[sht];

				rBeg = Convert.ToUInt32(s[1]);
				rEnd = Convert.ToUInt32(s[2]);
				cBeg = Convert.ToUInt32(s[3]);
				cEnd = Convert.ToUInt32(s[4]);

				for (r = rBeg; r <= rEnd; r++)
				{
					try
					{
						c = cBeg;

						if ((double?)wks.Cells[r, c].Value +
							(double?)wks.Cells[r, ++c].Value +
							(double?)wks.Cells[r, ++c].Value +
							(double?)wks.Cells[r, ++c].Value +
							(double?)wks.Cells[r, ++c].Value > 0.0f)
						{
							c = cBeg;

							SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Quantity]", cn);
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum				CHAR (9),
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID	VARCHAR (20),
							rng = wks.Cells[r, c];
							cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = ConvertSupplementalID(rng);

							#region Quantity

							//@Q1Feed			REAL	= NULL,
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q1Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Q2Feed			REAL	= NULL,
							c++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q2Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Q3Feed			REAL	= NULL,
							c++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q3Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Q4Feed			REAL	= NULL,
							c++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Q4Feed", SqlDbType.Float).Value = ReturnFloat(rng); }

							#endregion

							#region Annual Quantity

							//@AnnFeedProd REAL = NULL,
							c = cBeg;
							if ((double?)wks.Cells[r, c].Value +
								(double?)wks.Cells[r, ++c].Value +
								(double?)wks.Cells[r, ++c].Value +
								(double?)wks.Cells[r, ++c].Value +
								(double?)wks.Cells[r, ++c].Value > 0.0f)
							{
								c = cBeg;
								double annQ = (double)((double?)wks.Cells[r, c].Value +
										(double?)wks.Cells[r, ++c].Value +
										(double?)wks.Cells[r, ++c].Value +
										(double?)wks.Cells[r, ++c].Value);

								cmd.Parameters.Add("@AnnFeedProd", SqlDbType.Float).Value = annQ;

							}

							#endregion

							if (wks.Name == "Table2C")
							{
								//@RecPcnt     REAL         = NULL,
								c = cEnd;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@RecPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }
							}

							if (wks.Name == "Table3")
							{
								c = 3;

								//@OthProdDesc VARCHAR (35) = NULL,
								rng = wks.Cells[r, c];
								if ((r == 37 || r == 38) && RangeHasValue(rng)) { cmd.Parameters.Add("@OthProdDesc", SqlDbType.VarChar, 35).Value = ReturnString(rng, 35); }


								//@MiscProd1   VARCHAR (50) = NULL,
								rng = wks.Cells[r, c];
								if (r == 37 && RangeHasValue(rng)) { cmd.Parameters.Add("@MiscProd1", SqlDbType.VarChar, 50).Value = ReturnString(rng, 50); }

								//@MiscProd2   VARCHAR (50) = NULL
								rng = wks.Cells[r, c];
								if (r == 38 && RangeHasValue(rng)) { cmd.Parameters.Add("@MiscProd2", SqlDbType.VarChar, 50).Value = ReturnString(rng, 50); }
							}

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadQuantity (Supplemental)", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Quantity]", ex);
					}
				}
			}

			rng = null;
			wks = null;
			itm = null;

			#endregion

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
		}
	}
}
