﻿namespace Chem.UpLoad
{
    partial class ControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.UploadOne = new System.Windows.Forms.Button();
			this.UpLoadLoop = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// UploadOne
			// 
			this.UploadOne.Location = new System.Drawing.Point(12, 12);
			this.UploadOne.Name = "UploadOne";
			this.UploadOne.Size = new System.Drawing.Size(214, 23);
			this.UploadOne.TabIndex = 0;
			this.UploadOne.Text = "Upload One File";
			this.UploadOne.UseVisualStyleBackColor = true;
			this.UploadOne.Click += new System.EventHandler(this.UpLoadOne_Click);
			// 
			// UpLoadLoop
			// 
			this.UpLoadLoop.Location = new System.Drawing.Point(12, 42);
			this.UpLoadLoop.Name = "UpLoadLoop";
			this.UpLoadLoop.Size = new System.Drawing.Size(214, 23);
			this.UpLoadLoop.TabIndex = 1;
			this.UpLoadLoop.Text = "Upload Folder";
			this.UpLoadLoop.UseVisualStyleBackColor = true;
			this.UpLoadLoop.Click += new System.EventHandler(this.UpLoadFolder_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(12, 150);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(394, 20);
			this.textBox1.TabIndex = 4;
			// 
			// ControlForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(418, 254);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.UpLoadLoop);
			this.Controls.Add(this.UploadOne);
			this.Name = "ControlForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ControlForm";
			this.Load += new System.EventHandler(this.ControlForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UploadOne;
		private System.Windows.Forms.Button UpLoadLoop;
		private System.Windows.Forms.TextBox textBox1;
    }
}