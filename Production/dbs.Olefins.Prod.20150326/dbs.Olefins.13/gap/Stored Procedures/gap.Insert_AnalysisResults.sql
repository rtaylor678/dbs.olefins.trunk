﻿CREATE PROCEDURE [gap].[Insert_AnalysisResults]
(
	@GroupId		VARCHAR(25),
	@TargetId		VARCHAR(25),
	@DatabaseName	NVARCHAR(128)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	DECLARE @FactorSetId	VARCHAR(12);

	SELECT TOP 1 @FactorSetId = t.[FactorSetId]
	FROM [cons].[TSortSolomon]	[t]
	WHERE	[t].[Refnum]	= @GroupId;
	
	IF (@DatabaseName = 'Olefins13') EXECUTE [gap].[Insert_AnalysisResults2013] @FactorSetId, @GroupId, @TargetId;

END;