﻿CREATE TABLE [dbo].[ProdLoss_LU] (
    [CauseID]     VARCHAR (20) NOT NULL,
    [Category]    CHAR (3)     NULL,
    [Description] VARCHAR (55) NULL,
    [SortKey]     TINYINT      NULL,
    CONSTRAINT [PK___4__19] PRIMARY KEY CLUSTERED ([CauseID] ASC)
);

