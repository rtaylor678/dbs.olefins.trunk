﻿CREATE TABLE [dim].[Standard_LookUp] (
    [StandardId]     INT                IDENTITY (1, 1) NOT NULL,
    [StandardTag]    VARCHAR (42)       NOT NULL,
    [StandardName]   VARCHAR (84)       NOT NULL,
    [StandardDetail] VARCHAR (256)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Standard_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Standard_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Standard_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Standard_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardTypes_LookUp] PRIMARY KEY CLUSTERED ([StandardId] ASC),
    CONSTRAINT [CL_Standard_LookUp_StandardDetail] CHECK ([StandardDetail]<>''),
    CONSTRAINT [CL_Standard_LookUp_StandardName] CHECK ([StandardName]<>''),
    CONSTRAINT [CL_Standard_LookUp_StandardTag] CHECK ([StandardTag]<>''),
    CONSTRAINT [UK_Standard_LookUp_StandardDetail] UNIQUE NONCLUSTERED ([StandardDetail] ASC),
    CONSTRAINT [UK_Standard_LookUp_StandardName] UNIQUE NONCLUSTERED ([StandardName] ASC),
    CONSTRAINT [UK_Standard_LookUp_StandardTag] UNIQUE NONCLUSTERED ([StandardTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_Standard_LookUp_u]
	ON [dim].[Standard_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Standard_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Standard_LookUp].[StandardId]	= INSERTED.[StandardId];

END;