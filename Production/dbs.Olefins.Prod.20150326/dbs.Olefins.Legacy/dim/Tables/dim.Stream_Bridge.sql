﻿CREATE TABLE [dim].[Stream_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [StreamId]           INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Stream_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Stream_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Stream_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Stream_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Stream_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Stream_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_Stream_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Stream_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Stream_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Stream_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [StreamId]) REFERENCES [dim].[Stream_Parent] ([MethodologyId], [StreamId]),
    CONSTRAINT [FK_Stream_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[Stream_Parent] ([MethodologyId], [StreamId]),
    CONSTRAINT [FK_Stream_Bridge_StreamID] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [dim].[t_Stream_Bridge_u]
	ON [dim].[Stream_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Stream_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Stream_Bridge].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Stream_Bridge].[StreamId]		= INSERTED.[StreamId]
		AND	[dim].[Stream_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;