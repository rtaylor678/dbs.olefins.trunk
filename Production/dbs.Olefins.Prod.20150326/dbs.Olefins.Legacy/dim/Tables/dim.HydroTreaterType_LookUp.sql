﻿CREATE TABLE [dim].[HydroTreaterType_LookUp] (
    [HydroTreaterTypeId]     INT                IDENTITY (1, 1) NOT NULL,
    [HydroTreaterTypeTag]    VARCHAR (42)       NOT NULL,
    [HydroTreaterTypeName]   VARCHAR (84)       NOT NULL,
    [HydroTreaterTypeDetail] VARCHAR (256)      NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_HydroTreaterType_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (128)     CONSTRAINT [DF_HydroTreaterType_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (128)     CONSTRAINT [DF_HydroTreaterType_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (128)     CONSTRAINT [DF_HydroTreaterType_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_HydroTreaterType_LookUp] PRIMARY KEY CLUSTERED ([HydroTreaterTypeId] ASC),
    CONSTRAINT [CL_HydroTreaterType_LookUp_HydroTreaterTypeDetail] CHECK ([HydroTreaterTypeDetail]<>''),
    CONSTRAINT [CL_HydroTreaterType_LookUp_HydroTreaterTypeName] CHECK ([HydroTreaterTypeName]<>''),
    CONSTRAINT [CL_HydroTreaterType_LookUp_HydroTreaterTypeTag] CHECK ([HydroTreaterTypeTag]<>''),
    CONSTRAINT [UK_HydroTreaterType_LookUp_HydroTreaterTypeDetail] UNIQUE NONCLUSTERED ([HydroTreaterTypeDetail] ASC),
    CONSTRAINT [UK_HydroTreaterType_LookUp_HydroTreaterTypeName] UNIQUE NONCLUSTERED ([HydroTreaterTypeName] ASC),
    CONSTRAINT [UK_HydroTreaterType_LookUp_HydroTreaterTypeTag] UNIQUE NONCLUSTERED ([HydroTreaterTypeTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_HydroTreaterType_LookUp_u]
	ON [dim].[HydroTreaterType_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[HydroTreaterType_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[HydroTreaterType_LookUp].[HydroTreaterTypeId]		= INSERTED.[HydroTreaterTypeId];

END;