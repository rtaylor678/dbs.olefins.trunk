﻿CREATE TABLE [dim].[Company_LookUp] (
    [CompanyId]      INT                IDENTITY (1, 1) NOT NULL,
    [CompanyTag]     VARCHAR (42)       NOT NULL,
    [CompanyName]    VARCHAR (84)       NOT NULL,
    [CompanyDetail]  VARCHAR (256)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Company_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Company_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Company_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Company_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Company_LookUp] PRIMARY KEY CLUSTERED ([CompanyId] ASC),
    CONSTRAINT [CL_Company_LookUp_CompanyDetail] CHECK ([CompanyDetail]<>''),
    CONSTRAINT [CL_Company_LookUp_CompanyName] CHECK ([CompanyName]<>''),
    CONSTRAINT [CL_Company_LookUp_CompanyTag] CHECK ([CompanyTag]<>''),
    CONSTRAINT [UK_Company_LookUp_CompanyDetail] UNIQUE NONCLUSTERED ([CompanyDetail] ASC),
    CONSTRAINT [UK_Company_LookUp_CompanyName] UNIQUE NONCLUSTERED ([CompanyName] ASC),
    CONSTRAINT [UK_Company_LookUp_CompanyTag] UNIQUE NONCLUSTERED ([CompanyTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_Company_LookUp_u]
	ON [dim].[Company_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Company_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Company_LookUp].[CompanyId]		= INSERTED.[CompanyId];

END;