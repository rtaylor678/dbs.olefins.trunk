﻿CREATE TABLE [dim].[Facility_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [FacilityId]         INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Facility_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Facility_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Facility_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Facility_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Facility_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Facility_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FacilityId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_Facility_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_Facility_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Facility_Bridge_FacilityId] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_Facility_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Facility_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [FacilityId]) REFERENCES [dim].[Facility_Parent] ([MethodologyId], [FacilityId]),
    CONSTRAINT [FK_Facility_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[Facility_Parent] ([MethodologyId], [FacilityId])
);


GO

CREATE TRIGGER [dim].[t_Facility_Bridge_u]
	ON [dim].[Facility_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Facility_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Facility_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Facility_Bridge].[FacilityId]		= INSERTED.[FacilityId]
		AND	[dim].[Facility_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;