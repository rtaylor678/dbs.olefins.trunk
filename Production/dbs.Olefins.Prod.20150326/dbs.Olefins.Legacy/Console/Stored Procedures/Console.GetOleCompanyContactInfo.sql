﻿CREATE PROCEDURE [Console].[GetOleCompanyContactInfo] 
	@RefNum dbo.Refnum, 
	@ContactType nvarchar(10),
	@StudyYear int
	AS
	

BEGIN
IF @ContactType='COORD'  
BEGIN
 

				SELECT FirstName ,
			LastName ,
			ContactType,
			cc.Email ,
			cc.Phone ,
			cc.Fax ,
			cc.MailAddr1 ,
			cc.MailAddr2 ,
			cc.MailAddr3 ,
			MailCity  ,
			MailState ,
			MailZip ,
			MailCountry ,
			StrAddr1,
			StrAddr2,
			StrAdd3,
			StrCity,
			StrState,
			StrZip,
			StrCountry,
			JobTitle,
			SendMethod,
			t.Password as CompanyPassword,
			SendMethod as CompanySendMethod,
			Comment,
			cc.AltFirstName,
			cc.AltLastName,
			cc.AltEmail,
			null as SANNumber
		 FROM CoContactInfo cc right outer join tsort t on t.ContactCode = cc.contactcode 
			WHERE t.Refnum=@RefNum  and ContactType=@ContactType and cc.StudyYear = @StudyYear
			END
			ELSE 
			BEGIN
			SET @ContactType = 'ALT'
						SELECT FirstName ,
			LastName ,
			ContactType,
			cc.Email ,
			cc.Phone ,
			cc.Fax ,
			cc.MailAddr1 ,
			cc.MailAddr2 ,
			cc.MailAddr3 ,
			MailCity  ,
			MailState ,
			MailZip ,
			MailCountry ,
			StrAddr1,
			StrAddr2,
			StrAdd3,
			StrCity,
			StrState,
			StrZip,
			StrCountry,
			JobTitle,
			SendMethod,
			t.Password as CompanyPassword,
			SendMethod as CompanySendMethod,
			Comment,
			cc.AltFirstName,
			cc.AltLastName,
			cc.AltEmail,
			null as SANNumber
		 FROM CoContactInfo cc right outer join tsort t on t.ContactCode = cc.contactcode and t.StudyYear = cc.StudyYear
			WHERE t.Refnum=@RefNum  and ContactType=@ContactType and cc.StudyYear = @StudyYear
			END
END
