﻿CREATE TABLE [ante].[MapFactorFeedClass] (
    [MethodologyId]  INT                NOT NULL,
    [FeedClassId]    INT                NOT NULL,
    [FactorId]       INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapFactorFeedClass_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_MapFactorFeedClass_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_MapFactorFeedClass_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_MapFactorFeedClass_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapFactorFeedClass] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FeedClassId] ASC, [FactorId] ASC),
    CONSTRAINT [FK_MapFactorFeedClass_Factor_LookUp] FOREIGN KEY ([FactorId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_MapFactorFeedClass_FeedClass_LookUp] FOREIGN KEY ([FeedClassId]) REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId]),
    CONSTRAINT [FK_MapFactorFeedClass_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId])
);


GO

CREATE TRIGGER [ante].[t_MapFactorFeedClass_u]
	ON [ante].[MapFactorFeedClass]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorFeedClass]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorFeedClass].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorFeedClass].[FeedClassId]	= INSERTED.[FeedClassId]
		AND	[ante].[MapFactorFeedClass].[FactorId]		= INSERTED.[FactorId];

END;