﻿CREATE TABLE [ante].[StdEnergyFractionator] (
    [MethodologyId]  INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [Energy_kBtuBbl] FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_StdEnergyFractionator_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_StdEnergyFractionator_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_StdEnergyFractionator_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_StdEnergyFractionator_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StdEnergyFractionator] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC),
    CONSTRAINT [CR_StdEnergyFractionator_Energy_kBtuBbl_MinIncl_0.0] CHECK ([Energy_kBtuBbl]>=(0.0)),
    CONSTRAINT [FK_StdEnergyFractionator_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StdEnergyFractionator_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_StdEnergyFractionator_u]
	ON [ante].[StdEnergyFractionator]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[StdEnergyFractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[StdEnergyFractionator].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[StdEnergyFractionator].[StreamId]		= INSERTED.[StreamId];

END;