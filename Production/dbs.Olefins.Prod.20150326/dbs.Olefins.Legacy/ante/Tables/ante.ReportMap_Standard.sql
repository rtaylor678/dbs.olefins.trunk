﻿CREATE TABLE [ante].[ReportMap_Standard] (
    [MethodologyId]  INT                NOT NULL,
    [StandardId]     INT                NOT NULL,
    [Report_Prefix]  VARCHAR (42)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReportMap_Standard_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_ReportMap_Standard_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_ReportMap_Standard_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_ReportMap_Standard_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReportMap_Standard] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC),
    CONSTRAINT [CL_ReportMap_Standard_Report_Prefix] CHECK ([Report_Prefix]<>''),
    CONSTRAINT [FK_ReportMap_Standard_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_ReportMap_Standard_Standard] FOREIGN KEY ([StandardId]) REFERENCES [dim].[Standard_LookUp] ([StandardId])
);


GO

CREATE TRIGGER [ante].[t_ReportMap_Standard_u]
	ON [ante].[ReportMap_Standard]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ReportMap_Standard]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[ReportMap_Standard].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[ReportMap_Standard].[StandardId]	= INSERTED.[StandardId];

END;