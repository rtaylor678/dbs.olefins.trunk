﻿CREATE PROCEDURE [stage].[Insert_SubmissionComments]
(
	@SubmissionId			INT,

	@SubmissionComment		NVARCHAR(MAX)
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[SubmissionComments]([SubmissionId], [SubmissionComment])
	SELECT
		@SubmissionId,
		@SubmissionComment
	WHERE	@SubmissionComment	<> '';

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
			+ COALESCE(', @SubmissionComment:'	+ CONVERT(VARCHAR, @SubmissionComment),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;