﻿CREATE TABLE [stage].[StreamDescription] (
    [SubmissionId]      INT                NOT NULL,
    [StreamNumber]      INT                NOT NULL,
    [StreamDescription] NVARCHAR (256)     NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_StreamDescription_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_StreamDescription_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_StreamDescription_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_StreamDescription_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamDescription] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC),
    CONSTRAINT [CL_StreamDescription_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_StreamDescription_StreamNumber] CHECK ([StreamNumber]>(0)),
    CONSTRAINT [FK_StreamDescription_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO


CREATE TRIGGER [stage].[t_StreamDescription_u]
	ON [stage].[StreamDescription]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamDescription]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamDescription].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[StreamDescription].[StreamNumber]	= INSERTED.[StreamNumber];

END;