﻿
/****** Object:  View dbo.vFeedPlan    Script Date: 7/17/2006 11:24:29 AM ******/








CREATE    VIEW dbo.vFeedPlan AS
SELECT t.Refnum, fp.PlanID,
  Daily= CASE WHEN fp.Daily='X' THEN 1 ELSE 0 END,
  BiWeekly= CASE WHEN fp.BiWeekly='X' THEN 1 ELSE 0 END,
  Weekly= CASE WHEN fp.Weekly='X' THEN 1 ELSE 0 END,
  BiMonthly= CASE WHEN fp.BiMonthly='X' THEN 1 ELSE 0 END,
  Monthly= CASE WHEN fp.Monthly='X' THEN 1 ELSE 0 END,
  LessMonthly= CASE WHEN fp.LessMonthly='X' THEN 1 ELSE 0 END,
  fp.Rpt
FROM FeedPlan fp
INNER JOIN TSort t ON fp.Refnum=t.Refnum











