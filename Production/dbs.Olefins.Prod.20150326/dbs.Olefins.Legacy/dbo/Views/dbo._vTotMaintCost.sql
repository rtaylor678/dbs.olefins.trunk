﻿


CREATE    View _vTotMaintCost as
Select m.Refnum	,
DirMaintMatl = SUM(RoutMaintMatl),
DirMaintLabor = SUM(RoutMaintLabor),
DirTAMatl = SUM(TAMatl),
DirTALabor = SUM(TALabor),
AllocOHMaintMatl = ISNULL(AVG(AllocOHMaintMatl),0),
AllocOHMaintLabor = ISNULL(AVG(AllocOHMaintLabor),0),
AllocOHTAMatl = ISNULL(AVG(AllocOHTAMatl),0),
AllocOHTALabor = ISNULL(AVG(AllocOHTALabor),0),
TotalMaintMatl = SUM(RoutMaintMatl) + ISNULL(AVG(AllocOHMaintMatl),0),
TotalMaintLabor = SUM(RoutMaintLabor) + ISNULL(AVG(AllocOHMaintLabor),0),
TotalTAMatl = SUM(TAMatl) + ISNULL(AVG(AllocOHTAMatl),0),
TotalTALabor = SUM(TALabor) + ISNULL(AVG(AllocOHTALabor),0),
DirMaintTot = SUM(RoutMaintMatl) + SUM(RoutMaintLabor),
DirTATot = SUM(TAMatl) + SUM(TALabor),
AllocOHMaintTot = ISNULL(AVG(AllocOHMaintMatl),0) + ISNULL(AVG(AllocOHMaintLabor),0),
AllocOHTATot = ISNULL(AVG(AllocOHTAMatl),0) + ISNULL(AVG(AllocOHTALabor),0),
TotRoutMaint = SUM(RoutMaintMatl) + SUM(RoutMaintLabor) + ISNULL(AVG(AllocOHMaintMatl),0) + ISNULL(AVG(AllocOHMaintLabor),0),
TotTAMaint = SUM(TAMatl) + SUM(TALabor) + ISNULL(AVG(AllocOHTAMatl),0) + ISNULL(AVG(AllocOHTALabor),0)
From TotMaintCost m JOIN Maint01 m01 on m01.Refnum=m.Refnum

Group by m.Refnum



