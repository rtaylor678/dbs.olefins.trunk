﻿CREATE PROCEDURE [dbo].[Process_DataSetStage]
(
	@SubmissionId			INT,
	@MethodologyId			INT = NULL
)
AS
BEGIN

SET NOCOUNT ON;

	DECLARE @Error_Count	INT = 0;

	IF (@MethodologyId IS NULL)
	SET @MethodologyId = IDENT_CURRENT('[ante].[Methodology]');

	IF (@Error_Count >= 0)	EXECUTE @Error_Count = [calc].[Delete_DataSet]	@MethodologyId,	@SubmissionId;
	IF (@Error_Count >= 0)	EXECUTE @Error_Count = [fact].[Delete_DataSet]					@SubmissionId;
	IF (@Error_Count >= 0)	EXECUTE @Error_Count = [fact].[Insert_DataSet]					@SubmissionId;
	IF (@Error_Count >= 0)	EXECUTE @Error_Count = [calc].[Insert_DataSet]	@MethodologyId,	@SubmissionId;

	RETURN @Error_Count;

END;
