﻿

CREATE PROCEDURE spDDLookups;3
	@ObjName varchar(30) ,
	@ColName varchar(30) ,
	@DescField varchar(30) ,
	@Desc varchar(100) = NULL ,
	@LookupID integer OUTPUT
AS
IF NOT EXISTS (SELECT * FROM DD_Lookups 
	WHERE ObjectName = @ObjName)
	INSERT INTO DD_Lookups (ObjectName, ColumnName, Description, DescField)
	VALUES (@ObjName, @ColName, @Desc, @DescField)
EXEC spDDLookups @ObjName, @LookupID OUTPUT


