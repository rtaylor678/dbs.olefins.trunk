﻿CREATE PROCEDURE [dbo].[Get_PGasHydrotreaterType]
AS
SELECT
	l.[HydroTreaterTypeId]		[PGasHydroTreaterTypeId],
	l.[HydroTreaterTypeName]	[PGasHydroTreaterTypeDesc]
FROM [dim].[HydroTreaterType_LookUp]	l
WHERE l.[HydroTreaterTypeId] >= 2;
