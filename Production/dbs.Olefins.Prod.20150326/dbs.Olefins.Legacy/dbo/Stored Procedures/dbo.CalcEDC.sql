﻿





-- =============================================
-- Author:		Rogge Heflin
-- Create date: 19 June 2009
-- Description:	kEDC Calculator
-- Version:		2009.06.19 a
-- =============================================
-- EXECUTE [dbo].[CalcEDC] '07PCH171', 2007

CREATE PROCEDURE [dbo].[CalcEDC](
	@Refnum Refnum,
	@FactorSet smallint
	)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE	@ratioEP		REAL		= 0.8;			-- Percent of Ethane/Propane Mix distributed to Ethane
	DECLARE	@pqFeedProdID	VarChar(8)	= 'Hydrogen';	-- 
	DECLARE @ModelVersion	varChar(10) = '2007';
	DECLARE @sgBreakPoint	REAL		= 0.7;			-- Split of Other liquids to light/heavy feeds

	IF OBJECT_ID('tempdb..#RawDataA')			IS NOT NULL BEGIN DROP TABLE #RawDataA;			END;
	IF OBJECT_ID('tempdb..#RawData')			IS NOT NULL BEGIN DROP TABLE #RawData;			END;
	IF OBJECT_ID('tempdb..#kEDCCalculations')	IS NOT NULL BEGIN DROP TABLE #kEDCCalculations;	END;
	
	-------------------------------------------------------------------------------------------------------------------
	-- Retrieve Raw data values used as basis for kEDC calculation
	
	SELECT feedQuant.*
		, IsNull(feedQual.SG1, 0) AS 'SG1', IsNull(feedQual.SG2, 0) AS 'SG2', IsNull(feedQual.SG3, 0) AS 'SG3'
		
		-- Aggregate the feed components to create Fresh Feed (used when FreshFeedDiv is null
		, IsNull(Ethane, 0) + IsNull(EPMix, 0) + IsNull(Propane, 0) + IsNull(Butane, 0) + IsNull(LPG, 0) + IsNull(OthLtFeed, 0) + IsNull(HeavyNGL, 0) + IsNull(Condensate, 0) + IsNull(FRNaphtha, 0) + IsNull(LtNaphtha, 0) + IsNull(Raffinate, 0) + IsNull(HeavyNaphtha, 0) + IsNull(Diesel, 0) + IsNull(HeavyGasoil, 0) + IsNull(HTGasoil, 0) + IsNull(OthLiqFeed1, 0) + IsNull(OthLiqFeed2, 0) + IsNull(OthLiqFeed3, 0) AS 'FreshFeed'
		
		-- Aggregate the Supplemental Feeds
		, IsNull(ConcEthylene, 0) + IsNull(DiEthylene, 0) + IsNull(RefEthylene, 0) + IsNull(ConcPropylene, 0) + IsNull(DiPropylene, 0) + IsNull(RefPropylene, 0) AS 'SupplEP'
		
		, IsNull(Hydrogen * PQ.MolePcnt / ( 8 - 7 * PQ.MolePcnt), 0)				AS 'ContH2Sold'
		, IsNull(Hydrogen / ( 8 - 7 * PQ.MolePcnt) * 2204.6 * 379 / 2 / 365, 0)		AS 'Stoichiometry'
		
	INTO	#RawDataA
	
	FROM	(
	SELECT	Refnum, FeedProdID, AnnFeedProd
			FROM	dbo.Quantity
			WHERE	Refnum = @Refnum
			) AS SourceTable
			PIVOT (
				MAX(AnnFeedProd)
				FOR FeedProdID IN
					(
					[Ethane],[EPMix],[Propane],[Butane],[LPG],[OthLtFeed],[HeavyNGL],[Condensate],[FRNaphtha],[LtNaphtha],[Raffinate],[HeavyNaphtha],[Diesel],[HeavyGasoil],[HTGasoil],[OthLiqFeed1],[OthLiqFeed2],[OthLiqFeed3],
					[DiEthylene],[RefEthylene],[ConcEthylene],[DiPropylene],[RefPropylene],[ConcPropylene],
					[Hydrogen]
					)
				) AS feedQuant
			
		LEFT OUTER JOIN
		
			(SELECT PT.Refnum
				, PT.OthLiqFeed1 'SG1'
				, PT.OthLiqFeed2 'SG2'
				, PT.OthLiqFeed3 'SG3'
			FROM
				(SELECT	Refnum, FeedProdID, SG
				FROM	dbo.FeedQuality
				WHERE	Refnum =  @Refnum
						AND SG IS NOT NULL AND FeedProdID LIKE 'OthLiqFeed%'
				) AS ST
				PIVOT
					(
					MAX(SG)
					FOR FeedProdID IN
						(
						[OthLiqFeed1],[OthLiqFeed2],[OthLiqFeed3]
						)
					) AS PT
				) AS feedQual ON feedQual.Refnum = feedQuant.Refnum
		
		INNER JOIN
			
			dbo.ProdQuality	PQ	ON	PQ.Refnum	 = feedQuant.Refnum

	WHERE	PQ.Refnum		= @Refnum
		AND	PQ.FeedProdID	= @pqFeedProdID
		
	-------------------------------------------------------------------------------------------------------------------
	-- Add fields that depend on aggregate from previous query
	
	SELECT	RD.*
			, IsNull(P.FreshFeedDiv, RD.FreshFeed) / (CAP.OlefinsCapMTD - RD.SupplEP) * 1000	AS 'FeedDivisor'
	INTO	#RawData
	FROM
		#RawDataA					RD
		INNER JOIN dbo.Production	P	ON	P.Refnum	= RD.Refnum
		INNER JOIN dbo.Capacity		CAP	ON	CAP.Refnum	= RD.Refnum
										AND	CAP.Refnum	= P.Refnum
	WHERE	P.Refnum	= @Refnum
		AND CAP.Refnum	= @Refnum;

	-------------------------------------------------------------------------------------------------------------------
	-- Calculate individual kEDC values
	
	SELECT RD.Refnum 
	
	--	[]

		, (IsNull(FC.FracFeedKBSD, 0)	* edcF.FracFeedKBSD)					/ 1000	AS 'FracFeedKBSD'
		, (IsNull(FC.HPBoilRate, 0)		* edcF.HPBoilRate)						/ 1000	AS 'HPBoilRate'
		, (IsNull(FC.LPBoilRate, 0)		* edcF.LPBoilRate)						/ 1000	AS 'LPBoilRate'
		, (IsNull(FC.ElecGenMW, 0)		* edcF.ElecGenMW)						/ 1000	AS 'ElecGenMW'
		, (IsNull(FC.HydroCryoCnt, 0)	* edcF.HydroCryoCnt	* RD.Stoichiometry)	/ 1000	AS 'HydroCryoCnt'
		, (IsNull(FC.HydroPSACnt, 0)	* edcF.HydroPSACnt	* RD.Stoichiometry)	/ 1000	AS 'HydroPSACnt'
		, (IsNull(FC.HydroMembCnt, 0)	* edcF.HydroMembCnt	* RD.Stoichiometry)	/ 1000	AS 'HydroMembCnt'
		, (IsNull(FC.PGasHydroKBSD, 0)	* edcF.PGasHydroKBSD)					/ 1000	AS 'PGasHydroKBSD'
	
	--	([EDC Coefficient] * (Feed ^ [EDC Exponent])) / 1000
	
		, (edcC.GasCompBHP			* Power(IsNull(FC.GasCompBHP, 0)		, edcE.GasCompBHP))
																/ 1000					AS 'GasCompBHP'
		, (edcC.MethCompBHP			* Power(IsNull(FC.MethCompBHP, 0)		, edcE.MethCompBHP))
																/ 1000					AS 'MethCompBHP'
		, (edcC.EthyCompBHP			* Power(IsNull(FC.EthyCompBHP, 0)		, edcE.EthyCompBHP))
																/ 1000					AS 'EthyCompBHP'
		, (edcC.PropCompBHP			* Power(IsNull(FC.PropCompBHP, 0)		, edcE.PropCompBHP))
																/ 1000					AS 'PropCompBHP'
																					
		, (edcC.FurnaceCapKMTA		* Power(IsNull(CAP.FurnaceCapKMTA, 0)	, edcE.FurnaceCapKMTA))
																/ 1000					AS 'FurnaceCapKMTA'
		, (edcC.SpareFurnaceCapKMTA	* Power(IsNull(CAP.FurnaceCapKMTA * FC.PyrFurnSparePcnt, 0)	, edcE.SpareFurnaceCapKMTA))
																/ 1000					AS 'SpareFurnaceCapKMTA'
		
		, edcC.Ethane			* IsNull(RD.Ethane + RD.EPMix * @ratioEP, 0)
																/ RD.FeedDivisor		AS 'Ethane'
		, edcC.Propane			* IsNull(RD.Propane + RD.EPMix * (1 - @ratioEP), 0)
																/ RD.FeedDivisor		AS 'Propane'
		, edcC.Butane			* IsNull(RD.Butane, 0)			/ RD.FeedDivisor		AS 'Butane'
		, edcC.LPG				* IsNull(RD.LPG, 0)				/ RD.FeedDivisor		AS 'LPG'
		, edcC.OthLtFeed		* IsNull(RD.OthLtFeed, 0)		/ RD.FeedDivisor		AS 'OthLtFeed'
		, edcC.HeavyGasoil		* IsNull(RD.HeavyGasoil, 0)		/ RD.FeedDivisor		AS 'HeavyGasoil'
		, edcC.HTGasoil			* IsNull(RD.HTGasoil, 0)		/ RD.FeedDivisor		AS 'HTGasoil'
		, edcC.OthLiqFeed1		* IsNull(RD.OthLiqFeed1, 0)		/ RD.FeedDivisor		AS 'OthLiqFeed1'
		, edcC.OthLiqFeed2		* IsNull(RD.OthLiqFeed2, 0)		/ RD.FeedDivisor		AS 'OthLiqFeed2'
		, edcC.OthLiqFeed3		* IsNull(RD.OthLiqFeed3, 0)		/ RD.FeedDivisor		AS 'OthLiqFeed3'
		, edcC.HeavyNaphtha		* IsNull(RD.HeavyNaphtha, 0)	/ RD.FeedDivisor		AS 'HeavyNaphtha'
		, edcC.LtNaphtha		* IsNull(RD.LtNaphtha, 0)		/ RD.FeedDivisor		AS 'LtNaphtha'
		, edcC.Raffinate		* IsNull(RD.Raffinate, 0)		/ RD.FeedDivisor		AS 'Raffinate'
		, edcC.FRNaphtha		* IsNull(RD.FRNaphtha, 0)		/ RD.FeedDivisor		AS 'FRNaphtha'
		, edcC.HeavyNGL			* IsNull(RD.HeavyNGL, 0)		/ RD.FeedDivisor		AS 'HeavyNGL'
		, edcC.Condensate		* IsNull(RD.Condensate, 0)		/ RD.FeedDivisor		AS 'Condensate'
		, edcC.Diesel			* IsNull(RD.Diesel, 0)			/ RD.FeedDivisor		AS 'Diesel'

		, edcC.ConcEthylene		* IsNull(RD.ConcEthylene, 0)	/ 1000					AS 'ConcEthylene'
		, edcC.DiEthylene		* IsNull(RD.DiEthylene, 0)		/ 1000					AS 'DiEthylene'
		, edcC.RefEthylene		* IsNull(RD.RefEthylene, 0)		/ 1000					AS 'RefEthylene'
		, edcC.ConcPropylene	* IsNull(RD.ConcPropylene, 0)	/ 1000					AS 'ConcPropylene'
		, edcC.DiPropylene		* IsNull(RD.DiPropylene, 0)		/ 1000					AS 'DiPropylene'
		, edcC.RefPropylene		* IsNull(RD.RefPropylene, 0)	/ 1000					AS 'RefPropylene'
		
		, RD.SG1 AS 'SG1'
		, RD.SG2 AS 'SG2'
		, RD.SG3 AS 'SG3'
		
	INTO #kEDCCalculations

	FROM
		
		#RawData						RD
		INNER JOIN dbo.Capacity			CAP		ON CAP.Refnum	  = RD.Refnum
		INNER JOIN dbo.Facilities		FC		ON FC.Refnum	  = RD.Refnum
			
			, dbo.EDCFactors			edcF
		INNER JOIN dbo.EDCCoefficients	edcC	ON	edcC.FactorSet = edcF.FactorSet
		INNER JOIN dbo.EDCExponents		edcE	ON	edcE.FactorSet = edcF.FactorSet
												AND	edcE.FactorSet = edcC.FactorSet
		
	WHERE	CAP.Refnum	= @Refnum
		AND FC.Refnum	= @Refnum
		AND edcF.FactorSet = @FactorSet
		AND edcC.FactorSet = @FactorSet
		AND edcE.FactorSet = @FactorSet;

	-------------------------------------------------------------------------------------------------------------------
	-- Aggregate kEDC and insert values into table

	DELETE FROM dbo.EDCVersionCalcs WHERE Refnum = @Refnum AND FactorSet = @FactorSet AND ModelVersion = @ModelVersion
	
	INSERT INTO dbo.EDCVersionCalcs
	SELECT e.Refnum
		, @FactorSet AS 'FactorSet'
		, @ModelVersion	AS 'ModelVersion'

		, FracFeedKBSD
			+ Ethane + Propane + Butane + LPG + OthLtFeed
				+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
				+ Diesel + HeavyGasoil + HTGasoil
				+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3
			+ FurnaceCapKMTA + SpareFurnaceCapKMTA
			+ GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP
			+ ConcEthylene + DiEthylene + RefEthylene + ConcPropylene + DiPropylene + RefPropylene
			+ HPBoilRate + LPBoilRate + ElecGenMW
			+ HydroCryoCnt + HydroPSACnt + HydroMembCnt
			+ PGasHydroKBSD														AS 'kEDC'
		
		, (FracFeedKBSD
			+ Ethane + Propane + Butane + LPG + OthLtFeed
				+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
				+ Diesel + HeavyGasoil + HTGasoil
				+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3
			+ FurnaceCapKMTA + SpareFurnaceCapKMTA
			+ GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP
			+ ConcEthylene + DiEthylene + RefEthylene + ConcPropylene + DiPropylene + RefPropylene
			+ HPBoilRate + LPBoilRate + ElecGenMW
			+ HydroCryoCnt + HydroPSACnt + HydroMembCnt
			+ PGasHydroKBSD) * 1000												AS 'EDC'

		, (FracFeedKBSD
			+ Ethane + Propane + Butane + LPG + OthLtFeed
				+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
				+ Diesel + HeavyGasoil + HTGasoil
				+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3
			+ FurnaceCapKMTA + SpareFurnaceCapKMTA
			+ GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP
			+ ConcEthylene + DiEthylene + RefEthylene + ConcPropylene + DiPropylene + RefPropylene
			+ HPBoilRate + LPBoilRate + ElecGenMW
			+ HydroCryoCnt + HydroPSACnt + HydroMembCnt
			+ PGasHydroKBSD) * TAAdjC2C3Util									AS 'TAAdjkUEDC'
		
		, (FracFeedKBSD
			+ Ethane + Propane + Butane + LPG + OthLtFeed
				+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
				+ Diesel + HeavyGasoil + HTGasoil
				+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3
			+ FurnaceCapKMTA + SpareFurnaceCapKMTA
			+ GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP
			+ ConcEthylene + DiEthylene + RefEthylene + ConcPropylene + DiPropylene + RefPropylene
			+ HPBoilRate + LPBoilRate + ElecGenMW
			+ HydroCryoCnt + HydroPSACnt + HydroMembCnt
			+ PGasHydroKBSD) * UnAdjC2C3Util									AS 'kuEDC'
		
		, FracFeedKBSD															AS 'FracFeed'
		, Ethane + Propane + Butane + LPG + OthLtFeed
			+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
			+ Diesel + HeavyGasoil + HTGasoil
			+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3							AS 'TotalFeedStock'
		, FurnaceCapKMTA + SpareFurnaceCapKMTA									AS 'Furnace'
		, GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP					AS 'Compression'
		, ConcEthylene + DiEthylene + RefEthylene + ConcPropylene + DiPropylene + RefPropylene
																				AS 'Supplemental'
		, HPBoilRate + LPBoilRate + ElecGenMW									AS 'Utilities'
		, HydroCryoCnt + HydroPSACnt + HydroMembCnt								AS 'HydrogenPurification'
		, PGasHydroKBSD															AS 'PyrolysisGasolineHydrotreater'

		--/*
		, Ethane + Propane + Butane + LPG + OthLtFeed							AS 'LightFeed'
	
		, LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
			+ CASE WHEN SG1 <= @sgBreakPoint THEN IsNull(OthLiqFeed1, 0) ELSE 0 END
			+ CASE WHEN SG2 <= @sgBreakPoint THEN IsNull(OthLiqFeed2, 0) ELSE 0 END
			+ CASE WHEN SG3 <= @sgBreakPoint THEN IsNull(OthLiqFeed3, 0) ELSE 0 END
																				AS 'NaphthatFeed'
		, Diesel + HeavyGasoil + HTGasoil
			+ CASE WHEN SG1 > @sgBreakPoint THEN IsNull(OthLiqFeed1, 0) ELSE 0 END
			+ CASE WHEN SG2 > @sgBreakPoint THEN IsNull(OthLiqFeed2, 0) ELSE 0 END
			+ CASE WHEN SG3 > @sgBreakPoint THEN IsNull(OthLiqFeed3, 0) ELSE 0 END
																				AS 'HeavyFeed'
																				
		, DiEthylene + DiPropylene												AS 'DiSuppl'
		, RefEthylene + RefPropylene											AS 'RefSuppl'
		, ConcEthylene + ConcPropylene											AS 'ConcSuppl'
		
		-- Raw Elements
		--*/
		, FracFeedKBSD
		, HPBoilRate
		, LPBoilRate
		, ElecGenMW
		, HydroCryoCnt
		, HydroPSACnt
		, HydroMembCnt
		, PGasHydroKBSD
		
		, GasCompBHP
		, MethCompBHP
		, EthyCompBHP
		, PropCompBHP
		
		, FurnaceCapKMTA
		, SpareFurnaceCapKMTA
		
		, Ethane
		, Propane
		
		, Butane
		, LPG
		, OthLtFeed
		
		, HeavyGasoil
		, HTGasoil
		
		, OthLiqFeed1
		, OthLiqFeed2
		, OthLiqFeed3
		
		, HeavyNaphtha
		, LtNaphtha
		, FRNaphtha
		, Condensate
		, HeavyNGL
		, Diesel
		
		, ConcEthylene
		, DiEthylene
		, RefEthylene
		, ConcPropylene
		, DiPropylene
		, RefPropylene
		
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL

		, GETDATE() AS 'tsCalculated'
			
	FROM #kEDCCalculations e
		INNER JOIN dbo.CapUtil u ON u.Refnum = e.Refnum
	WHERE u.Refnum = @Refnum

	UPDATE dbo.TSort
	SET EDCPeerGrp = g.GroupValue
	FROM
		dbo.TSort t
		INNER JOIN dbo.EDCVersionCalcs c ON c.Refnum = t.Refnum
		INNER JOIN dbo.EDCGroups g ON g.RangeMin <= c.kEDC AND g.RangeMax > c.kEDC
	WHERE	t.Refnum = @Refnum AND c.Refnum = @Refnum
	
END





