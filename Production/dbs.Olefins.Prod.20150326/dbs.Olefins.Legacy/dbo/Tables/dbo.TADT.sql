﻿CREATE TABLE [dbo].[TADT] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [TrainID]        TINYINT        NOT NULL,
    [PlanInterval]   REAL           NULL,
    [PlanDT]         REAL           NULL,
    [ActInterval]    REAL           NULL,
    [ActDT]          REAL           NULL,
    [TACost]         REAL           NULL,
    [TAManHrs]       REAL           NULL,
    [TADate]         DATETIME       NULL,
    [PctTotEthylCap] REAL           NULL,
    [TAMini]         CHAR (3)       NULL,
    CONSTRAINT [PK___5__19] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TrainID] ASC)
);

