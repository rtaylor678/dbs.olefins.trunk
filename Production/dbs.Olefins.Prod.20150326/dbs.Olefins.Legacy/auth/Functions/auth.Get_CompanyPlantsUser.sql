﻿CREATE FUNCTION [auth].[Get_CompanyPlantsUser]
(
	@CompanyId		INT,
	@LoginId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		jcp.[PlantId],
		p.[PlantName],
		COALESCE(jpl.[Active], '')			[Active]
	FROM [auth].[JoinCompanyPlant]			jcp
	INNER JOIN [auth].[Plants]				p
		ON	p.[PlantId]		= jcp.[PlantId]
		AND	p.[Active]		= 1
	LEFT OUTER JOIN [auth].[JoinPlantLogin]	jpl
		ON	jpl.[PlantId]	= jcp.[PlantId]
		AND	jpl.[LoginId]	= @LoginId
	WHERE	jcp.[CompanyId]	= @CompanyId
		AND	jcp.[Active]	= 1
);