﻿CREATE TABLE [dim].[FeedStockDistillation_LookUp] (
    [DistillationId]     VARCHAR (4)        NOT NULL,
    [DistillationName]   NVARCHAR (84)      NOT NULL,
    [DistillationDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_FeedStockDistillation_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistillation_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistillation_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistillation_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedStockDistillation_LookUp] PRIMARY KEY CLUSTERED ([DistillationId] ASC),
    CONSTRAINT [CL_FeedStockDistillation_LookUp_DistillationDetail] CHECK ([DistillationDetail]<>''),
    CONSTRAINT [CL_FeedStockDistillation_LookUp_DistillationID] CHECK ([DistillationId]<>''),
    CONSTRAINT [CL_FeedStockDistillation_LookUp_DistillationName] CHECK ([DistillationName]<>''),
    CONSTRAINT [UK_FeedStockDistillation_LookUp_DistillationDetail] UNIQUE NONCLUSTERED ([DistillationDetail] ASC),
    CONSTRAINT [UK_FeedStockDistillation_LookUp_DistillationName] UNIQUE NONCLUSTERED ([DistillationName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FeedStockDistillation_LookUp_u]
	ON [dim].[FeedStockDistillation_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FeedStockDistillation_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedStockDistillation_LookUp].[DistillationId]		= INSERTED.[DistillationId];
		
END;
