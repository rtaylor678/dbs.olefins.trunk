﻿CREATE TABLE [dim].[PolymerFamily_LookUp] (
    [PolymerFamilyId]     VARCHAR (6)        NOT NULL,
    [PolymerFamilyName]   NVARCHAR (84)      NOT NULL,
    [PolymerFamilyDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_PolymerFamily_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_PolymerFamily_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_PolymerFamily_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_PolymerFamily_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]        ROWVERSION         NOT NULL,
    CONSTRAINT [PK_PolymerFamily_LookUp] PRIMARY KEY CLUSTERED ([PolymerFamilyId] ASC),
    CONSTRAINT [CL_PolymerFamily_LookUp_PolymerFamilyDetail] CHECK ([PolymerFamilyDetail]<>''),
    CONSTRAINT [CL_PolymerFamily_LookUp_PolymerFamilyId] CHECK ([PolymerFamilyId]<>''),
    CONSTRAINT [CL_PolymerFamily_LookUp_PolymerFamilyName] CHECK ([PolymerFamilyName]<>''),
    CONSTRAINT [UK_PolymerFamily_LookUp_PolymerFamilyDetail] UNIQUE NONCLUSTERED ([PolymerFamilyDetail] ASC),
    CONSTRAINT [UK_PolymerFamily_LookUp_PolymerFamilyName] UNIQUE NONCLUSTERED ([PolymerFamilyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_PolymerFamily_LookUp_u]
	ON [dim].[PolymerFamily_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[PolymerFamily_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[PolymerFamily_LookUp].[PolymerFamilyId]		= INSERTED.[PolymerFamilyId];
		
END;
