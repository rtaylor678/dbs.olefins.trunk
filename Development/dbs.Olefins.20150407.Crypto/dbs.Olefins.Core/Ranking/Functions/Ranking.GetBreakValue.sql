﻿
CREATE FUNCTION [Ranking].[GetBreakValue](@Refnum varchar(18), @RankBreak sysname, @ListId varchar(42))
RETURNS varchar(12)
AS
BEGIN
	DECLARE @BreakValue varchar(12)
	IF @RankBreak = 'TotalList'
		SET @BreakValue = 'ALL'
	ELSE IF @RankBreak = 'UserGroup'
		SELECT @BreakValue = UserGroup FROM cons.RefList WHERE ListId = @ListId AND Refnum = @Refnum
	ELSE
		SELECT @BreakValue = BreakValue
		FROM Ranking.RefineryBreakValues
		WHERE Refnum = @Refnum AND RankBreak = @RankBreak
		
	RETURN @BreakValue
END
