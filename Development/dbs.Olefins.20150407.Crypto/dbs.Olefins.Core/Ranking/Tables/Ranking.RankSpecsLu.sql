﻿CREATE TABLE [Ranking].[RankSpecsLu] (
    [RankSpecsID]  INT           IDENTITY (1, 1) NOT NULL,
    [ListId]       VARCHAR (42)  NOT NULL,
    [RankBreak]    VARCHAR (128) NOT NULL,
    [BreakValue]   CHAR (12)     NOT NULL,
    [RankVariable] VARCHAR (128) NOT NULL,
    [ListName]     NVARCHAR (84) NULL,
    CONSTRAINT [PK_RankSpecsLU_1] PRIMARY KEY CLUSTERED ([RankSpecsID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [UniqRankSpec_1] UNIQUE NONCLUSTERED ([ListId] ASC, [RankBreak] ASC, [BreakValue] ASC, [RankVariable] ASC) WITH (FILLFACTOR = 70)
);

