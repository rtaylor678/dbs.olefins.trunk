﻿CREATE VIEW [fact].[ReliabilityOppLossAggregate]
WITH SCHEMABINDING
AS
SELECT
	b.[FactorSetId],
	o.[Refnum],
	o.[CalDateKey],
	b.[OppLossId],
	o.[StreamId],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(o.DownTimeLoss_MT, 0.0 )
		WHEN '-' THEN - ISNULL(o.DownTimeLoss_MT, 0.0 )
		ELSE 0.0
		END)			[DownTimeLoss_MT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(o.SlowDownLoss_MT, 0.0)
		WHEN '-' THEN - ISNULL(o.SlowDownLoss_MT, 0.0)
		ELSE 0.0
		END)			[SlowDownLoss_MT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(o.DownTimeLoss_MT, 0.0 ) + ISNULL(o.SlowDownLoss_MT, 0.0)
		WHEN '-' THEN - ISNULL(o.DownTimeLoss_MT, 0.0 ) + ISNULL(o.SlowDownLoss_MT, 0.0)
		ELSE 0.0
		END)			[TotLoss_MT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(o._DownTimeLoss_kMT, 0.0 )
		WHEN '-' THEN - ISNULL(o._DownTimeLoss_kMT, 0.0 )
		ELSE 0.0
		END)			[DownTimeLoss_kMT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(o._SlowDownLoss_kMT, 0.0)
		WHEN '-' THEN - ISNULL(o._SlowDownLoss_kMT, 0.0)
		ELSE 0.0
		END)			[SlowDownLoss_kMT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(o._DownTimeLoss_kMT, 0.0 ) + ISNULL(o._SlowDownLoss_kMT, 0.0)
		WHEN '-' THEN - ISNULL(o._DownTimeLoss_kMT, 0.0 ) + ISNULL(o._SlowDownLoss_kMT, 0.0)
		ELSE 0.0
		END)			[TotLoss_kMT],
	o.[CurrentYear],
	COUNT_BIG(*)		[IndexItems]
FROM [dim].[ReliabilityOppLoss_Bridge]	b
INNER JOIN [fact].[ReliabilityOppLoss]	o
	ON	o.[OppLossId] = b.[DescendantId]
GROUP BY
	b.[FactorSetId],
	o.[Refnum],
	o.[CalDateKey],
	b.[OppLossId],
	o.[StreamId],
	o.[CurrentYear];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_ReliabilityOppLossAggregate]
    ON [fact].[ReliabilityOppLossAggregate]([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [OppLossId] ASC, [CalDateKey] ASC);

