﻿CREATE VIEW [fact].[StreamQuantityMetaAggregate]
WITH SCHEMABINDING
AS
SELECT
	b.[FactorSetId],
	q.[Refnum],
	b.[StreamId],
	CASE WHEN b.[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'ProdOther') THEN q.[StreamDescription] END	[StreamDescription],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + q.[Quantity_kMT]
		WHEN '-' THEN - q.[Quantity_kMT]
		ELSE 0.0
		END)							[Quantity_kMT],
	COUNT_BIG(*)						[IndexItems]
FROM [dim].[Stream_Meta_Bridge]				b
INNER JOIN [fact].[Quantity]				q
	ON	q.[StreamId] = b.[DescendantId]
GROUP BY
	b.[FactorSetId],
	q.[Refnum],
	b.[StreamId],
	CASE WHEN b.[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'ProdOther') THEN q.[StreamDescription] END;

GO
CREATE UNIQUE CLUSTERED INDEX [UX_StreamQuantityMetaAggregate]
    ON [fact].[StreamQuantityMetaAggregate]([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_StreamQuantityMetaAggregate]
    ON [fact].[StreamQuantityMetaAggregate]([StreamId] ASC)
    INCLUDE([FactorSetId], [Refnum], [Quantity_kMT]);

