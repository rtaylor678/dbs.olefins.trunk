﻿
CREATE VIEW fact.FeedStockDistillationPivot
WITH SCHEMABINDING
AS
SELECT
	  p.Refnum
	, p.CalDateKey
	, p.StreamId
	, p.StreamDescription
	, p.D000
	, p.D005
	, p.D010
	, p.D030
	, p.D050
	, p.D070
	, p.D090
	, p.D095
	, p.D100
	
	, CASE WHEN ISNULL(p.D000, 0.0) > 71.0
	THEN 0.0
	ELSE
		CASE WHEN ISNULL(p.D010, 0.0) > 71.0
		THEN (71.0 - ISNULL(p.D000, 0.0)) / (ISNULL(p.D010, 0.0) - ISNULL(p.D000, 0.0)) * 10.0
		ELSE
			CASE WHEN ISNULL(p.D030, 0.0) > 71.0
			THEN (71.0 - ISNULL(p.D010, 0.0)) / (ISNULL(p.D030, 0.0) - ISNULL(p.D010, 0.0)) * 20.0 + 10.0
			ELSE
				CASE WHEN ISNULL(p.D050, 0.0) > 71.0
				THEN (71.0 -ISNULL( p.D030, 0.0)) / (ISNULL(p.D050, 0.0) - ISNULL(p.D030, 0.0)) * 20.0 + 30.0
				ELSE
					CASE WHEN ISNULL(p.D070, 0.0) > 71.0
					THEN (71.0 - ISNULL(p.D050, 0.0)) / (ISNULL(p.D070, 0.0) - ISNULL(p.D050, 0.0)) * 20.0 + 50.0
					ELSE
						CASE WHEN ISNULL(p.D090, 0.0) > 71.0
						THEN (71.0 - ISNULL(p.D070, 0.0)) / (ISNULL(p.D090, 0.0) - ISNULL(p.D070, 0.0)) * 20.0 + 70.0
						ELSE
							CASE WHEN ISNULL(p.D100, 0.0) > 71.0
							THEN (71.0 - ISNULL(p.D090, 0.0)) / (ISNULL(p.D100, 0.0) - ISNULL(p.D090, 0.0)) * 10.0 + 90.0
							ELSE
								100
							END
						END
					END
				END
			END
		END
	END / 100.0	[C5C6_Fraction]
	
FROM (
	SELECT
		  d.Refnum
		, d.CalDateKey
		, d.StreamId
		, d.StreamDescription
		, d.DistillationID
		, d.Temp_C
	FROM fact.FeedStockDistillation d
	) u
	PIVOT (
	MAX(u.Temp_C) FOR u.DistillationID IN (
		  D000
		, D005
		, D010
		, D030
		, D050
		, D070
		, D090
		, D095
		, D100
		)
	) p;