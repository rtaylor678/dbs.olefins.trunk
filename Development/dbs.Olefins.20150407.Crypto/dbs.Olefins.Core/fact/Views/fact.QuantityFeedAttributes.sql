﻿CREATE VIEW fact.QuantityFeedAttributes
WITH SCHEMABINDING
AS
SELECT
	t.FactorSetId,
	t.Refnum,
	SUM(t.Quantity_kMT)														[Tot_kMT],
	SUM(t.Quantity_kMT * t.Density_SG)				/ SUM(t.Quantity_kMT)	[Density_SG],
	SUM(t.Quantity_kMT * t.FurnConstruction_Year)	/ SUM(t.Quantity_kMT)	[FurnConstruction_Year],
	SUM(t.Quantity_kMT * t.ResidenceTime_s)			/ SUM(t.Quantity_kMT)	[ResidenceTime_s]
FROM (
	SELECT
		q.FactorSetId,
		q.Refnum,
		q.Quantity_kMT,
		ISNULL(cp.Density_SG, SUM(cq.Component_WtPcnt * l.Density_SG) / SUM(cq.Component_WtPcnt))	[Density_SG],
		cp.FurnConstruction_Year,
		cp.ResidenceTime_s
	FROM fact.StreamQuantityAggregate			q	WITH (NOEXPAND)
	INNER JOIN fact.FeedStockCrackingParameters	cp
		ON	cp.Refnum = q.Refnum
		AND	cp.StreamId = q.StreamId
		AND	cp.StreamDescription = COALESCE(q.StreamDescription, cp.StreamDescription)
	LEFT OUTER JOIN fact.CompositionQuantity	cq
		ON	cq.Refnum = q.Refnum
		AND	cq.StreamId = q.StreamId
		AND	cq.StreamDescription = COALESCE(q.StreamDescription, cq.StreamDescription)
	LEFT OUTER JOIN ante.ComponentDensity		l
		ON	l.FactorSetId = q.FactorSetId
		AND	l.ComponentId = cq.ComponentId
	GROUP BY
		q.FactorSetId,
		q.Refnum,
		q.Quantity_kMT,
		cp.Density_SG,
		cp.FurnConstruction_Year,
		cp.ResidenceTime_s
	) t
GROUP BY
	t.FactorSetId,
	t.Refnum;