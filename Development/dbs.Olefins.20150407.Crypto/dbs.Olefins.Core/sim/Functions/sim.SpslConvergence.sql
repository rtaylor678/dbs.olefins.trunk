﻿
CREATE FUNCTION sim.SpslConvergence
(
	@Item				TINYINT,
	@FeedConv			REAL	= NULL,
	@FeedID				REAL	= NULL,
	@PE_Ratio			REAL	= NULL,
	@PM_Ratio			REAL	= NULL,
	@CoilOutletTemp		REAL	= NULL
)
RETURNS REAL
WITH SCHEMABINDING
AS
BEGIN

	RETURN
		CASE @Item
		WHEN 10	THEN
			CASE  
				WHEN @FeedConv > 0.0 AND @FeedID > 0	THEN @FeedID
				WHEN @PE_Ratio			> 0.0			THEN -2
				WHEN @PM_Ratio			> 0.0			THEN -3
				WHEN @CoilOutletTemp	> 0.0			THEN -1
				ELSE 0
			END
		WHEN  8	THEN
			CASE
				WHEN @FeedConv > 0.0 AND @FeedID > 0	THEN @FeedConv
				WHEN @PE_Ratio			> 0.0			THEN @PE_Ratio
				WHEN @PM_Ratio			> 0.0			THEN 1.0 / @PM_Ratio
				WHEN @CoilOutletTemp	> 0.0			THEN @CoilOutletTemp
				ELSE 0
			END
		ELSE 0
		END;
END;