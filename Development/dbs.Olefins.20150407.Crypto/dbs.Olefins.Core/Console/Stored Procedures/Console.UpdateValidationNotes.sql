﻿CREATE PROCEDURE [Console].[UpdateValidationNotes]

	@RefNum varchar(18),
	@NoteType varchar(20),
	@Note text,
	@UpdatedBy varchar(3)

AS
BEGIN

	declare @OldNote varchar(MAX)
	select @OldNote = Note COLLATE SQL_Latin1_General_CP1_CS_AS 
	from Val.Notes where Refnum = @RefNum and NoteType = @NoteType order by Updated desc
	
	if not exists(select * from Val.Notes where Refnum = @RefNum and NoteType = @NoteType and 
	Note COLLATE SQL_Latin1_General_CP1_CS_AS like @Note)
		INSERT INTO val.Notes (RefNum, NoteType, Note, UpdatedBy, Updated) Values(@RefNum,'Validation',@Note, @UpdatedBy, GETDATE())

END
