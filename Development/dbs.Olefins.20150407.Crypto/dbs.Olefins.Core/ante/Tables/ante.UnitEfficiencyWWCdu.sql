﻿CREATE TABLE [ante].[UnitEfficiencyWWCdu] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [EfficiencyId]   VARCHAR (42)       NOT NULL,
    [Coefficient]    REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_UnitEfficiencyWWCdu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_UnitEfficiencyWWCdu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_UnitEfficiencyWWCdu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_UnitEfficiencyWWCdus_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_UnitEfficiencyWWCdu] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EfficiencyId] ASC),
    CONSTRAINT [FK_UnitEfficiencyWWCdu_Efficiency_LookUp] FOREIGN KEY ([EfficiencyId]) REFERENCES [dim].[Efficiency_LookUp] ([EfficiencyId]),
    CONSTRAINT [FK_UnitEfficiencyWWCdu_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_UnitEfficiencyWWCdu_u]
	ON [ante].[UnitEfficiencyWWCdu]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[UnitEfficiencyWWCdu]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[UnitEfficiencyWWCdu].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[UnitEfficiencyWWCdu].[EfficiencyId]		= INSERTED.[EfficiencyId];

END;
