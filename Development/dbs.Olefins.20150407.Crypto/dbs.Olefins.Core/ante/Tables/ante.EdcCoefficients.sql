﻿CREATE TABLE [ante].[EdcCoefficients] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [EdcId]          VARCHAR (42)       NOT NULL,
    [Value]          REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EdcCoefficients_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EdcCoefficients_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EdcCoefficients_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EdcCoefficients_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EdcCoefficients] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EdcId] ASC),
    CONSTRAINT [FK_EdcCoefficients_Edc_LookUp] FOREIGN KEY ([EdcId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_EdcCoefficients_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_AnteEdcCoefficients_u]
	ON [ante].[EdcCoefficients]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EdcCoefficients]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EdcCoefficients].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[EdcCoefficients].EdcId			= INSERTED.EdcId;

END;