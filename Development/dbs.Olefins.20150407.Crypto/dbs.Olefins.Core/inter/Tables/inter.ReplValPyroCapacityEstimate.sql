﻿CREATE TABLE [inter].[ReplValPyroCapacityEstimate] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [Refnum]                VARCHAR (25)       NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [Capability_Pcnt]       REAL               NOT NULL,
    [Demonstrate_Pcnt]      REAL               NOT NULL,
    [Escalated_ENC_Cur]     REAL               NOT NULL,
    [Act_C2Yield_EstWtPcnt] REAL               NOT NULL,
    [DesignCapability_Pcnt] REAL               NULL,
    [DemonCapability_Pcnt]  REAL               NULL,
    [_Actual_ReplValue]     AS                 (CONVERT([real],([Escalated_ENC_Cur]*[Act_C2Yield_EstWtPcnt])/(100.0),(0))) PERSISTED NOT NULL,
    [_Design_ReplValue]     AS                 (CONVERT([real],([Escalated_ENC_Cur]*[DesignCapability_Pcnt])/(100.0),(0))) PERSISTED,
    [_Demon_ReplValue]      AS                 (CONVERT([real],([Escalated_ENC_Cur]*[DemonCapability_Pcnt])/(100.0),(0))) PERSISTED,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_ReplValPyroCapacityEstimate_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroCapacityEstimate_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroCapacityEstimate_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroCapacityEstimate_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReplValPyroCapacityEstimate] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReplValPyroCapacityEstimate_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReplValPyroCapacityEstimate_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_ReplValPyroCapacityEstimate_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReplValPyroCapacityEstimate_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReplValPyroCapacityEstimate_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER inter.t_ReplValPyroCapacityEstimate_u
	ON inter.ReplValPyroCapacityEstimate
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.ReplValPyroCapacityEstimate
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.ReplValPyroCapacityEstimate.FactorSetId	= INSERTED.FactorSetId
		AND inter.ReplValPyroCapacityEstimate.Refnum			= INSERTED.Refnum
		AND inter.ReplValPyroCapacityEstimate.CurrencyRpt	= INSERTED.CurrencyRpt
		AND inter.ReplValPyroCapacityEstimate.StreamId		= INSERTED.StreamId
		AND inter.ReplValPyroCapacityEstimate.CalDateKey		= INSERTED.CalDateKey;

END;