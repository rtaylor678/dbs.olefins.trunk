﻿CREATE PROCEDURE [inter].[Insert_Pricing]
(
	@FactorSetId			VARCHAR(12) = NULL
)
AS
BEGIN

	PRINT N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + COALESCE(@FactorSetId, 'xx') + N');';

	EXECUTE inter.Insert_PricingStaging			@FactorSetId;
	EXECUTE inter.Insert_PricingStream			@FactorSetId;
	EXECUTE inter.Insert_PricingComposition		@FactorSetId;

END;