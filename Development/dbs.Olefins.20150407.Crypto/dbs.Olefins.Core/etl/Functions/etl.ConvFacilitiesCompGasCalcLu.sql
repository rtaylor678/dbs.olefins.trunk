﻿
CREATE FUNCTION etl.ConvFacilitiesCompGasCalcLu
(
	@TypeID	VARCHAR(42)
)
RETURNS VARCHAR(7)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(7)=
	CASE @TypeID
		WHEN 'API Standard equation'		THEN 'API'
		WHEN 'Company developed equation'	THEN 'Company'
	END;

	RETURN @ResultVar

END