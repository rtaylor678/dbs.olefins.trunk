﻿


CREATE              PROC [reports].[spAPC](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DECLARE	@APCImplIndex real, @APCOnLineIndex real, @APCOnLineFactor real
Select 
  @APCImplIndex = SUM(Apc_Index) / @RefCount
, @APCOnLineIndex = SUM(ApcOnLine_Index) / @RefCount
, @APCOnLineFactor = CASE WHEN SUM(Apc_Index) > 0 THEN SUM(ApcOnLine_Index) / SUM(Apc_Index) * 100.0 END
FROM @MyGroup r JOIN calc.ApcIndexAggregate a on a.Refnum=r.Refnum
JOIN dbo.TSort t on t.Refnum=r.Refnum and a.FactorSetId=t.FactorSetId
WHERE @RefCount > 0 and a.ApcId = 'TotalAPC'

DELETE FROM reports.APC WHERE GroupId = @GroupId
Insert into reports.APC (GroupId
, APCImplIndex
, APCOnLineIndex
, APCOnLineFactor
, FurnAPC_OnlinePcnt
, CompAPC_OnlinePcnt
, ProdAPC_OnlinePcnt
, Loop_Count
, ClosedLoopAPC_Pcnt
, CritLoopNoMonitor
, CritLoopException
, CritLoopOffline
, CritLoopOnlineSch
, CritLoopContinuous
, ApcAge_Years
, OffLineLinearPrograms_Pcnt
, OffLineNonLinearSimulators_Pcnt
, OffLineNonLinearInput_Pcnt
, OnLineClosedLoop_Pcnt
, OffLineComparison_Days
, OnLineComparison_Days
, ClosedLoopModelCokingPred_Pcnt
, OptimizationOffLine_Mnths
, OptimizationPlanFreq_Mnths
, OptimizationOnLineOper_Mnths
, OnLineInput_Count
, OnLineVariable_Count
, OnLineOperation_Pcnt
, OnLinePointCycles_Count
, PyroFurnModeledIndependent_Pcnt
, OnLineAutoModeSwitch_Pcnt
)

SELECT GroupId = @GroupId
, APCImplIndex				= @APCImplIndex
, APCOnLineIndex			= @APCOnLineIndex
, APCOnLineFactor			= @APCOnLineFactor
, FurnAPC_OnlinePcnt		= [$(DbGlobal)].dbo.WtAvg(olp.OnLine_Pcnt, CASE WHEN olp.ApcId = 'PyroFurn' THEN 1 ELSE 0 END)
, CompAPC_OnlinePcnt		= [$(DbGlobal)].dbo.WtAvg(olp.OnLine_Pcnt, CASE WHEN olp.ApcId = 'Comp' THEN 1 ELSE 0 END)
, ProdAPC_OnlinePcnt		= [$(DbGlobal)].dbo.WtAvg(olp.OnLine_Pcnt, CASE WHEN olp.ApcId = 'ProdRecovery' THEN 1 ELSE 0 END)
, Loop_Count				= [$(DbGlobal)].dbo.WtAvgNN(Loop_Count, 1)
, ClosedLoopAPC_Pcnt		= [$(DbGlobal)].dbo.WtAvgNN(ClosedLoopAPC_Pcnt, CASE WHEN Loop_Count > 0 THEN 1 ELSE 0 END)
, CritLoopNoMonitor			= CASE WHEN SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) > 0 THEN 
								100.0 * SUM(CASE WHEN ApcMonitorId = 'A' THEN 1 ELSE 0 END) /  SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) END
, CritException				= CASE WHEN SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) > 0 THEN 
								100.0 * SUM(CASE WHEN ApcMonitorId = 'B' THEN 1 ELSE 0 END) /  SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) END
, CritLoopOffline			= CASE WHEN SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) > 0 THEN 
								100.0 * SUM(CASE WHEN ApcMonitorId = 'C' THEN 1 ELSE 0 END) /  SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) END
, CritLoopOnlineSch			= CASE WHEN SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) > 0 THEN 
								100.0 * SUM(CASE WHEN ApcMonitorId = 'D' THEN 1 ELSE 0 END) /  SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) END
, CritLoopContinuous		= CASE WHEN SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) > 0 THEN 
								100.0 * SUM(CASE WHEN ApcMonitorId = 'E' THEN 1 ELSE 0 END) /  SUM(CASE WHEN ApcMonitorId in ('A','B','C','D','E') THEN 1 ELSE 0 END) END
, ApcAge_Years						= [$(DbGlobal)].dbo.WtAvg(ApcAge_Years, 1)
, OffLineLinearPrograms_Pcnt		= [$(DbGlobal)].dbo.WtAvg(OffLineLinearPrograms_Bit, 1) * 100.0
, OffLineNonLinearSimulators_Pcnt	= [$(DbGlobal)].dbo.WtAvg(OffLineNonLinearSimulators_Bit, 1) * 100.0
, OffLineNonLinearInput_Pcnt		= [$(DbGlobal)].dbo.WtAvg(OffLineNonLinearInput_Bit, 1) * 100.0
, OnLineClosedLoop_Pcnt				= [$(DbGlobal)].dbo.WtAvg(OnLineClosedLoop_Bit, 1) * 100.0
, OffLineComparison_Days			= [$(DbGlobal)].dbo.WtAvg(OffLineComparison_Days, 1)
, OnLineComparison_Days				= [$(DbGlobal)].dbo.WtAvg(OnLineComparison_Days, 1)
, ClosedLoopModelCokingPred_Pcnt	= [$(DbGlobal)].dbo.WtAvg(ClosedLoopModelCokingPred_Bit, 1) * 100.0
, OptimizationOffLine_Mnths			= [$(DbGlobal)].dbo.WtAvg(OptimizationOffLine_Mnths, 1)
, OptimizationPlanFreq_Mnths		= [$(DbGlobal)].dbo.WtAvg(OptimizationPlanFreq_Mnths, 1)
, OptimizationOnLineOper_Mnths		= [$(DbGlobal)].dbo.WtAvg(OptimizationOnLineOper_Mnths, 1)
, OnLineInput_Count					= [$(DbGlobal)].dbo.WtAvg(OnLineInput_Count, 1)
, OnLineVariable_Count				= [$(DbGlobal)].dbo.WtAvg(OnLineVariable_Count, 1)
, OnLineOperation_Pcnt				= [$(DbGlobal)].dbo.WtAvg(OnLineOperation_Pcnt, 1)
, OnLinePointCycles_Count			= [$(DbGlobal)].dbo.WtAvg(OnLinePointCycles_Count, 1)
, PyroFurnModeledIndependent_Pcnt	= [$(DbGlobal)].dbo.WtAvg(PyroFurnModeledIndependent_Bit, 1) * 100.0
, OnLineAutoModeSwitch_Pcnt			= [$(DbGlobal)].dbo.WtAvg(OnLineAutoModeSwitch_Bit, 1) * 100.0
FROM @MyGroup r JOIN fact.Apc a on a.Refnum=r.Refnum
CROSS APPLY (VALUES ('Comp'),('PyroFurn'), ('ProdRecovery' )) dt(APCID)  -- Just incase all records are not there!
LEFT JOIN fact.ApcOnLinePcnt olp on olp.Refnum=r.Refnum and dt.APCID=olp.ApcId

SET NOCOUNT OFF

















