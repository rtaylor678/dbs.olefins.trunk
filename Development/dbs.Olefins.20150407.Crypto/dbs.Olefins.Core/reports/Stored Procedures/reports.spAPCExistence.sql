﻿


CREATE              PROC [reports].[spAPCExistence](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup
--print @refcount


DELETE FROM reports.APCExistence WHERE GroupId = @GroupId

IF @RefCount = 0 BEGIN RETURN END

Insert into reports.APCExistence (GroupId
, DataType
, StartStop_Pcnt
, DecokeAutomatic_Pcnt
, DecokeOperator_Pcnt
, ThroughputIndivid_Pcnt
, FracEthyleneThroughput_Pcnt
, ClosedLoop_Pcnt
, CoilOutlet_Pcnt
, FiringConstraint_Pcnt
, ExcessAir_Pcnt
, FiringDuty_Pcnt
, Balancing_Pcnt
, SteamHydrocarbon_Pcnt
, RecycleFlowRate_Pcnt
, Suction_Pcnt
, SurgeMargin_Pcnt
, CompLoad_Pcnt
, Surge_Pcnt
, Cooling_Pcnt
, CompConstraint_Pcnt
, HotWater_Pcnt
, PyroGasBP_Pcnt
, StrippingSteamRatio_Pcnt
, DemethComposition_Pcnt
, DemethThroughput_Pcnt
, DeethComposition_Pcnt
, DeethThroughput_Pcnt
, FracEthyleneComposition_Pcnt
, ThroughputTotal_Pcnt
, Refrig_Pcnt
, RefrigCompSurge_Pcnt
, RefrigCompSurgeMargin_Pcnt
, C3Recovery_Pcnt
, C3RecoveryThroughput_Pcnt
, SteamNaphthaComposition_Pcnt
, SteamNaphthaThroughput_Pcnt
, PressureMin_Pcnt)

SELECT GroupId = @GroupId
, DataType = 'MPC'
, StartStop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStartStop' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, DecokeAutomatic_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeAutomatic' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, DecokeOperator_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeOperator' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, ThroughputIndivid_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputIndivid' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, FracEthyleneThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneThroughput' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, ClosedLoop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcClosedLoop' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, CoilOutlet_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCoilOutlet' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, FiringConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringConstraint' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, ExcessAir_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcExcessAir' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, FiringDuty_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringDuty' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, Balancing_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcBalancing' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, SteamHydrocarbon_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamHydrocarbon' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, RecycleFlowRate_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRecycleFlowRate' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, Suction_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSuction' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, SurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurgeMargin' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, CompLoad_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompLoad' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, Surge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurge' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, Cooling_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCooling' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, CompConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompConstraint' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, HotWater_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcHotWater' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, PyroGasBP_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPyroGasBP' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, StrippingSteamRatio_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStrippingSteamRatio' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, DemethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethComposition' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, DemethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethThroughput' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, DeethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethComposition' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, DeethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethThroughput' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, FracEthyleneComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneComposition' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, ThroughputTotal_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputTotal' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, Refrig_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrig' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, RefrigCompSurge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurge' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, RefrigCompSurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurgeMargin' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, C3Recovery_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3Recovery' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, C3RecoveryThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3RecoveryThroughput' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, SteamNaphthaComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaComposition' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, SteamNaphthaThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaThroughput' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
, PressureMin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPressureMin' THEN Mpc_Bit ELSE 0 END),0) / @RefCount
FROM @MyGroup r JOIN fact.ApcExistance a on a.Refnum=r.Refnum
WHERE @RefCount > 0
UNION
SELECT GroupId = @GroupId
, DataType = 'SEP'
, StartStop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStartStop' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, DecokeAutomatic_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeAutomatic' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, DecokeOperator_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeOperator' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, ThroughputIndivid_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputIndivid' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, FracEthyleneThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneThroughput' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, ClosedLoop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcClosedLoop' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, CoilOutlet_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCoilOutlet' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, FiringConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringConstraint' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, ExcessAir_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcExcessAir' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, FiringDuty_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringDuty' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, Balancing_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcBalancing' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, SteamHydrocarbon_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamHydrocarbon' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, RecycleFlowRate_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRecycleFlowRate' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, Suction_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSuction' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, SurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurgeMargin' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, CompLoad_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompLoad' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, Surge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurge' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, Cooling_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCooling' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, CompConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompConstraint' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, HotWater_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcHotWater' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, PyroGasBP_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPyroGasBP' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, StrippingSteamRatio_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStrippingSteamRatio' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, DemethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethComposition' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, DemethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethThroughput' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, DeethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethComposition' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, DeethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethThroughput' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, FracEthyleneComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneComposition' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, ThroughputTotal_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputTotal' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, Refrig_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrig' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, RefrigCompSurge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurge' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, RefrigCompSurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurgeMargin' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, C3Recovery_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3Recovery' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, C3RecoveryThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3RecoveryThroughput' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, SteamNaphthaComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaComposition' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, SteamNaphthaThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaThroughput' THEN Sep_Bit ELSE 0 END),0) / @RefCount
, PressureMin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPressureMin' THEN Sep_Bit ELSE 0 END),0) / @RefCount
FROM @MyGroup r JOIN fact.ApcExistance a on a.Refnum=r.Refnum
WHERE @RefCount > 0
UNION
SELECT GroupId = @GroupId
, DataType = 'M+S'
, StartStop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStartStop' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, DecokeAutomatic_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeAutomatic' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, DecokeOperator_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeOperator' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, ThroughputIndivid_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputIndivid' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, FracEthyleneThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneThroughput' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, ClosedLoop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcClosedLoop' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, CoilOutlet_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCoilOutlet' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, FiringConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringConstraint' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, ExcessAir_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcExcessAir' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, FiringDuty_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringDuty' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, Balancing_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcBalancing' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, SteamHydrocarbon_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamHydrocarbon' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, RecycleFlowRate_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRecycleFlowRate' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, Suction_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSuction' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, SurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurgeMargin' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, CompLoad_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompLoad' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, Surge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurge' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, Cooling_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCooling' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, CompConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompConstraint' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, HotWater_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcHotWater' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, PyroGasBP_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPyroGasBP' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, StrippingSteamRatio_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStrippingSteamRatio' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, DemethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethComposition' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, DemethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethThroughput' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, DeethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethComposition' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, DeethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethThroughput' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, FracEthyleneComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneComposition' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, ThroughputTotal_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputTotal' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, Refrig_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrig' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, RefrigCompSurge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurge' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, RefrigCompSurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurgeMargin' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, C3Recovery_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3Recovery' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, C3RecoveryThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3RecoveryThroughput' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, SteamNaphthaComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaComposition' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, SteamNaphthaThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaThroughput' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
, PressureMin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPressureMin' THEN Mpc_Bit*1.0 + Sep_Bit*1.0 ELSE 0 END),0) / @RefCount / 2.0
FROM @MyGroup r JOIN fact.ApcExistance a on a.Refnum=r.Refnum
WHERE @RefCount > 0
UNION
SELECT GroupId = @GroupId
, DataType = 'MorS'
, StartStop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStartStop' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, DecokeAutomatic_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeAutomatic' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, DecokeOperator_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDecokeOperator' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, ThroughputIndivid_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputIndivid' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, FracEthyleneThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneThroughput' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, ClosedLoop_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcClosedLoop' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, CoilOutlet_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCoilOutlet' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, FiringConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringConstraint' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, ExcessAir_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcExcessAir' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, FiringDuty_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFiringDuty' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, Balancing_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcBalancing' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, SteamHydrocarbon_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamHydrocarbon' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, RecycleFlowRate_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRecycleFlowRate' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, Suction_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSuction' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, SurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurgeMargin' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, CompLoad_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompLoad' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, Surge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSurge' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, Cooling_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCooling' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, CompConstraint_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcCompConstraint' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, HotWater_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcHotWater' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, PyroGasBP_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPyroGasBP' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, StrippingSteamRatio_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcStrippingSteamRatio' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, DemethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethComposition' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, DemethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDemethThroughput' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, DeethComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethComposition' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, DeethThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcDeethThroughput' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, FracEthyleneComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcFracEthyleneComposition' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, ThroughputTotal_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcThroughputTotal' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, Refrig_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrig' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, RefrigCompSurge_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurge' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, RefrigCompSurgeMargin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcRefrigCompSurgeMargin' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, C3Recovery_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3Recovery' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, C3RecoveryThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcC3RecoveryThroughput' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, SteamNaphthaComposition_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaComposition' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, SteamNaphthaThroughput_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcSteamNaphthaThroughput' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
, PressureMin_Pcnt = ISNULL(100.0 * SUM(CASE WHEN ApcId = 'ApcPressureMin' THEN (CASE WHEN Mpc_Bit = 1 or Sep_Bit = 1 THEN 1 ELSE 0 END) ELSE 0 END),0) / @RefCount
FROM @MyGroup r JOIN fact.ApcExistance a on a.Refnum=r.Refnum
WHERE @RefCount > 0

SET NOCOUNT OFF

--select * from fact.ApcExistance where Refnum = '2011pch194'















