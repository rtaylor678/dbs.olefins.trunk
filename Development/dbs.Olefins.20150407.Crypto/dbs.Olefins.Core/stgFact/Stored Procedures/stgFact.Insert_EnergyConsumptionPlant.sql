﻿CREATE PROCEDURE [stgFact].[Insert_EnergyConsumptionPlant]
(
	@Refnum					VARCHAR (25),
	@SimModelId				VARCHAR (12),
	@OpCondId				VARCHAR (12),

	@SimEnergy_kCalkg		REAL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO stgFact.EnergyConsumptionPlant([Refnum], [SimModelId], [OpCondId], [SimEnergy_kCalkg])
	VALUES(@Refnum, @SimModelId, @OpCondId, @SimEnergy_kCalkg);

END;