﻿CREATE PROCEDURE [stgFact].[Insert_SimulationSeverity_Pyps]
(
	@Refnum					VARCHAR (25),
	@StreamId				VARCHAR (42),

	@SeverityBasis			VARCHAR (12)
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[SimulationSeverity_Pyps]([Refnum], [StreamId], [SeverityBasis])
	VALUES(@Refnum, @StreamId, @SeverityBasis);

END;