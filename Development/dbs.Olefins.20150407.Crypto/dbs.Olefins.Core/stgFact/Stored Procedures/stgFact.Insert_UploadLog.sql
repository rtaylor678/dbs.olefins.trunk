﻿CREATE PROCEDURE [stgFact].[Insert_UploadLog]
(
	@Refnum				VARCHAR (25),
	@FileType			VARCHAR (20),

	@FilePath			VARCHAR (512),

	@TimeBeg			DATETIMEOFFSET(7),
	@TimeEnd			DATETIMEOFFSET(7)		= NULL,

	@Error_Count		INT						= NULL,
	@Msg				VARCHAR (MAX)			= NULL,
	@Archive			BIT						= 0
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[UploadLog]([Refnum], [FileType], [FilePath], [TimeBeg], [TimeEnd], [Error_Count], [Msg], [Archive])
	VALUES(@Refnum, @FileType, @FilePath, @TimeBeg, @TimeEnd, @Error_Count, @Msg, @Archive);

END;