﻿CREATE TABLE [stgFact].[MetaOpEx] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Type]           VARCHAR (10)       NOT NULL,
    [NVE]            REAL               NULL,
    [Vol]            REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_MetaOpEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaOpEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaOpEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaOpEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_MetaOpEx] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Type] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_MetaOpEx_u]
	ON [stgFact].[MetaOpEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[MetaOpEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[MetaOpEx].Refnum		= INSERTED.Refnum
		AND	[stgFact].[MetaOpEx].[Type]		= INSERTED.[Type];

END;