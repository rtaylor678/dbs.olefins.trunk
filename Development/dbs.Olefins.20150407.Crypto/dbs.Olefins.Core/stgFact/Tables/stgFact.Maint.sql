﻿CREATE TABLE [stgFact].[Maint] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [ProjectID]      VARCHAR (15)       NOT NULL,
    [RoutMaintMatl]  REAL               NULL,
    [RoutMaintLabor] REAL               NULL,
    [RoutMaintTot]   REAL               NULL,
    [TAMatl]         REAL               NULL,
    [TALabor]        REAL               NULL,
    [TATot]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Maint_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Maint_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Maint_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Maint_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Maint] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ProjectID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Maint_u]
	ON [stgFact].[Maint]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Maint]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Maint].Refnum		= INSERTED.Refnum
		AND	[stgFact].[Maint].ProjectID		= INSERTED.ProjectID;

END;