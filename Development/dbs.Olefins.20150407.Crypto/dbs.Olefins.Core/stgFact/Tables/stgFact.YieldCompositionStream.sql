﻿CREATE TABLE [stgFact].[YieldCompositionStream] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [SimModelId]       VARCHAR (12)       NOT NULL,
    [OpCondId]         VARCHAR (12)       NOT NULL,
    [StreamId]         VARCHAR (42)       NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_YieldCompositionStream_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_YieldCompositionStream_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_YieldCompositionStream_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_YieldCompositionStream_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_YieldCompositionStream] PRIMARY KEY CLUSTERED ([Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC, [ComponentId] ASC),
    CONSTRAINT [FK_stgFact_YieldCompositionStream_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_stgFact_YieldCompositionStream_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_stgFact_YieldCompositionStream_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId])
);


GO

CREATE TRIGGER [stgFact].[t_YieldCompositionStream_u]
	ON [stgFact].[YieldCompositionStream]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[YieldCompositionStream]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[YieldCompositionStream].Refnum		= INSERTED.Refnum
		AND	[stgFact].[YieldCompositionStream].SimModelId	= INSERTED.SimModelId
		AND	[stgFact].[YieldCompositionStream].OpCondId		= INSERTED.OpCondId
		AND	[stgFact].[YieldCompositionStream].StreamId		= INSERTED.StreamId
		AND	[stgFact].[YieldCompositionStream].ComponentId	= INSERTED.ComponentId;

END;