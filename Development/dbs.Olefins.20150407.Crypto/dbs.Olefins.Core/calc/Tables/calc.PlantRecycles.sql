﻿CREATE TABLE [calc].[PlantRecycles] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [RecycleId]      INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PlantRecycles_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PlantRecycles_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PlantRecycles_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PlantRecycles_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PlantRecycles] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PlantRecycles_RecycleId] CHECK ([RecycleId]>=(0)),
    CONSTRAINT [FK_calc_PlantRecycles_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PlantRecycles_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PlantRecycles_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [calc].[t_PlantRecycles_u]
	ON [calc].[PlantRecycles]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[PlantRecycles]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[calc].[PlantRecycles].[FactorSetId]	= INSERTED.[FactorSetId]
		AND [calc].[PlantRecycles].[Refnum]			= INSERTED.[Refnum]
		AND [calc].[PlantRecycles].[CalDateKey]		= INSERTED.[CalDateKey];

END;