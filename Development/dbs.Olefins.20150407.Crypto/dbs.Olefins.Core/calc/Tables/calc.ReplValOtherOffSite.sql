﻿CREATE TABLE [calc].[ReplValOtherOffSite] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [OtherOffSite_IBSL] REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_ReplValOtherOffSite_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_ReplValOtherOffSite_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_ReplValOtherOffSite_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_ReplValOtherOffSite_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ReplValOtherOffSite] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ReplValOtherOffSite_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ReplValOtherOffSite_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_ReplValOtherOffSite_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ReplValOtherOffSite_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ReplValOtherOffSite_u
	ON  calc.ReplValOtherOffSite
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ReplValOtherOffSite
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ReplValOtherOffSite.FactorSetId	= INSERTED.FactorSetId
		AND	calc.ReplValOtherOffSite.Refnum			= INSERTED.Refnum
		AND calc.ReplValOtherOffSite.CalDateKey		= INSERTED.CalDateKey
		AND calc.ReplValOtherOffSite.CurrencyRpt	= INSERTED.CurrencyRpt;
				
END