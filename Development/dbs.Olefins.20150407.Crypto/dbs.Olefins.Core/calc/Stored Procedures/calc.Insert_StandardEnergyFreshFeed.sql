﻿CREATE PROCEDURE [calc].[Insert_StandardEnergyFreshFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [calc].[StandardEnergyFreshFeed]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [Energy_kBTU], [Yield_HvcFeed], [Energy_kBtu_LbHvc], [ProdHvc_kMT], [StandardEnergy_MBtuDay])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'FreshPyroFeed',
			SUM(n.[NicenessEnergy_Tot]) + i.[Energy]			[Energy_kBTU],
			SUM(n.[NicenessYield_Tot]) + i.[Yield]				[Yield_HvcFeed],

			(SUM(n.[NicenessEnergy_Tot]) + i.[Energy])
				 / (SUM(n.[NicenessYield_Tot]) + i.[Yield])		[Energy_kBtu_LbHvc],

			c.[Component_kMT]									[ProdHvc_kMT],

			(SUM(n.[NicenessEnergy_Tot]) + i.[Energy])
				/ (SUM(n.[NicenessYield_Tot]) + i.[Yield]) 
				* c.[Component_kMT] * 2204.6 / 365.0			[StandardEnergy_MBtuDay]
		FROM @fpl											fpl
		INNER JOIN [calc].[NicenessEnergy]					n
			ON	n.[FactorSetId]	= fpl.[FactorSetId]
			AND	n.[Refnum]		= fpl.[Refnum]
			AND	n.[CalDateKey]	= fpl.[Plant_QtrDateKey]
		INNER JOIN [ante].[NicenessModelIntercepts]			i
			ON	i.[FactorSetId]	= fpl.[FactorSetId]
		INNER JOIN [calc].[CompositionYieldPlantAggregate]	c
			ON	c.[FactorSetId]	= fpl.[FactorSetId]
			AND	c.[Refnum]		= fpl.[Refnum]
			AND	c.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	c.SimModelId	= 'Plant'
			AND	c.OpCondId		= 'OSOP'
			AND	c.ComponentId	= 'ProdHVC'

		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			i.[Energy],
			i.[Yield],
			c.[Component_kMT];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;