﻿CREATE PROCEDURE calc.Insert_TaAdjRatio
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.TaAdjRatio(FactorSetId, Refnum, CalDateKey, StreamId, ComponentId, OppLossId, Stream_kMT, Component_kMT, OppLoss_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,

			c.StreamId,
			p.ComponentId,
			l.OppLossId,

			c.Capacity_kMT			[Stream_kMT],
			p.Production_kMT		[Component_kMT],
			l._TotLoss_kMT			[OppLoss_kMT]
		FROM @fpl									tsq
		INNER JOIN fact.Capacity					c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = 'Ethylene'
		LEFT OUTER JOIN inter.DivisorsProduction	p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
			AND	p.ComponentId = 'C2H4'
		LEFT OUTER JOIN fact.ReliabilityOppLoss		l
			ON	l.Refnum = tsq.Refnum
			AND	l.CalDateKey = tsq.Plant_QtrDateKey
			AND l.OppLossId = 'TurnAround'
			AND l.StreamId = 'Ethylene'
			AND l.StreamId = c.StreamId
		WHERE tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;