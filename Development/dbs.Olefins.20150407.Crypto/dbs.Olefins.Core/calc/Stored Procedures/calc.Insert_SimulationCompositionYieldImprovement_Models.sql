﻿CREATE PROCEDURE [calc].[Insert_SimulationCompositionYieldImprovement_Models]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[SimulationCompositionYieldImprovement]([FactorSetId], [Refnum], [CalDateKey], [SimModelId], [OpCondId], [RecycleId], [ComponentId], [Simulation_kMT])
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			s.ModelId,
			o.OpCondId,
			y.RecycleId,
			b.DescendantId,
			CASE WHEN a.DescendantId IS NULL THEN y.Component_Extrap_kMT END	[Component_Extrap_kMT]
		FROM @fpl											fpl

		INNER JOIN dim.SimModel_LookUp						s
			ON	s.ModelId <> 'Plant'
		INNER JOIN dim.SimOpCond_LookUp						o
			ON	o.OpCondId <> 'PlantFeed'
		INNER JOIN dim.Component_Bridge						b
			ON	b.FactorSetId = fpl.FactorSetId
			AND	b.ComponentId = 'ProdHVC'

		INNER JOIN calc.CompositionYieldPlant				y
			ON	y.FactorSetId = fpl.FactorSetId
			AND	y.Refnum = fpl.Refnum
			AND	y.CalDateKey = fpl.Plant_QtrDateKey
			AND	y.SimModelId = s.ModelId
			AND	y.OpCondId = o.OpCondId
			AND	y.ComponentId = b.DescendantId
		LEFT OUTER JOIN dim.Component_Bridge				a
			ON	a.FactorSetId = fpl.FactorSetId
			AND	a.DescendantId = b.DescendantId
			AND	a.ComponentId = 'AcetMAPD'
		WHERE	CASE WHEN a.DescendantId IS NULL THEN y.Component_Extrap_kMT END IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;