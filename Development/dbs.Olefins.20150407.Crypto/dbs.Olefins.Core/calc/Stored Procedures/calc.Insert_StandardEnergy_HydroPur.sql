﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_HydroPur]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			m.ModelId,
			k.StandardEnergyId,
			k.StandardEnergyId,
			k.Value * CONVERT(REAL, f.Unit_Count, 2) * ABS(q.Tot_kMT) / 1000.0 *
					2204.6 * 379.0 / 2.0 / (8.0 - 7.0 * calc.ConvMoleWeight('H2', c.Component_WtPcnt, 'M') / 100.0)
		FROM @fpl												tsq
		INNER JOIN fact.FacilitiesCount							f	WITH (NOEXPAND)
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN fact.QuantityPivot							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND	q.StreamId = 'Hydrogen'
			AND q.Tot_kMT IS NOT NULL
		INNER JOIN calc.[CompositionStream]						c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = q.StreamId
			AND	c.StreamId = 'Hydrogen'
			AND	c.ComponentId = 'H2'
			AND	c.StreamDescription = q.StreamDescription
		INNER JOIN ante.StandardEnergyCoefficients				k
			ON	k.FactorSetId = tsq.FactorSetId
			AND k.StandardEnergyId = f.FacilityId
		INNER JOIN dim.SimModel_LookUp							m
			ON	m.ModelId	<>	'Plant'
		WHERE	f.FacilityId IN ('HydroPurCryogenic', 'HydroPurMembrane', 'HydroPurPSA')
			AND	k.Value > 0.0
			AND	f.Unit_Count > 0
			AND	q.Tot_kMT > 0.0
			AND	tsq.FactorSet_AnnDateKey	<= 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;