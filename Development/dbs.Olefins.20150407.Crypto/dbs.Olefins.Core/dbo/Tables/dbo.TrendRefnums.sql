﻿CREATE TABLE [dbo].[TrendRefnums] (
    [Refnum]      VARCHAR (25) NOT NULL,
    [RefnumP1]    VARCHAR (25) NULL,
    [RefnumP2]    VARCHAR (25) NULL,
    [RefnumP3]    VARCHAR (25) NULL,
    [StudyYear]   INT          NULL,
    [StudyYearP1] INT          NULL,
    [StudyYearP2] INT          NULL,
    [StudyYearP3] INT          NULL,
    CONSTRAINT [PK_TrendRefnums] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 70)
);

