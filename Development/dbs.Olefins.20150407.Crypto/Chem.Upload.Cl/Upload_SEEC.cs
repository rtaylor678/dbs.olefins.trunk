﻿using System;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public class Upload_SEEC: Chem.UpLoad.InputForm
	{
		public void UpLoad(string Refnum, string PathFile)
		{
			string sRefnum = Common.GetRefnumSmall(Refnum);
			uint StudyYear = Common.GetStudyYear(Refnum);

			Delete(sRefnum);
			Insert(PathFile, sRefnum, StudyYear);
		}

		void Insert(string PathFile, string sRefnum, uint StudyYear)
		{
			Excel.Application xla = Common.NewExcelApplication();
			Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, PathFile);

			Parallel.Invoke
			(
				() => { UpLoadTSort(wkb, sRefnum, StudyYear); },
				() => { UpLoadCapacity(wkb, sRefnum, StudyYear); },
				() => { UpLoadFacilities(wkb, sRefnum, StudyYear); },
				() => { UpLoadFeedQuality(wkb, sRefnum); },
				() => { UpLoadQuantity(wkb, sRefnum); },
				() => { UpLoadEnergyQnty(wkb, sRefnum, StudyYear); },
				() => { UpLoadMisc_SEEC(wkb, sRefnum, StudyYear); },
				() => { UpLoadProdQuality(wkb, sRefnum); },
				() => { UpLoadComposition(wkb, sRefnum); }
			);

			Common.CloseExcel(ref xla, ref wkb);
		}
	}
}
