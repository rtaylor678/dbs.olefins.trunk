﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPolyMatlBalance(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;
			Excel.Range rngMatl = null;
			Excel.Range rngProd = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const UInt32 cBeg = 6;
			const UInt32 cEnd = 11;

			const UInt32 d = 3;	   // Description Column

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table12-2"];

			for (c = cBeg; c <= cEnd; c++)
			{
				try
				{
					rngMatl = wks.Range[wks.Cells[7, c], wks.Cells[21, c]];
					rngProd = wks.Range[wks.Cells[28, c], wks.Cells[70, c]];
					rng = wks.Cells[76, c];
					if (RangeHasValue(rngMatl) || RangeHasValue(rngProd) || RangeHasValue(rng))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PolyMatlBalance]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum				CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
						
						//@TrainID            TINYINT
						cmd.Parameters.Add("@TrainID", SqlDbType.TinyInt).Value = Convert.ToByte(c - cBeg + 1);

						#region Raw Materials

						//@Hydrogen           REAL         = NULL
						r = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Hydrogen", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Ethylene_Plant     REAL         = NULL
						r = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Ethylene_Plant", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Ethylene_Other     REAL         = NULL
						r = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Ethylene_Other", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Propylene_Plant    REAL         = NULL
						r = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Propylene_Plant", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Propylene_Other    REAL         = NULL
						r = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Propylene_Other", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Butene             REAL         = NULL
						r = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Butene", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Hexene             REAL         = NULL
						r = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Hexene", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Octene             REAL         = NULL
						r = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Octene", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthMono1           REAL         = NULL
						r = 16;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthMono1", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthMono2           REAL         = NULL
						r = 17;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthMono2", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthMono3           REAL         = NULL
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthMono3", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Additives          REAL         = NULL
						r = 19;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Additives", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Catalysts          REAL         = NULL
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Catalysts", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OtherFeed          REAL         = NULL
						r = 21;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OtherFeed", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TotalRawMatl       REAL         = NULL
						r = 22;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotalRawMatl", SqlDbType.Float).Value = ReturnFloat(rng); }

						#region Other Monomer Description

						//@OthMono1Desc       VARCHAR (50) = NULL
						r = 16;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthMono1Desc", SqlDbType.VarChar, 50).Value = ConvertFeedProdId(rng, 50); }

						//@OthMono2Desc       VARCHAR (50) = NULL
						r = 17;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthMono2Desc", SqlDbType.VarChar, 50).Value = ConvertFeedProdId(rng, 50); }

						//@OthMono3Desc       VARCHAR (50) = NULL
						r = 18;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthMono3Desc", SqlDbType.VarChar, 50).Value = ConvertFeedProdId(rng, 50); }

						#endregion

						#endregion

						#region LDPE

						//@LDPE_Liner         REAL         = NULL
						r = 28;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_Liner", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LDPE_Clarity       REAL         = NULL
						r = 29;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_Clarity", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LDPE_Blow          REAL         = NULL
						r = 30;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_Blow", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LDPE_GP            REAL         = NULL
						r = 31;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_GP", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LDPE_LidResin      REAL         = NULL
						r = 32;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_LidResin", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LDPE_EVA           REAL         = NULL
						r = 33;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_EVA", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LDPE_Other         REAL         = NULL
						r = 34;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_Other", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LDPE_Offspec       REAL         = NULL
						r = 35;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LDPE_Offspec", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region LLDPE

						//@LLDPE_ButeneLiner  REAL         = NULL
						r = 37;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_ButeneLiner", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_ButeneMold   REAL         = NULL
						r = 38;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_ButeneMold", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_HAOLiner     REAL         = NULL
						r = 39;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_HAOLiner", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_HAOMeltIndex REAL         = NULL
						r = 40;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_HAOMeltIndex", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_HAOInject    REAL         = NULL
						r = 41;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_HAOInject", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_HAOLidResin  REAL         = NULL
						r = 42;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_HAOLidResin", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_HAORoto      REAL         = NULL
						r = 43;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_HAORoto", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_Other        REAL         = NULL
						r = 44;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_Other", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LLDPE_Offspec      REAL         = NULL
						r = 45;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LLDPE_Offspec", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region HDPE

						//@HDPE_Homopoly      REAL         = NULL
						r = 47;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Homopoly", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_Copoly        REAL         = NULL
						r = 48;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Copoly", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_Film          REAL         = NULL
						r = 49;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Film", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_GP            REAL         = NULL
						r = 50;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_GP", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_Pipe          REAL         = NULL
						r = 51;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Pipe", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_Drum          REAL         = NULL
						r = 52;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Drum", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_Roto          REAL         = NULL
						r = 53;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Roto", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_Other         REAL         = NULL
						r = 54;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Other", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HDPE_Offspec       REAL         = NULL
						r = 55;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HDPE_Offspec", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region POLY

						//@POLY_Homo          REAL         = NULL
						r = 57;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_Homo", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@POLY_Copoly        REAL         = NULL
						r = 58;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_Copoly", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@POLY_CopolyBlow    REAL         = NULL
						r = 59;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_CopolyBlow", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@POLY_CopolyInject  REAL         = NULL
						r = 60;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_CopolyInject", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@POLY_CopolyBlock   REAL         = NULL
						r = 61;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_CopolyBlock", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@POLY_AtacticPP     REAL         = NULL
						r = 62;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_AtacticPP", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@POLY_Other         REAL         = NULL
						r = 63;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_Other", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@POLY_Offspec       REAL         = NULL
						r = 64;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POLY_Offspec", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Other By-Products

						//@ByProdOth1         REAL         = NULL
						r = 66;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth1", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ByProdOth2         REAL         = NULL
						r = 67;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth2", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ByProdOth3         REAL         = NULL
						r = 68;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth3", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ByProdOth4         REAL         = NULL
						r = 69;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth4", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ByProdOth5         REAL         = NULL
						r = 70;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth5", SqlDbType.Float).Value = ReturnFloat(rng); }
		
						//@TotProducts        REAL         = NULL
						r = 71;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotProducts", SqlDbType.Float).Value = ReturnFloat(rng); }
		
						#region Other By-Products Description

						//@ByProdOth1Desc     VARCHAR (50) = NULL
						r = 66;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth1Desc", SqlDbType.NVarChar, 50).Value = ConvertFeedProdId(rng, 50); }

						//@ByProdOth2Desc     VARCHAR (50) = NULL
						r = 67;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth2Desc", SqlDbType.NVarChar, 50).Value = ConvertFeedProdId(rng, 50); }

						//@ByProdOth3Desc     VARCHAR (50) = NULL
						r = 68;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth3Desc", SqlDbType.NVarChar, 50).Value = ConvertFeedProdId(rng, 50); }

						//@ByProdOth4Desc     VARCHAR (50) = NULL
						r = 69;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth4Desc", SqlDbType.NVarChar, 50).Value = ConvertFeedProdId(rng, 50); }

						//@ByProdOth5Desc     VARCHAR (50) = NULL
						r = 70;
						rng = wks.Cells[r, d];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ByProdOth5Desc", SqlDbType.NVarChar, 50).Value = ConvertFeedProdId(rng, 50); }
	
						#endregion

						#endregion

						//@ReactorProd        REAL         = NULL
						r = 76;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ReactorProd", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MonomerConsumed    REAL         = NULL
						r = 77;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MonomerConsumed", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MonomerUtil        REAL         = NULL
						r = 78;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MonomerUtil", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPolyMatlBalance", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PolyMatlBalance]", ex);
				}
			}
			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rngMatl = null;
			rngProd = null;
			rng = null;
			wks = null;
		}
	}
}
