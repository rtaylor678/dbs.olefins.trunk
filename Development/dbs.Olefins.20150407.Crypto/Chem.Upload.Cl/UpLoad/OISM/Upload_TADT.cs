﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadTADT(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const UInt32 cBeg = 6;
			const UInt32 cEnd = 8;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table6-3"];

			for (c = cBeg; c <= cEnd; c++)
			{
				try
				{
					r = 6;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_TADT]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@TrainID			TINYINT,
						cmd.Parameters.Add("@TrainID", SqlDbType.Int).Value = (byte)(c - 5);

						#region Train Turnaround

						//@PctTotEthylCap	REAL     = NULL
						r = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PctTotEthylCap", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PlanInterval		REAL     = NULL
						r = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PlanInterval", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PlanDT			REAL     = NULL
						r = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PlanDT", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ActInterval		REAL     = NULL
						r = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ActInterval", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ActDT			REAL     = NULL
						r = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ActDT", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TACost			REAL     = NULL
						r = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TACost", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TAManHrs			REAL     = NULL
						r = 15;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TAManHrs", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TADate			DATETIME = NULL
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TADate", SqlDbType.DateTime).Value = ReturnDateTime(rng); }

						//@TAMini			CHAR (3) = NULL
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TAMini", SqlDbType.VarChar, 3).Value = ReturnString(rng, 3); }

						#endregion

						cmd.ExecuteNonQuery();

					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadTADT", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_TADT]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
