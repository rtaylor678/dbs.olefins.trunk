﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadMetaFeedProd(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table13"];

			string FeedProdId = string.Empty;

			object[] itm = new object[2]
			{
				new object[3] { "Feed", 15, 24 },
				new object[3] { "Prod", 31, 34 }
			};

			foreach (object[] s in itm)
			{
				try
				{
					rBeg = Convert.ToUInt32(s[1]);
					rEnd = Convert.ToUInt32(s[2]);

					for (r = rBeg; r <= rEnd; r++)
					{
						rng = wks.Range[wks.Cells[r, 6], wks.Cells[r, 9]];
						if (RangeHasValue(rng))
						{
							switch (r)
							{
								case 15: FeedProdId = "Ethy_OC"; break;
								case 16: FeedProdId = "Butene1_OC"; break;
								case 17: FeedProdId = "Butene2_OC"; break;
								case 18: FeedProdId = "Butanes_OC"; break;
								case 19: FeedProdId = "Oth_OC"; break;

								case 20: FeedProdId = "Ethy_Purch"; break;
								case 21: FeedProdId = "Butene1_Purch"; break;
								case 22: FeedProdId = "Butene2_Purch"; break;
								case 23: FeedProdId = "Butanes_Purch"; break;
								case 24: FeedProdId = "Oth_Purch"; break;

								case 31: FeedProdId = "PGP"; break;
								case 32: FeedProdId = "UnreactLP"; break;
								case 33: FeedProdId = "UnreatButanes"; break;
								case 34: FeedProdId = "C5"; break;
							}

							SqlCommand cmd = new SqlCommand("[stgFact].[Insert_MetaFeedProd]", cn);
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum				CHAR (9)
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdId			CHAR (20)
							cmd.Parameters.Add("@FeedProdId", SqlDbType.VarChar, 20).Value = FeedProdId;

							//@Qtr1			REAL		= NULL,
							c = 6;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr1", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Qtr2			REAL		= NULL,
							c = 7;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr2", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Qtr3			REAL		= NULL,
							c = 8;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr3", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@Qtr4			REAL		= NULL,
							c = 9;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Qtr4", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@AnnFeedProd		REAL		= NULL
							c = 10;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@AnnFeedProd", SqlDbType.Float).Value = ReturnFloat(rng); }
			
							cmd.ExecuteNonQuery();
						}
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMetaFeedProd", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_MetaFeedProd]", ex);
				}
			}
			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
