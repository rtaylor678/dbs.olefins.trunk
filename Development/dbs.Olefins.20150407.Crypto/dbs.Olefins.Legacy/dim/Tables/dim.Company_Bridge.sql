﻿CREATE TABLE [dim].[Company_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [CompanyId]          INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Company_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Company_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Company_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Company_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Company_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Company_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [CompanyId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_Company_Bridge_CompanyId] FOREIGN KEY ([CompanyId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_Company_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_Company_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Company_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Company_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [CompanyId]) REFERENCES [dim].[Company_Parent] ([MethodologyId], [CompanyId]),
    CONSTRAINT [FK_Company_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[Company_Parent] ([MethodologyId], [CompanyId])
);


GO

CREATE TRIGGER [dim].[t_Company_Bridge_u]
	ON [dim].[Company_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Company_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Company_Bridge].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Company_Bridge].[CompanyId]		= INSERTED.[CompanyId]
		AND	[dim].[Company_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;