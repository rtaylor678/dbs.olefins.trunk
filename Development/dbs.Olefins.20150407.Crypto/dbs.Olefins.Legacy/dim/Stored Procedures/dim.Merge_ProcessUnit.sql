﻿CREATE PROCEDURE [dim].[Merge_ProcessUnit]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [dim].[ProcessUnit_LookUp] AS Target
	USING
	(
		VALUES
		('Total', 'Total', 'Total EDC and ProcessUnit Consumption'),
			('ProcUnit', 'Process Units', 'Process Units'),
				('Feed', 'Feed', 'Feed'),
					('Fresh', 'Fresh', 'Fresh Feed, MT/sd'),
					('Supp', 'Suplemental', 'Suplemental Feed, MT/sd'),
				('Auxiliary', 'Auxiliary', 'Auxiliary Units'),
					('FeedPrep', 'Feedstock Preparation', 'Feedstock Preparation, b/sd'),
					('PyroGasHydroTreat', 'Pyrolysis Gasoline Hydrotreating', 'Pyrolysis Gasoline Hydrotreating, b/sd'),
					('HydroPur', 'Hydrogen Purification', 'Hydrogen Purification for Sales, kSCF/day'),
				('Reduction', 'Reduction', 'Reductions For Lower Purity and Cracked Gas Transfers'),
					('RedPropylene', 'Propylene Reduction', 'Propylene Reduction'),
						('RedPropyleneCG', 'Chemical-Grade Propylene Reduction', 'Chemical-Grade Propylene Reduction'),
						('RedPropyleneRG', 'Refinery-Grade Propylene Reduction', 'Refinery-Grade Propylene Reduction'),
					('RedEthylene', 'Ethylene Reduction', 'Ethylene Reduction'),
						('RedEthyleneCG', 'Chemical Grade Ethylene Reduction', 'Chemical Grade Ethylene Reduction'),
					('RedCrackedGasTrans', 'Cracked Gas Transfers Reduction', 'Cracked Gas Transfers Reduction'),
			('Utilities', 'Utilities', 'Utilities'),
				('BoilFiredSteam', 'Fired Steam Boilers', 'Fired Steam Boilers, klbs/hour'),
				('ElecGen', 'Electric Power Generation', 'Electric Power Generation, MW')
	)
	AS Source([ProcessUnitTag], [ProcessUnitName], [ProcessUnitDetail])
	ON	Target.[ProcessUnitTag]			= Source.[ProcessUnitTag]
	WHEN MATCHED THEN UPDATE SET
		Target.[ProcessUnitName]		= Source.[ProcessUnitName],
		Target.[ProcessUnitDetail]		= Source.[ProcessUnitDetail]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([ProcessUnitTag], [ProcessUnitName], [ProcessUnitDetail])
		VALUES([ProcessUnitTag], [ProcessUnitName], [ProcessUnitDetail])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	MERGE INTO [dim].[ProcessUnit_Parent] AS Target
	USING
	(
		SELECT
			m.MethodologyId,
			l.ProcessUnitId,
			p.ProcessUnitId,
			t.Operator,
			t.SortKey,
			'/'
		FROM (VALUES
			('Total', 'Total',						'+', 1000000),
				('ProcUnit', 'Total',					'+', 1300000),
					('Feed', 'ProcUnit',					'+', 1303000),
						('Fresh',	'Feed',						'+', 1303300),
						('Supp',	'Feed',						'+', 1303600),
					('Auxiliary',	'ProcUnit',				'+', 1306000),
						('FeedPrep', 'Auxiliary',				'+', 1306200),
						('PyroGasHydroTreat', 'Auxiliary',		'+', 1306400),
						('HydroPur', 'Auxiliary',				'+', 1306600),
					('Reduction', 'ProcUnit',				'+', 1309000),
						('RedPropylene', 'Reduction',			'+', 1309200),
							('RedPropyleneCG', 'RedPropylene',		'+', 1309203),
							('RedPropyleneRG', 'RedPropylene',		'+', 1309206),
						('RedEthylene', 'Reduction',			'+', 1309400),
							('RedEthyleneCG', 'RedEthylene',		'+', 1318403),
						('RedCrackedGasTrans', 'Reduction',		'+', 1309600),
				('Utilities', 'Total',					'+', 1600000),
					('BoilFiredSteam', 'Utilities',			'+', 1603000),
					('ElecGen', 'Utilities',				'+', 1606000)
			)	t(ProcessUnitTag, ParentTag, Operator, SortKey)
		INNER JOIN [dim].[ProcessUnit_LookUp]		l
			ON	l.ProcessUnitTag = t.ProcessUnitTag
		INNER JOIN [dim].[ProcessUnit_LookUp]		p
			ON	p.ProcessUnitTag = t.ParentTag
		INNER JOIN [ante].[Methodology]				m
			ON	m.[MethodologyTag] = '2013'
	)
	AS Source([MethodologyId], [ProcessUnitId], [ParentId], [Operator], [SortKey], [Hierarchy])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[ProcessUnitId]		= Source.[ProcessUnitId]
	WHEN MATCHED THEN UPDATE SET
		Target.[ParentId]		= Source.[ParentId],
		Target.[Operator]		= Source.[Operator],
		Target.[SortKey]		= Source.[SortKey],
		Target.[Hierarchy]		= Source.[Hierarchy]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [ProcessUnitId], [ParentId], [Operator], [SortKey], [Hierarchy])
		VALUES([MethodologyId], [ProcessUnitId], [ParentId], [Operator], [SortKey], [Hierarchy])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	EXECUTE dim.Update_Parent 'dim', 'ProcessUnit_Parent', 'MethodologyId', 'ProcessUnitId', 'ParentId', 'SortKey', 'Hierarchy';
	EXECUTE dim.Merge_Bridge 'dim', 'ProcessUnit_Parent', 'dim', 'ProcessUnit_Bridge', 'MethodologyId', 'ProcessUnitId', 'SortKey', 'Hierarchy', 'Operator';

END;