﻿CREATE TABLE [calc].[StandardEnergy_Reduction] (
    [MethodologyId]          INT                NOT NULL,
    [SubmissionId]           INT                NOT NULL,
    [ProcessUnitId]          INT                NOT NULL,
    [StreamId]               INT                NOT NULL,
    [Quantity_kMT]           FLOAT (53)         NOT NULL,
    [Quantity_bbl]           FLOAT (53)         NOT NULL,
    [StandardEnergy_MBtu]    FLOAT (53)         NOT NULL,
    [StandardEnergy_MBtuDay] FLOAT (53)         NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_Reduction_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Reduction_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Reduction_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Reduction_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_Reduction] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [ProcessUnitId] ASC),
    CONSTRAINT [CR_StandardEnergy_Reduction_Quantity_bbl_MinIncl_0.0] CHECK ([Quantity_bbl]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Reduction_Quantity_kMT_MinIncl_0.0] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Reduction_StandardEnergy_MBtu_MinIncl_0.0] CHECK ([StandardEnergy_MBtu]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Reduction_StandardEnergy_MBtuDay_MinIncl_0.0] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [FK_StandardEnergy_Reduction_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StandardEnergy_Reduction_ProcessUnit] FOREIGN KEY ([ProcessUnitId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId]),
    CONSTRAINT [FK_StandardEnergy_Reduction_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_StandardEnergy_Reduction_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_StandardEnergy_Reduction_u]
	ON [calc].[StandardEnergy_Reduction]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_Reduction]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_Reduction].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_Reduction].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[StandardEnergy_Reduction].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;