﻿CREATE TABLE [xls].[StreamComposition] (
    [Refnum]            VARCHAR (12)       NOT NULL,
    [StreamNumber]      INT                NOT NULL,
    [StreamId]          INT                NOT NULL,
    [ComponentId]       INT                NOT NULL,
    [Component_WtPcnt]  FLOAT (53)         NOT NULL,
    [Component_Dur_kMT] FLOAT (53)         NOT NULL,
    [Component_Ann_kMT] FLOAT (53)         NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_StreamComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamComposition] PRIMARY KEY CLUSTERED ([Refnum] DESC, [StreamNumber] ASC, [ComponentId] ASC),
    CONSTRAINT [CL_StreamComposition_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_StreamComposition_Component_Ann_kMT_MinIncl_0.0] CHECK ([Component_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamComposition_Component_Dur_kMT_MinIncl_0.0] CHECK ([Component_Dur_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MaxIncl_100.0] CHECK ([Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MinIncl_0.0] CHECK ([Component_WtPcnt]>=(0.0)),
    CONSTRAINT [FK_StreamComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StreamComposition_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [xls].[t_StreamComposition_u]
	ON [xls].[StreamComposition]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StreamComposition]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StreamComposition].[Refnum]			= INSERTED.[Refnum]
		AND	[xls].[StreamComposition].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[xls].[StreamComposition].[ComponentId]		= INSERTED.[ComponentId];

END;