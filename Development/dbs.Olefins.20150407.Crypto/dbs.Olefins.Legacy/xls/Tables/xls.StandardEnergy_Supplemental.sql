﻿CREATE TABLE [xls].[StandardEnergy_Supplemental] (
    [Refnum]                 VARCHAR (12)       NOT NULL,
    [Quantity_kMT]           FLOAT (53)         NOT NULL,
    [StandardEnergy_MBtu]    FLOAT (53)         NOT NULL,
    [StandardEnergy_MBtuDay] FLOAT (53)         NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_Supplemental_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Supplemental_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Supplemental_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Supplemental_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_Supplemental] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_StandardEnergy_Supplemental_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_StandardEnergy_Supplemental_Quantity_kMT_MinIncl_0.0] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Supplemental_StandardEnergy_MBtu_MinIncl_0.0] CHECK ([StandardEnergy_MBtu]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Supplemental_StandardEnergy_MBtuDay_MinIncl_0.0] CHECK ([StandardEnergy_MBtuDay]>=(0.0))
);


GO

CREATE TRIGGER [xls].[t_StandardEnergy_Supplemental_u]
	ON [xls].[StandardEnergy_Supplemental]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StandardEnergy_Supplemental]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StandardEnergy_Supplemental].[Refnum]	= INSERTED.[Refnum];

END;