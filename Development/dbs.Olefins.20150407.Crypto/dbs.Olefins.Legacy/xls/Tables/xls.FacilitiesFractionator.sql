﻿CREATE TABLE [xls].[FacilitiesFractionator] (
    [Refnum]         VARCHAR (12)       NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [FeedStock_BSD]  FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesFractionator_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesFractionator] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_FacilitiesFractionator_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_FacilitiesFractionator_FeedStock_BSD_MinIncl_0.0] CHECK ([FeedStock_BSD]>=(0.0)),
    CONSTRAINT [FK_FacilitiesFractionator_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId])
);


GO

CREATE TRIGGER [xls].[t_FacilitiesFractionator_u]
	ON [xls].[FacilitiesFractionator]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesFractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesFractionator].[Refnum]	= INSERTED.[Refnum];

END;