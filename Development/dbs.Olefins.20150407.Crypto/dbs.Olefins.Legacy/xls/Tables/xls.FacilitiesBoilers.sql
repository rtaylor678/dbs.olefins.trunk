﻿CREATE TABLE [xls].[FacilitiesBoilers] (
    [Refnum]         VARCHAR (12)       NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [Rate_kLbHr]     FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesBoilers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesBoilers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesBoilers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesBoilers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesBoilers] PRIMARY KEY CLUSTERED ([Refnum] DESC, [FacilityId] ASC),
    CONSTRAINT [CL_FacilitiesBoilers_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_FacilitiesBoilers_Rate_kLbHr_MinIncl_0.0] CHECK ([Rate_kLbHr]>=(0.0)),
    CONSTRAINT [FK_FacilitiesBoilers_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId])
);


GO

CREATE TRIGGER [xls].[t_FacilitiesBoilers_u]
	ON [xls].[FacilitiesBoilers]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesBoilers]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesBoilers].[Refnum]		= INSERTED.[Refnum]
		AND	[xls].[FacilitiesBoilers].[FacilityId]	= INSERTED.[FacilityId];

END;