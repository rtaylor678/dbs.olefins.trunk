﻿CREATE TABLE [xls].[HydrogenPurification] (
    [Refnum]                   VARCHAR (12)       NOT NULL,
    [FacilityId]               INT                NOT NULL,
    [StreamNumber]             INT                NOT NULL,
    [StreamId]                 INT                NOT NULL,
    [ComponentId]              INT                NOT NULL,
    [Component_WtPcnt]         FLOAT (53)         NOT NULL,
    [Component_kMT]            FLOAT (53)         NOT NULL,
    [Utilization_Pcnt]         FLOAT (53)         NOT NULL,
    [H2Unit_Count]             INT                NOT NULL,
    [Duration_Days]            FLOAT (53)         NOT NULL,
    [_H2Pur_MScfDur]           AS                 (CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)) PERSISTED NOT NULL,
    [_H2Pur_MScfDay]           AS                 (case when [Duration_Days]<>(0.0) then CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days]  end) PERSISTED,
    [_H2Pur_MScfYear]          AS                 (case when [Duration_Days]<>(0.0) then (CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days])*(365.0)  end) PERSISTED,
    [_H2Pur_kScfDur]           AS                 (CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)*(1000.0)) PERSISTED NOT NULL,
    [_H2Pur_kScfDay]           AS                 (case when [Duration_Days]<>(0.0) then (CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days])*(1000.0)  end) PERSISTED,
    [_H2Pur_kScfYear]          AS                 (case when [Duration_Days]<>(0.0) then ((CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days])*(365.0))*(1000.0)  end) PERSISTED,
    [_H2PurUtil_kScfDay]       AS                 (case when [Duration_Days]<>(0.0) AND [Utilization_Pcnt]<>(0.0) then (((CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days])*(1000.0))/[Utilization_Pcnt])*(100.0)  end) PERSISTED,
    [_H2PurUtil_kScfYear]      AS                 (case when [Duration_Days]<>(0.0) AND [Utilization_Pcnt]<>(0.0) then ((((CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days])*(365.0))*(1000.0))/[Utilization_Pcnt])*(100.0)  end) PERSISTED,
    [_H2PurUtilSales_kScfDay]  AS                 (case when [Duration_Days]<>(0.0) AND [Utilization_Pcnt]<>(0.0) AND [H2Unit_Count]>(0) AND [Component_WtPcnt]>(40.0) then (((CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days])*(1000.0))/[Utilization_Pcnt])*(100.0) else (0.0) end) PERSISTED NOT NULL,
    [_H2PurUtilSales_kScfYear] AS                 (case when [Duration_Days]<>(0.0) AND [Utilization_Pcnt]<>(0.0) AND [H2Unit_Count]>(0) AND [Component_WtPcnt]>(40.0) then ((((CONVERT([float],((423.3)*(1.1)+((49.33)*(3.0))*(1.1))*[Component_kMT],0)/[Duration_Days])*(365.0))*(1000.0))/[Utilization_Pcnt])*(100.0) else (0.0) end) PERSISTED NOT NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_HydrogenPurification_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (128)     CONSTRAINT [DF_HydrogenPurification_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (128)     CONSTRAINT [DF_HydrogenPurification_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (128)     CONSTRAINT [DF_HydrogenPurification_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]             ROWVERSION         NOT NULL,
    CONSTRAINT [PK_HydrogenPurification] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_HydrogenPurification_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_HydrogenPurification_Component_kMT_MinIncl_0.0] CHECK ([Component_kMT]>=(0.0)),
    CONSTRAINT [CR_HydrogenPurification_Component_WtPcnt_MaxIncl_100.0] CHECK ([Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_HydrogenPurification_Component_WtPcnt_MinIncl_0.0] CHECK ([Component_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_HydrogenPurification_Duration_Days_MinIncl_0.0] CHECK ([Duration_Days]>=(0.0)),
    CONSTRAINT [CR_HydrogenPurification_H2Unit_Count_MinIncl_0] CHECK ([H2Unit_Count]>=(0)),
    CONSTRAINT [CR_HydrogenPurification_Utilization_Pcnt_MaxIncl_100.0] CHECK ([Utilization_Pcnt]<=(110.0)),
    CONSTRAINT [CR_HydrogenPurification_Utilization_Pcnt_MinIncl_0.0] CHECK ([Utilization_Pcnt]>=(0.0)),
    CONSTRAINT [FK_HydrogenPurification_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_HydrogenPurification_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_HydrogenPurification_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [xls].[t_HydrogenPurification_u]
	ON [xls].[HydrogenPurification]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[HydrogenPurification]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[HydrogenPurification].[Refnum]	= INSERTED.[Refnum];

END;