﻿CREATE TABLE [Val].[Weighting] (
    [RefineryType] VARCHAR (10) NOT NULL,
    [Category]     VARCHAR (10) NOT NULL,
    [Weighting]    REAL         NOT NULL,
    [Section]      VARCHAR (10) NULL,
    [DivMethod]    VARCHAR (10) NULL,
    CONSTRAINT [PK_Weighting_1] PRIMARY KEY CLUSTERED ([RefineryType] ASC, [Category] ASC)
);

