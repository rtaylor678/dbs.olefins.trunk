﻿CREATE TABLE [Val].[Sections] (
    [Section]      VARCHAR (10) NOT NULL,
    [StudyYear]    INT          NOT NULL,
    [RefineryType] VARCHAR (50) NOT NULL,
    [Slope]        REAL         NOT NULL,
    [Intercept]    REAL         NOT NULL,
    [WtFactor]     REAL         NOT NULL,
    CONSTRAINT [PK_Sections] PRIMARY KEY CLUSTERED ([Section] ASC, [StudyYear] ASC, [RefineryType] ASC)
);

