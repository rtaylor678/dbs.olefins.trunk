﻿CREATE TABLE [stage].[FacilitiesFractionator] (
    [SubmissionId]   INT                NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [Quantity_kBSD]  FLOAT (53)         NOT NULL,
    [StreamId]       INT                NOT NULL,
    [Throughput_kMT] FLOAT (53)         NOT NULL,
    [Density_SG]     FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesFractionator_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesFractionator_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesFractionator] PRIMARY KEY CLUSTERED ([SubmissionId] DESC),
    CONSTRAINT [CR_FacilitiesFractionator_Density_SG_MinIncl_0.0] CHECK ([Density_SG]>=(0.0)),
    CONSTRAINT [CR_FacilitiesFractionator_Quantity_kBSD_MinIncl_0.0] CHECK ([Quantity_kBSD]>=(0.0)),
    CONSTRAINT [CR_FacilitiesFractionator_Throughput_kMT_MinIncl_0.0] CHECK ([Throughput_kMT]>=(0.0)),
    CONSTRAINT [FK_FacilitiesFractionator_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesFractionator_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FacilitiesFractionator_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [stage].[t_FacilitiesFractionator_u]
	ON [stage].[FacilitiesFractionator]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[FacilitiesFractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[FacilitiesFractionator].[SubmissionId]		= INSERTED.[SubmissionId];

END;