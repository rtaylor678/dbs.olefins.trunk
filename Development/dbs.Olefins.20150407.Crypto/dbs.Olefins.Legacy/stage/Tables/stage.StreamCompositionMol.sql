﻿CREATE TABLE [stage].[StreamCompositionMol] (
    [SubmissionId]      INT                NOT NULL,
    [StreamNumber]      INT                NOT NULL,
    [ComponentId]       INT                NOT NULL,
    [Component_MolPcnt] FLOAT (53)         NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_StreamCompositionMol_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_StreamCompositionMol_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_StreamCompositionMol_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_StreamCompositionMol_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamCompositionMol] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_StreamCompositionMol_Component_MolPcnt_MaxIncl_100.0] CHECK ([Component_MolPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamCompositionMol_Component_MolPcnt_MinIncl_0.0] CHECK ([Component_MolPcnt]>=(0.0)),
    CONSTRAINT [CR_StreamCompositionMol_StreamNumber] CHECK ([StreamNumber]>(0)),
    CONSTRAINT [FK_StreamCompositionMol_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StreamCompositionMol_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [stage].[t_StreamCompositionMol_u]
	ON [stage].[StreamCompositionMol]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamCompositionMol]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamCompositionMol].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[StreamCompositionMol].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[stage].[StreamCompositionMol].[ComponentId]	= INSERTED.[ComponentId];

END;