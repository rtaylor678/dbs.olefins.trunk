﻿CREATE PROCEDURE [Console].[UpdateComments]

	@RefNum dbo.Refnum,
	@Comments varchar(255)

AS
BEGIN

UPDATE C  SET C.Comment = @Comments  FROM CoContactInfo c join tsort t on t.ContactCode = c.contactcode and t.StudyYear = c.StudyYear
		WHERE t.Refnum=@RefNum  

END
