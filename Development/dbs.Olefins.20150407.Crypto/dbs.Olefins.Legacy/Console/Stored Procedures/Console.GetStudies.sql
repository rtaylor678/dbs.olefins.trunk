﻿CREATE PROCEDURE [Console].[GetStudies](@RefineryType varchar(5) = NULL)
AS
	SELECT distinct 'OLE' as Studies, StudyYear  
	FROM dbo.TSort
	WHERE StudyYear > 2008 
	ORDER BY StudyYear DESC
