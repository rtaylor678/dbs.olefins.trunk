﻿CREATE proc [Console].[UpdateContact]
@RefNum dbo.Refnum,
@Name nvarchar(80),
@Address1 nvarchar(80),
@Address2 nvarchar(80),
@Address3 nvarchar(80),
@City nvarchar(40),
@State nvarchar(20),
@Zip nvarchar(20),
@Phone nvarchar(20),
@Phone2 nvarchar(20),
@JobTitle nvarchar(40),
@Email nvarchar(80),
@StAddress1 as nvarchar(80),
@StAddress2 as nvarchar(80),
@StAddress3 as nvarchar(80),
@StAddress4 as nvarchar(80),
@StAddress5 as nvarchar(80),
@StAddress6 as nvarchar(80),
@Fax as nvarchar(20)

AS

UPDATE ContactInfo
SET Name = @Name, StreetAddr1 = @Address1, StreetAddr2 = @Address2, StreetAddr3 = @Address3, StreetAddr4 = @City, StreetAddr5 = @State, StreetAddr6=@Zip,
MailAddr1 = @StAddress1, MailAddr2 = @StAddress2, MailAddr3 = @StAddress3, MailAddr4 = @StAddress4, MailAddr5 = @StAddress5, MailAddr6=@StAddress6,
EMail = @Email, Title = @JobTitle, Phone = @Phone, Phone2 = @Phone2
WHERE Refnum = @RefNum
