﻿CREATE TABLE [ante].[Methodology] (
    [MethodologyId]     INT                IDENTITY (1, 1) NOT NULL,
    [MethodologyTag]    VARCHAR (42)       NOT NULL,
    [MethodologyName]   VARCHAR (84)       NOT NULL,
    [MethodologyDetail] VARCHAR (256)      NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Methodology_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_Methodology_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_Methodology_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_Methodology_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Methodology_LookUp] PRIMARY KEY CLUSTERED ([MethodologyId] DESC),
    CONSTRAINT [CL_Methodology_LookUp_MethodologyDetail] CHECK ([MethodologyDetail]<>''),
    CONSTRAINT [CL_Methodology_LookUp_MethodologyName] CHECK ([MethodologyName]<>''),
    CONSTRAINT [CL_Methodology_LookUp_MethodologyTag] CHECK ([MethodologyTag]<>''),
    CONSTRAINT [UK_Methodology_LookUp_MethodologyDetail] UNIQUE NONCLUSTERED ([MethodologyDetail] ASC),
    CONSTRAINT [UK_Methodology_LookUp_MethodologyName] UNIQUE NONCLUSTERED ([MethodologyName] ASC),
    CONSTRAINT [UK_Methodology_LookUp_MethodologyTag] UNIQUE NONCLUSTERED ([MethodologyTag] ASC)
);


GO

CREATE TRIGGER [ante].[t_Methodology_u]
	ON [ante].[Methodology]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Methodology]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Methodology].[MethodologyId]	= INSERTED.[MethodologyId];

END;