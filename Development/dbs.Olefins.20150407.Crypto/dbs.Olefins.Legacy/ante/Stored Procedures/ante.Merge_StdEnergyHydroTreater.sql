﻿CREATE PROCEDURE [ante].[Merge_StdEnergyHydroTreater]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[StdEnergyHydroTreater] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_HydroTreaterTypeId('B'),	100.0),
		(@MethodologyId, dim.Return_HydroTreaterTypeId('DS'),	 80.0),
		(@MethodologyId, dim.Return_HydroTreaterTypeId('SS'),	 55.0)
	)
	AS Source([MethodologyId], [HydroTreaterTypeId], [Energy_kBtuBbl])
	ON	Target.[MethodologyId]		= Source.[MethodologyId]
	AND	Target.[HydroTreaterTypeId]	= Source.[HydroTreaterTypeId]
	WHEN MATCHED THEN UPDATE SET
		Target.[Energy_kBtuBbl]	= Source.[Energy_kBtuBbl]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [HydroTreaterTypeId], [Energy_kBtuBbl])
		VALUES([MethodologyId], [HydroTreaterTypeId], [Energy_kBtuBbl])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;