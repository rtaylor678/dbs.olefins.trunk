﻿CREATE PROCEDURE [ante].[Merge_FactorsWwCduScu]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[FactorsWwCduScu] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_StandardId('PersMaint'),	 0.597),
			(@MethodologyId, dim.Return_StandardId('PersNonMaint'),	 0.5932),
			(@MethodologyId, dim.Return_StandardId('Mes'),			28.17),
			(@MethodologyId, dim.Return_StandardId('NonEnergy'),	64.25)
	)
	AS Source([MethodologyId], [StandardId], [WwCduScu])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StandardId]		= Source.[StandardId]
	WHEN MATCHED THEN UPDATE SET
		Target.[WwCduScu]	= Source.[WwCduScu]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StandardId], [WwCduScu])
		VALUES([MethodologyId], [StandardId], [WwCduScu])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;