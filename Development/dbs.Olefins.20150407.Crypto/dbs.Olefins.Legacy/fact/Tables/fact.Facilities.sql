﻿CREATE TABLE [fact].[Facilities] (
    [SubmissionId]   INT                NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [Unit_Count]     INT                NOT NULL,
    [_HasUnit_Bit]   AS                 (CONVERT([bit],case when [Unit_Count]>(0) then (1) else (0) end,(0))) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Facility_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Facility_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Facility_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Facility_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Facilities] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [FacilityId] ASC),
    CONSTRAINT [CR_Facility_Unit_Count_MinIncl_0] CHECK ([Unit_Count]>=(0)),
    CONSTRAINT [FK_Facility_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_Facility_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_Facility_u]
	ON [fact].[Facilities]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Facilities]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[Facilities].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[fact].[Facilities].[FacilityId]		= INSERTED.[FacilityId];

END;