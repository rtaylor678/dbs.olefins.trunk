﻿CREATE FUNCTION [dbo].[GetCompanyPassword](@RefNum dbo.Refnum)
RETURNS varchar(25)
AS
BEGIN
	DECLARE @pwd varchar(25)
	SELECT @pwd = s.Password
	FROM dbo.SAMaster s 
	WHERE s.SANumber = dbo.GetSANumber(@RefNum)
	RETURN @pwd
END
