﻿


CREATE            PROC [dbo].[CalcList] (@ListName varchar(30))

AS

DECLARE cRefnum CURSOR READ_ONLY
FOR SELECT Refnum FROM TSort WHERE @ListName IS NULL OR Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)

DECLARE @Refnum varchar(9)
OPEN cRefnum

FETCH NEXT FROM cRefnum INTO @Refnum
WHILE (@@fetch_status = 0)
BEGIN
	PRINT @Refnum
	EXEC CalcALL @Refnum
	FETCH NEXT FROM cRefnum INTO @Refnum
END

CLOSE cRefnum
DEALLOCATE cRefnum

















