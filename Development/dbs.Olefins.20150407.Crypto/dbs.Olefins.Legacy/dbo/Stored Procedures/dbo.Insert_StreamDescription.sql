﻿CREATE PROCEDURE [dbo].[Insert_StreamDescription]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamDescription		NVARCHAR(256)
)
AS
BEGIN

	IF(@StreamDescription	<> '')
	EXECUTE [stage].[Insert_StreamDescription] @SubmissionId, @StreamNumber, @StreamDescription;

END;
