﻿

/****** Object:  Stored Procedure dbo.CalcUnitOPEX    Script Date: 6/10/2008 12:53:05 PM ******/
--EXEC CalcUnitOPEX '05PCH004'

CREATE  PROC [dbo].[CalcUnitOPEX](@Refnum Refnum) as
--
--	This is really brute force way to do it, but is only for <= 2005
--
declare @SY as integer
Select @SY = StudyYear From TSort where Refnum = @Refnum

if @SY<2007 BEGIN
	Update UnitOpex set C2NEOpex = 
	(Select C2NEOpex = NonEnergyOPEX * hvc.DivFactor / b.DivFactor
	from ByProd b JOIN _OpexMisc o on o.Refnum=b.Refnum and b.ByProdBasis = 'C2'
	JOIN ByProd hvc on HVC.Refnum=o.Refnum and hvc.ByProdBasis = 'HVC'
	where o.Refnum = UnitOpex.Refnum) Where Refnum = @Refnum

	Update UnitOpex set C2C3NEOpex = 
	(Select C2NEOpex = NonEnergyOPEX * hvc.DivFactor / b.DivFactor
	from ByProd b JOIN _OpexMisc o on o.Refnum=b.Refnum and b.ByProdBasis = 'OLE'
	JOIN ByProd hvc on HVC.Refnum=o.Refnum and hvc.ByProdBasis = 'HVC'
	where o.Refnum = UnitOpex.Refnum) Where Refnum = @Refnum

	Update UnitOpex set HVChemNEOpex = 
	(Select C2NEOpex = NonEnergyOPEX * hvc.DivFactor / b.DivFactor
	from ByProd b JOIN _OpexMisc o on o.Refnum=b.Refnum and b.ByProdBasis = 'HVC'
	JOIN ByProd hvc on HVC.Refnum=o.Refnum and hvc.ByProdBasis = 'HVC'
	where o.Refnum = UnitOpex.Refnum) Where Refnum = @Refnum

	Update UnitOpex set C2EnergyOpex = 
	(Select C2NEOpex = NetEnergyCost_MTHVC * hvc.DivFactor / b.DivFactor
	from ByProd b JOIN EnergyCost o on o.Refnum=b.Refnum and b.ByProdBasis = 'C2'
	JOIN ByProd hvc on HVC.Refnum=o.Refnum and hvc.ByProdBasis = 'HVC'
	where o.Refnum = UnitOpex.Refnum) Where Refnum = @Refnum

	Update UnitOpex set C2C3EnergyOpex = 
	(Select C2NEOpex = NetEnergyCost_MTHVC * hvc.DivFactor / b.DivFactor
	from ByProd b JOIN EnergyCost o on o.Refnum=b.Refnum and b.ByProdBasis = 'OLE'
	JOIN ByProd hvc on HVC.Refnum=o.Refnum and hvc.ByProdBasis = 'HVC'
	where o.Refnum = UnitOpex.Refnum) Where Refnum = @Refnum

	Update UnitOpex set HVChemEnergyOpex = 
	(Select C2Opex = NetEnergyCost_MTHVC * hvc.DivFactor / b.DivFactor
	from ByProd b JOIN EnergyCost o on o.Refnum=b.Refnum and b.ByProdBasis = 'HVC'
	JOIN ByProd hvc on HVC.Refnum=o.Refnum and hvc.ByProdBasis = 'HVC'
	where o.Refnum = UnitOpex.Refnum) Where Refnum = @Refnum

	Update FinancialTot set NEOpex = (Select NonEnergyOPEX * b.DivFactor
	from ByProd b JOIN _OpexMisc o on o.Refnum=b.Refnum and b.ByProdBasis = 'HVC'
	where o.Refnum = FinancialTot.Refnum) Where Refnum = @Refnum

	Update FinancialTot set EnergyOpex = (Select NetEnergyCost_MTHVC * b.DivFactor
	from ByProd b JOIN EnergyCost o on o.Refnum=b.Refnum and b.ByProdBasis = 'HVC'
	where o.Refnum = FinancialTot.Refnum) Where Refnum = @Refnum

END


