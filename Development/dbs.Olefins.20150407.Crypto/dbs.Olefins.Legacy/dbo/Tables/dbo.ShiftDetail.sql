﻿CREATE TABLE [dbo].[ShiftDetail] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [WeekHrs]     REAL           NULL,
    [DayHrs]      REAL           NULL,
    [ShiftCyc]    REAL           NULL,
    [ConsolePost] REAL           NULL,
    [OutsidePost] REAL           NULL,
    [OffsitePost] REAL           NULL,
    [TotPosts]    REAL           NULL,
    [OperPerPost] REAL           NULL,
    CONSTRAINT [PK___2__12] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

