﻿CREATE TABLE [dbo].[EIIExponents] (
    [FactorSet]   SMALLINT   NOT NULL,
    [GasCompUHP]  FLOAT (53) NOT NULL,
    [MethCompUHP] FLOAT (53) NOT NULL,
    [EthyCompUHP] FLOAT (53) NOT NULL,
    [PropCompUHP] FLOAT (53) NOT NULL,
    [Purity]      FLOAT (53) NOT NULL,
    CONSTRAINT [PK_EIIExponents] PRIMARY KEY CLUSTERED ([FactorSet] ASC)
);

