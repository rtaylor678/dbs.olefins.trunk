﻿CREATE TABLE [dbo].[DD_Relationships] (
    [ObjectA] INT NOT NULL,
    [ObjectB] INT NOT NULL,
    [ColumnA] INT NOT NULL,
    [ColumnB] INT NOT NULL,
    CONSTRAINT [UniqueItemA] UNIQUE NONCLUSTERED ([ObjectA] ASC, [ObjectB] ASC, [ColumnA] ASC),
    CONSTRAINT [UniqueItemB] UNIQUE NONCLUSTERED ([ObjectA] ASC, [ObjectB] ASC, [ColumnB] ASC)
);

