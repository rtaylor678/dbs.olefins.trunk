﻿CREATE TABLE [dbo].[RankSpecs_LU] (
    [RankSpecsID]    INT           IDENTITY (1, 1) NOT NULL,
    [RefListNo]      INT           NOT NULL,
    [BreakID]        INT           NOT NULL,
    [BreakValue]     CHAR (12)     NOT NULL,
    [RankVariableID] SMALLINT      NOT NULL,
    [LastTime]       SMALLDATETIME CONSTRAINT [DF__RankSpecs__LastT__5090EFD7] DEFAULT (getdate()) NOT NULL,
    [NumTiles]       TINYINT       NULL,
    [DataCount]      SMALLINT      NULL,
    CONSTRAINT [PK_RankSpecsLU_1] PRIMARY KEY CLUSTERED ([RankSpecsID] ASC) WITH (FILLFACTOR = 50),
    CONSTRAINT [UniqRankSpec_1] UNIQUE NONCLUSTERED ([RefListNo] ASC, [BreakID] ASC, [BreakValue] ASC, [RankVariableID] ASC) WITH (FILLFACTOR = 50)
);

