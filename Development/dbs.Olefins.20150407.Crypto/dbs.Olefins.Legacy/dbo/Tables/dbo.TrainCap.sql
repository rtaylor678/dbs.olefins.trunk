﻿CREATE TABLE [dbo].[TrainCap] (
    [Refnum]        [dbo].[Refnum] NOT NULL,
    [Train1CapPcnt] REAL           NULL,
    [Train2CapPcnt] REAL           NULL,
    [Train3CapPcnt] REAL           NULL,
    CONSTRAINT [PK___TrainCap] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

