﻿CREATE TABLE [dbo].[FeedSelections] (
    [Refnum]        [dbo].[Refnum] NOT NULL,
    [FeedProdID]    VARCHAR (20)   NOT NULL,
    [Mix]           CHAR (1)       NULL,
    [Blend]         CHAR (1)       NULL,
    [Fractionation] CHAR (1)       NULL,
    [Hydrotreat]    CHAR (1)       NULL,
    [Purification]  CHAR (1)       NULL,
    [MinSGEst]      REAL           NULL,
    [MaxSGEst]      REAL           NULL,
    [MinSGAct]      REAL           NULL,
    [MaxSGAct]      REAL           NULL,
    [PipelinePct]   REAL           NULL,
    [TruckPct]      REAL           NULL,
    [BargePct]      REAL           NULL,
    [ShipPct]       REAL           NULL,
    CONSTRAINT [PK_FeedSelections] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

