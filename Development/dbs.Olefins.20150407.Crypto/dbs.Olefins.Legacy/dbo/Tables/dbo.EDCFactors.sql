﻿CREATE TABLE [dbo].[EDCFactors] (
    [FactorSet]     SMALLINT NOT NULL,
    [FracFeedKBSD]  REAL     NOT NULL,
    [HPBoilRate]    REAL     NOT NULL,
    [LPBoilRate]    REAL     NOT NULL,
    [ElecGenMW]     REAL     NOT NULL,
    [HydroCryoCnt]  REAL     NOT NULL,
    [HydroPSACnt]   REAL     NOT NULL,
    [HydroMembCnt]  REAL     NOT NULL,
    [PGasHydroKBSD] REAL     NOT NULL,
    CONSTRAINT [PK_EDCFactors] PRIMARY KEY CLUSTERED ([FactorSet] ASC)
);

