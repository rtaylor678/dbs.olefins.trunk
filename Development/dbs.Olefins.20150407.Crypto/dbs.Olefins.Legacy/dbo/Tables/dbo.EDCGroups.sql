﻿CREATE TABLE [dbo].[EDCGroups] (
    [RefineryType] VARCHAR (7)  NOT NULL,
    [StudyYear]    SMALLINT     NOT NULL,
    [FactorSet]    SMALLINT     NOT NULL,
    [GroupName]    VARCHAR (10) NOT NULL,
    [GroupValue]   SMALLINT     NOT NULL,
    [RangeMin]     REAL         NOT NULL,
    [RangeMax]     REAL         NOT NULL,
    CONSTRAINT [PK_EDCGroups] PRIMARY KEY CLUSTERED ([RefineryType] ASC, [StudyYear] ASC, [FactorSet] ASC, [GroupName] ASC, [GroupValue] ASC, [RangeMin] ASC, [RangeMax] ASC)
);

