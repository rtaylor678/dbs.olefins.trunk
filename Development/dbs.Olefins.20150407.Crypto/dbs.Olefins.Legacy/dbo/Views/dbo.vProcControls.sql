﻿






CREATE   VIEW dbo.vProcControls AS
SELECT t.Refnum,
  Tbl9003A=CASE WHEN pa.Tbl9003='A' THEN 100 ELSE 0 END,
  Tbl9003B=CASE WHEN pa.Tbl9003='B' THEN 100 ELSE 0 END,
  Tbl9003C=CASE WHEN pa.Tbl9003='C' THEN 100 ELSE 0 END,
  Tbl9003D=CASE WHEN pa.Tbl9003='D' THEN 100 ELSE 0 END,
  Tbl9003E=CASE WHEN pa.Tbl9003='E' THEN 100 ELSE 0 END,
  Tbl9005=CASE WHEN pa.Tbl9005='X' THEN 100 ELSE 0 END,
  Tbl9006=CASE WHEN pa.Tbl9006='X' THEN 100 ELSE 0 END,
  Tbl9007=CASE WHEN pa.Tbl9007='X' THEN 100 ELSE 0 END,
  Tbl9008=CASE WHEN pa.Tbl9008='X' THEN 100 ELSE 0 END,
  Tbl9010=CASE WHEN pa.Tbl9010='Y' THEN 100 ELSE 0 END,
  Tbl9018=CASE WHEN pa.Tbl9018='Y' THEN 100 ELSE 0 END,
  Tbl9019=CASE WHEN pa.Tbl9019='Y' THEN 100 ELSE 0 END
FROM PracAdv pa
INNER JOIN TSort t ON t.Refnum=pa.Refnum






