﻿
/****** Object:  View dbo.vFacilitiesIntegration    Script Date: 6/30/2006 7:30:27 AM ******/

/****** Object:  View dbo.vFacilitiesIntegration    Script Date: 6/28/2006 9:37:16 AM ******/





CREATE    VIEW dbo.vFacilitiesIntegration AS
SELECT t.Refnum, EtyCarrier=CASE WHEN pr.EtyCarrier='y' THEN 100 ELSE 0 END,
  ButaExtract=CASE WHEN pr.ButaExtract='y' THEN 100 ELSE 0 END,
  AromExtract=CASE WHEN pr.AromExtract='y' THEN 100 ELSE 0 END,
  MTBE=CASE WHEN pr.MTBE='y' THEN 100 ELSE 0 END,
  IsoMTBE=CASE WHEN pr.IsoMTBE='y' THEN 100 ELSE 0 END,
  IsoHighPurity=CASE WHEN pr.IsoHighPurity='y' THEN 100 ELSE 0 END,
  Butene1=CASE WHEN pr.Butene1='y' THEN 100 ELSE 0 END,
  Isoprene=CASE WHEN pr.Isoprene='y' THEN 100 ELSE 0 END,
  Cyclopent=CASE WHEN pr.Cyclopent='y' THEN 100 ELSE 0 END,
  IntegrationRefine=CASE WHEN po.IntegrationRefine='y' THEN 100 ELSE 0 END,
  Tbl1205=CASE WHEN po.Tbl1205='y' THEN 100 ELSE 0 END,
  PolyPlantShare=CASE WHEN po.PolyPlantShare='y' THEN 100 ELSE 0 END
FROM Prac pr
INNER JOIN TSort t ON t.Refnum=pr.Refnum
INNER JOIN PracOrg po ON t.Refnum=po.Refnum






