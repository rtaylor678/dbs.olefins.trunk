﻿

CREATE  VIEW [dbo].[PersCost]
as
SELECT 
ft.Refnum,
PersCostTA_MTHVC=(SUM(ISNULL(Opex.OCCSal,0)+ ISNULL(Opex.MPSSal,0)+ 
ISNULL(Opex.OCCBen,0) + ISNULL(Opex.MPSBen,0) + ISNULL(Opex.ContMaintLabor,0))+
SUM((0.8*ISNULL(Opex.ContTechSvc,0)) + (0.8*ISNULL(Opex.OthCont,0)))+
CASE WHEN tmc.TotTAMaint = 0 then 0 
ELSE SUM(ft.AnnTAExp* (ISNULL(tmc.TotalTALabor,0)/ISNULL(tmc.TotTAMaint,0)))END)/p.TAAdjHVChemDiv,
PersCostTA_kEDC=(SUM(ISNULL(Opex.OCCSal,0)+ ISNULL(Opex.MPSSal,0)+ 
ISNULL(Opex.OCCBen,0) + ISNULL(Opex.MPSBen,0) + ISNULL(Opex.ContMaintLabor,0))+
SUM((0.8*ISNULL(Opex.ContTechSvc,0)) + (0.8*ISNULL(Opex.OthCont,0)))+
CASE WHEN tmc.TotTAMaint = 0 then 0
ELSE SUM(ft.AnnTAExp* (ISNULL(tmc.TotalTALabor,0)/ISNULL(tmc.TotTAMaint,0)))END)/EDC.kEDC
FROM FinancialTot ft, Opex Opex, 
Production p,  TotMaintCost tmc,
EDC edc
WHERE  ft.Refnum = Opex.Refnum 
AND ft.Refnum = p.Refnum 
AND ft.Refnum = tmc.Refnum 
AND ft.Refnum = EDC.Refnum
GROUP BY ft.Refnum, p.TAAdjHVChemDiv,EDC.kEDC,
ft.AnnTAExp,tmc.TotalTALabor,tmc.TotTAMaint



