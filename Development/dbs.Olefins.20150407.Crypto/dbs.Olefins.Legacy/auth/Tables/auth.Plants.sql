﻿CREATE TABLE [auth].[Plants] (
    [PlantId]        INT                IDENTITY (1, 1) NOT NULL,
    [CompanyId]      INT                NULL,
    [PlantName]      NVARCHAR (128)     NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_Plants_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Plants_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Plants_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Plants_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Plants_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Plants] PRIMARY KEY CLUSTERED ([PlantId] ASC),
    CONSTRAINT [CL_Plants_PlantName] CHECK (len([PlantName])>(0)),
    CONSTRAINT [FK_Plants_Companies] FOREIGN KEY ([CompanyId]) REFERENCES [auth].[Companies] ([CompanyId]),
    CONSTRAINT [UK_Plants_PlantName] UNIQUE NONCLUSTERED ([PlantName] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Plants_Active]
    ON [auth].[Plants]([PlantId] ASC)
    INCLUDE([PlantName]) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_Plants_u]
	ON [auth].[Plants]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[Plants]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[Plants].[PlantId]	= INSERTED.[PlantId];

END;