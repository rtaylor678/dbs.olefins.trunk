﻿CREATE TABLE [dbo].[StudyDatabases] (
    [ServerName]   [sysname] NOT NULL,
    [DatabaseName] [sysname] NOT NULL,
    [StudyGroup]   CHAR (5)  NOT NULL,
    CONSTRAINT [PK_StudyDatabases_1__10] PRIMARY KEY CLUSTERED ([ServerName] ASC, [DatabaseName] ASC) WITH (FILLFACTOR = 90)
);

