﻿
DECLARE	@CalDateKey		INT				= 20131231;
DECLARE @StudyID		VARCHAR(4)		= 'PCH'
DECLARE @FactorSetId	VARCHAR(4)		= '2013'
SET NOCOUNT ON;

DECLARE @CompanyId		VARCHAR(42)		= 'Solomon';
DECLARE @CompanyName	VARCHAR(84)		= 'Solomon';
DECLARE @CompanyDetail	VARCHAR(256)	= 'Solomon';

DECLARE @AssetIDPri		VARCHAR(3)		= '998';
DECLARE @AssetName		VARCHAR(84)		= 'Singapore';
DECLARE @AssetDetail	VARCHAR(192)	= 'Singapore';
DECLARE @AssetCountry	VARCHAR(4)		= 'SGP';
DECLARE @AssetState		VARCHAR(42)		= NULL;

DECLARE @Refnum			VARCHAR(25)		=  @FactorSetId + @StudyID + @AssetIDPri;

--	Proforma Insert.
--INSERT INTO [cons].[SubscriptionsAssets]([CompanyId], [CalDateKey], [StudyId], [AssetId], [ScenarioId], [ScenarioName], [ScenarioDetail], [SubscriberAssetName], [SubscriberAssetDetail])
--VALUES(@CompanyId, @CalDateKey, @StudyID, @AssetIDPri, 'A', 'Proforma', 'Proforma', @AssetName, @AssetDetail);


--PRINT 'INSERT INTO [dim].[Company_LookUp]';

--INSERT INTO [dim].[Company_LookUp]([CompanyId], [CompanyName], [CompanyDetail])
--VALUES(@CompanyId, @CompanyName, @CompanyDetail);

--PRINT 'INSERT INTO [dim].[Company_Parent]';

--INSERT INTO [dim].[Company_Parent]([FactorSetId], [CompanyID], [ParentId], [Operator], [SortKey], [Hierarchy])
--VALUES(@FactorSetId, @CompanyId, @CompanyId, '~', 1, '/');

--PRINT 'EXECUTE [dim].[Update_Parent]';

--EXECUTE [dim].[Update_Parent] 'dim', 'Company_Parent', 'FactorSetId', 'CompanyId', 'ParentId', 'SortKey', 'Hierarchy';
--EXECUTE [dim].[Merge_Bridge] 'dim', 'Company_Parent', 'dim', 'Company_Bridge', 'FactorSetId', 'CompanyId', 'SortKey', 'Hierarchy', 'Operator';

--PRINT 'INSERT INTO [cons].[SubscriptionsCompanies]';

--INSERT INTO [cons].[SubscriptionsCompanies]([CompanyID], [CalDateKey], [StudyID], [SubscriberCompanyName], [SubscriberCompanyDetail])
--VALUES(@CompanyId, @CalDateKey, @StudyID, @CompanyName, @CompanyDetail);

--PRINT 'INSERT INTO [cons].[Assets]';

--INSERT INTO [cons].[Assets]([AssetIDPri], [AssetName], [AssetDetail], [CountryID], [StateName])
--VALUES(@AssetIDPri, @AssetName, @AssetDetail, @AssetCountry, @AssetState);

--PRINT 'INSERT INTO [cons].[AssetsParents]';

--INSERT INTO [cons].[AssetsParents]([CalDateKey], [AssetID], [Parent], [Operator], [Hierarchy])
--VALUES(@CalDateKey, @AssetIDPri, @AssetIDPri, '+', '/');

--PRINT 'INSERT INTO [cons].[AssetsStudies]';

--INSERT INTO [cons].[AssetsStudies]([StudyID], [AssetID])
--VALUES(@StudyID, @AssetIDPri);

--PRINT 'INSERT INTO [cons].[SubscriptionsAssets]';

--INSERT INTO [cons].[SubscriptionsAssets]([CompanyId], [CalDateKey], [StudyId], [AssetId], [ScenarioName], [SubscriberAssetName], [SubscriberAssetDetail])
--VALUES(@CompanyId, @CalDateKey, @StudyID, @AssetIDPri, 'Basis', @AssetName, @AssetDetail);

--PRINT 'INSERT INTO [cons].[TSortSolomon]';

--INSERT INTO [cons].[TSortSolomon]([Refnum], [CalDateKey], [FactorSetId])
--VALUES(@FactorSetId + @StudyID + @AssetIDPri, @CalDateKey, @FactorSetId);

--SELECT * FROM [dim].[Company_LookUp] [l] WHERE [l].[CompanyId] = @CompanyId;
--SELECT * FROM [dim].[Company_Parent] [p] WHERE [p].[CompanyId] = @CompanyId;
--SELECT * FROM [dim].[Company_Bridge] [b] WHERE [b].[CompanyId] = @CompanyId;
--SELECT * FROM [cons].[SubscriptionsCompanies] [c] WHERE [c].[CompanyId] = @CompanyId;
--SELECT * FROM [cons].[Assets] [a] WHERE [a].[AssetIdPri] = @AssetIDPri;
--SELECT * FROM [cons].[AssetsParents] [p] WHERE [p].[AssetId] = @AssetIDPri;
--SELECT * FROM [cons].[AssetsStudies] [s] WHERE [s].[AssetId] = @AssetIDPri AND [s].[StudyId] = @StudyID;
--SELECT * FROM [cons].[SubscriptionsAssets] [s] WHERE [s].[AssetId] = @AssetIDPri AND [s].[CompanyId] = @CompanyId AND [s].[CalDateKey] = @CalDateKey AND [s].[StudyId] = @StudyID;
--SELECT * FROM [cons].[TSortSolomon] [t] WHERE [t].[Refnum] = @FactorSetId + @StudyID + @AssetIDPri;


--SELECT * FROm [dbo].[TSort] [t] where [t].[Refnum] = @FactorSetId + @StudyID + @AssetIDPri OR [t].[Refnum] = '2013PCH998';


EXECUTE [dbo].[spCalcs] @Refnum
