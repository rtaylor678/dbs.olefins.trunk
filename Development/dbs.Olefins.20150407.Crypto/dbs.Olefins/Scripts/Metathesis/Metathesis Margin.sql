﻿/*

EXECUTE [dbo].[spCalcs] '2013PCH033';
EXECUTE [dbo].[spCalcs] '2013PCH144';
EXECUTE [dbo].[spCalcs] '2013PCH157';
EXECUTE [dbo].[spCalcs] '2013PCH195';
EXECUTE [dbo].[spCalcs] '2013PCH198';

EXECUTE [dim].[Update_Parent] 'dim', 'MarginMeta_ProdOlefins_Parent', 'FactorSetId', 'MarginId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'MarginMeta_ProdOlefins_Parent', 'dim', 'MarginMeta_ProdOlefins_Bridge', 'FactorSetId', 'MarginId', 'SortKey', 'Hierarchy', 'Operator';


DECLARE @Meta	TABLE
(
	[MarginId]		VARCHAR(42)		NOT	NULL	CHECK([MarginId] <> ''),
	[ParentId]		VARCHAR(42)		NOT	NULL	CHECK([ParentId] <> ''),
	[Operator]		CHAR(1)			NOT	NULL	CHECK([Operator] <> ''),
	[SortKey]		INT				NOT	NULL,
	[MarginName]	VARCHAR(84)		NOT	NULL	CHECK([MarginName] <> ''),
	[MarginDetail]	VARCHAR(256)	NOT	NULL	CHECK([MarginDetail] <> ''),

	PRIMARY KEY CLUSTERED([MarginId] ASC)
);

INSERT INTO @Meta([MarginId], [ParentId], [Operator], [SortKey], [MarginName], [MarginDetail])
SELECT
	[t].[MarginId],
	[t].[ParentId],
	[t].[Operator],
	[t].[SortKey],
	[t].[MarginName],
	[t].[MarginDetail]
FROM (VALUES
	('MetaOCEthylene', 'EthylenePG', '+', 1, 'Meta OC Ethylene', 'Meta Ethylene from Olefin Cracker'),
	('MetaOCButanes', 'C4Oth', '+', 1, 'Meta OC Butanes', 'Meta Butanes from Olefin Cracker'),
	('MetaOCButene1', 'C4Oth', '+', 1, 'Meta OC Butene-1', 'Meta Butene-1 from Olefin Cracker'),
	('MetaOCButene2', 'C4Oth', '+', 1, 'Meta OC Butene-2', 'Meta Butene-2 from Olefin Cracker'),
	('MetaOCOther', 'IsoButylene', '+', 1, 'Meta OC Other', 'Meta Other feeds from Olefin Cracker'),

	('MetaPurchEthylene', 'ConcEthylene', '+', 1, 'Purch Ethylene', 'Meta Purchased Ethylene'),
	('MetaPurchButane', 'DilButane', '+', 1, 'Meta Purch Butane', 'Meta Purchased Butane'),
	('MetaPurchButene1', 'DilButylene', '+', 1, 'Meta Purch Butene-1', 'Meta Purchased Butene-1'),
	('MetaPurchButene2', 'DilButylene', '+', 1, 'Meta Purch Butene-2', 'Meta Purchased Butene-2'),
	('MetaPurchOther', 'DilButylene', '+', 1, 'Meta Purch Other', 'Meta Purchased, Other Feeds'),

	('MetaProdPropylenePG', 'PropylenePG', '-', 1, 'Meta Prod Polymer-Grade Propylene', 'Meta Product Polymer-Grade Propylene'),
	('MetaProdUnReactLightPurge', 'PropaneC3Resid', '-', 1, 'Meta Prod Unracted Light Purge', 'Meta Product Unracted Light Purge'),
	('MetaProdUnReactButane', 'C4Oth', '-', 1, 'Meta Prod Unracted Butanes', 'Meta Product Unracted Butanes'),
	('MetaProdC5', 'PyroGasoline', '-', 1, 'Meta Prod C5+', 'Meta Product C5+')

	) [t]([MarginId], [ParentId], [Operator], [SortKey], [MarginName], [MarginDetail]);
	
INSERT INTO [dim].[MarginMeta_Basis_Parent]([FactorSetId], [MarginId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	'2013',
	[m].[MarginId],
	[m].[ParentId],
	[m].[Operator],
	[m].[SortKey],
	'/'
FROM @Meta	[m];

INSERT INTO [dim].[MarginMeta_C2H4_Parent]([FactorSetId], [MarginId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	'2013',
	[m].[MarginId],
	[m].[ParentId],
	[m].[Operator],
	[m].[SortKey],
	'/'
FROM @Meta	[m];

INSERT INTO [dim].[MarginMeta_C3H6_Parent]([FactorSetId], [MarginId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	'2013',
	[m].[MarginId],
	[m].[ParentId],
	[m].[Operator],
	[m].[SortKey],
	'/'
FROM @Meta	[m];

INSERT INTO [dim].[MarginMeta_ProdHvc_Parent]([FactorSetId], [MarginId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	'2013',
	[m].[MarginId],
	[m].[ParentId],
	[m].[Operator],
	[m].[SortKey],
	'/'
FROM @Meta	[m];

INSERT INTO [dim].[MarginMeta_ProdOlefins_Parent]([FactorSetId], [MarginId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	'2013',
	[m].[MarginId],
	[m].[ParentId],
	[m].[Operator],
	[m].[SortKey],
	'/'
FROM @Meta	[m];

SELECT * FROM [dim].[Stream_Meta_Parent]	[p] WHERE [p].[FactorSetId] = '2013' AND [StreamId] = 'MetaOCEthylene' ORDER BY [Hierarchy]
SELECT * FROM [dim].[MarginMeta_Basis_Parent][p] WHERE [p].[FactorSetId] = '2013'AND [MarginId] = 'MetaOCEthylene' ORDER BY [Hierarchy]

EXECUTE [dim].[Update_Parent] 'dim', 'MarginMeta_Basis_Parent', 'FactorSetId', 'MarginId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'MarginMeta_Basis_Parent', 'dim', 'MarginMeta_Basis_Bridge', 'FactorSetId', 'MarginId', 'SortKey', 'Hierarchy', 'Operator';
*/

UPDATE [dim].[MarginMeta_C2H4_Parent] SET [operator] = '+' WHERE [MarginId] IN ('MetaOCOther', 'MetaOCButanes', 'MetaOCButene1', 'MetaOCButene2');
UPDATE [dim].[MarginMeta_C3H6_Parent] SET [operator] = '+' WHERE [MarginId] IN ('MetaOCOther', 'MetaOCButanes', 'MetaOCButene1', 'MetaOCButene2');
UPDATE [dim].[MarginMeta_ProdHvc_Parent] SET [operator] = '+' WHERE [MarginId] IN ('MetaOCOther', 'MetaOCButanes', 'MetaOCButene1', 'MetaOCButene2');
UPDATE [dim].[MarginMeta_ProdOlefins_Parent] SET [operator] = '+' WHERE [MarginId] IN ('MetaOCOther', 'MetaOCButanes', 'MetaOCButene1', 'MetaOCButene2');


UPDATE [dim].[Stream_Meta_Parent] SET [ParentId] = 'DilButylene' WHERE [StreamId] = 'MetaPurchButene2';

EXECUTE [dim].[Update_Parent] 'dim', 'Stream_Meta_Parent', 'FactorSetId', 'StreamId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'Stream_Meta_Parent', 'dim', 'Stream_Meta_Bridge', 'FactorSetId', 'StreamId', 'SortKey', 'Hierarchy', 'Operator';

UPDATE [dim].[MarginMeta_Basis_Parent] SET [ParentId] = 'DilButylene' WHERE [MarginId] = 'MetaPurchButene2';
UPDATE [dim].[MarginMeta_C2H4_Parent] SET [ParentId] = 'DilButylene' WHERE [MarginId] = 'MetaPurchButene2';
UPDATE [dim].[MarginMeta_C3H6_Parent] SET [ParentId] = 'DilButylene' WHERE [MarginId] = 'MetaPurchButene2';
UPDATE [dim].[MarginMeta_ProdHvc_Parent] SET [ParentId] = 'DilButylene' WHERE [MarginId] = 'MetaPurchButene2';
UPDATE [dim].[MarginMeta_ProdOlefins_Parent] SET [ParentId] = 'DilButylene' WHERE [MarginId] = 'MetaPurchButene2';

EXECUTE [dim].[Update_Parent] 'dim', 'MarginMeta_Basis_Parent', 'FactorSetId', 'MarginId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'MarginMeta_Basis_Parent', 'dim', 'MarginMeta_Basis_Bridge', 'FactorSetId', 'MarginId', 'SortKey', 'Hierarchy', 'Operator';

EXECUTE [dim].[Update_Parent] 'dim', 'MarginMeta_C2H4_Parent', 'FactorSetId', 'MarginId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'MarginMeta_C2H4_Parent', 'dim', 'MarginMeta_C2H4_Bridge', 'FactorSetId', 'MarginId', 'SortKey', 'Hierarchy', 'Operator';

EXECUTE [dim].[Update_Parent] 'dim', 'MarginMeta_C3H6_Parent', 'FactorSetId', 'MarginId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'MarginMeta_C3H6_Parent', 'dim', 'MarginMeta_C3H6_Bridge', 'FactorSetId', 'MarginId', 'SortKey', 'Hierarchy', 'Operator';

EXECUTE [dim].[Update_Parent] 'dim', 'MarginMeta_ProdHvc_Parent', 'FactorSetId', 'MarginId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'MarginMeta_ProdHvc_Parent', 'dim', 'MarginMeta_ProdHvc_Bridge', 'FactorSetId', 'MarginId', 'SortKey', 'Hierarchy', 'Operator';

EXECUTE [dim].[Update_Parent] 'dim', 'MarginMeta_ProdOlefins_Parent', 'FactorSetId', 'MarginId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE [dim].[Merge_Bridge] 'dim', 'MarginMeta_ProdOlefins_Parent', 'dim', 'MarginMeta_ProdOlefins_Bridge', 'FactorSetId', 'MarginId', 'SortKey', 'Hierarchy', 'Operator';


EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH033';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH144';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH157';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH195';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH198';


SELECT
	REPLICATE(' ', POWER([p].[Hierarchy].[GetLevel]() - 1, 2)) + [p].[MarginId],
	[p].[MarginId],
	[p].[Operator],
	[mma].[Amount_Cur]
FROM
	[dim].[MarginMeta_Basis_Parent] [p]
INNER JOIN
	[calc].[MarginMetaAggregate]	[mma]
		ON	[mma].[FactorSetId]		= [p].[FactorSetId]
		AND	[mma].[MarginId]		= [p].[MarginId]
WHERE	[p].[FactorSetId]		= '2013'
	AND	[mma].[MarginAnalysis]	= 'None'
	AND	[mma].[Refnum]			= '2013PCH157'
ORDER BY
	[p].[Hierarchy]	ASC;
