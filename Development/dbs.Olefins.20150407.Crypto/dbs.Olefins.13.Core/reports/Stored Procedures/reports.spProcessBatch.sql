﻿


Create   PROC reports.[spProcessBatch](@Batch varchar(10))
AS
DECLARE @GroupId varchar(25)
DECLARE cReports CURSOR LOCAL FAST_FORWARD
FOR	SELECT GroupId FROM reports.ReportGroups WHERE Batch = @Batch
OPEN cReports
FETCH NEXT FROM cReports INTO @GroupId
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC reports.spALL @GroupId
	FETCH NEXT FROM cReports INTO @GroupId
END
CLOSE cReports
DEALLOCATE cReports
