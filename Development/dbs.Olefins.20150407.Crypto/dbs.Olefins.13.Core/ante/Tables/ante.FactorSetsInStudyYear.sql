﻿CREATE TABLE [ante].[FactorSetsInStudyYear] (
    [CalDateKey]        INT                NOT NULL,
    [_StudyYear]        AS                 (CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0))) PERSISTED NOT NULL,
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [BaseFactorSet_Bit] BIT                CONSTRAINT [DF_FactorSetsInStudyYear_BaseFactorSet_Bit] DEFAULT ((0)) NOT NULL,
    [Calculate_Bit]     BIT                CONSTRAINT [DF_FactorSetsInStudyYear_Calculate_Bit] DEFAULT ((1)) NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_FactorSetsInStudyYear_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_FactorSetsInStudyYear_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_FactorSetsInStudyYear_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_FactorSetsInStudyYear_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FactorSetsInStudyYear] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [_StudyYear] ASC),
    CONSTRAINT [FK_FactorSetsInStudyYear_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FactorSetsInStudyYear_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER ante.t_FactorSetsInStudyYear_u
	ON [ante].[FactorSetsInStudyYear]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[FactorSetsInStudyYear]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[FactorSetsInStudyYear].CalDateKey	= INSERTED.CalDateKey
		AND	[ante].[FactorSetsInStudyYear].FactorSetId	= INSERTED.FactorSetId;

END;