﻿CREATE PROCEDURE [sim].[Delete_SimulationData]
(
	@Refnum			VARCHAR(25)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM sim.EnergyConsumptionPlant
	WHERE	Refnum			= @Refnum;

	DELETE FROM sim.YieldCompositionPlant
	WHERE	Refnum			= @Refnum;

	DELETE FROM sim.EnergyConsumptionStream
	WHERE	Refnum			= @Refnum;

	DELETE FROM sim.YieldCompositionStream
	WHERE	Refnum			= @Refnum;

END;
