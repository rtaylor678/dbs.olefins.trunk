﻿CREATE PROCEDURE [calc].[Insert_Edc_Utilities]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			k.EdcId,
			k.EdcId,
			k.Value * f.Rate_kLbHr / 1000.0			[kEdc]
		FROM @fpl										tsq
		INNER JOIN	fact.FacilitiesPressure				f
			ON	f.Refnum		= tsq.Refnum
			AND	f.CalDateKey	= tsq.Plant_QtrDateKey
		INNER JOIN ante.EdcCoefficients					k
			ON	k.FactorSetId	= tsq.FactorSetId
			AND	k.EdcId			= f.FacilityId
		WHERE	f.Rate_kLbHr	IS NOT NULL
			AND	tsq.[FactorSet_AnnDateKey] < 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;