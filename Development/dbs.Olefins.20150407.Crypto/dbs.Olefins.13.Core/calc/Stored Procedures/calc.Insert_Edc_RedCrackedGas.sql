﻿CREATE PROCEDURE [calc].[Insert_Edc_RedCrackedGas]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey]	[CalDateKey],
			'RedCrackedGasTrans',
			'RedCrackedGasTrans',
			(-1.0) * SUM(cgq.[Component_kMT]) / u.[_UtilizationStream_Pcnt] * (std.[Value] - comp.[Value]) / 10.0
		FROM @fpl											fpl
		INNER JOIN [calc].[CompositionStream]				cgq
			ON	cgq.[FactorSetId]	= fpl.[FactorSetId]
			AND	cgq.[Refnum]		= fpl.[Refnum]
			AND	cgq.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	cgq.[StreamId]		= 'ProdOther'
			AND	cgq.[ComponentId]	IN ('C2H4', 'C3H6')
			AND	cgq.[Component_kMT]	IS NOT NULL
		INNER JOIN [calc].[CapacityUtilization]				u
			ON	u.[FactorSetId]		= fpl.[FactorSetId]
			AND	u.[Refnum]			= fpl.[Refnum]
			AND	u.[CalDateKey]		= fpl.Plant_AnnDateKey
			AND	u.[SchedId]			= 'P'
			AND	u.[StreamId]		= 'ProdOlefins'
		INNER JOIN [calc].[PeerGroupFeedClass]				fc
			ON	fc.[FactorSetId]	= fpl.[FactorSetId]
			AND	fc.[Refnum]			= fpl.[Refnum]
		INNER JOIN (VALUES
			('1',	'Ethane'),
			('2',	'LPG'),
			('3',	'Liquid'),
			('4',	'Liquid')
			)												map ([PeerGroup], [EdcId])
			ON	map.[PeerGroup] = fc.[PeerGroup]
		INNER JOIN [ante].[EdcCoefficients]					std
			ON	std.[FactorSetId]	= fpl.[FactorSetId]
			AND	std.[EdcId]			= map.[EdcId]
		INNER JOIN [ante].[EdcCoefficients]					comp
			ON	comp.[FactorSetId]	= fpl.[FactorSetId]
			AND	comp.[EdcId]		= 'Comp' + map.[EdcId]
		WHERE	fpl.[FactorSet_QtrDateKey]			> 20130000
			AND	u.[_UtilizationStream_Pcnt]	<> 0.0
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			u.[_UtilizationStream_Pcnt],
			std.[Value],
			comp.[Value]
		HAVING
			SUM(cgq.[Component_kMT])	IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;