﻿CREATE TABLE [calc].[MaintCostRoutine] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [FacilityId]        VARCHAR (42)       NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [MaintMaterial_Cur] REAL               NULL,
    [MaintLabor_Cur]    REAL               NULL,
    [_Maint_Cur]        AS                 (CONVERT([real],coalesce([MaintMaterial_Cur],(0.0))+coalesce([MaintLabor_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_MaintCostRoutine_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_MaintCostRoutine_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_MaintCostRoutine_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_MaintCostRoutine_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MaintCostRoutine] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MaintCostRoutine_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MaintCostRoutine_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_MaintCostRoutine_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_MaintCostRoutine_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MaintCostRoutine_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [calc].[t_MaintCostRoutine_u]
	ON [calc].[MaintCostRoutine]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[MaintCostRoutine]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[calc].[MaintCostRoutine].FactorSetId		= INSERTED.FactorSetId
		AND	[calc].[MaintCostRoutine].Refnum		= INSERTED.Refnum
		AND [calc].[MaintCostRoutine].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [calc].[MaintCostRoutine].FacilityId	= INSERTED.FacilityId
		AND [calc].[MaintCostRoutine].CalDateKey	= INSERTED.CalDateKey;

END;