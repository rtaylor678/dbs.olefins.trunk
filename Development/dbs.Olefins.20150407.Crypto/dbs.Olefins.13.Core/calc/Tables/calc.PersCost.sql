﻿CREATE TABLE [calc].[PersCost] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [AccountId]            VARCHAR (42)       NOT NULL,
    [CurrencyRpt]          VARCHAR (4)        NOT NULL,
    [TaLabor_Cur]          REAL               NULL,
    [Amount_Cur]           REAL               NULL,
    [_Ann_Amount_Cur]      AS                 (CONVERT([real],case when [AccountId]='ContractServOth' OR [AccountId]='ContractTechSvc' then (0.8)*[Amount_Cur] else [Amount_Cur] end,(1))) PERSISTED,
    [_TaAdjAnn_Amount_Cur] AS                 (CONVERT([real],isnull([TaLabor_Cur],case when [AccountId]='ContractServOth' OR [AccountId]='ContractTechSvc' then (0.8)*[Amount_Cur] else [Amount_Cur] end),(1))) PERSISTED,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_PersCost_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_PersCost_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_PersCost_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_PersCost_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PersCost] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PersCost_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_calc_PersCost_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PersCost_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_PersCost_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PersCost_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_PersCost_u
	ON  calc.PersCost
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.PersCost
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PersCost.FactorSetId	= INSERTED.FactorSetId
		AND calc.PersCost.Refnum		= INSERTED.Refnum
		AND calc.PersCost.CalDateKey	= INSERTED.CalDateKey
		AND calc.PersCost.AccountId		= INSERTED.AccountId;
	
END