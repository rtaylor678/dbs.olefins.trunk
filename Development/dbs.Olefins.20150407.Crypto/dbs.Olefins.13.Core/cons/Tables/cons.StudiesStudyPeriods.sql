﻿CREATE TABLE [cons].[StudiesStudyPeriods] (
    [CalDateKey]     INT                NOT NULL,
    [StudyId]        VARCHAR (4)        NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_StudiesStudyPeriods_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_StudiesStudyPeriods_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_StudiesStudyPeriods_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_StudiesStudyPeriods_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StudiesStudyPeriods] PRIMARY KEY CLUSTERED ([CalDateKey] ASC, [StudyId] ASC),
    CONSTRAINT [FK_StudiesStudyPeriods_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_StudiesStudyPeriods_Study_LookUp] FOREIGN KEY ([StudyId]) REFERENCES [dim].[Study_LookUp] ([StudyId]),
    CONSTRAINT [FK_StudiesStudyPeriods_StudyPeriods_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[StudyPeriods_LookUp] ([CalDateKey])
);


GO


CREATE TRIGGER [cons].[t_StudiesStudyPeriods_u]
	ON [cons].[StudiesStudyPeriods]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[StudiesStudyPeriods]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[StudiesStudyPeriods].CalDateKey		= INSERTED.CalDateKey
		AND	[cons].[StudiesStudyPeriods].StudyId		= INSERTED.StudyId;

END;