﻿CREATE TABLE [Val].[Notes] (
    [Refnum]         CHAR (9)           NOT NULL,
    [NoteType]       CHAR (20)          NULL,
    [Note]           NVARCHAR (MAX)     NULL,
    [UpdatedBy]      CHAR (10)          NULL,
    [Updated]        DATETIME           NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Notes_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Notes_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Notes_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Notes_tsModifiedApp] DEFAULT (app_name()) NOT NULL
);


GO
CREATE TRIGGER [Val].[t_Notes_u]
	ON [Val].[Notes]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [Val].[Notes]
	SET	tsModified		= sysdatetimeoffset(),
		tsModifiedHost	= host_name(),
		tsModifiedUser	= suser_sname(),
		tsModifiedApp	= app_name()
	FROM INSERTED
	WHERE	[Val].[Notes].[Refnum]		= INSERTED.[Refnum]
		AND	[Val].[Notes].[NoteType]	= INSERTED.[NoteType]
		AND	[Val].[Notes].[Updated]		= INSERTED.[Updated];
END;
