﻿CREATE PROCEDURE [Console].[GetRefNums]
	@StudyYear smallint,
	@Study varchar(3)
AS
BEGIN
	SELECT substring(Refnum,3,8) as refnum, CoLoc FROM dbo.TSort
	WHERE StudyYear = @StudyYear 
	ORDER BY RefNum ASC
END
