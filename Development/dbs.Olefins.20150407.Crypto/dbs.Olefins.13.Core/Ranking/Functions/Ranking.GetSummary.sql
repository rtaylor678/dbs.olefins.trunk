﻿CREATE FUNCTION [Ranking].[GetSummary](@RankedData [Ranking].[RankedData] READONLY, @MinRangeValue real, @MaxRangeValue real)
RETURNS @Summary TABLE (NumTiles tinyint NULL, DataCount int NULL, Top2 real NULL, Tile1Break real NULL, Tile2Break real NULL, Tile3Break real NULL, Btm2 real NULL)
AS
BEGIN
DECLARE @MinRank int, @MaxRank int, @MaxRank1 int, @MaxRank2 int, @MaxRank3 int
DECLARE @Tiles tinyint, @DataCount int, @Top2 real, @Tile1Break real, @Tile2Break real, @Tile3Break real, @Btm2 real

SELECT @Tiles = MAX(Tile), @DataCount = COUNT(*) FROM @RankedData 

SELECT @MinRank = MIN(Rank), @MaxRank = MAX(Rank)
FROM @RankedData 
WHERE (Value < @MaxRangeValue OR @MaxRangeValue IS NULL)
AND (Value > @MinRangeValue OR @MinRangeValue IS NULL)

	
IF @Tiles > 1
	SELECT @MaxRank1 = MAX(Rank) FROM @RankedData WHERE Tile = 1
IF @Tiles > 2
	SELECT @MaxRank2 = MAX(Rank) FROM @RankedData WHERE Tile = 2
IF @Tiles > 3
	SELECT @MaxRank3 = MAX(Rank) FROM @RankedData WHERE Tile = 3

IF @MinRank IS NOT NULL
	SELECT @Top2 = AVG(Value) FROM @RankedData WHERE Rank BETWEEN @MinRank AND @MinRank + 1
IF @MaxRank1 IS NOT NULL
	SELECT @Tile1Break = AVG(Value) FROM @RankedData WHERE Rank BETWEEN @MaxRank1 AND @MaxRank1 + 1
IF @MaxRank2 IS NOT NULL
	SELECT @Tile2Break = AVG(Value) FROM @RankedData WHERE Rank BETWEEN @MaxRank2 AND @MaxRank2 + 1
IF @MaxRank3 IS NOT NULL
	SELECT @Tile3Break = AVG(Value) FROM @RankedData WHERE Rank BETWEEN @MaxRank3 AND @MaxRank3 + 1
IF @MaxRank IS NOT NULL
	SELECT @Btm2 = AVG(Value) FROM @RankedData WHERE Rank BETWEEN @MaxRank - 1 AND @MaxRank

INSERT @Summary (NumTiles, DataCount, Top2, Tile1Break, Tile2Break, Tile3Break, Btm2)
VALUES (@Tiles, @DataCount, @Top2, @Tile1Break, @Tile2Break, @Tile3Break, @Btm2)

	RETURN
	
END