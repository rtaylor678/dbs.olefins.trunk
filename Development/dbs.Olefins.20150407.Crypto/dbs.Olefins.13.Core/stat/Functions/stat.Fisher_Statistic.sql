﻿CREATE FUNCTION [stat].[Fisher_Statistic]
(@DegreesOfFreedomNumerator FLOAT (53), @DegreesOfFreedomDenominator FLOAT (53), @Probability FLOAT (53))
RETURNS FLOAT (53)
AS
 EXTERNAL NAME [Sa.Meta.Numerics].[UserDefinedFunctions].[Fisher_Statistic]

