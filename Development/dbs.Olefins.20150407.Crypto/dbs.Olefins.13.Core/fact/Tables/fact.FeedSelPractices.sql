﻿CREATE TABLE [fact].[FeedSelPractices] (
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [StreamId]            VARCHAR (42)       NOT NULL,
    [Mix_Bit]             BIT                NULL,
    [Blend_Bit]           BIT                NULL,
    [Fractionation_Bit]   BIT                NULL,
    [HydroTreat_Bit]      BIT                NULL,
    [Purification_Bit]    BIT                NULL,
    [MinConsidDensity_SG] REAL               NULL,
    [MaxConsidDensity_SG] REAL               NULL,
    [MinActualDensity_SG] REAL               NULL,
    [MaxActualDensity_SG] REAL               NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelPractices_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_FeedSelPractices_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_FeedSelPractices_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_FeedSelPractices_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSelPractices] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FeedSelPractices_MaxActualDensity_SG] CHECK ([MaxActualDensity_SG]>=(0.0)),
    CONSTRAINT [CR_FeedSelPractices_MaxConsidDensity_SG] CHECK ([MaxConsidDensity_SG]>=(0.0)),
    CONSTRAINT [CR_FeedSelPractices_MinActualDensity_SG] CHECK ([MinActualDensity_SG]>=(0.0)),
    CONSTRAINT [CR_FeedSelPractices_MinConsidDensity_SG] CHECK ([MinConsidDensity_SG]>=(0.0)),
    CONSTRAINT [FK_FeedSelPractices_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSelPractices_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FeedSelPractices_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSelPractices_u]
	ON [fact].[FeedSelPractices]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSelPractices]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSelPractices].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSelPractices].StreamId		= INSERTED.StreamId
		AND [fact].[FeedSelPractices].CalDateKey	= INSERTED.CalDateKey;

END;