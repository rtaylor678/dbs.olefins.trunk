﻿CREATE PROCEDURE [fact].[Insert_EnergyCogen]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.EnergyCogen(Refnum, CalDateKey, HasCogen_Bit, Thermal_Pcnt)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvBit(e.YN)										[HasCogen]
			, e.ThermalPct * 100.0									[ThermalPcnt]
		FROM stgFact.EnergyQnty e
		INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
		WHERE (e.YN IS NOT NULL OR e.ThermalPct <> 0.0)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;