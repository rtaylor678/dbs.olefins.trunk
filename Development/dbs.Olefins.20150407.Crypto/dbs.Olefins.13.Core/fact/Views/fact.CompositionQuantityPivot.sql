﻿CREATE VIEW fact.CompositionQuantityPivot
WITH SCHEMABINDING
AS
SELECT
	t.Refnum,
	t.StreamId,
	t.StreamDescription,
	t.CH4,
	t.C2H6,
	t.C2H4,
	t.C3H8,
	t.C3H6,
	t.NBUTA,
	t.IBUTA,
	t.IB,
	t.B1,
	t.C4H6,
	t.NC5,
	t.IC5,
	t.NC6,
	t.C6ISO,
	t.C7H16,		--	20121203	NC7
	t.C8H18,		--	20121203	NC8
	t.H2,
	t.S,
	t.P,
	t.I,
	t.A,
	t.N,
	t.O
FROM(
	SELECT
		c.Refnum,
		c.StreamId,
		c.StreamDescription,
		c.ComponentId,
		c.Component_WtPcnt
	FROM fact.CompositionQuantity				c
	WHERE c.Component_WtPcnt IS NOT NULL
	) u
	PIVOT (
	MAX(u.Component_WtPcnt) FOR u.ComponentId IN (
		CH4,
		C2H6,
		C2H4,
		C3H8,
		C3H6,
		NBUTA,
		IBUTA,
		IB,
		B1,
		C4H6,
		NC5,
		IC5,
		NC6,
		C6ISO,
		C7H16,	--	20121203	NC7
		C8H18,	--	20121203	NC8
		H2,
		S,
		P,
		I,
		A,
		N,
		O
		)
	) t;