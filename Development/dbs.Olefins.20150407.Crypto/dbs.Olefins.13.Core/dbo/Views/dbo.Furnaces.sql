﻿Create VIEW dbo.Furnaces as
select t.Refnum, FurnNo,	FurnFeed,	DTDecoke,	DTTLEClean,	DTMinor,	DTMajor,	STDT,	DTStandby,	TotTimeOff,	TADownDays,	OthDownDays,	
FurnAvail,	AdjFurnOnstream,	FeedQtyKMTA,	FeedCapMTD,	OperDays,	ActCap,	AdjFurnUtil,	FurnSlowD,	YearRetubed,	RetubeCost,	
RetubeCostMatl,	RetubeCostLabor,	OtherMajorCost,	TotCost,	RetubeInterval,	OnStreamDays,	FuelCons,	FuelType,	StackOxy,	ArchDraft,	
StackTemp,	CrossTemp,	FurnFeedBreak,	AnnRetubeCostMatlMT,	AnnRetubeCostLaborMT,	AnnRetubeCostMT,	OtherMajorCostMT,	TotCostMT,	
FuelTypeYN,	FuelTypeFO,	FuelTypeFG
from stgFact.FurnRely f JOIN TSort t on t.SmallRefnum=f.Refnum