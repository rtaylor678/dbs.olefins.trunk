﻿CREATE TABLE [dbo].[YieldOther] (
    [Refnum]        VARCHAR (25)  NOT NULL,
    [DataType]      VARCHAR (9)   NOT NULL,
    [FeedLtOther]   VARCHAR (100) NULL,
    [FeedLiqOther1] VARCHAR (100) NULL,
    [FeedLiqOther2] VARCHAR (100) NULL,
    [FeedLiqOther3] VARCHAR (100) NULL,
    [ProdOther1]    VARCHAR (100) NULL,
    [ProdOther2]    VARCHAR (100) NULL,
    CONSTRAINT [PK_YieldOther] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC) WITH (FILLFACTOR = 70)
);

