﻿CREATE TABLE [dim].[ReliabilityPredMaintCause_LookUp] (
    [PredMaintCauseId]     VARCHAR (42)       NOT NULL,
    [PredMaintCauseName]   NVARCHAR (84)      NOT NULL,
    [PredMaintCauseDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityPredMaintCause_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPredMaintCause_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPredMaintCause_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPredMaintCause_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReliabilityPredMaintCause_LookUp] PRIMARY KEY CLUSTERED ([PredMaintCauseId] ASC),
    CONSTRAINT [CL_ReliabilityPredMaintCause_LookUp_PredMaintCauseDetail] CHECK ([PredMaintCauseDetail]<>''),
    CONSTRAINT [CL_ReliabilityPredMaintCause_LookUp_PredMaintCauseId] CHECK ([PredMaintCauseId]<>''),
    CONSTRAINT [CL_ReliabilityPredMaintCause_LookUp_PredMaintCauseName] CHECK ([PredMaintCauseName]<>''),
    CONSTRAINT [UK_ReliabilityPredMaintCause_LookUp_PredMaintCauseDetail] UNIQUE NONCLUSTERED ([PredMaintCauseDetail] ASC),
    CONSTRAINT [UK_ReliabilityPredMaintCause_LookUp_PredMaintCauseName] UNIQUE NONCLUSTERED ([PredMaintCauseName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReliabilityPredMaintCause_LookUp_u]
	ON [dim].[ReliabilityPredMaintCause_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReliabilityPredMaintCause_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityPredMaintCause_LookUp].[PredMaintCauseId]		= INSERTED.[PredMaintCauseId];
		
END;
