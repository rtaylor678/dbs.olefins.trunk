﻿CREATE TABLE [dim].[Efficiency_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [EfficiencyId]   VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Efficiency_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Efficiency_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Efficiency_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Efficiency_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Efficiency_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Efficiency_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EfficiencyId] ASC),
    CONSTRAINT [CR_Efficiency_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Efficiency_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Efficiency_Parent_LookUp_Efficiency] FOREIGN KEY ([EfficiencyId]) REFERENCES [dim].[Efficiency_LookUp] ([EfficiencyId]),
    CONSTRAINT [FK_Efficiency_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Efficiency_LookUp] ([EfficiencyId]),
    CONSTRAINT [FK_Efficiency_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Efficiency_Parent] ([FactorSetId], [EfficiencyId])
);


GO

CREATE TRIGGER [dim].[t_Efficiency_Parent_u]
ON [dim].[Efficiency_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Efficiency_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Efficiency_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Efficiency_Parent].[EfficiencyId]	= INSERTED.[EfficiencyId];

END;
