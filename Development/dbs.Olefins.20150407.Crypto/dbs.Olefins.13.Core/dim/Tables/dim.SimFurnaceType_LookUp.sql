﻿CREATE TABLE [dim].[SimFurnaceType_LookUp] (
    [ModelId]                  VARCHAR (12)       NOT NULL,
    [FurnaceTypeId]            VARCHAR (12)       NOT NULL,
    [FurnaceTypeName]          VARCHAR (84)       NOT NULL,
    [FurnaceTypeDetail]        VARCHAR (256)      NOT NULL,
    [SeverityConversion]       TINYINT            NULL,
    [CoilInletTemp_Min_C]      REAL               NULL,
    [CoilInletTemp_Max_C]      REAL               NULL,
    [CoilOutletPressure_kgcm2] REAL               NULL,
    [CoilInletPressure_kgcm2]  REAL               NULL,
    [CoilOutletTemp_C]         REAL               NULL,
    [CoilInletTemp_C]          REAL               NULL,
    [RadiantWallTemp_C]        REAL               NULL,
    [FlowRate_KgHr]            REAL               NULL,
    [SteamHydrocarbon_Ratio]   REAL               NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_SimFurnaceType_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceType_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceType_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceType_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]             ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SimSimFurnaceType_LookUp] PRIMARY KEY CLUSTERED ([ModelId] ASC, [FurnaceTypeId] ASC),
    CONSTRAINT [CL_SimFurnaceType_LookUp_FurnaceTypeDetail] CHECK (len([FurnaceTypeDetail])>(0)),
    CONSTRAINT [CL_SimFurnaceType_LookUp_FurnaceTypeId] CHECK (len([FurnaceTypeId])>(0)),
    CONSTRAINT [CL_SimFurnaceType_LookUp_FurnaceTypeName] CHECK (len([FurnaceTypeName])>(0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_CoilInletPressure_kgcm2] CHECK ([CoilInletPressure_kgcm2]>(0.0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_CoilInletTemp_C] CHECK ([CoilInletTemp_C]>(0.0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_CoilInletTemp_Max_C] CHECK ([CoilInletTemp_Max_C]>(0.0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_CoilInletTemp_Min_C] CHECK ([CoilInletTemp_Min_C]>(0.0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_CoilOutletPressure_kgcm2] CHECK ([CoilOutletPressure_kgcm2]>(0.0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_CoilOutletTemp_C] CHECK ([CoilOutletTemp_C]>(0.0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_FlowRate_kghr] CHECK ([FlowRate_KgHr]>(0.0)),
    CONSTRAINT [CR_SimFurnaceType_LookUp_RadiantWallTemp_C] CHECK ([RadiantWallTemp_C]>(0.0)),
    CONSTRAINT [CV_SimSimFurnaceType_CoilInletTemp] CHECK ([CoilInletTemp_Min_C]<=[CoilInletTemp_Max_C]),
    CONSTRAINT [FK_SimFurnaceType_LookUp_SimModel_LookUp] FOREIGN KEY ([ModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId])
);


GO

CREATE TRIGGER [dim].[t_SimFurnaceType_LookUp_u]
	ON [dim].[SimFurnaceType_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[SimFurnaceType_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[SimFurnaceType_LookUp].[ModelId]			= INSERTED.[ModelId]
		AND	[dim].[SimFurnaceType_LookUp].[FurnaceTypeId]	= INSERTED.[FurnaceTypeId];
		
END;
