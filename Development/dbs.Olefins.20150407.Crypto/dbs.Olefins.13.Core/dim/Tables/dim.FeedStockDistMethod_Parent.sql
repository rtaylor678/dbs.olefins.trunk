﻿CREATE TABLE [dim].[FeedStockDistMethod_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [DistMethodId]   VARCHAR (8)         NOT NULL,
    [ParentId]       VARCHAR (8)         NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_FeedStockDistMethod_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedStockDistMethod_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistMethod_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistMethod_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistMethod_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedStockDistMethod_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [DistMethodId] ASC),
    CONSTRAINT [CR_FeedStockDistMethod_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_FeedStockDistMethod_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_FeedStockDistMethod_Parent_LookUp_FeedStockDistMethod] FOREIGN KEY ([DistMethodId]) REFERENCES [dim].[FeedStockDistMethod_LookUp] ([DistMethodId]),
    CONSTRAINT [FK_FeedStockDistMethod_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[FeedStockDistMethod_LookUp] ([DistMethodId]),
    CONSTRAINT [FK_FeedStockDistMethod_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[FeedStockDistMethod_Parent] ([FactorSetId], [DistMethodId])
);


GO

CREATE TRIGGER [dim].[t_FeedStockDistMethod_Parent_u]
ON [dim].[FeedStockDistMethod_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedStockDistMethod_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedStockDistMethod_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[FeedStockDistMethod_Parent].[DistMethodId]	= INSERTED.[DistMethodId];

END;