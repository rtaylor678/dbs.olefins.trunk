﻿CREATE TABLE [dim].[Apc_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [ApcId]          VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Apc_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Apc_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Apc_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Apc_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Apc_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Apc_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ApcId] ASC),
    CONSTRAINT [CR_Apc_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Apc_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Apc_Parent_LookUp_Apc] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_Apc_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_Apc_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Apc_Parent] ([FactorSetId], [ApcId])
);


GO

CREATE TRIGGER [dim].[t_Apc_Parent_u]
ON [dim].[Apc_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Apc_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Apc_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Apc_Parent].[ApcId]			= INSERTED.[ApcId];

END;