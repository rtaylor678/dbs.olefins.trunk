﻿CREATE TABLE [dim].[FurnRetubeWork_LookUp] (
    [WorkId]         VARCHAR (12)       NOT NULL,
    [WorkName]       NVARCHAR (84)      NOT NULL,
    [WorkDetail]     NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FurnRetubeWork_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FurnRetubeWork_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FurnRetubeWork_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FurnRetubeWork_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FurnRetubeWork_LookUp] PRIMARY KEY CLUSTERED ([WorkId] ASC),
    CONSTRAINT [CL_FurnRetubeWork_LookUp_DistMethodDetail] CHECK ([WorkDetail]<>''),
    CONSTRAINT [CL_FurnRetubeWork_LookUp_DistMethodID] CHECK ([WorkId]<>''),
    CONSTRAINT [CL_FurnRetubeWork_LookUp_DistMethodName] CHECK ([WorkName]<>''),
    CONSTRAINT [UK_FurnRetubeWork_LookUp_DistMethodDetail] UNIQUE NONCLUSTERED ([WorkDetail] ASC),
    CONSTRAINT [UK_FurnRetubeWork_LookUp_DistMethodName] UNIQUE NONCLUSTERED ([WorkName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FurnRetubeWork_LookUp_u]
	ON [dim].[FurnRetubeWork_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FurnRetubeWork_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FurnRetubeWork_LookUp].[WorkId]		= INSERTED.[WorkId];
		
END;