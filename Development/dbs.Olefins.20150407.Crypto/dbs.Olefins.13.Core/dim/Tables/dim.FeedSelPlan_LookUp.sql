﻿CREATE TABLE [dim].[FeedSelPlan_LookUp] (
    [PlanId]         VARCHAR (42)       NOT NULL,
    [PlanName]       NVARCHAR (84)      NOT NULL,
    [PlanDetail]     NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelPlan_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FeedSelPlan_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FeedSelPlan_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelPlan_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedSelPlan_LookUp] PRIMARY KEY CLUSTERED ([PlanId] ASC),
    CONSTRAINT [CL_FeedSelPlan_LookUp_PlanDetail] CHECK ([PlanDetail]<>''),
    CONSTRAINT [CL_FeedSelPlan_LookUp_PlanId] CHECK ([PlanId]<>''),
    CONSTRAINT [CL_FeedSelPlan_LookUp_PlanName] CHECK ([PlanName]<>''),
    CONSTRAINT [UK_FeedSelPlan_LookUp_PlanDetail] UNIQUE NONCLUSTERED ([PlanDetail] ASC),
    CONSTRAINT [UK_FeedSelPlan_LookUp_PlanName] UNIQUE NONCLUSTERED ([PlanName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FeedSelPlan_LookUp_u]
	ON [dim].[FeedSelPlan_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FeedSelPlan_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedSelPlan_LookUp].[PlanId]	= INSERTED.[PlanId];
		
END;
