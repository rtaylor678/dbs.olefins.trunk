﻿CREATE TABLE [stgFact].[Inventory] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [TankType]       VARCHAR (2)        NOT NULL,
    [KMT]            REAL               NULL,
    [YEIL]           REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Inventory_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Inventory_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Inventory_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Inventory_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Inventory] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TankType] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Inventory_u]
	ON [stgFact].[Inventory]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Inventory]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Inventory].Refnum		= INSERTED.Refnum
		AND	[stgFact].[Inventory].TankType		= INSERTED.TankType;

END;