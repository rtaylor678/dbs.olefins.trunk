﻿CREATE TABLE [stgFact].[UploadLog] (
    [FileType]       VARCHAR (25)       NOT NULL,
    [Refnum]         VARCHAR (18)       NOT NULL,
    [FilePath]       VARCHAR (512)      NOT NULL,
    [TimeBeg]        DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_UploadLog_TimeBeg] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [TimeEnd]        DATETIMEOFFSET (7) NULL,
    [_Duration_mcs]  AS                 (datediff(microsecond,[TimeBeg],[TimeEnd])),
    [Error_Count]    INT                NULL,
    [Msg]            VARCHAR (MAX)      NULL,
    [Archive]        BIT                CONSTRAINT [DF_stgFact_UploadLog_Archive] DEFAULT ((0)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_UploadLog_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_UploadLog_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_UploadLog_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_UploadLog_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_UploadLog] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FileType] ASC, [TimeBeg] ASC, [tsModified] ASC)
);


GO
CREATE TRIGGER [stgFact].[t_UploadLog_u]
	ON [stgFact].[UploadLog]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[UploadLog]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[UploadLog].[FileType]	= INSERTED.[FileType]
		AND	[stgFact].[UploadLog].[Refnum]		= INSERTED.[Refnum]
		AND	[stgFact].[UploadLog].[FilePath]	= INSERTED.[FilePath]
		AND	[stgFact].[UploadLog].[TimeBeg]		= INSERTED.[TimeBeg];

END;