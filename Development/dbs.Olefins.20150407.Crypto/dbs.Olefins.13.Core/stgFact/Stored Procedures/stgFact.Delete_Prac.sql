﻿CREATE PROCEDURE [stgFact].[Delete_Prac]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Prac]
	WHERE [Refnum] = @Refnum;

END;