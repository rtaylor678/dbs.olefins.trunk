﻿CREATE PROCEDURE [stgFact].[Delete_OpExMiscDetail]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[OpExMiscDetail]
	WHERE [Refnum] = @Refnum;

END;