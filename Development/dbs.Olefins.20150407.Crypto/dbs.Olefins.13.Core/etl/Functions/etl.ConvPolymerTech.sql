﻿
CREATE FUNCTION etl.ConvPolymerTech
(
	@PolymerTechID	NVARCHAR(24)
)
RETURNS NVARCHAR(24)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar NVARCHAR(24) = @PolymerTechID;
	--CASE @PolymerTechID
	--	--WHEN 'ARCO POLYMERS'		THEN ''
	--	--WHEN 'ASAHI'				THEN ''
	--	--WHEN 'ATO, France'			THEN ''
	--	--WHEN 'ATO-FRANCE'			THEN ''
	--	WHEN 'Autocave HP'			THEN 'Autoclave'
	--	WHEN 'Autoclave HP'			THEN 'Autoclave'
	--	WHEN 'Autoclaves'			THEN 'Autoclave'
	--	WHEN 'AUTOCLVE HP'			THEN 'Autoclave'
	--	--WHEN 'Basell LuPOTECH G'	THEN ''
	--	--WHEN 'Basell LuPOTECH TS'	THEN ''
	--	--WHEN 'BASELL'				THEN ''
	--	--WHEN 'BESSELS'				THEN ''
	--	--WHEN 'BORSATR'				THEN ''
	--	--WHEN 'BP Chemicals'			THEN ''
	--	--WHEN 'BP'					THEN ''
	--	--WHEN 'BP-Amoco'				THEN ''
	--	--WHEN 'Dow'					THEN ''
	--	--WHEN 'EXXON'				THEN ''
	--	--WHEN 'EXXONMOBIL'			THEN ''
	--	--WHEN 'ExxonMobile'			THEN ''
	--	--WHEN 'FREE RAD POLIMZ'		THEN ''
	--	WHEN 'Gas Phase Bed Unipol'	THEN 'GasPhase'
	--	WHEN 'Gas Phase'			THEN 'GasPhase'
	--	WHEN 'gas-phase'			THEN 'GasPhase'
	--	WHEN 'Gasphase'				THEN 'GasPhase'
	--	--WHEN 'High Pressure'		THEN ''
	--	--WHEN 'HOECHST'				THEN ''
	--	--WHEN 'ICI'					THEN ''
	--	--WHEN 'MITSBISHI'			THEN ''
	--	--WHEN 'Mitsubishi HYPOL'		THEN ''
	--	--WHEN 'Mitsubishi'			THEN ''
	--	--WHEN 'Mitsubishi-BASF'		THEN ''
	--	--WHEN 'MITSUI'				THEN ''
	--	--WHEN 'MITSUIBISI'			THEN ''
	--	--WHEN 'NISSAN'				THEN ''
	--	--WHEN 'NOVACOR'				THEN ''
	--	--WHEN 'NOVOLEN'				THEN ''
	--	--WHEN 'POLIMERI EUROPA'		THEN ''
	--	--WHEN 'Pressure'				THEN ''
	--	WHEN 'Slurry loop'			THEN 'Slurry'
	--	WHEN 'slurry'				THEN 'Slurry'
	--	--WHEN 'Solution'				THEN ''
	--	WHEN 'Spheriphol'			THEN 'Spheripol'
	--	WHEN 'Spheripol'			THEN 'Spheripol'
	--	--WHEN 'SPHERIZONE'			THEN ''
	--	--WHEN 'ST(SEI)'				THEN ''
	--	--WHEN 'SUSPEN POLIMZ'		THEN ''
	--	--WHEN 'Tubular'				THEN ''
	--	--WHEN 'UCC / (Dow)'			THEN ''
	--	--WHEN 'UCC, USA (DOW)'		THEN ''
	--	--WHEN 'Union Carbide'		THEN ''
	--	--WHEN 'UNIPOL'				THEN ''
	--	--WHEN 'UNIVATION'			THEN ''
	--	--WHEN 'Univatiot'			THEN ''
	--	--WHEN 'UOP'					THEN ''
	--	ELSE NULL
	--END;
	
	RETURN @ResultVar;

END;