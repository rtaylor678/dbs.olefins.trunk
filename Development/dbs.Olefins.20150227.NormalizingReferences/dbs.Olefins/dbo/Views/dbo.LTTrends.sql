﻿
CREATE view [dbo].[LTTrends] AS
Select Refnum = LEFT(t.StudyYear, 2) + Rtrim(t.Refnum), 
t.StudyYear,
SourceDB = 'OlefinsLegacy',
RV_MUS = r.TotReplVal,
MaintIndexRV = MaintCostIndex * (CASE WHEN t.StudyYear <=1993 THEN 100 ELSE 1 END),
RelInd = m.ReliabInd * (CASE WHEN t.StudyYear <=1993 THEN 100 ELSE 1 END)
FROM  [$(DbOlefinsLegacy)].dbo.MaintInd m INNER JOIN  [$(DbOlefinsLegacy)].dbo.Replacement r on r.Refnum=m.Refnum
INNER JOIN  [$(DbOlefinsLegacy)].dbo.TSort t on t.Refnum=m.Refnum  
where t.StudyYear < 2007

UNION

Select g.Refnum, 
g.StudyYear, 
SourceDB = 'Olefins',
g.RV_MUS,
g.MaintIndexRV,
g.RelInd
from [$(DatabaseName)].dbo.GENSUM g 
where g.StudyYear >= 2007
