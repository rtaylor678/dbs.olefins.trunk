﻿SELECT * FROM cons.TSortSolomon

UPDATE cons.TSortSolomon
SET
	FactorSetId = '2013',
	RefDirectory = 'K:\Study\Olefins\2013\Plant\13PCH998\'
WHERE Refnum = '2013PCH998'


DECLARE @ListName VARCHAR(25) = '13PCH+Solomon';


EXECUTE spCalcs '2013PCH998'


--INSERT INTO [cons].[RefListLu]([ListId], [ListName], [ListDetail], [BaseList_Bit], [Operator], [Parent], [SortKey], [Hierarchy])
--SELECT
--	@ListName,
--	@ListName,
--	@ListName,
--	[l].[BaseList_Bit],
--	[l].[Operator],
--	[l].[Parent],
--	[l].[SortKey],
--	[l].[Hierarchy]
--FROM [cons].[RefListLu] [l]
--WHERE [l].[ListId] = '13PCH+late';

--INSERT INTO [cons].[RefList]([ListId], [Refnum], [UserGroup])
--SELECT
--	@ListName,
--	[l].[Refnum],
--	0
--FROM [cons].[RefList] [l]
--WHERE [l].[ListId] = '13PCH+late';

--INSERT INTO [cons].[RefList]([ListId], [Refnum], [UserGroup])
--SELECT
--	'13PCH+late',
--	'2013PCH998',
--	0;
