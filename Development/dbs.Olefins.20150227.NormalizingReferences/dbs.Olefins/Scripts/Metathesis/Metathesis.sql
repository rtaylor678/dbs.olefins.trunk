﻿EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH033';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH144';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH157';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH195';
EXECUTE [Olefins].[dbo].[spCalcs] '2013PCH198';



--ETHDiv_MT
--OLEProd_MT
--HVC_MT

SELECT * FROM calc.Divisors d
WHERE d.Refnum = '2013PCH033'
AND FactorSetId = '2013'
AND	[d].[DivisorField] = 'Production_kMT'
AND ([d].[DivisorID] = 'C2H4'
OR	[d].[DivisorID] = 'ProdOlefins'
OR	[d].[DivisorID] = 'ProdHVC')
