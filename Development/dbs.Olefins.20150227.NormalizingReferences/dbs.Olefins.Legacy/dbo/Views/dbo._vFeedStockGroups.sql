﻿
/****** Object:  View dbo._vFeedStockGroups    Script Date: 4/18/2003 4:32:51 PM ******/

/****** Object:  View dbo._vFeedStockGroups    Script Date: 12/28/2001 7:34:24 AM ******/
CREATE VIEW _vFeedStockGroups AS 



SELECT Refnum, FeedProdID, Q1Feed, Q2Feed, Q3Feed, Q4Feed, AnnFeedProd
from Quantity q
WHERE FeedProdID IN ('Propane', 'LPG', 'Butane',
 'HeavyNGL', 'Condensate', 'Raffinate')

UNION 

SELECT t.Refnum, FeedProdID, Q1Feed, Q2Feed, Q3Feed, Q4Feed, AnnFeedProd
from Quantity q, TSort t
WHERE FeedProdID IN ('Ethane', 'EPMix')
AND t.Refnum = q.Refnum AND Region NOT IN ('Eur', 'Asia')

UNION 

SELECT t.Refnum, 'Ethane/EPMix', SUM(Q1Feed), SUM(Q2Feed), SUM(Q3Feed),
SUM(Q4Feed), SUM(AnnFeedProd)
from Quantity q, TSort t
WHERE FeedProdID IN  ('Ethane', 'EPMix')
AND t.Refnum = q.Refnum AND Region IN ('Eur', 'Asia')

GROUP BY t.Refnum

UNION

SELECT Refnum, 'GasOil', SUM(Q1Feed), SUM(Q2Feed), SUM(Q3Feed),
SUM(Q4Feed), SUM(AnnFeedProd)
from Quantity q
WHERE FeedProdID IN  ('Diesel', 'HeavyGasOil', 'HTGasOil')
GROUP BY Refnum

UNION

SELECT Refnum, 'Naphtha', SUM(Q1Feed), SUM(Q2Feed), SUM(Q3Feed),
SUM(Q4Feed), SUM(AnnFeedProd)
from Quantity q
WHERE FeedProdID IN  ('LtNaphtha', 'FRNaphtha', 'HeavyNaphtha')
GROUP BY Refnum
