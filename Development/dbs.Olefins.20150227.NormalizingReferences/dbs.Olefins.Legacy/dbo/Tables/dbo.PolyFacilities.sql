﻿CREATE TABLE [dbo].[PolyFacilities] (
    [Refnum]          [dbo].[Refnum] NOT NULL,
    [PlantID]         TINYINT        NOT NULL,
    [Name]            VARCHAR (50)   NULL,
    [City]            VARCHAR (40)   NULL,
    [State]           VARCHAR (25)   NULL,
    [Country]         VARCHAR (30)   NULL,
    [CapMTD]          REAL           NULL,
    [CapKMTA]         REAL           NULL,
    [ReactorTrainCnt] INT            NULL,
    [ReactorStartup]  SMALLINT       NULL,
    [Product]         VARCHAR (20)   NULL,
    [Technology]      VARCHAR (20)   NULL,
    [ReactorResinPct] REAL           NULL,
    CONSTRAINT [PK_PolyFacilities_1__10] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PlantID] ASC)
);

