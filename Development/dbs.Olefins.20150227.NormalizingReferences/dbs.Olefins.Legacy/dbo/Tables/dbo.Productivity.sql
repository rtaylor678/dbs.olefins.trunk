﻿CREATE TABLE [dbo].[Productivity] (
    [Refnum]              [dbo].[Refnum] NOT NULL,
    [C2TEPmt]             REAL           NULL,
    [C2C3TEPmt]           REAL           NULL,
    [HVChemTEPmt]         REAL           NULL,
    [EDCTEP]              REAL           NULL,
    [ProcOTPcnt]          REAL           NULL,
    [RoutOTPcnt]          REAL           NULL,
    [TAOTPcnt]            REAL           NULL,
    [MaintOTPcnt]         REAL           NULL,
    [AdminOTPcnt]         REAL           NULL,
    [OCCOTPcnt]           REAL           NULL,
    [ProcContractorPcnt]  REAL           NULL,
    [RoutContractorPcnt]  REAL           NULL,
    [TAContractorPcnt]    REAL           NULL,
    [MaintContractorPcnt] REAL           NULL,
    [AdmContractorPcnt]   REAL           NULL,
    [OCCContractorPcnt]   REAL           NULL,
    [EDCTotWHr]           REAL           NULL,
    [HVChemTotWHrmt]      REAL           NULL,
    CONSTRAINT [PK___Productivity] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Productivity', @level2type = N'COLUMN', @level2name = N'C2C3TEPmt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RankVariable 7 - TEP/MMTHVC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Productivity', @level2type = N'COLUMN', @level2name = N'HVChemTEPmt';

