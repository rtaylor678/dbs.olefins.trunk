﻿CREATE TABLE [dbo].[FeedPlan] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [PlanID]      VARCHAR (25)   NOT NULL,
    [Daily]       CHAR (1)       NULL,
    [BiWeekly]    CHAR (1)       NULL,
    [Weekly]      CHAR (1)       NULL,
    [BiMonthly]   CHAR (1)       NULL,
    [Monthly]     CHAR (1)       NULL,
    [LessMonthly] CHAR (1)       NULL,
    [Rpt]         TINYINT        NULL,
    CONSTRAINT [PK_FeedPlan] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PlanID] ASC)
);

