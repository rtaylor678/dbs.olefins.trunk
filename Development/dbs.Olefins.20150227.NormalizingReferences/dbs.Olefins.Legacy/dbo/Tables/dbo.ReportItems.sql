﻿CREATE TABLE [dbo].[ReportItems] (
    [ReportID]             INT                  NOT NULL,
    [DisplayOrder]         SMALLINT             NOT NULL,
    [ReportItemID]         INT                  NOT NULL,
    [ItemType]             CHAR (1)             NOT NULL,
    [LabelOverride]        VARCHAR (50)         NULL,
    [FormatOverride]       TINYINT              NULL,
    [DisplayUnitsOverride] VARCHAR (20)         NULL,
    [RptItemScenario]      VARCHAR (8)          NULL,
    [RptItemSQL]           VARCHAR (255)        NULL,
    [RptItemCurrency]      [dbo].[CurrencyCode] NULL,
    CONSTRAINT [PK_ReportItems_1__19] PRIMARY KEY CLUSTERED ([ReportID] ASC, [DisplayOrder] ASC)
);

