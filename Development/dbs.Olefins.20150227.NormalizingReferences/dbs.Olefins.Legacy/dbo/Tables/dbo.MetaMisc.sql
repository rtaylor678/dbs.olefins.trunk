﻿CREATE TABLE [dbo].[MetaMisc] (
    [Refnum]        [dbo].[Refnum] NOT NULL,
    [CapitalCost]   REAL           NULL,
    [UnitStartUpYr] REAL           NULL,
    CONSTRAINT [PK_MetaMisc] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

