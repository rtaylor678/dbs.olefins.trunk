﻿CREATE TABLE [dbo].[MetaFeedProd] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [FeedProdID]  VARCHAR (20)   NOT NULL,
    [Qtr1]        REAL           NULL,
    [Qtr2]        REAL           NULL,
    [Qtr3]        REAL           NULL,
    [Qtr4]        REAL           NULL,
    [AnnFeedProd] REAL           NULL,
    CONSTRAINT [PK_MetaFeedProd] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

