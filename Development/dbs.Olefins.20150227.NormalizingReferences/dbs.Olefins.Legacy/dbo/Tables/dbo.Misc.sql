﻿CREATE TABLE [dbo].[Misc] (
    [Refnum]                [dbo].[Refnum] NOT NULL,
    [EthPcntTot]            REAL           NULL,
    [ProPcntTot]            REAL           NULL,
    [ButPcntTot]            REAL           NULL,
    [OCCAbsence]            REAL           NULL,
    [MPSAbsence]            REAL           NULL,
    [SplFeedEnergy]         REAL           NULL,
    [LtFeedPriceBasis]      REAL           NULL,
    [OthLiqFeedName1]       VARCHAR (25)   NULL,
    [OthLiqFeedPriceBasis1] REAL           NULL,
    [OthLiqFeedName2]       VARCHAR (25)   NULL,
    [OthLiqFeedPriceBasis2] REAL           NULL,
    [OthLiqFeedName3]       VARCHAR (25)   NULL,
    [OthLiqFeedPriceBasis3] REAL           NULL,
    [TotCompOutage]         REAL           NULL,
    [PctFurnRelyLimit]      REAL           NULL,
    [ROG_CGCI]              CHAR (1)       NULL,
    [ROG_CGCD]              CHAR (1)       NULL,
    [ROG_C2RSF]             CHAR (1)       NULL,
    [ROG_C3RSF]             CHAR (1)       NULL,
    CONSTRAINT [PK___23__14] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

