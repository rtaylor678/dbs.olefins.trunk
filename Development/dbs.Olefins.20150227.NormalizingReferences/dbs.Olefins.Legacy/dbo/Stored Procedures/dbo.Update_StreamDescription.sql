﻿CREATE PROCEDURE [dbo].[Update_StreamDescription]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamDescription		NVARCHAR(256)	= NULL
)
AS
BEGIN

	IF EXISTS (SELECT 1 FROM [stage].[StreamDescription] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamNumber] = @StreamNumber)
	BEGIN
		EXECUTE [stage].[Update_StreamDescription] @SubmissionId, @StreamNumber, @StreamDescription;
	END;
	ELSE
	BEGIN
		IF(@StreamDescription	<> '')
		EXECUTE [stage].[Insert_StreamDescription] @SubmissionId, @StreamNumber, @StreamDescription;
	END;

END;
