﻿CREATE PROCEDURE [dbo].[Update_StreamRecycled]
(
	@SubmissionId			INT,
	@ComponentId			INT,

	@Recycled_WtPcnt		FLOAT	= NULL
)
AS
BEGIN

	IF EXISTS (SELECT 1 FROM [stage].[StreamRecycled] t WHERE t.[SubmissionId] = @SubmissionId AND t.[ComponentId] = @ComponentId)
	BEGIN
		SET @Recycled_WtPcnt = COALESCE(@Recycled_WtPcnt, 0.0);
		EXECUTE [stage].[Update_StreamRecycled] @SubmissionId, @ComponentId, @Recycled_WtPcnt;
	END;
	ELSE
	BEGIN
		IF(@Recycled_WtPcnt >= 0.0)
		EXECUTE [stage].[Insert_StreamRecycled] @SubmissionId, @ComponentId, @Recycled_WtPcnt;
	END;

END;
