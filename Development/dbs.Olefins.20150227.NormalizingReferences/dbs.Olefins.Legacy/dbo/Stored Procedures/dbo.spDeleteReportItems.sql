﻿
/****** Object:  Stored Procedure dbo.spDeleteReportItems    Script Date: 4/18/2003 4:32:53 PM ******/

/****** Object:  Stored Procedure dbo.spDeleteReportItems    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.spDeleteReportItems    Script Date: 04/13/2000 8:32:50 AM ******/
CREATE PROCEDURE spDeleteReportItems
		@ReportName varchar(30) = NULL,
		@ReportID integer = NULL
AS
IF @ReportID IS NULL
	SELECT @ReportID = ReportID
	FROM ReportDef
	WHERE ReportName = @ReportName
DELETE FROM ReportItems
WHERE ReportID = @ReportID


