﻿

/****** Object:  Stored Procedure dbo.spCheckListOwner    Script Date: 4/18/2003 4:32:52 PM ******/

CREATE  PROCEDURE [dbo].[spCheckListOwner]
	@ListName RefListName = NULL,
	@ListNo RefListNo = NULL,
	@CurrUser varchar(30) = NULL
AS
DECLARE @UName varchar(30), @UGroup varchar(30)

IF @ListNo Is NULL
	SELECT @ListNo = RefListNo FROM RefList_LU
	WHERE ListName = @ListName
IF @ListNo IS NOT NULL
BEGIN
	IF @CurrUser IS NULL
		SELECT @UName = User_Name()
	ELSE
		SELECT @UName = @CurrUser
	IF @UName = 'sa'
		SELECT @UName = 'dbo'

	IF EXISTS (SELECT * FROM sys.sysusers u INNER JOIN sys.sysmembers m ON m.memberuid = u.uid INNER JOIN sys.sysusers g ON g.uid = m.groupuid 
				WHERE u.name = @UName AND g.name in ('developer', 'Datagrp', 'db_owner')) OR @UName = 'dbo'
		RETURN 2
	ELSE
	BEGIN
		IF EXISTS (SELECT * FROM RefList_LU
			WHERE RefListNo = @ListNo AND
			(Owner <> @UName AND Owner IS NOT NULL))
			RETURN -101
		ELSE
			RETURN 1
	END
END

