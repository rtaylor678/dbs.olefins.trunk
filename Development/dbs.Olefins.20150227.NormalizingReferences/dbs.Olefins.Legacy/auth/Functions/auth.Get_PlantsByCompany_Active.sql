﻿CREATE FUNCTION [auth].[Get_PlantsByCompany_Active]
(
	@CompanyId		INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		pbc.[JoinId],
		pbc.[CompanyId],
		pbc.[PlantId],
		pbc.[CompanyName],
		pbc.[PlantName]
	FROM [auth].[PlantsByCompany_Active]	pbc WITH (NOEXPAND)
	WHERE	pbc.[CompanyId]	= @CompanyId
);