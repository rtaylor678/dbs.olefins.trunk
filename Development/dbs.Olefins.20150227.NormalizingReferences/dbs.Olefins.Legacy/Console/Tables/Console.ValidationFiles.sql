﻿CREATE TABLE [Console].[ValidationFiles] (
    [StudyYear]   [dbo].[StudyYear] NULL,
    [Study]       [dbo].[Study]     NULL,
    [Description] NVARCHAR (100)    NULL,
    [Link]        NVARCHAR (100)    NULL
);

