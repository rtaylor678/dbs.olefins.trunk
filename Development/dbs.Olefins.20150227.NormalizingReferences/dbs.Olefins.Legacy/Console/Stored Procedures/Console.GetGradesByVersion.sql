﻿CREATE proc [Console].[GetGradesByVersion]
@RefNum dbo.Refnum,
@Version int
as
SELECT Version, ROUND(GRADE,0) AS Grade, NumReds, NumBlues, NumTeals, NumGreens FROM Val.RefineryGrade WHERE Refnum = @RefNum  ORDER BY version DESC, Grade DESC
