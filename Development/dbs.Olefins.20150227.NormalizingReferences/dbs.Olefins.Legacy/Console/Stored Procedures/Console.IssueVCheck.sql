﻿CREATE PROCEDURE [Console].[IssueVCheck]
	@RefNum dbo.RefNum,
	@IssueID varchar(20)
AS
BEGIN
DECLARE @Ret int
Select @Ret = Completed from Val.Checklist where Refnum=@RefNum and IssueID = @IssueID
IF @Ret > 'Y'
	Return 1
ELSE
	return 0
END