﻿CREATE PROCEDURE [Console].[GetRefNums]
	@StudyYear dbo.StudyYear,
	@Study varchar(3)
AS
BEGIN
	SELECT RefNum, CoLoc, Consultant FROM dbo.TSort
	WHERE StudyYear = @StudyYear --AND Study = @Study 
	ORDER BY RefNum ASC
END
