﻿CREATE FUNCTION [dim].[Return_FacilityId]
(
	@FacilityTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[FacilityId]
	FROM [dim].[Facility_LookUp]	l
	WHERE l.[FacilityTag] = @FacilityTag;

	RETURN @Id;

END;