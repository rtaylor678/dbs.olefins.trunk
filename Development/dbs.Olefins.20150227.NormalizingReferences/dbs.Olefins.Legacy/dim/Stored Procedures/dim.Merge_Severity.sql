﻿CREATE PROCEDURE [dim].[Merge_Severity]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [dim].[Severity_LookUp] AS Target
	USING
	(
		VALUES
			('Information', 'Informational Message', 'Informational Message'),
			('Warning', 'Warning Message', 'Warning Message'),
			('Error', 'Error Message', 'Error Message; Calculations will fail'),
			('Critical', 'Critical Error Message', 'Critical Error Message; Processing will fail')
	)
	AS Source([SeverityTag], [SeverityName], [SeverityDetail])
	ON	Target.[SeverityTag]			= Source.[SeverityTag]
	WHEN MATCHED THEN UPDATE SET
		Target.[SeverityName]		= Source.[SeverityName],
		Target.[SeverityDetail]		= Source.[SeverityDetail]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([SeverityTag], [SeverityName], [SeverityDetail])
		VALUES([SeverityTag], [SeverityName], [SeverityDetail])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;