﻿CREATE TABLE [dim].[FeedClass_Parent] (
    [MethodologyId]  INT                 NOT NULL,
    [FeedClassId]    INT                 NOT NULL,
    [ParentId]       INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_FeedClass_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedClass_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_FeedClass_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_FeedClass_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_FeedClass_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedClass_Parent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FeedClassId] ASC),
    CONSTRAINT [FK_FeedClass_Parent_LookUp_FeedClass] FOREIGN KEY ([FeedClassId]) REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId]),
    CONSTRAINT [FK_FeedClass_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId]),
    CONSTRAINT [FK_FeedClass_Parent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_FeedClass_Parent_Operator] FOREIGN KEY ([Operator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_FeedClass_Parent_Parent] FOREIGN KEY ([MethodologyId], [FeedClassId]) REFERENCES [dim].[FeedClass_Parent] ([MethodologyId], [FeedClassId])
);


GO

CREATE TRIGGER [dim].[t_FeedClass_Parent_u]
	ON [dim].[FeedClass_Parent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedClass_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[FeedClass_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[FeedClass_Parent].[FeedClassId]		= INSERTED.[FeedClassId];

END;