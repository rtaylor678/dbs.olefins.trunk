﻿CREATE TABLE [dim].[Facility_LookUp] (
    [FacilityId]     INT                IDENTITY (1, 1) NOT NULL,
    [FacilityTag]    VARCHAR (42)       NOT NULL,
    [FacilityName]   VARCHAR (84)       NOT NULL,
    [FacilityDetail] VARCHAR (256)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Facility_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Facility_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Facility_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Facility_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Facility_LookUp] PRIMARY KEY CLUSTERED ([FacilityId] ASC),
    CONSTRAINT [CL_Facility_LookUp_FacilitiesDetail] CHECK ([FacilityDetail]<>''),
    CONSTRAINT [CL_Facility_LookUp_FacilitiesName] CHECK ([FacilityName]<>''),
    CONSTRAINT [CL_Facility_LookUp_FacilitiesTag] CHECK ([FacilityTag]<>''),
    CONSTRAINT [UK_Facility_LookUp_FacilitiesDetail] UNIQUE NONCLUSTERED ([FacilityDetail] ASC),
    CONSTRAINT [UK_Facility_LookUp_FacilitiesName] UNIQUE NONCLUSTERED ([FacilityName] ASC),
    CONSTRAINT [UK_Facility_LookUp_FacilitiesTag] UNIQUE NONCLUSTERED ([FacilityTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_Facility_LookUp_u]
	ON [dim].[Facility_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Facility_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Facility_LookUp].[FacilityId]		= INSERTED.[FacilityId];

END;