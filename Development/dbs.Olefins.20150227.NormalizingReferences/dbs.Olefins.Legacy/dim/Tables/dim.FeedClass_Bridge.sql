﻿CREATE TABLE [dim].[FeedClass_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [FeedClassId]        INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_FeedClass_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedClass_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_FeedClass_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_FeedClass_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_FeedClass_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedClass_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FeedClassId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_FeedClass_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId]),
    CONSTRAINT [FK_FeedClass_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_FeedClass_Bridge_FeedClassId] FOREIGN KEY ([FeedClassId]) REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId]),
    CONSTRAINT [FK_FeedClass_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_FeedClass_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [FeedClassId]) REFERENCES [dim].[FeedClass_Parent] ([MethodologyId], [FeedClassId]),
    CONSTRAINT [FK_FeedClass_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[FeedClass_Parent] ([MethodologyId], [FeedClassId])
);


GO

CREATE TRIGGER [dim].[t_FeedClass_Bridge_u]
	ON [dim].[FeedClass_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedClass_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[FeedClass_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[FeedClass_Bridge].[FeedClassId]		= INSERTED.[FeedClassId]
		AND	[dim].[FeedClass_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;