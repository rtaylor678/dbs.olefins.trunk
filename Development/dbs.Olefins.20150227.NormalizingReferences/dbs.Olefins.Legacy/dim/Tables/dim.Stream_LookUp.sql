﻿CREATE TABLE [dim].[Stream_LookUp] (
    [StreamId]       INT                IDENTITY (1, 1) NOT NULL,
    [StreamTag]      VARCHAR (42)       NOT NULL,
    [StreamName]     VARCHAR (84)       NOT NULL,
    [StreamDetail]   VARCHAR (256)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Stream_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Stream_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Stream_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Stream_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Stream_LookUp] PRIMARY KEY CLUSTERED ([StreamId] ASC),
    CONSTRAINT [CL_Stream_LookUp_StreamDetail] CHECK ([StreamDetail]<>''),
    CONSTRAINT [CL_Stream_LookUp_StreamName] CHECK ([StreamName]<>''),
    CONSTRAINT [CL_Stream_LookUp_StreamTag] CHECK ([StreamTag]<>''),
    CONSTRAINT [UK_Stream_LookUp_StreamDetail] UNIQUE NONCLUSTERED ([StreamDetail] ASC),
    CONSTRAINT [UK_Stream_LookUp_StreamName] UNIQUE NONCLUSTERED ([StreamName] ASC),
    CONSTRAINT [UK_Stream_LookUp_StreamTag] UNIQUE NONCLUSTERED ([StreamTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_Stream_LookUp_u]
	ON [dim].[Stream_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Stream_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Stream_LookUp].[StreamId]	= INSERTED.[StreamId];

END;