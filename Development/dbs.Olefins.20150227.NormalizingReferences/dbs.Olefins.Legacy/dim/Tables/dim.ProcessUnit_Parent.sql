﻿CREATE TABLE [dim].[ProcessUnit_Parent] (
    [MethodologyId]  INT                 NOT NULL,
    [ProcessUnitId]  INT                 NOT NULL,
    [ParentId]       INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_ProcessUnit_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_ProcessUnit_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_ProcessUnit_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_ProcessUnit_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_ProcessUnit_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ProcessUnit_Parent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ProcessUnitId] ASC),
    CONSTRAINT [FK_ProcessUnit_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId]),
    CONSTRAINT [FK_ProcessUnit_Parent_LookUp_ProcessUnit] FOREIGN KEY ([ProcessUnitId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId]),
    CONSTRAINT [FK_ProcessUnit_Parent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_ProcessUnit_Parent_Operator] FOREIGN KEY ([Operator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_ProcessUnit_Parent_Parent] FOREIGN KEY ([MethodologyId], [ProcessUnitId]) REFERENCES [dim].[ProcessUnit_Parent] ([MethodologyId], [ProcessUnitId])
);


GO

CREATE TRIGGER [dim].[t_ProcessUnit_Parent_u]
	ON [dim].[ProcessUnit_Parent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnit_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[ProcessUnit_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[ProcessUnit_Parent].[ProcessUnitId]		= INSERTED.[ProcessUnitId];

END;