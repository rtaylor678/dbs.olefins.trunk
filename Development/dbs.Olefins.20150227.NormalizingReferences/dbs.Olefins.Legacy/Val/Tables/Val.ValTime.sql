﻿CREATE TABLE [Val].[ValTime] (
    [Refnum]     CHAR (9)  NOT NULL,
    [Consultant] CHAR (10) NULL,
    [StartTime]  DATETIME  NOT NULL,
    [StopTime]   DATETIME  NULL,
    [ValTime]    REAL      NULL,
    [RevNo]      CHAR (3)  NULL,
    CONSTRAINT [PK_ValTime] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StartTime] ASC) WITH (FILLFACTOR = 70)
);

