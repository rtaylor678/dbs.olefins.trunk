﻿CREATE TABLE [fact].[FacilitiesHydroTreater] (
    [SubmissionId]       INT                NOT NULL,
    [FacilityId]         INT                NOT NULL,
    [Quantity_kBSD]      FLOAT (53)         NOT NULL,
    [_Quantity_BSD]      AS                 (CONVERT([float],[Quantity_kBSD]*(1000.0),0)) PERSISTED NOT NULL,
    [HydroTreaterTypeId] INT                NOT NULL,
    [Pressure_PSIg]      FLOAT (53)         NULL,
    [Processed_kMT]      FLOAT (53)         NULL,
    [Processed_Pcnt]     FLOAT (53)         NULL,
    [Density_SG]         FLOAT (53)         NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesHydroTreater_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesHydroTreater] PRIMARY KEY CLUSTERED ([SubmissionId] DESC),
    CONSTRAINT [CR_FacilitiesHydroTreater_Density_SG_MinIncl_0.0] CHECK ([Density_SG]>=(0.0)),
    CONSTRAINT [CR_FacilitiesHydroTreater_Pressure_PSIg_MinIncl_0.0] CHECK ([Pressure_PSIg]>=(0.0)),
    CONSTRAINT [CR_FacilitiesHydroTreater_Processed_kMT_MinIncl_0.0] CHECK ([Processed_kMT]>=(0.0)),
    CONSTRAINT [CR_FacilitiesHydroTreater_Processed_Pcnt_MaxIncl_100.0] CHECK ([Processed_Pcnt]<=(100.0)),
    CONSTRAINT [CR_FacilitiesHydroTreater_Processed_Pcnt_MinIncl_0.0] CHECK ([Processed_Pcnt]>=(0.0)),
    CONSTRAINT [CR_FacilitiesHydroTreater_Quantity_kBSD_MinIncl_0.0] CHECK ([Quantity_kBSD]>=(0.0)),
    CONSTRAINT [FK_FacilitiesHydroTreater_Facilities] FOREIGN KEY ([SubmissionId], [FacilityId]) REFERENCES [fact].[Facilities] ([SubmissionId], [FacilityId]),
    CONSTRAINT [FK_FacilitiesHydroTreater_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesHydroTreater_HydroTreater_LookUp] FOREIGN KEY ([HydroTreaterTypeId]) REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId]),
    CONSTRAINT [FK_FacilitiesHydroTreater_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_FacilitiesHydroTreater_u]
	ON [fact].[FacilitiesHydroTreater]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[FacilitiesHydroTreater].[SubmissionId]		= INSERTED.[SubmissionId];

END;