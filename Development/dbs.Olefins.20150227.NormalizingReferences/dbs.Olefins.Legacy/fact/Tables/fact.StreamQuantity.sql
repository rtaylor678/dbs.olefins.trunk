﻿CREATE TABLE [fact].[StreamQuantity] (
    [SubmissionId]   INT                NOT NULL,
    [StreamNumber]   INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [Quantity_kMT]   FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_StreamQuantity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_StreamQuantity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_StreamQuantity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_StreamQuantity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamQuantity] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC),
    CONSTRAINT [CR_StreamQuantity_Quantity_kMT_MinIncl_0.0] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamQuantity_StreamNumber] CHECK ([StreamNumber]>(0)),
    CONSTRAINT [FK_StreamQuantity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_StreamQuantity_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId]),
    CONSTRAINT [UK_StreamQuantity_Attributes] UNIQUE NONCLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC, [StreamId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_StreamQuantity_StreamId]
    ON [fact].[StreamQuantity]([SubmissionId] DESC, [StreamId] ASC)
    INCLUDE([Quantity_kMT]);


GO
CREATE NONCLUSTERED INDEX [IX_StreamQuantity_ReferencedFields]
    ON [fact].[StreamQuantity]([SubmissionId] DESC, [StreamNumber] ASC, [StreamId] ASC)
    INCLUDE([Quantity_kMT]);


GO

CREATE TRIGGER [fact].[t_StreamQuantity_u]
	ON [fact].[StreamQuantity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamQuantity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamQuantity].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[fact].[StreamQuantity].[StreamNumber]		= INSERTED.[StreamNumber];

END;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1000 Light; 2000 Liquid; 3000 Recycle; 4000 Supplemental; 5000 Product', @level0type = N'SCHEMA', @level0name = N'fact', @level1type = N'TABLE', @level1name = N'StreamQuantity', @level2type = N'COLUMN', @level2name = N'StreamNumber';

