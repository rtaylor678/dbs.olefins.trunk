﻿CREATE TABLE [stage].[Submissions] (
    [SubmissionId]   INT                IDENTITY (1, 1) NOT NULL,
    [SubmissionName] NVARCHAR (128)     NOT NULL,
    [DateBeg]        DATE               NOT NULL,
    [DateEnd]        DATE               NOT NULL,
    [_Duration_Days] AS                 (CONVERT([float],datediff(day,[DateBeg],[DateEnd])+(1.0),0)) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Submissions_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Submissions] PRIMARY KEY CLUSTERED ([SubmissionId] DESC),
    CONSTRAINT [CL_Sumbissions_SumbissionName] CHECK ([SubmissionName]<>'')
);


GO


CREATE TRIGGER [stage].[t_Submissions_u]
	ON [stage].[Submissions]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[Submissions]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[Submissions].[SubmissionId]		= INSERTED.[SubmissionId]

END;