﻿CREATE TABLE [dbo].[CptlCode_LU] (
    [CptlCode]    CHAR (3)     NOT NULL,
    [Description] VARCHAR (40) NULL,
    CONSTRAINT [PK___4__10] PRIMARY KEY CLUSTERED ([CptlCode] ASC)
);

