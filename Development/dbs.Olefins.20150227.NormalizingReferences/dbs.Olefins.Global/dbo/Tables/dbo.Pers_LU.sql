﻿CREATE TABLE [dbo].[Pers_LU] (
    [PersID]      CHAR (15)    NOT NULL,
    [Description] VARCHAR (50) NULL,
    [SortKey]     INT          NULL,
    [SectionID]   CHAR (4)     NULL,
    CONSTRAINT [PK___10__10] PRIMARY KEY CLUSTERED ([PersID] ASC)
);

