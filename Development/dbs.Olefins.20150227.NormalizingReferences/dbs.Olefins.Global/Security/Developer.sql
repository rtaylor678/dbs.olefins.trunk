﻿CREATE ROLE [Developer]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'FRS';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'glc';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'JED';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'PRD';


GO
EXECUTE sp_addrolemember @rolename = N'Developer', @membername = N'RRH';

