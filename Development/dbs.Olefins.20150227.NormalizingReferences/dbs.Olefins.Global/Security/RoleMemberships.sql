﻿EXECUTE sp_addrolemember @rolename = N'db_ddladmin', @membername = N'RRH';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'Anyone';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'DGM';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'EJE';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'wlw';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'MGV';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'EJB';

