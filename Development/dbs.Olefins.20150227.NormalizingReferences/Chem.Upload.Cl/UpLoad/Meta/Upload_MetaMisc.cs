﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadMetaMisc(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rngCost = null;
			Excel.Range rngStart = null;

			const UInt32 rCost = 59;
			const UInt32 rStart = 61;

			const UInt32 c = 8;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table13"];

				rngCost = wks.Cells[rCost, c];
				rngStart = wks.Cells[rStart, c];

				if (RangeHasValue(rngCost) || RangeHasValue(rngStart))
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_MetaMisc]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum				CHAR (9)
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@CapitalCost   REAL     = NULL
					if (RangeHasValue(rngCost)) { cmd.Parameters.Add("@CapitalCost", SqlDbType.Float).Value = ReturnFloat(rngCost); }

					//@UnitStartUpYr REAL     = NULL
					if (RangeHasValue(rngStart)) { cmd.Parameters.Add("@UnitStartUpYr", SqlDbType.Float).Value = ReturnFloat(rngStart); }
					
					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMetaEnergy", Refnum, wkb, wks, rngCost, rCost, c, "[stgFact].[Insert_MetaMisc]", ex);
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMetaEnergy", Refnum, wkb, wks, rngStart, rStart, c, "[stgFact].[Insert_MetaMisc]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rngCost = null;
				rngStart = null;
				wks = null;
			}
		}
	}
}
