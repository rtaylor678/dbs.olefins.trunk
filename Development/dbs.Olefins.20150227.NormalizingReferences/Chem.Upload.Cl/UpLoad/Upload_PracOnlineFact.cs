﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracOnlineFact(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 9;

			string sht = string.Empty;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();

				sht = "Table9-5";

				wks = wkb.Worksheets[sht];

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracOnlineFact]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum			CHAR (9),
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				//@Tbl9051 REAL     = NULL
				r = 34;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9051", SqlDbType.Float).Value = ReturnFloat(rng); }

				sht = "Table9-6";

				wks = wkb.Worksheets[sht];

				//@Tbl9061 REAL     = NULL
				r = 21;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9061", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Tbl9071 REAL     = NULL
				r = 63;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9071", SqlDbType.Float).Value = ReturnFloat(rng); }

				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracOnlineFact", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracOnlineFact]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
