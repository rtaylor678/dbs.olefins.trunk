﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracOrg(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			const UInt32 c = 9;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table8-2"];

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracOrg]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum		CHAR (9)
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				//@Tbl1205           CHAR (1)     = NULL,
				r = 19;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) {cmd.Parameters.Add("@Tbl1205", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1);}

				//@IntegrationRefine CHAR (1)     = NULL,
				r = 20;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@IntegrationRefine", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@PolyPlantShare    VARCHAR (3)  = NULL
				r = 17;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PolyPlantShare", SqlDbType.VarChar, 3).Value = ReturnString(rng, 3); }

				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracOrg", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracOrg]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
