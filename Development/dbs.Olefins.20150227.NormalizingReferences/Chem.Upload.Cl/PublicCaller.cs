﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using Microsoft.CSharp;

using Chem.UpLoad;

//	http://richnewman.wordpress.com/2007/04/15/a-beginner%E2%80%99s-guide-to-calling-a-net-library-from-excel/

[ComVisible(true)]
[ClassInterface(ClassInterfaceType.AutoDual)]
public class ChemUpLoad
{
	public void UpLoadChemFile(string PathFile)
	{
		string FileType = Common.GetFileType(PathFile);
		string Refnum = Common.GetRefnum(FileType, PathFile);
		string sRefnum = Common.GetRefnumSmall(Refnum);
		System.DateTimeOffset TimeBeg = System.DateTimeOffset.Now;

		ErrorHandler.EtlErrors.Clear();
		ErrorHandler.Delete_UploadErrors(FileType, Refnum);

		InsertLog(sRefnum, PathFile, FileType, TimeBeg);

		if (FileType == Chem.UpLoad.Common.Pyps || FileType == Chem.UpLoad.Common.Spsl)
		{
			Chem.UpLoad.Simulation s = new Chem.UpLoad.Simulation();
			s.UpLoad(Refnum, PathFile, FileType);
			s = null;
		}
		else if (FileType == Chem.UpLoad.Common.Osim )
		{
			Chem.UpLoad.Upload_OSIM s = new Chem.UpLoad.Upload_OSIM();
			s.UpLoad(Refnum, PathFile);
			s = null;
		}
		else if (FileType == Chem.UpLoad.Common.Seec)
		{
			Chem.UpLoad.Upload_SEEC s = new Chem.UpLoad.Upload_SEEC();
			s.UpLoad(Refnum, PathFile);
			s = null;
		}

		int Error_Count = ErrorHandler.EtlErrors.Count();

		if (Error_Count == 0)
		{
			uint StudyYear = Common.GetStudyYear(Refnum);
			Common.CopyFile(PathFile, Refnum, StudyYear);
		}

		System.DateTimeOffset TimeEnd = System.DateTimeOffset.Now;
		string msg = ErrorHandler.AggregateMessage();

		UpdateLog(sRefnum, PathFile, FileType, TimeBeg, TimeEnd, Error_Count, msg);
		spInsertMessage(PathFile);
	}

	public static void UpLoadFile(string FilePath)
	{
		ChemUpLoad u = new ChemUpLoad();
		u.UpLoadChemFile(FilePath);
	}

	void InsertLog(string Refnum, string PathFile, string FileType, System.DateTimeOffset TimeBeg)
	{
		SqlConnection cn = new SqlConnection(Common.cnString());
		cn.Open();

		SqlCommand cmd = new SqlCommand("[stgFact].[Insert_UploadLog]", cn);
		cmd.CommandType = CommandType.StoredProcedure;

		cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
		cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 20).Value = FileType;
		cmd.Parameters.Add("@FilePath", SqlDbType.VarChar, 512).Value = PathFile;
		cmd.Parameters.Add("@TimeBeg", SqlDbType.DateTimeOffset).Value = TimeBeg;

		cmd.ExecuteNonQuery();

		cn.Close();
	}

	void UpdateLog(string Refnum, string PathFile, string FileType, System.DateTimeOffset TimeBeg, System.DateTimeOffset TimeEnd, int Error_Count, string Msg)
	{
		SqlConnection cn = new SqlConnection(Common.cnString());
		cn.Open();

		SqlCommand cmd = new SqlCommand("[stgFact].[Update_UploadLog]", cn);
		cmd.CommandType = CommandType.StoredProcedure;

		cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
		cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 20).Value = FileType;
		cmd.Parameters.Add("@FilePath", SqlDbType.VarChar, 512).Value = PathFile;
		cmd.Parameters.Add("@TimeBeg", SqlDbType.DateTimeOffset).Value = TimeBeg;
		cmd.Parameters.Add("@TimeEnd", SqlDbType.DateTimeOffset).Value = TimeEnd;
		cmd.Parameters.Add("@Error_Count", SqlDbType.Int).Value = Error_Count;
		cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 8000).Value = Msg;

		cmd.ExecuteNonQuery();

		cn.Close();
	}

	void spInsertMessage(string PathFile)
	{

		string FileType = Common.GetFileType(PathFile);
		string Refnum = Common.GetRefnum(FileType, PathFile);
		string sRefnum = Common.GetRefnumSmall(Refnum);

		int EtlErrCount = ErrorHandler.EtlErrors.Count;

		string Source = "DLL " + Common.ProdVer();

		string UploadedFile = "Unknown";
		int MsgId = 0;

		if (FileType == "OSIM" || FileType == "SEEC") { UploadedFile = "OSIM"; MsgId = 5; }
		if (FileType == "PYPS") { UploadedFile = "PYPS"; MsgId = 8; }
		if (FileType == "SPSL") { UploadedFile = "SPSL"; MsgId = 9; }

		SqlConnection cn = new SqlConnection(Common.cnString());
		cn.Open();

		SqlCommand cmd = new SqlCommand("[dbo].[spInsertMessage]", cn);
		cmd.CommandType = CommandType.StoredProcedure;

		cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
		cmd.Parameters.Add("@MsgID", SqlDbType.Int).Value = MsgId;
		cmd.Parameters.Add("@Source", SqlDbType.VarChar, 12).Value = Source;

		cmd.Parameters.Add("@P1", SqlDbType.VarChar, 255).Value = EtlErrCount.ToString();
		cmd.Parameters.Add("@P2", SqlDbType.VarChar, 255).Value = UploadedFile;
		cmd.Parameters.Add("@P3", SqlDbType.VarChar, 255).Value = ErrorHandler.AggregateMessage();
		cmd.Parameters.Add("@P4", SqlDbType.VarChar, 255).Value = Common.ProdNameVer();

		cmd.ExecuteNonQuery();

		cn.Close();
	}

	public List<string> EtlErrors 
	{
		get {return ErrorHandler.EtlErrors;}
	}
}

