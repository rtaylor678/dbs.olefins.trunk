﻿CREATE TABLE [dim].[ReliabilityOppLoss_LookUp] (
    [OppLossId]      VARCHAR (42)       NOT NULL,
    [OppLossName]    NVARCHAR (84)      NOT NULL,
    [OppLossDetail]  NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityOppLoss_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReliabilityOppLoss_LookUp] PRIMARY KEY CLUSTERED ([OppLossId] ASC),
    CONSTRAINT [CL_ReliabilityOppLoss_LookUp_OppLossDetail] CHECK ([OppLossDetail]<>''),
    CONSTRAINT [CL_ReliabilityOppLoss_LookUp_OppLossId] CHECK ([OppLossId]<>''),
    CONSTRAINT [CL_ReliabilityOppLoss_LookUp_OppLossName] CHECK ([OppLossName]<>''),
    CONSTRAINT [UK_ReliabilityOppLoss_LookUp_OppLossDetail] UNIQUE NONCLUSTERED ([OppLossDetail] ASC),
    CONSTRAINT [UK_ReliabilityOppLoss_LookUp_OppLossName] UNIQUE NONCLUSTERED ([OppLossName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReliabilityOppLoss_LookUp_u]
	ON [dim].[ReliabilityOppLoss_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReliabilityOppLoss_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityOppLoss_LookUp].[OppLossId]		= INSERTED.[OppLossId];
		
END;
