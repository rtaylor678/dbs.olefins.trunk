﻿CREATE TABLE [dim].[Facility_LookUp] (
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [FacilityName]   NVARCHAR (84)      NOT NULL,
    [FacilityDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Facility_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Facility_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Facility_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Facility_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Facility_LookUp] PRIMARY KEY CLUSTERED ([FacilityId] ASC),
    CONSTRAINT [CL_Facility_LookUp_FacilityDetail] CHECK ([FacilityDetail]<>''),
    CONSTRAINT [CL_Facility_LookUp_FacilityID] CHECK ([FacilityId]<>''),
    CONSTRAINT [CL_Facility_LookUp_FacilityName] CHECK ([FacilityName]<>''),
    CONSTRAINT [UK_Facility_LookUp_FacilityDetail] UNIQUE NONCLUSTERED ([FacilityDetail] ASC),
    CONSTRAINT [UK_Facility_LookUp_FacilityName] UNIQUE NONCLUSTERED ([FacilityName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Facility_LookUp_u]
	ON [dim].[Facility_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Facility_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Facility_LookUp].[FacilityId]		= INSERTED.[FacilityId];
		
END;
