﻿CREATE TABLE [dim].[Edc_LookUp] (
    [EdcId]          VARCHAR (42)       NOT NULL,
    [EdcName]        NVARCHAR (84)      NOT NULL,
    [EdcDetail]      NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Edc_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Edc_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Edc_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Edc_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Edc_LookUp] PRIMARY KEY CLUSTERED ([EdcId] ASC),
    CONSTRAINT [CL_Edc_LookUp_EdcDetail] CHECK ([EdcDetail]<>''),
    CONSTRAINT [CL_Edc_LookUp_EdcId] CHECK ([EdcId]<>''),
    CONSTRAINT [CL_Edc_LookUp_EdcName] CHECK ([EdcName]<>''),
    CONSTRAINT [UK_Edc_LookUp_EdcDetail] UNIQUE NONCLUSTERED ([EdcDetail] ASC),
    CONSTRAINT [UK_Edc_LookUp_EdcName] UNIQUE NONCLUSTERED ([EdcName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Edc_LookUp_u]
	ON [dim].[Edc_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Edc_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Edc_LookUp].[EdcId]		= INSERTED.[EdcId];
		
END;
