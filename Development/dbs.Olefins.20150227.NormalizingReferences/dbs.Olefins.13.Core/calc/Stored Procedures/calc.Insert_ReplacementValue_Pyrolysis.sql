﻿CREATE PROCEDURE [calc].[Insert_ReplacementValue_Pyrolysis]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplacementValue(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReplacementValueId, ReplacementValue)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			i.CurrencyRpt,
			ht.FacilityId,
			POWER(ht.FeedRate_kBsd / 23.4, 0.7) * 17.0 * 361.3 / 355.4 * i._InflationFactor
		FROM @fpl									fpl
		INNER JOIN ante.InflationFactor				i
			--ON	i.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, i.FactorSetId) = fpl.DataYear
			AND	i.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN	fact.FacilitiesHydroTreat		ht
			ON	ht.Refnum = fpl.Refnum
			AND	ht.CalDateKey = fpl.Plant_QtrDateKey
			AND	ht.FeedRate_kBsd > 0.0
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;