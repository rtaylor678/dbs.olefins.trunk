﻿CREATE PROCEDURE [calc].[Insert_MeiMaintenanceCostIndex]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.MeiMaintenanceCostIndex(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MaintAvg_Cur, InflAdjAnn_TurnAroundCost_kCur, Ann_MaintCostIndex_Cur, MaintAvg_PcntRv, InflAdjAnn_TurnAroundCost_PcntRv, Ann_MaintCostIndex_PcntRv)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			
			m.MaintAvg_Cur											[MaintAvg_Cur],
			rl.InflAdjAnn_TurnAroundCost_kCur						[InflAdjAnn_TurnAroundCost_kCur],
			(m.MaintAvg_Cur + rl.InflAdjAnn_TurnAroundCost_kCur)	[Ann_TotMaintCost_Cur],

			m.MaintAvg_Cur / rv.ReplacementLocation / 10.0			[MaintAvg_PcntRv],
			rl.InflAdjAnn_TurnAroundCost_kCur / rv.ReplacementLocation / 10.0
																	[InflAdjAnn_TurnAroundCost_PcntRv],
			(m.MaintAvg_Cur + rl.InflAdjAnn_TurnAroundCost_kCur) / rv.ReplacementLocation / 10.0
																	[MaintCostIndex_PcntRv]

		FROM @fpl									fpl
		INNER JOIN calc.MaintCostAvgAggregate	m
			ON	m.FactorSetId	= fpl.FactorSetId
			AND	m.Refnum		= fpl.Refnum
			AND	m.CalDateKey	= fpl.Plant_QtrDateKey
			AND	m.CurrencyRpt	= fpl.CurrencyRpt
			AND	m.AccountId		= 'Maint'
		INNER JOIN calc.ReliabilityTAAdjAggregate	rl
			ON	rl.FactorSetId	= fpl.FactorSetId
			AND	rl.Refnum		= fpl.Refnum
			AND	rl.CalDateKey	= fpl.Plant_QtrDateKey
			AND	rl.CurrencyRpt	= fpl.CurrencyRpt
		INNER JOIN calc.ReplacementValueAggregate	rv
			ON	rv.FactorSetId	= fpl.FactorSetId
			AND	rv.Refnum		= fpl.Refnum
			AND	rv.CalDateKey	= fpl.Plant_QtrDateKey
			AND	rv.CurrencyRpt	= fpl.CurrencyRpt
			AND	rv.ReplacementValueId = 'UscgReplacement'
			AND	rv.ReplacementLocation IS NOT NULL

		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END