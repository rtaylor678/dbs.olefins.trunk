﻿CREATE PROCEDURE [calc].[Insert_CapacityUtilization_FreshPyroFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.CapacityUtilization(FactorSetId, Refnum, CalDateKey, SchedId, StreamId,
			CapacityAvg_kMT,
			ComponentId, ProductionActual_kMT, Loss_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			c.[SchedId],
			f.StreamId,
			ABS(c.CapacityAvg_kMT) * (100.0 - c.Ann_TurnAroundLoss_Pcnt) / 100.0		[CapacityAvg_kMT],
			'Tot'						[ComponentId],
			f.FeedAnalysis_kMT			[ProductionActual_kMT],
			calc.MaxValue(0.0, 
				calc.MinValue(r.EthyleneLoss_kMT / r.Ethylene_kMT * f.FeedAnalysis_kMT,	- f.FeedAnalysis_kMT - c.CapacityAvg_kMT * (100.0 - c.Ann_TurnAroundLoss_Pcnt) / 100.0)
				)
		FROM @fpl								tsq
		INNER JOIN calc.DivisorsFeed			f
			ON	f.FactorSetId	= tsq.FactorSetId
			AND	f.Refnum		= tsq.Refnum
			AND	f.CalDateKey	= tsq.Plant_QtrDateKey
			AND	f.StreamId		= 'FreshPyroFeed'
		INNER JOIN calc.CapacityPlantTaAdj		c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_QtrDateKey
			AND	c.StreamId		= 'FreshPyroFeed'
		INNER JOIN calc.CapacityProductionRatio	r
			ON	r.FactorSetId	= tsq.FactorSetId
			AND	r.Refnum		= tsq.Refnum
		WHERE tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;