﻿CREATE PROCEDURE [calc].[Insert_PricingYieldComposition]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[PricingYieldComposition](
			FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, SimModelId, OpCondId, RecycleId, ComponentId,
			Component_kMT, Component_WtPcnt, Region_Amount_Cur, Country_Amount_Cur)
		SELECT
			fpl.FactorSetId,
			psc.ScenarioId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			cyp.SimModelId,
			cyp.OpCondId,
			cyp.RecycleId,
			cyp.ComponentId,
			CASE WHEN cyp.SimModelId = 'Plant'
				THEN cyp.Component_kMT
				ELSE cyp.Component_Extrap_kMT
				END,
			CASE WHEN cyp.SimModelId = 'Plant'
				THEN cyp.Component_WtPcnt
				ELSE cyp.Component_Extrap_WtPcnt
				END,
			pcr._MatlBal_Amount_Cur,
			pcc._MatlBal_Amount_Cur
		FROM @fpl											fpl
		INNER JOIN ante.PricingScenarioConfig				psc
			ON	psc.FactorSetId = fpl.FactorSetId
		INNER JOIN calc.CompositionYieldPlant				cyp
			ON	cyp.FactorSetId = fpl.FactorSetId
			AND	cyp.Refnum = fpl.Refnum
			AND	cyp.CalDateKey = fpl.Plant_AnnDateKey
			AND	cyp.Component_WtPcnt IS NOT NULL
			AND cyp.ComponentId NOT IN ('Inerts', 'Loss')
		INNER JOIN ante.CountryEconRegion					cer
			ON	cer.FactorSetId = fpl.FactorSetId
			AND	cer.CountryId = fpl.CountryId
		LEFT OUTER JOIN inter.PricingCompositionRegion		pcr
			ON	/*pcr.FactorSetId = fpl.FactorSetId
			AND	*/pcr.RegionId = cer.EconRegionID
			AND	pcr.CalDateKey = fpl.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	pcr.CurrencyRpt = fpl.CurrencyRpt
			AND	pcr.StreamId = 'Yield'
			AND	pcr.ComponentId = cyp.ComponentId
			AND pcr.ComponentId <> 'H2'
		LEFT OUTER JOIN inter.PricingCompositionCountry		pcc
			ON	/*pcc.FactorSetId = fpl.FactorSetId
			AND	*/pcc.CountryId = fpl.CountryId
			AND	pcc.CalDateKey = fpl.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	pcc.CurrencyRpt = fpl.CurrencyRpt
			AND	pcc.StreamId = 'Yield'
			AND	pcc.ComponentId = cyp.ComponentId
		WHERE  (pcr._MatlBal_Amount_Cur		IS NOT NULL
			OR	pcc._MatlBal_Amount_Cur		IS NOT NULL);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;