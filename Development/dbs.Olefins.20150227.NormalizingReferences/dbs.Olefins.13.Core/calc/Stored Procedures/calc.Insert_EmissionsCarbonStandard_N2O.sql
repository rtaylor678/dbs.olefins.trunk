﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonStandard_N2O]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonStandard(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_MBtu, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			'PYPS',
			fpl.Plant_QtrDateKey,
			ef.EmissionsId,
			NULL,
			ef.N2O_TBtu * 310.0 / 10000000.0,
			(ISNULL(SUM(CASE b.DescendantOperator
				WHEN '+' THEN	ecp.Quantity_MBtu
				WHEN '-' THEN -	ecp.Quantity_MBtu
				END * stD.Energy_Pcnt
				) * esd.EeiScaleDown / eei._Eei, 0.0)
			+
			ISNULL(SUM(CASE b.DescendantOperator
				WHEN '+' THEN	exu.Quantity_MBtu
				WHEN '-' THEN 	exu.Quantity_MBtu
				END
				), 0.0)) * ef.N2O_TBtu * 310.0 / 10000000.0
		FROM @fpl										fpl
		INNER JOIN calc.Eei								eei
			ON	eei.FactorSetId	= fpl.FactorSetId
			AND	eei.Refnum		= fpl.Refnum
			AND	eei.CalDateKey	= fpl.Plant_QtrDateKey
			AND	eei.OpCondId	= 'OS25'
			AND	eei.ComponentId	= 'ProdHVC'
			AND	eei.SimModelId	= 'PYPS'
		INNER JOIN ante.EeiScaleDown					esd
			ON	esd.FactorSetId	= fpl.FactorSetId
			AND	esd.SimModelId	= eei.SimModelId

		INNER JOIN calc.EmissionsCarbonPlant			ecp
			ON	ecp.FactorSetId	= fpl.FactorSetId
			AND	ecp.Refnum		= fpl.Refnum
			AND	ecp.CalDateKey	= fpl.Plant_QtrDateKey
		INNER JOIN dim.Emissions_Bridge					b
			ON	b.FactorSetId	= fpl.FactorSetId
			AND	b.DescendantId	= ecp.EmissionsId
			AND	b.EmissionsId	IN ('PurchasedUtilities', 'CO2Combustion', 'ExportUtilities')

		INNER JOIN ante.EmissionsStdEnergyDistribution	stD
			ON	stD.FactorSetId	= fpl.FactorSetId
			AND stD.EmissionsId	<> 'PurElec'

		LEFT OUTER JOIN calc.EmissionsCarbonPlant		exu
			ON	exu.FactorSetId	= fpl.FactorSetId
			AND	exu.Refnum		= fpl.Refnum
			AND	exu.CalDateKey	= fpl.Plant_QtrDateKey
			AND exu.EmissionsId	IN (SELECT d.DescendantId FROM dim.Emissions_Bridge d WHERE d.FactorSetId = fpl.FactorSetId aND d.EmissionsId = 'ExportUtilities')
			AND exu.EmissionsId	= ecp.EmissionsId
			AND stD.EmissionsId	= 'CO2Methane'

		INNER JOIN ante.EmissionsFactorNitrousOxide				ef
			ON	ef.FactorSetId = fpl.FactorSetId
			AND	ef.N2O_TBtu IS NOT NULL
			AND ef.EmissionsId	= 'N2OPPFCFuelGas'

		WHERE fpl.CalQtr = 4
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			eei.SimModelId,
			eei._Eei,
			esd.EeiScaleDown,
			ef.EmissionsId,
			ef.N2O_TBtu;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;