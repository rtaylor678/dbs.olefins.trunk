﻿CREATE TABLE [calc].[PeerGroupTech] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PeerGroup]      TINYINT            NOT NULL,
    [Tech_Year]      REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupTech_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupTech_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupTech_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_PeerGroupTech_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PeerGroupTech] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PeerGroupTech_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PeerGroupTech_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PeerGroupTech_PeerGroup] FOREIGN KEY ([FactorSetId], [PeerGroup]) REFERENCES [ante].[PeerGroupTech] ([FactorSetId], [PeerGroup]),
    CONSTRAINT [FK_calc_PeerGroupTech_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_PeerGroupTech_u
ON  calc.PeerGroupTech
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE calc.PeerGroupTech
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PeerGroupTech.FactorSetId		= INSERTED.FactorSetId
		AND calc.PeerGroupTech.Refnum			= INSERTED.Refnum
		AND calc.PeerGroupTech.CalDateKey		= INSERTED.CalDateKey;
		
END;