﻿CREATE TABLE [calc].[NicenessFeed] (
    [FactorSetId]        VARCHAR (12)       NOT NULL,
    [Refnum]             VARCHAR (25)       NOT NULL,
    [CalDateKey]         INT                NOT NULL,
    [StreamId]           VARCHAR (42)       NOT NULL,
    [Stream_kMT]         REAL               NOT NULL,
    [Niceness]           REAL               NULL,
    [NicenessYield]      REAL               NULL,
    [NicenessYield_kMT]  REAL               NOT NULL,
    [NicenessEnergy]     REAL               NULL,
    [NicenessEnergy_kMT] REAL               NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessFeed_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_NicenessFeed_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_NicenessFeed_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_NicenessFeed_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [FK_NicenessFeed_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_NicenessFeed_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_NicenessFeed_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_NicenessFeed_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_NicenessFeed_u
	ON  calc.NicenessFeed
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.NicenessFeed
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.NicenessFeed.[FactorSetId]		= INSERTED.[FactorSetId]
		AND calc.NicenessFeed.[Refnum]			= INSERTED.[Refnum]
		AND calc.NicenessFeed.[CalDateKey]		= INSERTED.[CalDateKey]
		AND calc.NicenessFeed.[StreamId]		= INSERTED.[StreamId];
				
END;
