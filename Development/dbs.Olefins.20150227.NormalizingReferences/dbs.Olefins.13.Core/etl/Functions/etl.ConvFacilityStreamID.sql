﻿
CREATE FUNCTION etl.ConvFacilityStreamID
(
	@StreamId	VARCHAR(42)
)
RETURNS VARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	VARCHAR(42) =
	CASE @StreamId
		WHEN 'Condensate'		THEN 'Condensate'
		WHEN 'NGL'				THEN 'Condensate'
		WHEN 'EP'				THEN 'EPMix'
		WHEN 'Heavy NGL'		THEN 'HeavyNGL'
		WHEN 'ETHANE/PROPANE'	THEN 'EPMix'
		WHEN 'RAW ETHANE'		THEN 'Ethane'
		WHEN 'E/P MIX'			THEN 'EPMix'
		WHEN 'NAPHTHA'			THEN 'Naphtha'
		WHEN 'LPG'				THEN 'LPG'
		WHEN 'ETHANE'			THEN 'Ethane'
		WHEN 'Y-GRADE'			THEN NULL
		WHEN '0'				THEN NULL
		WHEN 'PROPANE/ETHANE'	THEN 'EPMix'
		WHEN 'e/p'				THEN 'EPMix'
		WHEN '32.47'			THEN NULL
		WHEN 'C2/C3'			THEN 'EPMix'
		
		ELSE NULL
	
	END;
		
	RETURN @ResultVar;

END