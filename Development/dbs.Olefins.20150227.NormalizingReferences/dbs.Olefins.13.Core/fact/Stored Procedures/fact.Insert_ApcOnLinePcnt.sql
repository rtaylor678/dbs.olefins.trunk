﻿CREATE PROCEDURE [fact].[Insert_ApcOnLinePcnt]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ApcOnLinePcnt(Refnum, CalDateKey, ApcId, OnLine_Pcnt)
		SELECT
			u.Refnum,
			u.CalDateKey,
			u.FacilityId,
			u.Pcnt * 100.0
		FROM (
			SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
				etl.ConvDateKey(t.StudyYear)								[CalDateKey],
				f.Tbl9051	[PyroFurn],
				f.Tbl9061	[Comp],
				f.Tbl9071	[ProdRecovery]
			FROM stgFact.PracOnlineFact f
			INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
			WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT (
			Pcnt FOR FacilityId IN
				(
				PyroFurn,
				Comp,
				ProdRecovery
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;