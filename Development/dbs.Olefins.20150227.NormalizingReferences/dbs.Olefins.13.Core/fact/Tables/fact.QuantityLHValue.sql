﻿CREATE TABLE [fact].[QuantityLHValue] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [LHValue_MBtuLb]    REAL               NOT NULL,
    [_LHValue_GJkMT]    AS                 (CONVERT([real],[LHValue_MBtuLb]*(0.232587405089023),(1))) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_QuantityLHValue_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_QuantityLHValue_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_QuantityLHValue_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_QuantityLHValue_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_QuantityLHValue] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_QuantityLHValue_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_QuantityLHValue_LHValue_MBtulb] CHECK ([LHValue_MBtuLb]>=(0.0)),
    CONSTRAINT [FK_QuantityLHValue_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_QuantityLHValue_Quantity] FOREIGN KEY ([Refnum], [StreamId], [StreamDescription], [CalDateKey]) REFERENCES [fact].[Quantity] ([Refnum], [StreamId], [StreamDescription], [CalDateKey]),
    CONSTRAINT [FK_QuantityLHValue_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_QuantityLHValue_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_QuantityLHValue_u]
	ON [fact].[QuantityLHValue]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[QuantityLHValue]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[QuantityLHValue].Refnum					= INSERTED.Refnum
		AND [fact].[QuantityLHValue].StreamId				= INSERTED.StreamId
		AND [fact].[QuantityLHValue].StreamDescription		= INSERTED.StreamDescription
		AND [fact].[QuantityLHValue].CalDateKey				= INSERTED.CalDateKey;

END;