﻿CREATE TABLE [fact].[PolyQuantity] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AssetId]        VARCHAR (30)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [Quantity_kMT]   REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PolyQuantity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PolyQuantity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PolyQuantity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PolyQuantity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PolyQuantity] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AssetId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_PolyQuantity_Assets] FOREIGN KEY ([AssetId]) REFERENCES [cons].[Assets] ([AssetId]),
    CONSTRAINT [FK_PolyQuantity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PolyQuantity_PolyFacilities] FOREIGN KEY ([Refnum], [AssetId], [CalDateKey]) REFERENCES [fact].[PolyFacilities] ([Refnum], [AssetId], [CalDateKey]),
    CONSTRAINT [FK_PolyQuantity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_PolyQuantity_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_PolyQuantity_u]
	ON [fact].[PolyQuantity]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[PolyQuantity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[PolyQuantity].Refnum		= INSERTED.Refnum
		AND [fact].[PolyQuantity].AssetId		= INSERTED.AssetId
		AND [fact].[PolyQuantity].StreamId		= INSERTED.StreamId
		AND [fact].[PolyQuantity].CalDateKey	= INSERTED.CalDateKey;

END;