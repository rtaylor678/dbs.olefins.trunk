﻿CREATE VIEW fact.MetaEnergyAggregate
WITH SCHEMABINDING
AS
SELECT
	p.Refnum,
	'MetaEnergy'	[AccountId],
	p.Q1,
	p.Q2,
	p.Q3,
	p.Q4,
	p.Total_MBtu
FROM (
	SELECT
		e.Refnum,
		COALESCE('Q' + CONVERT(CHAR(1), c.CalQtr), 'Total_MBtu')	[Qtr],
		SUM(e.LHValue_MBtu) [LHValue_MBtu]
	FROM fact.MetaEnergy		e
	INNER JOIN dim.Calendar_LookUp	c
		ON	c.CalDateKey = e.CalDateKey
	GROUP BY
		e.Refnum,
		ROLLUP(c.CalQtr)
	) u
	PIVOT (MAX([LHValue_MBtu]) FOR [Qtr] IN (
		[Q1], [Q2], [Q3], [Q4], [Total_MBtu]
		)
	)p;