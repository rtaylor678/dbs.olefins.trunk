﻿CREATE PROCEDURE [stgFact].[Insert_EnergyConsumptionStream]
(
	@Refnum					VARCHAR (25),
	@SimModelId				VARCHAR (12),
	@OpCondId				VARCHAR (12),
	@StreamId				VARCHAR (42),

	@SimEnergy_kCalkg		REAL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO stgFact.EnergyConsumptionStream([Refnum], [SimModelId], [OpCondId], [StreamId], [SimEnergy_kCalkg])
	VALUES(@Refnum, @SimModelId, @OpCondId, @StreamId, @SimEnergy_kCalkg);

END;