﻿CREATE PROCEDURE [stgFact].[Insert_MetaOpEx]
(
	@Refnum VARCHAR (25),
	@Type   VARCHAR (10),

	@NVE    REAL      = NULL,
	@Vol    REAL      = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[MetaOpEx]([Refnum], [Type], [NVE], [Vol])
	VALUES(@Refnum, @Type, @NVE, @Vol);

END;
