﻿CREATE PROCEDURE [stgFact].[Insert_CapGrowth]
(
	@Refnum			VARCHAR (25),
	@Year			INT,

	@EthylProdn		REAL		= NULL,
	@PropylProdn	REAL		= NULL,
	@EthylCap		REAL		= NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO stgFact.CapGrowth([Refnum], [Year], [EthylProdn], [PropylProdn], [EthylCap])
	VALUES(@Refnum, @Year, @EthylProdn, @PropylProdn, @EthylCap);

END;