﻿CREATE PROCEDURE [stgFact].[Delete_UploadErrors]
(
	@FileType			VARCHAR (20),
	@Refnum				VARCHAR (25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[UploadErrors]
	WHERE	[FileType]	= @FileType
		AND	[Refnum]	= @Refnum;

END;