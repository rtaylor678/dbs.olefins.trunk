﻿CREATE TABLE [stgFact].[CoContactInfo] (
    [ContactCode]    VARCHAR (25)       NOT NULL,
    [StudyYear]      SMALLINT           NOT NULL,
    [ContactType]    VARCHAR (5)        NOT NULL,
    [FirstName]      VARCHAR (25)       NULL,
    [LastName]       VARCHAR (25)       NULL,
    [JobTitle]       VARCHAR (75)       NULL,
    [PersonalTitle]  VARCHAR (5)        NULL,
    [Phone]          VARCHAR (40)       NULL,
    [PhoneSecondary] VARCHAR (40)       NULL,
    [Fax]            VARCHAR (40)       NULL,
    [Email]          VARCHAR (255)      NULL,
    [StrAddr1]       VARCHAR (75)       NULL,
    [StrAddr2]       VARCHAR (50)       NULL,
    [StrAdd3]        VARCHAR (50)       NULL,
    [StrCity]        VARCHAR (30)       NULL,
    [StrState]       VARCHAR (30)       NULL,
    [StrZip]         VARCHAR (30)       NULL,
    [StrCountry]     VARCHAR (30)       NULL,
    [MailAddr1]      VARCHAR (75)       NULL,
    [MailAddr2]      VARCHAR (50)       NULL,
    [MailAddr3]      VARCHAR (50)       NULL,
    [MailCity]       VARCHAR (30)       NULL,
    [MailState]      VARCHAR (30)       NULL,
    [MailZip]        VARCHAR (30)       NULL,
    [MailCountry]    VARCHAR (30)       NULL,
    [AltFirstName]   VARCHAR (20)       NULL,
    [AltLastName]    VARCHAR (25)       NULL,
    [AltEmail]       VARCHAR (255)      NULL,
    [CCAlt]          CHAR (1)           NULL,
    [SendMethod]     VARCHAR (3)        NULL,
    [Comment]        VARCHAR (255)      NULL,
    [Password]       VARCHAR (25)       NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_CoContactInfo_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_CoContactInfo_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_CoContactInfo_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_CoContactInfo_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_CoContactInfo] PRIMARY KEY CLUSTERED ([ContactCode] ASC, [StudyYear] ASC, [ContactType] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_CoContactInfo_u]
	ON [stgFact].[CoContactInfo]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[CoContactInfo]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[CoContactInfo].ContactCode		= INSERTED.ContactCode
		AND	[stgFact].[CoContactInfo].StudyYear			= INSERTED.StudyYear
		AND	[stgFact].[CoContactInfo].ContactType		= INSERTED.ContactType;

END;