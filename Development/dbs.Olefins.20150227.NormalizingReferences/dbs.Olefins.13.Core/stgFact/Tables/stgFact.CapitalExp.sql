﻿CREATE TABLE [stgFact].[CapitalExp] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CptlCode]       VARCHAR (3)        NOT NULL,
    [Onsite]         REAL               NULL,
    [Offsite]        REAL               NULL,
    [Exist]          REAL               NULL,
    [EnergyCons]     REAL               NULL,
    [Envir]          REAL               NULL,
    [Safety]         REAL               NULL,
    [Computer]       REAL               NULL,
    [Maint]          REAL               NULL,
    [Total]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_CapitalExp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_CapitalExp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_CapitalExp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_CapitalExp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_CapitalExp] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CptlCode] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_CapitalExp_u]
	ON [stgFact].[CapitalExp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[CapitalExp]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[CapitalExp].Refnum		= INSERTED.Refnum
		AND	[stgFact].[CapitalExp].CptlCode		= INSERTED.CptlCode;

END;