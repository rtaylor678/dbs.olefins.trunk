﻿
/****** Object:  Stored Procedure dbo.sp_InsertMessage    Script Date: 4/18/2003 4:32:53 PM ******/

/****** Object:  Stored Procedure dbo.sp_InsertMessage    Script Date: 12/28/2001 7:34:24 AM ******/
/****** Object:  Stored Procedure dbo.sp_InsertMessage    Script Date: 05/08/2000 10:29:49 AM ******/
Create PROCEDURE [dbo].[spInsertMessage] @Refnum char(18), @MsgID int,
 @Source varchar(12), @P1 varchar(255) = '', @P2 varchar(255) = '',
 @P3 varchar(255) = '', @P4 varchar(255) = ''
AS
DECLARE @MsgStr varchar(255), @Severity char(1), @SysAdmin Bit,
 @Consultant Bit, @Pricing Bit, @Audience4 Bit, @Audience5 Bit,
 @Audience6 Bit, @Audience7 Bit
SELECT @MsgStr = MessageText, @Severity = Severity, @SysAdmin = SysAdmin,
	@Consultant = Consultant, @Pricing = Pricing, @Audience4 = Audience4,
	@Audience5 = Audience5, @Audience6 = Audience6, @Audience7 = Audience7
FROM Message_LU
WHERE MessageID = @MsgID
if CHARINDEX('@P1', @MsgStr) > 0
	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P1', @MsgStr), 3, @P1)
if CHARINDEX('@P2', @MsgStr) > 0

	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P2', @MsgStr), 3, @P2)
if CHARINDEX('@P3', @MsgStr) > 0
	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P3', @MsgStr), 3, @P3)
if CHARINDEX('@P4', @MsgStr) > 0
	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P4', @MsgStr), 3, @P4)
INSERT INTO MessageLog (Refnum, MessageID, Severity, Source, MessageText,
	SysAdmin, Consultant, Pricing, Audience4, Audience5,
	Audience6, Audience7)
VALUES (@Refnum, @MsgID, @Severity, @Source, @MsgStr, @SysAdmin,
	@Consultant, @Pricing, @Audience4, @Audience5, @Audience6,
	@Audience7)
