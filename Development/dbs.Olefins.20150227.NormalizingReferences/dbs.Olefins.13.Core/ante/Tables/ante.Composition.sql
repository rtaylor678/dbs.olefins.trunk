﻿CREATE TABLE [ante].[Composition] (
    [FactorSetId]         VARCHAR (12)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [StreamId]            VARCHAR (42)       NOT NULL,
    [ComponentId]         VARCHAR (42)       NOT NULL,
    [Component_Expansion] BIT                NOT NULL,
    [Component_WtPcnt]    REAL               NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_Composition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_Composition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_Composition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_Composition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Composition] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CalDateKey] ASC, [StreamId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_Composition_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_Composition_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Composition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Composition_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Composition_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_Composition_u]
	ON  [ante].[Composition]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [ante].[Composition]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[Composition].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[Composition].CalDateKey		= INSERTED.CalDateKey
		AND	[ante].[Composition].StreamId		= INSERTED.StreamId
		AND	[ante].[Composition].ComponentId	= INSERTED.ComponentId;

END;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Default component weight percent used to calculate composition.', @level0type = N'SCHEMA', @level0name = N'ante', @level1type = N'TABLE', @level1name = N'Composition';

