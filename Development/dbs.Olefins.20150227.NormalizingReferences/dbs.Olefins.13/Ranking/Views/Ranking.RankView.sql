﻿CREATE VIEW [Ranking].[RankView]
AS
SELECT     r.Refnum, ts.CoLoc, lu.RankVariable, lu.ListId, lu.ListName, lu.RankBreak, lu.BreakValue, r.[Value], r.Rank, s.DataCount, r.Percentile
                  , PercentRanking = CAST(CASE WHEN Rank = 1 THEN 0 WHEN Rank = DataCount THEN 100 ELSE 100-Percentile+0.5 END as int)
                  , r.Tile, s.NumTiles, v.Description AS Variable, lu.RankSpecsID, b.Description AS BreakCondition, s.LastTime
FROM         Ranking.Rank r 
INNER JOIN Ranking.RankSpecsLu lu ON r.RankSpecsID = lu.RankSpecsID 
INNER JOIN Ranking.RankSpecsStats s ON r.RankSpecsID = s.RankSpecsID 
INNER JOIN Ranking.RankBreaks b ON lu.RankBreak = b.RankBreak 
INNER JOIN Ranking.RankVariables v ON lu.RankVariable = v.RankVariable 
INNER JOIN fact.TSort ts ON r.Refnum = ts.Refnum
