﻿CREATE TABLE [Val].[OutlierStats] (
    [GroupId]      VARCHAR (15)  NOT NULL,
    [DataTable]    VARCHAR (50)  NOT NULL,
    [DataColumn]   VARCHAR (50)  NOT NULL,
    [Criteria]     VARCHAR (200) NOT NULL,
    [Key1]         VARCHAR (20)  NOT NULL,
    [Key2]         VARCHAR (20)  NOT NULL,
    [Key3]         VARCHAR (20)  NOT NULL,
    [Count]        REAL          NULL,
    [MinValue]     REAL          NULL,
    [AverageValue] REAL          NULL,
    [MaxValue]     REAL          NULL,
    [StdDev]       REAL          NULL,
    [StdDevToMIN]  REAL          NULL,
    [StdDevToMAX]  REAL          NULL,
    [tsModified]   SMALLDATETIME DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_OutlierStats] PRIMARY KEY CLUSTERED ([GroupId] ASC, [DataTable] ASC, [DataColumn] ASC, [Criteria] ASC, [Key1] ASC, [Key2] ASC, [Key3] ASC)
);

