﻿
CREATE PROCEDURE [dbo].[Insert_GapEnergySeparation]
(
	@Refnum			VARCHAR(25),
	@FactorSetId	VARCHAR(12)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [dbo].[GapEnergySeparation]
		(
			[Refnum],
			[DataType],
			[StandardEnergyTot],
			[PlantFeed],
			[FreshPyroFeed],
			[SuppTot],
			[SuppConc],
			[ConcEthylene],
			[ConcPropylene],
			[ConcBenzene],
			[ConcButadiene],
			[SuppROG],
			[ROGHydrogen],
			[ROGMethane],
			[ROGEthane],
			[ROGEthylene],
			[ROGPropane],
			[ROGPropylene],
			[ROGC4Plus],
			[ROGInerts],
			[SuppDil],
			[DilHydrogen],
			[DilMethane],
			[DilEthane],
			[DilEthylene],
			[DilPropane],
			[DilPropylene],
			[DilBenzene],
			[DilButadiene],
			[DilButane],
			[DilButylene],
			[DilMoGas],
			[SuppMisc],
			[SuppGasOil],
			[SuppWashOil],
			[SuppOther],
			[Vessel],
			[FracFeed],
			[TowerPyroGasHT],
			[Benzene],
			[PyroGasoline],
			[HydroPur],
			[Reduction],
			[RedPropylene],
			[RedPropyleneCG],
			[RedPropyleneRG],
			[RedEthylene],
			[RedEthyleneCG],
			[RedCrackedGasTrans]
		)
		SELECT
			p.[Refnum],
			p.[DataType],
			p.[StandardEnergyTot],
			p.[PlantFeed],
			p.[FreshPyroFeed],
			p.[SuppTot],
			p.[SuppConc],
			p.[ConcEthylene],
			p.[ConcPropylene],
			p.[ConcBenzene],
			p.[ConcButadiene],
			p.[SuppROG],
			p.[ROGHydrogen],
			p.[ROGMethane],
			p.[ROGEthane],
			p.[ROGEthylene],
			p.[ROGPropane],
			p.[ROGPropylene],
			p.[ROGC4Plus],
			p.[ROGInerts],
			p.[SuppDil],
			p.[DilHydrogen],
			p.[DilMethane],
			p.[DilEthane],
			p.[DilEthylene],
			p.[DilPropane],
			p.[DilPropylene],
			p.[DilBenzene],
			p.[DilButadiene],
			p.[DilButane],
			p.[DilButylene],
			p.[DilMoGas],
			p.[SuppMisc],
			p.[SuppGasOil],
			p.[SuppWashOil],
			p.[SuppOther],
			p.[Vessel],
			p.[FracFeed],
			p.[TowerPyroGasHT],
			p.[Benzene],
			p.[PyroGasoline],
			p.[HydroPur],
			p.[Reduction],
			p.[RedPropylene],
			p.[RedPropyleneCG],
			p.[RedPropyleneRG],
			p.[RedEthylene],
			p.[RedEthyleneCG],
			p.[RedCrackedGasTrans]
		FROM (
			SELECT
				a.[Refnum],
				a.[StandardEnergyId],
				c.[DataType],
				c.[Energy]
			FROM [calc].[EnergySeparationAggregate] a WITH (NOEXPAND)
			CROSS APPLY (VALUES
				('Btu_Lb', a.[EnergySeparation_BtuLb]),
				('MBTU', a.[EnergySeparation_MBTU])
				) c([DataType], [Energy])
			WHERE	a.[FactorSetId]	= @FactorSetId
				AND	a.[Refnum]		= @Refnum
				) u
		PIVOT (MAX(u.[Energy]) FOR u.[StandardEnergyId] IN
			(
				[StandardEnergyTot],
				[PlantFeed],
				[FreshPyroFeed],
				[SuppTot],
				[SuppConc],
				[ConcEthylene],
				[ConcPropylene],
				[ConcBenzene],
				[ConcButadiene],
				[SuppROG],
				[ROGHydrogen],
				[ROGMethane],
				[ROGEthane],
				[ROGEthylene],
				[ROGPropane],
				[ROGPropylene],
				[ROGC4Plus],
				[ROGInerts],
				[SuppDil],
				[DilHydrogen],
				[DilMethane],
				[DilEthane],
				[DilEthylene],
				[DilPropane],
				[DilPropylene],
				[DilBenzene],
				[DilButadiene],
				[DilButane],
				[DilButylene],
				[DilMoGas],
				[SuppMisc],
				[SuppGasOil],
				[SuppWashOil],
				[SuppOther],
				[Vessel],
				[FracFeed],
				[TowerPyroGasHT],
				[Benzene],
				[PyroGasoline],
				[HydroPur],
				[Reduction],
				[RedPropylene],
				[RedPropyleneCG],
				[RedPropyleneRG],
				[RedEthylene],
				[RedEthyleneCG],
				[RedCrackedGasTrans]
			)
		) p;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;


