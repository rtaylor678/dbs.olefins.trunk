﻿CREATE TABLE [dim].[Gap_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [GapId]          VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Gap_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Gap_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Gap_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Gap_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Gap_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Gap_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] DESC, [GapId] ASC),
    CONSTRAINT [CR_Gap_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Gap_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Gap_Parent_LookUp_Gap] FOREIGN KEY ([GapId]) REFERENCES [dim].[Gap_LookUp] ([GapId]),
    CONSTRAINT [FK_Gap_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Gap_LookUp] ([GapId]),
    CONSTRAINT [FK_Gap_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Gap_Parent] ([FactorSetId], [GapId])
);


GO

CREATE TRIGGER [dim].[t_Gap_Parent_u]
ON [dim].[Gap_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Gap_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Gap_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Gap_Parent].[GapId]			= INSERTED.[GapId];

END;
