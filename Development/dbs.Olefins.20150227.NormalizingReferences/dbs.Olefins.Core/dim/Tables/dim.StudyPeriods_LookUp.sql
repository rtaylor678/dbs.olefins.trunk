﻿CREATE TABLE [dim].[StudyPeriods_LookUp] (
    [CalDateKey]     INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_StudyPeriods_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_StudyPeriods_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_StudyPeriods_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_StudyPeriods_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StudyPeriods_LookUp] PRIMARY KEY CLUSTERED ([CalDateKey] ASC),
    CONSTRAINT [FK_StudyPeriods_LookUp_Calendar] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey])
);


GO

CREATE TRIGGER [dim].[t_StudyPeriods_LookUp_u]
	ON [dim].[StudyPeriods_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[StudyPeriods_LookUp]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[StudyPeriods_LookUp].[CalDateKey]		= INSERTED.[CalDateKey];
		
END;