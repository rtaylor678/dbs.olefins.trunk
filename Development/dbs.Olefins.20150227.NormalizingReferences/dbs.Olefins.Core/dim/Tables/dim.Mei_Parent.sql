﻿CREATE TABLE [dim].[Mei_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [MeiId]          VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Mei_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Mei_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Mei_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Mei_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Mei_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Mei_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [MeiId] ASC),
    CONSTRAINT [CR_Mei_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Mei_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Mei_Parent_LookUp_Mei] FOREIGN KEY ([MeiId]) REFERENCES [dim].[Mei_LookUp] ([MeiId]),
    CONSTRAINT [FK_Mei_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Mei_LookUp] ([MeiId]),
    CONSTRAINT [FK_Mei_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Mei_Parent] ([FactorSetId], [MeiId])
);


GO

CREATE TRIGGER [dim].[t_Mei_Parent_u]
ON [dim].[Mei_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Mei_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Mei_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Mei_Parent].[MeiId]	= INSERTED.[MeiId];

END;