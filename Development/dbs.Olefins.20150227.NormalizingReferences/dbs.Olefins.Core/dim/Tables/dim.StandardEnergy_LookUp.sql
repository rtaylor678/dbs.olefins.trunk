﻿CREATE TABLE [dim].[StandardEnergy_LookUp] (
    [StandardEnergyId]     VARCHAR (42)       NOT NULL,
    [StandardEnergyName]   NVARCHAR (84)      NOT NULL,
    [StandardEnergyDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_StandardEnergy_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_StandardEnergy_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_StandardEnergy_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_LookUp] PRIMARY KEY CLUSTERED ([StandardEnergyId] ASC),
    CONSTRAINT [CL_StandardEnergy_LookUp_StandardEnergyDetail] CHECK ([StandardEnergyDetail]<>''),
    CONSTRAINT [CL_StandardEnergy_LookUp_StandardEnergyId] CHECK ([StandardEnergyId]<>''),
    CONSTRAINT [CL_StandardEnergy_LookUp_StandardEnergyName] CHECK ([StandardEnergyName]<>''),
    CONSTRAINT [UK_StandardEnergy_LookUp_StandardEnergyDetail] UNIQUE NONCLUSTERED ([StandardEnergyDetail] ASC),
    CONSTRAINT [UK_StandardEnergy_LookUp_StandardEnergyName] UNIQUE NONCLUSTERED ([StandardEnergyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_StandardEnergy_LookUp_u]
	ON [dim].[StandardEnergy_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[StandardEnergy_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[StandardEnergy_LookUp].[StandardEnergyId]		= INSERTED.[StandardEnergyId];
		
END;
