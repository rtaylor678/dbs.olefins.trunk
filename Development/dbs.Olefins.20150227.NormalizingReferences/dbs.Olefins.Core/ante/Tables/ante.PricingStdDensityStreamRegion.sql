﻿CREATE TABLE [ante].[PricingStdDensityStreamRegion] (
    [FactorSetId]        VARCHAR (12)       NOT NULL,
    [RegionId]           VARCHAR (5)        NOT NULL,
    [CalDateKey]         INT                NOT NULL,
    [StreamId]           VARCHAR (42)       NOT NULL,
    [DensityStandard_SG] REAL               NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_PricingStdDensityStreamRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_PricingStdDensityStreamRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_PricingStdDensityStreamRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_PricingStdDensityStreamRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingStdDensityStreamRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [RegionId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingStdDensityStreamRegion_Dens] CHECK ([DensityStandard_SG]>(0.0)),
    CONSTRAINT [FK_PricingStdDensityStreamRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingStdDensityStreamRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingStdDensityStreamRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingStdDensityStreamRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_PricingStdDensityStreamRegion_u]
	ON [ante].[PricingStdDensityStreamRegion]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingStdDensityStreamRegion]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingStdDensityStreamRegion].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[PricingStdDensityStreamRegion].RegionId		= INSERTED.RegionId
		AND	[ante].[PricingStdDensityStreamRegion].CalDateKey	= INSERTED.CalDateKey
		AND	[ante].[PricingStdDensityStreamRegion].StreamId		= INSERTED.StreamId;

END;