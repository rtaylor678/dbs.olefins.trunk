﻿CREATE TABLE [fact].[GenPlantCommIntegration] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [StreamId]         VARCHAR (42)       NOT NULL,
    [Integration_Pcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantCommIntegration_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_GenPlantCommIntegration_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_GenPlantCommIntegration_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_GenPlantCommIntegration_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantCommIntegration] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantCommIntegration_Integration_Pcnt] CHECK ([Integration_Pcnt]>=(0.0) AND [Integration_Pcnt]<=(100.0)),
    CONSTRAINT [FK_GenPlantCommIntegration_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantCommIntegration_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_GenPlantCommIntegration_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantCommIntegration_u]
	ON [fact].[GenPlantCommIntegration]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantCommIntegration]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantCommIntegration].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlantCommIntegration].StreamId	= INSERTED.StreamId
		AND [fact].[GenPlantCommIntegration].CalDateKey	= INSERTED.CalDateKey;

END;