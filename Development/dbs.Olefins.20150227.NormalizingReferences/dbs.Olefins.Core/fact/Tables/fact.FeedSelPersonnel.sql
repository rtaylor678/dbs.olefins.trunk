﻿CREATE TABLE [fact].[FeedSelPersonnel] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PersId]         VARCHAR (34)       NOT NULL,
    [Plant_FTE]      REAL               NULL,
    [Corporate_FTE]  REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelPersonnel_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FeedSelPersonnel_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FeedSelPersonnel_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelPersonnel_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSelPersonnel] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PersId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FeedSelPersonnel_Corporate_FTE] CHECK ([Corporate_FTE]>=(0.0)),
    CONSTRAINT [CR_FeedSelPersonnel_Plant_FTE] CHECK ([Plant_FTE]>=(0.0)),
    CONSTRAINT [FK_FeedSelPersonnel_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSelPersonnel_Pers_LookUp] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_FeedSelPersonnel_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSelPersonnel_u]
	ON [fact].[FeedSelPersonnel]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSelPersonnel]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSelPersonnel].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSelPersonnel].PersId		= INSERTED.PersId
		AND [fact].[FeedSelPersonnel].CalDateKey	= INSERTED.CalDateKey;

END;