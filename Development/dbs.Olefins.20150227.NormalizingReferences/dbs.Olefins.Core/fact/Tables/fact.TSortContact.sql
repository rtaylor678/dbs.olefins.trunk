﻿CREATE TABLE [fact].[TSortContact] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [ContactTypeId]     VARCHAR (12)       NOT NULL,
    [NameFull]          NVARCHAR (128)     NOT NULL,
    [NameTitle]         NVARCHAR (128)     NULL,
    [AddressPOBox]      NVARCHAR (128)     NULL,
    [AddressStreet]     NVARCHAR (128)     NULL,
    [AddressCity]       NVARCHAR (128)     NULL,
    [AddressState]      NVARCHAR (128)     NULL,
    [AddressCountry]    NVARCHAR (128)     NULL,
    [AddressCountryId]  CHAR (3)           NULL,
    [AddressPostalCode] NVARCHAR (32)      NULL,
    [NumberVoice]       NVARCHAR (32)      NULL,
    [NumberFax]         NVARCHAR (32)      NULL,
    [NumberMobile]      NVARCHAR (32)      NULL,
    [eMail]             NVARCHAR (254)     NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_TSortContact_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_TSortContact_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_TSortContact_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_TSortContact_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_TSortContact] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ContactTypeId] ASC),
    CONSTRAINT [CL_TSortContact_AddressCity] CHECK ([AddressCity]<>''),
    CONSTRAINT [CL_TSortContact_AddressCountry] CHECK ([AddressCountry]<>''),
    CONSTRAINT [CL_TSortContact_AddressPOBox] CHECK ([AddressPOBox]<>''),
    CONSTRAINT [CL_TSortContact_AddressState] CHECK ([AddressState]<>''),
    CONSTRAINT [CL_TSortContact_AddressStreet] CHECK ([AddressStreet]<>''),
    CONSTRAINT [CL_TSortContact_eMail] CHECK ([eMail]<>''),
    CONSTRAINT [CL_TSortContact_NameFull] CHECK ([NameFull]<>''),
    CONSTRAINT [CL_TSortContact_NameTitle] CHECK ([NameTitle]<>''),
    CONSTRAINT [CL_TSortContact_NumberFax] CHECK ([NumberFax]<>''),
    CONSTRAINT [CL_TSortContact_NumberMobile] CHECK ([NumberMobile]<>''),
    CONSTRAINT [CL_TSortContact_NumberVoice] CHECK ([NumberVoice]<>''),
    CONSTRAINT [CL_TSortContact_PostalCode] CHECK ([AddressPostalCode]<>''),
    CONSTRAINT [FK_TSortContact_ContactType_LookUp] FOREIGN KEY ([ContactTypeId]) REFERENCES [dim].[ContactType_LookUp] ([ContactTypeId]),
    CONSTRAINT [FK_TSortContact_Country_LookUp] FOREIGN KEY ([AddressCountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_TSortContact_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_TSortContact_u]
	ON [fact].[TSortContact]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[TSortContact]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[TSortContact].[Refnum]			= INSERTED.[Refnum]
		AND	[fact].[TSortContact].[ContactTypeId]	= INSERTED.[ContactTypeId];

END;