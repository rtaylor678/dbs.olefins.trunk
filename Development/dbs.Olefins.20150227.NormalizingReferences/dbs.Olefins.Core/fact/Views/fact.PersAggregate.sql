﻿CREATE VIEW fact.PersAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	p.Refnum,
	p.CalDateKey,
	b.PersId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(p.Personnel_Count, 0.0)
		WHEN '-' THEN - ISNULL(p.Personnel_Count, 0.0)
		ELSE 0.0
		END)											[Personnel_Count],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(p.StraightTime_Hrs, 0.0)
		WHEN '-' THEN - ISNULL(p.StraightTime_Hrs, 0.0)
		ELSE 0.0
		END)											[StraightTime_Hrs],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(p.OverTime_Hrs, 0.0)
		WHEN '-' THEN - ISNULL(p.OverTime_Hrs, 0.0)
		ELSE 0.0
		END)											[OverTime_Hrs],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + p._Company_Hrs
		WHEN '-' THEN - p._Company_Hrs
		ELSE 0.0
		END)											[Company_Hrs],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(p.Contract_Hrs, 0.0)
		WHEN '-' THEN - ISNULL(p.Contract_Hrs, 0.0)
		ELSE 0.0
		END)											[Contract_Hrs],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + p._Tot_Hrs
		WHEN '-' THEN - p._Tot_Hrs
		ELSE 0.0
		END)											[Tot_Hrs],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + p._Company_Fte
		WHEN '-' THEN - p._Company_Fte
		ELSE 0.0
		END)											[Company_Fte],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + p._Contract_Fte
		WHEN '-' THEN - p._Contract_Fte
		ELSE 0.0
		END)											[Contract_Fte],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + p._Tot_Fte
		WHEN '-' THEN - p._Tot_Fte
		ELSE 0.0
		END)											[Tot_Fte],
	COUNT_BIG(*)										[IndexItems]
FROM dim.Pers_Bridge		b
INNER JOIN fact.Pers		p
	ON	p.PersId		= b.DescendantId
INNER JOIN fact.TSortClient	t
	ON	t.Refnum		= p.Refnum
	AND	t.CalDateKey	= p.CalDateKey
GROUP BY
	b.FactorSetId,
	p.Refnum,
	p.CalDateKey,
	b.PersId;
GO
CREATE UNIQUE CLUSTERED INDEX [UX_PersAggregate]
    ON [fact].[PersAggregate]([FactorSetId] ASC, [Refnum] ASC, [PersId] ASC, [CalDateKey] ASC);

