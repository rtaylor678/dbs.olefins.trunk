﻿CREATE VIEW [fact].[ReliabilityTAAggregate]
WITH SCHEMABINDING
AS
SELECT
	r.[Refnum],
	r.[CalDateKey],
	r.[SchedId],
	r.[StreamId],
	r.[CurrencyRpt],
	CONVERT(FLOAT, COUNT(r.[TrainId]))						[Train_Count],
	SUM(r.[Interval_Mnths]	* r.[Capacity_Pcnt]) / 100.0	[Interval_Mnths],
	SUM(r.[_Interval_Years]	* r.[Capacity_Pcnt]) / 100.0	[Interval_Years],
	SUM(r.[DownTime_Hrs]	* r.[Capacity_Pcnt]) / 100.0	[DownTime_Hrs],
	SUM(r.[_Ann_TurnAroundCost_kCur])						[Ann_TurnAroundCost_kCur],
	SUM(r.[_Ann_TurnAround_ManHrs])							[Ann_TurnAround_ManHrs],
	SUM(r.[_Ann_TurnAroundCost_kCur])
		/ SUM(r.[_Ann_TurnAround_ManHrs]) * 1000.0			[Ann_CostPerManHour_kCur],
	COUNT_BIG(*)											[IndexItems]
FROM [fact].[ReliabilityTA]	r
GROUP BY
	r.[Refnum],
	r.[CalDateKey],
	r.[SchedId],
	r.[StreamId],
	r.[CurrencyRpt]
HAVING	(SUM(r.[Interval_Mnths]	* r.[Capacity_Pcnt]) / 100.0		IS NOT NULL
	OR	SUM(r.[_Interval_Years]	* r.[Capacity_Pcnt]) / 100.0		IS NOT NULL
	OR	SUM(r.[DownTime_Hrs]		* r.[Capacity_Pcnt]) / 100.0	IS NOT NULL
	OR	SUM(r.[_Ann_TurnAroundCost_kCur])							IS NOT NULL
	OR	SUM(r.[_Ann_TurnAround_ManHrs])								IS NOT NULL
	OR	SUM(r.[_Ann_TurnAroundCost_kCur])
		/ SUM(r.[_Ann_TurnAround_ManHrs]) * 1000.0					IS NOT NULL);