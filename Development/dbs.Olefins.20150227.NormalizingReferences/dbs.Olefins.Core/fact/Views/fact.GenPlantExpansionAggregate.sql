﻿CREATE VIEW fact.GenPlantExpansionAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	e.Refnum,
	e.CalDateKey,
	b.AccountId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + e.CapacityAdded_kMT
		WHEN '-' THEN - e.CapacityAdded_kMT
		END
		)						[CapacityAdded_kMT]
FROM dim.Account_Bridge				b
INNER JOIN fact.GenPlantExpansion	e
	ON	e.AccountId = b.DescendantId
GROUP BY
	b.FactorSetId,
	e.Refnum,
	e.CalDateKey,
	b.AccountId;