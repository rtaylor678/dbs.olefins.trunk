﻿
CREATE VIEW fact.MaintOpExAggregate
WITH SCHEMABINDING
AS
SELECT
	  c.Refnum
	, ISNULL(c.AccountId, 'Maint')	[AccountId]
	, p.CurrencyRpt	
	, p.CalDateKey					[CalDateKeyPrev]
	, SUM(p.Amount_Cur)				[AmountPrev_Cur]
	, c.CalDateKey
	, SUM(c.Amount_Cur)				[Amount_Cur]
	, (SUM(p.Amount_Cur) + SUM(c.Amount_Cur)) / 2.0
									[AmountAvg_Cur]
FROM fact.OpEx c
INNER JOIN fact.OpEx p
	ON	p.Refnum = c.Refnum
	AND p.AccountId = c.AccountId
	AND p.CurrencyRpt = c.CurrencyRpt
	AND	p.CalDateKey = (c.CalDateKey - 10000)
GROUP BY
	  c.Refnum
	, c.CalDateKey
	, p.CalDateKey
	, p.CurrencyRpt
	, c.CurrencyRpt
	, ROLLUP(c.AccountId);
