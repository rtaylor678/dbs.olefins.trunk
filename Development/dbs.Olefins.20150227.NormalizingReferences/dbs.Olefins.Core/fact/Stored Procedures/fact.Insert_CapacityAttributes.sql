﻿CREATE PROCEDURE [fact].[Insert_CapacityAttributes]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.CapacityAttributes(Refnum, CalDateKey, Expansion_Pcnt, Expansion_Date, [CapAllow_Bit], CapLoss_Pcnt, StartUp_Year)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		
			, c.ExpansionPcnt * 100.0								[Expansion_Pcnt]
			, c.ExpansionDate										[Expansion_Date]
			, CASE WHEN UPPER(LEFT(c.CapAllow, 1)) IN('Y', 'X') THEN 1 ELSE 0 END
																	[CapAllow]
			, c.CapLossPcnt * 100.0									[CapLoss_Pcnt]
			, c.PlantStartUpDate									[StartUpYear]
		FROM stgFact.Capacity c
		INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
		WHERE t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;