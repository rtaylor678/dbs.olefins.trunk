﻿CREATE TABLE [Console].[Notes] (
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [ValidationNotes]         TEXT               NULL,
    [ConsultingOpportunities] TEXT               NULL,
    [ContinuingIssues]        TEXT               NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_Notes_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_Notes_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_Notes_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_Notes_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ValidationNotes] PRIMARY KEY CLUSTERED ([Refnum] ASC),
    CONSTRAINT [CL_Notes_Refnum] CHECK ([Refnum]<>'')
);


GO

CREATE TRIGGER [Console].[t_Notes_u]
	ON  [Console].[Notes]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [Console].[Notes]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[Console].[Notes].[Refnum]			= INSERTED.[Refnum];

END;