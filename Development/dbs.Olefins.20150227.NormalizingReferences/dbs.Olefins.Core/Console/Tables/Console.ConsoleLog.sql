﻿CREATE TABLE [Console].[ConsoleLog] (
    [ConsoleLogId]   INT                IDENTITY (1, 1) NOT NULL,
    [UserName]       NVARCHAR (20)      NOT NULL,
    [LogType]        NVARCHAR (20)      NOT NULL,
    [Message]        NVARCHAR (2000)    NOT NULL,
    [DateAdded]      DATETIME           NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ConsoleLog_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ConsoleLog_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ConsoleLog_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ConsoleLog_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ConsoleLog] PRIMARY KEY CLUSTERED ([ConsoleLogId] ASC),
    CONSTRAINT [CL_ConsoleLog_LogType] CHECK ([LogType]<>''),
    CONSTRAINT [CL_ConsoleLog_Message] CHECK ([Message]<>''),
    CONSTRAINT [CL_ConsoleLog_UserName] CHECK ([UserName]<>'')
);

