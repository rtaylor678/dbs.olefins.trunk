﻿CREATE TABLE [Console].[Consultants] (
    [Initials]       NVARCHAR (3)       NOT NULL,
    [ConsultantName] NVARCHAR (80)      NOT NULL,
    [StudyYear]      NVARCHAR (4)       NOT NULL,
    [StudyType]      NVARCHAR (10)      NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_Consultants_Active] DEFAULT ((0)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Consultants_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Consultants_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Consultants_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Consultants_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Consultants] PRIMARY KEY CLUSTERED ([Initials] ASC),
    CONSTRAINT [CL_Consultants_ConsultantName] CHECK ([ConsultantName]<>''),
    CONSTRAINT [CL_Consultants_Initials] CHECK ([Initials]<>''),
    CONSTRAINT [CL_Consultants_StudyType] CHECK ([StudyType]<>''),
    CONSTRAINT [CL_Consultants_StudyYear] CHECK ([StudyYear]<>'')
);


GO

CREATE TRIGGER [Console].[t_Consultants_u]
	ON  [Console].[Consultants]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [Console].[Consultants]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[Console].[Consultants].[Initials]			= INSERTED.[Initials];

END;