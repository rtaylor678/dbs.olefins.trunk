﻿CREATE TABLE [stgFact].[TADT] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [TrainId]        TINYINT            NOT NULL,
    [PlanInterval]   REAL               NULL,
    [PlanDT]         REAL               NULL,
    [ActInterval]    REAL               NULL,
    [ActDT]          REAL               NULL,
    [TACost]         REAL               NULL,
    [TAManHrs]       REAL               NULL,
    [TaDate]         DATETIME           NULL,
    [PctTotEthylCap] REAL               NULL,
    [TAMini]         CHAR (3)           NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_TADT_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_TADT_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_TADT_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_TADT_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_TADT] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TrainId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_TADT_u]
	ON [stgFact].[TADT]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[TADT]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[TADT].Refnum		= INSERTED.Refnum
		AND	[stgFact].[TADT].TrainId		= INSERTED.TrainId;

END;