﻿CREATE TABLE [stgFact].[Pricing] (
    [Year]            SMALLINT           NOT NULL,
    [PeriodID]        VARCHAR (10)       NOT NULL,
    [Region]          VARCHAR (4)        NOT NULL,
    [Ethane]          REAL               NULL,
    [Propane]         REAL               NULL,
    [EPMix]           REAL               NULL,
    [Butane]          REAL               NULL,
    [ISOBUTANE]       REAL               NULL,
    [MIXEDBUTANES]    REAL               NULL,
    [LtNaphtha]       REAL               NULL,
    [HeavyNGL]        REAL               NULL,
    [LPG]             REAL               NULL,
    [Raffinate]       REAL               NULL,
    [REFORM_NAPHTHA]  REAL               NULL,
    [ATMO_GASOIL]     REAL               NULL,
    [GasOil]          REAL               NULL,
    [KEROSENE]        REAL               NULL,
    [NRC]             REAL               NULL,
    [REGULAR_MOGAS]   REAL               NULL,
    [PREMIUM_MOGAS]   REAL               NULL,
    [OCTANE]          REAL               NULL,
    [DIESEL]          REAL               NULL,
    [RES_LO_S]        REAL               NULL,
    [RES_HI_S]        REAL               NULL,
    [NAPHTHA_GENERIC] REAL               NULL,
    [METHANE]         REAL               NULL,
    [Hydrogen]        REAL               NULL,
    [Acetylene]       REAL               NULL,
    [Ethylene]        REAL               NULL,
    [Propylene]       REAL               NULL,
    [PropyleneCG]     REAL               NULL,
    [PROPYLENE_REFY]  REAL               NULL,
    [PROPYLENE_ALKY]  REAL               NULL,
    [Butadiene]       REAL               NULL,
    [BUTYLENE_ALKY]   REAL               NULL,
    [Benzene]         REAL               NULL,
    [PYROGASOLINE_HY] REAL               NULL,
    [PYROGASOLINE_UT] REAL               NULL,
    [PyGasoil]        REAL               NULL,
    [PyFuelOil]       REAL               NULL,
    [NATURAL_GAS]     REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Pricing_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Pricing_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Pricing_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_Pricing_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Pricing] PRIMARY KEY CLUSTERED ([Year] ASC, [PeriodID] ASC, [Region] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Pricing_u]
	ON [stgFact].[Pricing]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Pricing]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Pricing].[Year]		= INSERTED.[Year]
		AND	[stgFact].[Pricing].PeriodID	= INSERTED.PeriodID
		AND	[stgFact].[Pricing].Region		= INSERTED.Region;

END;