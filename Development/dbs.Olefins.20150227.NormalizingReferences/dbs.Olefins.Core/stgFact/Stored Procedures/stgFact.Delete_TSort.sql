﻿CREATE PROCEDURE [stgFact].[Delete_TSort]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[TSort]
	WHERE [Refnum] = @Refnum;

END;