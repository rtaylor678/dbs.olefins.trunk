﻿CREATE PROCEDURE [stgFact].[Insert_PolyFacilities]
(
	@Refnum          VARCHAR (25),
	@PlantID         TINYINT,

	@Name            NVARCHAR (50) = NULL,
	@City            NVARCHAR (40) = NULL,
	@State           NVARCHAR (25) = NULL,
	@Country         NVARCHAR (30) = NULL,
	@CapMTD          REAL         = NULL,
	@CapKMTA         REAL         = NULL,
	@ReactorTrainCnt INT          = NULL,
	@ReactorStartup  SMALLINT     = NULL,
	@Product         VARCHAR (20) = NULL,
	@Technology      VARCHAR (20) = NULL,
	@ReactorResinPct REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PolyFacilities]([Refnum], [PlantID], [Name], [City], [State], [Country], [CapMTD], [CapKMTA], [ReactorTrainCnt], [ReactorStartup], [Product], [Technology], [ReactorResinPct])
	VALUES(@Refnum, @PlantID, @Name, @City, @State, @Country, @CapMTD, @CapKMTA, @ReactorTrainCnt, @ReactorStartup, @Product, @Technology, @ReactorResinPct);

END;