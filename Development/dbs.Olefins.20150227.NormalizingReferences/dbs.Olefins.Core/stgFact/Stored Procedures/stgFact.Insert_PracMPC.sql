﻿CREATE PROCEDURE [stgFact].[Insert_PracMPC]
(
    @Refnum     VARCHAR (25),

    @Tbl9020    TINYINT = NULL,
    @Tbl9021    TINYINT = NULL,
    @Tbl9022    TINYINT = NULL,
    @Tbl9023    TINYINT = NULL,
    @Tbl9024    TINYINT = NULL,
    @Tbl9025    TINYINT = NULL,
    @Tbl9026    TINYINT = NULL,
    @Tbl9027    TINYINT = NULL,
    @Tbl9028    TINYINT = NULL,
    @Tbl9029    TINYINT = NULL,
    @Tbl9030    REAL = NULL,
    @Tbl9031    REAL = NULL,
    @Tbl9032    REAL = NULL,
    @Tbl9033    TINYINT = NULL,
    @Tbl9034    TINYINT = NULL,
    @Tbl9035    SMALLINT = NULL,
    @Tbl9036    TINYINT = NULL,
    @Tbl9037    SMALLINT = NULL, 
    @Tot9020_28 TINYINT = NULL  
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PracMPC]([Refnum], [Tbl9020], [Tbl9021], [Tbl9022], [Tbl9023], [Tbl9024], [Tbl9025], [Tbl9026], [Tbl9027], [Tbl9028], [Tbl9029], [Tbl9030], [Tbl9031], [Tbl9032], [Tbl9033], [Tbl9034], [Tbl9035], [Tbl9036], [Tbl9037], [Tot9020_28])
	VALUES(@Refnum, @Tbl9020, @Tbl9021, @Tbl9022, @Tbl9023, @Tbl9024, @Tbl9025, @Tbl9026, @Tbl9027, @Tbl9028, @Tbl9029, @Tbl9030, @Tbl9031, @Tbl9032, @Tbl9033, @Tbl9034, @Tbl9035, @Tbl9036, @Tbl9037, @Tot9020_28);

END;