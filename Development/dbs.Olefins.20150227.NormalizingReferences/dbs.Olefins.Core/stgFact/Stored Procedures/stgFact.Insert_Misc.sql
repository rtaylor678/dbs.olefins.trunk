﻿CREATE PROCEDURE [stgFact].[Insert_Misc]
(
	@Refnum                VARCHAR (25),

	@EthPcntTot            REAL         = NULL,
	@ProPcntTot            REAL         = NULL,
	@ButPcntTot            REAL         = NULL,
	@OCCAbsence            REAL         = NULL,
	@MpsAbsence            REAL         = NULL,
	@SplFeedEnergy         REAL         = NULL,
	@LtFeedPriceBasis      REAL         = NULL,
	@OthLiqFeedName1       VARCHAR (25) = NULL,
	@OthLiqFeedPriceBasis1 REAL         = NULL,
	@OthLiqFeedName2       VARCHAR (25) = NULL,
	@OthLiqFeedPriceBasis2 REAL         = NULL,
	@OthLiqFeedName3       VARCHAR (25) = NULL,
	@OthLiqFeedPriceBasis3 REAL         = NULL,
	@TotCompOutage         REAL         = NULL,
	@PctFurnRelyLimit      REAL         = NULL,
	@ROG_CGCI              CHAR (1)     = NULL,
	@ROG_CGCD              CHAR (1)     = NULL,
	@ROG_C2RSF             CHAR (1)     = NULL,
	@ROG_C3RSF             CHAR (1)     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Misc]([Refnum], [EthPcntTot], [ProPcntTot], [ButPcntTot], [OCCAbsence], [MpsAbsence], [SplFeedEnergy], [LtFeedPriceBasis], [OthLiqFeedName1], [OthLiqFeedPriceBasis1], [OthLiqFeedName2], [OthLiqFeedPriceBasis2], [OthLiqFeedName3], [OthLiqFeedPriceBasis3], [TotCompOutage], [PctFurnRelyLimit], [ROG_CGCI], [ROG_CGCD], [ROG_C2RSF], [ROG_C3RSF])
	VALUES(@Refnum, @EthPcntTot, @ProPcntTot, @ButPcntTot, @OCCAbsence, @MpsAbsence, @SplFeedEnergy, @LtFeedPriceBasis, @OthLiqFeedName1, @OthLiqFeedPriceBasis1, @OthLiqFeedName2, @OthLiqFeedPriceBasis2, @OthLiqFeedName3, @OthLiqFeedPriceBasis3, @TotCompOutage, @PctFurnRelyLimit, @ROG_CGCI, @ROG_CGCD, @ROG_C2RSF, @ROG_C3RSF);

END;