﻿CREATE PROCEDURE [stgFact].[Delete_PolyOpEx]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PolyOpEx]
	WHERE [Refnum] = @Refnum;

END;