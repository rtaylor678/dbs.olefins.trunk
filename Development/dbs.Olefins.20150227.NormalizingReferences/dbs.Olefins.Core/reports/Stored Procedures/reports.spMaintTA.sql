﻿




CREATE              PROC [reports].[spMaintTA](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint, @Currency varchar(5)
SELECT  @Currency = 'USD'
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.MaintTA WHERE GroupId = @GroupId
INSERT into reports.MaintTA (GroupId
, Currency
, TAPlanInt_Month
, TAPlanDT_Hrs
, TAPlanDT_Days
, TAPlan_LossPcnt
, TAInt_Month
, TADT_Hrs
, TADT_Days
, TA_LossPcnt
, TACost_MUS
, TACost_PcntRV
, TACost_PerEDC
, TAWHrs
, TAAdjFactor
, TATimingRegInsp
, TATimingDiscInsp
, TATimingDeterioration
, TATimingLowDemand
, TATimingRegular
, TATimingCptl
, CritPathCompGas
, CritPathCompRefrig
, CritPathPyroFurnTle
, CritPathExchanger
, CritPathVessel
, CritPathFurn
, CritPathUtilities
, CritPathCritPathCapProj
, CritPathCritPathOther
, TAPrepHrs
, TAMechHrs
, TAStartupHrs
, TAMini
, TaAdj_Feed_Ratio
, TaAdj_Prod_Ratio)

SELECT GroupId = @GroupId
, Currency = @Currency
, TAPlanInt_Month = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAPlanInt_Month, d.EthyleneCpbyAvg_kMT)
, TAPlanDT_Hrs = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAPlanDT_Hrs, d.EthyleneCpbyAvg_kMT)
, TAPlanDT_Days = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAPlanDT_Days, d.EthyleneCpbyAvg_kMT)
, TAPlan_LossPcnt = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAPlan_LossPcnt, d.EthyleneCpbyAvg_kMT)
, TAInt_Month = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAInt_Month, d.EthyleneCpbyAvg_kMT)
, TADT_Hrs = [$(DbGlobal)].dbo.WtAvgNZ(ta.TADT_Hrs, d.EthyleneCpbyAvg_kMT)
, TADT_Days = [$(DbGlobal)].dbo.WtAvgNZ(ta.TADT_Days, d.EthyleneCpbyAvg_kMT)
, TA_LossPcnt = [$(DbGlobal)].dbo.WtAvgNZ(ta.TA_LossPcnt, d.EthyleneCpbyAvg_kMT)
, TACost_MUS = [$(DbGlobal)].dbo.WtAvgNZ(ta.TACost_MUS, d.EthyleneCpbyAvg_kMT)
, TACost_PcntRV = [$(DbGlobal)].dbo.WtAvgNZ(ta.TACost_PcntRV, d.RV_MUS)
, TACost_PerEDC = [$(DbGlobal)].dbo.WtAvgNZ(ta.TACost_PerEDC, d.kEdc)
, TAWHrs = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAWHrs, d.EthyleneCpbyAvg_kMT)
, TAAdjFactor = [$(DbGlobal)].dbo.WtAvg(ta.TAAdjFactor, d.OlefinsProd_kMT)
, TATimingRegInsp = [$(DbGlobal)].dbo.WtAvgNN(ta.TATimingRegInsp, 1.0)
, TATimingDiscInsp = [$(DbGlobal)].dbo.WtAvgNN(ta.TATimingDiscInsp, 1.0)
, TATimingDeterioration = [$(DbGlobal)].dbo.WtAvgNN(ta.TATimingDeterioration, 1.0)
, TATimingLowDemand = [$(DbGlobal)].dbo.WtAvgNN(ta.TATimingLowDemand, 1.0)
, TATimingRegular = [$(DbGlobal)].dbo.WtAvgNN(ta.TATimingRegular, 1.0)
, TATimingCptl = [$(DbGlobal)].dbo.WtAvgNN(ta.TATimingCptl, 1.0)
, CritPathCompGas = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathCompGas, 1.0)
, CritPathCompRefrig = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathCompRefrig, 1.0)
, CritPathPyroFurnTle = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathPyroFurnTle, 1.0)
, CritPathExchanger = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathExchanger, 1.0)
, CritPathVessel = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathVessel, 1.0)
, CritPathFurn = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathFurn, 1.0)
, CritPathUtilities = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathUtilities, 1.0)
, CritPathCritPathCapProj = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathCritPathCapProj, 1.0)
, CritPathCritPathOther = [$(DbGlobal)].dbo.WtAvgNN(ta.CritPathCritPathOther, 1.0)
, TAPrepHrs = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAPrepHrs, 1.0)
, TAMechHrs = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAMechHrs, 1.0)
, TAStartupHrs = [$(DbGlobal)].dbo.WtAvgNZ(ta.TAStartupHrs, 1.0)
, TAMini = [$(DbGlobal)].dbo.WtAvgNN(ta.TAMini, 1.0)
, TaAdj_Feed_Ratio = [$(DbGlobal)].dbo.WtAvgNN(ta.TaAdj_Feed_Ratio, 1.0)
, TaAdj_Prod_Ratio = [$(DbGlobal)].dbo.WtAvgNN(ta.TaAdj_Prod_Ratio, 1.0)
FROM @MyGroup r JOIN dbo.MaintTA ta on ta.Refnum=r.Refnum
JOIN dbo.Divisors d on d.Refnum=r.Refnum

SET NOCOUNT OFF
