﻿CREATE TABLE [Ranking].[RankBreaks] (
    [RankBreak]   VARCHAR (128) NOT NULL,
    [Description] VARCHAR (40)  NOT NULL,
    [Active]      CHAR (1)      CONSTRAINT [DF__RankBreak__Active] DEFAULT ('Y') NOT NULL,
    CONSTRAINT [PK_RankBreaks] PRIMARY KEY NONCLUSTERED ([RankBreak] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [UniqueBreakDesc] UNIQUE NONCLUSTERED ([Description] ASC) WITH (FILLFACTOR = 70)
);

