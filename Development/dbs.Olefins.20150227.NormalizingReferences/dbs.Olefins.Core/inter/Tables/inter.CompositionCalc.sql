﻿CREATE TABLE [inter].[CompositionCalc] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [SimModelId]        VARCHAR (12)       NOT NULL,
    [OpCondId]          VARCHAR (12)       NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [RecycleId]         TINYINT            NULL,
    [ComponentId]       VARCHAR (42)       NOT NULL,
    [Component_WtPcnt]  REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_CompositionCalc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_CompositionCalc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_CompositionCalc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_CompositionCalc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_CompositionCalc] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [StreamId] ASC, [StreamDescription] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_CompositionCalc_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_CompositionCalc_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_CompositionCalc_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_CompositionCalc_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_CompositionCalc_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_CompositionCalc_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [inter].[t_CompositionCalc_u]
	ON [inter].[CompositionCalc]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [inter].[CompositionCalc]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].[CompositionCalc].FactorSetId	= INSERTED.FactorSetId
		AND	[inter].[CompositionCalc].Refnum		= INSERTED.Refnum
		AND	[inter].[CompositionCalc].CalDateKey	= INSERTED.CalDateKey
		AND	[inter].[CompositionCalc].StreamId		= INSERTED.StreamId;

END