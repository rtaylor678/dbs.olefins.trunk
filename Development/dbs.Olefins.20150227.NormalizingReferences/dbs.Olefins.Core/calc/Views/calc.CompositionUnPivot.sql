﻿CREATE VIEW [calc].[CompositionUnPivot]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	p.CalDateKey,
	p.SimModelId,
	p.OpCondId,
	p.StreamId,
	p.StreamDescription,
	p.RecycleId,
	p.C2H4,
	p.C2H6,
	p.C3H6,
	p.C3H8,
	p.NBUTA,
	p.IBUTA,
	p.IB,
	p.B1,
	p.H2,
	p.CH4
FROM (
	SELECT
		c.FactorSetId,
		c.Refnum,
		c.CalDateKey,
		c.SimModelId,
		c.OpCondId,
		c.StreamId,
		c.StreamDescription,
		c.RecycleId,
		c.ComponentId,
		c.Component_WtPcnt
	FROM  calc.[CompositionStream] c
	WHERE	c.SimModelId = 'Plant'
		AND	c.OpCondId IN ('OSOP', 'PlantFeed')
		AND RIGHT(c.CalDateKey, 4) = 1231
		AND c.ComponentId IN (
			'C2H4',
			'C2H6',
			'C3H6',
			'C3H8',
			'NBUTA',
			'IBUTA',
			'IB',
			'B1',
			'H2',
			'CH4'
		)
	) u
	PIVOT(
	MAX(u.Component_WtPcnt) FOR u.ComponentId In
	(
		C2H4,
		C2H6,
		C3H6,
		C3H8,
		NBUTA,
		IBUTA,
		IB,
		B1,
		H2,
		CH4
	)
) p
WHERE	p.C2H4	IS NOT NULL
	OR	p.C2H6	IS NOT NULL
	OR	p.C3H6	IS NOT NULL
	OR	p.C3H8	IS NOT NULL
	OR	p.NBUTA	IS NOT NULL
	OR	p.IBUTA	IS NOT NULL
	OR	p.IB	IS NOT NULL
	OR	p.B1	IS NOT NULL
	OR	p.B1	IS NOT NULL
	OR	p.CH4	IS NOT NULL
	;