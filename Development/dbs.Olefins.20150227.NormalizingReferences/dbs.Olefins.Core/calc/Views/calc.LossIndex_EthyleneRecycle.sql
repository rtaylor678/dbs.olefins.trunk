﻿CREATE VIEW [calc].[LossIndex_EthyleneRecycle]
WITH SCHEMABINDING
AS
SELECT
	cs.FactorSetId,
	cs.Refnum,
	cs.SimModelId,
	cs.OpCondId,
	SUM(cs.Component_kMT) /	r.Quantity_kMT * 100.0		[EthyleneRecycle_WtPcnt],
	SUM(cs.Component_kMT) / ABS(qp.Quantity_kMT) * 100.0	[Loss_EthyleneRecycle_Prod_WtPcnt]
FROM calc.CompositionStream					cs
INNER JOIN fact.StreamQuantityAggregate		qp	WITH (NOEXPAND)
	ON	qp.FactorSetId = cs.FactorSetId
	AND	qp.Refnum = cs.Refnum
	AND	qp.StreamId = 'EthylenePG'
INNER JOIN fact.StreamQuantityAggregate		r	WITH (NOEXPAND)
	ON	r.FactorSetId = cs.FactorSetId
	AND	r.Refnum = cs.Refnum
	AND	r.StreamId = 'Recycle'
WHERE	cs.StreamId IN ('EthRec', 'ProRec', 'ButRec')
	AND cs.ComponentId = 'C2H4'
GROUP BY
	cs.FactorSetId,
	cs.Refnum,
	cs.SimModelId,
	cs.OpCondId,
	qp.Quantity_kMT,
	r.Quantity_kMT;