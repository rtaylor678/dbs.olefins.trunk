﻿CREATE PROCEDURE [calc].[Insert_Mei_Unreliability]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Mei(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MeiId, Indicator_Index, Indicator_PcntRv)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			'Unreliability',
			calc.MaxValue(mei.Unreliability_Index, ri.ReliabilityIndicator * 1.5 * rv.ReplacementLocation),
			calc.MaxValue(mei.Unreliability_PcntRv, ri.ReliabilityIndicator * 0.15)
		FROM @fpl									fpl
		INNER JOIN calc.MeiUnreliability			mei
			ON	mei.FactorSetId	= fpl.FactorSetId
			AND	mei.Refnum		= fpl.Refnum
			AND	mei.CalDateKey	= fpl.Plant_QtrDateKey
			AND	mei.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN calc.ReliabilityIndicator		ri
			ON	ri.FactorSetId	= fpl.FactorSetId
			AND	ri.Refnum		= fpl.Refnum
			AND	ri.CalDateKey	= fpl.Plant_QtrDateKey
			AND	ri.CurrencyRpt	= fpl.CurrencyRpt
		INNER JOIN calc.ReplacementValueAggregate	rv
			ON	rv.FactorSetId	= fpl.FactorSetId
			AND rv.Refnum		= fpl.Refnum
			AND rv.CalDateKey	= fpl.Plant_QtrDateKey
			AND rv.CurrencyRpt	= fpl.CurrencyRpt
			AND rv.ReplacementValueId  = 'UscgReplacement';

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END