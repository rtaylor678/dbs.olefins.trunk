﻿CREATE PROCEDURE [calc].[Insert_ApcIndexPyroFurnIndivid]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ApcIndexPyroFurnIndivid]([FactorSetId], [Refnum], [CalDateKey], [ApcId], [AbsenceCorrectionFactor], [OnLine_Pcnt], [Mpc_Value], [Apc_Index])
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			w.ApcId,
			c._AbsenceCorrectionFactor,
			COALESCE(o.OnLine_Pcnt, 0.0)						[OnLine_Pcnt],
			w.Mpc_Value,
			calc.MinValue(CONVERT(FLOAT, COALESCE(k.Controller_Count, 0)) / CONVERT(FLOAT, f.Unit_Count), 1.0) * w.Mpc_Value / c._AbsenceCorrectionFactor
																[Apc_Index]
		FROM @fpl												tsq
		INNER JOIN calc.ApcAbsenceCorrection					c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_QtrDateKey
		INNER JOIN ante.ApcWeighting							w
			ON	w.FactorSetId	= tsq.FactorSetId
			AND	w.ApcId			= 'ApcPyroFurnIndivid'
		LEFT OUTER JOIN fact.ApcControllers						k
			ON	k.Refnum		= tsq.Refnum
			AND	k.CalDateKey	= tsq.Plant_QtrDateKey
			AND	k.ApcId			= w.ApcId
		LEFT OUTER JOIN fact.ApcOnLinePcnt						o
			ON	o.Refnum		= tsq.Refnum
			AND	o.CalDateKey	= tsq.Plant_QtrDateKey
			AND	o.ApcId			= 'PyroFurn'
		LEFT OUTER JOIN calc.FacilitiesCountComplexity			f
			ON	f.FactorSetId	= tsq.FactorSetId
			AND	f.Refnum		= tsq.Refnum
			AND	f.FacilityId	= 'PyroFurn'
		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;