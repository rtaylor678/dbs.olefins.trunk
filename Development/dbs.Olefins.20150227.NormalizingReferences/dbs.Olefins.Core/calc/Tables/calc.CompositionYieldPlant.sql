﻿CREATE TABLE [calc].[CompositionYieldPlant] (
    [FactorSetId]             VARCHAR (12)       NOT NULL,
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [SimModelId]              VARCHAR (12)       NOT NULL,
    [OpCondId]                VARCHAR (12)       NOT NULL,
    [RecycleId]               INT                NOT NULL,
    [ComponentId]             VARCHAR (42)       NOT NULL,
    [Component_kMT]           REAL               NULL,
    [Component_WtPcnt]        REAL               NULL,
    [Component_Extrap_kMT]    REAL               NULL,
    [Component_Extrap_WtPcnt] REAL               NULL,
    [Component_Supp_kMT]      REAL               NULL,
    [_Component_Div_kMT]      AS                 (CONVERT([real],isnull([Component_Extrap_kMT],(0.0))-isnull([Component_Supp_kMT],(0.0)),(1))) PERSISTED,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_Calc_CompositionYieldPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_Calc_CompositionYieldPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_Calc_CompositionYieldPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_Calc_CompositionYieldPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CompositionYieldPlant] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC, [RecycleId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CompositionYieldPlant_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CompositionYieldPlant_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_CompositionYieldPlant_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CompositionYieldPlant_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_CompositionYieldPlant_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_calc_CompositionYieldPlant_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [calc].[t_Calc_CompositionYieldPlant_u]
    ON [calc].[CompositionYieldPlant]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[CompositionYieldPlant]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[CompositionYieldPlant].FactorSetId	= INSERTED.FactorSetId
		AND calc.[CompositionYieldPlant].Refnum			= INSERTED.Refnum
		AND calc.[CompositionYieldPlant].CalDateKey		= INSERTED.CalDateKey
		AND calc.[CompositionYieldPlant].SimModelId		= INSERTED.SimModelId
		AND calc.[CompositionYieldPlant].OpCondId		= INSERTED.OpCondId
		AND calc.[CompositionYieldPlant].RecycleId		= INSERTED.RecycleId
		AND calc.[CompositionYieldPlant].ComponentId	= INSERTED.ComponentId;

END