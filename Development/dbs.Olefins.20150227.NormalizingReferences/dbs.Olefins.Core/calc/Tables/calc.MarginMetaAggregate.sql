﻿CREATE TABLE [calc].[MarginMetaAggregate] (
    [FactorSetId]      VARCHAR (12)        NOT NULL,
    [Refnum]           VARCHAR (25)        NOT NULL,
    [CalDateKey]       INT                 NOT NULL,
    [CurrencyRpt]      VARCHAR (4)         NOT NULL,
    [MarginAnalysis]   VARCHAR (42)        NOT NULL,
    [MarginId]         VARCHAR (42)        NOT NULL,
    [Hierarchy]        [sys].[hierarchyid] NOT NULL,
    [Stream_Cur]       REAL                NULL,
    [OpEx_Cur]         REAL                NULL,
    [Amount_Cur]       REAL                NOT NULL,
    [TaAdj_Stream_Cur] REAL                NULL,
    [TaAdj_OpEx_Cur]   REAL                NULL,
    [TaAdj_Amount_Cur] REAL                NOT NULL,
    [tsModified]       DATETIMEOFFSET (7)  CONSTRAINT [DF_MarginMetaAggregate_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)      CONSTRAINT [DF_MarginMetaAggregate_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)      CONSTRAINT [DF_MarginMetaAggregate_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)      CONSTRAINT [DF_MarginMetaAggregate_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MarginMetaAggregate] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MarginAnalysis] ASC, [MarginId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MarginMetaAggregate_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MarginMetaAggregate_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_MarginMetaAggregate_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MarginMetaAggregate_Margin_LookUp] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_MarginMetaAggregate_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [calc].[t_MarginMetaAggregate_u]
	ON [calc].[MarginMetaAggregate]
	AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.MarginMetaAggregate
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MarginMetaAggregate.FactorSetId	= INSERTED.FactorSetId
		AND	calc.MarginMetaAggregate.Refnum			= INSERTED.Refnum
		AND calc.MarginMetaAggregate.CalDateKey		= INSERTED.CalDateKey
		AND calc.MarginMetaAggregate.CurrencyRpt	= INSERTED.CurrencyRpt
		AND calc.MarginMetaAggregate.MarginAnalysis	= INSERTED.MarginAnalysis
		AND calc.MarginMetaAggregate.MarginId		= INSERTED.MarginId;

END;
