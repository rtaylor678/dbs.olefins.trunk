﻿CREATE TABLE [calc].[CapacityUtilization] (
    [FactorSetId]                    VARCHAR (12)       NOT NULL,
    [Refnum]                         VARCHAR (25)       NOT NULL,
    [CalDateKey]                     INT                NOT NULL,
    [SchedId]                        CHAR (1)           NOT NULL,
    [StreamId]                       VARCHAR (42)       NOT NULL,
    [StreamAvg_kMT]                  REAL               NULL,
    [CapacityAvg_kMT]                REAL               NULL,
    [ComponentId]                    VARCHAR (42)       NOT NULL,
    [ProductionActual_kMT]           REAL               NOT NULL,
    [_UtilizationStream_Pcnt]        AS                 (CONVERT([real],case when [StreamAvg_kMT]<>(0.0) then ([ProductionActual_kMT]/[StreamAvg_kMT])*(100.0)  end,(1))) PERSISTED,
    [_UtilizationNominal_Pcnt]       AS                 (CONVERT([real],case when [CapacityAvg_kMT]<>(0.0) then ([ProductionActual_kMT]/[CapacityAvg_kMT])*(100.0)  end,(1))) PERSISTED,
    [Loss_kMT]                       REAL               NULL,
    [_TaAdj_UtilizationStream_Pcnt]  AS                 (CONVERT([real],case when [StreamAvg_kMT]<>(0.0) then (([ProductionActual_kMT]+isnull([Loss_kMT],(0.0)))/[StreamAvg_kMT])*(100.0)  end,(1))) PERSISTED,
    [_TaAdj_UtilizationNominal_Pcnt] AS                 (CONVERT([real],case when [CapacityAvg_kMT]<>(0.0) then (([ProductionActual_kMT]+isnull([Loss_kMT],(0.0)))/[CapacityAvg_kMT])*(100.0)  end,(1))) PERSISTED,
    [tsModified]                     DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityUtilization_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                 NVARCHAR (168)     CONSTRAINT [DF_CapacityUtilization_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                 NVARCHAR (168)     CONSTRAINT [DF_CapacityUtilization_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                  NVARCHAR (168)     CONSTRAINT [DF_CapacityUtilization_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CapacityUtilization] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SchedId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CapacityUtilization_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CapacityUtilization_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_CapacityUtilization_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CapacityUtilization_Schedule_LookUp] FOREIGN KEY ([SchedId]) REFERENCES [dim].[Schedule_LookUp] ([ScheduleId]),
    CONSTRAINT [FK_calc_CapacityUtilization_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_CapacityUtilization_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_CapacityUtilization_u
	ON  calc.CapacityUtilization
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.CapacityUtilization
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.CapacityUtilization.FactorSetId	= INSERTED.FactorSetId
		AND calc.CapacityUtilization.Refnum			= INSERTED.Refnum
		AND calc.CapacityUtilization.CalDateKey		= INSERTED.CalDateKey
		AND calc.CapacityUtilization.SchedId		= INSERTED.SchedId
		AND calc.CapacityUtilization.StreamId		= INSERTED.StreamId;
				
END