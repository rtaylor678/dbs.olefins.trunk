﻿CREATE TABLE [calc].[TaAdjPers] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [PersId]            VARCHAR (34)       NOT NULL,
    [Ann_Company_Pcnt]  REAL               NOT NULL,
    [Ann_Contract_Pcnt] REAL               NOT NULL,
    [_Ann_Tot_Pcnt]     AS                 (CONVERT([real],[Ann_Company_Pcnt]+[Ann_Contract_Pcnt],(1))) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_TaAdjPers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_TaAdjPers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_TaAdjPers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_TaAdjPers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_TaAdjPers] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [PersId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_TaAdjPers_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_TaAdjPers_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_TaAdjPers_Pers_LookUp] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_calc_TaAdjPers_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_TaAdjPers_u
	ON  calc.TaAdjPers
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.TaAdjPers
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.TaAdjPers.FactorSetId		= INSERTED.FactorSetId
		AND calc.TaAdjPers.Refnum			= INSERTED.Refnum
		AND calc.TaAdjPers.CalDateKey		= INSERTED.CalDateKey
		AND calc.TaAdjPers.PersId			= INSERTED.PersId;
			
END