﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SimulationControlUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStartStop = New System.Windows.Forms.Button()
        Me.lblInterval = New System.Windows.Forms.Label()
        Me.numInterval = New System.Windows.Forms.NumericUpDown()
        Me.lblCnString = New System.Windows.Forms.Label()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.lblLastQueue = New System.Windows.Forms.Label()
        Me.lvProcessing = New System.Windows.Forms.ListView()
        Me.cQueueID = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cModel = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cRefnum = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cFactorSet = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cStatus = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cDetail = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cDateTime = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.numThreadsPyps = New System.Windows.Forms.NumericUpDown()
        Me.lblThreadsPyps = New System.Windows.Forms.Label()
        Me.numThreadsSpsl = New System.Windows.Forms.NumericUpDown()
        Me.lblThreadsSpsl = New System.Windows.Forms.Label()
        Me.btnTerminate = New System.Windows.Forms.Button()
        Me.txtQueuedItemsPyps = New System.Windows.Forms.TextBox()
        Me.txtQueuedItemsSpsl = New System.Windows.Forms.TextBox()
        Me.lblQueuedItemsPyps = New System.Windows.Forms.Label()
        Me.lblQueuedItemsSpsl = New System.Windows.Forms.Label()
        Me.btnClearFolder = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnTerminateExcel = New System.Windows.Forms.Button()
        CType(Me.numInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numThreadsPyps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numThreadsSpsl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnStartStop
        '
        Me.btnStartStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStartStop.Location = New System.Drawing.Point(113, 28)
        Me.btnStartStop.Name = "btnStartStop"
        Me.btnStartStop.Size = New System.Drawing.Size(75, 23)
        Me.btnStartStop.TabIndex = 0
        Me.btnStartStop.Text = "Start"
        Me.btnStartStop.UseVisualStyleBackColor = True
        '
        'lblInterval
        '
        Me.lblInterval.AutoSize = True
        Me.lblInterval.Location = New System.Drawing.Point(194, 13)
        Me.lblInterval.Name = "lblInterval"
        Me.lblInterval.Size = New System.Drawing.Size(93, 13)
        Me.lblInterval.TabIndex = 2
        Me.lblInterval.Text = "Interval (Seconds)"
        '
        'numInterval
        '
        Me.numInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.numInterval.Location = New System.Drawing.Point(293, 11)
        Me.numInterval.Maximum = New Decimal(New Integer() {600, 0, 0, 0})
        Me.numInterval.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numInterval.Name = "numInterval"
        Me.numInterval.Size = New System.Drawing.Size(120, 20)
        Me.numInterval.TabIndex = 4
        Me.numInterval.Value = New Decimal(New Integer() {600, 0, 0, 0})
        '
        'lblCnString
        '
        Me.lblCnString.AutoSize = True
        Me.lblCnString.Location = New System.Drawing.Point(29, 54)
        Me.lblCnString.Name = "lblCnString"
        Me.lblCnString.Size = New System.Drawing.Size(39, 13)
        Me.lblCnString.TabIndex = 6
        Me.lblCnString.Text = "Label1"
        '
        'btnLogin
        '
        Me.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogin.Location = New System.Drawing.Point(20, 28)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(75, 23)
        Me.btnLogin.TabIndex = 3
        Me.btnLogin.Text = "SQL Login"
        Me.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLogin.UseVisualStyleBackColor = True
        '
        'lblLastQueue
        '
        Me.lblLastQueue.AutoSize = True
        Me.lblLastQueue.Location = New System.Drawing.Point(194, 38)
        Me.lblLastQueue.Name = "lblLastQueue"
        Me.lblLastQueue.Size = New System.Drawing.Size(69, 13)
        Me.lblLastQueue.TabIndex = 7
        Me.lblLastQueue.Text = "Last run at ..."
        '
        'lvProcessing
        '
        Me.lvProcessing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvProcessing.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.cQueueID, Me.cModel, Me.cRefnum, Me.cFactorSet, Me.cStatus, Me.cDetail, Me.cDateTime})
        Me.lvProcessing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvProcessing.FullRowSelect = True
        Me.lvProcessing.Location = New System.Drawing.Point(3, 79)
        Me.lvProcessing.MultiSelect = False
        Me.lvProcessing.Name = "lvProcessing"
        Me.lvProcessing.Size = New System.Drawing.Size(1115, 383)
        Me.lvProcessing.TabIndex = 10
        Me.lvProcessing.UseCompatibleStateImageBehavior = False
        Me.lvProcessing.View = System.Windows.Forms.View.Details
        '
        'cQueueID
        '
        Me.cQueueID.Text = "QueueID"
        Me.cQueueID.Width = 70
        '
        'cModel
        '
        Me.cModel.Text = "Model"
        Me.cModel.Width = 70
        '
        'cRefnum
        '
        Me.cRefnum.Text = "Refnum"
        Me.cRefnum.Width = 100
        '
        'cFactorSet
        '
        Me.cFactorSet.Text = "Factor Set"
        Me.cFactorSet.Width = 100
        '
        'cStatus
        '
        Me.cStatus.Text = "Status"
        Me.cStatus.Width = 100
        '
        'cDetail
        '
        Me.cDetail.Text = "Detail"
        Me.cDetail.Width = 450
        '
        'cDateTime
        '
        Me.cDateTime.Text = "Date and Time"
        Me.cDateTime.Width = 200
        '
        'numThreadsPyps
        '
        Me.numThreadsPyps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.numThreadsPyps.Location = New System.Drawing.Point(639, 11)
        Me.numThreadsPyps.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.numThreadsPyps.Name = "numThreadsPyps"
        Me.numThreadsPyps.Size = New System.Drawing.Size(40, 20)
        Me.numThreadsPyps.TabIndex = 12
        '
        'lblThreadsPyps
        '
        Me.lblThreadsPyps.AutoSize = True
        Me.lblThreadsPyps.Location = New System.Drawing.Point(520, 13)
        Me.lblThreadsPyps.Name = "lblThreadsPyps"
        Me.lblThreadsPyps.Size = New System.Drawing.Size(114, 13)
        Me.lblThreadsPyps.TabIndex = 2
        Me.lblThreadsPyps.Text = "PYPS Threads (Count)"
        '
        'numThreadsSpsl
        '
        Me.numThreadsSpsl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.numThreadsSpsl.Location = New System.Drawing.Point(639, 37)
        Me.numThreadsSpsl.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.numThreadsSpsl.Name = "numThreadsSpsl"
        Me.numThreadsSpsl.Size = New System.Drawing.Size(40, 20)
        Me.numThreadsSpsl.TabIndex = 12
        Me.numThreadsSpsl.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblThreadsSpsl
        '
        Me.lblThreadsSpsl.AutoSize = True
        Me.lblThreadsSpsl.Location = New System.Drawing.Point(520, 39)
        Me.lblThreadsSpsl.Name = "lblThreadsSpsl"
        Me.lblThreadsSpsl.Size = New System.Drawing.Size(113, 13)
        Me.lblThreadsSpsl.TabIndex = 2
        Me.lblThreadsSpsl.Text = "SPSL Threads (Count)"
        '
        'btnTerminate
        '
        Me.btnTerminate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTerminate.Location = New System.Drawing.Point(784, 32)
        Me.btnTerminate.Name = "btnTerminate"
        Me.btnTerminate.Size = New System.Drawing.Size(126, 23)
        Me.btnTerminate.TabIndex = 11
        Me.btnTerminate.Text = "Terminate Simulations"
        Me.btnTerminate.UseVisualStyleBackColor = True
        '
        'txtQueuedItemsPyps
        '
        Me.txtQueuedItemsPyps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtQueuedItemsPyps.Location = New System.Drawing.Point(731, 11)
        Me.txtQueuedItemsPyps.Name = "txtQueuedItemsPyps"
        Me.txtQueuedItemsPyps.ReadOnly = True
        Me.txtQueuedItemsPyps.Size = New System.Drawing.Size(30, 20)
        Me.txtQueuedItemsPyps.TabIndex = 13
        '
        'txtQueuedItemsSpsl
        '
        Me.txtQueuedItemsSpsl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtQueuedItemsSpsl.Location = New System.Drawing.Point(731, 36)
        Me.txtQueuedItemsSpsl.Name = "txtQueuedItemsSpsl"
        Me.txtQueuedItemsSpsl.ReadOnly = True
        Me.txtQueuedItemsSpsl.Size = New System.Drawing.Size(30, 20)
        Me.txtQueuedItemsSpsl.TabIndex = 13
        '
        'lblQueuedItemsPyps
        '
        Me.lblQueuedItemsPyps.AutoSize = True
        Me.lblQueuedItemsPyps.Location = New System.Drawing.Point(693, 14)
        Me.lblQueuedItemsPyps.Name = "lblQueuedItemsPyps"
        Me.lblQueuedItemsPyps.Size = New System.Drawing.Size(32, 13)
        Me.lblQueuedItemsPyps.TabIndex = 2
        Me.lblQueuedItemsPyps.Text = "Items"
        '
        'lblQueuedItemsSpsl
        '
        Me.lblQueuedItemsSpsl.AutoSize = True
        Me.lblQueuedItemsSpsl.Location = New System.Drawing.Point(694, 39)
        Me.lblQueuedItemsSpsl.Name = "lblQueuedItemsSpsl"
        Me.lblQueuedItemsSpsl.Size = New System.Drawing.Size(32, 13)
        Me.lblQueuedItemsSpsl.TabIndex = 2
        Me.lblQueuedItemsSpsl.Text = "Items"
        '
        'btnClearFolder
        '
        Me.btnClearFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearFolder.Location = New System.Drawing.Point(32, 124)
        Me.btnClearFolder.Name = "btnClearFolder"
        Me.btnClearFolder.Size = New System.Drawing.Size(126, 23)
        Me.btnClearFolder.TabIndex = 11
        Me.btnClearFolder.Text = "Clear Folder"
        Me.btnClearFolder.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1129, 491)
        Me.TabControl1.TabIndex = 14
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lvProcessing)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1121, 465)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Simulation"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.btnClear)
        Me.Panel1.Controls.Add(Me.lblCnString)
        Me.Panel1.Controls.Add(Me.btnLogin)
        Me.Panel1.Controls.Add(Me.numThreadsPyps)
        Me.Panel1.Controls.Add(Me.txtQueuedItemsSpsl)
        Me.Panel1.Controls.Add(Me.btnTerminate)
        Me.Panel1.Controls.Add(Me.lblThreadsPyps)
        Me.Panel1.Controls.Add(Me.numInterval)
        Me.Panel1.Controls.Add(Me.lblInterval)
        Me.Panel1.Controls.Add(Me.lblLastQueue)
        Me.Panel1.Controls.Add(Me.lblThreadsSpsl)
        Me.Panel1.Controls.Add(Me.numThreadsSpsl)
        Me.Panel1.Controls.Add(Me.txtQueuedItemsPyps)
        Me.Panel1.Controls.Add(Me.lblQueuedItemsPyps)
        Me.Panel1.Controls.Add(Me.lblQueuedItemsSpsl)
        Me.Panel1.Controls.Add(Me.btnStartStop)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1115, 76)
        Me.Panel1.TabIndex = 14
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(916, 14)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Location = New System.Drawing.Point(784, 8)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 23)
        Me.btnClear.TabIndex = 14
        Me.btnClear.Text = "Clear List"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnTerminateExcel)
        Me.TabPage2.Controls.Add(Me.btnClearFolder)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1121, 465)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Housekeeping"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnTerminateExcel
        '
        Me.btnTerminateExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTerminateExcel.Location = New System.Drawing.Point(32, 80)
        Me.btnTerminateExcel.Name = "btnTerminateExcel"
        Me.btnTerminateExcel.Size = New System.Drawing.Size(126, 23)
        Me.btnTerminateExcel.TabIndex = 12
        Me.btnTerminateExcel.Text = "Terminate Excel"
        Me.btnTerminateExcel.UseVisualStyleBackColor = True
        '
        'SimulationControlUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1153, 515)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "SimulationControlUI"
        Me.Text = "Olefins Simulation"
        CType(Me.numInterval, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numThreadsPyps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numThreadsSpsl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents btnStartStop As System.Windows.Forms.Button
    Private WithEvents lblInterval As System.Windows.Forms.Label
    Private WithEvents btnLogin As System.Windows.Forms.Button
    Private WithEvents numInterval As System.Windows.Forms.NumericUpDown
    Private WithEvents lblCnString As System.Windows.Forms.Label
    Private WithEvents lblLastQueue As System.Windows.Forms.Label
    Friend WithEvents lvProcessing As System.Windows.Forms.ListView
    Friend WithEvents cQueueID As System.Windows.Forms.ColumnHeader
    Friend WithEvents cModel As System.Windows.Forms.ColumnHeader
    Friend WithEvents cRefnum As System.Windows.Forms.ColumnHeader
    Friend WithEvents cStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents cDetail As System.Windows.Forms.ColumnHeader
    Friend WithEvents numThreadsPyps As System.Windows.Forms.NumericUpDown
    Private WithEvents lblThreadsPyps As System.Windows.Forms.Label
    Friend WithEvents cDateTime As System.Windows.Forms.ColumnHeader
    Friend WithEvents numThreadsSpsl As System.Windows.Forms.NumericUpDown
    Private WithEvents lblThreadsSpsl As System.Windows.Forms.Label
    Friend WithEvents btnTerminate As System.Windows.Forms.Button
    Friend WithEvents txtQueuedItemsPyps As System.Windows.Forms.TextBox
    Friend WithEvents txtQueuedItemsSpsl As System.Windows.Forms.TextBox
    Private WithEvents lblQueuedItemsPyps As System.Windows.Forms.Label
    Private WithEvents lblQueuedItemsSpsl As System.Windows.Forms.Label
    Friend WithEvents btnClearFolder As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnTerminateExcel As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cFactorSet As System.Windows.Forms.ColumnHeader
End Class
