﻿CREATE TABLE [xls].[StandardEnergy_PyrolysisBtu] (
    [Refnum]                   VARCHAR (12)       NOT NULL,
    [Energy_kBtu]              FLOAT (53)         NOT NULL,
    [HvcYieldDivisor_kLbDay]   FLOAT (53)         NOT NULL,
    [StandardEnergy_kBtuLbDay] FLOAT (53)         NOT NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]             ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_PyrolysisBtu] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_StandardEnergy_PyrolysisBtu_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_Energy_kBtu_MinIncl_0.0] CHECK ([Energy_kBtu]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_HvcYieldDivisor_kLbDay_MinIncl_0.0] CHECK ([HvcYieldDivisor_kLbDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_StandardEnergy_kBtuLbDay_MinIncl_0.0] CHECK ([StandardEnergy_kBtuLbDay]>=(0.0))
);


GO

CREATE TRIGGER [xls].[t_StandardEnergy_PyrolysisBtu_u]
	ON [xls].[StandardEnergy_PyrolysisBtu]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StandardEnergy_PyrolysisBtu]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StandardEnergy_PyrolysisBtu].[Refnum]	= INSERTED.[Refnum];

END;