﻿CREATE TABLE [xls].[FacilitiesElecGeneration] (
    [Refnum]         VARCHAR (12)       NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [Capacity_MW]    FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesElecGeneration_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesElecGeneration] PRIMARY KEY CLUSTERED ([Refnum] DESC, [FacilityId] ASC),
    CONSTRAINT [CL_FacilitiesElecGeneration_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_FacilitiesElecGeneration_Capacity_MW_MinIncl_0.0] CHECK ([Capacity_MW]>=(0.0)),
    CONSTRAINT [FK_FacilitiesElecGeneration_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId])
);


GO

CREATE TRIGGER [xls].[t_FacilitiesElecGeneration_u]
	ON [xls].[FacilitiesElecGeneration]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesElecGeneration]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesElecGeneration].[Refnum]		= INSERTED.[Refnum]
		AND	[xls].[FacilitiesElecGeneration].[FacilityId]	= INSERTED.[FacilityId];

END;