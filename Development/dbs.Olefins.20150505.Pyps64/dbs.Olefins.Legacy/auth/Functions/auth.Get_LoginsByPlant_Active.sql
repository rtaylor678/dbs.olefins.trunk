﻿CREATE FUNCTION [auth].[Get_LoginsByPlant_Active]
(
	@PlantId		INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		pbl.[JoinId],
		pbl.[PlantId],
		pbl.[LoginId],
		pbl.[PlantName],
		pbl.[LoginTag],
		pbl.[NameFirst],
		pbl.[NameLast],
		pbl.[eMail],
		pbl.[_NameComma],
		pbl.[_NameFull]
	FROM [auth].[LoginsByPlant_Active]	pbl WITH (NOEXPAND)
	WHERE	pbl.[PlantId]	= @PlantId
);