﻿CREATE PROCEDURE [audit].[Merge_Logging]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [audit].[Log_Fact] AS Target
	USING
	(
		VALUES
			(1, -1)
	)
	AS Source([LogChanges_Bit], [TruncateData_Days])
	ON	Target.[LogChanges_Bit]			= Source.[LogChanges_Bit]
	WHEN MATCHED THEN UPDATE SET
		Target.[TruncateData_Days]		= Source.[TruncateData_Days]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([LogChanges_Bit], [TruncateData_Days])
		VALUES([LogChanges_Bit], [TruncateData_Days])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	MERGE INTO [audit].[Log_Calc] AS Target
	USING
	(
		VALUES
			(1, -1)
	)
	AS Source([LogChanges_Bit], [TruncateData_Days])
	ON	Target.[LogChanges_Bit]			= Source.[LogChanges_Bit]
	WHEN MATCHED THEN UPDATE SET
		Target.[TruncateData_Days]		= Source.[TruncateData_Days]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([LogChanges_Bit], [TruncateData_Days])
		VALUES([LogChanges_Bit], [TruncateData_Days])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;
