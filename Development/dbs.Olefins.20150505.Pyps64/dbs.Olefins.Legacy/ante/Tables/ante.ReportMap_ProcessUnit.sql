﻿CREATE TABLE [ante].[ReportMap_ProcessUnit] (
    [MethodologyId]  INT                NOT NULL,
    [ProcessUnitId]  INT                NOT NULL,
    [Report_Suffix]  VARCHAR (42)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReportMap_ProcessUnit_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_ReportMap_ProcessUnit_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_ReportMap_ProcessUnit_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_ReportMap_ProcessUnit_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReportMap_ProcessUnit] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ProcessUnitId] ASC),
    CONSTRAINT [CL_ReportMap_ProcessUnit_Report_Suffix] CHECK ([Report_Suffix]<>''),
    CONSTRAINT [FK_ReportMap_ProcessUnit_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_ReportMap_ProcessUnit_ProcessUnit] FOREIGN KEY ([ProcessUnitId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])
);


GO

CREATE TRIGGER [ante].[t_ReportMap_ProcessUnit_u]
	ON [ante].[ReportMap_ProcessUnit]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ReportMap_ProcessUnit]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[ReportMap_ProcessUnit].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[ReportMap_ProcessUnit].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;