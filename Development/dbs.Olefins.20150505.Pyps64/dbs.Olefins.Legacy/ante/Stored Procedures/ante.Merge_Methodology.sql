﻿CREATE PROCEDURE [ante].[Merge_Methodology]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [ante].[Methodology] AS Target
	USING
	(
		VALUES
			('2013', '2013', '2013')
	)
	AS Source([MethodologyTag], [MethodologyName], [MethodologyDetail])
	ON	Target.[MethodologyTag]		= Source.[MethodologyTag]
	WHEN MATCHED THEN UPDATE SET
		Target.[MethodologyName]	= Source.[MethodologyName],
		Target.[MethodologyDetail]	= Source.[MethodologyDetail]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyTag], [MethodologyName], [MethodologyDetail])
		VALUES([MethodologyTag], [MethodologyName], [MethodologyDetail])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;