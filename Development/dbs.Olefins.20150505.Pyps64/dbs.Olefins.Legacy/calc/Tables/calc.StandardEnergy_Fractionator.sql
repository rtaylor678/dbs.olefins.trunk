﻿CREATE TABLE [calc].[StandardEnergy_Fractionator] (
    [MethodologyId]          INT                NOT NULL,
    [SubmissionId]           INT                NOT NULL,
    [Throughput_kMT]         FLOAT (53)         NOT NULL,
    [Convert_bblTon]         FLOAT (53)         NOT NULL,
    [Throughput_bbl]         FLOAT (53)         NOT NULL,
    [Throughput_bblDay]      FLOAT (53)         NOT NULL,
    [StandardEnergy_MBtuDay] FLOAT (53)         NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_Fractionator_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Fractionator_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Fractionator_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_Fractionator_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]           ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_Fractionator] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_StandardEnergy_Fractionator_Convert_bblTon_MinIncl_0.0] CHECK ([Convert_bblTon]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Fractionator_StandardEnergy_MBtuDay_MinIncl_0.0] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Fractionator_Throughput_bbl_MinIncl_0.0] CHECK ([Throughput_bbl]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Fractionator_Throughput_bblDay_MinIncl_0.0] CHECK ([Throughput_bblDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_Fractionator_Throughput_kMT_MinIncl_0.0] CHECK ([Throughput_kMT]>=(0.0)),
    CONSTRAINT [FK_StandardEnergy_Fractionator_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StandardEnergy_Fractionator_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_StandardEnergy_Fractionator_u]
	ON [calc].[StandardEnergy_Fractionator]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_Fractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_Fractionator].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_Fractionator].[SubmissionId]	= INSERTED.[SubmissionId];

END;