﻿CREATE TABLE [calc].[SupplementalRecovered] (
    [MethodologyId]     INT                NOT NULL,
    [SubmissionId]      INT                NOT NULL,
    [StreamId]          INT                NOT NULL,
    [ComponentId]       INT                NOT NULL,
    [Recovered_Dur_kMT] FLOAT (53)         NOT NULL,
    [Recovered_Ann_kMT] FLOAT (53)         NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_SupplementalRecovered_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_SupplementalRecovered_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_SupplementalRecovered_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_SupplementalRecovered_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SupplementalRecovered] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [ComponentId] ASC, [StreamId] ASC),
    CONSTRAINT [CR_SupplementalRecovered_Recovered_Ann_kMT_MinIncl_0.0] CHECK ([Recovered_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_SupplementalRecovered_Recovered_Dur_kMT_MinIncl_0.0] CHECK ([Recovered_Dur_kMT]>=(0.0)),
    CONSTRAINT [FK_SupplementalRecovered_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_SupplementalRecovered_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_SupplementalRecovered_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_SupplementalRecovered_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_SupplementalRecovered_u]
	ON [calc].[SupplementalRecovered]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[SupplementalRecovered]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[SupplementalRecovered].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[SupplementalRecovered].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[SupplementalRecovered].[ComponentId]	= INSERTED.[ComponentId];

END;