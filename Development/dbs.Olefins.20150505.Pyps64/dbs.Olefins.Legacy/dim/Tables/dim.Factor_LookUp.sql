﻿CREATE TABLE [dim].[Factor_LookUp] (
    [FactorId]       INT                IDENTITY (1, 1) NOT NULL,
    [FactorTag]      VARCHAR (42)       NOT NULL,
    [FactorName]     VARCHAR (84)       NOT NULL,
    [FactorDetail]   VARCHAR (256)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Factor_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Factor_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Factor_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Factor_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FactorTypes_LookUp] PRIMARY KEY CLUSTERED ([FactorId] ASC),
    CONSTRAINT [CL_Factor_LookUp_FactorDetail] CHECK ([FactorDetail]<>''),
    CONSTRAINT [CL_Factor_LookUp_FactorName] CHECK ([FactorName]<>''),
    CONSTRAINT [CL_Factor_LookUp_FactorTag] CHECK ([FactorTag]<>''),
    CONSTRAINT [UK_Factor_LookUp_FactorDetail] UNIQUE NONCLUSTERED ([FactorDetail] ASC),
    CONSTRAINT [UK_Factor_LookUp_FactorName] UNIQUE NONCLUSTERED ([FactorName] ASC),
    CONSTRAINT [UK_Factor_LookUp_FactorTag] UNIQUE NONCLUSTERED ([FactorTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_Factor_LookUp_u]
	ON [dim].[Factor_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Factor_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Factor_LookUp].[FactorId]	= INSERTED.[FactorId];

END;