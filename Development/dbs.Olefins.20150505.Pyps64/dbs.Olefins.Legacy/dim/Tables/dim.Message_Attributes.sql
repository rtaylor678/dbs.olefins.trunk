﻿CREATE TABLE [dim].[Message_Attributes] (
    [MessageId]      INT                NOT NULL,
    [SeverityId]     INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Message_Attributes_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Message_Attributes_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Message_Attributes_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Message_Attributes_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Message_Attributes] PRIMARY KEY CLUSTERED ([MessageId] ASC),
    CONSTRAINT [FK_Message_Attributes_Message_LookUp] FOREIGN KEY ([MessageId]) REFERENCES [dim].[Message_LookUp] ([MessageId]),
    CONSTRAINT [FK_Message_Attributes_Severity_LookUp] FOREIGN KEY ([SeverityId]) REFERENCES [dim].[Severity_LookUp] ([SeverityId])
);


GO

CREATE TRIGGER [dim].[t_Message_Attributes_u]
	ON [dim].[Message_Attributes]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Message_Attributes]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Message_Attributes].[MessageId]	= INSERTED.[MessageId];

END;