﻿CREATE TABLE [dim].[Factor_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [FactorId]           INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Factor_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Factor_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Factor_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Factor_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Factor_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Factor_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FactorId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_Factor_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_Factor_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Factor_Bridge_FactorId] FOREIGN KEY ([FactorId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_Factor_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Factor_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [FactorId]) REFERENCES [dim].[Factor_Parent] ([MethodologyId], [FactorId]),
    CONSTRAINT [FK_Factor_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[Factor_Parent] ([MethodologyId], [FactorId])
);


GO

CREATE TRIGGER [dim].[t_Factor_Bridge_u]
	ON [dim].[Factor_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Factor_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Factor_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Factor_Bridge].[FactorId]			= INSERTED.[FactorId]
		AND	[dim].[Factor_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;