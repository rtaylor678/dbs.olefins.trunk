﻿CREATE FUNCTION [dim].[Return_FeedClassId]
(
	@FeedClassTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	IF(ISNUMERIC(@FeedClassTag) = 1)
	BEGIN
		SELECT @Id = l.[FeedClassId]
		FROM [dim].[FeedClass_LookUp]	l
		WHERE l.[FeedClassTag] = @FeedClassTag;
	END
	ELSE
	BEGIN
		SELECT @Id = l.[FeedClassId]
		FROM [dim].[FeedClass_LookUp]	l
		WHERE l.[FeedClassName] = @FeedClassTag;
	END

	RETURN @Id;

END;