﻿CREATE FUNCTION [dim].[Return_ComponentId]
(
	@ComponentTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[ComponentId]
	FROM [dim].[Component_LookUp]	l
	WHERE l.[ComponentTag] = @ComponentTag;

	RETURN @Id;

END;