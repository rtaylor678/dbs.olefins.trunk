﻿CREATE TABLE [dbo].[FinancialTot] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [VarExpAmt]      REAL           NULL,
    [FixExpAmt]      REAL           NULL,
    [AnnTAExp]       REAL           NULL,
    [GAExp]          REAL           NULL,
    [Depreciation]   REAL           NULL,
    [Interest]       REAL           NULL,
    [GPV]            REAL           NULL,
    [RMC]            REAL           NULL,
    [GrossMargin]    REAL           NULL,
    [NEOpex]         REAL           NULL,
    [EnergyOpex]     REAL           NULL,
    [TotCash]        REAL           NULL,
    [TotNonCash]     REAL           NULL,
    [NetMargin]      REAL           NULL,
    [TotOpex]        REAL           NULL,
    [NetCashMargin]  REAL           NULL,
    [CashMfgCost]    REAL           NULL,
    [TotMfgCost]     REAL           NULL,
    [TotSal]         REAL           NULL,
    [TotBen]         REAL           NULL,
    [PPCFGCost]      REAL           NULL,
    [PPCOthFuelCost] REAL           NULL,
    CONSTRAINT [PK___OpexInd] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Variable Cash Operating Expense
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FinancialTot', @level2type = N'COLUMN', @level2name = N'VarExpAmt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fixed Cash Operating Expense
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FinancialTot', @level2type = N'COLUMN', @level2name = N'FixExpAmt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Annualized Turnaround Expense
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FinancialTot', @level2type = N'COLUMN', @level2name = N'AnnTAExp';

