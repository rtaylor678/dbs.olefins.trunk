﻿CREATE TABLE [dbo].[LocationFactors] (
    [CountryID] REAL         NOT NULL,
    [Country]   VARCHAR (50) NULL,
    [LOC_1999]  REAL         NULL,
    [LOC_2001]  REAL         NULL,
    [LOC_2003]  REAL         NULL,
    CONSTRAINT [PK_LocationFactors] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);

