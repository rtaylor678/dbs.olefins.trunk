﻿CREATE TABLE [dbo].[E_Score_LU] (
    [FieldName] VARCHAR (50) NULL,
    [SortOrder] CHAR (1)     NULL,
    [Category]  VARCHAR (10) NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'> is DESC and ASC is <', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'E_Score_LU', @level2type = N'COLUMN', @level2name = N'SortOrder';

