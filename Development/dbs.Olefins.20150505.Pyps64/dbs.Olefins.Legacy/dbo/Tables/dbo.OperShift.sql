﻿CREATE TABLE [dbo].[OperShift] (
    [Refnum]  [dbo].[Refnum] NOT NULL,
    [Task]    VARCHAR (10)   NOT NULL,
    [Console] REAL           NULL,
    [Outside] REAL           NULL,
    CONSTRAINT [PK___1__12] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Task] ASC)
);

