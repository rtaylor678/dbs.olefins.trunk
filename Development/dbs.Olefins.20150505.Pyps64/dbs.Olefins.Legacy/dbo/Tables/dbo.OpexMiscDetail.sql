﻿CREATE TABLE [dbo].[OpexMiscDetail] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [OpexID]      VARCHAR (6)    NOT NULL,
    [Description] VARCHAR (100)  NULL,
    [Amount]      REAL           NULL,
    CONSTRAINT [PK_OpexMiscDetail] PRIMARY KEY CLUSTERED ([Refnum] ASC, [OpexID] ASC)
);

