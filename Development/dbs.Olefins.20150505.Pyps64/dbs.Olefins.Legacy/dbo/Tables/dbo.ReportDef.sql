﻿CREATE TABLE [dbo].[ReportDef] (
    [ReportID]          INT                  IDENTITY (1, 1) NOT NULL,
    [ReportName]        VARCHAR (30)         NOT NULL,
    [Description]       VARCHAR (100)        NULL,
    [ReportDefSQL]      VARCHAR (255)        NULL,
    [ReportDefScenario] VARCHAR (8)          NULL,
    [ReportDefCurrency] [dbo].[CurrencyCode] NULL,
    CONSTRAINT [PK_ReportDef_1__14] PRIMARY KEY CLUSTERED ([ReportID] ASC),
    CONSTRAINT [UniqueReportName] UNIQUE NONCLUSTERED ([ReportName] ASC)
);

