﻿CREATE TABLE [dbo].[TotMaintCost] (
    [Refnum]            [dbo].[Refnum] NOT NULL,
    [DirMaintMatl]      REAL           NULL,
    [DirMaintLabor]     REAL           NULL,
    [DirTAMatl]         REAL           NULL,
    [DirTALabor]        REAL           NULL,
    [AllocOHMaintMatl]  REAL           NULL,
    [AllocOHMaintLabor] REAL           NULL,
    [AllocOHTAMatl]     REAL           NULL,
    [AllocOHTALabor]    REAL           NULL,
    [TotalMaintMatl]    REAL           NULL,
    [TotalMaintLabor]   REAL           NULL,
    [TotalTAMatl]       REAL           NULL,
    [TotalTALabor]      REAL           NULL,
    [DirMaintTot]       REAL           NULL,
    [DirTATot]          REAL           NULL,
    [AllocOHMaintTot]   REAL           NULL,
    [AllocOHTATot]      REAL           NULL,
    [TotRoutMaint]      REAL           NULL,
    [TotTAMaint]        REAL           NULL,
    CONSTRAINT [PK___1__13] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

