﻿

CREATE PROCEDURE spRankInsert;2
@Refnum Refnum,
@RankSpecsId integer,
@Rank smallint,
@Percentile real,
@Tile Tinyint,
@Value float
AS
INSERT Rank (Refnum, RankSpecsId, Rank, Percentile, Tile, Value)
VALUES (@Refnum, @RankSpecsId, @Rank, @Percentile, @Tile, @Value)

