﻿
/****** Object:  Stored Procedure dbo.procGetObjectID    Script Date: 4/18/2003 4:32:52 PM ******/

/****** Object:  Stored Procedure dbo.procGetObjectID    Script Date: 12/28/2001 7:34:24 AM ******/
/****** Object:  Stored Procedure dbo.procGetObjectID    Script Date: 04/13/2000 8:32:49 AM ******/
CREATE PROC procGetObjectID @ObjName varchar(30), @ObjID integer OUTPUT AS
SELECT @ObjID = ObjectID FROM DD_Objects WHERE ObjectName = @ObjName


