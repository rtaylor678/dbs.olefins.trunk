﻿CREATE PROCEDURE [dbo].[Insert_StreamCompositionMol]
(
	@SubmissionId			INT,
	@StreamNumber			INT,
	@ComponentId			INT,

	@Component_MolPcnt		FLOAT
)
AS
BEGIN

	IF(@Component_MolPcnt >= 0.0)
	EXECUTE [stage].[Insert_StreamCompositionMol] @SubmissionId, @StreamNumber, @ComponentId, @Component_MolPcnt;

END;
