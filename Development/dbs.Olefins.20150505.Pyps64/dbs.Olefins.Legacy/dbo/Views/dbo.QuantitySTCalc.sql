﻿
/****** Object:  View dbo.QuantitySTCalc    Script Date: 4/18/2003 4:32:50 PM ******/

/****** Object:  View dbo.QuantitySTCalc    Script Date: 12/28/2001 7:34:24 AM ******/
CREATE VIEW QuantitySTCalc AS 



SELECT a.Refnum, a.Category, Q1Feed = ISNULL(Q1Feed, 0), 
	Q2Feed = ISNULL(Q2Feed, 0), Q3Feed = ISNULL(Q3Feed, 0), 
	Q4Feed = ISNULL(Q4Feed, 0), AnnFeedProd = ISNULL(AnnFeedProd, 0)
FROM
((SELECT DISTINCT Refnum, Category 
FROM FeedProd_LU, TSort 
WHERE Category IS NOT NULL) a
LEFT JOIN (SELECT Refnum , Category , SUM(AnnFeedProd) as AnnFeedProd, 
	SUM(Q1Feed) AS Q1Feed, SUM(Q2Feed) AS Q2Feed,
	SUM(Q3Feed) AS Q3Feed, SUM(Q4Feed) AS Q4Feed
	
FROM Quantity q, FeedProd_LU lu
WHERE q.FeedProdID = lu.FeedProdID
GROUP BY Refnum, Category) b
ON a.Refnum = b.Refnum AND a.Category = b.Category)

