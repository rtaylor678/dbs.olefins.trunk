﻿
/****** Object:  View dbo._vEFurnAvg    Script Date: 7/26/2006 1:26:50 PM ******/
CREATE  VIEW [dbo].[vE_FurnAvg] 
AS
SELECT fc.Refnum, 'PctFurnMnlTest' = CONVERT(real, SUM(f.O2_Mnl_YN))/CONVERT(real, SUM(f.FurnCnt))
, CASE WHEN SUM(O2_Mnl_YN*Cap_MT_Yr*FurnCnt) > 0 THEN 
     SUM(ISNULL(f.O2_Mnl_Mol_Pct*Cap_MT_Yr*O2_Mnl_YN*FurnCnt,0))/SUM(O2_Mnl_YN*Cap_MT_Yr*FurnCnt)  ELSE 0 END AS 'ExcessO2MnlMolPct'
, CASE WHEN SUM(O2_Mnl_YN*Cap_MT_Yr*FurnCnt) > 0 THEN 
     SUM(ISNULL(O2_FreqMnl_Mnth*Cap_MT_Yr*O2_Mnl_YN*FurnCnt,0))/SUM(O2_Mnl_YN*Cap_MT_Yr*FurnCnt)  ELSE 0 END AS 'O2FreqMnlTestMnth'
, CASE WHEN SUM(FurnCnt) > 0 THEN 
     CONVERT(real, SUM(ISNULL(O2_Online,0))/SUM(FurnCnt)) ELSE 0 END AS 'O2OnlinePct'
, CASE WHEN SUM(O2_Online*Cap_MT_Yr) > 0 THEN 
     SUM(ISNULL(f.O2_Online*Cap_MT_Yr*O2_Online_Mol_Pct,0))/SUM(O2_Online*Cap_MT_Yr) ELSE 0 END AS 'O2ExcessOnlineMolPct'
, CASE WHEN SUM(f.O2_Online*Cap_MT_Yr*FurnCnt) > 0 THEN 
     SUM(ISNULL(O2_VarOnline_Mol_Pct*f.O2_Online*Cap_MT_Yr*FurnCnt,0))/SUM(O2_Online*Cap_MT_Yr*FurnCnt) ELSE 0 END AS 'ExcessO2VarOnlineMolPct'
, CASE WHEN SUM(f.O2_Online*Cap_MT_Yr) > 0 THEN 
     SUM(ISNULL(O2_UptimeOnline_Pct*f.O2_Online*Cap_MT_Yr,0))/SUM(O2_Online*Cap_MT_Yr)*.01 ELSE 0 END AS 'O2UptimeOnlinePct'
, CASE WHEN SUM(f.O2_Callibration_Hrs) > 0 THEN 
     SUM(ISNULL(CONVERT(real,O2_Callibration_Hrs)*O2_Online*Cap_MT_Yr,0))/SUM(O2_Online*Cap_MT_Yr) ELSE 0 END AS 'O2CalibrationHrs'
, CASE WHEN SUM(f.O2_CLC) > 0 THEN 
     SUM(ISNULL(CONVERT(real,O2_CLC),0))/SUM(FurnCnt) ELSE 0 END AS 'O2CLCFurnPct'
, CASE WHEN SUM(f.O2_CLC_Pct) > 0 THEN 
     SUM(ISNULL(f.O2_CLC_Pct*f.O2_CLC*Cap_MT_Yr,0))/SUM(O2_CLC*Cap_MT_Yr)*.01 ELSE 0 END AS 'O2CLCPct'
, CASE WHEN SUM(f.CO_Online) > 0 THEN 
     SUM(ISNULL(CONVERT(real,CO_Online),0))/SUM(FurnCnt) ELSE 0 END AS 'CO_OnlinePct'
, CASE WHEN SUM(f.CO_Uptime_Pct) > 0 THEN 
     SUM(ISNULL(f.CO_Uptime_Pct*f.CO_Online*Cap_MT_Yr,0))/SUM(f.CO_Online*Cap_MT_Yr)*.01 ELSE 0 END AS 'CO_UptimePct'
, CASE WHEN SUM(f.CO_Calibration_Hrs) > 0 THEN 
     SUM(ISNULL(CONVERT(real,f.CO_Calibration_Hrs)*f.CO_Online*Cap_MT_Yr,0))/SUM(f.CO_Online*Cap_MT_Yr) ELSE 0 END AS 'CO_CalibrationFreq'
, CASE WHEN SUM(f.SG) > 0 THEN 
     SUM(ISNULL(CONVERT(real,SG),0))/SUM(FurnCnt) ELSE 0 END AS 'SG'
, CASE WHEN SUM(f.SG_BFW_MBtu_hr) > 0 THEN 
     SUM(ISNULL(f.SG_BFW_MBtu_hr*f.SG*Cap_MT_Yr,0))/SUM(SG*Cap_MT_Yr)*.01 ELSE 0 END AS 'SG_HeatRecover'
, CASE WHEN SUM(f.SG_Combust) > 0 THEN 
     SUM(ISNULL(CONVERT(real,SG_Combust),0))/SUM(FurnCnt) ELSE 0 END AS 'SG_Combust'
, CASE WHEN SUM(f.SG_AirPreheat_MBtu_hr) > 0 THEN 
     SUM(ISNULL(f.SG_AirPreheat_MBtu_hr*f.SG_Combust*Cap_MT_Yr,0))/SUM(SG_Combust*Cap_MT_Yr)*.01 ELSE 0 END AS 'SG_AirPreheatMBtu_hr'
, CASE WHEN SUM(f.SG_Addl_YN) > 0 THEN 
     SUM(ISNULL(f.SG_Addl_YN*FurnCnt,0))/SUM(FurnCnt) ELSE 0 END AS 'SG_AddlHeatRecoverPct'
, CASE WHEN SUM(f.SG_Recover_MBtu_hr) > 0 THEN 
     SUM(ISNULL(f.SG_Recover_MBtu_hr*f.SG_Addl_YN*Cap_MT_Yr,0))/SUM(SG_Addl_YN*Cap_MT_Yr)*.01 ELSE 0 END AS 'SG_RecoverMBtu_hr'
, CASE WHEN SUM(f.SG_Temp_C) > 0 THEN 
     SUM(ISNULL(f.SG_Temp_C*f.FurnCnt*Cap_MT_Yr,0))/SUM(FurnCnt*Cap_MT_Yr) ELSE 0 END AS 'SG_Temp'
, CASE WHEN SUM(f.FP) > 0 THEN 
     SUM(ISNULL(CONVERT(real,FP),0))/SUM(FurnCnt) ELSE 0 END AS 'FP'
, CASE WHEN SUM(f.FP_Recover_MBtu_hr) > 0 THEN 
     SUM(ISNULL(f.FP_Recover_MBtu_hr*f.FP*Cap_MT_Yr,0))/SUM(FP*Cap_MT_Yr)*.01 ELSE 0 END AS 'FP_HeatRecoverMBtu_hr'
, CASE WHEN SUM(f.FP_Temp_C) > 0 THEN 
     SUM(ISNULL(f.FP_Temp_C*f.FurnCnt*Cap_MT_Yr,0))/SUM(FurnCnt*Cap_MT_Yr) ELSE 0 END AS 'FP_Temp'
, CASE WHEN SUM(f.TLE) > 0 THEN 
     SUM(ISNULL(CONVERT(real,TLE),0))/SUM(FurnCnt) ELSE 0 END AS 'TLE'
, CASE WHEN SUM(f.TLE_Mbtu_Hr) > 0 THEN 
     SUM(ISNULL(f.TLE_Mbtu_Hr*f.TLE*Cap_MT_Yr,0))/SUM(TLE*Cap_MT_Yr)*.01 ELSE 0 END AS 'TLE_HeatRecoverMBtu_hr'
FROM E_Furnaces f INNER JOIN E_FurnaceConfig fc ON fc.Refnum = f.Refnum
GROUP BY fc.Refnum



