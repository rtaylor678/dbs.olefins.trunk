﻿



/****** Object:  View dbo._vMaintCostTotals    Script Date: 4/18/2003 4:32:51 PM ******/

/****** Object:  View dbo._vMaintCostTotals    Script Date: 12/28/2001 7:34:24 AM ******/
CREATE             VIEW _vMaintCostTotals AS 


SELECT tmc.Refnum,  tmc.TotalMaintMatl, tmc.TotalMaintLabor, tmc.TotalTAMatl,
 tmc.TotalTALabor, tmc.TotRoutMaint, tmc.TotTAMaint, 
 TACost_PctRV = 100*mi.AnnAdjTACost/rv.TotReplVal/1000           
FROM _vTotMaintCost tmc 
 INNER JOIN MaintInd mi ON mi.Refnum = tmc.Refnum
 INNER JOIN Replacement rv ON mi.Refnum = rv.Refnum




