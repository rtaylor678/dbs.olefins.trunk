﻿CREATE TABLE [stage].[FacilitiesBoilers] (
    [SubmissionId]   INT                NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [Rate_kLbHr]     FLOAT (53)         NOT NULL,
    [Pressure_PSIg]  FLOAT (53)         NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesBoilers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesBoilers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesBoilers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesBoilers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesBoilers] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [FacilityId] ASC),
    CONSTRAINT [CR_FacilitiesBoilers_Pressure_PSIg_MinIncl_0.0] CHECK ([Pressure_PSIg]>=(0.0)),
    CONSTRAINT [CR_FacilitiesBoilers_Rate_kLbHr_MinIncl_0.0] CHECK ([Rate_kLbHr]>=(0.0)),
    CONSTRAINT [FK_FacilitiesBoilers_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesBoilers_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [stage].[t_FacilitiesBoilers_u]
	ON [stage].[FacilitiesBoilers]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[FacilitiesBoilers]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[FacilitiesBoilers].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[FacilitiesBoilers].[FacilityId]	= INSERTED.[FacilityId];

END;