﻿CREATE PROCEDURE [stage].[Insert_StreamRecovered]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@Recovered_WtPcnt		FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[StreamRecovered]([SubmissionId], [StreamNumber], [Recovered_WtPcnt])
	SELECT
		@SubmissionId,
		@StreamNumber,
		@Recovered_WtPcnt
	WHERE	@Recovered_WtPcnt >= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @StreamNumber:'		+ CONVERT(VARCHAR, @StreamNumber))
			+ COALESCE(', @Recovered_WtPcnt:'	+ CONVERT(VARCHAR, @Recovered_WtPcnt),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;