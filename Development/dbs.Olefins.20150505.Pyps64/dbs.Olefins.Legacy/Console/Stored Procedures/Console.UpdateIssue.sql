﻿

CREATE PROCEDURE [Console].[UpdateIssue] 

	@RefNum dbo.Refnum,
	@IssueTitle nvarchar(20),
	@Status nvarchar(1),
	@UpdatedBy nvarchar(3)

AS
BEGIN
DECLARE @UpdateDate datetime
SET @UpdateDate = GETDATE()

IF @Status = 'N'
	BEGIN
		SET @UpdatedBy = null
		SET @UpdateDate = null
	END
UPDATE ValStat
SET Completed = @Status, SetTime = @UpdateDate,SetBy = @UpdatedBy
Where Refnum = dbo.FormatRefNum(@RefNum,0) AND IssueID = @IssueTitle
END



