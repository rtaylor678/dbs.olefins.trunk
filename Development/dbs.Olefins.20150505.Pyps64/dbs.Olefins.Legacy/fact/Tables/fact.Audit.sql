﻿CREATE TABLE [fact].[Audit] (
    [SubmissionId]   INT                NOT NULL,
    [TimeBeg]        DATETIMEOFFSET (7) CONSTRAINT [DF_Audit_TimeBeg] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [TimeEnd]        DATETIMEOFFSET (7) NULL,
    [_Duration_mcs]  AS                 (datediff(microsecond,[TimeBeg],[TimeEnd])),
    [Error_Count]    INT                NULL,
    [Active]         BIT                CONSTRAINT [DF_Audit_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Audit_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Audit_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Audit_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Audit_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED ([SubmissionId] DESC),
    CONSTRAINT [CV_Audit_Times] CHECK ([TimeBeg]<=[TimeEnd]),
    CONSTRAINT [FK_Audit_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO
ALTER TABLE [fact].[Audit] NOCHECK CONSTRAINT [FK_Audit_Submissions];


GO

CREATE TRIGGER [fact].[Audit_LogChanges]
ON [fact].[Audit]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Insert	BIT = 0;
	DECLARE @Delete	BIT = 0;

	IF EXISTS (SELECT TOP 1 1 FROM inserted)	SET @Insert = 1;
	IF EXISTS (SELECT TOP 1 1 FROM deleted)		SET @Delete = 1;

	DECLARE @Action	CHAR(1) = '~';

	IF (@Insert = 1 AND @Delete = 0)			SET @Action = 'I';
	IF (@Insert = 1 AND @Delete = 1)			SET @Action = 'U';
	IF (@Insert = 0 AND @Delete = 1)			SET @Action = 'D';

	IF (@Action = 'U')
	UPDATE [fact].[Audit]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM inserted
	WHERE [fact].[Audit].[SubmissionId]	= INSERTED.[SubmissionId];

	DECLARE @Audit			BIT = 0;
	DECLARE @Truncate_Days	INT = -1;

	SELECT
		@Audit			= l.[LogChanges_Bit],
		@Truncate_Days	= l.[TruncateData_Days]
	FROM [audit].[Log_Fact]	l
	WHERE	l.[LogChanges_Bit]		= 1;

	IF (@Audit = 1)
	BEGIN

		IF (@Action IN ('I', 'U'))
		INSERT INTO [fact].[Shadow_Audit]([tsAction], [SubmissionId], [TimeBeg], [TimeEnd], [Error_Count], [Active])
		SELECT @Action, i.[SubmissionId], i.[TimeBeg], i.[TimeEnd], i.[Error_Count], i.[Active]
		FROM inserted i;

		IF (@Action = 'D')
		INSERT INTO [fact].[Shadow_Audit]([tsAction], [SubmissionId], [TimeBeg], [TimeEnd], [Error_Count], [Active])
		SELECT @Action, d.[SubmissionId], d.[TimeBeg], d.[TimeEnd], d.[Error_Count], d.[Active]
		FROM deleted d;

		DELETE FROM [fact].[Shadow_Audit]
		FROM [fact].[Shadow_Audit]		a
		WHERE	DATEDIFF(DAY, a.[tsModified], SYSDATETIMEOFFSET()) >= @Truncate_Days 
			AND	@Truncate_Days >= 0;

	END;

END;
