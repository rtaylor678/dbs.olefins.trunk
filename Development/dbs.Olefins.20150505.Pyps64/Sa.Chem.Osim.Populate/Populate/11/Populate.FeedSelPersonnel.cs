﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSelPersonnel(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string persId;

			Dictionary<string, int> feedSelPers = FeedSelPersRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + T11_01];
				
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSelPersonnel]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								persId = rdr.GetString(rdr.GetOrdinal("PersId"));

								if (feedSelPers.TryGetValue(persId, out r))
								{
									c = 7;
									AddRangeValues(wks, r, c, rdr, "Plant_FTE");
									
									c = 8;
									AddRangeValues(wks, r, c, rdr, "Corporate_FTE");
								}
							}
						}
					}
					cn.Close();
				};
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSelPersonnel", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSelPersonnel]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> FeedSelPersRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("BeaProdPlan", 130);
			d.Add("BeaProdPlanExperienced", 131);
			d.Add("BeaTrader", 132);
			d.Add("BeaAnalyst", 133);
			d.Add("BeaLogistics", 134);

			return d;
		}
	}
}