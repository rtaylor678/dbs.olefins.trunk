﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSel(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			Dictionary<string, RangeReference> feedSel = FeedSelDictionary();
			Dictionary<string, RangeReference> feedSelPcnt = FeedSelPcntDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSel]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								foreach (KeyValuePair<string, RangeReference> entry in feedSel)
								{
									wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
									r = entry.Value.row;
									c = entry.Value.col;
									AddRangeValues(wks, r, c, rdr, entry.Key);
								}

								foreach (KeyValuePair<string, RangeReference> entry in feedSelPcnt)
								{
									wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
									r = entry.Value.row;
									c = entry.Value.col;
									AddRangeValues(wks, r, c, rdr, entry.Key, 100.0);
								}

							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSel", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSel]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> FeedSelDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("MaxVaporPresFRS_RVP", new RangeReference(T11_01, 68, 8));

			d.Add("YieldPattern_Count", new RangeReference(T11_01, 103, 7));
			d.Add("YieldPatternLP_Count", new RangeReference(T11_01, 104, 7));
			d.Add("YieldGroup_Count", new RangeReference(T11_01, 105, 7));

			//	YPS_RPT
			d.Add("YPS_Lummus", new RangeReference(T11_01, 109, 21));
			d.Add("YPS_SPYRO", new RangeReference(T11_01, 109, 22));
			d.Add("YPS_SelfDev", new RangeReference(T11_01, 109, 23));
			d.Add("YPS_Other", new RangeReference(T11_01, 109, 24));
			d.Add("YPS_OtherName", new RangeReference(T11_01, 109, 6));

			//LP_RPT
			d.Add("LP_Aspen", new RangeReference(T11_01, 116, 21));
			d.Add("LP_Honeywell", new RangeReference(T11_01, 116, 22));
			d.Add("LP_Haverly", new RangeReference(T11_01, 116, 23));
			d.Add("LP_SelfDev", new RangeReference(T11_01, 116, 24));
			d.Add("LP_Other", new RangeReference(T11_01, 116, 25));
			d.Add("LP_OtherName", new RangeReference(T11_01, 116, 7));
			d.Add("LP_RowCount", new RangeReference(T11_01, 118, 7));

			//LP_DeltaBase_No	LP_DeltaBase_NotSure	LP_DeltaBase_RPT	LP_DeltaBase_Yes
			//LP_MixInt_No	LP_MixInt_NotSure	LP_MixInt_RPT	LP_MixInt_Yes
			//LP_Online_No	LP_Online_NotSure	LP_Online_RPT	LP_Online_Yes
			//LP_Recur_No	LP_Recur_NotSure	LP_Recur_RPT	LP_Recur_Yes

			//BeyondCrack_RPT
			d.Add("BeyondCrack_None", new RangeReference(T11_01, 126, 21));
			d.Add("BeyondCrack_C4", new RangeReference(T11_01, 126, 22));
			d.Add("BeyondCrack_C5", new RangeReference(T11_01, 126, 23));
			d.Add("BeyondCrack_Pygas", new RangeReference(T11_01, 126, 24));
			d.Add("BeyondCrack_BTX", new RangeReference(T11_01, 126, 25));
			d.Add("BeyondCrack_OthChem", new RangeReference(T11_01, 126, 26));
			d.Add("BeyondCrack_OthRefine", new RangeReference(T11_01, 126, 27));
			d.Add("BeyondCrack_Oth", new RangeReference(T11_01, 126, 28));
			d.Add("BeyondCrack_OtherName", new RangeReference(T11_01, 126, 10));

			d.Add("DecisionPlant_X", new RangeReference(T11_01, 135, 7));
			d.Add("DecisionCorp_X", new RangeReference(T11_01, 135, 8));
				
			d.Add("PlanTime_Days", new RangeReference(T11_01, 153, 6));
			d.Add("EffFeedChg_Levels", new RangeReference(T11_01, 165, 10));
			d.Add("FeedPurchase_Levels", new RangeReference(T11_01, 166, 10));

			return d;
		}

		private Dictionary<string, RangeReference> FeedSelPcntDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("MinParaffin_Pcnt", new RangeReference(T11_01, 67, 8));

			return d;
		}
	}
}