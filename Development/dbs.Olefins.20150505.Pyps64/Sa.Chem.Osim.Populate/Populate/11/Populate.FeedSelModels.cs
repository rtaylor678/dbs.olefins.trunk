﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSelModels(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string modelId;

			Dictionary<string, int> feedSelResx = FeedSeResourcesRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + T11_01];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSelModels]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								modelId = rdr.GetString(rdr.GetOrdinal("ModelId"));
								
								if(feedSelResx.TryGetValue(modelId, out r))
								{
									c = 7;
									AddRangeValues(wks, r, c, rdr, "Implemented_YN");

									c = 8;
									AddRangeValues(wks, r, c, rdr, "LocationID");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSelModels", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSelModels]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> FeedSeResourcesRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("LP", 91);
			d.Add("Scheduling", 92);
			d.Add("LPMultiPlant", 93);
			d.Add("LPMultiPeriod", 94);
			d.Add("PricingTrieredFeed", 95);
			d.Add("PricingTieredProd", 96);
			d.Add("NonLP", 97);
			d.Add("SpreadSheet", 98);
			d.Add("Margin", 99);
			d.Add("CaseStudies", 100);
			d.Add("BreakEven", 101);
			d.Add("YPS", 102);

			return d;
		}
	}
}