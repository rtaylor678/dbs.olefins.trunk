﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void EnergyCogenPcnt(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			const int c = 8;

			string accountId;

			Dictionary<string, int> accountRow = CoGenAccountRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "7"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_EnergyCogenPcnt]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("AccountID"));

								if(accountRow.TryGetValue(accountId, out r))
								{
									AddRangeValues(wks, r, c, rdr, "CogenEnergy_Pcnt", 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "EnergyCogenPcnt", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_EnergyCogenPcnt]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> CoGenAccountRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("PurSteam", 63);
			d.Add("PurElec", 65);

			return d;
		}
	}
}