﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void RogEntryPoint(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 3;

			Dictionary<string, int> rogEntry = RogEntryPointDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "2C"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ROGEntryPoint]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								foreach (KeyValuePair<string, int> entry in rogEntry)
								{
									r = entry.Value;
									AddRangeValues(wks, r, c, rdr, entry.Key);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "RogEntryPoint", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ROGEntryPoint]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> RogEntryPointDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("CompGasInlet_X", 53);
			d.Add("CompGasDischarge_X", 54);
			d.Add("C2Recovery_X", 55);
			d.Add("C3Recovery_X", 56);

			return d;
		}
	}
}