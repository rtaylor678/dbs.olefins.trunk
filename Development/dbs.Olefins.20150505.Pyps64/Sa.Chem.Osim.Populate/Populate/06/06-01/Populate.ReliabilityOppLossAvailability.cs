﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityOppLossAvailability(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string oppLossID;
			int currentYear;

			Dictionary<string, int> oppLossAvailRow = OppLossAvailRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-1"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityOppLoss_Availability]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								oppLossID = rdr.GetString(rdr.GetOrdinal("OppLossID"));

								if (oppLossAvailRow.TryGetValue(oppLossID, out r))
								{
									currentYear = rdr.GetInt32(rdr.GetOrdinal("CurrentYear"));

									AddRangeValues(wks, r, 6 + (currentYear * 2), rdr, "DownTimeLoss_Pcnt", 100.0);
									AddRangeValues(wks, r, 7 + (currentYear * 2), rdr, "SlowDownLoss_Pcnt", 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityOppLossAvailability", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityOppLoss_Availability]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> OppLossAvailRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Demand", 71);
			d.Add("ExtFeedInterrupt", 72);
			d.Add("CapProject", 74);

			return d;
		}
	}
}