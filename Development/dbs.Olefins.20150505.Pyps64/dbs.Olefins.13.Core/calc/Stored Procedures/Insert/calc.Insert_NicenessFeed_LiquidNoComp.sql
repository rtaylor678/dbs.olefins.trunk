﻿CREATE PROCEDURE [calc].[Insert_NicenessFeed_LiquidNoComp]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessFeed]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [Niceness], [NicenessYield], [NicenessYield_kMT], [NicenessEnergy], [NicenessEnergy_kMT])
		SELECT
			t.[FactorSetId],
			t.[Refnum],
			t.[Plant_AnnDateKey],
			t.[StreamId],
			t.[Quantity_kMT],
			t.[Niceness],
			CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[AllowMax] - t.[AllowMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [AllowMin]
				END	[Niceness_kMT],

			t.[Quantity_kMT] / CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[AllowMax] - t.[AllowMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [AllowMin]
				END	[NicenessYield_kMT],

			CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[EnergyMax] - t.[EnergyMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [EnergyMin]
				END	[NicenessEnergy],

			t.[Quantity_kMT] / CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[EnergyMax] - t.[EnergyMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [EnergyMin]
				END	[NicenessEnergy_kMT]

		FROM (
			SELECT
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey],
				sqa.[StreamId],
				sqa.[Quantity_kMT],
				fcp.[Density_SG] / nsc.[Coefficient]			[Niceness],
				nsc.[HistMin],
				nsc.[HistMax],
				nsc.[AllowMin],
				nsc.[AllowMax],
				nsc.[EnergyMin],
				nsc.[EnergyMax]
			FROM @fpl											fpl
			INNER JOIN [fact].[StreamQuantityAggregate]			sqa	WITH (NOEXPAND)
				ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
				AND	sqa.[Refnum]			= fpl.[Refnum]
			INNER JOIN [fact].[FeedStockCrackingParameters]		fcp
				ON	fcp.[Refnum]			= fpl.[Refnum]
				AND	fcp.[CalDateKey]		= fpl.[Plant_AnnDateKey]
				AND	fcp.[StreamId]			= sqa.[StreamId]
			INNER JOIN [ante].[NicenessStreamCoefficients]		nsc
				ON	nsc.[FactorSetId]		= fpl.[FactorSetId]
				AND	nsc.[StreamId]			= sqa.[StreamId]
			LEFT OUTER JOIN [calc].[CompositionStream]			c
				ON	c.[FactorSetId]			= fpl.[FactorSetId]
				AND	c.[Refnum]				= fpl.[Refnum]
				AND	c.[CalDateKey]			= fpl.[Plant_AnnDateKey]
				AND	c.[StreamId]			= sqa.[StreamId]
				AND	c.[ComponentId]			IN('P', 'I', 'A', 'N', 'O')
			WHERE	fpl.[CalQtr]			= 4
				AND	fpl.[FactorSet_AnnDateKey]	> 20130000
				AND	c.[ComponentId]			IS NULL
			) t;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;