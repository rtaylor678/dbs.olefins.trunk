﻿CREATE PROCEDURE calc.Insert_CapacityLostProdOpportunity
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.[CapacityLostProdOpportunity](FactorSetId, Refnum, CalDateKey, StreamId, StreamAvg_kMT, Production_kMT, Loss_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			c.StreamId,
			c._Ann_Stream_kMT			[EthyleneAvg_kMT],
			p.[ProductionSold_kMT]			[C2H4_kMT],
			SUM(l._TotLoss_kMT)			[Loss_kMT]
		FROM @fpl								tsq
		INNER JOIN calc.CapacityPlant			c
			ON	c.Refnum = tsq.Refnum
			AND c.CalDateKey = tsq.Plant_QtrDateKey
			AND c.StreamId = 'Ethylene'
		INNER JOIN calc.DivisorsProduction		p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
			AND	p.ComponentId = 'C2H4'
		INNER JOIN fact.ReliabilityOppLoss		l
			ON	l.Refnum = tsq.Refnum
			AND	l.CalDateKey = tsq.Plant_QtrDateKey
			AND	l.StreamId = c.StreamId
			AND l.StreamId = 'Ethylene'
		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			c.StreamId,
			c._Ann_Stream_kMT,
			p.[ProductionSold_kMT];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;