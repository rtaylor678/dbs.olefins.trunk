﻿CREATE PROCEDURE [calc].[Insert_Mei_Losses]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Mei(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MeiId, Indicator_Index, Indicator_PcntRv)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			'Losses',
			mei.Losses_Index,
			mei.Losses_PcntRv
		FROM @fpl									fpl
		INNER JOIN calc.MeiLosses					mei
			ON	mei.FactorSetId = fpl.FactorSetId
			AND	mei.Refnum = fpl.Refnum
			AND	mei.CalDateKey = fpl.Plant_QtrDateKey
			AND	mei.CurrencyRpt = fpl.CurrencyRpt;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END