﻿CREATE PROCEDURE [calc].[Insert_NicenessFeed_Liquid]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessFeed]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [Niceness], [NicenessYield], [NicenessYield_kMT], [NicenessEnergy], [NicenessEnergy_kMT])
		SELECT
			t.[FactorSetId],
			t.[Refnum],
			t.[Plant_AnnDateKey],
			t.[StreamId],
			t.[Component_kMT],
			t.[Niceness],
			CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[AllowMax] - t.[AllowMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [AllowMin]
				END	[Niceness_kMT],

			t.[Component_kMT] / CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[AllowMax] - t.[AllowMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [AllowMin]
				END	[NicenessYield_kMT],

			CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[EnergyMax] - t.[EnergyMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [EnergyMin]
				END	[NicenessEnergy],

			t.[Component_kMT] / CASE
				WHEN t.[Niceness] < 0.8 * t.[HistMin] THEN 1.0
				WHEN t.[Niceness] > 1.2 * t.[HistMax] THEN 2.0
				ELSE (t.[EnergyMax] - t.[EnergyMin]) / (t.[HistMax] - t.[HistMin]) * (t.[Niceness] - t.[HistMin]) + [EnergyMin]
				END	[NicenessEnergy_kMT]

		FROM (
			SELECT
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey],
				c.[StreamId],
				SUM(c.[Component_kMT])																	[Component_kMT],
				SUM(DISTINCT c.[Component_WtPcnt] * ncc.[Coefficient]) / nsc.[Coefficient] / 100.0		[Niceness],
				nsc.[HistMin],
				nsc.[HistMax],
				nsc.[AllowMin],
				nsc.[AllowMax],
				nsc.[EnergyMin],
				nsc.[EnergyMax]
			FROM @fpl											fpl
			INNER JOIN [calc].[CompositionStream]				c
				ON	c.[FactorSetId]		= fpl.[FactorSetId]
				AND	c.[Refnum]			= fpl.[Refnum]
				AND	c.[CalDateKey]		= fpl.[Plant_QtrDateKey]
				AND	c.[StreamId]		IN(SELECT d.[DescendantId] FROM [dim].[Stream_Bridge] d WHERE d.[FactorSetId] = c.[FactorSetId] AND d.[StreamId] = 'Liquid')
				AND	c.[StreamId]		<> 'FeedLiqOther'
				AND	c.[ComponentId]		IN('P', 'I', 'A', 'N', 'O')
			INNER JOIN [ante].[NicenessComponentCoefficients]		ncc
				ON	ncc.[FactorSetId]	= fpl.[FactorSetId]
				AND	ncc.[ComponentId]	= c.[ComponentId]
			INNER JOIN [ante].[NicenessStreamCoefficients]		nsc
				ON	nsc.[FactorSetId]	= fpl.[FactorSetId]
				AND	nsc.[StreamId]		= c.[StreamId]
			WHERE	fpl.[FactorSet_AnnDateKey]	> 20130000
			GROUP BY
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey],
				c.[StreamId],
				nsc.[Coefficient],
				nsc.[HistMin],
				nsc.[HistMax],
				nsc.[AllowMin],
				nsc.[AllowMax],
				nsc.[EnergyMin],
				nsc.[EnergyMax]
			) t;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;