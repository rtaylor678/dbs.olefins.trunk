﻿CREATE PROCEDURE [calc].[Insert_NicenessEnergy_Methane]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessEnergy]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [Stream_Pcnt], [NicenessYield_kMT], [NicenessYield_Pcnt], [NicenessYield_Tot], [NicenessEnergy_kMT], [NicenessEnergy_Pcnt], [NicenessEnergy_Tot])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'Methane',
			SUM(cs.[Component_kMT]),
			SUM(cs.[Component_kMT]) / nf.[Stream_kMT] * 100.0,
			0.0,
			0.0,
			SUM(cs.[Component_kMT]) / nf.[Stream_kMT] * co.[Yield],
			0.0,
			0.0,
			0.0
		FROM @fpl										fpl
		INNER JOIN [calc].[CompositionStream]			cs
			ON	cs.[FactorSetId]	= fpl.[FactorSetId]
			AND	cs.[Refnum]			= fpl.[Refnum]
			AND	cs.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	cs.[SimModelId]		= 'Plant'
			AND	cs.[OpCondId]		= 'PlantFeed'
			AND	cs.[ComponentId]	= 'CH4'
			AND	cs.[StreamId]		IN (SELECT d.[DescendantId] FROM dim.[Stream_Bridge] d WHERE d.[FactorSetId] = fpl.[FactorSetId] AND d.[StreamId] = 'Light')
		INNER JOIN(
			SELECT
				nf.[FactorSetId],
				nf.[Refnum],
				nf.[CalDateKey],
				SUM(nf.[Stream_kMT]) [Stream_kMT]
			FROM [calc].[NicenessFeed]					nf
			GROUP BY
				nf.[FactorSetId],
				nf.[Refnum],
				nf.[CalDateKey]
			) nf
			ON	nf.[FactorSetId]	= fpl.[FactorSetId]
			AND	nf.[Refnum]			= fpl.[Refnum]
			AND	nf.[CalDateKey]		= fpl.[Plant_AnnDateKey]
		INNER JOIN [ante].[NicenessModelCoefficients]		co
			ON	co.[FactorSetId]	= fpl.[FactorSetId]
			AND	co.[StreamId]		= 'Methane'
	
		GROUP BY
			fpl.[FactorSetId],
			fpl.Refnum,
			fpl.[Plant_AnnDateKey],
			nf.[Stream_kMT],
			co.[Yield];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;