﻿CREATE PROCEDURE [calc].[Insert_Efficiency_BoilFiredSteam]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Efficiency([FactorSetId], [Refnum], [CalDateKey], [EfficiencyId], [UnitId], [EfficiencyStandard])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			wCdu.[EfficiencyId],
			uef.[UnitId],
			uef.[Coefficient]
			* POWER(
				[calc].[MaxValue](
					[calc].[MinValue](
						uef.[ValueMax],
						SUM(fp.[Rate_kLbHr]) / CONVERT(REAL, SUM(fc.[Unit_Count]))
						),
					uef.[ValueMin]
					),
				uef.[Exponent])
			* SUM(fp.[Rate_kLbHr])
			* wCdu.[Coefficient] / 1000.0
		FROM @fpl										fpl
		INNER JOIN ante.UnitEfficiencyWWCdu					wCdu
			ON	wCdu.[FactorSetId]	= fpl.[FactorSetId]
		INNER JOIN ante.UnitEfficiencyFactors			uef
			ON	uef.[FactorSetId]	= fpl.[FactorSetId]
			AND	uef.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	uef.[UnitId]		= 'BoilFiredSteam'
		INNER JOIN [fact].[Facilities]					fc
			ON	fc.[Refnum]			= fpl.[Refnum]
			AND	fc.[CalDateKey]		= fpl.[Plant_AnnDateKey]
			AND	fc.[FacilityId]		IN ('BoilHP', 'BoilLP')
			AND	fc.[Unit_Count]		> 0
		INNER JOIN [fact].[FacilitiesPressure]			fp
			ON	fp.[Refnum]			= fpl.[Refnum]
			AND	fp.[CalDateKey]		= fpl.[Plant_AnnDateKey]
			AND	fp.[FacilityId]		= fc.[FacilityId]
		WHERE	fpl.CalQtr			= 4
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			uef.[UnitId],
			wCdu.[EfficiencyId],
			wCdu.[Coefficient],
			uef.[Coefficient],
			uef.[Exponent],
			uef.[ValueMax],
			uef.[ValueMin];
		
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;