﻿CREATE PROCEDURE calc.Insert_CapacityPlant
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.CapacityPlant(FactorSetId, Refnum, CalDateKey, StreamId, Study_Date, Expansion_Date, Expansion_Pcnt,
			Stream_MTd,
			Stream_kMT,
			Capacity_kMT
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			c.StreamId,
			tsq.Plant_QtrDate					[Study_Date],
			a.Expansion_Date,
			a.Expansion_Pcnt,
			c.Stream_MTd			* (-1.0)	[Stream_MTd],
			c.Stream_MTd * 0.365	* (-1.0)	[Stream_kMT],
			c.Capacity_kMT			* (-1.0)	[Capacity_kMT]
		FROM @fpl							tsq
		INNER JOIN fact.CapacityAggregate	c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN fact.CapacityAttributes	a
			ON	a.Refnum = c.Refnum
		WHERE	tsq.CalQtr = 4
			AND	c.StreamId <> 'Recycle';

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;