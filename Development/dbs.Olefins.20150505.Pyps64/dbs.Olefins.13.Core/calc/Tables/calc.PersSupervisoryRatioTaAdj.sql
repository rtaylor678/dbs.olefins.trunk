﻿CREATE TABLE [calc].[PersSupervisoryRatioTaAdj] (
    [FactorSetId]                  VARCHAR (12)       NOT NULL,
    [Refnum]                       VARCHAR (25)       NOT NULL,
    [CalDateKey]                   INT                NOT NULL,
    [PersIdSec]                    VARCHAR (22)       NOT NULL,
    [Occ_Count]                    REAL               NULL,
    [Mps_Count]                    REAL               NULL,
    [Occ_Fte]                      REAL               NULL,
    [Mps_Fte]                      REAL               NULL,
    [InflTaAdj_Occ_Fte]            REAL               NULL,
    [InflTaAdj_Mps_Fte]            REAL               NULL,
    [_SuperRatio]                  AS                 (CONVERT([real],case when [Mps_Count]<>(0.0) then [Occ_Count]/[Mps_Count]  end,(1))) PERSISTED,
    [_Supervisory_Ratio]           AS                 (CONVERT([real],case when [Mps_Fte]<>(0.0) then [Occ_Fte]/[Mps_Fte]  end,(1))) PERSISTED,
    [_InflTaAdj_Supervisory_Ratio] AS                 (CONVERT([real],case when [InflTaAdj_Mps_Fte]<>(0.0) then [InflTaAdj_Occ_Fte]/[InflTaAdj_Mps_Fte]  end,(1))) PERSISTED,
    [tsModified]                   DATETIMEOFFSET (7) CONSTRAINT [DF_PersSupervisoryRatioTaAdj_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]               NVARCHAR (168)     CONSTRAINT [DF_PersSupervisoryRatioTaAdj_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]               NVARCHAR (168)     CONSTRAINT [DF_PersSupervisoryRatioTaAdj_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                NVARCHAR (168)     CONSTRAINT [DF_PersSupervisoryRatioTaAdj_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PersSupervisoryRatioTaAdj] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [PersIdSec] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PersSupervisoryRatioTaAdj_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PersSupervisoryRatioTaAdj_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PersSupervisoryRatioTaAdj_PersSec_LookUp_LookUp] FOREIGN KEY ([PersIdSec]) REFERENCES [dim].[PersSec_LookUp] ([PersSecId]),
    CONSTRAINT [FK_calc_PersSupervisoryRatioTaAdj_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_PersSupervisoryRatioTaAdj_u
	ON  calc.PersSupervisoryRatioTaAdj
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.PersSupervisoryRatioTaAdj
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PersSupervisoryRatioTaAdj.FactorSetId	= INSERTED.FactorSetId
		AND calc.PersSupervisoryRatioTaAdj.Refnum		= INSERTED.Refnum
		AND calc.PersSupervisoryRatioTaAdj.CalDateKey	= INSERTED.CalDateKey
		AND calc.PersSupervisoryRatioTaAdj.PersIdSec	= INSERTED.PersIdSec;
	
END