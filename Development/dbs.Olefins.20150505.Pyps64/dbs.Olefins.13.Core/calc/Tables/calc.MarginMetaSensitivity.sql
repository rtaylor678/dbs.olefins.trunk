﻿CREATE TABLE [calc].[MarginMetaSensitivity] (
    [FactorSetId]                 VARCHAR (12)       NOT NULL,
    [Refnum]                      VARCHAR (25)       NOT NULL,
    [CalDateKey]                  INT                NOT NULL,
    [CurrencyRpt]                 VARCHAR (4)        NOT NULL,
    [MarginAnalysis]              VARCHAR (42)       NOT NULL,
    [MarginId]                    VARCHAR (42)       NOT NULL,
    [Amount_Cur]                  REAL               NOT NULL,
    [Amount_SensEDC]              REAL               NULL,
    [Amount_SensProd_MT]          REAL               NULL,
    [Amount_SensProd_Cents]       REAL               NULL,
    [TaAdj_Amount_Cur]            REAL               NOT NULL,
    [TAAdj_Amount_SensEDC]        REAL               NULL,
    [TaAdj_Amount_SensProd_MT]    REAL               NULL,
    [TAAdj_Amount_SensProd_Cents] REAL               NULL,
    [tsModified]                  DATETIMEOFFSET (7) CONSTRAINT [DF_MarginMetaSensitivity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]              NVARCHAR (168)     CONSTRAINT [DF_MarginMetaSensitivity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]              NVARCHAR (168)     CONSTRAINT [DF_MarginMetaSensitivity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]               NVARCHAR (168)     CONSTRAINT [DF_MarginMetaSensitivity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MarginMetaSensitivity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MarginAnalysis] ASC, [MarginId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MarginMetaSensitivity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MarginMetaSensitivity_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_MarginMetaSensitivity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MarginMetaSensitivity_Margin_LookUp] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_MarginMetaSensitivity_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [calc].[t_MarginMetaSensitivity_u]
	ON [calc].[MarginMetaSensitivity]
	AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.MarginMetaSensitivity
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MarginMetaSensitivity.FactorSetId		= INSERTED.FactorSetId
		AND	calc.MarginMetaSensitivity.Refnum			= INSERTED.Refnum
		AND calc.MarginMetaSensitivity.CalDateKey		= INSERTED.CalDateKey
		AND calc.MarginMetaSensitivity.CurrencyRpt		= INSERTED.CurrencyRpt
		AND calc.MarginMetaSensitivity.MarginAnalysis	= INSERTED.MarginAnalysis
		AND calc.MarginMetaSensitivity.MarginId			= INSERTED.MarginId;

END;
