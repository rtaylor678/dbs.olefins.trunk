﻿CREATE TABLE [calc].[MaintExpenseOccSwb] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MaintExpenseOccSwb_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MaintExpenseOccSwb_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MaintExpenseOccSwb_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MaintExpenseOccSwb_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MaintExpenseOccSwb] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MaintExpenseOccSwb_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_MaintExpenseOccSwb_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MaintExpenseOccSwb_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_MaintExpenseOccSwb_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MaintExpenseOccSwb_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_MaintExpenseOccSwb_u
	ON  calc.MaintExpenseOccSwb
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.MaintExpenseOccSwb
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MaintExpenseOccSwb.FactorSetId		= INSERTED.FactorSetId
		AND calc.MaintExpenseOccSwb.Refnum			= INSERTED.Refnum
		AND calc.MaintExpenseOccSwb.CalDateKey		= INSERTED.CalDateKey
		AND calc.MaintExpenseOccSwb.CurrencyRpt		= INSERTED.CurrencyRpt
		AND calc.MaintExpenseOccSwb.AccountId		= INSERTED.AccountId;
				
END;