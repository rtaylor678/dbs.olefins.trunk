﻿CREATE TABLE [calc].[Edc] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [EdcId]          VARCHAR (42)       NOT NULL,
    [EdcDetail]      NVARCHAR (256)     NOT NULL,
    [kEdc]           REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_CalcEdc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_CalcEdc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_CalcEdc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_CalcEdc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_Edc] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [EdcId] ASC, [EdcDetail] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_Edc_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_Edc_Edc_LookUp] FOREIGN KEY ([EdcId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_calc_Edc_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_Edc_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_CalcEdc_u
	ON  calc.Edc
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.Edc
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.Edc.FactorSetId	= INSERTED.FactorSetId
		AND	calc.Edc.Refnum			= INSERTED.Refnum
		AND calc.Edc.CalDateKey		= INSERTED.CalDateKey
		AND calc.Edc.EdcId			= INSERTED.EdcId
		AND calc.Edc.EdcDetail		= INSERTED.EdcDetail;
	
END