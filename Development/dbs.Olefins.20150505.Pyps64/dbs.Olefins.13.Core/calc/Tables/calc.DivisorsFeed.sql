﻿CREATE TABLE [calc].[DivisorsFeed] (
    [FactorSetId]                         VARCHAR (12)       NOT NULL,
    [Refnum]                              VARCHAR (25)       NOT NULL,
    [CalDateKey]                          INT                NOT NULL,
    [StreamId]                            VARCHAR (42)       NOT NULL,
    [StreamDescription]                   VARCHAR (256)      NULL,
    [FeedPlant_kMT]                       REAL               NOT NULL,
    [FeedPyrolysis_kMT]                   REAL               NULL,
    [FeedH2_kMT]                          REAL               NULL,
    [FeedInerts_kMT]                      REAL               NULL,
    [FeedAnalysis_kMT]                    REAL               NULL,
    [Losses_kMT]                          REAL               NULL,
    [FeedPlantWithLosses_kMT]             REAL               NULL,
    [FeedPyrolysis_WithLosses_kMT]        REAL               NULL,
    [FeedAnalysisWithLosses_kMT]          REAL               NULL,
    [TaAdj_Feed_Ratio]                    REAL               NOT NULL,
    [_TaAdj_FeedPlant_kMT]                AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedPlant_kMT],(1))) PERSISTED NOT NULL,
    [_TaAdj_FeedPyrolysis_kMT]            AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedPyrolysis_kMT],(1))) PERSISTED,
    [_TaAdj_FeedAnalysis_kMT]             AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedAnalysis_kMT],(1))) PERSISTED,
    [_TaAdj_FeedH2_kMT]                   AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedH2_kMT],(1))) PERSISTED,
    [_TaAdj_FeedInerts_kMT]               AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedInerts_kMT],(1))) PERSISTED,
    [_TaAdj_Losses_kMT]                   AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[Losses_kMT],(1))) PERSISTED,
    [_TaAdj_FeedPlantWithLosses_kMT]      AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedPlantWithLosses_kMT],(1))) PERSISTED,
    [_TaAdj_FeedPyrolysis_WithLosses_kMT] AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedPyrolysis_WithLosses_kMT],(1))) PERSISTED,
    [_TaAdj_FeedAnalysisWithLosses_kMT]   AS                 (CONVERT([real],[TaAdj_Feed_Ratio]*[FeedAnalysisWithLosses_kMT],(1))) PERSISTED,
    [tsModified]                          DATETIMEOFFSET (7) CONSTRAINT [DF_DivisorsFeed_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                      NVARCHAR (168)     CONSTRAINT [DF_DivisorsFeed_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                      NVARCHAR (168)     CONSTRAINT [DF_DivisorsFeed_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                       NVARCHAR (168)     CONSTRAINT [DF_DivisorsFeed_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [CR_calc_DivisorsFeed_FeedAnalysis_kMT] CHECK ([FeedAnalysis_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_FeedAnalysisWithLosses_kMT] CHECK ([FeedAnalysisWithLosses_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_FeedH2_kMT] CHECK ([FeedH2_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_FeedInerts_kMT] CHECK ([FeedInerts_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_FeedPlant_kMT] CHECK ([FeedPlant_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_FeedPyrolysis_kMT] CHECK ([FeedPyrolysis_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_Losses_kMT] CHECK ([Losses_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_Feed_Ratio] CHECK ([TaAdj_Feed_Ratio]>=(1.0)),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_FeedAnalysis_kMT] CHECK ([_TaAdj_FeedAnalysis_kMT]>=(0.0) AND [_TaAdj_FeedAnalysis_kMT]>=[FeedAnalysis_kMT]),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_FeedAnalysisWithLosses_kMT] CHECK ([_TaAdj_FeedAnalysisWithLosses_kMT]>=(0.0) AND [_TaAdj_FeedAnalysisWithLosses_kMT]>=[FeedAnalysisWithLosses_kMT]),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_FeedH2_kMT] CHECK ([_TaAdj_FeedH2_kMT]>=(0.0) AND [_TaAdj_FeedH2_kMT]>=[FeedH2_kMT]),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_FeedInerts_kMT] CHECK ([_TaAdj_FeedInerts_kMT]>=(0.0) AND [_TaAdj_FeedInerts_kMT]>=[FeedInerts_kMT]),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_FeedPlant_kMT] CHECK ([_TaAdj_FeedPlant_kMT]>=(0.0) AND [_TaAdj_FeedPlant_kMT]>=[FeedPlant_kMT]),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_FeedPyrolysis_kMT] CHECK ([_TaAdj_FeedPyrolysis_kMT]>=(0.0) AND [_TaAdj_FeedPyrolysis_kMT]>=[FeedPyrolysis_kMT]),
    CONSTRAINT [CR_calc_DivisorsFeed_TaAdj_Losses_kMT] CHECK ([_TaAdj_Losses_kMT]>=(0.0) AND [_TaAdj_Losses_kMT]>=[Losses_kMT]),
    CONSTRAINT [FK_calc_DivisorsFeed_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_DivisorsFeed_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_DivisorsFeed_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_DivisorsFeed_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum]),
    CONSTRAINT [UK_calc_DivisorsFeed] UNIQUE CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DivisorsFeed_Calc_CapacityUtilization]
    ON [calc].[DivisorsFeed]([StreamId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [FeedAnalysis_kMT], [Losses_kMT]);


GO
CREATE TRIGGER calc.t_DivisorsFeed_u
	ON  calc.DivisorsFeed
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.DivisorsFeed
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.DivisorsFeed.FactorSetId			= INSERTED.FactorSetId
		AND calc.DivisorsFeed.Refnum				= INSERTED.Refnum
		AND calc.DivisorsFeed.CalDateKey			= INSERTED.CalDateKey
		AND calc.DivisorsFeed.StreamId				= INSERTED.StreamId
		AND calc.DivisorsFeed.StreamDescription		= INSERTED.StreamDescription;
	
END