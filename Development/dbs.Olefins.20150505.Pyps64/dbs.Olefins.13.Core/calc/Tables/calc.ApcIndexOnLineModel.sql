﻿CREATE TABLE [calc].[ApcIndexOnLineModel] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [ApcId]            VARCHAR (42)       NOT NULL,
    [OnLine_Pcnt]      REAL               NULL,
    [Mpc_Int]          INT                NULL,
    [Mpc_Value]        REAL               NOT NULL,
    [Abs_Mpc_Value]    REAL               NULL,
    [_Apc_Index]       AS                 (CONVERT([real],case when [Mpc_Int]>=(1) then [Abs_Mpc_Value] else (0.0) end,(1))) PERSISTED,
    [_ApcOnLine_Index] AS                 (CONVERT([real],(case when [Mpc_Int]>=(1) then [Abs_Mpc_Value] else (0.0) end*[OnLine_Pcnt])/(100.0),(1))) PERSISTED,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_ApcIndexOnLineModel_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_ApcIndexOnLineModel_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_ApcIndexOnLineModel_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_ApcIndexOnLineModel_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ApcIndexOnLineModel] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ApcId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ApcIndexOnLineModel_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_calc_ApcIndexOnLineModel_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ApcIndexOnLineModel_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ApcIndexOnLineModel_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_ApcIndexOnLineModel_u
	ON  calc.ApcIndexOnLineModel
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ApcIndexOnLineModel
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ApcIndexOnLineModel.FactorSetId	= INSERTED.FactorSetId
		AND calc.ApcIndexOnLineModel.Refnum			= INSERTED.Refnum
		AND calc.ApcIndexOnLineModel.CalDateKey		= INSERTED.CalDateKey
		AND calc.ApcIndexOnLineModel.ApcId			= INSERTED.ApcId;
				
END;