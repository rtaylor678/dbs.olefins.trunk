﻿CREATE TABLE [calc].[SpTables] (
    [ItemId]  INT           IDENTITY (1, 1) NOT NULL,
    [DataSet] VARCHAR (128) NOT NULL,
    CONSTRAINT [PK_SpTables] PRIMARY KEY CLUSTERED ([DataSet] ASC),
    CONSTRAINT [CL_SpTables_DataSet] CHECK ([DataSet]<>'')
);

