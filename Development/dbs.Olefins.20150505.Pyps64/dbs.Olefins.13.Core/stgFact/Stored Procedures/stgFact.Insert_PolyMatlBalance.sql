﻿CREATE PROCEDURE [stgFact].[Insert_PolyMatlBalance]
(
	@Refnum             VARCHAR (25),
	@TrainId            TINYINT,

	@Hydrogen           REAL         = NULL,
	@Ethylene_Plant     REAL         = NULL,
	@Ethylene_Other     REAL         = NULL,
	@Propylene_Plant    REAL         = NULL,
	@Propylene_Other    REAL         = NULL,
	@Butene             REAL         = NULL,
	@Hexene             REAL         = NULL,
	@Octene             REAL         = NULL,
	@OthMono1           REAL         = NULL,
	@OthMono2           REAL         = NULL,
	@OthMono3           REAL         = NULL,
	@Additives          REAL         = NULL,
	@Catalysts          REAL         = NULL,
	@OtherFeed          REAL         = NULL,
	@TotalRawMatl       REAL         = NULL,
	@OthMono1Desc       NVARCHAR (50) = NULL,
	@OthMono2Desc       NVARCHAR (50) = NULL,
	@OthMono3Desc       NVARCHAR (50) = NULL,
	@LDPE_Liner         REAL         = NULL,
	@LDPE_Clarity       REAL         = NULL,
	@LDPE_Blow          REAL         = NULL,
	@LDPE_GP            REAL         = NULL,
	@LDPE_LidResin      REAL         = NULL,
	@LDPE_EVA           REAL         = NULL,
	@LDPE_Other         REAL         = NULL,
	@LDPE_Offspec       REAL         = NULL,
	@LLDPE_ButeneLiner  REAL         = NULL,
	@LLDPE_ButeneMold   REAL         = NULL,
	@LLDPE_HAOLiner     REAL         = NULL,
	@LLDPE_HAOMeltIndex REAL         = NULL,
	@LLDPE_HAOInject    REAL         = NULL,
	@LLDPE_HAOLidResin  REAL         = NULL,
	@LLDPE_HAORoto      REAL         = NULL,
	@LLDPE_Other        REAL         = NULL,
	@LLDPE_Offspec      REAL         = NULL,
	@HDPE_Homopoly      REAL         = NULL,
	@HDPE_Copoly        REAL         = NULL,
	@HDPE_Film          REAL         = NULL,
	@HDPE_GP            REAL         = NULL,
	@HDPE_Pipe          REAL         = NULL,
	@HDPE_Drum          REAL         = NULL,
	@HDPE_Roto          REAL         = NULL,
	@HDPE_Other         REAL         = NULL,
	@HDPE_Offspec       REAL         = NULL,
	@POLY_Homo          REAL         = NULL,
	@POLY_Copoly        REAL         = NULL,
	@POLY_CopolyBlow    REAL         = NULL,
	@POLY_CopolyInject  REAL         = NULL,
	@POLY_CopolyBlock   REAL         = NULL,
	@POLY_AtacticPP     REAL         = NULL,
	@POLY_Other         REAL         = NULL,
	@POLY_Offspec       REAL         = NULL,
	@ByProdOth1         REAL         = NULL,
	@ByProdOth2         REAL         = NULL,
	@ByProdOth3         REAL         = NULL,
	@ByProdOth4         REAL         = NULL,
	@ByProdOth5         REAL         = NULL,
	@TotProducts        REAL         = NULL,
	@ByProdOth1Desc     NVARCHAR (50) = NULL,
	@ByProdOth2Desc     NVARCHAR (50) = NULL,
	@ByProdOth3Desc     NVARCHAR (50) = NULL,
	@ByProdOth4Desc     NVARCHAR (50) = NULL,
	@ByProdOth5Desc     NVARCHAR (50) = NULL,
	@ReactorProd        REAL         = NULL,
	@MonomerConsumed    REAL         = NULL,
	@MonomerUtil        REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PolyMatlBalance]([Refnum], [TrainId], [Hydrogen], [Ethylene_Plant], [Ethylene_Other], [Propylene_Plant], [Propylene_Other], [Butene], [Hexene], [Octene], [OthMono1], [OthMono2], [OthMono3], [Additives], [Catalysts], [OtherFeed], [TotalRawMatl], [OthMono1Desc], [OthMono2Desc], [OthMono3Desc], [LDPE_Liner], [LDPE_Clarity], [LDPE_Blow], [LDPE_GP], [LDPE_LidResin], [LDPE_EVA], [LDPE_Other], [LDPE_Offspec], [LLDPE_ButeneLiner], [LLDPE_ButeneMold], [LLDPE_HAOLiner], [LLDPE_HAOMeltIndex], [LLDPE_HAOInject], [LLDPE_HAOLidResin], [LLDPE_HAORoto], [LLDPE_Other], [LLDPE_Offspec], [HDPE_Homopoly], [HDPE_Copoly], [HDPE_Film], [HDPE_GP], [HDPE_Pipe], [HDPE_Drum], [HDPE_Roto], [HDPE_Other], [HDPE_Offspec], [POLY_Homo], [POLY_Copoly], [POLY_CopolyBlow], [POLY_CopolyInject], [POLY_CopolyBlock], [POLY_AtacticPP], [POLY_Other], [POLY_Offspec], [ByProdOth1], [ByProdOth2], [ByProdOth3], [ByProdOth4], [ByProdOth5], [TotProducts], [ByProdOth1Desc], [ByProdOth2Desc], [ByProdOth3Desc], [ByProdOth4Desc], [ByProdOth5Desc], [ReactorProd], [MonomerConsumed], [MonomerUtil])
	VALUES(@Refnum, @TrainId, @Hydrogen, @Ethylene_Plant, @Ethylene_Other, @Propylene_Plant, @Propylene_Other, @Butene, @Hexene, @Octene, @OthMono1, @OthMono2, @OthMono3, @Additives, @Catalysts, @OtherFeed, @TotalRawMatl, @OthMono1Desc, @OthMono2Desc, @OthMono3Desc, @LDPE_Liner, @LDPE_Clarity, @LDPE_Blow, @LDPE_GP, @LDPE_LidResin, @LDPE_EVA, @LDPE_Other, @LDPE_Offspec, @LLDPE_ButeneLiner, @LLDPE_ButeneMold, @LLDPE_HAOLiner, @LLDPE_HAOMeltIndex, @LLDPE_HAOInject, @LLDPE_HAOLidResin, @LLDPE_HAORoto, @LLDPE_Other, @LLDPE_Offspec, @HDPE_Homopoly, @HDPE_Copoly, @HDPE_Film, @HDPE_GP, @HDPE_Pipe, @HDPE_Drum, @HDPE_Roto, @HDPE_Other, @HDPE_Offspec, @POLY_Homo, @POLY_Copoly, @POLY_CopolyBlow, @POLY_CopolyInject, @POLY_CopolyBlock, @POLY_AtacticPP, @POLY_Other, @POLY_Offspec, @ByProdOth1, @ByProdOth2, @ByProdOth3, @ByProdOth4, @ByProdOth5, @TotProducts, @ByProdOth1Desc, @ByProdOth2Desc, @ByProdOth3Desc, @ByProdOth4Desc, @ByProdOth5Desc, @ReactorProd, @MonomerConsumed, @MonomerUtil);

END;