﻿CREATE PROCEDURE [stgFact].[Insert_MetaFeedProd]
(
	@Refnum      VARCHAR (25),
	@FeedProdID  VARCHAR (20),

	@Qtr1        REAL         = NULL,
	@Qtr2        REAL         = NULL,
	@Qtr3        REAL         = NULL,
	@Qtr4        REAL         = NULL,
	@AnnFeedProd REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[MetaFeedProd]([Refnum], [FeedProdID], [Qtr1], [Qtr2], [Qtr3], [Qtr4], [AnnFeedProd])
	VALUES(@Refnum, @FeedProdID, @Qtr1, @Qtr2, @Qtr3, @Qtr4, @AnnFeedProd);

END;
