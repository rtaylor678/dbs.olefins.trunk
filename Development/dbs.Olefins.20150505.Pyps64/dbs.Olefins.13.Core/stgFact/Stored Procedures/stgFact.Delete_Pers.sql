﻿CREATE PROCEDURE [stgFact].[Delete_Pers]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Pers]
	WHERE [Refnum] = @Refnum;

END;