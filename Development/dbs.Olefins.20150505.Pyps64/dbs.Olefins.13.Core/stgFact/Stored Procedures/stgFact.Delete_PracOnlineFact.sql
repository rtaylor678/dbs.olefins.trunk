﻿CREATE PROCEDURE [stgFact].[Delete_PracOnlineFact]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracOnlineFact]
	WHERE [Refnum] = @Refnum;

END;