﻿CREATE TABLE [stgFact].[PracOnlineFact] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Tbl9051]        REAL               NULL,
    [Tbl9061]        REAL               NULL,
    [Tbl9071]        REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PracOnlineFact_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracOnlineFact_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracOnlineFact_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracOnlineFact_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PracOnlineFact] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PracOnlineFact_u]
	ON [stgFact].[PracOnlineFact]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PracOnlineFact]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PracOnlineFact].Refnum		= INSERTED.Refnum;

END;