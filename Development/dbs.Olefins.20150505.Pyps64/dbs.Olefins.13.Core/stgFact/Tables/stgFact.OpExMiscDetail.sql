﻿CREATE TABLE [stgFact].[OpExMiscDetail] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [OpExId]         VARCHAR (6)        NOT NULL,
    [Description]    VARCHAR (100)      NULL,
    [Amount]         REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_OpExMiscDetail_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_OpExMiscDetail_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_OpExMiscDetail_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_OpExMiscDetail_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_OpExMiscDetail] PRIMARY KEY CLUSTERED ([Refnum] ASC, [OpExId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_OpExMiscDetail_u]
	ON [stgFact].[OpExMiscDetail]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[OpExMiscDetail]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[OpExMiscDetail].Refnum		= INSERTED.Refnum
		AND	[stgFact].[OpExMiscDetail].OpExId		= INSERTED.OpExId;

END;