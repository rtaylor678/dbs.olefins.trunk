﻿CREATE PROCEDURE [inter].[Insert_ReplValPyroCapacity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO inter.ReplValPyroCapacity(FactorSetId, Refnum, CalDateKey, CurrencyRpt, PyroEthyleneCap_kMT)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			CASE WHEN ISNULL(y.Component_kMT, 0.0) / calc.MaxValue(c.Capacity_kMT * 0.92, c._TaAdj_Capacity_kMT) > 0.5
				THEN
					calc.MinValue(
					(calc.MaxValue(c.Capacity_kMT * 0.92, c._TaAdj_Capacity_kMT) - ISNULL(y.Component_kMT, 0.0)) * 1.2,
					f.Capacity_kMT * (75.0 / 150.0 * ISNULL(enc.Act_C2Yield_EstWtPcnt, 0.0) + 30.0 / 140.0 * (100.0 - ISNULL(enc.Act_C2Yield_EstWtPcnt, 0.0))) / 100.0
					)
				ELSE
					calc.MaxValue(c.Capacity_kMT * 0.92, c._TaAdj_Capacity_kMT) - ISNULL(y.Component_kMT, 0.0)
				END								[PyroEthyleneCap_kMT]
		FROM	@fpl									fpl
		INNER JOIN	calc.CapacityPlantTaAdj				c
			ON	c.FactorSetId = fpl.FactorSetId
			AND	c.CalDateKey = fpl.Plant_QtrDateKey
			AND	c.Refnum = fpl.Refnum
			AND	c.SchedId = 'P'
			AND	c.StreamId = 'Ethylene'
		LEFT OUTER JOIN inter.YIRSupplemental			y
			ON	y.FactorSetId = fpl.FactorSetId
			AND	y.Refnum = fpl.Refnum
			AND	y.CalDateKey = fpl.Plant_QtrDateKey
			AND	y.ComponentId = 'C2H4'
		INNER JOIN(
			SELECT
				c.Refnum,
				c.CalDateKey,
				SUM(c.Capacity_kMT)	[Capacity_kMT]
			FROM fact.Capacity	c
			WHERE	c.StreamId IN ('FreshPyroFeed', 'Recycle')
			GROUP BY
				c.Refnum,
				c.CalDateKey
			)											f
			ON	f.Refnum = fpl.Refnum
			AND	f.CalDateKey = fpl.Plant_QtrDateKey
		LEFT OUTER JOIN inter.ReplValEncEstimate			enc
			ON	enc.FactorSetId = fpl.FactorSetId
			AND	enc.Refnum = fpl.Refnum
			AND	enc.CalDateKey = fpl.Plant_QtrDateKey
			AND enc.CurrencyRpt = 'USD'
			AND	enc.ComponentId = 'C2H6'
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;