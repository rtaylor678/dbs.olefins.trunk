﻿CREATE PROCEDURE [Console].[AddIssue] 
	@RefNum varchar(18),
	@IssueID nvarchar(8),
	@IssueTitle nvarchar(50),
	@IssueText text,
	@UserName nvarchar(20)
AS
BEGIN
INSERT INTO Val.Checklist
Values('20' + @RefNum, @IssueID, @IssueTitle, @IssueText, @UserName, GETDATE(),'N', null,null)
END
