﻿CREATE PROCEDURE [Console].[GetInterimContactInfo]
	@RefNum varchar(18)
AS
BEGIN
	SELECT FirstName, LastName, JobTitle = NULL, Email, Phone  
	FROM Console.GetCompanyContact(@RefNum, 'Inter')
END
