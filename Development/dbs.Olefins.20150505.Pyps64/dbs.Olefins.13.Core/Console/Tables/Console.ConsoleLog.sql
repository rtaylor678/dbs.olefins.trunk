﻿CREATE TABLE [Console].[ConsoleLog] (
    [UserName]  NVARCHAR (20)   NULL,
    [LogType]   NVARCHAR (20)   NULL,
    [Message]   NVARCHAR (2000) NULL,
    [DateAdded] DATETIME        NULL
);

