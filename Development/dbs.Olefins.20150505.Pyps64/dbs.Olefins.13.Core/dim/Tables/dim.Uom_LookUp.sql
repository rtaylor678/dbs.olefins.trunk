﻿CREATE TABLE [dim].[Uom_LookUp] (
    [UomId]          VARCHAR (12)       NOT NULL,
    [UomName]        NVARCHAR (84)      NOT NULL,
    [UomDetail]      NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Uom_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Uom_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Uom_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Uom_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Uom_LookUp] PRIMARY KEY CLUSTERED ([UomId] ASC),
    CONSTRAINT [CL_Uom_LookUp_UomDetail] CHECK ([UomDetail]<>''),
    CONSTRAINT [CL_Uom_LookUp_UomId] CHECK ([UomId]<>''),
    CONSTRAINT [CL_Uom_LookUp_UomName] CHECK ([UomName]<>''),
    CONSTRAINT [UK_Uom_LookUp_UomDetail] UNIQUE NONCLUSTERED ([UomDetail] ASC),
    CONSTRAINT [UK_Uom_LookUp_UomName] UNIQUE NONCLUSTERED ([UomName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Uom_LookUp_u]
	ON [dim].[Uom_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Uom_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Uom_LookUp].[UomId]		= INSERTED.[UomId];
		
END;
