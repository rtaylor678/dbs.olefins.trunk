﻿CREATE TABLE [dim].[ApcLoopMonitor_LookUp] (
    [ApcLoopMonitorId]     CHAR (1)           NOT NULL,
    [ApcLoopMonitorName]   NVARCHAR (84)      NOT NULL,
    [ApcLoopMonitorDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_ApcLoopMonitor_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_ApcLoopMonitor_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_ApcLoopMonitor_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_ApcLoopMonitor_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ApcLoopMonitor_LookUp] PRIMARY KEY CLUSTERED ([ApcLoopMonitorId] ASC),
    CONSTRAINT [CL_ApcLoopMonitor_LookUp_ApcLoopMonitorDetail] CHECK ([ApcLoopMonitorDetail]<>''),
    CONSTRAINT [CL_ApcLoopMonitor_LookUp_ApcLoopMonitorId] CHECK ([ApcLoopMonitorId]<>''),
    CONSTRAINT [CL_ApcLoopMonitor_LookUp_ApcLoopMonitorName] CHECK ([ApcLoopMonitorName]<>''),
    CONSTRAINT [UK_ApcLoopMonitor_LookUp_ApcLoopMonitorDetail] UNIQUE NONCLUSTERED ([ApcLoopMonitorDetail] ASC),
    CONSTRAINT [UK_ApcLoopMonitor_LookUp_ApcLoopMonitorName] UNIQUE NONCLUSTERED ([ApcLoopMonitorName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ApcLoopMonitor_LookUp_u]
	ON [dim].[ApcLoopMonitor_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ApcLoopMonitor_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ApcLoopMonitor_LookUp].[ApcLoopMonitorId]		= INSERTED.[ApcLoopMonitorId];
		
END;
