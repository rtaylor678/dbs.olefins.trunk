﻿CREATE TABLE [dim].[Apc_LookUp] (
    [ApcId]          VARCHAR (42)       NOT NULL,
    [ApcName]        NVARCHAR (84)      NOT NULL,
    [ApcDetail]      NVARCHAR (348)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Apc_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Apc_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Apc_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Apc_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Apc_LookUp] PRIMARY KEY CLUSTERED ([ApcId] ASC),
    CONSTRAINT [CL_Apc_LookUp_ApcDetail] CHECK ([ApcDetail]<>''),
    CONSTRAINT [CL_Apc_LookUp_ApcId] CHECK ([ApcId]<>''),
    CONSTRAINT [CL_Apc_LookUp_ApcName] CHECK ([ApcName]<>''),
    CONSTRAINT [UK_Apc_LookUp_ApcDetail] UNIQUE NONCLUSTERED ([ApcDetail] ASC),
    CONSTRAINT [UK_Apc_LookUp_ApcName] UNIQUE NONCLUSTERED ([ApcName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Apc_LookUp_u]
	ON [dim].[Apc_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Apc_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Apc_LookUp].[ApcId]		= INSERTED.[ApcId];
		
END;
