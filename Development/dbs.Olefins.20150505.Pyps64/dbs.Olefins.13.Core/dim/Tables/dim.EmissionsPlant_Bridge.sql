﻿CREATE TABLE [dim].[EmissionsPlant_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [EmissionsId]        VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_EmissionsPlant_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_EmissionsPlant_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_EmissionsPlant_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_EmissionsPlant_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_EmissionsPlant_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_EmissionsPlant_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EmissionsId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_EmissionsPlant_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_EmissionsPlant_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsPlant_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_EmissionsPlant_Bridge_LookUp_Emissions] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsPlant_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [EmissionsId]) REFERENCES [dim].[EmissionsPlant_Parent] ([FactorSetId], [EmissionsId]),
    CONSTRAINT [FK_EmissionsPlant_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[EmissionsPlant_Parent] ([FactorSetId], [EmissionsId])
);


GO

CREATE TRIGGER [dim].[t_EmissionsPlant_Bridge_u]
ON [dim].[EmissionsPlant_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[EmissionsPlant_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[EmissionsPlant_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[EmissionsPlant_Bridge].[EmissionsId]	= INSERTED.[EmissionsId]
		AND	[dim].[EmissionsPlant_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
