﻿


CREATE VIEW [dbo].[PyroYieldImprovementRatios] AS
SELECT Refnum, FactorSetId, SimModelId, OpCondId, C2H4YIR = [C2H4], C3H6YIR = [C3H6], ProdOlefinsYIR = [ProdOlefins], ProdHVCYIR = [ProdHVC]
      FROM (SELECT t.Refnum, t.FactorSetId, SimModelId, OpCondId, ComponentId, Improvement_Pcnt
            FROM calc.CompositionYieldRatioImprovement c JOIN dbo.TSort t on t.Refnum=c.Refnum and t.FactorSetId=c.FactorSetId) nrm
      PIVOT (SUM(Improvement_Pcnt) FOR ComponentId IN ([C2H4],[C3H6],[ProdOlefins],[ProdHVC])) pvt


