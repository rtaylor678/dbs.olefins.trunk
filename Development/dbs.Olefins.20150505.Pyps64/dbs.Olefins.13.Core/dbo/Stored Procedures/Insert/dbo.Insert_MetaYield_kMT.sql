﻿CREATE PROCEDURE [dbo].[Insert_MetaYield_kMT]
(
	@Refnum			VARCHAR(25),
	@FactorSetId	VARCHAR(12)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE	@CurrencyId		VARCHAR(5) = 'USD';
		DECLARE	@DataType		VARCHAR(8) = 'kMT';

		DECLARE	@C2H6			REAL;
		DECLARE	@C3H8			REAL;
		DECLARE	@C4H10			REAL;

		SELECT
			@C2H6	= [p].[C2H6],
			@C3H8	= [p].[C3H8],
			@C4H10	= [p].[C4H10]
		FROM (
			SELECT
				[qr].[FactorSetId],
				[qr].[Refnum],
				[qr].[ComponentId],
				[qr].[_Recycled_kMT]
			FROM [calc].[QuantityRecycled]	[qr]
			WHERE	qr.[FactorSetId]	= @FactorSetId
				AND	qr.[Refnum]			= @Refnum
			) [u]
			PIVOT (
				SUM([u].[_Recycled_kMT]) FOR [u].[ComponentId] IN
				(
					[C2H6],
					[C3H8],
					[C4H10]
				)
			) [p];

		INSERT INTO [dbo].[MetaYield]
		(
			[Refnum],
			[DataType],
			[Ethane],
			[EPMix],
			[Propane],
			[LPG],
			[Butane],
			[FeedLtOther],
			[NaphthaLt],
			[NaphthaFr],
			[NaphthaHv],
			[Condensate],
			[HeavyNGL],
			[Raffinate],
			[Diesel],
			[GasOilHv],
			[GasOilHt],
			[FeedLiqOther],
			[FreshPyroFeed],
			[ConcEthylene],
			[ConcPropylene],
			[ConcButadiene],
			[ConcBenzene],
			[ROGHydrogen],
			[ROGMethane],
			[ROGEthane],
			[ROGEthylene],
			[ROGPropylene],
			[ROGPropane],
			[ROGC4Plus],
			[ROGInerts],
			[DilHydrogen],
			[DilMethane],
			[DilEthane],
			[DilEthylene],
			[DilPropane],
			[DilPropylene],
			[DilButane],
			[DilButylene],
			[DilButadiene],
			[DilBenzene],
			[DilMogas],
			[SuppWashOil],
			[SuppGasOil],
			[SuppOther],
			[SuppTot],
			[PlantFeed],
			[Hydrogen],
			[Methane],
			[Acetylene],
			[EthylenePG],
			[EthyleneCG],
			[PropylenePG],
			[PropyleneCG],
			[PropyleneRG],
			[PropaneC3Resid],
			[Butadiene],
			[IsoButylene],
			[C4Oth],
			[Benzene],
			[PyroGasoline],
			[PyroGasOil],
			[PyroFuelOil],
			[AcidGas],
			[PPFC],
			[ProdOther],
			[Prod],
			[LossFlareVent],
			[LossOther],
			[LossMeasure],
			[Loss],
			[ProdLoss],
			[FeedProdLoss],
			[FeedLiqOther1],
			[FeedLiqOther2],
			[FeedLiqOther3],
			[ProdOther1],
			[ProdOther2],
			[RecSuppEthane],
			[RecSuppPropane],
			[RecSuppButane],
			[Recycle]
		)
		SELECT
			[ppsma].[Refnum],
			@DataType,

			[Ethane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Ethane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[EPMix]				= SUM(CASE WHEN [ppsma].[StreamId] = 'EPMix' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Propane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Propane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[LPG]				= SUM(CASE WHEN [ppsma].[StreamId] = 'LPG' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Butane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Butane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FeedLtOther]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLtOther' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[NaphthaLt]			= SUM(CASE WHEN [ppsma].[StreamId] = 'NaphthaLt' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[NaphthaFr]			= SUM(CASE WHEN [ppsma].[StreamId] = 'NaphthaFr' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[NaphthaHv]			= SUM(CASE WHEN [ppsma].[StreamId] = 'NaphthaHv' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Condensate]		= SUM(CASE WHEN [ppsma].[StreamId] = 'Condensate' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[HeavyNGL]			= SUM(CASE WHEN [ppsma].[StreamId] = 'HeavyNGL' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Raffinate]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Raffinate' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Diesel]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Diesel' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[GasOilHv]			= SUM(CASE WHEN [ppsma].[StreamId] = 'GasOilHv' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[GasOilHt]			= SUM(CASE WHEN [ppsma].[StreamId] = 'GasOilHt' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FeedLiqOther]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FreshPyroFeed]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FreshPyroFeed' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ConcEthylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcEthylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ConcPropylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcPropylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ConcButadiene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcButadiene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ConcBenzene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcBenzene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGHydrogen]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGHydrogen' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGMethane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGMethane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGEthane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGEthane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGEthylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGEthylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGPropylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGPropylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGPropane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGPropane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGC4Plus]			= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGC4Plus' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ROGInerts]			= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGInerts' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilHydrogen]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilHydrogen' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilMethane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilMethane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilEthane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'DilEthane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilEthylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilEthylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilPropane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilPropane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilPropylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilPropylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilButane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'DilButane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilButylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilButylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilButadiene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilButadiene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilBenzene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilBenzene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[DilMogas]			= SUM(CASE WHEN [ppsma].[StreamId] = 'DilMogas' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[SuppWashOil]		= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppWashOil' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[SuppGasOil]		= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppGasOil' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[SuppOther]			= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppOther' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[SuppTot]			= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppTot' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PlantFeed]			= SUM(CASE WHEN [ppsma].[StreamId] = 'PlantFeed' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),

			[Hydrogen]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'Hydrogen' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Methane]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'Methane' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Acetylene]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'Acetylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[EthylenePG]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'EthylenePG' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[EthyleneCG]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'EthyleneCG' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PropylenePG]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'PropylenePG' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PropyleneCG]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'PropyleneCG' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PropyleneRG]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'PropyleneRG' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PropaneC3Resid]	= -SUM(CASE WHEN [ppsma].[StreamId] = 'PropaneC3Resid' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Butadiene]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'Butadiene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[IsoButylene]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'IsoButylene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[C4Oth]				= -SUM(CASE WHEN [ppsma].[StreamId] = 'C4Oth' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Benzene]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'Benzene' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PyroGasoline]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'PyroGasoline' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PyroGasOil]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'PyroGasOil' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PyroFuelOil]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'PyroFuelOil' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[AcidGas]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'AcidGas' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[PPFC]				= -SUM(CASE WHEN [ppsma].[StreamId] = 'PPFC' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ProdOther]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'ProdOther' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Prod]				= -SUM(CASE WHEN [ppsma].[StreamId] = 'Prod' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[LossFlareVent]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'LossFlareVent' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[LossOther]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'LossOther' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[LossMeasure]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'LossMeasure' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Loss]				= -SUM(CASE WHEN [ppsma].[StreamId] = 'Loss' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ProdLoss]			= -SUM(CASE WHEN [ppsma].[StreamId] = 'ProdLoss' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FeedProdLoss]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'FeedProdLoss' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FeedLiqOther1]		=  SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' AND [ppsma].[StreamDescription] LIKE '%(Other Feed 1)%' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FeedLiqOther2]		=  SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' AND [ppsma].[StreamDescription] LIKE '%(Other Feed 2)%' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FeedLiqOther3]		=  SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' AND [ppsma].[StreamDescription] LIKE '%(Other Feed 3)%' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ProdOther1]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'ProdOther' AND [ppsma].[StreamDescription] LIKE '%(Other Prod 1)%' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[ProdOther2]		= -SUM(CASE WHEN [ppsma].[StreamId] = 'ProdOther' AND [ppsma].[StreamDescription] LIKE '%(Other Prod 2)%' THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),

			@C2H6,
			@C3H8,
			@C4H10,

			[Recycle]			= COALESCE(@C2H6, 0.0) + COALESCE(@C3H8, 0.0) + COALESCE(@C4H10, 0.0)
		FROM
			[calc].[PricingPlantStreamMetaAggregate] [ppsma]
		WHERE	[ppsma].[Refnum]		= @Refnum
			AND	[ppsma].[FactorSetId]	= @FactorSetId
			AND	[ppsma].[CurrencyRpt]	= @CurrencyId
		GROUP BY
			[ppsma].[Refnum];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;