﻿CREATE TABLE [ante].[InflationFactor] (
    [FactorSetId]        VARCHAR (12)       NOT NULL,
    [CurrencyRpt]        VARCHAR (4)        NOT NULL,
    [BaseYear]           SMALLINT           NOT NULL,
    [StudyYear]          SMALLINT           NOT NULL,
    [Inflation]          REAL               NOT NULL,
    [InflationSupersede] REAL               NULL,
    [_InflationFactor]   AS                 (CONVERT([real],isnull([InflationSupersede],[Inflation]),0)) PERSISTED NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_InflationFactor_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_InflationFactor_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_InflationFactor_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_InflationFactor_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_InflationFactor] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [BaseYear] ASC),
    CONSTRAINT [FK_InflationFactor_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_InflationFactor_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER ante.t_InflationFactor_u
	ON [ante].[InflationFactor]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[InflationFactor]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[InflationFactor].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[InflationFactor].CurrencyRpt	= INSERTED.CurrencyRpt
		AND	[ante].[InflationFactor].BaseYear		= INSERTED.BaseYear;

END;