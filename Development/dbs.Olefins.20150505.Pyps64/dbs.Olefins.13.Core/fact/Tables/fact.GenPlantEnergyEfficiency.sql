﻿CREATE TABLE [fact].[GenPlantEnergyEfficiency] (
    [Refnum]                    VARCHAR (25)       NOT NULL,
    [CalDateKey]                INT                NOT NULL,
    [PyroFurnInletTemp_C]       REAL               NULL,
    [PyroFurnPreheat_Pcnt]      REAL               NULL,
    [PyroFurnPrimaryTLE_Pcnt]   REAL               NULL,
    [PyroFurnTLESteam_PSIa]     REAL               NULL,
    [PyroFurnSecondaryTLE_Pcnt] REAL               NULL,
    [DriverGT_Bit]              BIT                NULL,
    [PyroFurnFlueGasTemp_C]     REAL               NULL,
    [PyroFurnFlueGasO2_Pcnt]    REAL               NULL,
    [TLEOutletTemp_C]           REAL               NULL,
    [EnergyDeterioration_Pcnt]  REAL               NULL,
    [IBSLSteamRequirement_Pcnt] REAL               NULL,
    [tsModified]                DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantEnergyEfficiency_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]            NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnergyEfficiency_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]            NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnergyEfficiency_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]             NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnergyEfficiency_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantEnergyEfficiency] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_EnergyDeterioration_Pcnt] CHECK ([EnergyDeterioration_Pcnt]>=(0.0) AND [EnergyDeterioration_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_IBSLSteamRequirement_Pcnt] CHECK ([IBSLSteamRequirement_Pcnt]>=(0.0) AND [IBSLSteamRequirement_Pcnt]<=(120.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_PyroFurnFlueGasO2_Pcnt] CHECK ([PyroFurnFlueGasO2_Pcnt]>=(0.0) AND [PyroFurnFlueGasO2_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_PyroFurnFlueGasTemp_C] CHECK ([PyroFurnFlueGasTemp_C]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_PyroFurnInletTemp_C] CHECK ([PyroFurnInletTemp_C]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_PyroFurnPreheat_Pcnt] CHECK ([PyroFurnPreheat_Pcnt]>=(0.0) AND [PyroFurnPreheat_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_PyroFurnPrimaryTLE_Pcnt] CHECK ([PyroFurnPrimaryTLE_Pcnt]>=(0.0) AND [PyroFurnPrimaryTLE_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_PyroFurnSecondaryTLE_Pcnt] CHECK ([PyroFurnSecondaryTLE_Pcnt]>=(0.0) AND [PyroFurnSecondaryTLE_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_PyroFurnTLESteam_PSIa] CHECK ([PyroFurnTLESteam_PSIa]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergyEfficiency_TLEOutletTemp_C] CHECK ([TLEOutletTemp_C]>=(0.0)),
    CONSTRAINT [FK_GenPlantEnergyEfficiency_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantEnergyEfficiency_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantEnergyEfficiency_u]
	ON [fact].[GenPlantEnergyEfficiency]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantEnergyEfficiency]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantEnergyEfficiency].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlantEnergyEfficiency].CalDateKey	= INSERTED.CalDateKey;

END;