﻿CREATE TABLE [fact].[ApcExistance] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [ApcId]          VARCHAR (42)       NOT NULL,
    [Mpc_Bit]        BIT                NOT NULL,
    [Sep_Bit]        BIT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ApcExistance_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ApcExistance_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ApcExistance_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ApcExistance_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ApcExistance] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ApcId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ApcExistance_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_ApcExistance_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ApcExistance_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ApcExistance_u]
	ON [fact].[ApcExistance]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ApcExistance]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ApcExistance].Refnum		= INSERTED.Refnum
		AND [fact].[ApcExistance].ApcId			= INSERTED.ApcId
		AND [fact].[ApcExistance].CalDateKey	= INSERTED.CalDateKey;

END;