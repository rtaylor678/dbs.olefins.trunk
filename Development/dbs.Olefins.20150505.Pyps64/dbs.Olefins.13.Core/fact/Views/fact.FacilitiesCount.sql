﻿CREATE VIEW [fact].[FacilitiesCount]
WITH SCHEMABINDING
AS
SELECT
	b.[FactorSetId],
	f.[Refnum],
	f.[CalDateKey],
	b.[FacilityId],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + f.[Unit_Count]
		WHEN '-' THEN - f.[Unit_Count]
		ELSE 0.0
		END)							[Unit_Count],
	COUNT_BIG(*)						[IndexItems]
FROM dim.Facility_Bridge	b
INNER JOIN fact.Facilities	f
	ON	f.FacilityId = b.DescendantId
GROUP BY
	b.FactorSetId,
	f.Refnum,
	f.CalDateKey,
	b.FacilityId;
GO
CREATE UNIQUE CLUSTERED INDEX [UX_FacilitiesCount]
    ON [fact].[FacilitiesCount]([FactorSetId] ASC, [Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC);

