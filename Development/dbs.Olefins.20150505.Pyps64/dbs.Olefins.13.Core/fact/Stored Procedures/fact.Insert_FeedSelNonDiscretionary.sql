﻿CREATE PROCEDURE [fact].[Insert_FeedSelNonDiscretionary]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSelNonDiscretionary(Refnum, CalDateKey, StreamId, Affiliated_Pcnt, NonAffiliated_Pcnt)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'Light'
			, p.AffGasPcntNonDisc * 100.0
			, p.NonAffGasPcnt * 100.0
		FROM stgFact.Prac p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE (p.AffGasPcnt IS NOT NULL OR p.NonAffGasPcnt IS NOT NULL)
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;