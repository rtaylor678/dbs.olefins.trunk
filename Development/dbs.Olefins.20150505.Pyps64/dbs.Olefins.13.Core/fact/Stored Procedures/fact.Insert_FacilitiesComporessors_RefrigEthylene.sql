﻿CREATE PROCEDURE [fact].[Insert_FacilitiesComporessors_RefrigEthylene]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesCompressors(Refnum, CalDateKey, FacilityId
			, Age_Years, Power_BHP, CoatingID, DriverID)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'CompRefrigEthylene'
			, f.EthyCompAge
			, f.EthyCompBHP
			, etl.ConvFacilityCoatingID(f.EthyCompCoat)				[CoatingID]
			, etl.ConvFacilityDriverID(f.EthyCompDriver)			[DriverID]
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;