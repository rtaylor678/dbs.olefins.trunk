﻿CREATE PROCEDURE [fact].[Insert_Reliability]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Reliability(Refnum, CalDateKey
				, TimingFactorID, TaPrep_Hrs, TaStartUp_Hrs
				, MiniTurnAround_Bit
				, FurnRetubeWorkID, FurnTubeLife_Mnths, FurnRetube_Days)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')					[Refnum]
			, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
			, CASE p.TimeFact WHEN '?' THEN NULL ELSE p.TimeFact END		[TimingFactorID]
			, p.TAPrepTime
			, p.TaStartUpTime
			, etl.ConvBit(a.TAMini)											[MiniTurnAround]
			, etl.ConvFurnRetubeWork(m.FurnRetubeWho)						[FurnRetubeWorkID]
			, m.FurnTubeLife
			, m.FurnRetubeTime
		FROM stgFact.PracTA p
		LEFT OUTER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		LEFT OUTER JOIN stgFact.MaintMisc m ON m.Refnum = p.Refnum
		LEFT OUTER JOIN stgFact.TADT a ON a.Refnum = p.Refnum AND a.TrainId = 1
		WHERE	(p.TimeFact IS NOT NULL
			OR	p.TAPrepTime IS NOT NULL
			OR	p.TaStartUpTime IS NOT NULL
			OR	a.TAMini IS NOT NULL
			OR	m.FurnRetubeWho IS NOT NULL
			OR	m.FurnTubeLife IS NOT NULL
			OR	m.FurnRetubeTime IS NOT NULL)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;