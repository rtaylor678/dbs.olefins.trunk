﻿CREATE PROC [Ranking].[GetRanking](@Refnum varchar(18), @ListId varchar(42), @RankBreak sysname, @RankVariable sysname)
AS
SET NOCOUNT ON
DECLARE @BreakValue varchar(12), @RankSpecsID int, @CurrentValue real
SELECT @BreakValue = Ranking.GetBreakValue(@Refnum, @RankBreak, @ListId)

IF @BreakValue IS NULL
	RAISERROR ('The Break %s is not defined for Refnum %s', 10, 1, @RankBreak, @Refnum)
ELSE BEGIN
	SELECT @CurrentValue = Value FROM Ranking.RefineryValues WHERE Refnum = @Refnum AND RankVariable = @RankVariable
	IF @CurrentValue IS NULL
		RAISERROR ('No value has been calculated for %s for Refnum %s', 10, 1, @RankVariable, @Refnum)
	ELSE BEGIN
		EXEC Ranking.GetRankSpecsID @ListId, @RankBreak, @BreakValue, @RankVariable, @RankSpecsID OUTPUT

		DECLARE @Rank int, @Percentile real, @Tile tinyint, @RankedValue real, @NumTiles tinyint, @DataCount int, @ManualRank Bit
		SELECT @Rank = Rank, @Percentile = Percentile, @Tile = Tile, @RankedValue = Value
		FROM Ranking.Rank WHERE Refnum = @Refnum AND RankSpecsID = @RankSpecsID

		IF @RankedValue IS NULL OR ABS(@CurrentValue - @RankedValue) > 0.01
		BEGIN
			/* Either has never been ranked or the value has changed and need to rerank */
			DECLARE @Refnums dbo.RefnumList, @ForceTiles tinyint
			INSERT @Refnums(Refnum) SELECT Refnum FROM Ranking.GetRefnumList(@ListId, @RankBreak, @BreakValue)
			IF NOT EXISTS (SELECT * FROM @Refnums WHERE Refnum = @Refnum)
				INSERT @Refnums (Refnum) VALUES (@Refnum)

			SELECT @ForceTiles = Ranking.GetForceTiles(@ListId, @RankBreak, @BreakValue)
			
			DECLARE @RankedData Ranking.RankedData
			INSERT @RankedData
			EXEC Ranking.RankRefnumList @Refnums, @RankVariable, @ForceTiles, @NumTiles OUTPUT, @DataCount OUTPUT, NULL, NULL, NULL, NULL, NULL /* Don't need the other summary data */

			SELECT @Rank = rd.Rank, @Percentile = rd.Percentile, @Tile = rd.Tile, @ManualRank = 1
			FROM @RankedData rd WHERE Refnum = @Refnum
		END
		ELSE
			SELECT @NumTiles = NumTiles, @DataCount = DataCount, @ManualRank = 0
			FROM Ranking.RankSpecsStats WHERE RankSpecsID = @RankSpecsID
			
		SET NOCOUNT OFF

		SELECT Rank = @Rank, Percentile = @Percentile, Tile = @Tile, NumTiles = @NumTiles, DataCount = @DataCount, BreakValue = @BreakValue, ManualRank = @ManualRank
		
	END
END
