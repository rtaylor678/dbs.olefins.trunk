﻿
--SELECT * FROM [stgFact].[TSort] [t] WHERE [t].[Refnum] IN  ('13PCH200', '13PCH127', '13PCH214', '13PCH201') OR [t].[Refnum] LIKE '13PCH%';
--SELECT * FROM [cons].[TSortSolomon] [t] WHERE [t].[Refnum] IN ('2013PCH200', '2013PCH127', '2013PCH214', '2013PCH201') OR [t].[Refnum] LIKE '2013PCH%';

DECLARE	@StudyId		VARCHAR(4)	= 'PCH';


INSERT INTO [cons].[TSortSolomon]
(
	[Refnum],
	[CalDateKey],
	[FactorSetId]
)
SELECT
	[Refnum]		= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
	[CalDateKey]	= [etl].[ConvDateKey]([t].[StudyYear]),
	[FactorSetId]	= [t].[StudyYear]
FROM
	[stgFact].[TSort]			[t]
LEFT OUTER JOIN
	[cons].[TSortSolomon]		[s]
		ON	CHARINDEX(RTRIM(LTRIM([t].[Refnum])), RTRIM(LTRIM([s].[Refnum])), 1) = 3
WHERE [t].[Refnum] LIKE '13PCH%'
	AND	[s].[Refnum]	IS NULL


--SELECT * FROM [cons].[SubscriptionsAssets] [t] WHERE [t].[Refnum] IN ('2013PCH200', '2013PCH127', '2013PCH214', '2013PCH201') OR [t].[Refnum] LIKE '2013PCH%';

INSERT INTO [cons].[SubscriptionsAssets]
(
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[AssetID],
	[SubscriberAssetName],
	[SubscriberAssetDetail]
)
SELECT
	[CompanyID]				= [etl].[ConvCompanyID]([t].[CompanyId], [t].[CoName], [t].[Co]),
	[CalDateKey]			= [etl].[ConvDateKey]([t].[StudyYear]),
	[StudyID]				= @StudyId,
	[AssetID]				= [etl].[ConvAssetPCH]([t].[Refnum]),
	[SubscriberAssetName]	= [t].[Loc],
	[SubscriberAssetDetail]	= [t].[Loc]
FROM
	[stgFact].[TSort]				[t]
LEFT OUTER JOIN
	[cons].[SubscriptionsAssets]	[s]
		ON	CHARINDEX(RTRIM(LTRIM([t].[Refnum])), RTRIM(LTRIM([s].[Refnum])), 1) = 3
WHERE	[s].[Refnum]	IS NULL
	AND	[t].[StudyYear] = 2013
	AND	[t].[Refnum]	= @sRefnum;

--SELECT * FROM [cons].[SubscriptionsCompanies] [t] WHERE [t].[CalDateKey] = 20131231;

INSERT INTO [cons].[SubscriptionsCompanies]
(
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[SubscriberCompanyName],
	[SubscriberCompanyDetail]
)
SELECT
	[m].[CompanyID],
	[CalDateKey]				= [etl].[ConvDateKey]([t].[StudyYear]),
	[StudyID]					= @StudyId,

	[SubscriberCompanyName]		= MAX([t].[Co]),
	[SubscriberCompanyDetail]	= MAX([t].[Co])
FROM
	[stgFact].[TSort]				[t]
INNER JOIN
	[etl].[MapCompanies]			[m]
		ON	[m].[Co]				= [t].[Co]
LEFT OUTER JOIN
	[cons].[SubscriptionsCompanies]	[s]
		ON	[s].[CompanyID]		= [m].[CompanyID]
		AND	[s].[CalDateKey]	= [etl].[ConvDateKey]([t].[StudyYear])
		AND	[s].[StudyID]		= @StudyId
WHERE	[t].[Refnum] LIKE '13PCH%'
	--AND	[s].[CompanyID]	IS NULL
GROUP BY
	[m].[CompanyID],
	[t].[StudyYear];
