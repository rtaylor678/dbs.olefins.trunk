﻿INSERT INTO dim.Company_LookUP
(
    [CompanyId],
    [CompanyName],
    [CompanyDetail]
)
VALUES
('SaudiPolymers', 'Saudi Polymers Company', 'Saudi Polymers Company'
)


DECLARE @Refnum			VARCHAR(18) = '2013PCH121';
DECLARE @sRefnum		VARCHAR(18) = RIGHT(@Refnum, LEN(@Refnum) - 2);
DECLARE @FactorSetID	VARCHAR(4)	= '2014'; -- LEFT(@Refnum, 4);
DECLARE @ProcedureDesc	NVARCHAR(4000);


		DECLARE @AssetId	VARCHAR(5)	= [etl].[ConvAssetPCH](@Refnum);
		DECLARE @StudyId	VARCHAR(4)	= [etl].[ConvRefnumStudy](@Refnum);


SELECT
	etl.ConvCompanyID([t].[CompanyId], [t].[CoName], [t].[Co]),
	[t].[CompanyId],
	[t].[CoName],
	[t].[Co]
FROM [stgFact].[TSort]	[t]
WHERE	[t].[Refnum]		= @sRefnum;