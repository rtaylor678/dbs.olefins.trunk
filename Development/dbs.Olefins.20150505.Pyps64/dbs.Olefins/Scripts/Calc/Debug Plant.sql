﻿--SELECT DATEDIFF(d, '1/1/2000', SYSDATETIME())

DECLARE @Beg			DATETIMEOFFSET(7)	= SYSDATETIMEOFFSET();

DECLARE @Refnum			VARCHAR(18) = '2013PCH074';
DECLARE @sRefnum		VARCHAR(18) = RIGHT(@Refnum, LEN(@Refnum) - 2);
DECLARE @FactorSetID	VARCHAR(4)	= '2013'; -- LEFT(@Refnum, 4);
DECLARE @StudyId		VARCHAR(4)	= 'PCH';

SET NOCOUNT ON;

DECLARE @ProcedureDesc	NVARCHAR(4000);

--EXECUTE [dbo].[spCalcs] @Refnum;

--SELECT * FROM [dbo].[LogError]	[le] WHERE [le].[Refnum] = @Refnum ORDER BY [le].[tsModified] DESC;

DECLARE @fpl	[calc].[FoundationPlantList];

		INSERT INTO @fpl
		(
			[FactorSetId],
			[FactorSetName],
			[FactorSet_AnnDateKey],
			[FactorSet_QtrDateKey],
			[FactorSet_QtrDate],
			[Refnum],
			[Plant_AnnDateKey],
			[Plant_QtrDateKey],
			[Plant_QtrDate],
			[CompanyId],
			[AssetId],
			[AssetName],
			[SubscriberCompanyName],
			[PlantCompanyName],
			[SubscriberAssetName],
			[PlantAssetName],
			[CountryId],
			[StateName],
			[EconRegionId],
			[UomId],
			[CurrencyFcn],
			[CurrencyRpt],
			[AssetPassWord_PlainText],
			[StudyYear],
			[DataYear],
			[Consultant],
			[StudyYearDifference],
			[CalQtr]
		)
		SELECT
			tsq.[FactorSetId],
			tsq.[FactorSetName],
			tsq.[FactorSet_AnnDateKey],
			tsq.[FactorSet_QtrDateKey],
			tsq.[FactorSet_QtrDate],
			tsq.[Refnum],
			tsq.[Plant_AnnDateKey],
			tsq.[Plant_QtrDateKey],
			tsq.[Plant_QtrDate],
			tsq.[CompanyId],
			tsq.[AssetId],
			tsq.[AssetName],
			tsq.[SubscriberCompanyName],
			tsq.[PlantCompanyName],
			tsq.[SubscriberAssetName],
			tsq.[PlantAssetName],
			tsq.[CountryId],
			tsq.[StateName],
			tsq.[EconRegionId],
			tsq.[UomId],
			tsq.[CurrencyFcn],
			tsq.[CurrencyRpt],
			tsq.[AssetPassWord_PlainText],
			tsq.[StudyYear],
			tsq.[DataYear],
			tsq.[Consultant],
			tsq.[StudyYearDifference],
			tsq.[CalQtr]
		FROM [calc].[FoundationPlantListSource]						tsq
		WHERE	tsq.Refnum = @Refnum
			AND	tsq.FactorSetId = ISNULL(@FactorSetId, tsq.FactorSetId);



--SELECT * FROM @fpl


SELECT * FROM stgFact.FurnRely WHERE Refnum = @sRefnum