﻿DECLARE @ListIdBase		VARCHAR(25)		= '13PCH';
DECLARE @ListIdCopy		VARCHAR(25)		= '13PCHFact';
DECLARE @FactorSetId	VARCHAR(12)		= '2013';
DECLARE @StudyYear		INT				= 2013;
DECLARE @DataYear		INT				= 2013;

INSERT INTO [cons].[RefListLu]
(
	[ListID],
	[ListName],
	[ListDetail],
	[BaseList_Bit],
	[SortKey],
	[Operator],
	[Parent],
	[Hierarchy]
)
SELECT
	[t].[ListID],
	[t].[ListName],
	[t].[ListDetail],
	[t].[BaseList_Bit],
	[t].[SortKey],
	[t].[Operator],
	[t].[Parent],
	[t].[Hierarchy]
FROM (VALUES
	(@ListIdCopy, @ListIdCopy, @ListIdCopy, 0, 0, '~', @ListIdCopy, '/')
	) [t]([ListID], [ListName], [ListDetail], [BaseList_Bit], [SortKey], [Operator], [Parent], [Hierarchy])
LEFT OUTER JOIN
	[cons].[RefListLu]	[x]
		ON	[x].[ListID]		= [t].[ListID]
WHERE
	[x].[ListID]		IS NULL;

INSERT INTO [cons].[RefList]
(
	[ListID],
	[Refnum],
	[UserGroup]
)
SELECT
	@ListIdCopy,
	[l].[Refnum],
	[l].[UserGroup]
FROM
	[cons].[RefList]	[l]
LEFT OUTER JOIN
	[cons].[RefList]	[x]
		ON	[x].[ListID]	=	[l].[ListID]
		AND	[x].[Refnum]	=	[l].[Refnum]
WHERE	[l].[ListId]		= @ListIdBase
	AND	[x].[ListId]		IS NULL;

EXECUTE [fact].[Insert_Fact_ListId] @ListIdCopy, @StudyYear, @FactorSetId, @DataYear;

EXECUTE [calc].[CalculatePlant] @FactorSetId, @ListIdCopy;
