﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracOnlineFact(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 9;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					string cT09_05 = (includeSpace) ? "Table 9-5" : "Table9-5";
					wks = wkb.Worksheets[cT09_05];

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracOnlineFact]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@Tbl9051 REAL     = NULL
						r = 34;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9051", SqlDbType.Float).Value = ReturnFloat(rng); }

						string cT09_06 = (includeSpace) ? "Table 9-6" : "Table9-6";
						wks = wkb.Worksheets[cT09_06];

						//@Tbl9061 REAL     = NULL
						r = 21;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9061", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9071 REAL     = NULL
						r = 63;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9071", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracOnlineFact", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracOnlineFact]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}