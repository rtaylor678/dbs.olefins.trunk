﻿CREATE TABLE [dbo].[RankForce] (
    [RefListNo]  [dbo].[RefListNo] NOT NULL,
    [BreakID]    [dbo].[DD_ID]     NOT NULL,
    [BreakValue] VARCHAR (12)      NOT NULL,
    [NumTiles]   TINYINT           NOT NULL,
    CONSTRAINT [PK_RankForce_1] PRIMARY KEY CLUSTERED ([RefListNo] ASC, [BreakID] ASC, [BreakValue] ASC) WITH (FILLFACTOR = 90)
);

