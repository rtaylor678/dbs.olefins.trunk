﻿CREATE TABLE [dbo].[DD_Lookups] (
    [LookupID]    [dbo].[DD_ID] IDENTITY (1, 1) NOT NULL,
    [ObjectName]  [sysname]     NOT NULL,
    [ColumnName]  [sysname]     NOT NULL,
    [DescField]   [sysname]     NOT NULL,
    [Description] VARCHAR (100) NULL,
    CONSTRAINT [PK_DD_Lookups_1__14] PRIMARY KEY CLUSTERED ([LookupID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqueLookup] UNIQUE NONCLUSTERED ([ObjectName] ASC) WITH (FILLFACTOR = 90)
);

