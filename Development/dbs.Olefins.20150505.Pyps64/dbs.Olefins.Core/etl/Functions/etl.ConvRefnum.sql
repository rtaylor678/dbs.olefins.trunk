﻿CREATE FUNCTION etl.ConvRefnum
(
	@Refnum		VARCHAR(25),
	@StudyYear	SMALLINT,
	@StudyId	VARCHAR(4)
)
RETURNS VARCHAR(25)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @StrYear	VARCHAR(4) = COALESCE(CONVERT(VARCHAR(4), @StudyYear, 0), '----');

	SET @StudyId	= COALESCE(@StudyId, '----');

	SET @Refnum		= RTRIM(LTRIM(@Refnum));

	DECLARE @AssetId	VARCHAR(5)	= [etl].[ConvAssetPCH](@Refnum);

	DECLARE @AssetSuffix	VARCHAR(1) =
		CASE RIGHT(@Refnum, 1)
		WHEN 'U' THEN 'U'
		WHEN 'P' THEN 'P'
		WHEN 'R' THEN 'R'
		WHEN 'A' THEN 'A'
		ELSE ''
		END;

	SET @AssetSuffix =
		CASE RIGHT(@AssetId + @AssetSuffix, 2)
		WHEN 'AA' THEN ''
		ELSE @AssetSuffix
		END;

	RETURN @StrYear + @StudyId + @AssetId + @AssetSuffix;
	
END;
GO