﻿CREATE PROCEDURE [Console].[UpdateComments]

	@Refnum varchar(18),
	@Comments varchar(255)

AS
BEGIN

UPDATE C  SET C.Comment = @Comments  FROM CoContactInfo c join TSort t on t.ContactCode = c.ContactCode and t.StudyYear = c.StudyYear
		WHERE t.SmallRefnum=@Refnum  

END
