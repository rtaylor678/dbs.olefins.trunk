﻿CREATE PROCEDURE [Console].[GetAnIssue] 

	@Refnum varchar(18),
	@IssueTitle nvarchar(50) = null,
	@IssueId nvarchar(50) = null

AS
BEGIN

Select * from Val.Checklist
Where Refnum = dbo.FormatRefNum('20' + @Refnum,0) And ((@IssueTitle IS NULL) OR (IssueTitle = @IssueTitle)) and 
((@IssueId IS NULL) OR (IssueId = @IssueId))

END
