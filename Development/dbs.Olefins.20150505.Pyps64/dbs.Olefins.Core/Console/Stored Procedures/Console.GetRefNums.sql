﻿CREATE PROCEDURE [Console].[GetRefNums]
	@StudyYear smallint,
	@Study varchar(3)
AS
BEGIN
	SELECT substring(Refnum,3,len(Refnum)-2) as Refnum, CoLoc FROM dbo.TSort
	WHERE StudyYear = @StudyYear and CompanyId <> 'Solomon' 
	ORDER BY Refnum ASC
END
