﻿CREATE VIEW [calc].[TaAdjCycleCosts]
WITH SCHEMABINDING
AS
SELECT
	c.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	o.CurrencyRpt,
	c.StreamId,
	o.AccountId,
	p.PersIdSec,
	c.SchedId,
	c.Interval_Years * o._Amount_Cur			[TaCycleCost_Cur],
	c.Interval_Years * p.InflAdjAnn_Tot_Hrs		[TaCycle_ManHours]
FROM calc.TaAdjCapacityAggregate		c
INNER JOIN calc.OpEx					o
	ON	o.FactorSetId = c.FactorSetId
	AND	o.Refnum = c.Refnum
	AND	o.CalDateKey = c.CalDateKey
	AND	o.AccountId = 'TACurrExp'
INNER JOIN calc.PersMaintTaAggregate	p
	ON	p.FactorSetId = c.FactorSetId
	AND	p.Refnum = c.Refnum
	AND	p.CalDateKey = c.CalDateKey
	AND	p.PersIdSec = 'MaintTaMaint'
