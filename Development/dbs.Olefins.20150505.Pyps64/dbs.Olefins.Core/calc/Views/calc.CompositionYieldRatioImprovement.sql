﻿CREATE VIEW [calc].[CompositionYieldRatioImprovement]
WITH SCHEMABINDING
AS
SELECT
	t.FactorSetId,
	t.Refnum,
	t.CalDateKey,
	t.SimModelId,
	t.OpCondId,
	t.RecycleId,
	t.ComponentId,
	t.[ComponentYield_kMT]	[Simulation_kMT],
	p.Component_kMT			[Plant_kMT],
	t.[ComponentYield_kMT] / p.Component_kMT * 100.0	[Improvement_Pcnt]
FROM [calc].[SimulationCompositionYieldImprovementAggregate]				t
INNER JOIN calc.CompositionYieldRatio		p
	ON	p.FactorSetId = t.FactorSetId
	AND	p.Refnum = t.Refnum
	AND	p.CalDateKey = t.CalDateKey
	AND	p.RecycleId = t.RecycleId
	AND	p.ComponentId = t.ComponentId
	AND	p.SimModelId = 'Plant'
	AND p.OpCondId = 'OSOP'
	AND	p.Component_kMT <> 0.0
WHERE t.SimModelId <> 'Plant';
