﻿
CREATE VIEW [calc].[SpPredessors]
AS
SELECT
	i.ItemId,
	d.SpName,
	d.SelectFrom,
	p.ItemId [Predecessor]
FROM [calc].[SpDependencies]	d
INNER JOIN [calc].[SpTables]	p
	ON	p.[DataSet] = d.[SelectFrom]
INNER JOIN [calc].[SpTables]	i
	ON	i.[DataSet] = d.[InsertInto]
