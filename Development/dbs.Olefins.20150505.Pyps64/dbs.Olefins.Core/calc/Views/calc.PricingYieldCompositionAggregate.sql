﻿CREATE VIEW [calc].[PricingYieldCompositionAggregate]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.ScenarioId,
	p.Refnum,
	MAX(p.CalDateKey)	[CalDateKey],
	p.CurrencyRpt,
	p.SimModelId,
	p.OpCondId,
	p.RecycleId,
	SUM(p._Ann_Calc_Amount_Cur)	[_Ann_Calc_Amount_Cur]
FROM calc.PricingYieldComposition	p
GROUP BY
	p.FactorSetId,
	p.ScenarioId,
	p.Refnum,
	p.CurrencyRpt,
	p.SimModelId,
	p.OpCondId,
	p.RecycleId;
