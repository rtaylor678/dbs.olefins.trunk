﻿CREATE PROCEDURE [calc].[Insert_ReplacementValue_FeedPrep]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplacementValue(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReplacementValueId, ReplacementValue)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			i.CurrencyRpt,
			ff.FacilityId,
			POWER(ff.FeedRate_kBsd / 20.0, 0.6) * 3.2 * 361.3 / 355.4 * i._InflationFactor
		FROM @fpl									fpl
		INNER JOIN ante.InflationFactor				i
			--ON	i.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, i.FactorSetId) = fpl.DataYear
			AND	i.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN	fact.FacilitiesFeedFrac			ff
			ON	ff.Refnum = fpl.Refnum
			AND	ff.CalDateKey = fpl.Plant_QtrDateKey
			AND ff.FeedRate_kBsd > 0.0
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;