﻿CREATE TABLE [calc].[PricingYieldComposition] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [ScenarioId]             VARCHAR (42)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [CurrencyRpt]            VARCHAR (4)        NOT NULL,
    [SimModelId]             VARCHAR (12)       NOT NULL,
    [OpCondId]               VARCHAR (12)       NOT NULL,
    [RecycleId]              INT                NOT NULL,
    [ComponentId]            VARCHAR (42)       NOT NULL,
    [Component_kMT]          REAL               NOT NULL,
    [Component_WtPcnt]       REAL               NOT NULL,
    [Client_Amount_Cur]      REAL               NULL,
    [Region_Amount_Cur]      REAL               NULL,
    [Country_Amount_Cur]     REAL               NULL,
    [Location_Amount_Cur]    REAL               NULL,
    [Composition_Amount_Cur] REAL               NULL,
    [Energy_Amount_Cur]      REAL               NULL,
    [_Ann_Calc_Amount_Cur]   AS                 (CONVERT([real],(coalesce([Energy_Amount_Cur],[Composition_Amount_Cur],[Location_Amount_Cur],[Country_Amount_Cur],[Region_Amount_Cur],[Client_Amount_Cur])*[Component_WtPcnt])/(400.0),(1))) PERSISTED NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_PricingYieldComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_PricingYieldComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_PricingYieldComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_PricingYieldComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PricingYieldComposition] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [SimModelId] ASC, [ScenarioId] ASC, [OpCondId] ASC, [RecycleId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PricingYieldComposition_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PricingYieldComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_PricingYieldComposition_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_PricingYieldComposition_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PricingYieldComposition_Scenarios] FOREIGN KEY ([FactorSetId], [ScenarioId]) REFERENCES [ante].[PricingScenarioConfig] ([FactorSetId], [ScenarioId]),
    CONSTRAINT [FK_calc_PricingYieldComposition_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_PricingYieldComposition_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_calc_PricingYieldComposition_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_PricingYieldComposition_u
	ON  calc.PricingYieldComposition
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.PricingYieldComposition
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PricingYieldComposition.FactorSetId	= INSERTED.FactorSetId
		AND calc.PricingYieldComposition.Refnum			= INSERTED.Refnum
		AND calc.PricingYieldComposition.CalDateKey		= INSERTED.CalDateKey
		AND calc.PricingYieldComposition.ScenarioId		= INSERTED.ScenarioId
		AND calc.PricingYieldComposition.CurrencyRpt	= INSERTED.CurrencyRpt
		AND calc.PricingYieldComposition.SimModelId		= INSERTED.SimModelId
		AND calc.PricingYieldComposition.OpCondId		= INSERTED.OpCondId
		AND calc.PricingYieldComposition.RecycleId		= INSERTED.RecycleId
		AND calc.PricingYieldComposition.ComponentId	= INSERTED.ComponentId;
				
END