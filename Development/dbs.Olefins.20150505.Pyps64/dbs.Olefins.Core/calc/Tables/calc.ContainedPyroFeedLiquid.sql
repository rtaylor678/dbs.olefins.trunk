﻿CREATE TABLE [calc].[ContainedPyroFeedLiquid] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [ContainedStreamId]    VARCHAR (42)       NOT NULL,
    [Lt_LightQuantity_kMT] REAL               NULL,
    [Lt_HeavyQuantity_kMT] REAL               NULL,
    [Lt_TotalQuantity_kMT] REAL               NULL,
    [Hv_LightQuantity_kMT] REAL               NULL,
    [Hv_HeavyQuantity_kMT] REAL               NULL,
    [Hv_TotalQuantity_kMT] REAL               NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_ContainedPyroFeedLiquid_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_ContainedPyroFeedLiquid_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_ContainedPyroFeedLiquid_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_ContainedPyroFeedLiquid_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ContainedPyroFeedLiquid] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ContainedStreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ContainedPyroFeedLiquid_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ContainedPyroFeedLiquid_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ContainedPyroFeedLiquid_Stream_LookUp] FOREIGN KEY ([ContainedStreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_ContainedPyroFeedLiquid_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ContainedPyroFeedLiquid_u
	ON  calc.[ContainedPyroFeedLiquid]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[ContainedPyroFeedLiquid]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[ContainedPyroFeedLiquid].FactorSetId			= INSERTED.FactorSetId
		AND calc.[ContainedPyroFeedLiquid].Refnum				= INSERTED.Refnum
		AND calc.[ContainedPyroFeedLiquid].CalDateKey			= INSERTED.CalDateKey
		AND calc.[ContainedPyroFeedLiquid].ContainedStreamId	= INSERTED.ContainedStreamId
				
END