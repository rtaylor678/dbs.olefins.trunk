﻿CREATE TABLE [calc].[TaAdjReliability] (
    [FactorSetId]                     VARCHAR (12)       NOT NULL,
    [Refnum]                          VARCHAR (25)       NOT NULL,
    [CalDateKey]                      INT                NOT NULL,
    [SchedId]                         CHAR (1)           NOT NULL,
    [TrainId]                         INT                NOT NULL,
    [TurnAround_Date]                 DATE               NULL,
    [DataYear]                        INT                NOT NULL,
    [CurrencyRpt]                     VARCHAR (4)        NOT NULL,
    [CurrencyFcn]                     VARCHAR (4)        NOT NULL,
    [TurnAroundCost_MCur]             REAL               NULL,
    [Ann_TurnAroundCost_kCur]         REAL               NULL,
    [Ann_TurnAround_ManHrs]           REAL               NULL,
    [CostPerManHour_Cur]                  REAL               NULL,
    [InflAdj]                         REAL               NULL,
    [_InflAdj_TurnAroundCost_MCur]    AS                 (((1.0)+([InflAdj]-(1.0))/(1.2))*[TurnAroundCost_MCur]) PERSISTED,
    [_InflAdjAnn_TurnAroundCost_kCur] AS                 (((1.0)+([InflAdj]-(1.0))/(1.2))*[Ann_TurnAroundCost_kCur]) PERSISTED,
    [_InflAdj_CostPerManHour_Cur]         AS                 (((1.0)+([InflAdj]-(1.0))/(1.2))*[CostPerManHour_Cur]) PERSISTED,
    [tsModified]                      DATETIMEOFFSET (7) CONSTRAINT [DF_TaAdjReliability_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                  NVARCHAR (168)     CONSTRAINT [DF_TaAdjReliability_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                  NVARCHAR (168)     CONSTRAINT [DF_TaAdjReliability_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                   NVARCHAR (168)     CONSTRAINT [DF_TaAdjReliability_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_TaAdjReliability] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SchedId] ASC, [TrainId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_TaAdjReliability_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_TaAdjReliability_Currency_LookUp_Fcn] FOREIGN KEY ([CurrencyFcn]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_TaAdjReliability_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_TaAdjReliability_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_TaAdjReliability_Schedule_LookUp] FOREIGN KEY ([SchedId]) REFERENCES [dim].[Schedule_LookUp] ([ScheduleId]),
    CONSTRAINT [FK_calc_TaAdjReliability_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_TaAdjReliability_u
	ON  calc.TaAdjReliability
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.TaAdjReliability
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.TaAdjReliability.FactorSetId	= INSERTED.FactorSetId
		AND calc.TaAdjReliability.Refnum		= INSERTED.Refnum
		AND calc.TaAdjReliability.CalDateKey	= INSERTED.CalDateKey
		AND calc.TaAdjReliability.SchedId		= INSERTED.SchedId
		AND calc.TaAdjReliability.TrainId		= INSERTED.TrainId;
				
END