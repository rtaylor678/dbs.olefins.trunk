﻿CREATE TABLE [calc].[ReliabilityOpLossAnnualized] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [OppLossId]        VARCHAR (42)       NOT NULL,
    [Item_Count]       INT                NOT NULL,
    [DownTimeLoss_MT]  REAL               NULL,
    [SlowDownLoss_MT]  REAL               NULL,
    [TotLoss_MT]       REAL               NOT NULL,
    [DownTimeLoss_kMT] REAL               NULL,
    [SlowDownLoss_kMT] REAL               NULL,
    [TotLoss_kMT]      REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityOpLossAnnualized_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOpLossAnnualized_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOpLossAnnualized_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOpLossAnnualized_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ReliabilityOpLossAnnualized] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [OppLossId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ReliabilityOpLossAnnualized_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ReliabilityOpLossAnnualized_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ReliabilityOpLossAnnualized_OpLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_calc_ReliabilityOpLossAnnualized_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_ReliabilityOpLossAnnualized_ReliabilityProductLost]
    ON [calc].[ReliabilityOpLossAnnualized]([OppLossId] ASC)
    INCLUDE([FactorSetId], [Refnum], [TotLoss_MT]);


GO
CREATE TRIGGER calc.t_ReliabilityOpLossAnnualized_u
	ON  calc.ReliabilityOpLossAnnualized
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ReliabilityOpLossAnnualized
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ReliabilityOpLossAnnualized.FactorSetId	= INSERTED.FactorSetId
		AND calc.ReliabilityOpLossAnnualized.Refnum			= INSERTED.Refnum
		AND calc.ReliabilityOpLossAnnualized.CalDateKey		= INSERTED.CalDateKey
		AND calc.ReliabilityOpLossAnnualized.OppLossId		= INSERTED.OppLossId;
			
	
	
END;