﻿CREATE TABLE [calc].[PeerGroupCoGen] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PeerGroup]      VARCHAR (8)        NOT NULL,
    [ElecGen_MW]     REAL               NULL,
    [LowerLimit_MW]  REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupCoGen_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupCoGen_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupCoGen_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_PeerGroupCoGen_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PeerGroupCoGen] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_PeerGroupCoGen_PeerGroup] CHECK ([PeerGroup]='Noncogen' OR [PeerGroup]='Cogen'),
    CONSTRAINT [FK_calc_PeerGroupCoGen_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PeerGroupCoGen_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PeerGroupCoGen_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_PeerGroupCoGen_u
ON  calc.PeerGroupCoGen
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE calc.PeerGroupCoGen
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PeerGroupCoGen.FactorSetId		= INSERTED.FactorSetId
		AND calc.PeerGroupCoGen.Refnum			= INSERTED.Refnum
		AND calc.PeerGroupCoGen.CalDateKey		= INSERTED.CalDateKey;
		
END;