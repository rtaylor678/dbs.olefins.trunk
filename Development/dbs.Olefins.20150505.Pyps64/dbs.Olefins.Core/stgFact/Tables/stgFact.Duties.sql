﻿CREATE TABLE [stgFact].[Duties] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdId]     VARCHAR (30)       NOT NULL,
    [ImpTx]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Duties_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Duties_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Duties_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Duties_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Duties] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Duties_u]
	ON [stgFact].[Duties]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Duties]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Duties].Refnum		= INSERTED.Refnum
		AND	[stgFact].[Duties].FeedProdId	= INSERTED.FeedProdId;

END;