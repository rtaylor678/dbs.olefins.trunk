﻿CREATE TABLE [stgFact].[YieldCompositionPlant] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [SimModelId]       VARCHAR (12)       NOT NULL,
    [OpCondId]         VARCHAR (12)       NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_YieldCompositionPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_YieldCompositionPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_YieldCompositionPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_YieldCompositionPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_YieldCompositionPlant] PRIMARY KEY CLUSTERED ([Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC, [ComponentId] ASC),
    CONSTRAINT [FK_stgFact_YieldCompositionPlant_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_stgFact_YieldCompositionPlant_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_stgFact_YieldCompositionPlant_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId])
);


GO

CREATE TRIGGER [stgFact].[t_YieldCompositionPlant_u]
	ON [stgFact].[YieldCompositionPlant]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[YieldCompositionPlant]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[YieldCompositionPlant].Refnum		= INSERTED.Refnum
		AND	[stgFact].[YieldCompositionPlant].SimModelId	= INSERTED.SimModelId
		AND	[stgFact].[YieldCompositionPlant].OpCondId		= INSERTED.OpCondId
		AND	[stgFact].[YieldCompositionPlant].ComponentId	= INSERTED.ComponentId;

END;