﻿CREATE TABLE [stgFact].[FacilitiesCoolingWater]
(
	[Refnum]				VARCHAR (25)			NOT	NULL,

	[FanHorsepower_bhp]		REAL						NULL,
	[PumpHorsepower_bhp]	REAL						NULL,
	[FlowRate_TonneDay]		REAL						NULL,

	[tsModified]			DATETIMEOFFSET (7)		NOT	NULL	CONSTRAINT [DF_stgFact_FacilitiesCoolingWater_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]		NVARCHAR (168)			NOT	NULL	CONSTRAINT [DF_stgFact_FacilitiesCoolingWater_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]		NVARCHAR (168)			NOT	NULL	CONSTRAINT [DF_stgFact_FacilitiesCoolingWater_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]			NVARCHAR (168)			NOT	NULL	CONSTRAINT [DF_stgFact_FacilitiesCoolingWater_tsModifiedApp]		DEFAULT(APP_NAME()),

	CONSTRAINT [PK_stgFact_FacilitiesCoolingWater] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);
GO

CREATE TRIGGER [stgFact].[t_FacilitiesCoolingWater_u]
ON [stgFact].[FacilitiesCoolingWater]
AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[FacilitiesCoolingWater]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[FacilitiesCoolingWater].[Refnum]		= INSERTED.[Refnum];

END;