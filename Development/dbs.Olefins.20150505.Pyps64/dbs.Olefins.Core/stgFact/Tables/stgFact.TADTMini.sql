﻿CREATE TABLE [stgFact].[TADTMini]
(
	[Refnum]					VARCHAR(25)			NOT	NULL,
	[TrainId]					TINYINT				NOT	NULL,

	[MiniTaDowntime_Hrs]		REAL					NULL,
	[MiniTaCost_USD]			REAL					NULL,
	[MiniTaMaintWork_ManHrs]	REAL					NULL,
	[MiniTa_Date]				DATE					NULL,

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_TADTMini_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]			NVARCHAR(168)		NOT	NULL	CONSTRAINT [DF_TADTMini_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]			NVARCHAR(168)		NOT	NULL	CONSTRAINT [DF_TADTMini_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]				NVARCHAR(168)		NOT	NULL	CONSTRAINT [DF_TADTMini_tsModifiedApp]		DEFAULT(APP_NAME()),

	CONSTRAINT [PK_TADTMini]	PRIMARY KEY CLUSTERED([Refnum] ASC, [TrainId] ASC)
);
GO

CREATE TRIGGER [stgFact].[t_TADTMini_u]
ON [stgFact].[TADTMini]
AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[TADTMini]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[TADTMini].Refnum		= INSERTED.Refnum
		AND	[stgFact].[TADTMini].TrainId	= INSERTED.TrainId;

END;