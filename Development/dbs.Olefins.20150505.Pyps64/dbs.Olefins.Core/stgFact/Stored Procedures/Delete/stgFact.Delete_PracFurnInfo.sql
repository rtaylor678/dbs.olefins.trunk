﻿CREATE PROCEDURE [stgFact].[Delete_PracFurnInfo]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracFurnInfo]
	WHERE [Refnum] = @Refnum;

END;