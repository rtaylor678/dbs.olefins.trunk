﻿CREATE PROCEDURE [stgFact].[Delete_PracAdv]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracAdv]
	WHERE [Refnum] = @Refnum;

END;