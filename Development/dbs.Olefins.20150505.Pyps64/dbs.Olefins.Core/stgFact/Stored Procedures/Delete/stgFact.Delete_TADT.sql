﻿CREATE PROCEDURE [stgFact].[Delete_TADT]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[TADT]
	WHERE [Refnum] = @Refnum;

END;