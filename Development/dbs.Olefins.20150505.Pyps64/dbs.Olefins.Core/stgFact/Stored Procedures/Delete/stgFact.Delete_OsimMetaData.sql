﻿CREATE PROCEDURE [stgFact].[Delete_OsimMetaData]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[OsimMetaData]
	WHERE [Refnum] = @Refnum;

END;