﻿CREATE PROCEDURE [stgFact].[Delete_FeedResources]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[FeedResources]
	WHERE [Refnum] = @Refnum;

END;