﻿CREATE PROCEDURE [stgFact].[Insert_OpEx]
(
	@Refnum            VARCHAR (25),

	@OCCSal            REAL     = NULL,
	@MpsSal            REAL     = NULL,
	@OCCBen            REAL     = NULL,
	@MpsBen            REAL     = NULL,
	@MaintMatl         REAL     = NULL,
	@ContMaintLabor    REAL     = NULL,
	@ContMaintMatl     REAL     = NULL,
	@ContTechSvc       REAL     = NULL,
	@Envir             REAL     = NULL,
	@OthCont           REAL     = NULL,
	@Equip             REAL     = NULL,
	@Tax               REAL     = NULL,
	@Insur             REAL     = NULL,
	@OthNonVol         REAL     = NULL,
	@STNonVol          REAL     = NULL,
	@TAAccrual         REAL     = NULL,
	@TACurrExp         REAL     = NULL,
	@STTaExp           REAL     = NULL,
	@Chemicals         REAL     = NULL,
	@Catalysts         REAL     = NULL,
	@Royalties         REAL     = NULL,
	@PurElec           REAL     = NULL,
	@PurSteam          REAL     = NULL,
	@PurCoolingWater   REAL     = NULL,
	@PurOth            REAL     = NULL,
	@PurFG             REAL     = NULL,
	@PurLiquid         REAL     = NULL,
	@PurSolid          REAL     = NULL,
	@PPCFuelGas        REAL     = NULL,
	@PPCFuelOth        REAL     = NULL,
	@SteamExports      REAL     = NULL,
	@PowerExports      REAL     = NULL,
	@OthVol            REAL     = NULL,
	@STVol             REAL     = NULL,
	@TotCashOpEx       REAL     = NULL,
	@InvenCarry        REAL     = NULL,
	@GANonPers         REAL     = NULL,
	@Depreciation      REAL     = NULL,
	@Interest          REAL     = NULL,
	@TotRefExp         REAL     = NULL,
	@ForexRate         REAL     = NULL,
	@NonMaintEquipRent REAL     = NULL,
	@ChemBFWCW         REAL     = NULL,
	@ChemWW            REAL     = NULL,
	@ChemCaustic       REAL     = NULL,
	@ChemPretreatment  REAL     = NULL,
	@ChemAntiFoul      REAL     = NULL,
	@ChemOthProc       REAL     = NULL,
	@ChemAdd           REAL     = NULL,
	@ChemOth           REAL     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[OpEx]([Refnum], [OCCSal], [MpsSal], [OCCBen], [MpsBen], [MaintMatl], [ContMaintLabor], [ContMaintMatl], [ContTechSvc], [Envir], [OthCont], [Equip], [Tax], [Insur], [OthNonVol], [STNonVol], [TAAccrual], [TACurrExp], [STTaExp], [Chemicals], [Catalysts], [Royalties], [PurElec], [PurSteam], [PurOth], [PurFG], [PurLiquid], [PurSolid], [PPCFuelGas], [PPCFuelOth], [SteamExports], [PowerExports], [OthVol], [STVol], [TotCashOpEx], [InvenCarry], [GANonPers], [Depreciation], [Interest], [TotRefExp], [ForexRate], [NonMaintEquipRent], [ChemBFWCW], [ChemWW], [ChemCaustic], [ChemPretreatment], [ChemAntiFoul], [ChemOthProc], [ChemAdd], [ChemOth], [PurCoolingWater])
	VALUES(@Refnum, @OCCSal, @MpsSal, @OCCBen, @MpsBen, @MaintMatl, @ContMaintLabor, @ContMaintMatl, @ContTechSvc, @Envir, @OthCont, @Equip, @Tax, @Insur, @OthNonVol, @STNonVol, @TAAccrual, @TACurrExp, @STTaExp, @Chemicals, @Catalysts, @Royalties, @PurElec, @PurSteam, @PurOth, @PurFG, @PurLiquid, @PurSolid, @PPCFuelGas, @PPCFuelOth, @SteamExports, @PowerExports, @OthVol, @STVol, @TotCashOpEx, @InvenCarry, @GANonPers, @Depreciation, @Interest, @TotRefExp, @ForexRate, @NonMaintEquipRent, @ChemBFWCW, @ChemWW, @ChemCaustic, @ChemPretreatment, @ChemAntiFoul, @ChemOthProc, @ChemAdd, @ChemOth, @PurCoolingWater);

END;
