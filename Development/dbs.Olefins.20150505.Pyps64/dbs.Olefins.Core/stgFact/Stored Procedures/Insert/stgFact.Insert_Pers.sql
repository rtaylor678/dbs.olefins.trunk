﻿CREATE PROCEDURE [stgFact].[Insert_Pers]
(
	@Refnum    VARCHAR (25),
	@PersId    VARCHAR (15),

	@SortKey   INT,

	@SectionId VARCHAR (4)  = NULL,
	@NumPers   REAL      = NULL,
	@STH       REAL      = NULL,
	@OvtHours  REAL      = NULL,
	@Contract  REAL      = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Pers]([Refnum], [PersId], [SortKey], [SectionId], [NumPers], [STH], [OvtHours], [Contract])
	VALUES(@Refnum, @PersId, @SortKey, @SectionId, @NumPers, @STH, @OvtHours, @Contract);

END;
