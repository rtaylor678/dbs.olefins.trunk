﻿CREATE TABLE [inter].[YIRSupplemental] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_kMT]    REAL               NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_YIRSupplemental_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_YIRSupplemental_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_YIRSupplemental_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_YIRSupplemental_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_inter_YIRSupplemental] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_YIRSupplemental_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_YIRSupplemental_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_YIRSupplemental_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [inter].[t_Inter_YIRSupplemental_u]
	ON [inter].[YIRSupplemental]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.YIRSupplemental
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.YIRSupplemental.FactorSetId		= INSERTED.FactorSetId
		AND inter.YIRSupplemental.Refnum			= INSERTED.Refnum
		AND inter.YIRSupplemental.CalDateKey		= INSERTED.CalDateKey
		AND inter.YIRSupplemental.ComponentId		= INSERTED.ComponentId;

END;