﻿



CREATE             PROC [dbo].[spOPEX](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON
DECLARE @Currency as varchar(5)
SELECT @Currency = 'USD'

DECLARE @TotCashOPEX as real
SELECT @TotCashOpEx = o.Amount_Cur 
FROM calc.OpExAggregate o 
WHERE o.FactorSetId = @FactorSetId AND o.CurrencyRpt = @Currency AND o.AccountId='TotCashOpEx'
AND Refnum = @Refnum

DECLARE @MyDivisors TABLE 
(
	Refnum				varchar(25) NOT NULL,
	DataType			varchar(10) NOT NULL,
	DivisorFieldName	varchar(20) NOT NULL,
	DivisorValue		real NULL,
	Mult				real NULL
)

INSERT @MyDivisors (Refnum, DataType, DivisorFieldName, DivisorValue, Mult)
SELECT d.Refnum
, d.DataType
, d.Variable
, DivisorValue = d.DivisorValue
, Mult = CASE WHEN d.DataType = 'NEI' THEN 100.0 ELSE 1.0 END
from dbo.DivisorsUnPivot d
WHERE Refnum = @Refnum
AND d.DataType in ('ADJ', 'EDC', 'HVC_LB','HVC_MT','UEDC', 'ETHDiv_MT', 'OLEDiv_MT', 'NEI', 'OLECpby_MT')

INSERT @MyDivisors (Refnum, DataType, DivisorFieldName, DivisorValue, Mult) 
SELECT @Refnum, 'Pcnt', 'PcntOfTot', @TotCashOPEX/100.0, 1.0

INSERT @MyDivisors (Refnum, DataType, DivisorFieldName, DivisorValue, Mult) 
SELECT @Refnum, 'RPT', 'None', 1.0, 1.0

DECLARE @ForexRate real
Select @ForexRate = ForexRate from fact.ForexRate where Refnum = @Refnum

DELETE FROM dbo.OpEx WHERE Refnum = @Refnum
Insert into dbo.OpEx (Refnum
, DataType
, DivisorValue
, Currency
, OCCSal
, MpsSal
, SalariesWages
, OCCBen
, MpsBen
, EmployeeBenefits
, MaintRetube
, MaintMatl
, MaintContractLabor
, MaintContractMatl
, MaintEquip
, ContractTechSvc
, Envir
, ContractServOth
, NonMaintEquipRent
, Tax
, Insur
, OtherNonVol
, STNonVol
, TAAccrual
, Chemicals
, Catalysts
, Royalties
, PurElec
, PurSteam
, PurOth
, PurFuelGas
, PurLiquid
, PurSolid
, PPFCFuelGas
, PPFCOther
, ExportSteam
, ExportElectric
, OtherVol
, STVol
, TotCashOpEx
, InvenCarry
, GANonPers
, Depreciation
, Interest
, NonCash
, TotRefExp
, EnergyOpex
, NEOpex
, TACurrExp
, ForexRate
)

SELECT Refnum = @Refnum
, DataType					= d.DataType
, DivisorValue				= d.DivisorValue
, Curency					= o.CurrencyRpt
, OCCSal			= SUM(CASE WHEN o.AccountId='OCCSal' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, MpsSal			= SUM(CASE WHEN o.AccountId='MpsSal' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, SalariesWages		= SUM(CASE WHEN o.AccountId='SalariesWages' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, OCCBen			= SUM(CASE WHEN o.AccountId='OCCBen' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, MpsBen			= SUM(CASE WHEN o.AccountId='MpsBen' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, EmployeeBenefits	= SUM(CASE WHEN o.AccountId='EmployeeBenefits' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, MaintRetube		= SUM(CASE WHEN o.AccountId='MaintRetube' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, MaintMatl			= SUM(CASE WHEN o.AccountId='MaintMatl' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, MaintContractLabor = SUM(CASE WHEN o.AccountId='MaintContractLabor' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, MaintContractMatl = SUM(CASE WHEN o.AccountId='MaintContractMatl' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, MaintEquip		= SUM(CASE WHEN o.AccountId='MaintEquip' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, ContractTechSvc	= SUM(CASE WHEN o.AccountId='ContractTechSvc' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Envir				= SUM(CASE WHEN o.AccountId='Envir' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, ContractServOth	= SUM(CASE WHEN o.AccountId='ContractServOth' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, NonMaintEquipRent = SUM(CASE WHEN o.AccountId='NonMaintEquipRent' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Tax				= SUM(CASE WHEN o.AccountId='Tax' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Insur				= SUM(CASE WHEN o.AccountId='Insur' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, OtherNonVol		= SUM(CASE WHEN o.AccountId='OtherNonVol' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, STNonVol			= SUM(CASE WHEN o.AccountId='STNonVol' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, TAAccrual			= SUM(CASE WHEN o.AccountId in ('TAAccrual') THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Chemicals			= SUM(CASE WHEN o.AccountId='Chemicals' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Catalysts			= SUM(CASE WHEN o.AccountId='Catalysts' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Royalties			= SUM(CASE WHEN o.AccountId='Royalties' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PurElec			= SUM(CASE WHEN o.AccountId='PurElec' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PurSteam			= SUM(CASE WHEN o.AccountId='PurSteam' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PurOth			= SUM(CASE WHEN o.AccountId='PurOth' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PurFuelGas		= SUM(CASE WHEN o.AccountId='PurFuelGas' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PurLiquid			= SUM(CASE WHEN o.AccountId='PurLiquid' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PurSolid			= SUM(CASE WHEN o.AccountId='PurSolid' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PPFCFuelGas		= SUM(CASE WHEN o.AccountId='PPFCFuelGas' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, PPFCOther			= SUM(CASE WHEN o.AccountId in ('PPFCLiquid','PPFCOther') THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, ExportSteam		= SUM(CASE WHEN o.AccountId='ExportSteam' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, ExportElectric	= SUM(CASE WHEN o.AccountId='ExportElectric' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, OtherVol			= SUM(CASE WHEN o.AccountId='OtherVol' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, STVol				= SUM(CASE WHEN o.AccountId='STVol' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, TotCashOpEx		= SUM(CASE WHEN o.AccountId='TotCashOpEx' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, InvenCarry		= SUM(CASE WHEN o.AccountId='InvenCarry' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, GANonPers			= SUM(CASE WHEN o.AccountId='GANonPers' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Depreciation		= SUM(CASE WHEN o.AccountId='Depreciation' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, Interest			= SUM(CASE WHEN o.AccountId='Interest' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, NonCash			= SUM(CASE WHEN o.AccountId='NonCash' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, TotRefExp			= SUM(CASE WHEN o.AccountId='TotRefExp' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, EnergyOpex		= SUM(CASE WHEN o.AccountId='EnergyBalance' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, NEOpex			= (SUM(CASE WHEN o.AccountId='TotCashOpEx' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) 
					- SUM(CASE WHEN o.AccountId='EnergyBalance' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END)) / d.DivisorValue * d.Mult
, TACurrExp			= SUM(CASE WHEN o.AccountId='TACurrExp' THEN (CASE WHEN d.DataType = 'RPT' THEN o.Reported_Cur ELSE o.Amount_Cur END) ELSE 0 END) / d.DivisorValue * d.Mult
, ForexRate			= @ForexRate
FROM calc.OpExAggregate o JOIN @MyDivisors d on o.Refnum=d.Refnum and o.FactorSetId = @FactorSetId
WHERE o.CurrencyRpt = @Currency and d.DivisorValue > 0
GROUP BY o.Refnum, o.CurrencyRpt, d.DataType, d.DivisorValue, d.Mult



SET NOCOUNT OFF


















