﻿CREATE VIEW [dbo].[TSort]
WITH SCHEMABINDING
AS
SELECT
	[t].[Refnum],
	[t].[SmallRefnum],
	[t].[CalDateKey],

	[t].[Co],
	[t].[Loc],
	[t].[CoLoc],

	[t].[PlantCompanyName],
	[t].[PlantAssetName],

	[t].[UomId],
	[t].[UomNumber],
	[t].[CurrencyFcn],
	[t].[CurrencyRpt],
	[t].[DataYear],
	[t].[NumDays],

	[t].[StudyYear],
	[t].[Consultant],
	[t].[FactorSetId],
	[t].[StudyId],

	--	Asset

	[t].[AssetId],
	[t].[AssetName],
	[t].[AssetDetail],
	[t].[AssetNameReport],
	[t].[SubscriberAssetName],
	[t].[SubscriberAssetDetail],
	[t].[AssetPassWord],

	[t].[Region],
	[t].[RegionId],
	[t].[EconRegionId],
	[t].[CountryId],
	[t].[CountryName],
	[t].[StateName],

	[t].[Longitude_Deg],
	[t].[Latitude_Deg],
	[t].[GeogLoc_WGS84],

	[t].[ScenarioId],
	[t].[ScenarioName],
	[t].[ScenarioDetail],

	--	Company

	[t].[CompanyId],
	[t].[CompanyName],
	[t].[CompanyDetail],
	[t].[CompanyNameReport],
	[t].[SubscriberCompanyName],
	[t].[SubscriberCompanyDetail],
	[t].[CompanyPassWord],

	--	Contact

	[t].[NameFull],
	[t].[NameTitle],
	[t].[AddressPOBox],
	[t].[AddressStreet],
	[t].[AddressCity],
	[t].[AddressState],
	[t].[AddressCountryId],
	[t].[AddressCountryName],
	[t].[AddressPostalCode],
	[t].[NumberVoice],
	[t].[NumberFax],
	[t].[eMail],

	[t].[PricingName],
	[t].[PrincingEMail],
	[t].[DataName],
	[t].[DataEMail],

		[IsPlant]				= CASE WHEN LEFT([t].[Refnum], 4) = CONVERT(CHAR(4), [t].[StudyYear]) THEN 1 ELSE 0 END,

		[GroupId]				= [t].[Refnum],
		[Company]				= [t].[Co],
		[Location]				= [t].[Loc],

		[HomeCurrency]			= [t].[CurrencyFcn],
		[RptCurrency]			= [t].[CurrencyRpt],
		[RefNumber]				= [t].[AssetId],

		[Country]				= [t].[CountryName],

		[Comment]				= [t].[Comments],
	[t].[ContactCode],
	[t].[RefDirectory]

FROM
	[fact].[TSort]	[t];
GO