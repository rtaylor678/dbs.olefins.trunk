﻿
CREATE view [dbo].[LTTrends] AS
Select Refnum = LEFT(t.StudyYear, 2) + Rtrim(t.Refnum), 
t.StudyYear,
SourceDB = 'OlefinsLegacy',
RV_MUS = r.TotReplVal,
MaintIndexRV = MaintCostIndex * (CASE WHEN t.StudyYear <=1993 THEN 100 ELSE 1 END),
RelInd = m.ReliabInd * (CASE WHEN t.StudyYear <=1993 THEN 100 ELSE 1 END)
FROM  [$(DbOlefinsLegacy)].dbo.MaintInd m INNER JOIN  [$(DbOlefinsLegacy)].dbo.Replacement r on r.refnum=m.refnum
INNER JOIN  [$(DbOlefinsLegacy)].dbo.TSort t on t.refnum=m.refnum  
where t.StudyYear < 2007

UNION

Select g.Refnum, 
g.StudyYear, 
SourceDB = 'Olefins',
g.RV_MUS,
g.MaintIndexRV,
g.RelInd
from Olefins.dbo.GENSUM g 
where g.Studyyear >= 2007
