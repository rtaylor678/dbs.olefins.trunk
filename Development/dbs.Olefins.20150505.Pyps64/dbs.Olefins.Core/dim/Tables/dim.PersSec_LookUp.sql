﻿CREATE TABLE [dim].[PersSec_LookUp] (
    [PersSecId]      VARCHAR (22)       NOT NULL,
    [PersSecName]    NVARCHAR (84)      NOT NULL,
    [PersSecDetail]  NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PersSec_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PersSec_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PersSec_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PersSec_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_PersSec_LookUp] PRIMARY KEY CLUSTERED ([PersSecId] ASC),
    CONSTRAINT [CL_PersSec_LookUp_PersSecDetail] CHECK ([PersSecDetail]<>''),
    CONSTRAINT [CL_PersSec_LookUp_PersSecId] CHECK ([PersSecId]<>''),
    CONSTRAINT [CL_PersSec_LookUp_PersSecName] CHECK ([PersSecName]<>''),
    CONSTRAINT [UK_PersSec_LookUp_PersSecDetail] UNIQUE NONCLUSTERED ([PersSecDetail] ASC),
    CONSTRAINT [UK_PersSec_LookUp_PersSecName] UNIQUE NONCLUSTERED ([PersSecName] ASC)
);


GO

CREATE TRIGGER [dim].[t_PersSec_LookUp_u]
	ON [dim].[PersSec_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[PersSec_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[PersSec_LookUp].[PersSecId]		= INSERTED.[PersSecId];
		
END;
