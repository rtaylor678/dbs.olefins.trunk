﻿CREATE TABLE [dim].[FeedStockDistMethod_LookUp] (
    [DistMethodId]     VARCHAR (8)        NOT NULL,
    [DistMethodName]   NVARCHAR (84)      NOT NULL,
    [DistMethodDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_FeedStockDistMethod_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistMethod_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistMethod_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistMethod_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedStockDistMethod_LookUp] PRIMARY KEY CLUSTERED ([DistMethodId] ASC),
    CONSTRAINT [CL_FeedStockDistMethod_LookUp_DistMethodDetail] CHECK ([DistMethodDetail]<>''),
    CONSTRAINT [CL_FeedStockDistMethod_LookUp_DistMethodId] CHECK ([DistMethodId]<>''),
    CONSTRAINT [CL_FeedStockDistMethod_LookUp_DistMethodName] CHECK ([DistMethodName]<>''),
    CONSTRAINT [UK_FeedStockDistMethod_LookUp_DistMethodDetail] UNIQUE NONCLUSTERED ([DistMethodDetail] ASC),
    CONSTRAINT [UK_FeedStockDistMethod_LookUp_DistMethodName] UNIQUE NONCLUSTERED ([DistMethodName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FeedStockDistMethod_LookUp_u]
	ON [dim].[FeedStockDistMethod_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FeedStockDistMethod_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedStockDistMethod_LookUp].[DistMethodId]		= INSERTED.[DistMethodId];
		
END;
