﻿CREATE TABLE [dim].[Efficiency_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [EfficiencyId]       VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Efficiency_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Efficiency_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Efficiency_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Efficiency_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Efficiency_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Efficiency_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EfficiencyId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Efficiency_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Efficiency_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Efficiency_LookUp] ([EfficiencyId]),
    CONSTRAINT [FK_Efficiency_Bridge_Factor_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Efficiency_Bridge_LookUp_Efficiency] FOREIGN KEY ([EfficiencyId]) REFERENCES [dim].[Efficiency_LookUp] ([EfficiencyId]),
    CONSTRAINT [FK_Efficiency_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [EfficiencyId]) REFERENCES [dim].[Efficiency_Parent] ([FactorSetId], [EfficiencyId]),
    CONSTRAINT [FK_Efficiency_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Efficiency_Parent] ([FactorSetId], [EfficiencyId])
);


GO

CREATE TRIGGER [dim].[t_Efficiency_Bridge_u]
ON [dim].[Efficiency_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Efficiency_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Efficiency_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Efficiency_Bridge].[EfficiencyId]	= INSERTED.[EfficiencyId]
		AND	[dim].[Efficiency_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
