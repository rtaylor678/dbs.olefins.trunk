﻿CREATE TABLE [dim].[ContactType_LookUp] (
    [ContactTypeId]     VARCHAR (12)       NOT NULL,
    [ContactTypeName]   NVARCHAR (84)      NOT NULL,
    [ContactTypeDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_ContactType_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_ContactType_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_ContactType_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_ContactType_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ContactType_LookUp] PRIMARY KEY CLUSTERED ([ContactTypeId] ASC),
    CONSTRAINT [CL_ContactType_LookUp_ContactTypeDetail] CHECK ([ContactTypeDetail]<>''),
    CONSTRAINT [CL_ContactType_LookUp_ContactTypeId] CHECK ([ContactTypeId]<>''),
    CONSTRAINT [CL_ContactType_LookUp_ContactTypeName] CHECK ([ContactTypeName]<>''),
    CONSTRAINT [UK_ContactType_LookUp_ContactTypeDetail] UNIQUE NONCLUSTERED ([ContactTypeDetail] ASC),
    CONSTRAINT [UK_ContactType_LookUp_ContactTypeName] UNIQUE NONCLUSTERED ([ContactTypeName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ContactType_LookUp_u]
ON [dim].[ContactType_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ContactType_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ContactType_LookUp].[ContactTypeId]		= INSERTED.[ContactTypeId];
		
END;
