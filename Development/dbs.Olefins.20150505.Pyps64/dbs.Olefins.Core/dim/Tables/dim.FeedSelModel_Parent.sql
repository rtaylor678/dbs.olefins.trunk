﻿CREATE TABLE [dim].[FeedSelModel_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [ModelId]        VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_FeedSelModel_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedSelModel_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_FeedSelModel_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_FeedSelModel_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_FeedSelModel_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedSelModel_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ModelId] ASC),
    CONSTRAINT [CR_FeedSelModel_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_FeedSelModel_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_FeedSelModel_Parent_LookUp_ModelId] FOREIGN KEY ([ModelId]) REFERENCES [dim].[FeedSelModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_FeedSelModel_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[FeedSelModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_FeedSelModel_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[FeedSelModel_Parent] ([FactorSetId], [ModelId])
);


GO

CREATE TRIGGER [dim].[t_FeedSelModel_Parent_u]
ON [dim].[FeedSelModel_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedSelModel_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedSelModel_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[FeedSelModel_Parent].[ModelId]		= INSERTED.[ModelId];
END;