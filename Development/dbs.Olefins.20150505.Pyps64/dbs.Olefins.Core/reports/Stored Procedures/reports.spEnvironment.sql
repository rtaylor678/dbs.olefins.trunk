﻿







CREATE              PROC [reports].[spEnvironment](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint

DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
Select @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.Environment WHERE GroupId = @GroupId
Insert into reports.Environment (GroupId
, PlantCnt
, Emissions_MT
, Emissions_PcntInput
, Emissions_MTPerEDC
, WasteHazMat_MT
, WasteHazMat_PcntInput
, WasteHazMat_MTPerEDC
, NOx_LbPerMBTU
, NOX_MTperGJ
, WasteWater_MTd
, WasteWater_PerInput
, WasteWater_MTPerEDC
, EstDirectCO2E_kMT
, EstTotCO2E_kMT
, CeiDirect
, CeiTotal
, StdDirectCO2E_kMT
, StdTotCO2E_kMT)
SELECT GroupId = @GroupId
, PlantCnt = @RefCount
, Emissions_MT			= [$(DbGlobal)].dbo.WtAvgNN(e.Emissions_MT, 1)
, Emissions_PcntInput   = [$(DbGlobal)].dbo.WtAvgNN(e.Emissions_PcntInput, d.TotalPlantInput_kMT)
, Emissions_MTPerEDC    = [$(DbGlobal)].dbo.WtAvgNN(e.Emissions_MTPerEDC, d.kEdc)
, WasteHazMat_MT		= [$(DbGlobal)].dbo.WtAvgNN(e.WasteHazMat_MT, 1)
, WasteHazMat_PcntInput	= [$(DbGlobal)].dbo.WtAvgNN(e.WasteHazMat_PcntInput, d.TotalPlantInput_kMT)
, WasteHazMat_MTPerEDC  = [$(DbGlobal)].dbo.WtAvgNN(e.WasteHazMat_MTPerEDC, d.kEdc)
, NOx_LbPerMBTU			= [$(DbGlobal)].dbo.WtAvgNN(e.NOx_LbPerMBTU, e.TotPyroFuel_MBTU) 
, NOX_MTperGJ			= [$(DbGlobal)].dbo.WtAvgNN(e.NOX_MTperGJ, e.TotPyroFuel_MBTU) 
, WasteWater_MTd		= [$(DbGlobal)].dbo.WtAvgNN(e.WasteWater_MTd, 1)
, WasteWater_PerInput   = [$(DbGlobal)].dbo.WtAvgNN(e.WasteWater_PerInput, d.TotalPlantInput_kMT)
, WasteWater_MTPerEDC   = [$(DbGlobal)].dbo.WtAvgNN(e.WasteWater_MTPerEDC, d.kEdc)
, EstDirectCO2E_kMT		= [$(DbGlobal)].dbo.WtAvgNN(e.EstDirectCO2E_kMT, 1)
, EstTotCO2E_kMT		= [$(DbGlobal)].dbo.WtAvgNN(e.EstTotCO2E_kMT, 1)
, CeiDirect				= [$(DbGlobal)].dbo.WtAvg(e.CeiDirect, e.StdDirectCO2E_kMT) 
, CeiTotal				= [$(DbGlobal)].dbo.WtAvg(e.CeiTotal, e.StdTotCO2E_kMT) 
, StdDirectCO2E_kMT		= [$(DbGlobal)].dbo.WtAvg(e.StdDirectCO2E_kMT, 1) 
, StdTotCO2E_kMT		= [$(DbGlobal)].dbo.WtAvg(e.StdTotCO2E_kMT,1 )
FROM @MyGroup r JOIN dbo.Environment e on e.Refnum=r.Refnum 
JOIN dbo.Divisors d on d.Refnum=r.Refnum


SET NOCOUNT OFF


















