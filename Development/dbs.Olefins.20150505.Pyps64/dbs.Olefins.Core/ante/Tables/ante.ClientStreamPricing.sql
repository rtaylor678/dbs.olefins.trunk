﻿CREATE TABLE [ante].[ClientStreamPricing] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ClientStreamPricing_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ClientStreamPricing_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ClientStreamPricing_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ClientStreamPricing_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ClientStreamPricing] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CONSTRAINT [FK_ClientStreamPricing_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ClientStreamPricing_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_ClientStreamPricing_u]
	ON [ante].[ClientStreamPricing]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ClientStreamPricing]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[ClientStreamPricing].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[ClientStreamPricing].StreamId		= INSERTED.StreamId;

END;