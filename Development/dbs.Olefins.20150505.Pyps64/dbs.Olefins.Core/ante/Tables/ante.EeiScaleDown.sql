﻿CREATE TABLE [ante].[EeiScaleDown] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [SimModelId]     VARCHAR (12)       NOT NULL,
    [EeiScaleDown]   REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EeiScaleDown_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EeiScaleDown_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EeiScaleDown_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EeiScaleDown_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EeiScaleDown] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [SimModelId] ASC),
    CONSTRAINT [FK_EeiScaleDown_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_EeiScaleDown_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId])
);


GO

CREATE TRIGGER [ante].[t_EeiScaleDown_u]
	ON [ante].[EeiScaleDown]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EeiScaleDown]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EeiScaleDown].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[EeiScaleDown].SimModelId	= INSERTED.SimModelId;

END;