﻿CREATE TABLE [ante].[EnergySeparation] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [ComponentId]            VARCHAR (42)       NOT NULL,
    [EnergySeparation_BtuLb] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_EnergySeparation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_EnergySeparation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_EnergySeparation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_EnergySeparation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergySeparation] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_EnergySeparation_EnergySeparation_BTUlb] CHECK ([EnergySeparation_BtuLb]>=(0.0)),
    CONSTRAINT [FK_EnergySeparation_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_EnergySeparation_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_EnergySeparation_u]
	ON [ante].[EnergySeparation]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EnergySeparation]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EnergySeparation].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[EnergySeparation].ComponentId	= INSERTED.ComponentId;

END;