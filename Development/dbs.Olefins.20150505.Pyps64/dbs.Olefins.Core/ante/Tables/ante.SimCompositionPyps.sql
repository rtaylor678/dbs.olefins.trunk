﻿CREATE TABLE [ante].[SimCompositionPyps]
(
	[FactorSetId]				VARCHAR(12)			NOT	NULL	CONSTRAINT [FK_SimCompositionPyps_FactorSet_LookUp]		REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
	[ComponentId]				VARCHAR(42)			NOT	NULL	CONSTRAINT [FK_SimCompositionPyps_Component_LookUp]		REFERENCES [dim].[Component_LookUp] ([ComponentId]),

	[PypsFieldNumber]			INT					NOT	NULL	CONSTRAINT [CR_SimCompositionPyps_PypsFieldNumber]		CHECK([PypsFieldNumber] BETWEEN 1 AND 148),

	[tsModified]				DATETIMEOFFSET (7)	NOT NULL	CONSTRAINT [DF_SimCompositionPyps_tsModified]			DEFAULT(sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR (168)		NOT NULL	CONSTRAINT [DF_SimCompositionPyps_tsModifiedHost]		DEFAULT(host_name()),
	[tsModifiedUser]			NVARCHAR (168)		NOT NULL	CONSTRAINT [DF_SimCompositionPyps_tsModifiedUser]		DEFAULT(suser_sname()),
	[tsModifiedApp]				NVARCHAR (168)		NOT NULL	CONSTRAINT [DF_SimCompositionPyps_tsModifiedApp]		DEFAULT(app_name()),

	CONSTRAINT [PK_SimCompositionPyps] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
);
GO

CREATE TRIGGER [ante].[t_SimCompositionPyps_u]
	ON [ante].[SimCompositionPyps]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[SimCompositionPyps]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[SimCompositionPyps].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[SimCompositionPyps].[ComponentId]	= INSERTED.[ComponentId];

END;
GO