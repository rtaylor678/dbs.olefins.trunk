﻿CREATE VIEW fact.ReliabilityPyroFurnDownTimeAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	o.Refnum,
	o.CalDateKey,
	b.OppLossId,
	o.FurnId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + o.DownTime_Days
		WHEN '-' THEN - o.DownTime_Days
		END)			[DownTime_Days],
	COUNT_BIG(*)						[IndexItems]

FROM dim.ReliabilityOppLoss_Bridge				b
INNER JOIN fact.ReliabilityPyroFurnDownTime		o
	ON	o.OppLossId = b.DescendantId
GROUP BY
	b.FactorSetId,
	o.Refnum,
	o.CalDateKey,
	b.OppLossId,
	o.FurnId;
