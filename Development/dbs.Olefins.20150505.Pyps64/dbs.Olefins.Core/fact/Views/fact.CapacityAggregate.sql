﻿CREATE VIEW fact.CapacityAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	b.StreamId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c.Capacity_kMT
		WHEN '-' THEN - c.Capacity_kMT
		END)										[Capacity_kMT],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c._Capacity_MTd
		WHEN '-' THEN - c._Capacity_MTd
		END)										[Capacity_MTd],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c.Stream_MTd
		WHEN '-' THEN - c.Stream_MTd
		END)										[Stream_MTd],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c.Record_MTd
		WHEN '-' THEN - c.Record_MTd
		END)										[Record_MTd],
	CASE WHEN 
			SUM(
			CASE b.DescendantOperator
			WHEN '+' THEN + c.Stream_MTd
			WHEN '-' THEN - c.Stream_MTd
			END) <> 0.0
		THEN
			SUM(
			CASE b.DescendantOperator
			WHEN '+' THEN + c.Record_MTd
			WHEN '-' THEN - c.Record_MTd
			END)
			/
			SUM(
			CASE b.DescendantOperator
			WHEN '+' THEN + c.Stream_MTd
			WHEN '-' THEN - c.Stream_MTd
			END) * 100.0
		END											[CapacityRecord_Pcnt],
	COUNT_BIG(*)									[IndexItems]
FROM dim.Stream_Bridge				b
INNER JOIN fact.Capacity			c
	ON	c.StreamId = b.DescendantId
WHERE b.StreamId IN (
	'Fresh',
	'Recycle',
	'Ethylene',
	'Propylene',
	'ProdOlefins',
	'FreshPyroFeed')
GROUP BY
	b.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	b.StreamId;