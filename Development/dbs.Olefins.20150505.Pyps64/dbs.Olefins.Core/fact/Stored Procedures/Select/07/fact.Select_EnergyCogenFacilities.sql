﻿CREATE PROCEDURE [fact].[Select_EnergyCogenFacilities]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 7
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[FacilityId],
		[f].[Unit_Count],
		[f].[Capacity_MW],
		[f].[Efficiency_Pcnt],
		[n].[FacilityName]
	FROM
		[fact].[EnergyCogenFacilities]			[f]
	LEFT OUTER JOIN
		[fact].[EnergyCogenFacilitiesNameOther]	[n]
			ON	[n].[Refnum]		= [f].[Refnum]
			AND	[n].[CalDateKey]	= [f].[CalDateKey]
			AND	[n].[FacilityId]	= [f].[FacilityId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;