﻿CREATE PROCEDURE [fact].[Select_GenPlantLogistics]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-1 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[StreamId],
		[f].[CurrencyRpt],
		[f].[Amount_Cur]
	FROM
		[fact].[GenPlantLogistics]	[f]
	WHERE	[f].[Refnum]		= @Refnum
		AND	[f].[Amount_Cur]	<> 0.0;

END;