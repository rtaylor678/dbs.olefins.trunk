﻿CREATE PROCEDURE [fact].[Select_GenPlantCapEx]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-3 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[CurrencyRpt],
		[f].[Amount_Cur]
	FROM
		[fact].[GenPlantCapEx]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;