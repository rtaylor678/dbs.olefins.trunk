﻿CREATE PROCEDURE [fact].[Select_GenPlantEnergyEfficiency]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-2 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[PyroFurnInletTemp_C],
		[f].[PyroFurnPreheat_Pcnt],
		[f].[PyroFurnPrimaryTLE_Pcnt],
		[f].[PyroFurnTLESteam_PSIa],
		[f].[PyroFurnSecondaryTLE_Pcnt],
		[f].[DriverGT_Bit],
			[DriverGT_YN]	 = CASE WHEN [f].[DriverGT_Bit] = 1 THEN 'Y' ELSE 'N' END,
		[f].[PyroFurnFlueGasTemp_C],
		[f].[PyroFurnFlueGasO2_Pcnt],
		[f].[TLEOutletTemp_C],
		[f].[EnergyDeterioration_Pcnt],
		[f].[IBSLSteamRequirement_Pcnt]
	FROM
		[fact].[GenPlantEnergyEfficiency]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;