﻿CREATE PROCEDURE [fact].[Select_CapacityGrowth]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 1-2
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
			[Date_Year]				= YEAR(LEFT([f].[CalDateKey], 4)),
		[f].[StreamId],
		[f].[Prod_kMT],
		[f].[Capacity_kMT],
			[Utilization_Pcnt]		= [f].[_Utilization_Pcnt],
			[StudyYearDifference]	= [f].[_StudyYearDifference]
	FROM
		[fact].[CapacityGrowth]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;