﻿CREATE PROCEDURE [fact].[Select_FeedSelNonDiscretionary]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[Affiliated_Pcnt],
		[f].[NonAffiliated_Pcnt]
	FROM
		[fact].[FeedSelNonDiscretionary]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;