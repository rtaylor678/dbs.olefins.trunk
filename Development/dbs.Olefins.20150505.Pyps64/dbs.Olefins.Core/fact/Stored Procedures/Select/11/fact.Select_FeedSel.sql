﻿CREATE PROCEDURE [fact].[Select_FeedSel]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (5)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[MinParaffin_Pcnt],
		[f].[MaxVaporPresFRS_RVP],
		[f].[YieldPattern_Count],
		[f].[YieldPatternLP_Count],
		[f].[YieldGroup_Count],
		[f].[YPS_Lummus],
		[f].[YPS_SPYRO],
		[f].[YPS_SelfDev],
		[f].[YPS_Other],
		[f].[YPS_OtherName],
		[f].[YPS_RPT],
		[f].[LP_Aspen],
		[f].[LP_DeltaBase_No],
		[f].[LP_DeltaBase_NotSure],
		[f].[LP_DeltaBase_RPT],
		[f].[LP_DeltaBase_Yes],
		[f].[LP_Haverly],
		[f].[LP_Honeywell],
		[f].[LP_MixInt_No],
		[f].[LP_MixInt_NotSure],
		[f].[LP_MixInt_RPT],
		[f].[LP_MixInt_Yes],
		[f].[LP_Online_No],
		[f].[LP_Online_NotSure],
		[f].[LP_Online_RPT],
		[f].[LP_Online_Yes],
		[f].[LP_Other],
		[f].[LP_OtherName],
		[f].[LP_Recur_No],
		[f].[LP_Recur_NotSure],
		[f].[LP_Recur_RPT],
		[f].[LP_Recur_Yes],
		[f].[LP_RPT],
		[f].[LP_SelfDev],
		[f].[LP_RowCount],
		[f].[BeyondCrack_None],
		[f].[BeyondCrack_C4],
		[f].[BeyondCrack_C5],
		[f].[BeyondCrack_Pygas],
		[f].[BeyondCrack_RPT],
		[f].[BeyondCrack_BTX],
		[f].[BeyondCrack_OthChem],
		[f].[BeyondCrack_OthRefine],
		[f].[BeyondCrack_Oth],
		[f].[BeyondCrack_OtherName],
		[f].[DecisionPlant_Bit],
			[DecisionPlant_X]	= CASE WHEN [f].[DecisionPlant_Bit] = 1 THEN 'X' END,
		[f].[DecisionCorp_Bit],
			[DecisionCorp_X]	= CASE WHEN [f].[DecisionCorp_Bit] = 1 THEN 'X' END,
		[f].[PlanTime_Days],
		[f].[EffFeedChg_Levels],
		[f].[FeedPurchase_Levels]
	FROM
		[fact].[FeedSel]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;