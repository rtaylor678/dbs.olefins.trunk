﻿CREATE PROCEDURE [fact].[Select_PolyQuantityDesc]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 12-2 (1)
	SELECT DISTINCT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[StreamName]
	FROM
		[fact].[PolyQuantityNameOther]			[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;