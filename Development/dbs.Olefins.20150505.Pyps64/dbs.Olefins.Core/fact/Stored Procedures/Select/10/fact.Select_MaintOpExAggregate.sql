﻿CREATE PROCEDURE [fact].[Select_MaintOpExAggregate]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 10-2 (1)
	SELECT
		[f].[Refnum],
		[f].[AccountId],
		[f].[CurrencyRpt],
		[f].[CalDateKeyPrev],
		[f].[AmountPrev_Cur],
		[f].[CalDateKey],
		[f].[Amount_Cur],
		[f].[AmountAvg_Cur]
	FROM 
		[fact].[MaintOpExAggregate]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;