﻿CREATE PROCEDURE [fact].[Insert_ReliabilityPyroFurnOpEx]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityPyroFurnOpEx(Refnum, CalDateKey, FurnId, AccountId, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.FurnId
			, etl.ConvAccountID(u.AccountId)
			, 'USD'
			, u.Amount_RPT
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum]
				, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
				, f.FurnNo													[FurnId]
				, RetubeCost
				, RetubeCostMatl
				, RetubeCostLabor
				, OtherMajorCost
			FROM  stgFact.FurnRely f
			INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
			WHERE t.Refnum = @sRefnum
			) p
			UNPIVOT(
			Amount_RPT FOR AccountId IN(
				  RetubeCost
				, RetubeCostMatl
				, RetubeCostLabor
				, OtherMajorCost
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;