﻿CREATE PROCEDURE [fact].[Insert_FacilitiesMisc]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesMisc(Refnum, CalDateKey, PyroFurnSpare_Pcnt, ElecGen_MW, StreamId, StreamShip_Pcnt, AcetylLocId)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, f.PyrFurnSparePcnt * 100.0
			, f.ElecGenMW
			, CASE WHEN f.EthyLiqShipPcnt IS NOT NULL THEN 'Ethylene' END
			, f.EthyLiqShipPcnt * 100.0
			, CASE f.AcetylLoc
				WHEN 'F' THEN 'F'
				WHEN 'B' THEN 'B'
				END
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE(	f.PyrFurnSparePcnt	>= 0.0
			OR	f.ElecGenMW			>= 0.0
			OR	f.EthyLiqShipPcnt	>= 0.0
			OR	f.AcetylLoc			IS NOT NULL)
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;