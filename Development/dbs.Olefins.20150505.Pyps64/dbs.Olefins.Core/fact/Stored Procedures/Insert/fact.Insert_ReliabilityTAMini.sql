﻿CREATE PROCEDURE [fact].[Insert_ReliabilityTAMini]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[ReliabilityTAMini]
		(
			[Refnum],
			[CalDateKey],
			[TrainId],

			[CurrencyRpt],
			[DownTime_Hrs],
			[TurnAroundCost_MCur],
			[TurnAround_ManHrs],
			[TurnAround_Date]
		)
		SELECT
				[Refnum]		= etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId),
				[CalDateKey]	= etl.ConvDateKey(t.StudyYear),
			[a].[TrainId],
				[CurrencyRpt]	= 'USD',					
			[a].[MiniTaDowntime_Hrs],
			[a].[MiniTaCost_USD],
			[a].[MiniTaMaintWork_ManHrs],
			[a].[MiniTa_Date]
		FROM
			[stgFact].[TADTMini]	[a]
		INNER JOIN
			[stgFact].[TSort]		[t]
				ON	[t].[Refnum]	= [a].[Refnum]
				AND [t].[Refnum]	= @sRefnum
		WHERE	(
				[a].[MiniTaDowntime_Hrs]		> 0.0
			OR	[a].[MiniTaCost_USD]			> 0.0
			OR	[a].[MiniTaMaintWork_ManHrs]	> 0.0
			);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;