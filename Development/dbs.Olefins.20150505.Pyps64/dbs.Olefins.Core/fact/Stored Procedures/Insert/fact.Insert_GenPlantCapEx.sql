﻿CREATE PROCEDURE [fact].[Insert_GenPlantCapEx]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantCapEx(Refnum, CalDateKey, AccountId, CurrencyRpt, Amount_Cur, Budget_Cur)
		SELECT
			  p.Refnum
			, p.CalDateKey
			, p.AccountId
			, 'USD'
			, CASE WHEN p.AmountRPT >= 0.0 THEN p.AmountRPT END
			, p.BudgetRPT
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, CASE e.CptlCode
					WHEN 'CUR'	THEN 'AmountRPT'
					WHEN 'FUT'	THEN 'BudgetRPT'
					END								[CptlCode]
				, e.Onsite
				, e.Offsite
				, e.Exist
				, e.EnergyCons
				, e.Envir			[RegEnvir]
				, e.[Safety]
				, e.Computer
				, e.Maint			[MaintCap]
			FROM stgFact.CapitalExp e
			INNER JOIN stgFact.TSort t ON t.Refnum = e.Refnum
			WHERE e.CptlCode <> 'ADD'
			AND t.Refnum = @sRefnum
			) b
			UNPIVOT (
			Amount FOR AccountId IN (
				  Onsite
				, Offsite
				, Exist
				, EnergyCons
				, [RegEnvir]
				, [Safety]
				, Computer
				, [MaintCap]
				)
			) u
			PIVOT (
			MAX(Amount) FOR CptlCode IN (
				  [AmountRPT]
				, [BudgetRPT]
				)
			) p
		WHERE p.AmountRPT IS NOT NULL OR p.BudgetRPT IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;