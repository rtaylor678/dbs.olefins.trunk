﻿CREATE PROCEDURE [fact].[Insert_ReliabilityCritPath]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityCritPath(Refnum, CalDateKey, FacilityId, CritPath_Bit)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvFacilityId(u.FacilityId)							[FacilityId]
			, etl.ConvBit(u.CritPath)									[CritPath_Bit]
		FROM (
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, p.CGC
				, p.RC
				, p.TLE
				, p.OHX
				, p.Tower
				, p.Furnace
				, p.Util
				, p.CapProj
				, p.Other		[CritPathOther]
			FROM stgFact.PracTA p
			INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
			WHERE t.Refnum = @sRefnum
			) p
			UNPIVOT (
			CritPath FOR FacilityId IN (
				  p.CGC
				, p.RC
				, p.TLE
				, p.OHX
				, p.Tower
				, p.Furnace
				, p.Util
				, p.CapProj
				, p.CritPathOther
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;