﻿CREATE TABLE [fact].[MaintAccuracy] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccuracyId]     CHAR (1)           NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MaintAccuracy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MaintAccuracy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MaintAccuracy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MaintAccuracy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MaintAccuracy] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MaintAccuracy_Accuracy_LookUp] FOREIGN KEY ([AccuracyId]) REFERENCES [dim].[Accuracy_LookUp] ([AccuracyId]),
    CONSTRAINT [FK_MaintAccuracy_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MaintAccuracy_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_MaintAccuracy_u]
	ON [fact].[MaintAccuracy]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[MaintAccuracy]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[MaintAccuracy].Refnum		= INSERTED.Refnum
		AND [fact].[MaintAccuracy].CalDateKey	= INSERTED.CalDateKey;

END;