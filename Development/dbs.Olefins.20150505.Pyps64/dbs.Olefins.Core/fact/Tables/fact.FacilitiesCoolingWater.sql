﻿CREATE TABLE [fact].[FacilitiesCoolingWater]
	(
	[Refnum]				VARCHAR (25)			NOT	NULL	CONSTRAINT [FK_FacilitiesCoolingWater_TSort]				REFERENCES [fact].[TSortClient] ([Refnum]),
	[CalDateKey]			INT						NOT	NULL	CONSTRAINT [FK_FacilitiesCoolingWater_Calendar_LookUp]		REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
	[FacilityId]			VARCHAR (42)			NOT	NULL	CONSTRAINT [FK_FacilitiesCoolingWater_Facility_LookUp]		REFERENCES [dim].[Facility_LookUp] ([FacilityId]),

	[FanHorsepower_bhp]		REAL						NULL,	CONSTRAINT [CR_FacilitiesCoolingWater_FanHorsepower_bhp]	CHECK([FanHorsepower_bhp]	>= 0.0),
	[PumpHorsepower_bhp]	REAL						NULL,	CONSTRAINT [CR_FacilitiesCoolingWater_PumpHorsepower_bhp]	CHECK([PumpHorsepower_bhp]	>= 0.0),
	[FlowRate_TonneDay]		REAL						NULL,	CONSTRAINT [CR_FacilitiesCoolingWater_FlowRate_TonneDay]	CHECK([FlowRate_TonneDay]	>= 0.0),

	[tsModified]			DATETIMEOFFSET (7)		NOT	NULL	CONSTRAINT [DF_FacilitiesCoolingWater_tsModified]			DEFAULT (SYSDATETIMEOFFSET()),
	[tsModifiedHost]		NVARCHAR (168)			NOT	NULL	CONSTRAINT [DF_FacilitiesCoolingWater_tsModifiedHost]		DEFAULT (HOST_NAME()),
	[tsModifiedUser]		NVARCHAR (168)			NOT	NULL	CONSTRAINT [DF_FacilitiesCoolingWater_tsModifiedUser]		DEFAULT (SUSER_SNAME()),
	[tsModifiedApp]			NVARCHAR (168)			NOT	NULL	CONSTRAINT [DF_FacilitiesCoolingWater_tsModifiedApp]		DEFAULT (APP_NAME()),

	CONSTRAINT [PK_FacilitiesCoolingWater]						PRIMARY KEY CLUSTERED([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),

	CONSTRAINT [FK_FacilitiesCoolingWater_Facilities]			FOREIGN KEY ([Refnum], [FacilityId], [CalDateKey])			REFERENCES [fact].[Facilities]([Refnum], [FacilityId], [CalDateKey]),
);
GO

CREATE TRIGGER [fact].[t_FacilitiesCoolingWater_u]
	ON [fact].[FacilitiesCoolingWater]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesCoolingWater]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FacilitiesCoolingWater].Refnum		= INSERTED.Refnum
		AND [fact].[FacilitiesCoolingWater].FacilityId	= INSERTED.FacilityId
		AND [fact].[FacilitiesCoolingWater].CalDateKey	= INSERTED.CalDateKey;

END;