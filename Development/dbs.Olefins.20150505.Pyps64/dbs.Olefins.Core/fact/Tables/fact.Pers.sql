﻿CREATE TABLE [fact].[Pers] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [PersId]           VARCHAR (34)       NOT NULL,
    [Personnel_Count]  REAL               NULL,
    [StraightTime_Hrs] REAL               NULL,
    [OverTime_Hrs]     REAL               NULL,
    [Contract_Hrs]     REAL               NULL,
    [_Company_Hrs]     AS                 (CONVERT([real],isnull([StraightTime_Hrs],(0.0))+isnull([OverTime_Hrs],(0.0)),(1))) PERSISTED NOT NULL,
    [_Tot_Hrs]         AS                 (CONVERT([real],(isnull([StraightTime_Hrs],(0.0))+isnull([OverTime_Hrs],(0.0)))+isnull([Contract_Hrs],(0.0)),(1))) PERSISTED NOT NULL,
    [_Company_Fte]     AS                 (CONVERT([real],(isnull([StraightTime_Hrs],(0.0))+isnull([OverTime_Hrs],(0.0)))/(2080.0),(1))) PERSISTED NOT NULL,
    [_Contract_Fte]    AS                 (CONVERT([real],isnull([Contract_Hrs],(0.0))/(2080.0),(1))) PERSISTED NOT NULL,
    [_Tot_Fte]         AS                 (CONVERT([real],((isnull([StraightTime_Hrs],(0.0))+isnull([OverTime_Hrs],(0.0)))+isnull([Contract_Hrs],(0.0)))/(2080.0),(1))) PERSISTED NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_Pers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_Pers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_Pers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_Pers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Pers] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PersId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_Pers_Contract_Hrs] CHECK ([Contract_Hrs]>=(0.0)),
    CONSTRAINT [CR_Pers_OverTime_Hrs] CHECK ([OverTime_Hrs]>=(0.0)),
    CONSTRAINT [CR_Pers_Personnel_Count] CHECK ([Personnel_Count]>=(0.0)),
    CONSTRAINT [CR_Pers_StraightTime_Hrs] CHECK ([StraightTime_Hrs]>=(0.0)),
    CONSTRAINT [FK_Pers_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Pers_Pers_LookUp] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_Pers_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_Pers_u]
	ON [fact].[Pers]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Pers]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[Pers].Refnum		= INSERTED.Refnum
		AND [fact].[Pers].PersId		= INSERTED.PersId
		AND [fact].[Pers].CalDateKey	= INSERTED.CalDateKey;

END;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Full Time Equilavent (FTE) is based on 2080.0 work hours in a year.', @level0type = N'SCHEMA', @level0name = N'fact', @level1type = N'TABLE', @level1name = N'Pers', @level2type = N'COLUMN', @level2name = N'_Contract_Fte';

