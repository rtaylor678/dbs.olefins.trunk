﻿CREATE TYPE [stat].[ObservationsSmoothedResults] AS TABLE (
    [xSmoothed]   FLOAT (53)   NOT NULL,
    [ySmoothed]   FLOAT (53)   NULL,
    [FactorSetId] VARCHAR (12) NULL,
    [Refnum]      VARCHAR (25) NULL,
    [rActual]     FLOAT (53)   NULL,
    [rSmoothed]   FLOAT (53)   NULL,
    PRIMARY KEY CLUSTERED ([xSmoothed] ASC),
    CHECK (round([xSmoothed],(5))>=(0.0) AND round([xSmoothed],(5))<=(100.0)),
    CHECK ([FactorSetId]<>''),
    CHECK ([Refnum]<>''));

