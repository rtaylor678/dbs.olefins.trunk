﻿
CREATE FUNCTION [stat].[ObservationsSmoothed]
(
	@Population				stat.PopulationRandomVar	READONLY,			--	Data points to examine
	@SmoothingType			CHAR (1)	= 'N',			--	Type of smoothing				
														--		L: Linear Regression
														--		M: Mean (Simple Average)
														--		N: Newton's Divided-Difference (Lagrange Interpolating Polynomials)
														--		S: Shepard's Method (Inverse Distance Weighted Interpolation)
	@SortOrder				SMALLINT	= 1,			--	Sort order
	@EndPointMean			TINYINT		= 2,			--	Number of endpoints to average
	@iPoints				TINYINT		= 4				--	Number of observations used during interpolation or regression

)
RETURNS @ReturnTable TABLE 
(
	[xSmoothed]				FLOAT			NOT	NULL	CHECK(ROUND([xSmoothed], 5) BETWEEN 0.0 AND 100.0),
	[ySmoothed]				FLOAT				NULL,

	[FactorSetId]			VARCHAR (12)		NULL	CHECK([FactorSetId]	<> ''),
	[Refnum]				VARCHAR (25)		NULL	CHECK([Refnum]		<> ''),
	[rActual]				FLOAT				NULL,
	[rSmoothed]				FLOAT				NULL,

	PRIMARY KEY CLUSTERED ([xSmoothed] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Order			FLOAT = CAST(ABS(@SortOrder) / @SortOrder AS FLOAT);

	DECLARE @Raw TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK([FactorSetId]	<> ''),
		[Refnum]				VARCHAR (25)	NOT	NULL	CHECK([Refnum]		<> ''),

		[w]						FLOAT			NOT	NULL,
		[y]						FLOAT			NOT	NULL,
		

		[wFunction]				FLOAT				NULL,	
		[yFunction]				FLOAT				NULL,	
		[ySmoothed]				FLOAT				NULL,	

		[wTile]					FLOAT				NULL	CHECK(ROUND([wTile], 5) BETWEEN 0.0 AND 100.0),
	
		[sOrder]				SMALLINT		NOT	NULL,
		[yOrder]				AS CAST([y] * [sOrder] AS FLOAT)
								PERSISTED		NOT	NULL,

		[aRow]					SMALLINT		NOT	NULL	CHECK([aRow] >= 1),
		[dRow]					SMALLINT		NOT	NULL	CHECK([dRow] >= 1),

		[Basis]					BIT				NOT	NULL,
		[Audit]					BIT				NOT	NULL,

		PRIMARY KEY CLUSTERED ([yOrder] ASC, [w] ASC, [FactorSetId] ASC, [Refnum] ASC)
	);

	INSERT INTO @Raw(FactorSetId, Refnum, w, y, aRow, dRow, Basis, Audit, sOrder)
	SELECT
		o.FactorSetId,
		o.Refnum,
		COALESCE(o.w, 1.0)	[w],
		o.x					[y],
		ROW_NUMBER() OVER (PARTITION BY [Basis] ORDER BY o.x * @Order ASC)	[aRow],
		ROW_NUMBER() OVER (PARTITION BY [Basis] ORDER BY o.x * @Order DESC)	[dRow],
		o.Basis,
		o.Audit,
		@Order
	FROM @Population	o;

	UPDATE @Raw
	SET
		wFunction = r.wSmoothed,
		yFunction = r.ySmoothed
	FROM (
	 	SELECT
			AVG(r.w)					[wSmoothed],
			SUM(r.y * r.w) / SUM(r.w)	[ySmoothed]
		FROM @Raw	r
		WHERE r.aRow <= @EndPointMean
		) r
	WHERE aRow <= @EndPointMean;

	UPDATE @Raw
	SET
		wFunction = r.wSmoothed,
		yFunction = r.ySmoothed
	FROM (
	 	SELECT
			AVG(r.w)					[wSmoothed],
			SUM(r.y * r.w) / SUM(r.w)	[ySmoothed]
		FROM @Raw	r
		WHERE r.dRow <= @EndPointMean
		) r
	WHERE dRow <= @EndPointMean;

	UPDATE r
	SET
		r.wFunction = w,
		r.yFunction = y
	FROM @Raw	r
	WHERE	r.aRow > @EndPointMean
		AND	r.dRow > @EndPointMean;

	DECLARE @iCount		SMALLINT;
	DECLARE @wSum		FLOAT;
	DECLARE @wTile		FLOAT = CASE WHEN @Order = 1.0 THEN 0.0 ELSE 100.0 END;

	SELECT
		@iCount	= COUNT(1),
		@wSum	= SUM(COALESCE(r.wFunction, 1.0))
	FROM @Raw r;
	
	UPDATE r
	SET
		@wTile	= @wTile + COALESCE(r.wFunction, 1.0)	/ @wSum * 100.0			* @Order,
		r.wTile	= @wTile - COALESCE(r.wFunction, 1.0)	/ @wSum * 100.0 / 2.0	* @Order
	FROM @Raw r;

	---------------------------------------------------------------------------

	-- Set the interpolation points to the allowed range
	IF (@iPoints > @iCount)	BEGIN SET @iPoints = @iCount; END;
	IF (@iPoints < 2)		BEGIN SET @iPoints = 2; END;
	IF (@iPoints > 4 AND @SmoothingType = 'I')
							BEGIN SET @iPoints = 4; END;

	---------------------------------------------------------------------------

	DECLARE @x		TINYINT = 0;
	DECLARE @xMax	TINYINT = 100;
	DECLARE @xStep	TINYINT	= 1;

	---------------------------------------------------------------------------
	--	Mean
	IF (@SmoothingType = 'M')
	BEGIN

		WHILE (@x < @xMax + @xStep)
		BEGIN
			
			INSERT INTO @ReturnTable(xSmoothed, ySmoothed)
			SELECT
				@x,
				AVG(r.y)
			FROM (
				SELECT
					r.y,
					DENSE_RANK() OVER(ORDER BY ABS(@x - r.wTile) ASC) rn
				FROM @Raw	r
				) r
			WHERE r.rn <= @iPoints;
			
		SET @x = @x + @xStep;
		END;

	END;

	---------------------------------------------------------------------------
	--	Linear Regression
	IF (@SmoothingType = 'L')
	BEGIN
	
		DECLARE @b0				FLOAT;		--	First Coefficient or Term
		DECLARE @b1				FLOAT;		--	Second Coefficient or Term
	
		WHILE (@x < @xMax + @xStep)
		BEGIN

		SELECT
			@b1 = CASE WHEN (SUM(SQUARE(r.wTile)) - SQUARE(SUM(r.wTile)) / CAST(COUNT(1) AS FLOAT	)) <> 0
				THEN
					(CAST(COUNT(1) AS FLOAT	) * SUM(r.wTile * r.y) - SUM(r.wTile) * SUM(r.y)) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(r.wTile)) - SQUARE(SUM(r.wTile)))
				ELSE
					0.0
				END
		FROM (
			SELECT
				r.wTile,
				r.y,
				DENSE_RANK() OVER(ORDER BY ABS(@x - r.wTile) ASC) rn
			FROM @Raw	r
			) r
		WHERE r.rn <= @iPoints;

		SELECT
			@b0 = AVG(r.y) - @b1 * AVG(r.wTile)
		FROM (
			SELECT
				r.wTile,
				r.y,
				DENSE_RANK() OVER(ORDER BY ABS(@x - r.wTile) ASC) rn
			FROM @Raw	r
			) r
		WHERE r.rn <= @iPoints;

		INSERT INTO @ReturnTable(xSmoothed, ySmoothed)
		SELECT
			@x,
			@b0 + @b1 * @x;
			
		SET @x = @x + @xStep;
		END;

	END;

	-------------------------------------------------------------------------------------
	-- Newton's Divided-Difference (Lagrange Interpolating Polynomials)
	--IF (@SmoothingType = 'N')
	--BEGIN

	--	DECLARE @LaGrange TABLE
	--	(
	--		p	FLOAT	NOT	NULL,
	--		y	FLOAT	NOT	NULL,
	--		i	FLOAT	NOT	NULL,
			
	--		PRIMARY KEY CLUSTERED(p ASC, y ASC, i ASC)
	--	);

	--	DECLARE @y	FLOAT;
	--	DECLARE @i	INT;
	--	DECLARE @j	INT;

	--	DECLARE @pi FLOAT;
	--	DECLARE @pj FLOAT;
	--	DECLARE @yp	FLOAT;

	--	WHILE (@x < @xMax + @xStep)
	--	BEGIN

	--		SET @y = 0;
	--		SET @i = 1;
	--		DELETE FROM @LaGrange;
			
	--		INSERT INTO @LaGrange(p, y, i)
	--		SELECT 
	--			r.wTile,
	--			r.y,
	--			r.rn
	--		FROM (
	--			SELECT
	--				r.wTile,
	--				r.y,
	--				DENSE_RANK() OVER(ORDER BY ABS(@x - r.wTile) ASC) rn
	--			FROM @Raw	r
	--			) r
	--		WHERE r.rn <= @iPoints;

	--		WHILE @i <= @iPoints
	--		BEGIN

	--			SELECT
	--				@yp = l.y,
	--				@pi = l.p
	--			FROM @LaGrange	l
	--			WHERE l.i = @i;

	--			SET @j= 1;
	--			WHILE @j <= @iPoints
	--			BEGIN

	--				SELECT @pj = l.p FROM #Lagrange l WHERE l.i = @j;
	--				SET @yp = @yp * (@x - @pj)/(@pi - @pj);

	--			SET @j = @j + 1;
	--			END;

	--		SET @y = @y + @yp;
	--		END;

	--	INSERT INTO @ReturnTable(xSmoothed, ySmoothed)
	--	SELECT
	--		@x,
	--		@y;

	--	SET @x = @x + @xStep;
	--	END;

	--END;
	
	---------------------------------------------------------------------------

	UPDATE r
	SET
		r.ySmoothed = (c.ySmoothed - f.ySmoothed)/(c.xSmoothed-f.xSmoothed) * (r.wTile-f.xSmoothed)+ f.ySmoothed
	FROM @Raw r
	INNER JOIN @ReturnTable	f
		ON	f.xSmoothed = FLOOR(r.wTile)
	INNER JOIN @ReturnTable	c
		ON	c.xSmoothed = CEILING(r.wTile)
	WHERE r.Audit = 1

	--INSERT INTO @ReturnTable(xSmoothed, FactorSetId, Refnum, rActual, rSmoothed)
	--SELECT
	--	r.wTile,
	--	r.FactorSetId,
	--	r.Refnum,
	--	r.y,
	--	r.ySmoothed
	--FROM @Raw	r

	RETURN;

END
