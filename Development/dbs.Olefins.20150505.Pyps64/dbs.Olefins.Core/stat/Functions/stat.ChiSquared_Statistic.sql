﻿CREATE FUNCTION [stat].[ChiSquared_Statistic]
(@DegreesOfFreedom INT, @Probability FLOAT (53))
RETURNS FLOAT (53)
AS
 EXTERNAL NAME [Sa.Meta.Numerics].[UserDefinedFunctions].[ChiSquared_Statistic]

