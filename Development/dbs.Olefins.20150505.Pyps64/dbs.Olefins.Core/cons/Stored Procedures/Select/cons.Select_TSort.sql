﻿CREATE PROCEDURE [cons].[Select_TSort]
(
@StudyYear	INT,
@Co			VARCHAR(84),
@Loc		VARCHAR(48)
)
AS
SELECT
	[t].[Refnum],
	[t].[SmallRefnum],
	[t].[CalDateKey],

	[t].[Co],
	[t].[Loc],
	[t].[CoLoc],

	[t].[StudyYear],
	[t].[Consultant],
	[t].[FactorSetId],
	[t].[StudyId],

	--	Asset

	[t].[AssetId],
	[t].[AssetName],
	[t].[AssetDetail],
	[t].[AssetNameReport],
	[t].[SubscriberAssetName],
	[t].[SubscriberAssetDetail],
	[t].[AssetPassWord],

	[t].[Region],
	[t].[RegionId],
	[t].[EconRegionId],
	[t].[CountryId],
	[t].[CountryName],
	[t].[StateName],

	[t].[Longitude_Deg],
	[t].[Latitude_Deg],
	[t].[GeogLoc_WGS84],

	[t].[ScenarioId],
	[t].[ScenarioName],
	[t].[ScenarioDetail],

	--	Company

	[t].[CompanyId],
	[t].[CompanyName],
	[t].[CompanyDetail],
	[t].[CompanyNameReport],
	[t].[SubscriberCompanyName],
	[t].[SubscriberCompanyDetail],
	[t].[CompanyPassWord],

	[t].[Comments],
	[t].[ContactCode],
	[t].[RefDirectory]

FROM
	[cons].[TSort]					[t]	WITH (NOEXPAND)
WHERE
		[t].[StudyYear]	= @StudyYear
	AND	[t].[Co]		= @Co
	AND	[t].[Loc]		= @Loc;
GO