﻿CREATE PROCEDURE [cons].[Insert_SubscriptionsCompanies]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [cons].[SubscriptionsCompanies]
		(
			[CompanyId],
			[CalDateKey],
			[StudyId],
			[SubscriberCompanyName],
			[SubscriberCompanyDetail]
		)
		SELECT
			[m].[CompanyId],
				[CalDateKey]				= [etl].[ConvDateKey]([t].[StudyYear]),
				[StudyId]					= @StudyId,
			
				[SubscriberCompanyName]		= MAX([t].[Co]),
				[SubscriberCompanyDetail]	= MAX([t].[Co])
		FROM
			[stgFact].[TSort]				[t]
		INNER JOIN
			[etl].[MapCompanies]			[m]
				ON	[m].[Co]				= [t].[Co]
		LEFT OUTER JOIN
			[cons].[SubscriptionsCompanies]	[s]
				ON	[s].[CompanyId]		= [m].[CompanyId]
				AND	[s].[CalDateKey]	= [etl].[ConvDateKey]([t].[StudyYear])
				AND	[s].[StudyId]		= @StudyId
		WHERE	[s].[CompanyId]	IS NULL
			AND	[t].[Refnum]	= @sRefnum
		GROUP BY
			[m].[CompanyId],
			[t].[StudyYear];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;