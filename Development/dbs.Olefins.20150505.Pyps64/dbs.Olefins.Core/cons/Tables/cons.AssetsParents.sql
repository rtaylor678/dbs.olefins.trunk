﻿CREATE TABLE [cons].[AssetsParents] (
    [CalDateKey]     INT                 NOT NULL,
    [AssetId]        VARCHAR (30)        NOT NULL,
    [Parent]         VARCHAR (30)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_AssetsParents_Operator] DEFAULT ('~') NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_AssetsParents_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)      CONSTRAINT [DF_AssetsParents_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)      CONSTRAINT [DF_AssetsParents_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)      CONSTRAINT [DF_AssetsParents_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_AssetsParents] PRIMARY KEY CLUSTERED ([CalDateKey] ASC, [AssetId] ASC),
    CONSTRAINT [CR_AssetsParents_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_AssetsParents_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_AssetsParents_Parents] FOREIGN KEY ([CalDateKey], [Parent]) REFERENCES [cons].[AssetsParents] ([CalDateKey], [AssetId]),
    CONSTRAINT [FK_AssetsParents_StudyPeriods_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[StudyPeriods_LookUp] ([CalDateKey])
);


GO

CREATE TRIGGER [cons].[t_AssetsParents_u]
	ON [cons].[AssetsParents]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[AssetsParents]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[AssetsParents].AssetId		= INSERTED.AssetId
		AND	[cons].[AssetsParents].CalDateKey	= INSERTED.CalDateKey;

END;