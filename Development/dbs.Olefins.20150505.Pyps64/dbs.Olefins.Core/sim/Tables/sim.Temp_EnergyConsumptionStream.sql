﻿CREATE TABLE [sim].[Temp_EnergyConsumptionStream]
(
	[FactorSetId]		VARCHAR(12)		NOT	NULL,
	[Refnum]			VARCHAR(25)		NOT	NULL,
	[ModelId]			VARCHAR(12)		NOT	NULL,
	[OpCondId]			VARCHAR(12)		NOT	NULL,
	[StreamId]			VARCHAR(42)		NOT	NULL,
	[StreamDescription]	VARCHAR(256)	NOT	NULL,
	[RecycleId]			INT				NOT	NULL,
	[ErrorId]			VARCHAR(12)		NOT	NULL,
	[Energy_kCalKg]		REAL			NOT	NULL
);