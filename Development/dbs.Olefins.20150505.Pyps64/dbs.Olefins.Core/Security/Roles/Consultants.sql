﻿CREATE ROLE [Consultants]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CLC';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'DBB';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JSJ';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'PRD';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JAB';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JGY';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JPH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'REB';

