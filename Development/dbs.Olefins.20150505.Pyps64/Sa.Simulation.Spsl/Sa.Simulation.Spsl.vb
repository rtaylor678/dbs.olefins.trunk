﻿
''' <summary>
''' Extends the simulation error codes to SPSL.
''' </summary>
''' <remarks></remarks>
Friend Class ErrorCode
    Inherits Sa.Simulation.ErrorCode

    Friend Const ClearContents As System.Int32 = -121

End Class

''' <summary>
''' Technip Pyrotec SPYRO® in Planning and Scheduling Link Simulation Engine.
''' </summary>
''' <remarks>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </remarks>
<System.Runtime.Remoting.Contexts.Synchronization()>
Friend NotInheritable Class Simulation

    <System.ThreadStatic()> Private items As System.Int32 = 0
    <System.ThreadStatic()> Private BgwOr As Sa.Simulation.Spsl.BackgroundWorker

    Private Const SimModelID As String = "SPSL"
    Private Const fCheckSum As String = "Hash\Sa.Simulation.Spsl.SHA512.txt"

    Friend Sub New(ByVal BGWs As Sa.Simulation.Spsl.BackgroundWorker)
        Me.BgwOr = BGWs
    End Sub

    ''' <summary>
    ''' Runs an instance of the Technip Pyrotec SPYRO® simulation.
    ''' </summary>
    ''' <param name="a">Argument to pass variables to filter records to be simulated</param>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)>
    Friend Sub StartSimulation(
        ByVal a As Sa.Simulation.Threading.Argument)

        If Me.BgwOr.CancellationPending Then Exit Sub

        Dim PathEngine As String = Sa.AssemblyInfo.PathApplication + SimModelID + "\"
        Dim EngineCheckSum As String = Sa.AssemblyInfo.PathApplication + fCheckSum

        Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Verify|" + fCheckSum)

        ' Verify orignal files
        If Sa.FileOps.VerifySourceFiles(EngineCheckSum, PathEngine) Then

            Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Starting|Deleting previous results")

            Sa.Simulation.SQL.StoredProcedures.SimQueueUpdate(a.ConnectionString, a.QueueID, , DateTimeOffset.Now())
            Sa.Simulation.SQL.StoredProcedures.SimulationResultsDelete(a.ConnectionString, SimModelID, a.FactorSetID, a.Refnum)

            Sa.Simulation.SQL.StoredProcedures.SpslOutDelete(a.ConnectionString, SimModelID, a.FactorSetID, a.Refnum)
            Sa.Simulation.SQL.StoredProcedures.SpslInDelete(a.ConnectionString, SimModelID, a.FactorSetID, a.Refnum)

            Using cn As New System.Data.SqlClient.SqlConnection(a.ConnectionString)

                Using cmd As New System.Data.SqlClient.SqlCommand("sim.InputSpsl", cn)

                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = a.FactorSetID
                    cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = a.Refnum

                    cn.Open()

                    Dim sError As String = "0" 'Nothing

                    Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Starting|Retrieving records to process")

                    Using rdr As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)

                        If rdr.HasRows Then sError = Me.LoopFeeds(a.ConnectionString, a.QueueID, a.Refnum, a.FactorSetID, rdr, a.DeleteFolder)

                    End Using

                    If sError = CType(Sa.Simulation.Spsl.ErrorCode.None, String) Then

                        Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Calculating|Calculating Plant")

                        Dim r As System.Int32 = 0

                        r = Sa.Simulation.SQL.StoredProcedures.YieldCompositionPlantInsert(a.ConnectionString, a.FactorSetID, SimModelID, a.Refnum)

                        If r <= 0 Then
                            Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.PlantComposition, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Error|Calculating Plant Composition")
                            sError = "Plant Composition"
                        End If

                        r = Sa.Simulation.SQL.StoredProcedures.EnergyConsumptionPlantInsert(a.ConnectionString, a.FactorSetID, SimModelID, a.Refnum)

                        If r <= 0 Then
                            Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.PlantComposition, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Error|Calculating Plant Energy Consumption")
                            sError = "Plant Energy Consumption"
                        End If

                    End If

                    Sa.Simulation.SQL.StoredProcedures.SimQueueUpdate(a.ConnectionString, a.QueueID, , , DateTimeOffset.Now(), , Me.items, sError)

                    Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.Finished, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Finished|Simulation Exit Code: " + sError)

                End Using

            End Using

        Else

            Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.SourceFiles, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Error|Stopping simulation... Corrupted Source Files")

            Sa.Log.Write(SimModelID + " Source files in " + PathEngine + ControlChars.CrLf + "are corrupted or could not be verified." + ControlChars.CrLf + ControlChars.CrLf + "Verify these files against " + EngineCheckSum + ".", EventLogEntryType.Error, True, Sa.Log.LogName, 0)

        End If

    End Sub

    ''' <summary>
    ''' Prepares feeds for processing.
    ''' </summary>
    ''' <param name="CnString">Connection String</param>
    ''' <param name="QueueID">QueueID (sim.SimQueue)</param>
    ''' <param name="rdr">SqlDataReader for providing the values used in the simulation.</param>
    ''' <param name="DeleteFolder">Flag to delete the simulation folder: True - Deletes the folder; False - Keeps the folder</param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Private Function LoopFeeds(
        ByVal CnString As String,
        ByVal QueueID As System.Int64,
        ByVal Refnum As String,
        ByVal FactorSetID As String,
        ByVal rdr As System.Data.SqlClient.SqlDataReader,
        ByVal DeleteFolder As Boolean) As String

        Dim sLoopFeeds As String = CType(Sa.Simulation.Spsl.ErrorCode.Initilization, String)

        Dim PathEngine As String = Sa.AssemblyInfo.PathApplication + SimModelID + "\"
        Dim PathEngineTarget As String = Sa.AssemblyInfo.SystemDrive + "PyrolysisSimulations\" + SimModelID + "\"

        Dim pSimulation As String = Sa.Simulation.Actions.CopyEngine(PathEngine, PathEngineTarget, System.Guid.NewGuid.ToString())

        Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.None, QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + Refnum + "|" + FactorSetID + "|Path|" + pSimulation)

        If Sa.FileOps.VerifyFolder(PathEngine, pSimulation) Then

            Dim aError As System.Int32 = Sa.Simulation.Spsl.ErrorCode.Initilization
            Dim iError As System.Int32 = Sa.Simulation.Spsl.ErrorCode.Initilization

            Dim SpslXl As New SpslApplication(pSimulation)

            Do While rdr.Read()

                If Me.BgwOr.CancellationPending Then aError = Sa.Simulation.Spsl.ErrorCode.Cancel : Exit Do

                Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.None, QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + rdr.GetString(rdr.GetOrdinal("Refnum")) + "|" + FactorSetID + "|Running|" + rdr.GetString(rdr.GetOrdinal("StreamID")) + " (" + rdr.GetString(rdr.GetOrdinal("StreamDescription")) + "): " + rdr.GetString(rdr.GetOrdinal("OpCondID")))

                iError = SpslXl.SimulateFeed(CnString, QueueID, rdr, pSimulation)

                If iError <> Sa.Simulation.Spsl.ErrorCode.None Then

                    If iError = Sa.Simulation.Spsl.ErrorCode.Password Then

                        Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.Password, QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|||Error (" + CType(iError, String) + ")|" + SimModelID + " Password has expired.")
                        Sa.Log.Write(SimModelID + " Password has expired.", EventLogEntryType.Error, False, Sa.Log.LogName, 0)
                        sLoopFeeds = CType(iError, String)
                        Exit Do

                    Else

                        Me.BgwOr.ReportProgress(Sa.Simulation.Spsl.ErrorCode.FeedError, QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + rdr.GetString(rdr.GetOrdinal("Refnum")) + "|" + FactorSetID + "|Error (" + CType(iError, String) + ")|" + rdr.GetString(rdr.GetOrdinal("StreamID")) + " (" + rdr.GetString(rdr.GetOrdinal("StreamDescription")) + "): " + rdr.GetString(rdr.GetOrdinal("OpCondID")))
                        Sa.Log.Write(SimModelID + " Error simulating feed:" + ControlChars.CrLf + rdr.GetString(rdr.GetOrdinal("Refnum")) + ControlChars.CrLf + FactorSetID + ControlChars.CrLf + rdr.GetString(rdr.GetOrdinal("StreamID")) + "(" + rdr.GetString(rdr.GetOrdinal("StreamDescription")) + ")" + ControlChars.CrLf + rdr.GetString(rdr.GetOrdinal("OpCondID")), EventLogEntryType.Error, False, Sa.Log.LogName, 0)

                    End If

                End If

                If aError = Sa.Simulation.Spsl.ErrorCode.None Or aError = Sa.Simulation.Spsl.ErrorCode.Initilization Then
                    aError = iError
                Else
                    If aError <> iError And iError <> Sa.Simulation.Spsl.ErrorCode.None Then aError = Sa.Simulation.Spsl.ErrorCode.Notice
                End If

                Me.items = Me.items + 1

            Loop

            SpslXl.TerminateExcel()
            SpslXl = Nothing

            sLoopFeeds = CType(aError, String)

        Else

            sLoopFeeds = CType(Sa.Simulation.Spsl.ErrorCode.SourceFiles, String)

        End If

        If DeleteFolder Then Sa.FileOps.DeleteFolder(pSimulation)

        Return sLoopFeeds

    End Function

End Class

''' <summary>
''' Technip Pyrotec SPYRO® in Planning and Scheduling Link Simulation Engine.
''' </summary>
''' <remarks>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </remarks>
<System.Runtime.Remoting.Contexts.Synchronization()>
Friend NotInheritable Class SpslApplication

    Private Const SimModelID As String = "SPSL"
    Private Const TimeOut As System.Int32 = 5000

#Region " Simulation Variables "

    Private Const SpslFileName As String = "Spsl.xls"
    Private Const strWksN As String = "SpyroIn"
    Private Const strWksO As String = "SpyroOut"

    Private Const strRngIdN As String = "IdN"
    Private Const strRngIdO As String = "IdO"

    Private Const strRngQ As String = "spslQ"
    Private Const strRngD As String = "spslD"
    Private Const strRngN As String = "spslN"
    Private Const strRngO As String = "spslO"

    Private Const strRngC As String = "CompositionID"
    Private Const strRngW As String = "Composition_WtPcnt"
    Private Const strRngE As String = "Energy"
    Private Const strRngP As String = "Password"

    <System.ThreadStatic()> Private xla As Microsoft.Office.Interop.Excel.Application = Nothing
    <System.ThreadStatic()> Private wkb As Microsoft.Office.Interop.Excel.Workbook = Nothing

    <System.ThreadStatic()> Private wksN As Microsoft.Office.Interop.Excel.Worksheet = Nothing
    <System.ThreadStatic()> Private wksO As Microsoft.Office.Interop.Excel.Worksheet = Nothing

    <System.ThreadStatic()> Private rngNId As Microsoft.Office.Interop.Excel.Range = Nothing
    <System.ThreadStatic()> Private rngOId As Microsoft.Office.Interop.Excel.Range = Nothing

    <System.ThreadStatic()> Private rngQ As Microsoft.Office.Interop.Excel.Range = Nothing
    <System.ThreadStatic()> Private rngD As Microsoft.Office.Interop.Excel.Range = Nothing
    <System.ThreadStatic()> Private rngN As Microsoft.Office.Interop.Excel.Range = Nothing

    <System.ThreadStatic()> Private rngO As Microsoft.Office.Interop.Excel.Range = Nothing
    <System.ThreadStatic()> Private rngC As Microsoft.Office.Interop.Excel.Range = Nothing
    <System.ThreadStatic()> Private rngW As Microsoft.Office.Interop.Excel.Range = Nothing
    <System.ThreadStatic()> Private rngE As Microsoft.Office.Interop.Excel.Range = Nothing
    <System.ThreadStatic()> Private rngP As Microsoft.Office.Interop.Excel.Range = Nothing

    <System.ThreadStatic()> Friend ProcessXl As System.Diagnostics.Process = Nothing
    <System.ThreadStatic()> Friend ProcessID As System.IntPtr = Nothing
    <System.ThreadStatic()> Private ThreadID As System.IntPtr = Nothing

    <System.ThreadStatic()> Private MRE As System.Threading.ManualResetEvent
    <System.ThreadStatic()> Private iWorker As System.Int32

#End Region

    ''' <summary>
    ''' Forces creation of usable Excel objects.
    ''' </summary>
    ''' <param name="PathSimulationFile">Path of the SPSL Excel file (*.xls) that contains the simulation code.</param>
    ''' <remarks></remarks>
    Friend Sub New(ByVal PathSimulationFile As String)

        Me.Initialize(PathSimulationFile)

    End Sub

    ''' <summary>
    ''' Initialized Excel and the Workbook.
    ''' </summary>
    ''' <param name="PathSimulationFile">Path of the SPSL Excel file (*.xls) that contains the simulation code.</param>
    ''' <remarks></remarks>
    Friend Sub Initialize(ByVal PathSimulationFile As String)

        Me.xla = New Microsoft.Office.Interop.Excel.Application

        Me.xla.AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityLow

        Me.xla.Visible = True
        Me.xla.ScreenUpdating = True
        Me.xla.DisplayAlerts = True

        Me.wkb = Me.xla.Workbooks.Open(PathSimulationFile + SpslFileName,
            Microsoft.Office.Interop.Excel.XlUpdateLinks.xlUpdateLinksNever,
            Microsoft.Office.Interop.Excel.XlFileAccess.xlReadWrite, , , , True,
            Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, , , True, , False, False,
            Microsoft.Office.Interop.Excel.XlCorruptLoad.xlNormalLoad)

        Me.wksN = CType(Me.wkb.Worksheets(strWksN), Microsoft.Office.Interop.Excel.Worksheet)
        Me.wksO = CType(Me.wkb.Worksheets(strWksO), Microsoft.Office.Interop.Excel.Worksheet)

        Me.rngNId = Me.wksN.Range(strRngIdN)
        Me.rngOId = Me.wksO.Range(strRngIdO)

        Me.rngQ = Me.wksN.Range(strRngQ)
        Me.rngD = Me.wksN.Range(strRngD)
        Me.rngN = Me.wksN.Range(strRngN)

        Me.rngO = Me.wksO.Range(strRngO)
        Me.rngC = Me.wksO.Range(strRngC)
        Me.rngW = Me.wksO.Range(strRngW)
        Me.rngE = Me.wksO.Range(strRngE)
        Me.rngP = Me.wksO.Range(strRngP)

        Me.ThreadID = Sa.WindowHandles.GetWindowThreadProcessId(New System.IntPtr(Me.xla.Hwnd), Me.ProcessID)
        Me.ProcessXl = System.Diagnostics.Process.GetProcessById(CType(Me.ProcessID, System.Int32))

    End Sub

    ''' <summary>
    ''' Sequentially executes the simulation for a set of feeds.
    ''' </summary>
    ''' <param name="CnString">Connection String</param>
    ''' <param name="QueueID">QueueID (sim.SimQueue)</param>
    ''' <param name="rdr">SqlDataReader for providing the values used in the simulation.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Friend Function SimulateFeed(
        ByVal CnString As String,
        ByVal QueueID As System.Int64,
        ByVal rdr As System.Data.SqlClient.SqlDataReader,
        ByVal pSimulation As String) As System.Int32

        If Me.xla Is Nothing Then Me.Initialize(pSimulation)

        Dim nErrorID As System.Int32 = Sa.Simulation.Spsl.ErrorCode.None

        If nErrorID = Sa.Simulation.Spsl.ErrorCode.None Then nErrorID = Me.ClearContents()

        If nErrorID = Sa.Simulation.Spsl.ErrorCode.None Then nErrorID = Me.PrepareSpsl(rdr)

        If nErrorID = Sa.Simulation.Spsl.ErrorCode.None Then nErrorID = Me.ProcessSpsl()

        If nErrorID = Sa.Simulation.Spsl.ErrorCode.None Then

            If nErrorID = Sa.Simulation.Spsl.ErrorCode.None Then nErrorID = Me.VerifyPassword()

            If nErrorID = Sa.Simulation.Spsl.ErrorCode.None Then nErrorID = Me.PutYield(CnString, QueueID, rdr, nErrorID)

        Else

            Me.TerminateExcel()

            Sa.Simulation.SQL.StoredProcedures.PutYieldHeader(CnString, QueueID, SimModelID, rdr, nErrorID, Nothing)

        End If

        Return nErrorID

    End Function

    ''' <summary>
    ''' Clear the Excel input and output ranges.
    ''' </summary>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Friend Function ClearContents() As System.Int32

        Dim iClearContents As System.Int32 = Sa.Simulation.Spsl.ErrorCode.ClearContents

        Try

            Me.rngQ.ClearContents()
            Me.rngD.ClearContents()
            Me.rngN.ClearContents()
            Me.rngO.ClearContents()
            Me.xla.CalculateFullRebuild()

            iClearContents = Sa.Simulation.Spsl.ErrorCode.None

        Catch ex As Exception

        End Try

        Return iClearContents

    End Function

    ''' <summary>
    ''' Insert the data into Excel for the SPSL simulation.
    ''' </summary>
    ''' <param name="rdr">SqlDataReader for providing the values used in the simulation.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Private Function PrepareSpsl(
        ByVal rdr As System.Data.SqlClient.SqlDataReader) As System.Int32

        Dim iPrepareSpsl As System.Int32 = Sa.Simulation.Spsl.ErrorCode.Prepare

        Dim fName As String = Nothing

        For i As System.Int32 = 1 To Me.rngNId.Cells.Count() Step 1

            fName = CType(CType(Me.rngNId.Cells(i), Microsoft.Office.Interop.Excel.Range).Value, String)

            If Sa.Simulation.SQL.StoredProcedures.FieldExists(rdr, fName) AndAlso Not rdr.GetValue(rdr.GetOrdinal(fName)) Is DBNull.Value Then
                Me.rngQ(i) = rdr.GetValue(rdr.GetOrdinal(fName))
            End If

            fName = "d" & fName

            If Sa.Simulation.SQL.StoredProcedures.FieldExists(rdr, fName) AndAlso Not rdr.GetValue(rdr.GetOrdinal(fName)) Is DBNull.Value Then
                Me.rngD(i) = rdr.GetValue(rdr.GetOrdinal(fName))
            End If

        Next i

        If Not wkb.ReadOnly Then Me.wkb.Save()

        iPrepareSpsl = Sa.Simulation.Spsl.ErrorCode.None

        Return iPrepareSpsl

    End Function

    ''' <summary>
    ''' Prepare to run the Excel macro that executes the simulation.
    ''' </summary>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Private Function ProcessSpsl() As System.Int32

        Dim iProcessSpsl As System.Int32 = Sa.Simulation.Spsl.ErrorCode.Process

        Me.MRE = New System.Threading.ManualResetEvent(False)

        Dim tWorker As System.Threading.Thread = New System.Threading.Thread(AddressOf SpslWorker)

        tWorker.Name = "xla.Run(OpenCOM.RunSPSL)"
        tWorker.Priority = System.Threading.ThreadPriority.Normal
        tWorker.Start()

        If Me.MRE.WaitOne(TimeOut, True) Then
            iProcessSpsl = Me.iWorker
        Else
            tWorker.Abort()
        End If

        tWorker = Nothing

        Return iProcessSpsl

    End Function

    ''' <summary>
    ''' Execute the SPSL Simulation though the Public Sub <c>OpenCOM.RunSPSL</c> in Excel.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SpslWorker()

        Me.iWorker = Sa.Simulation.Spsl.ErrorCode.Worker

        Try
            Me.iWorker = CType(Me.xla.Run("OpenCOM.RunSPSL"), System.Int32)
            Me.MRE.Set()
            Me.MRE.Close()

        Catch Com As System.Runtime.InteropServices.COMException
            Me.iWorker = Sa.Simulation.Spsl.ErrorCode.Worker

        End Try

    End Sub

    ''' <summary>
    ''' Verifies the password file has not expired.
    ''' </summary>
    ''' <returns>System.Int32</returns>
    ''' <remarks>The simulation must first run to determine if the password is valid.</remarks>
    Friend Function VerifyPassword() As System.Int32

        Dim iVerify As System.Int32 = Sa.Simulation.Spsl.ErrorCode.Password

        If CType(Me.rngP.Value, Single) > 0 Then iVerify = Sa.Simulation.Spsl.ErrorCode.None

        Return iVerify

    End Function

    ''' <summary>
    ''' Insert the calculated energy adn yield into SQL Server.
    ''' </summary>
    ''' <param name="CnString">Connection String</param>
    ''' <param name="QueueID">System.Int64 representing the QueueId (q.Simulation).</param>
    ''' <param name="rdr">SqlDataReader for providing the values used in the simulation.</param>
    ''' <param name="nErrorID"></param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Private Function PutYield(
        ByVal CnString As String,
        ByVal QueueID As System.Int64,
        ByVal rdr As SqlClient.SqlDataReader,
        ByVal nErrorID As System.Int32) As System.Int32

        Using cn As New System.Data.SqlClient.SqlConnection(CnString)

            cn.Open()

            If nErrorID = 0 Then

                Dim nEnergy As System.Single = CType(Me.rngE.Value, Single)

                If nEnergy <= 0 Then nErrorID = Sa.Simulation.Spsl.ErrorCode.Energy

                Sa.Simulation.SQL.StoredProcedures.PutYieldHeader(CnString, QueueID, SimModelID, rdr, nErrorID, nEnergy)

                ' Input Differences

                Dim Value As Single

                For i As System.Int32 = 1 To Me.rngQ.Cells.Count() Step 1

                    If CType(DirectCast(Me.rngQ.Cells(i), Microsoft.Office.Interop.Excel.Range).Value, String) <> CType(DirectCast(Me.rngN.Cells(i), Microsoft.Office.Interop.Excel.Range).Value, String) Then
                        Value = CType(DirectCast(Me.rngN.Cells(i), Microsoft.Office.Interop.Excel.Range).Value, Single)
                        If Value > 0.0 Then Sa.Simulation.SQL.StoredProcedures.SpslInInsert(CnString, QueueID, SimModelID, rdr, "Input", CType(i, String), Value)
                    End If

                Next i


                ' Non-Composition Output

                For i As System.Int32 = 129 To Me.rngOId.Count() Step 1

                    Value = CType(DirectCast(Me.rngO.Cells(i), Microsoft.Office.Interop.Excel.Range).Value, Single)

                    If Value > 0.0 Then Sa.Simulation.SQL.StoredProcedures.SpslOutInsert(CnString, QueueID, SimModelID, rdr, "Output", CType(i, String), Value)

                Next i


                ' Composition

                Dim cErrorID As System.Int32 = 0
                Dim eUpdated As Boolean = False

                Dim sComp As String = Nothing
                Dim nPcnt As Single = Nothing

                For i As System.Int32 = 1 To Me.rngC.Cells.Count() Step 1

                    sComp = CType(DirectCast(Me.rngC.Cells(i), Microsoft.Office.Interop.Excel.Range).Value, String)
                    nPcnt = CType(DirectCast(Me.rngW.Cells(i), Microsoft.Office.Interop.Excel.Range).Value, Single)

                    If Not (nPcnt >= 0.0 And nPcnt <= 100.0) Then cErrorID = Sa.Simulation.Spsl.ErrorCode.Composition

                    Sa.Simulation.SQL.StoredProcedures.YieldCompositionStreamInsert(CnString, QueueID, SimModelID, rdr, sComp, nPcnt)

                    If cErrorID = Sa.Simulation.Spsl.ErrorCode.Composition AndAlso Not eUpdated Then

                        Sa.Simulation.SQL.StoredProcedures.EnergyConsumptionStreamUpdate(CnString, QueueID, SimModelID, rdr, CType((Sa.Simulation.Spsl.ErrorCode.EnergyAndComposition), String), rdr.GetString(rdr.GetOrdinal("FurnaceTypeID")), nEnergy)

                        eUpdated = True

                    End If

                Next i

            End If

            If nErrorID <> 0 AndAlso Not (nErrorID = Sa.Simulation.Spsl.ErrorCode.Energy Or nErrorID = Sa.Simulation.Spsl.ErrorCode.Composition Or nErrorID = Sa.Simulation.Spsl.ErrorCode.EnergyAndComposition) Then

                Sa.Simulation.SQL.StoredProcedures.PutYieldHeader(CnString, QueueID, SimModelID, rdr, nErrorID, Nothing)

            End If

        End Using

        Return nErrorID

    End Function

    ''' <summary>
    ''' Terminate Excel and all references to the Excel objects.
    ''' </summary>
    ''' <remarks></remarks>
    Friend Sub TerminateExcel()

        Me.CloseExcel()
        Me.ReleaseExcel()
        Me.DumpExcelReferences()
        Me.KillExcel()

    End Sub

    ''' <summary>
    ''' Close the Excel workbook and application.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CloseExcel()

        If Not Me.wkb Is Nothing Then

            Try
                Me.wkb.Close(False)
            Catch ex As System.Runtime.InteropServices.COMException
                Sa.Log.Write("Error closing Excel Workbook." + ControlChars.CrLf + ControlChars.CrLf + ex.Message, EventLogEntryType.Error, False, Sa.Log.LogName, 0)
            End Try

        End If

        If Not Me.xla Is Nothing Then

            Dim cls As String = Nothing
            Dim txt As String = Nothing
            Dim h As System.IntPtr = Nothing

            cls = "#32770"
            txt = "run-time error "
            h = Sa.WindowHandles.ReturnWindowHandle(Me.ThreadID, cls, txt)

            If Sa.WindowHandles.IsWindow(h) Then
                Sa.WindowHandles.SendMessageA(h, Sa.WindowHandles.WM_CLOSE, IntPtr.Zero, IntPtr.Zero)
            End If

            If Not Me.ProcessXl.HasExited Then

                Try
                    Me.xla.Application.Quit()
                Catch ex As System.Runtime.InteropServices.COMException
                    Sa.Log.Write("Error quitting Excel Applicaion." + ControlChars.CrLf + ControlChars.CrLf + ex.Message, EventLogEntryType.Error, False, Sa.Log.LogName, 0)
                End Try

            End If

        End If

    End Sub

    ''' <summary>
    ''' Release the Excel InteropServices from use by the simulation engine.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ReleaseExcel()

        If Not Me.rngNId Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngNId)
        If Not Me.rngOId Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngOId)

        If Not Me.rngQ Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngQ)
        If Not Me.rngD Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngD)
        If Not Me.rngN Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngN)

        If Not Me.rngO Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngO)
        If Not Me.rngC Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngC)
        If Not Me.rngW Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngW)
        If Not Me.rngE Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngE)
        If Not Me.rngP Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.rngP)

        If Not Me.wksN Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.wksN)
        If Not Me.wksO Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.wksO)

        If Not Me.wkb Is Nothing Then System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.wkb)

        If Not Me.xla Is Nothing Then

            Try
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(Me.xla)
            Catch ex As Exception
                Sa.Log.Write("Error releasing Excel Application ComObject." + ControlChars.CrLf + ControlChars.CrLf + ex.Message, EventLogEntryType.Error, False, Sa.Log.LogName, 0)
            End Try

        End If

        System.GC.WaitForPendingFinalizers()
        System.GC.Collect()
        System.GC.WaitForFullGCComplete()

    End Sub

    ''' <summary>
    ''' Set the Excel objects to nothing.
    ''' </summary>
    ''' <remarks>The combination of CloseExcel, Release Excel, and DumpExcelReferences completely closes Excel and destroys all references to the Excel objects.</remarks>
    Private Sub DumpExcelReferences()

        Me.rngNId = Nothing
        Me.rngOId = Nothing

        Me.rngQ = Nothing
        Me.rngD = Nothing
        Me.rngN = Nothing

        Me.rngO = Nothing
        Me.rngC = Nothing
        Me.rngW = Nothing
        Me.rngE = Nothing
        Me.rngP = Nothing

        Me.wksN = Nothing
        Me.wksO = Nothing

        Me.wkb = Nothing
        Me.xla = Nothing

    End Sub

    ''' <summary>
    ''' Abruptly terminates excel.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub KillExcel()

        If Not Me.ProcessXl Is Nothing Then
            Me.ProcessXl.Kill()
        End If

        Me.ProcessXl = Nothing
        Me.ProcessID = Nothing
        Me.ThreadID = Nothing

    End Sub

End Class

