﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Sa.Chem
{
	public sealed partial class Pyps : Simulation
	{
		private const string modelId = "PYPS";

		private sealed class Constants
		{
			internal const string EngineVerification = "Engine.Pyps.HashContents";

			internal const string FileUsrSol = "USRSOL.EXE";
			internal const string FileSolInp = "SOLINP.DAT";
			internal const string FileMdlOut = "MDLOUT.DAT";
			internal const string FileNapIn = "NAPIN.DAT";
			internal const string FileCoeYld = "COEYLD.DAT";
			internal const string FileTemp = "TEMP1.DAT";

			internal const string OpCondMs25 = "MS25";
			internal const string MdlOutError = "**********";

			//	Zero Based list; these values should be one (1) less than the column number
			internal const int IndexNapInN = 10;
			internal const int IndexNapInG = 11;
			internal const int IndexNapIn1 = 12;
			internal const int IndexNapIn2 = 13;

			internal const int IndexIbpInN = 81;
			internal const int IndexIbpInG = 94;
			internal const int IndexIbpIn1 = 103;
			internal const int IndexIbpIn2 = 117;

			internal const int IndexSgInN = 80;
			internal const int IndexSgInG = 93;
			internal const int IndexSgIn1 = 102;
			internal const int IndexSgIn2 = 116;

			internal const int ItemsSolInp = 158;
			internal const int ItemsMdlOut = 25;

			internal static int PypsTimeOut
			{
				get
				{
					return (Environment.Is64BitOperatingSystem) ? 250 : 1500;
				}
			}
		}

		private sealed class PypsErrorCode
		{
			internal const int SolInpCreation = -530;
			internal const int SolInpError = -530;

			internal const int MdlOutCreation = -530;
			internal const int MdlOutError = -530;

			internal const int NapInVerify = -530;
			internal const int NapInReset = -530;

			internal const int ExecutePyps = -530;

			/// <summary>
			/// USRSOL.EXE exited improperly.
			/// </summary>
			/// <remarks>
			/// USRSOL.EXE returns the the exit code <c>129</c> when failing.
			/// </remarks>
			internal const int UsrSolExit = 129;

			/// <summary>
			/// USRSOL.EXE enountered a <c>divide by zero</c> operation.
			/// </summary>
			/// <remarks>
			/// USRSOL.EXE returns the exit code <c>131</c> when a <c>divide by zero</c> error occurs.
			/// </remarks>
			internal const int UsrSolDivZ = 131;
		}

		internal sealed class Engine : AEngine
		{
			public Engine(string pathEngine = "Engine.Pyps", string pathIsolated = "Engine.Temp", bool autoDelete = false)
				: base(pathEngine, pathIsolated, autoDelete)
			{
			}

			protected override bool VerifyEngine()
			{
				List<string> badFiles = new	List<string>();
				return Sa.FileOps.VerifyFolderContents(Constants.EngineVerification, this.PathIsolated, out badFiles);
			}

			public override void SimulateFeed(Feed feed, IInputReader iRdr, IOutputWriter oWtr)
			{
				this.ResetEngine();

				SolInput solInput = new SolInput(iRdr.Read().InputItems);

				if (this.VerifyEngine() && iRdr.Verified())
				{
					//IInputWriter iWtr = new SolInpWriter(this.PathIsolated, Constants.FileSolInp);
					IInputWriter iWtr = new SolInpWriterSaveFeed(this.PathIsolated, Constants.FileSolInp, feed);
					iWtr.Write(solInput);

					if (iWtr.Exists())
					{
						if (FileOps.WaitForUnlockedFiles(this.PathIsolated))
						{
							this.Execute();

							//IOutputReader oRdr = new MdlOutReader(this.PathIsolated, Constants.FileMdlOut);
							IOutputReader oRdr = new MdlOutReaderSaveFeed(this.PathIsolated, Constants.FileMdlOut, feed);

							if (oRdr.Exists() && oRdr.Verified())
							{
								SimResults simResults = oRdr.Read(feed);

								if (UploadResults(solInput)) { oWtr.Write(feed, simResults); }
							}
						}
					}
				}
			}

			protected override void ResetEngine()
			{
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileSolInp);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileMdlOut);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileTemp);

				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileCoeYld);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileNapIn);

				Sa.FileOps.Copyfile(this.PathEngine + Constants.FileCoeYld, this.PathIsolated + Constants.FileCoeYld, true);
				Sa.FileOps.Copyfile(this.PathEngine + Constants.FileNapIn, this.PathIsolated + Constants.FileNapIn, true);
			}

			private bool UploadResults(SolInput solInput)
			{
				if (solInput.IsLiquid)
				{
					if (!VerifyNapIn(solInput))
					{
						return ResetNapId(solInput);
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}

			private List<string> ReadNapIn()
			{
				string napin = File.ReadAllText(this.PathIsolated + Constants.FileNapIn, Encoding.ASCII);
				napin = Regex.Replace(napin, @"\s+", Environment.NewLine);

				return (!string.IsNullOrEmpty(napin)) ? new List<string>(napin.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)) : new List<string>();
			}

			private bool VerifyNapIn(SolInput solInput)
			{
				int e = 0;

				List<string> NapIn = ReadNapIn();

				if (NapIn.Count >= 11)
				{
					for (int i = 5; i <= 11; i++)
					{
						if (Single.Parse(NapIn[i], NumberFormatInfo.InvariantInfo) == solInput.InputItems[i + solInput.IndexIbp - 5].FValue)
						{
							e++;
						}
					}
				}

				return (e == 7);
			}

			private bool ResetNapId(SolInput solInput)
			{
				SolInput tSolInput = new SolInput(solInput.InputItems);

				//	Increase the specific gravity
				tSolInput.InputItems[solInput.IndexDensity].FValue = solInput.Density + 0.1F;

				//	Reduce the initial boiling point
				tSolInput.InputItems[solInput.IndexIbp].FValue = solInput.InitialBoilingPoint - 5F;

				//	Reset all files
				ResetEngine();

				//	Run simulation with temporary values
				IInputWriter iWtr = new SolInpWriter(this.PathIsolated, Constants.FileSolInp);
				iWtr.Write(tSolInput);
				this.Execute();

				//	Delete SolInp and MdlOut files
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileSolInp);
				Sa.FileOps.DeleteFile(this.PathIsolated + Constants.FileMdlOut);

				//	Run simulation with original values
				iWtr.Write(solInput);
				this.Execute();

				//	Verify NapIn
				return this.VerifyNapIn(solInput);
			}

			private sealed class SolInpWriter : IInputWriter
			{
				private readonly string SolInpPath;
				private readonly string SolInpFile;

				private string SolInp
				{
					get
					{
						return SolInpPath + SolInpFile;
					}
				}

				internal SolInpWriter(string path, string file)
				{
					this.SolInpPath = path;
					this.SolInpFile = file;
				}

				public void Write(SimInput simInput)
				{
					string str = string.Empty;

					foreach (InputItem item in simInput.InputItems)
					{
						str = str + " " + item.FValue + " ";
					}

					File.WriteAllText(this.SolInp, str, Encoding.ASCII);
				}

				public bool Exists()
				{
					return (File.Exists(SolInp));
				}
			}

			private sealed class SolInpWriterSaveFeed : IInputWriter
			{
				private readonly string SolInpPath;
				private readonly string SolInpFile;
				private readonly Feed feed;
				
				private string SolInp
				{
					get
					{
						return SolInpPath + SolInpFile;
					}
				}

				private string SolFeed
				{
					get
					{
						string fileName = feed.refnum + " " + feed.streamId + " " + feed.streamDescription.Replace("\\", "-") + " " + feed.opCondId + " f127-" + feed.f127.ToString() + " f130-" + feed.f130.ToString() + " f131-" + feed.f131.ToString() + " f132-" + feed.f132.ToString() + " (" + OSBit() + "-bit OS)";
						return "C:\\PYPS IO\\" + fileName + " SOLINP.txt";
					}
				}

				internal SolInpWriterSaveFeed(string path, string file, Feed feed)
				{
					this.SolInpPath = path;
					this.SolInpFile = file;
					this.feed = feed;
				}

				public void Write(SimInput simInput)
				{
					string str = string.Empty;

					foreach (InputItem item in simInput.InputItems)
					{
						str = str + " " + item.FValue + " ";
					}

					File.WriteAllText(this.SolInp, str, Encoding.ASCII);
					File.WriteAllText(this.SolFeed, str, Encoding.ASCII);
				}

				public bool Exists()
				{
					return (File.Exists(SolInp));
				}
			}

			private class MdlOutReader : IOutputReader
			{
				private readonly string SolMdlOutPath;
				private readonly string SolMdlOutFile;

				private List<string> mdlOut;

				internal string SolMdlOut
				{
					get
					{
						return SolMdlOutPath + SolMdlOutFile;
					}
				}

				internal MdlOutReader(string path, string file)
				{
					this.SolMdlOutPath = path;
					this.SolMdlOutFile = file;
				}

				public bool Exists()
				{
					return File.Exists(this.SolMdlOut);
				}

				public bool Verified()
				{
					this.mdlOut = this.ReadMdlOut();
					return (this.mdlOut.Count == Constants.ItemsMdlOut);
				}

				private List<string> ReadMdlOut()
				{
					string output = File.ReadAllText(this.SolMdlOut, Encoding.ASCII);

					return (!string.IsNullOrEmpty(output))
						? new List<string>(output.Split(Environment.NewLine.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries))
						: new List<string>();
				}

				public SimResults Read(Feed feed)
				{
					Yield yield = ReadYield(feed.opCondId, this.mdlOut);
					Energy energy = ReadEnergy(feed.opCondId, this.mdlOut);

					return new SimResults(yield, energy);
				}

				private Yield ReadYield(string opCondId, List<string> mdlOut)
				{
					float fCompWt = float.NaN;
					string sCompId;

					Yield yield = new Yield();

					for (int i = 4; i <= mdlOut.Count - 3; i++)
					{
						sCompId = ConvertComponentId(mdlOut[i].Substring(1, 14).Trim());
						fCompWt = ParseMdlOutLine(opCondId, mdlOut[i]);

						yield.Add(new Component(sCompId, fCompWt));
					}

					return yield;
				}

				private Energy ReadEnergy(string opCondId, List<string> mdlOut)
				{
					float fEnergy = float.NaN;

					fEnergy = ParseMdlOutLine(opCondId, mdlOut[24]);

					return new Energy(fEnergy);
				}

				private static float ParseMdlOutLine(string opCondId, string mdlOutLine)
				{
					string sOutput = null;
					float fOutput = float.NaN;

					if (opCondId != Constants.OpCondMs25)
					{
						sOutput = mdlOutLine.Substring(21, 12).Trim();
					}
					else
					{
						sOutput = mdlOutLine.Substring(31, mdlOutLine.Length - 31).Trim();
					}

					if (sOutput != Constants.MdlOutError && IsNumeric(sOutput))
					{
						fOutput = float.Parse(sOutput, System.Globalization.NumberFormatInfo.InvariantInfo);
					}

					return fOutput;
				}

				/// <summary>
				/// Converts the MDLOUT.DAT composition names to the Solomon ComponentID (dim.CompositionLu)
				/// </summary>
				/// <param name="pypsComponent">MDLOUT.DAT compsition name.</param>
				/// <returns>String</returns>
				/// <remarks>
				/// C9-400F and FUEL OIL transformation defined by Calcs!YIRCalc (Yield Prediction)
				/// </remarks>
				private static string ConvertComponentId(string pypsComponent)
				{
					string ComponentId = string.Empty;

					switch (pypsComponent)
					{
						case "HYDROGEN":
							ComponentId = "H2";
							break;
						case "METHANE":
							ComponentId = "CH4";
							break;
						case "ACETYLENE":
							ComponentId = "C2H2";
							break;
						case "ETHYLENE":
							ComponentId = "C2H4";
							break;
						case "ETHANE":
							ComponentId = "C2H6";
							break;
						case "MA + PD":
							ComponentId = "C3H4";
							break;
						case "PROPYLENE":
							ComponentId = "C3H6";
							break;
						case "PROPANE":
							ComponentId = "C3H8";
							break;
						case "BUTADIENE":
							ComponentId = "C4H6";
							break;
						case "BUTENE":
							ComponentId = "C4H8";
							break;
						case "BUTANE":
							ComponentId = "C4H10";
							break;
						case "C5-S":
							ComponentId = "C5S";
							break;
						case "C6-C8 NA":
							ComponentId = "C6C8NA";
							break;
						case "BENZENE":
							ComponentId = "C6H6";
							break;
						case "TOLUENE":
							ComponentId = "C7H8";
							break;
						case "XY + ETB":
							ComponentId = "C8H10";
							break;
						case "STYRENE":
							ComponentId = "C8H8";
							break;
						case "C9-400F":
							ComponentId = "PyroGasOil";
							break;
						case "FUEL OIL":
							ComponentId = "PyroFuelOil";
							break;
						default:
							ComponentId = string.Empty;
							break;
					}
					return ComponentId;
				}

				private static bool IsNumeric(string value)
				{
					float v;
					return (float.TryParse(value, out v));
				}
			}

			private class MdlOutReaderSaveFeed : IOutputReader
			{
				private readonly string SolMdlOutPath;
				private readonly string SolMdlOutFile;
				private readonly Feed feed;

				private List<string> mdlOut;

				internal string SolMdlOut
				{
					get
					{
						return SolMdlOutPath + SolMdlOutFile;
					}
				}

				internal MdlOutReaderSaveFeed(string path, string file, Feed feed)
				{
					this.SolMdlOutPath = path;
					this.SolMdlOutFile = file;
					this.feed = feed;
				}

				public bool Exists()
				{
					return File.Exists(this.SolMdlOut);
				}

				public bool Verified()
				{
					this.mdlOut = this.ReadMdlOut();
					return (this.mdlOut.Count == Constants.ItemsMdlOut);
				}

				private List<string> ReadMdlOut()
				{
					string output = File.ReadAllText(this.SolMdlOut, Encoding.ASCII);

					string fileName = feed.refnum + " " + feed.streamId + " " + feed.streamDescription.Replace("\\", "-") + " " + feed.opCondId + " f127-" + feed.f127.ToString() + " f130-" + feed.f130.ToString() + " f131-" + feed.f131.ToString() + " f132-" + feed.f132.ToString() + " (" + OSBit() + "-bit OS)";
					string p = "C:\\PYPS IO\\" + fileName + " MDLOUT.txt";
					File.WriteAllText(p, output, Encoding.ASCII);

					return (!string.IsNullOrEmpty(output))
						? new List<string>(output.Split(Environment.NewLine.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries))
						: new List<string>();
				}

				public SimResults Read(Feed feed)
				{
					Yield yield = ReadYield(feed.opCondId, this.mdlOut);
					Energy energy = ReadEnergy(feed.opCondId, this.mdlOut);

					return new SimResults(yield, energy);
				}

				private Yield ReadYield(string opCondId, List<string> mdlOut)
				{
					float fCompWt = float.NaN;
					string sCompId;

					Yield yield = new Yield();

					for (int i = 4; i <= mdlOut.Count - 3; i++)
					{
						sCompId = ConvertComponentId(mdlOut[i].Substring(1, 14).Trim());
						fCompWt = ParseMdlOutLine(opCondId, mdlOut[i]);

						yield.Add(new Component(sCompId, fCompWt));
					}

					return yield;
				}

				private Energy ReadEnergy(string opCondId, List<string> mdlOut)
				{
					float fEnergy = float.NaN;

					fEnergy = ParseMdlOutLine(opCondId, mdlOut[24]);

					return new Energy(fEnergy);
				}

				private static float ParseMdlOutLine(string opCondId, string mdlOutLine)
				{
					string sOutput = null;
					float fOutput = float.NaN;

					if (opCondId != Constants.OpCondMs25)
					{
						sOutput = mdlOutLine.Substring(21, 12).Trim();
					}
					else
					{
						sOutput = mdlOutLine.Substring(31, mdlOutLine.Length - 31).Trim();
					}

					if (sOutput != Constants.MdlOutError && IsNumeric(sOutput))
					{
						fOutput = float.Parse(sOutput, System.Globalization.NumberFormatInfo.InvariantInfo);
					}

					return fOutput;
				}

				/// <summary>
				/// Converts the MDLOUT.DAT composition names to the Solomon ComponentID (dim.CompositionLu)
				/// </summary>
				/// <param name="pypsComponent">MDLOUT.DAT compsition name.</param>
				/// <returns>String</returns>
				/// <remarks>
				/// C9-400F and FUEL OIL transformation defined by Calcs!YIRCalc (Yield Prediction)
				/// </remarks>
				private static string ConvertComponentId(string pypsComponent)
				{
					string ComponentId = string.Empty;

					switch (pypsComponent)
					{
						case "HYDROGEN":
							ComponentId = "H2";
							break;
						case "METHANE":
							ComponentId = "CH4";
							break;
						case "ACETYLENE":
							ComponentId = "C2H2";
							break;
						case "ETHYLENE":
							ComponentId = "C2H4";
							break;
						case "ETHANE":
							ComponentId = "C2H6";
							break;
						case "MA + PD":
							ComponentId = "C3H4";
							break;
						case "PROPYLENE":
							ComponentId = "C3H6";
							break;
						case "PROPANE":
							ComponentId = "C3H8";
							break;
						case "BUTADIENE":
							ComponentId = "C4H6";
							break;
						case "BUTENE":
							ComponentId = "C4H8";
							break;
						case "BUTANE":
							ComponentId = "C4H10";
							break;
						case "C5-S":
							ComponentId = "C5S";
							break;
						case "C6-C8 NA":
							ComponentId = "C6C8NA";
							break;
						case "BENZENE":
							ComponentId = "C6H6";
							break;
						case "TOLUENE":
							ComponentId = "C7H8";
							break;
						case "XY + ETB":
							ComponentId = "C8H10";
							break;
						case "STYRENE":
							ComponentId = "C8H8";
							break;
						case "C9-400F":
							ComponentId = "PyroGasOil";
							break;
						case "FUEL OIL":
							ComponentId = "PyroFuelOil";
							break;
						default:
							ComponentId = string.Empty;
							break;
					}
					return ComponentId;
				}

				private static bool IsNumeric(string value)
				{
					float v;
					return (float.TryParse(value, out v));
				}
			}
			
			private static string OSBit()
			{
				return (Environment.Is64BitOperatingSystem) ? "64" : "32";
			}

			protected override int Execute()
			{
				int rtn = ErrorCode.ExecuteSimulation;

				using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
				{
					proc.EnableRaisingEvents = true;

					proc.StartInfo.CreateNoWindow = true;
					proc.StartInfo.FileName = this.PathIsolated + Constants.FileUsrSol;
					proc.StartInfo.UseShellExecute = false;
					proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
					proc.StartInfo.WorkingDirectory = this.PathIsolated;

					rtn = Execute(proc);

					// Re-run if PYPS improperly exits.
					// Occasionally, PYPS will improperly exit but will run normally when executed a second time.
					if (rtn == PypsErrorCode.UsrSolExit)
					{
						rtn = Execute(proc);
					}
				}

				return rtn;
			}

			private static int Execute(System.Diagnostics.Process proc)
			{
				int rtn = PypsErrorCode.ExecutePyps;

				proc.Start();
				proc.WaitForExit(Constants.PypsTimeOut);

				if (proc.HasExited)
				{
					rtn = proc.ExitCode;
				}
				else
				{
					try
					{
						if (!proc.HasExited) proc.Kill();
					}
					catch (InvalidOperationException ex)
					{
						rtn = ErrorCode.ExecuteKill;
						Console.WriteLine(ex);
						//Sa.Log.Write("Could not kill " & proc.ProcessName & " (" & proc.Id & ") process", EventLogEntryType.Error, False, Sa.Log.LogName, 0)
					}
				}

				proc.Close();
				GC.Collect();

				return rtn;
			}
		}
	}
}