﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Maintenance
		{
			internal class Routine
			{
				private static RangeReference CompMaintHours_Pcnt = new RangeReference(Tabs.T10_02, 0, 9, SqlDbType.Float, 100.0);

				private static RangeReference MaintCorrEmergancy = new RangeReference(Tabs.T10_02, 36, 9, SqlDbType.Float, 100.0);
				private static RangeReference MaintCorrProcess = new RangeReference(Tabs.T10_02, 37, 9, SqlDbType.Float, 100.0);
				private static RangeReference MaintPrevCond = new RangeReference(Tabs.T10_02, 39, 9, SqlDbType.Float, 100.0);
				private static RangeReference MaintPrevCondAdd = new RangeReference(Tabs.T10_02, 40, 9, SqlDbType.Float, 100.0);
				private static RangeReference MaintPrevSys = new RangeReference(Tabs.T10_02, 41, 9, SqlDbType.Float, 100.0);
				private static RangeReference MaintPrevSysAdd = new RangeReference(Tabs.T10_02, 42, 9, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MaintExpense]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "AccountId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("CompMaintHours_Pcnt", Routine.CompMaintHours_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("MaintCorrEmergancy", Routine.MaintCorrEmergancy);
							d.Add("MaintCorrProcess", Routine.MaintCorrProcess);
							d.Add("MaintPrevCond", Routine.MaintPrevCond);
							d.Add("MaintPrevCondAdd", Routine.MaintPrevCondAdd);
							d.Add("MaintPrevSys", Routine.MaintPrevSys);
							d.Add("MaintPrevSysAdd", Routine.MaintPrevSysAdd);

							return d;
						}
					}
				}
			}
		}
	}
}