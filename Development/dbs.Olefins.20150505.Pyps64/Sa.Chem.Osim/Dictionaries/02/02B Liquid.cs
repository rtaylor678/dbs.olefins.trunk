﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal class Liquid
			{
				private static RangeReference Density_SG = new RangeReference(Tabs.T02_A2, 5, 0, SqlDbType.Float);
				private static RangeReference S_PPM = new RangeReference(Tabs.T02_A2, 6, 0, SqlDbType.Float);

				private static RangeReference DistMethodID = new RangeReference(Tabs.T02_A2, 8, 0, SqlDbType.VarChar);
				private static RangeReference D000 = new RangeReference(Tabs.T02_A2, 9, 0, SqlDbType.Float);
				private static RangeReference D005 = new RangeReference(Tabs.T02_A2, 10, 0, SqlDbType.Float);
				private static RangeReference D010 = new RangeReference(Tabs.T02_A2, 11, 0, SqlDbType.Float);
				private static RangeReference D030 = new RangeReference(Tabs.T02_A2, 12, 0, SqlDbType.Float);
				private static RangeReference D050 = new RangeReference(Tabs.T02_A2, 13, 0, SqlDbType.Float);
				private static RangeReference D070 = new RangeReference(Tabs.T02_A2, 14, 0, SqlDbType.Float);
				private static RangeReference D090 = new RangeReference(Tabs.T02_A2, 15, 0, SqlDbType.Float);
				private static RangeReference D095 = new RangeReference(Tabs.T02_A2, 16, 0, SqlDbType.Float);
				private static RangeReference D100 = new RangeReference(Tabs.T02_A2, 17, 0, SqlDbType.Float);

				private static RangeReference P = new RangeReference(Tabs.T02_A2, 20, 0, SqlDbType.Float, 100.0);
				private static RangeReference I = new RangeReference(Tabs.T02_A2, 21, 0, SqlDbType.Float, 100.0);
				private static RangeReference N = new RangeReference(Tabs.T02_A2, 22, 0, SqlDbType.Float, 100.0);
				private static RangeReference O = new RangeReference(Tabs.T02_A2, 23, 0, SqlDbType.Float, 100.0);
				private static RangeReference A = new RangeReference(Tabs.T02_A2, 24, 0, SqlDbType.Float, 100.0);

				private static RangeReference H2 = new RangeReference(string.Empty, 26, 0, SqlDbType.Float, 100.0);

				public static Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("NaphthaLt", Streams.NaphthaLt);
						d.Add("NaphthaFr", Streams.NaphthaFr);
						d.Add("NaphthaHv", Streams.NaphthaHv);
						d.Add("Raffinate", Streams.Raffinate);
						d.Add("HeavyNGL", Streams.HeavyNGL);
						d.Add("Condensate", Streams.Condensate);
						d.Add("Diesel", Streams.Diesel);
						d.Add("GasOilHv", Streams.GasOilHv);
						d.Add("GasOilHt", Streams.GasOilHt);
						d.Add("FeedLiqOther1", Streams.FeedLiqOther1);
						d.Add("FeedLiqOther2", Streams.FeedLiqOther2);
						d.Add("FeedLiqOther3", Streams.FeedLiqOther3);

						return d;
					}
				}

						
				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_StreamsLiquid]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return new Streams().LookUpColumn;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Density_SG", Liquid.Density_SG);
							d.Add("S_PPM", Liquid.S_PPM);

							d.Add("DistMethodID", Liquid.DistMethodID);
							d.Add("D000", Liquid.D000);
							d.Add("D005", Liquid.D005);
							d.Add("D010", Liquid.D010);
							d.Add("D030", Liquid.D030);
							d.Add("D050", Liquid.D050);
							d.Add("D070", Liquid.D070);
							d.Add("D090", Liquid.D090);
							d.Add("D095", Liquid.D095);
							d.Add("D100", Liquid.D100);

							d.Add("P", Liquid.P);
							d.Add("I", Liquid.I);
							d.Add("N", Liquid.N);
							d.Add("O", Liquid.O);
							d.Add("A", Liquid.A);

							d.Add("H2", Liquid.H2);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Liquid.Items;
						}
					}
				}

				internal class Upload : TransferData.IUploadMultiple
				{
					public string StoredProcedure
					{
						get
						{
							return "[stgFact].[Insert_FeedQuality]";
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Liquid.Items;
						}
					}

					public Dictionary<string, RangeReference> Parameters
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("@FeedProdID", Feed.FeedProdId);

							d.Add("@SG", Liquid.Density_SG);
							d.Add("@SulfurPPM", Liquid.S_PPM);

							d.Add("@ASTMethod", Liquid.DistMethodID);
							d.Add("@IBP", Liquid.D000);
							d.Add("@D5pcnt", Liquid.D005);
							d.Add("@D10Pcnt", Liquid.D010);
							d.Add("@D30Pcnt", Liquid.D030);
							d.Add("@D50Pcnt", Liquid.D050);
							d.Add("@D70Pcnt", Liquid.D070);
							d.Add("@D90Pcnt", Liquid.D090);
							d.Add("@D95Pcnt", Liquid.D095);
							d.Add("@EP", Liquid.D100);

							d.Add("@NParaffins", Liquid.P);
							d.Add("@IsoParaffins", Liquid.I);
							d.Add("@Naphthenes", Liquid.N);
							d.Add("@Olefins", Liquid.O);
							d.Add("@Aromatics", Liquid.A);

							d.Add("@Hydrogen", Liquid.H2);

							d.Add("@CoilOutletPressure", Feed.CoilOutletPressure_Psia);
							d.Add("@SteamHCRatio", Feed.SteamHydrocarbon_Ratio);
							d.Add("@CoilOutletTemp", Feed.CoilOutletTemp_C);

							d.Add("@EthyleneYield", Feed.EthyleneYield_WtPcnt);
							d.Add("@PropylEthylRatio", Feed.PropyleneEthylene_Ratio);
							d.Add("@FeedConv", Feed.FeedConv_WtPcnt);
							d.Add("@PropylMethaneRatio", Feed.PropyleneMethane_Ratio);

							d.Add("@ConstrYear", Feed.FurnConstruction_Year);
							d.Add("@ResTime", Feed.ResidenceTime_s);

							d.Add("@OthLiqFeedDESC", Feed.StreamDesc);
							d.Add("@OthLiqFeedPriceBasis", Feed.Amount_Cur);

							return d;
						}
					}
				}

			}
		}
	}
}