﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem
{
    public class Database
    {
		public static void InsertEnergy(string factorSetId, string refnum, string modelId, string opCondId, string streamId, string streamDescription, int recycleId, string errorId, float energy_kCalKg)
		{
			string Procedure = "[sim].[Insert_Temp_EnergyConsumptionStream]";

			string cString = Sa.Database.dbConnectionString("DBS1", "Olefins", "rrh", "rrh#4279");

			using (SqlConnection cn = new SqlConnection(cString))
			{
				using (SqlCommand cmd = new SqlCommand(Procedure, cn))
				{
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@FactorSetId", System.Data.SqlDbType.VarChar, 12).Value = factorSetId;
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;
					cmd.Parameters.Add("@ModelId", SqlDbType.VarChar, 12).Value = modelId;
					cmd.Parameters.Add("@OpCondId", SqlDbType.VarChar, 12).Value = opCondId;
					cmd.Parameters.Add("@StreamId", SqlDbType.VarChar, 42).Value = streamId;
					cmd.Parameters.Add("@StreamDescription", SqlDbType.VarChar, 256).Value = streamDescription;
					cmd.Parameters.Add("@RecycleId", SqlDbType.Int).Value = recycleId;
					cmd.Parameters.Add("@ErrorId", SqlDbType.VarChar, 12).Value = errorId;

					cmd.Parameters.Add("@Energy_kCalKg", SqlDbType.Real).Value = energy_kCalKg;

					cmd.Connection.Open();

					cmd.ExecuteNonQuery();

					cmd.Connection.Close();
				}
			}
		}

		public static void InsertComposition(SqlConnection cn, string factorSetId, string refnum, string modelId, string opCondId, string streamId, string streamDescription, int recycleId, string errorId, string componentId, float component_WtPcnt)
		{
			string Procedure = "[sim].[Insert_Temp_YieldCompositionStream]";

			using (SqlCommand cmd = new SqlCommand(Procedure, cn))
			{
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@FactorSetId", System.Data.SqlDbType.VarChar, 12).Value = factorSetId;
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;
				cmd.Parameters.Add("@ModelId", SqlDbType.VarChar, 12).Value = modelId;
				cmd.Parameters.Add("@OpCondId", SqlDbType.VarChar, 12).Value = opCondId;
				cmd.Parameters.Add("@StreamId", SqlDbType.VarChar, 42).Value = streamId;
				cmd.Parameters.Add("@StreamDescription", SqlDbType.VarChar, 256).Value = streamDescription;
				cmd.Parameters.Add("@RecycleId", SqlDbType.Int).Value = recycleId;
				cmd.Parameters.Add("@ErrorId", SqlDbType.VarChar, 12).Value = errorId;

				cmd.Parameters.Add("@ComponentId", SqlDbType.VarChar, 42).Value = componentId;
				cmd.Parameters.Add("@Component_WtPcnt", SqlDbType.Real).Value = component_WtPcnt;

				cmd.Connection.Open();

				cmd.ExecuteNonQuery();

				cmd.Connection.Close();
			}
		}
    }
}