﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
//using System.Windows;

namespace Sa.Chem
{
	public abstract class SimulationMT
	{
		public abstract class ThreadManager
		{
			public int maxThreadCount;

			public ThreadManager(int maxThreadCount)
			{
				this.maxThreadCount = maxThreadCount;
			}

			public Queue<Plant> engineQueue = new Queue<Plant>();
			public List<EngineWorker> engineWorkers = new List<EngineWorker>();

			public abstract void AddToQueue();

			public abstract void OnDoWork(EngineWorker engineWorker, DoWorkEventArgs e);
			public abstract void OnProgressChanged(EngineWorker engineWorker, ProgressChangedEventArgs e);

			public delegate void DoneHandler();
			public event DoneHandler Done;

			public void OnRunWorkerCompleted(EngineWorker engineWorker, RunWorkerCompletedEventArgs e)
			{
				if (this.engineQueue.Count > 0)
				{
					engineWorker.RunWorkerAsync(this.engineQueue.Dequeue());
				}
				else
				{
					this.engineWorkers.Remove(engineWorker);

					if (this.engineWorkers.Count == 0)
					{
						Done();
					}
				}
			}

			public void ProcessQueue()
			{
				int threads = Math.Min(this.maxThreadCount, this.engineQueue.Count);

				if (engineWorkers.Count < threads)
				{
					for (int t = 0; t < threads; t++)
					{
						Plant plant = this.engineQueue.Dequeue();

						Console.WriteLine(plant.refnum);

						engineWorkers.Add(new EngineWorker(this));
						engineWorkers[t].RunWorkerAsync(plant);
					}
				}
			}

			public void StopProcesses()
			{
				this.engineQueue.Clear();

				Parallel.ForEach(this.engineWorkers,
					engineWorker =>
					{
						engineWorker.CancelAsync();
						engineWorker.Dispose();
					}
				);

				this.engineWorkers.Clear();
				this.Done();
			}
		}

		public class EngineWorker : BackgroundWorker
		{
			protected ThreadManager threadManager;

			public EngineWorker(ThreadManager threadManager)
			{
				base.WorkerReportsProgress = true;
				base.WorkerSupportsCancellation = true;
				this.threadManager = threadManager;
			}

			protected override void OnDoWork(DoWorkEventArgs e)
			{
				base.OnDoWork(e);
				this.threadManager.OnDoWork(this, e);
			}

			protected override void OnProgressChanged(ProgressChangedEventArgs e)
			{
				base.OnProgressChanged(e);
				this.threadManager.OnProgressChanged(this, e);
			}

			protected override void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
			{
				base.OnRunWorkerCompleted(e);
				this.threadManager.OnRunWorkerCompleted(this, e);
			}

			protected override void Dispose(bool disposing)
			{
				base.Dispose(disposing);
			}
		}
	}
}