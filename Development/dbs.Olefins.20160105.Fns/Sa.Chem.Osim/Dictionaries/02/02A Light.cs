﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal class Light
			{
				private static RangeReference CH4 = new RangeReference(string.Empty, 6, 0, SqlDbType.Float, 100.0);
				private static RangeReference C2H6 = new RangeReference(string.Empty, 7, 0, SqlDbType.Float, 100.0);
				private static RangeReference C2H4 = new RangeReference(string.Empty, 8, 0, SqlDbType.Float, 100.0);
				private static RangeReference C3H8 = new RangeReference(string.Empty, 9, 0, SqlDbType.Float, 100.0);
				private static RangeReference C3H6 = new RangeReference(string.Empty, 10, 0, SqlDbType.Float, 100.0);
				private static RangeReference NBUTA = new RangeReference(string.Empty, 11, 0, SqlDbType.Float, 100.0);
				private static RangeReference IBUTA = new RangeReference(string.Empty, 12, 0, SqlDbType.Float, 100.0);
				private static RangeReference IB = new RangeReference(string.Empty, 13, 0, SqlDbType.Float, 100.0);
				private static RangeReference B1 = new RangeReference(string.Empty, 14, 0, SqlDbType.Float, 100.0);
				private static RangeReference C4H6 = new RangeReference(string.Empty, 15, 0, SqlDbType.Float, 100.0);
				private static RangeReference NC5 = new RangeReference(string.Empty, 16, 0, SqlDbType.Float, 100.0);
				private static RangeReference IC5 = new RangeReference(string.Empty, 17, 0, SqlDbType.Float, 100.0);
				private static RangeReference NC6 = new RangeReference(string.Empty, 18, 0, SqlDbType.Float, 100.0);
				private static RangeReference C6ISO = new RangeReference(string.Empty, 19, 0, SqlDbType.Float, 100.0);
				private static RangeReference C7H16 = new RangeReference(string.Empty, 20, 0, SqlDbType.Float, 100.0);
				private static RangeReference C8H18 = new RangeReference(string.Empty, 21, 0, SqlDbType.Float, 100.0);
				private static RangeReference CO_CO2 = new RangeReference(string.Empty, 22, 0, SqlDbType.Float, 100.0);
				private static RangeReference H2 = new RangeReference(string.Empty, 23, 0, SqlDbType.Float, 100.0);
				private static RangeReference S = new RangeReference(string.Empty, 24, 0, SqlDbType.Float, 100.0);

				internal static Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						//	Light
						d.Add("Ethane", Streams.Ethane);
						d.Add("EPMix", Streams.EPMix);
						d.Add("Propane", Streams.Propane);
						d.Add("LPG", Streams.LPG);
						d.Add("Butane", Streams.Butane);
						d.Add("FeedLtOther", Streams.FeedLtOther);

						//	Recycle
						d.Add("EthRec", Streams.EthRec);
						d.Add("ProRec", Streams.ProRec);
						d.Add("ButRec", Streams.ButRec);

						return d;
					}
				}

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_StreamsLight]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return new Streams().LookUpColumn;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("CH4", Light.CH4);
							d.Add("C2H6", Light.C2H6);
							d.Add("C2H4", Light.C2H4);
							d.Add("C3H8", Light.C3H8);
							d.Add("C3H6", Light.C3H6);
							d.Add("NBUTA", Light.NBUTA);
							d.Add("IBUTA", Light.IBUTA);
							d.Add("IB", Light.IB);
							d.Add("B1", Light.B1);
							d.Add("C4H6", Light.C4H6);
							d.Add("NC5", Light.NC5);
							d.Add("IC5", Light.IC5);
							d.Add("NC6", Light.NC6);
							d.Add("C6ISO", Light.C6ISO);
							d.Add("C7H16", Light.C7H16);
							d.Add("C8H18", Light.C8H18);
							d.Add("CO_CO2", Light.CO_CO2);
							d.Add("H2", Light.H2);
							d.Add("S", Light.S);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Light.Items;
						}
					}
				}

				internal class Upload : TransferData.IUploadMultiple
				{
					public string StoredProcedure
					{
						get
						{
							return "[stgFact].[Insert_FeedQuality]";
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Light.Items;
						}
					}

					public Dictionary<string, RangeReference> Parameters
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("@FeedProdID", Feed.FeedProdId);

							d.Add("@Methane", Light.CH4);
							d.Add("@Ethane", Light.C2H6);
							d.Add("@Ethylene", Light.C2H4);
							d.Add("@Propane", Light.C3H8);
							d.Add("@Propylene", Light.C3H6);
							d.Add("@nButane", Light.NBUTA);
							d.Add("@iButane", Light.IBUTA);
							d.Add("@Isobutylene", Light.IB);
							d.Add("@Butene1", Light.B1);
							d.Add("@Butadiene", Light.C4H6);
							d.Add("@nPentane", Light.NC5);
							d.Add("@iPentane", Light.IC5);
							d.Add("@nHexane", Light.NC6);
							d.Add("@iHexane", Light.C6ISO);
							d.Add("@Septane", Light.C7H16);
							d.Add("@Octane", Light.C8H18);
							d.Add("@CO2", Light.CO_CO2);
							d.Add("@Hydrogen", Light.H2);
							d.Add("@Sulfur", Light.S);

							d.Add("@CoilOutletPressure", Feed.CoilOutletPressure_Psia);
							d.Add("@SteamHCRatio", Feed.SteamHydrocarbon_Ratio);
							d.Add("@CoilOutletTemp", Feed.CoilOutletTemp_C);

							d.Add("@EthyleneYield", Feed.EthyleneYield_WtPcnt);
							d.Add("@PropylEthylRatio", Feed.PropyleneEthylene_Ratio);
							d.Add("@FeedConv", Feed.FeedConv_WtPcnt);
							d.Add("@PropylMethaneRatio", Feed.PropyleneMethane_Ratio);

							d.Add("@ConstrYear", Feed.FurnConstruction_Year);
							d.Add("@ResTime", Feed.ResidenceTime_s);

							return d;
						}
					}
				}
			}
		}
	}
}