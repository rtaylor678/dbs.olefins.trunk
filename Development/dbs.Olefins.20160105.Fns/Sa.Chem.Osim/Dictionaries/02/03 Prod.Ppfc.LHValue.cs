﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Prod
			{
				internal partial class Ppfc
				{
					internal class LHValue
					{
						internal void CustomLoad(SqlConnection cn, Excel.Workbook wkb, string tabNamePrefix, string refnum, bool forceIn)
						{
							string sheetName = tabNamePrefix + Tabs.T03;

							if (wkb.SheetExists(sheetName))
							{
								Excel.Worksheet wks = wkb.Worksheets[sheetName];

								string uomSuffix = (wkb.Worksheets["Conv"].Cells[2, 2].Value == 1) ? "Imp" : "Met";
								string storedProc = "[fact].[Select_PpfcLHValue]";

								using (SqlCommand cmd = new SqlCommand(storedProc, cn))
								{
									cmd.CommandType = CommandType.StoredProcedure;

									cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

									using (SqlDataReader rdr = cmd.ExecuteReader())
									{
										while (rdr.Read())
										{
											XL.SetCellValue(rdr, "LHValue_" + uomSuffix, wks, 41, 8, forceIn);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}