﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Metathesis
		{
			internal partial class CapEx
			{
				private static RangeReference Amount_Cur = new RangeReference(Tabs.T13, 59, 8, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{

					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MetaCapEx]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Amount_Cur", CapEx.Amount_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}