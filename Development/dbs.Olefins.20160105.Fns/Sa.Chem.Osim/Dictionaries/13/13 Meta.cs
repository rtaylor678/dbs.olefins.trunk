﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Metathesis
		{
			private static RangeReference Q1 = new RangeReference(Tabs.T13, 0, 6, SqlDbType.Float);
			private static RangeReference Q2 = new RangeReference(Tabs.T13, 0, 7, SqlDbType.Float);
			private static RangeReference Q3 = new RangeReference(Tabs.T13, 0, 8, SqlDbType.Float);
			private static RangeReference Q4 = new RangeReference(Tabs.T13, 0, 9, SqlDbType.Float);

			internal static Dictionary<string, RangeReference> Values
			{
				get
				{
					Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

					d.Add("Q1", Metathesis.Q1);
					d.Add("Q2", Metathesis.Q2);
					d.Add("Q3", Metathesis.Q3);
					d.Add("Q4", Metathesis.Q4);

					return d;
				}
			}
		}
	}
}