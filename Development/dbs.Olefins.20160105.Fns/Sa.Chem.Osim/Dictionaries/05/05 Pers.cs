﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Pers
		{
			internal class General
			{
				private static RangeReference Personnel_Count = new RangeReference(string.Empty, 0, 4, SqlDbType.VarChar);
				private static RangeReference StraightTime_Hrs = new RangeReference(string.Empty, 0, 5, SqlDbType.Float);
				private static RangeReference OverTime_Hrs = new RangeReference(string.Empty, 0, 6, SqlDbType.Float);
				private static RangeReference Contract_Hrs = new RangeReference(string.Empty, 0, 7, SqlDbType.Float);

				private static RangeReference OccProcProc = new RangeReference(Tabs.T05_A, 10, 0, SqlDbType.Float);
				private static RangeReference OccProcUtil = new RangeReference(Tabs.T05_A, 11, 0, SqlDbType.Float);
				private static RangeReference OccProcOffSite = new RangeReference(Tabs.T05_A, 12, 0, SqlDbType.Float);
				private static RangeReference OccProcTruckRail = new RangeReference(Tabs.T05_A, 13, 0, SqlDbType.Float);
				private static RangeReference OccProcMarine = new RangeReference(Tabs.T05_A, 14, 0, SqlDbType.Float);
				private static RangeReference OccProcRelief = new RangeReference(Tabs.T05_A, 15, 0, SqlDbType.Float);
				private static RangeReference OccProcTraining = new RangeReference(Tabs.T05_A, 16, 0, SqlDbType.Float);
				private static RangeReference OccProcOther = new RangeReference(Tabs.T05_A, 17, 0, SqlDbType.Float);

				private static RangeReference OccMaintRoutine = new RangeReference(Tabs.T05_A, 21, 0, SqlDbType.Float);
				private static RangeReference OccMaintTaMaint = new RangeReference(Tabs.T05_A, 22, 0, SqlDbType.Float);
				private static RangeReference OccMaintInsp = new RangeReference(Tabs.T05_A, 23, 0, SqlDbType.Float);
				private static RangeReference OccMaintShop = new RangeReference(Tabs.T05_A, 24, 0, SqlDbType.Float);
				private static RangeReference OccMaintPlan = new RangeReference(Tabs.T05_A, 25, 0, SqlDbType.Float);
				private static RangeReference OccMaintWarehouse = new RangeReference(Tabs.T05_A, 26, 0, SqlDbType.Float);
				private static RangeReference OccMaintTraining = new RangeReference(Tabs.T05_A, 27, 0, SqlDbType.Float);

				private static RangeReference OccAdminLab = new RangeReference(Tabs.T05_A, 31, 0, SqlDbType.Float);
				private static RangeReference OccAdminAcct = new RangeReference(Tabs.T05_A, 32, 0, SqlDbType.Float);
				private static RangeReference OccAdminPurchase = new RangeReference(Tabs.T05_A, 33, 0, SqlDbType.Float);
				private static RangeReference OccAdminSecurity = new RangeReference(Tabs.T05_A, 34, 0, SqlDbType.Float);
				private static RangeReference OccAdminClerical = new RangeReference(Tabs.T05_A, 35, 0, SqlDbType.Float);
				private static RangeReference OccAdminOther = new RangeReference(Tabs.T05_A, 36, 0, SqlDbType.Float);

				private static RangeReference MpsProcProc = new RangeReference(Tabs.T05_B, 9, 0, SqlDbType.Float);

				private static RangeReference MpsMaintRoutine = new RangeReference(Tabs.T05_B, 12, 0, SqlDbType.Float);
				private static RangeReference MpsMaintTaMaint = new RangeReference(Tabs.T05_B, 13, 0, SqlDbType.Float);
				private static RangeReference MpsMaintShop = new RangeReference(Tabs.T05_B, 14, 0, SqlDbType.Float);
				private static RangeReference MpsMaintPlan = new RangeReference(Tabs.T05_B, 15, 0, SqlDbType.Float);
				private static RangeReference MpsMaintWarehouse = new RangeReference(Tabs.T05_B, 16, 0, SqlDbType.Float);

				private static RangeReference MpsAdminLab = new RangeReference(Tabs.T05_B, 19, 0, SqlDbType.Float);

				private static RangeReference MpsTechProc = new RangeReference(Tabs.T05_B, 22, 0, SqlDbType.Float);
				private static RangeReference MpsTechEcon = new RangeReference(Tabs.T05_B, 23, 0, SqlDbType.Float);
				private static RangeReference MpsTechRely = new RangeReference(Tabs.T05_B, 24, 0, SqlDbType.Float);
				private static RangeReference MpsTechInsp = new RangeReference(Tabs.T05_B, 25, 0, SqlDbType.Float);
				private static RangeReference MpsTechAPC = new RangeReference(Tabs.T05_B, 26, 0, SqlDbType.Float);
				private static RangeReference MpsTechEnviron = new RangeReference(Tabs.T05_B, 27, 0, SqlDbType.Float);
				private static RangeReference MpsTechOther = new RangeReference(Tabs.T05_B, 28, 0, SqlDbType.Float);

				private static RangeReference MpsAdminAcct = new RangeReference(Tabs.T05_B, 31, 0, SqlDbType.Float);
				private static RangeReference MpsAdminMIS = new RangeReference(Tabs.T05_B, 32, 0, SqlDbType.Float);
				private static RangeReference MpsAdminPurchase = new RangeReference(Tabs.T05_B, 33, 0, SqlDbType.Float);
				private static RangeReference MpsAdminSecurity = new RangeReference(Tabs.T05_B, 34, 0, SqlDbType.Float);
				private static RangeReference MpsAdminOther = new RangeReference(Tabs.T05_B, 35, 0, SqlDbType.Float);

				private static RangeReference MpsGenPurchase = new RangeReference(Tabs.T05_B, 38, 0, SqlDbType.Float);
				private static RangeReference MpsGenOther = new RangeReference(Tabs.T05_B, 39, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_Personnel]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "PersID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Personnel_Count", General.Personnel_Count);
							d.Add("StraightTime_Hrs", General.StraightTime_Hrs);
							d.Add("OverTime_Hrs", General.OverTime_Hrs);
							d.Add("Contract_Hrs", General.Contract_Hrs);

							return d;
						}
					}


					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("OccProcProc", General.OccProcProc);
							d.Add("OccProcUtil", General.OccProcUtil);
							d.Add("OccProcOffSite", General.OccProcOffSite);
							d.Add("OccProcTruckRail", General.OccProcTruckRail);
							d.Add("OccProcMarine", General.OccProcMarine);
							d.Add("OccProcRelief", General.OccProcRelief);
							d.Add("OccProcTraining", General.OccProcTraining);
							d.Add("OccProcOther", General.OccProcOther);

							d.Add("OccMaintRoutine", General.OccMaintRoutine);
							d.Add("OccMaintTaMaint", General.OccMaintTaMaint);
							d.Add("OccMaintInsp", General.OccMaintInsp);
							d.Add("OccMaintShop", General.OccMaintShop);
							d.Add("OccMaintPlan", General.OccMaintPlan);
							d.Add("OccMaintWarehouse", General.OccMaintWarehouse);
							d.Add("OccMaintTraining", General.OccMaintTraining);

							d.Add("OccAdminLab", General.OccAdminLab);
							d.Add("OccAdminAcct", General.OccAdminAcct);
							d.Add("OccAdminPurchase", General.OccAdminPurchase);
							d.Add("OccAdminSecurity", General.OccAdminSecurity);
							d.Add("OccAdminClerical", General.OccAdminClerical);
							d.Add("OccAdminOther", General.OccAdminOther);

							d.Add("MpsProcProc", General.MpsProcProc);

							d.Add("MpsMaintTaMaint", General.MpsMaintTaMaint);
							d.Add("MpsMaintRoutine", General.MpsMaintRoutine);
							d.Add("MpsMaintShop", General.MpsMaintShop);
							d.Add("MpsMaintPlan", General.MpsMaintPlan);
							d.Add("MpsMaintWarehouse", General.MpsMaintWarehouse);

							d.Add("MpsAdminLab", General.MpsAdminLab);

							d.Add("MpsTechProc", General.MpsTechProc);
							d.Add("MpsTechEcon", General.MpsTechEcon);
							d.Add("MpsTechRely", General.MpsTechRely);
							d.Add("MpsTechInsp", General.MpsTechInsp);
							d.Add("MpsTechAPC", General.MpsTechAPC);
							d.Add("MpsTechEnviron", General.MpsTechEnviron);
							d.Add("MpsTechOther", General.MpsTechOther);

							d.Add("MpsAdminAcct", General.MpsAdminAcct);
							d.Add("MpsAdminMIS", General.MpsAdminMIS);
							d.Add("MpsAdminPurchase", General.MpsAdminPurchase);
							d.Add("MpsAdminSecurity", General.MpsAdminSecurity);
							d.Add("MpsAdminOther", General.MpsAdminOther);

							d.Add("MpsGenPurchase", General.MpsGenPurchase);
							d.Add("MpsGenOther", General.MpsGenOther);

							return d;
						}
					}
				}


	//@Refnum    VARCHAR (25),
	//@PersId    VARCHAR (15),

	//@SortKey   INT,

	//@SectionID VARCHAR (4)  = NULL,
	//@NumPers   REAL      = NULL,
	//@STH       REAL      = NULL,
	//@OvtHours  REAL      = NULL,
	//@Contract  REAL      = NULL
			}
		}
	}
}