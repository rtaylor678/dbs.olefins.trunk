﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal class General
			{
				private static RangeReference MinParaffin_Pcnt = new RangeReference(Tabs.T11, 67, 8, SqlDbType.Float, 100.0);
				private static RangeReference MaxVaporPresFRS_RVP = new RangeReference(Tabs.T11, 68, 8, SqlDbType.Float);

				private static RangeReference YieldPattern_Count = new RangeReference(Tabs.T11, 103, 7, SqlDbType.Int);
				private static RangeReference YieldPatternLP_Count = new RangeReference(Tabs.T11, 104, 7, SqlDbType.Int);
				private static RangeReference YieldGroup_Count = new RangeReference(Tabs.T11, 105, 7, SqlDbType.Int);

				//	YPS_RPT
				private static RangeReference YPS_Lummus = new RangeReference(Tabs.T11, 109, 20, SqlDbType.Bit);
				private static RangeReference YPS_SPYRO = new RangeReference(Tabs.T11, 109, 21, SqlDbType.Bit);
				private static RangeReference YPS_SelfDev = new RangeReference(Tabs.T11, 109, 22, SqlDbType.Bit);
				private static RangeReference YPS_Other = new RangeReference(Tabs.T11, 109, 23, SqlDbType.Bit);
				private static RangeReference YPS_OtherName = new RangeReference(Tabs.T11, 109, 6, SqlDbType.VarChar);

				//	LP_RPT
				private static RangeReference LP_Aspen = new RangeReference(Tabs.T11, 116, 20, SqlDbType.Bit);
				private static RangeReference LP_Honeywell = new RangeReference(Tabs.T11, 116, 21, SqlDbType.Bit);
				private static RangeReference LP_Haverly = new RangeReference(Tabs.T11, 116, 22, SqlDbType.Bit);
				private static RangeReference LP_SelfDev = new RangeReference(Tabs.T11, 116, 23, SqlDbType.Bit);
				private static RangeReference LP_Other = new RangeReference(Tabs.T11, 116, 24, SqlDbType.Bit);
				private static RangeReference LP_OtherName = new RangeReference(Tabs.T11, 116, 7, SqlDbType.VarChar);

				private static RangeReference LP_RowCount = new RangeReference(Tabs.T11, 118, 7, SqlDbType.Int);

				//LP_DeltaBase_No	LP_DeltaBase_NotSure	LP_DeltaBase_RPT	LP_DeltaBase_Yes
				//LP_MixInt_No	LP_MixInt_NotSure	LP_MixInt_RPT	LP_MixInt_Yes
				//LP_Online_No	LP_Online_NotSure	LP_Online_RPT	LP_Online_Yes
				//LP_Recur_No	LP_Recur_NotSure	LP_Recur_RPT	LP_Recur_Yes

				//	BeyondCrack_RPT
				private static RangeReference BeyondCrack_None = new RangeReference(Tabs.T11, 126, 20, SqlDbType.Bit);
				private static RangeReference BeyondCrack_C4 = new RangeReference(Tabs.T11, 126, 21, SqlDbType.Bit);
				private static RangeReference BeyondCrack_C5 = new RangeReference(Tabs.T11, 126, 22, SqlDbType.Bit);
				private static RangeReference BeyondCrack_Pygas = new RangeReference(Tabs.T11, 126, 23, SqlDbType.Bit);
				private static RangeReference BeyondCrack_BTX = new RangeReference(Tabs.T11, 126, 24, SqlDbType.Bit);
				private static RangeReference BeyondCrack_OthChem = new RangeReference(Tabs.T11, 126, 25, SqlDbType.Bit);
				private static RangeReference BeyondCrack_OthRefine = new RangeReference(Tabs.T11, 126, 26, SqlDbType.Bit);
				private static RangeReference BeyondCrack_Oth = new RangeReference(Tabs.T11, 126, 27, SqlDbType.Bit);
				private static RangeReference BeyondCrack_OtherName = new RangeReference(Tabs.T11, 126, 10, SqlDbType.VarChar);

				private static RangeReference DecisionPlant_X = new RangeReference(Tabs.T11, 135, 7, SqlDbType.VarChar);
				private static RangeReference DecisionCorp_X = new RangeReference(Tabs.T11, 135, 8, SqlDbType.VarChar);

				private static RangeReference PlanTime_Days = new RangeReference(Tabs.T11, 153, 6, SqlDbType.Int);
				private static RangeReference EffFeedChg_Levels = new RangeReference(Tabs.T11, 165, 10, SqlDbType.Int);
				private static RangeReference FeedPurchase_Levels = new RangeReference(Tabs.T11, 166, 10, SqlDbType.Int);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_FeedSel]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("MinParaffin_Pcnt", General.MinParaffin_Pcnt);
							d.Add("MaxVaporPresFRS_RVP", General.MaxVaporPresFRS_RVP);

							d.Add("YieldPattern_Count", General.YieldPattern_Count);
							d.Add("YieldPatternLP_Count", General.YieldPatternLP_Count);
							d.Add("YieldGroup_Count", General.YieldGroup_Count);

							d.Add("YPS_Lummus", General.YPS_Lummus);
							d.Add("YPS_SPYRO", General.YPS_SPYRO);
							d.Add("YPS_SelfDev", General.YPS_SelfDev);
							d.Add("YPS_Other", General.YPS_Other);
							d.Add("YPS_OtherName", General.YPS_OtherName);

							d.Add("LP_Aspen", General.LP_Aspen);
							d.Add("LP_Honeywell", General.LP_Honeywell);
							d.Add("LP_Haverly", General.LP_Haverly);
							d.Add("LP_SelfDev", General.LP_SelfDev);
							d.Add("LP_Other", General.LP_Other);
							d.Add("LP_OtherName", General.LP_OtherName);

							d.Add("LP_RowCount", General.LP_RowCount);

							d.Add("BeyondCrack_None", General.BeyondCrack_None);
							d.Add("BeyondCrack_C4", General.BeyondCrack_C4);
							d.Add("BeyondCrack_C5", General.BeyondCrack_C5);
							d.Add("BeyondCrack_Pygas", General.BeyondCrack_Pygas);
							d.Add("BeyondCrack_BTX", General.BeyondCrack_BTX);
							d.Add("BeyondCrack_OthChem", General.BeyondCrack_OthChem);
							d.Add("BeyondCrack_OthRefine", General.BeyondCrack_OthRefine);
							d.Add("BeyondCrack_Oth", General.BeyondCrack_Oth);
							d.Add("BeyondCrack_OtherName", General.BeyondCrack_OtherName);

							d.Add("DecisionPlant_X", General.DecisionPlant_X);
							d.Add("DecisionCorp_X", General.DecisionCorp_X);

							d.Add("PlanTime_Days", General.PlanTime_Days);
							d.Add("EffFeedChg_Levels", General.EffFeedChg_Levels);
							d.Add("FeedPurchase_Levels", General.FeedPurchase_Levels);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}