﻿using System.Collections.Generic;
using System.Data;


namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Energy
		{
			internal class Cogen
			{
				private static RangeReference HasCogen_YN = new RangeReference(Tabs.T07, 53, 8, SqlDbType.VarChar);
				private static RangeReference Thermal_Pcnt = new RangeReference(Tabs.T07, 68, 8, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_EnergyCogen]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("HasCogen_YN", Cogen.HasCogen_YN);
							d.Add("Thermal_Pcnt", Cogen.Thermal_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}

				internal class Facilities
				{
					private static RangeReference Unit_Count = new RangeReference(Tabs.T07, 0, 6, SqlDbType.Float);
					private static RangeReference Capacity_MW = new RangeReference(Tabs.T07, 0, 7, SqlDbType.Float);
					private static RangeReference Efficiency_Pcnt = new RangeReference(Tabs.T07, 0, 8, SqlDbType.Float, 100.0);
					private static RangeReference FacilityName = new RangeReference(Tabs.T07, 60, 3, SqlDbType.VarChar);

					private static RangeReference CogenGT = new RangeReference(Tabs.T07, 57, 0);
					private static RangeReference CogenST = new RangeReference(Tabs.T07, 58, 0);
					private static RangeReference CogenCC = new RangeReference(Tabs.T07, 59, 0);
					private static RangeReference CogenOth = new RangeReference(Tabs.T07, 60, 0);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_EnergyCogenFacilities]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return "FacilityID";
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Unit_Count", Facilities.Unit_Count);
								d.Add("Capacity_MW", Facilities.Capacity_MW);
								d.Add("Efficiency_Pcnt", Facilities.Efficiency_Pcnt);
								d.Add("FacilityName", Facilities.FacilityName);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("CogenGT", Facilities.CogenGT);
								d.Add("CogenST", Facilities.CogenST);
								d.Add("CogenCC", Facilities.CogenCC);
								d.Add("CogenOth", Facilities.CogenOth);

								return d;
							}
						}
					}
				}

				internal class EnergyPercent
				{
					private static RangeReference CogenEnergy_Pcnt = new RangeReference(Tabs.T07, 0, 8, SqlDbType.Float, 100.0);

					private static RangeReference PurSteam = new RangeReference(Tabs.T07, 63, 8, SqlDbType.Float, 100.0);
					private static RangeReference PurElec = new RangeReference(Tabs.T07, 65, 8, SqlDbType.Float, 100.0);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_EnergyCogenPcnt]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return "AccountID";
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("CogenEnergy_Pcnt", EnergyPercent.CogenEnergy_Pcnt);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("PurSteam", EnergyPercent.PurSteam);
								d.Add("PurElec", EnergyPercent.PurElec);

								return d;
							}
						}
					}
				}
			}
		}
	}
}