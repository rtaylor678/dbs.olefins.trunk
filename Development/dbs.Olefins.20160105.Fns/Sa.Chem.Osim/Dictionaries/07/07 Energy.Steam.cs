﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Energy
		{
			internal class Steam
			{
				private RangeReference Amount = new RangeReference(Tabs.T07, 0, 4, SqlDbType.Float);
				private RangeReference Pressure = new RangeReference(Tabs.T07, 0, 5, SqlDbType.Float);
				private RangeReference Temp = new RangeReference(Tabs.T07, 0, 6, SqlDbType.Float);
				private RangeReference LHValue = new RangeReference(Tabs.T07, 0, 7, SqlDbType.Float);
				private RangeReference Amount_Cur = new RangeReference(Tabs.T07, 0, 8, SqlDbType.Float);

				private Dictionary<string, RangeReference> Values
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("Amount_", this.Amount);
						d.Add("Pressure_", this.Pressure);
						d.Add("Temp_", this.Temp);
						d.Add("LHValue_", this.LHValue);
						d.Add("Amount_Cur_", this.Amount_Cur);

						return d;
					}
				}

				private RangeReference PurFuelNatural = new RangeReference(Tabs.T07, 7, 0);
				private RangeReference PurEthane = new RangeReference(Tabs.T07, 8, 0);
				private RangeReference PurPropane = new RangeReference(Tabs.T07, 9, 0);
				private RangeReference PurButane = new RangeReference(Tabs.T07, 10, 0);
				private RangeReference PurNaphtha = new RangeReference(Tabs.T07, 11, 0);
				private RangeReference PurDistallate = new RangeReference(Tabs.T07, 12, 0);
				private RangeReference PurResidual = new RangeReference(Tabs.T07, 13, 0);
				private RangeReference PurCoalCoke = new RangeReference(Tabs.T07, 14, 0);

				private RangeReference PurSteamSHP = new RangeReference(Tabs.T07, 16, 0);
				private RangeReference PurSteamHP = new RangeReference(Tabs.T07, 17, 0);
				private RangeReference PurSteamIP = new RangeReference(Tabs.T07, 18, 0);
				private RangeReference PurSteamLP = new RangeReference(Tabs.T07, 19, 0);

				private RangeReference PurSteam = new RangeReference(Tabs.T07, 21, 0);

				private RangeReference PPFCFuelGas = new RangeReference(Tabs.T07, 27, 0);
				private RangeReference PPFCEthane = new RangeReference(Tabs.T07, 28, 0);
				private RangeReference PPFCPropane = new RangeReference(Tabs.T07, 29, 0);
				private RangeReference PPFCButane = new RangeReference(Tabs.T07, 30, 0);
				private RangeReference PPFCPyroNaphtha = new RangeReference(Tabs.T07, 31, 0);
				private RangeReference PPFCPyroGasOil = new RangeReference(Tabs.T07, 32, 0);
				private RangeReference PPFCPyroFuelOil = new RangeReference(Tabs.T07, 33, 0);

				private RangeReference ExportSteamSHP = new RangeReference(Tabs.T07, 40, 0);
				private RangeReference ExportSteamHP = new RangeReference(Tabs.T07, 41, 0);
				private RangeReference ExportSteamIP = new RangeReference(Tabs.T07, 42, 0);
				private RangeReference ExportSteamLP = new RangeReference(Tabs.T07, 43, 0);

				private Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("PurFuelNatural", this.PurFuelNatural);
						d.Add("PurEthane", this.PurEthane);
						d.Add("PurPropane", this.PurPropane);
						d.Add("PurButane", this.PurButane);
						d.Add("PurNaphtha", this.PurNaphtha);
						d.Add("PurDistallate", this.PurDistallate);
						d.Add("PurResidual", this.PurResidual);
						d.Add("PurCoalCoke", this.PurCoalCoke);

						d.Add("PurSteamSHP", this.PurSteamSHP);
						d.Add("PurSteamHP", this.PurSteamHP);
						d.Add("PurSteamIP", this.PurSteamIP);
						d.Add("PurSteamLP", this.PurSteamLP);

						d.Add("PurSteam", this.PurSteam);		// Fuel Used for Purchased Steam Generation, this.Methane Content, this.wt %

						d.Add("PPFCFuelGas", this.PPFCFuelGas);
						d.Add("PPFCEthane", this.PPFCEthane);
						d.Add("PPFCPropane", this.PPFCPropane);
						d.Add("PPFCButane", this.PPFCButane);
						d.Add("PPFCPyroNaphtha", this.PPFCPyroNaphtha);
						d.Add("PPFCPyroGasOil", this.PPFCPyroGasOil);
						d.Add("PPFCPyroFuelOil", this.PPFCPyroFuelOil);

						d.Add("ExportSteamSHP", this.ExportSteamSHP);
						d.Add("ExportSteamHP", this.ExportSteamHP);
						d.Add("ExportSteamIP", this.ExportSteamIP);
						d.Add("ExportSteamLP", this.ExportSteamLP);

						return d;
					}
				}

				internal void CustomLoad(SqlConnection cn, Excel.Workbook wkb, string tabNamePrefix, string refnum, bool forceIn)
				{
					string sheetName = tabNamePrefix + Tabs.T07;

					if (wkb.SheetExists(sheetName))
					{
						Excel.Worksheet wks = wkb.Worksheets[sheetName];
						
					
						string uomSuffix = (wkb.Worksheets["Conv"].Cells[2, 2].Value == 1) ? "Imp" : "Met";
						string storedProc = "[fact].[Select_EnergyLHValue]";
						string columnName = "AccountId";

						string sht;
						int row;
						int col;
						SqlDbType typ;
						double div;

						using (SqlCommand cmd = new SqlCommand(storedProc, cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

							using (SqlDataReader rdr = cmd.ExecuteReader())
							{
								RangeReference r;
								RangeReference rr;

								while (rdr.Read())
								{
									if (Items.TryGetValue(rdr.GetString(rdr.GetOrdinal(columnName)), out r))
									{
										foreach (KeyValuePair<string, RangeReference> entry in Values)
										{
											sht = (r.sheetSuffix != string.Empty) ? r.sheetSuffix : entry.Value.sheetSuffix;
											row = (r.row != 0) ? r.row : entry.Value.row;
											col = (r.col != 0) ? r.col : entry.Value.col;
											typ = (r.sqlType != SqlDbType.Variant) ? r.sqlType : entry.Value.sqlType;
											div = (!double.IsNaN(r.divisor)) ? r.divisor : entry.Value.divisor;

											rr = new RangeReference(sht, row, col, typ, div, forceIn);

											XL.SetCellValue(rdr, entry.Key + uomSuffix, wkb, tabNamePrefix, rr);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}