﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class RunLength
			{
				private static RangeReference RunLengthDays = new RangeReference(Tabs.T06_03, 43, 0, SqlDbType.Float);

				private static RangeReference Ethane = new RangeReference(Tabs.T06_03, 43, 6);
				private static RangeReference Propane = new RangeReference(Tabs.T06_03, 43, 7);
				private static RangeReference Naphtha = new RangeReference(Tabs.T06_03, 43, 8);
				private static RangeReference LiqHeavy = new RangeReference(Tabs.T06_03, 43, 9);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityPyroFurn_RunLength]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("RunLength_Days", RunLength.RunLengthDays);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Ethane", RunLength.Ethane);
							d.Add("Propane", RunLength.Propane);
							d.Add("Naphtha", RunLength.Naphtha);
							d.Add("LiqHeavy", RunLength.LiqHeavy);

							return d;
						}
					}
				}
			}
		}
	}
}