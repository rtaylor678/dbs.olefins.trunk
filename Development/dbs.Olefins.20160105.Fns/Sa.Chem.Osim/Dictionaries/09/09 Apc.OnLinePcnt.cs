﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Apc
		{
			internal class OnLinePcnt
			{
				private static RangeReference OnLine_Pcnt = new RangeReference(string.Empty, 0, 9, SqlDbType.Float, 100.0);

				private static RangeReference PyroFurn = new RangeReference(Tabs.T09_05, 34, 9, SqlDbType.Float, 100.0);
				private static RangeReference Comp = new RangeReference(Tabs.T09_06, 21, 9, SqlDbType.Float, 100.0);
				private static RangeReference ProdRecovery = new RangeReference(Tabs.T09_06, 63, 9, SqlDbType.Float, 100.0);
				private static Dictionary<string, RangeReference> Items;



				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ApcOnLinePcnt]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "ApcId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("OnLine_Pcnt", OnLinePcnt.OnLine_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("PyroFurn", OnLinePcnt.PyroFurn);
							d.Add("Comp", OnLinePcnt.Comp);
							d.Add("ProdRecovery", OnLinePcnt.ProdRecovery);

							return d;
						}
					}
				}

				internal class Upload : TransferData.IUploadMultiple
				{

					string TransferData.IUploadMultiple.StoredProcedure
					{
						get
						{
							return "[fact].[Select_ApcOnLinePcnt]";
						}
					}



					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return OnLinePcnt.Items;

						}
					}

					public Dictionary<string, RangeReference> Parameters
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("@PyroFurn", OnLinePcnt.PyroFurn);
							d.Add("@Comp", OnLinePcnt.Comp);
							d.Add("@ProdRecovery", OnLinePcnt.ProdRecovery);

							return d;
						}
					}




					public string LookUpColumn
					{
						get
						{
							return "ApcId";
						}
					}


				}
			}
		}
	}
}
		

