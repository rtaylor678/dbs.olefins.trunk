﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class Energy
			{
				private static RangeReference CoolingWater_MTd = new RangeReference(Tabs.T08_04, 36, 5, SqlDbType.Float);
				private static RangeReference CoolingWaterSupplyTemp_C = new RangeReference(Tabs.T08_04, 39, 5, SqlDbType.Float);
				private static RangeReference CoolingWaterReturnTemp_C = new RangeReference(Tabs.T08_04, 40, 5, SqlDbType.Float);
				//private static RangeReference FinFanHeatAbsorb_kBTU = new RangeReference(Tabs.T08_04, 43, 5, SqlDbType.Float);

				private static RangeReference CompGasEfficiency_Pcnt = new RangeReference(Tabs.T08_04, 43, 5, SqlDbType.Float, 100.0);
				private static RangeReference CalcTypeID = new RangeReference(Tabs.T08_04,46 , 5, SqlDbType.VarChar);

				//private static RangeReference SteamExtratRate_kMTd = new RangeReference(Tabs.T08_04, 52, 5, SqlDbType.Float);
				//private static RangeReference SteamCondenseRate_kMTd = new RangeReference(Tabs.T08_04, 53, 5, SqlDbType.Float);
				//private static RangeReference ExchangerHeatDuty_BTU = new RangeReference(Tabs.T08_04, 58, 5, SqlDbType.Float);

				private static RangeReference QuenchHeatMatl_Pcnt = new RangeReference(Tabs.T08_04, 49, 5, SqlDbType.Float, 100.0);
				private static RangeReference QuenchDilution_Pcnt = new RangeReference(Tabs.T08_04, 51, 5, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantEnergy]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("CoolingWater_MTd", Energy.CoolingWater_MTd);
							d.Add("CoolingWaterSupplyTemp_C", Energy.CoolingWaterSupplyTemp_C);
							d.Add("CoolingWaterReturnTemp_C", Energy.CoolingWaterReturnTemp_C);
							//d.Add("FinFanHeatAbsorb_kBTU", Energy.FinFanHeatAbsorb_kBTU);

							d.Add("CompGasEfficiency_Pcnt", Energy.CompGasEfficiency_Pcnt);
							d.Add("CalcTypeID", Energy.CalcTypeID);

							//d.Add("SteamExtratRate_kMTd", Energy.SteamExtratRate_kMTd);
							//d.Add("SteamCondenseRate_kMTd", Energy.SteamCondenseRate_kMTd);
							//d.Add("ExchangerHeatDuty_BTU", Energy.ExchangerHeatDuty_BTU);

							d.Add("QuenchHeatMatl_Pcnt", Energy.QuenchHeatMatl_Pcnt);
							d.Add("QuenchDilution_Pcnt", Energy.QuenchDilution_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}