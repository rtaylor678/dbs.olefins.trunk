﻿namespace Chem.UpLoad
{
    partial class ControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.UploadOne = new System.Windows.Forms.Button();
			this.UpLoadLoop = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.btnPopulate = new System.Windows.Forms.Button();
			this.btnValidatedInput = new System.Windows.Forms.Button();
			this.lbFiles = new System.Windows.Forms.ListBox();
			this.cboRefnums = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// UploadOne
			// 
			this.UploadOne.Location = new System.Drawing.Point(11, 91);
			this.UploadOne.Name = "UploadOne";
			this.UploadOne.Size = new System.Drawing.Size(214, 23);
			this.UploadOne.TabIndex = 0;
			this.UploadOne.Text = "Upload One File";
			this.UploadOne.UseVisualStyleBackColor = true;
			this.UploadOne.Click += new System.EventHandler(this.UpLoadOne_Click);
			// 
			// UpLoadLoop
			// 
			this.UpLoadLoop.Location = new System.Drawing.Point(11, 121);
			this.UpLoadLoop.Name = "UpLoadLoop";
			this.UpLoadLoop.Size = new System.Drawing.Size(214, 23);
			this.UpLoadLoop.TabIndex = 1;
			this.UpLoadLoop.Text = "Upload Folder";
			this.UpLoadLoop.UseVisualStyleBackColor = true;
			this.UpLoadLoop.Click += new System.EventHandler(this.UpLoadFolder_Click);
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(11, 150);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(770, 20);
			this.textBox1.TabIndex = 4;
			// 
			// btnPopulate
			// 
			this.btnPopulate.Location = new System.Drawing.Point(11, 12);
			this.btnPopulate.Name = "btnPopulate";
			this.btnPopulate.Size = new System.Drawing.Size(214, 23);
			this.btnPopulate.TabIndex = 5;
			this.btnPopulate.Text = "Populate OSIM Form";
			this.btnPopulate.UseVisualStyleBackColor = true;
			this.btnPopulate.Click += new System.EventHandler(this.btnPopulate_Click);
			// 
			// btnValidatedInput
			// 
			this.btnValidatedInput.Location = new System.Drawing.Point(12, 243);
			this.btnValidatedInput.Name = "btnValidatedInput";
			this.btnValidatedInput.Size = new System.Drawing.Size(213, 23);
			this.btnValidatedInput.TabIndex = 6;
			this.btnValidatedInput.Text = "Create VI from input form";
			this.btnValidatedInput.UseVisualStyleBackColor = true;
			this.btnValidatedInput.Click += new System.EventHandler(this.btnValidatedInput_Click);
			// 
			// lbFiles
			// 
			this.lbFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbFiles.FormattingEnabled = true;
			this.lbFiles.Location = new System.Drawing.Point(12, 272);
			this.lbFiles.Name = "lbFiles";
			this.lbFiles.Size = new System.Drawing.Size(769, 121);
			this.lbFiles.TabIndex = 7;
			// 
			// cboRefnums
			// 
			this.cboRefnums.FormattingEnabled = true;
			this.cboRefnums.Location = new System.Drawing.Point(231, 14);
			this.cboRefnums.Name = "cboRefnums";
			this.cboRefnums.Size = new System.Drawing.Size(161, 21);
			this.cboRefnums.TabIndex = 8;
			// 
			// ControlForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(794, 411);
			this.Controls.Add(this.cboRefnums);
			this.Controls.Add(this.lbFiles);
			this.Controls.Add(this.btnValidatedInput);
			this.Controls.Add(this.btnPopulate);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.UpLoadLoop);
			this.Controls.Add(this.UploadOne);
			this.Name = "ControlForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ControlForm";
			this.Load += new System.EventHandler(this.ControlForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button UploadOne;
		private System.Windows.Forms.Button UpLoadLoop;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button btnPopulate;
		private System.Windows.Forms.Button btnValidatedInput;
		private System.Windows.Forms.ListBox lbFiles;
		private System.Windows.Forms.ComboBox cboRefnums;
    }
}