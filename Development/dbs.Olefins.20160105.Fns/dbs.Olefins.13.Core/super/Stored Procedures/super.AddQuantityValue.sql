﻿CREATE PROCEDURE [super].[AddQuantityValue]
	@Refnum				VARCHAR (25),
	@CalDateKey			INT,
	@StreamId			VARCHAR (42),
	@StreamDescription	NVARCHAR (256),
	@CurrencyRpt		VARCHAR (4),
	@Amount_Cur			REAL
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM super.QuantityValue
	WHERE	Refnum = @Refnum
		AND	CalDateKey = @CalDateKey
		AND	StreamId = @StreamId
		AND StreamDescription = @StreamDescription
		AND	CurrencyRpt = @CurrencyRpt;

	INSERT INTO super.QuantityValue(Refnum, CalDateKey, StreamId, StreamDescription, CurrencyRpt, Amount_Cur)
	VALUES(@Refnum, @CalDateKey, @StreamId, @StreamDescription, @CurrencyRpt, @Amount_Cur);

END