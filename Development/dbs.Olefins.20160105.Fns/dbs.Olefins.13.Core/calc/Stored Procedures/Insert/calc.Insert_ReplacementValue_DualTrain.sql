﻿CREATE PROCEDURE [calc].[Insert_ReplacementValue_DualTrain]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplacementValue(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReplacementValueId, ReplacementValue)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			'DualTrain',
			CASE WHEN c.PyroEthyleneCap_kMT < 550
				THEN 0
				ELSE rfx.Capability_ISBL * 0.15 * calc.MinValue(1.0, (c.PyroEthyleneCap_kMT - 550.0) / 350.0)
				END
		FROM @fpl									fpl
		INNER JOIN inter.ReplValPyroCapacity		c
			ON	c.FactorSetId = fpl.FactorSetId
			AND	c.Refnum = fpl.Refnum
			AND	c.CalDateKey = fpl.Plant_QtrDateKey
			AND	c.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN	calc.ReplValPyroFlexibility		rfx
			ON	rfx.FactorSetId = fpl.FactorSetId
			AND	rfx.Refnum = fpl.Refnum
			AND	rfx.CalDateKey = fpl.Plant_QtrDateKey
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;