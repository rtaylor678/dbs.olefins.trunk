﻿CREATE PROCEDURE [calc].[Insert_Edc_SuppTot]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[Edc]([FactorSetId], [Refnum], [CalDateKey], [EdcId], [EdcDetail], [kEdc])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'SuppTot',
			'SuppTot',
			(COALESCE(SUM(qSupp.[Quantity_kMT] * qRec.[Recovered_WtPcnt]) / cUtil.[_UtilizationStream_Pcnt] * 1000.0 / 365.0, 0.0)) * k.[Value] / 1000.0
		FROM @fpl											fpl

		INNER JOIN [fact].[Quantity]						qSupp
			ON	qSupp.[Refnum]		= fpl.[Refnum]
			AND	qSupp.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	qSupp.StreamId		IN ('ConcEthylene', 'ConcPropylene', 'ROGEthylene', 'ROGPropylene', 'DilEthylene', 'DilPropylene')
		INNER JOIN [fact].[QuantitySuppRecovery]			qRec
			ON	qRec.[Refnum]		= fpl.[Refnum]
			AND	qRec.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	qRec.[StreamId]		= qSupp.[StreamId]

		INNER JOIN [calc].[CapacityUtilization]				cUtil
			ON	cUtil.[FactorSetId]	= fpl.[FactorSetId]
			AND	cUtil.[Refnum]		= fpl.[Refnum]
			AND	cUtil.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	cUtil.[SchedId]		= 'P'
			AND	cUtil.[ComponentId]	= 'ProdOlefins'

		INNER JOIN [calc].[PeerGroupFeedClass]				f
			ON	f.[FactorSetId]		= fpl.[FactorSetId]
			AND	f.[Refnum]			= fpl.[Refnum]
			AND	f.[CalDateKey]		= fpl.[Plant_AnnDateKey]

		INNER JOIN (VALUES
			('1', 'SuppEthane'),
			('2', 'SuppLPG'),
			('3', 'SuppLiquid'),
			('4', 'SuppLiquid')
			)												map ([PeerGroup], [EdcId])
			ON map.[PeerGroup]	= f.[PeerGroup]
		INNER JOIN [ante].[EdcCoefficients]					k
			ON	k.[FactorSetId]	= fpl.[FactorSetId]
			AND	k.[EdcId]		= map.[EdcId]
	
		WHERE	fpl.[FactorSet_AnnDateKey] > 20130000
			AND	cUtil.[_UtilizationStream_Pcnt]	<> 0.0
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			cUtil.[_UtilizationStream_Pcnt],
			k.[Value];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;