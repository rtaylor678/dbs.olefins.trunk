﻿CREATE PROCEDURE [calc].[Insert_PeerGroup_Edc]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PeerGroupEdc(FactorSetId, Refnum, CalDateKey, PeerGroup, [kEdc])
		SELECT
			pg.FactorSetId,
			a.Refnum,
			a.CalDateKey,
			pg.PeerGroup,
			ABS(a.kEdc)
		FROM @fpl								fpl
		INNER JOIN	ante.PeerGroupEdc			pg
			ON	pg.FactorSetId = fpl.FactorSetId
		INNER JOIN calc.EdcAggregate		a
			ON	a.FactorSetId = fpl.FactorSetId
			AND	a.Refnum = fpl.Refnum
			AND	a.CalDateKey = fpl.Plant_QtrDateKey
			AND	ABS(a.kEdc) >=  pg.LimitLower_Edc
			AND	ABS(a.kEdc) <	pg.LimitUpper_Edc
		WHERE	fpl.CalQtr = 4
			AND	a.EdcId = 'kEdcTot';

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;