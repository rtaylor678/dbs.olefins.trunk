﻿CREATE PROCEDURE [calc].[Insert_NicenessEnergy_Feed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessEnergy]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [Stream_Pcnt], [NicenessYield_kMT], [NicenessYield_Pcnt], [NicenessYield_Tot], [NicenessEnergy_kMT], [NicenessEnergy_Pcnt], [NicenessEnergy_Tot])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			nf.[StreamId],
			SUM(nf.[Stream_kMT])											[Stream_kMT],
			SUM(nf.[Stream_kMT])
				/ SUM(SUM(nf.[Stream_kMT]))			OVER() * 100.0			[Stream_Pcnt],

			SUM(nf.[NicenessYield_kMT])										[NicenessYield_kMT],
			SUM(nf.[NicenessYield_kMT])
				/ SUM(SUM(nf.[NicenessYield_kMT]))	OVER() * 100.0			[NicenessYield_Pcnt],
			SUM(nf.[NicenessYield_kMT])
				/ SUM(SUM(nf.[NicenessYield_kMT]))	OVER() * co.[Yield]		[NicenessYield_Tot],

			SUM(nf.[NicenessEnergy_kMT])									[NicenessEnergy_kMT],
			SUM(nf.[NicenessEnergy_kMT])
				/ SUM(SUM(nf.[NicenessEnergy_kMT]))	OVER() * 100.0			[NicenessEnergy_Pcnt],
			SUM(nf.[NicenessEnergy_kMT])
				/ SUM(SUM(nf.[NicenessEnergy_kMT]))	OVER() * co.[Energy]	[NicenessEnergy_Tot]
		FROM @fpl											fpl
		INNER JOIN [calc].[NicenessFeed]					nf
			ON	nf.[FactorSetId]	= fpl.[FactorSetId]
			AND	nf.[Refnum]			= fpl.[Refnum]
			AND	nf.[CalDateKey]		= fpl.[Plant_QtrDateKey]
		LEFT OUTER JOIN [ante].[NicenessModelCoefficients]	co
			ON	co.[FactorSetId]	= fpl.[FactorSetId]
			AND	co.[StreamId]		= nf.[StreamId]
		WHERE	fpl.[CalQtr]		= 4
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			nf.[StreamId],
			co.[Yield],
			co.[Energy];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;