﻿CREATE VIEW [calc].[PricingPlantStreamMetaAggregate]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.ScenarioId,
	p.Refnum,
	MAX(p.CalDateKey)								[CalDateKey],
	p.CurrencyRpt,
	b.StreamId,
	CASE WHEN b.StreamId IN ('FeedLtOther', 'LiqLightOther', 'LiqHeavyOther', 'FeedLiqOther', 'ProdOther') THEN p.StreamDescription END	[StreamDescription],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p.Quantity_kMT
		WHEN '-' THEN - p.Quantity_kMT
		END)							[Quantity_kMT],

	ABS(SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p._Calc_Amount_Cur
		WHEN '-' THEN - p._Calc_Amount_Cur
		END))							[Calc_Amount_Cur],

	ABS(SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p._Calc_Amount_Cur * p.Quantity_kMT
		WHEN '-' THEN - p._Calc_Amount_Cur * p.Quantity_kMT
		END) /
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p.Quantity_kMT
		WHEN '-' THEN - p.Quantity_kMT
		END))							[Avg_Calc_Amount_Cur],

	ABS(SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p._Stream_Value_Cur
		WHEN '-' THEN - p._Stream_Value_Cur
		END))							[Stream_Value_Cur],

	ABS(AVG(CASE b.DescendantOperator
		WHEN '+' THEN + p._Stream_Value_Cur
		WHEN '-' THEN - p._Stream_Value_Cur
		END))							[Avg_Stream_Value_Cur]

FROM calc.PricingPlantStream			p
INNER JOIN dim.Stream_Meta_Bridge		b
	ON	b.FactorSetId	= p.FactorSetId
	AND	b.DescendantId	= p.StreamId
GROUP BY
	p.FactorSetId,
	p.ScenarioId,
	p.Refnum,
	p.CurrencyRpt,
	b.StreamId,
	CASE WHEN b.StreamId IN ('FeedLtOther', 'LiqLightOther', 'LiqHeavyOther', 'FeedLiqOther', 'ProdOther') THEN p.StreamDescription END
HAVING
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p.Quantity_kMT
		WHEN '-' THEN - p.Quantity_kMT
		END)	<> 0.0;
