﻿CREATE VIEW [calc].[ManufacturingCost]
WITH SCHEMABINDING
AS
SELECT
	a.FactorSetId,
	a.Refnum,
	a.CalDateKey,
	'ActualProdCost'		[AccountId],
	ISNULL(p.Amount_Cur, 0.0) - ISNULL(f.Amount_Cur, 0.0) - ISNULL(a.Amount_Cur / 1000.0, 0.0)	[Amount_Cur]
FROM calc.OpExAggregate						a
LEFT OUTER JOIN	fact.GenPlantCostsRevenue	f
	ON	f.Refnum = a.Refnum
	AND	f.CalDateKey = f.CalDateKey
	AND	f.CurrencyRpt = a.CurrencyRpt
	AND	f.AccountId = 'FreshPyroFeed'
LEFT OUTER JOIN	fact.GenPlantCostsRevenue	p
	ON	p.Refnum = a.Refnum
	AND	p.CalDateKey = f.CalDateKey
	AND	p.CurrencyRpt = a.CurrencyRpt
	AND	p.AccountId = 'Prod'
WHERE	a.AccountId = 'TotCashOpEx'
	AND ISNULL(p.Amount_Cur, 0.0) - ISNULL(f.Amount_Cur, 0.0) - ISNULL(a.Amount_Cur / 1000.0, 0.0) <> 0.0