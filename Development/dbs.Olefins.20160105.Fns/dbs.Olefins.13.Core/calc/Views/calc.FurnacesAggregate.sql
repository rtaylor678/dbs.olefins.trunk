﻿CREATE VIEW calc.FurnacesAggregate
WITH SCHEMABINDING
AS
SELECT
	f.FactorSetId,
	f.Refnum,
	f.CalDateKey,
	f.CurrencyRpt,
	SUM(ISNULL(f._InflAdjAnn_MaintRetubeMatl_Cur, 0.0))			[InflAdjAnn_MaintRetubeMatl_Cur],
	SUM(ISNULL(f._InflAdjAnn_MaintRetubeLabor_Cur, 0.0))		[InflAdjAnn_MaintRetubeLabor_Cur],
	SUM(ISNULL(f._InflAdjAnn_MaintRetubeMatl_Cur, 0.0) + ISNULL(f._InflAdjAnn_MaintRetubeLabor_Cur, 0.0))
																[InflAdjAnn_MaintRetube_Cur],
	SUM(ISNULL(f._InflAdjAnn_MaintLabor_Hrs, 0.0))				[InflAdjAnn_MaintLabor_Hrs],
	SUM(ISNULL(f.MaintMajor_Cur, 0.0))							[MaintMajor_Cur],
	COUNT_BIG(*)												[IndexItems]															
FROM calc.Furnaces f
GROUP BY
	f.FactorSetId,
	f.Refnum,
	f.CalDateKey,
	f.CurrencyRpt;
GO
CREATE UNIQUE CLUSTERED INDEX [UX_FurnacesAggregate]
    ON [calc].[FurnacesAggregate]([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC);

