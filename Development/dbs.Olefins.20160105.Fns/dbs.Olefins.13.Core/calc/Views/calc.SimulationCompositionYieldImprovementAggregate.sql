﻿CREATE VIEW [calc].[SimulationCompositionYieldImprovementAggregate]
WITH SCHEMABINDING
AS
SELECT
	s.[FactorSetId],
	s.[Refnum],
	s.[CalDateKey],
    s.[SimModelId],
    s.[OpCondId],
	s.[RecycleId],
 	d.[ComponentId],
	SUM(
		CASE d.DescendantOperator
		WHEN '+' THEN + s.[_ProductionYield_kMT]
		WHEN '-' THEN - s.[_ProductionYield_kMT]
		END)	[ComponentYield_kMT]
--	Task 141: Implement Tri-Tables in calculations
--FROM dim.BridgeComponentLu_Composition							d
--INNER JOIN [calc].[SimulationCompositionYieldImprovement]		s
--	ON	s.ComponentId = d.DescendantId
FROM [calc].[SimulationCompositionYieldImprovement]		s
INNER JOIN dim.Component_Bridge							d
	ON	d.FactorSetId = s.FactorSetId
	AND	d.DescendantId = s.ComponentId
GROUP BY
	s.[FactorSetId],
	s.[Refnum],
	s.[CalDateKey],
    s.[SimModelId],
    s.[OpCondId],
	s.[RecycleId],
 	d.[ComponentId]
HAVING 
	SUM(
		CASE d.DescendantOperator
		WHEN '+' THEN + s.[_ProductionYield_kMT]
		WHEN '-' THEN - s.[_ProductionYield_kMT]
		END) IS NOT NULL;