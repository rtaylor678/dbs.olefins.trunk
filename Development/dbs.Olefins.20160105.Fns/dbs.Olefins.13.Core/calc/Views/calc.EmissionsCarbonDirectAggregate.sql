﻿CREATE VIEW [calc].[EmissionsCarbonDirectAggregate]
WITH SCHEMABINDING
AS
SELECT
	ecd.FactorSetId,
	ecd.Refnum,
	ecd.SimModelId,
	ecd.CalDateKey,
	b.EmissionsId,

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecd.Quantity_MBtu
		WHEN '-' THEN 	ecd.Quantity_MBtu
		END)				[Quantity_MBtu],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecd.EmissionsCarbon_MTCO2e
		WHEN '-' THEN 	ecd.EmissionsCarbon_MTCO2e
		END)/
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecd.Quantity_MBtu
		WHEN '-' THEN 	ecd.Quantity_MBtu
		END)				[CefGwp],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecd.EmissionsCarbon_MTCO2e
		WHEN '-' THEN 	ecd.EmissionsCarbon_MTCO2e
		END)				[EmissionsCarbon_MTCO2e]

FROM calc.EmissionsCarbonDirect				ecd
INNER JOIN dim.EmissionsStandard_Bridge		b
	ON	b.FactorSetId	= ecd.FactorSetId
	AND	b.DescendantId	= ecd.EmissionsId
GROUP BY
	ecd.FactorSetId,
	ecd.Refnum,
	ecd.SimModelId,
	ecd.CalDateKey,
	b.EmissionsId
HAVING
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecd.Quantity_MBtu
		WHEN '-' THEN 	ecd.Quantity_MBtu
		END)	<> 0.0