﻿CREATE TABLE [calc].[StandardEnergy] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [SimModelId]           VARCHAR (12)       NOT NULL,
    [StandardEnergyId]     VARCHAR (42)       NOT NULL,
    [StandardEnergyDetail] VARCHAR (256)      NOT NULL,
    [StandardEnergy]       REAL               NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_CalcStandardEnergy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_CalcStandardEnergy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_CalcStandardEnergy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_CalcStandardEnergy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_StandardEnergy] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [StandardEnergyId] ASC, [StandardEnergyDetail] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_StandardEnergy_StandardEnergy] CHECK ([StandardEnergy]>=(0.0)),
    CONSTRAINT [FK_calc_StandardEnergy_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_StandardEnergy_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_StandardEnergy_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_StandardEnergy_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_calc_StandardEnergy_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_CalcStandardEnergy_u
	ON  calc.StandardEnergy
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.StandardEnergy
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.StandardEnergy.FactorSetId				= INSERTED.FactorSetId
		AND	calc.StandardEnergy.Refnum					= INSERTED.Refnum
		AND calc.StandardEnergy.CalDateKey				= INSERTED.CalDateKey
		AND calc.StandardEnergy.SimModelId				= INSERTED.SimModelId
		AND calc.StandardEnergy.StandardEnergyId		= INSERTED.StandardEnergyId
		AND calc.StandardEnergy.StandardEnergyDetail	= INSERTED.StandardEnergyDetail;
				
END