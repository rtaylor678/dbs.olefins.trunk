﻿CREATE PROCEDURE [fact].[Insert_PersAbsence]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.PersAbsence(Refnum, CalDateKey, PersId, Absence_Pcnt)
		SELECT
			  etl.ConvRefnum(u.Refnum, t.StudyYear, 'PCH')					[Refnum]
			, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
			, etl.ConvPersID(u.PersId)
			, CASE WHEN u.Pcnt > 1.0 THEN u.Pcnt / 100.0 ELSE u.Pcnt END * 100.0	[Pcnt]
		FROM(
			SELECT
				  m.Refnum
				, m.OccAbsence	[Occ]
				, m.MpsAbsence	[Mps]
			FROM stgFact.Misc m
			) p
			UNPIVOT (
			Pcnt FOR PersId IN(
				  [Occ]
				, [Mps]
				)
			) u
		INNER JOIN stgFact.TSort t ON t.Refnum = u.Refnum
		WHERE etl.ConvRefnum(u.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;