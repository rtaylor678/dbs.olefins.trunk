﻿CREATE TABLE [fact].[EnergyComposition] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [AccountId]        VARCHAR (42)       NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_EnergyComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_EnergyComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_EnergyComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyComposition] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AccountId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyComposition_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_EnergyComposition_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_EnergyComposition_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_EnergyComposition_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergyComposition_u]
	ON [fact].[EnergyComposition]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyComposition]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyComposition].Refnum			= INSERTED.Refnum
		AND [fact].[EnergyComposition].AccountId		= INSERTED.AccountId
		AND [fact].[EnergyComposition].ComponentId		= INSERTED.ComponentId
		AND [fact].[EnergyComposition].CalDateKey		= INSERTED.CalDateKey;

END;