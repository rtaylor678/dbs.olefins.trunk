﻿CREATE TABLE [fact].[PolyQuantityNameOther] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AssetId]        VARCHAR (30)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [StreamName]     NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PolyQuantityNameOther_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PolyQuantityNameOther_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PolyQuantityNameOther_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PolyQuantityNameOther_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PolyQuantityNameOther] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AssetId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [Cl_PolyQuantityNameOther_StreamName] CHECK (len([StreamName])>(0)),
    CONSTRAINT [FK_PolyQuantityNameOther_Assets] FOREIGN KEY ([AssetId]) REFERENCES [cons].[Assets] ([AssetId]),
    CONSTRAINT [FK_PolyQuantityNameOther_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PolyQuantityNameOther_PolyFacilities] FOREIGN KEY ([Refnum], [AssetId], [CalDateKey]) REFERENCES [fact].[PolyFacilities] ([Refnum], [AssetId], [CalDateKey]),
    CONSTRAINT [FK_PolyQuantityNameOther_PolyQuantity] FOREIGN KEY ([Refnum], [AssetId], [StreamId], [CalDateKey]) REFERENCES [fact].[PolyQuantity] ([Refnum], [AssetId], [StreamId], [CalDateKey]),
    CONSTRAINT [FK_PolyQuantityNameOther_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_PolyQuantityNameOther_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_PolyQuantityNameOther_u]
	ON [fact].[PolyQuantityNameOther]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[PolyQuantityNameOther]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[PolyQuantityNameOther].Refnum		= INSERTED.Refnum
		AND [fact].[PolyQuantityNameOther].AssetId		= INSERTED.AssetId
		AND [fact].[PolyQuantityNameOther].StreamId		= INSERTED.StreamId
		AND [fact].[PolyQuantityNameOther].CalDateKey	= INSERTED.CalDateKey;

END;