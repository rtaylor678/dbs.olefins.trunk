﻿CREATE TABLE [fact].[GenPlantInventory] (
    [Refnum]                VARCHAR (25)       NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [FacilityId]            VARCHAR (42)       NOT NULL,
    [StorageCapacity_kMT]   REAL               NOT NULL,
    [StorageLevel_Pcnt]     REAL               NULL,
    [_YearEndInventory_kMT] AS                 (CONVERT([real],(coalesce([StorageCapacity_kMT],(0.0))*coalesce([StorageLevel_Pcnt],(0.0)))/(100.0),(1))),
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantInventory_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_GenPlantInventory_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_GenPlantInventory_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_GenPlantInventory_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantInventory] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantInventory_StorageCapacity_kMT] CHECK ([StorageCapacity_kMT]>=(0.0)),
    CONSTRAINT [CR_GenPlantInventory_StorageLevel_Pcnt] CHECK ([StorageLevel_Pcnt]>=(0.0) AND [StorageLevel_Pcnt]<=(100.0)),
    CONSTRAINT [FK_GenPlantInventory_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantInventory_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_GenPlantInventory_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantInventory_u]
	ON [fact].[GenPlantInventory]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantInventory]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantInventory].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlantInventory].FacilityId	= INSERTED.FacilityId
		AND [fact].[GenPlantInventory].CalDateKey	= INSERTED.CalDateKey;

END;