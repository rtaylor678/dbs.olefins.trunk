﻿
CREATE VIEW fact.FeedSelLogisticsPivot
WITH SCHEMABINDING
AS
SELECT
	  p.Refnum
	, p.CalDateKey
	, p.FacilityId
	, p.LPG			[LPG_Pcnt]
	, p.Condensate	[Condensate_Pcnt]
	, p.Naphtha		[Naphtha_Pcnt]
	, p.LiqHeavy	[LiqHeavy_Pcnt]
FROM(
	SELECT
		  l.Refnum
		, l.CalDateKey
		, ISNULL(l.FacilityId, 'Logistics (Total)')	[FacilityId]
		, l.StreamId
		, SUM(l.Delivery_Pcnt)	[Delivery_Pcnt]
	FROM fact.FeedSelLogistics l
	GROUP BY ROLLUP(l.FacilityId)
		, l.Refnum
		, l.CalDateKey
		, l.StreamId
	) u
	PIVOT(
	MAX(Delivery_Pcnt) FOR StreamId IN(
		  LPG
		, Condensate
		, Naphtha
		, LiqHeavy
		)
	) p;