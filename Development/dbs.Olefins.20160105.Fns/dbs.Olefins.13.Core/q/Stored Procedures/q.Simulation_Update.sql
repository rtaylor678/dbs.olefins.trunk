﻿
CREATE PROCEDURE q.Simulation_Update(
	  @tsQueued				DATETIMEOFFSET(7)	= NULL
	, @tsStart				DATETIMEOFFSET(7)	= NULL
	, @tsComplete			DATETIMEOFFSET(7)	= NULL
	, @ItemsQueued			INT					= NULL
	, @ItemsProcessed		INT					= NULL
	, @ErrorID				VARCHAR(12)			= NULL
	----------------------------------------------
	, @QueueID_init			BIGINT
	)
AS
	UPDATE q.Simulation
	SET	  tsQueued			= ISNULL(@tsQueued		, tsQueued)
		, tsStart			= ISNULL(@tsStart		, tsStart)
		, tsComplete		= ISNULL(@tsComplete	, tsComplete)
		, ItemsQueued		= ISNULL(@ItemsQueued	, ItemsQueued)
		, ItemsProcessed	= ISNULL(@ItemsProcessed, ItemsProcessed)
		, ErrorID			= ISNULL(@ErrorID		, ErrorID)
	WHERE QueueID = @QueueID_init;