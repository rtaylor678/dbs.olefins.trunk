﻿CREATE TABLE [dim].[SimErrorMessages_LookUp] (
    [ModelId]        VARCHAR (12)       NOT NULL,
    [ErrorId]        VARCHAR (12)       NOT NULL,
    [ErrorType]      VARCHAR (15)       NOT NULL,
    [ErrMessage]     VARCHAR (256)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_SimErrorMessages_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_SimErrorMessages_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_SimErrorMessages_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_SimErrorMessages_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SimErrorMessages_LookUp] PRIMARY KEY CLUSTERED ([ModelId] ASC, [ErrorId] ASC),
    CONSTRAINT [CL_SimErrorMessages_LookUp_ErrMessage] CHECK ([ErrMessage]<>''),
    CONSTRAINT [CL_SimErrorMessages_LookUp_ErrorId] CHECK ([ErrorId]<>''),
    CONSTRAINT [CL_SimErrorMessages_LookUp_ErrorType] CHECK ([ErrorType]<>''),
    CONSTRAINT [FK_SimErrorMessages_LookUp_SimModelLu] FOREIGN KEY ([ModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId])
);


GO

CREATE TRIGGER [dim].[t_SimErrorMessages_LookUp_u]
	ON [dim].[SimErrorMessages_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[SimErrorMessages_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[SimErrorMessages_LookUp].[ModelId]	= INSERTED.[ModelId]
		AND	[dim].[SimErrorMessages_LookUp].[ErrorId]	= INSERTED.[ErrorId];
		
END;
