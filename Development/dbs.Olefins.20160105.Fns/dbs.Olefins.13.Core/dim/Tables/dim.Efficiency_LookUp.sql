﻿CREATE TABLE [dim].[Efficiency_LookUp] (
    [EfficiencyId]     VARCHAR (42)       NOT NULL,
    [EfficiencyName]   NVARCHAR (84)      NOT NULL,
    [EfficiencyDetail] NVARCHAR (348)     NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_Efficiency_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_Efficiency_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_Efficiency_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_Efficiency_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Efficiency_LookUp] PRIMARY KEY CLUSTERED ([EfficiencyId] ASC),
    CHECK ([EfficiencyDetail]<>''),
    CHECK ([EfficiencyId]<>''),
    CHECK ([EfficiencyName]<>''),
    UNIQUE NONCLUSTERED ([EfficiencyDetail] ASC),
    UNIQUE NONCLUSTERED ([EfficiencyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Efficiency_LookUp_u]
	ON [dim].[Efficiency_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Efficiency_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Efficiency_LookUp].[EfficiencyId]	= INSERTED.[EfficiencyId];
		
END;
