﻿CREATE TABLE [dim].[ReliabilityEventCause_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [EventCauseId]       VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ReliabilityEventCause_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityEventCause_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityEventCause_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityEventCause_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReliabilityEventCause_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityEventCause_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EventCauseId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ReliabilityEventCause_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ReliabilityEventCause_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ReliabilityEventCause_LookUp] ([EventCauseId]),
    CONSTRAINT [FK_ReliabilityEventCause_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityEventCause_Bridge_LookUp_EventCause] FOREIGN KEY ([EventCauseId]) REFERENCES [dim].[ReliabilityEventCause_LookUp] ([EventCauseId]),
    CONSTRAINT [FK_ReliabilityEventCause_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [EventCauseId]) REFERENCES [dim].[ReliabilityEventCause_Parent] ([FactorSetId], [EventCauseId]),
    CONSTRAINT [FK_ReliabilityEventCause_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ReliabilityEventCause_Parent] ([FactorSetId], [EventCauseId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityEventCause_Bridge_u]
ON [dim].[ReliabilityEventCause_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityEventCause_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityEventCause_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityEventCause_Bridge].[EventCauseId]		= INSERTED.[EventCauseId]
		AND	[dim].[ReliabilityEventCause_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;