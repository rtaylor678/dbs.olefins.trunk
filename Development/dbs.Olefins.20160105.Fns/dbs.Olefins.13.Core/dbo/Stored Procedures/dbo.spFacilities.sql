﻿






CREATE              PROC [dbo].[spFacilities](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @FracFeedCnt real,@PyroFurnCnt real,@TowerQuenchCnt real,@QWaterSysCnt real,@TowerGasCnt real,@CompGasCnt real,@TowerCausticCnt real,@DryerProcessCnt real,
	@MethSysCnt real,@TowerDemethanizerCnt real,@TowerDeethanizerCnt real,@AcetylCnt real,@TowerEthyleneCnt real,@TowerDepropanizerCnt real,@TowerPropyleneCnt real,
	@TowerDebutanizerCnt real,@CompRefrigPropyleneCnt real,@CompRefrigEthyleneCnt real,@CompRefrigMethaneCnt real,@BoilFiredSteamCnt real,@ElecGenDistCnt real,
	@SteamSuperHeatCnt real,@TowerCoolCnt real,@HydroPurCnt real,@EthyLiqCnt real,@TowerPyroGasHTCnt real,@RGRCnt real,@VesselOthCnt real,@FacilitiesCnt real
Select 
  @FracFeedCnt = SUM(CASE WHEN acc.FacilityId = 'FracFeed' THEN fc.Unit_Count ELSE 0 END)
, @PyroFurnCnt = SUM(CASE WHEN acc.FacilityId = 'PyroFurn' THEN fc.Unit_Count ELSE 0 END)
, @TowerQuenchCnt = SUM(CASE WHEN acc.FacilityId = 'TowerQuench' THEN fc.Unit_Count ELSE 0 END)
, @QWaterSysCnt = SUM(CASE WHEN acc.FacilityId = 'QWaterSys' THEN fc.Unit_Count ELSE 0 END)
, @TowerGasCnt = SUM(CASE WHEN acc.FacilityId = 'TowerGas' THEN fc.Unit_Count ELSE 0 END)
, @CompGasCnt = SUM(CASE WHEN acc.FacilityId = 'CompGas' THEN fc.Unit_Count ELSE 0 END)
, @TowerCausticCnt = SUM(CASE WHEN acc.FacilityId = 'TowerCaustic' THEN fc.Unit_Count ELSE 0 END)
, @DryerProcessCnt = SUM(CASE WHEN acc.FacilityId = 'DryerProcess' THEN fc.Unit_Count ELSE 0 END)
, @MethSysCnt = SUM(CASE WHEN acc.FacilityId = 'MethSys' THEN fc.Unit_Count ELSE 0 END)
, @TowerDemethanizerCnt = SUM(CASE WHEN acc.FacilityId = 'TowerDemethanizer' THEN fc.Unit_Count ELSE 0 END)
, @TowerDeethanizerCnt = SUM(CASE WHEN acc.FacilityId = 'TowerDeethanizer' THEN fc.Unit_Count ELSE 0 END)
, @AcetylCnt = SUM(CASE WHEN acc.FacilityId = 'Acetyl' THEN fc.Unit_Count ELSE 0 END)
, @TowerEthyleneCnt = SUM(CASE WHEN acc.FacilityId = 'TowerEthylene' THEN fc.Unit_Count ELSE 0 END)
, @TowerDepropanizerCnt = SUM(CASE WHEN acc.FacilityId = 'TowerDepropanizer' THEN fc.Unit_Count ELSE 0 END)
, @TowerPropyleneCnt = SUM(CASE WHEN acc.FacilityId = 'TowerPropylene' THEN fc.Unit_Count ELSE 0 END)
, @TowerDebutanizerCnt = SUM(CASE WHEN acc.FacilityId = 'TowerDebutanizer' THEN fc.Unit_Count ELSE 0 END)
, @CompRefrigPropyleneCnt = SUM(CASE WHEN acc.FacilityId = 'CompRefrigPropylene' THEN fc.Unit_Count ELSE 0 END)
, @CompRefrigEthyleneCnt = SUM(CASE WHEN acc.FacilityId = 'CompRefrigEthylene' THEN fc.Unit_Count ELSE 0 END)
, @CompRefrigMethaneCnt = SUM(CASE WHEN acc.FacilityId = 'CompRefrigMethane' THEN fc.Unit_Count ELSE 0 END)
, @BoilFiredSteamCnt = SUM(CASE WHEN acc.FacilityId = 'BoilFiredSteam' THEN fc.Unit_Count ELSE 0 END)
, @ElecGenDistCnt = SUM(CASE WHEN acc.FacilityId = 'ElecGenDist' THEN fc.Unit_Count ELSE 0 END)
, @SteamSuperHeatCnt = SUM(CASE WHEN acc.FacilityId = 'SteamSuperHeat' THEN fc.Unit_Count ELSE 0 END)
, @TowerCoolCnt = SUM(CASE WHEN acc.FacilityId = 'TowerCool' THEN fc.Unit_Count ELSE 0 END)
, @HydroPurCnt = SUM(CASE WHEN acc.FacilityId = 'HydroPur' THEN fc.Unit_Count ELSE 0 END)
, @EthyLiqCnt = SUM(CASE WHEN acc.FacilityId = 'EthyLiq' THEN fc.Unit_Count ELSE 0 END)
, @TowerPyroGasHTCnt = SUM(CASE WHEN acc.FacilityId = 'TowerPyroGasHT' THEN fc.Unit_Count ELSE 0 END)
, @RGRCnt = SUM(CASE WHEN acc.FacilityId = 'RGR' THEN fc.Unit_Count ELSE 0 END)
, @VesselOthCnt = SUM(CASE WHEN acc.FacilityId = 'VesselOth' THEN fc.Unit_Count ELSE 0 END)
, @FacilitiesCnt = SUM(CASE WHEN acc.FacilityId = 'Facilities' THEN fc.Unit_Count ELSE 0 END)

FROM dbo.Divisors d CROSS APPLY (VALUES ('FracFeed'), ('PyroFurn'), ('TowerQuench'), ('QWaterSys'), ('TowerGas'), ('CompGas'), ('TowerCaustic'), 
	('DryerProcess'), ('MethSys'), ('TowerDemethanizer'), ('TowerDeethanizer'), ('Acetyl'), ('TowerEthylene'), ('TowerDepropanizer'), ('TowerPropylene'), 
	('TowerDebutanizer'), ('CompRefrigPropylene'), ('CompRefrigEthylene'), ('CompRefrigMethane'), ('BoilFiredSteam'), ('ElecGenDist'), 
	('SteamSuperHeat'), ('TowerCool'), ('HydroPur'), ('EthyLiq'), ('TowerPyroGasHT'), ('RGR'), ('VesselOth'), ('Facilities')) acc(FacilityId)
LEFT OUTER JOIN calc.FacilitiesCountComplexity fc on fc.Refnum=d.Refnum and fc.FacilityId=acc.FacilityId
Where d.Refnum = @Refnum and fc.FactorSetId = @FactorSetId

DECLARE @SingleTrain_pcnt real, @DualTrain_pcnt real, @ThreeTrain_pcnt real
SELECT 
  @SingleTrain_pcnt = SUM(CASE WHEN TA.TrainCnt = 1 THEN 100 ELSE 0.0 END)
, @DualTrain_pcnt = SUM(CASE WHEN TA.TrainCnt = 2 THEN 100 ELSE 0.0 END)
, @ThreeTrain_pcnt = SUM(CASE WHEN TA.TrainCnt = 3 THEN 100 ELSE 0.0 END)
FROM 
(SELECT TrainCnt = COUNT(*) From calc.TaAdjReliability t WHERE t.Refnum=@Refnum AND t.FactorSetId = @FactorSetId
	AND t.TurnAroundCost_MCur > 0 Group by t.Refnum) TA

DECLARE @EDCk_FracFeed real,@EDCk_FeedStockMix real,@EDCk_PyroFurn real,@EDCk_Comp real,@EDCk_SuppTot real,@EDCk_Utly real,@EDCk_HydroPur real,@EDCk_TowerPyroGasHT real,@EDCk real
SELECT
  @EDCk_FracFeed = ISNULL(SUM(CASE WHEN acc.EdcId = 'FracFeed' THEN e.kEdc ELSE 0 END),0) 
, @EDCk_FeedStockMix = ISNULL(SUM(CASE WHEN acc.EdcId in ('Light','Liquid') THEN e.kEdc ELSE 0 END),0) 
, @EDCk_PyroFurn = ISNULL(SUM(CASE WHEN acc.EdcId = 'PyroFurn' THEN e.kEdc ELSE 0 END),0) 
, @EDCk_Comp = ISNULL(SUM(CASE WHEN acc.EdcId = 'Comp' THEN e.kEdc ELSE 0 END),0) 
, @EDCk_SuppTot = ISNULL(SUM(CASE WHEN acc.EdcId = 'SuppTot' THEN e.kEdc ELSE 0 END),0) 
, @EDCk_Utly = ISNULL(SUM(CASE WHEN acc.EdcId in ('BoilFiredSteam','ElecGenDist') THEN e.kEdc ELSE 0 END),0) 
, @EDCk_HydroPur = ISNULL(SUM(CASE WHEN acc.EdcId = 'HydroPur' THEN e.kEdc ELSE 0 END),0) 
, @EDCk_TowerPyroGasHT = ISNULL(SUM(CASE WHEN acc.EdcId = 'TowerPyroGasHT' THEN e.kEdc ELSE 0 END),0) 
, @EDCk = ISNULL(SUM(CASE WHEN acc.EdcId = 'kEDCTot' THEN e.kEdc ELSE 0 END),0) 
FROM dbo.Divisors d CROSS APPLY (VALUES ('BoilFiredSteam'), ('Comp'), ('ElecGenDist'), ('FracFeed'), ('HydroPur'), ('kEdcTot'), ('Light'), 
	('Liquid'), ('PyroFurn'), ('SuppTot'), ('TowerPyroGasHT')) acc(EdcId)
LEFT JOIN calc.EdcAggregate e on e.Refnum=d.Refnum and e.EdcId=acc.EdcId
WHERE d.Refnum = @Refnum and e.FactorSetId = @FactorSetId

DECLARE @PyroFurnSpare_Pcnt real, @ElecGen_MW real
Select 
  @PyroFurnSpare_Pcnt = PyroFurnSpare_Pcnt
, @ElecGen_MW = ElecGen_MW
FROM fact.FacilitiesMisc fm WHERE fm.Refnum=@Refnum

DECLARE @CGCompPower_BHP real,@CGCompStages_Count real,@CGCompAge_Years real,@PRCompPower_BHP real,@PRCompAge_Years real,
	@ERCompPower_BHP real,@ERCompAge_Years real,@MRCompPower_BHP real,@MRCompAge_Years real
Select  
  @CGCompPower_BHP		= SUM(CASE WHEN fm.FacilityId = 'CompGas' THEN fm.Power_BHP END)
, @CGCompStages_Count	= SUM(CASE WHEN fm.FacilityId = 'CompGas' THEN fm.Stages_Count END)
, @CGCompAge_Years		= SUM(CASE WHEN fm.FacilityId = 'CompGas' THEN fm.Age_Years END)
, @PRCompPower_BHP		= SUM(CASE WHEN fm.FacilityId = 'CompRefrigPropylene' THEN fm.Power_BHP END)
, @PRCompAge_Years		= SUM(CASE WHEN fm.FacilityId = 'CompRefrigPropylene' THEN fm.Age_Years END)
, @ERCompPower_BHP		= SUM(CASE WHEN fm.FacilityId = 'CompRefrigEthylene' THEN fm.Power_BHP END)
, @ERCompAge_Years		= SUM(CASE WHEN fm.FacilityId = 'CompRefrigEthylene' THEN fm.Age_Years END)
, @MRCompPower_BHP		= SUM(CASE WHEN fm.FacilityId = 'CompRefrigMethane' THEN fm.Power_BHP END)
, @MRCompAge_Years		= SUM(CASE WHEN fm.FacilityId = 'CompRefrigMethane' THEN fm.Age_Years END)
FROM fact.FacilitiesCompressors fm WHERE fm.Refnum=@Refnum

DECLARE @BoilHPPressure_PSIg real,@BoilHPRate_kLbHr real,@BoilLPPressure_PSIg real,@BoilLPRate_kLbHr real
Select 
  @BoilHPPressure_PSIg	= SUM(CASE WHEN fp.FacilityId = 'BoilHP' THEN fp.Pressure_PSIg ELSE 0 END)
, @BoilHPRate_kLbHr		= SUM(CASE WHEN fp.FacilityId = 'BoilHP' THEN fp.Rate_kLbHr ELSE 0 END)
, @BoilLPPressure_PSIg	= SUM(CASE WHEN fp.FacilityId = 'BoilLP' THEN fp.Pressure_PSIg ELSE 0 END)
, @BoilLPRate_kLbHr		= SUM(CASE WHEN fp.FacilityId = 'BoilLP' THEN fp.Rate_kLbHr ELSE 0 END)
FROM fact.FacilitiesPressure fp WHERE fp.Refnum=@Refnum

DECLARE @NHYTFeedRate_kBsd real, @NHYTFeedProcessed_kMT real, @NHYTFeedPressure_PSIG real
Select 
  @NHYTFeedRate_kBsd		= SUM(CASE WHEN fh.FacilityId = 'TowerPyroGasHT' THEN FeedRate_kBsd ELSE 0 END)
, @NHYTFeedProcessed_kMT	= SUM(CASE WHEN fh.FacilityId = 'TowerPyroGasHT' THEN FeedProcessed_kMT ELSE 0 END)
, @NHYTFeedPressure_PSIG	= SUM(CASE WHEN fh.FacilityId = 'TowerPyroGasHT' THEN FeedPressure_PSIg ELSE 0 END)
FROM fact.FacilitiesHydroTreat fh WHERE fh.Refnum=@Refnum

DECLARE @CompGasHPToRefrigHPRatio real
Select 
  @CompGasHPToRefrigHPRatio = CASE WHEN SUM(CASE WHEN fc.FacilityId = 'CompRefrig' THEN fc.Power_BHP ELSE 0 END) >0 THEN
	SUM(CASE WHEN fc.FacilityId = 'CompGas' THEN fc.Power_BHP ELSE 0 END) / SUM(CASE WHEN fc.FacilityId = 'CompRefrig' THEN fc.Power_BHP ELSE 0 END)
	ELSE 0 END
FROM fact.FacilitiesCompressorsAggregate fc WHERE fc.Refnum=@Refnum and fc.FactorSetId = @FactorSetId

DECLARE @RV_ISBL real, @RVAdj_FeedFlex real,@RVAdj_FeedPrep real,@RVAdj_Boilers real,@RVAdj_OtherOffSite real,@RVAdj_FeedSuppl real,
	@RVAdj_DualTrain real,@RVAdj_Owner real,@RV_USGC real,@RV_LocFact real,@RV real
SELECT 
  @RV_ISBL				= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'Base' THEN rv.ReplacementValue ELSE 0 END) ,0)
, @RVAdj_FeedFlex		= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'FeedFlex' THEN rv.ReplacementValue ELSE 0 END),0)
, @RVAdj_FeedPrep		= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'FeedPrep' THEN rv.ReplacementValue ELSE 0 END),0)
, @RVAdj_Boilers		= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'BoilFiredSteamSuperheaters' THEN rv.ReplacementValue ELSE 0 END),0)
, @RVAdj_OtherOffSite	= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'OtherOffSite' THEN rv.ReplacementValue ELSE 0 END),0)
, @RVAdj_FeedSuppl		= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'FeedSupplemetal' THEN rv.ReplacementValue ELSE 0 END),0)
, @RVAdj_DualTrain		= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'DualTrain' THEN rv.ReplacementValue ELSE 0 END),0)
, @RVAdj_Owner			= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'OwnerCostAndStartUp' THEN rv.ReplacementValue ELSE 0 END),0)
, @RV_USGC				= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'UscgReplacement' THEN rv.ReplacementValue ELSE 0 END),0)
, @RV					= ISNULL(SUM(CASE WHEN acc.ReplacementValueId = 'UscgReplacement' THEN rv.ReplacementLocation ELSE 0 END),0)
FROM dbo.Divisors d CROSS APPLY (VALUES ('Base'), ('FeedFlex'), ('FeedPrep'), ('BoilFiredSteamSuperheaters'), ('OtherOffSite'), ('FeedSupplemetal'), ('DualTrain'), 
	('OwnerCostAndStartUp'), ('UscgReplacement')) acc(ReplacementValueId)
LEFT JOIN calc.ReplacementValueAggregate rv on rv.Refnum=d.Refnum  and rv.ReplacementValueId=acc.ReplacementValueId AND rv.FactorSetId = @FactorSetId and rv.CurrencyRpt = 'USD'
and d.Refnum = @Refnum

SELECT @RV_LocFact = @RV - @RV_USGC

DECLARE @IntegrationFeed_Pcnt real, @LogisticsFeed_USDperMT real
Select 
  @IntegrationFeed_Pcnt		= ISNULL([$(DbGlobal)].dbo.WtAvgNN(gpi.Integration_Pcnt, df._TaAdj_FeedPyrolysis_kMT),0)
, @LogisticsFeed_USDperMT	= [$(DbGlobal)].dbo.WtAvg(gpl.Amount_Cur, df._TaAdj_FeedPyrolysis_kMT)
FROM dbo.Divisors d LEFT JOIN fact.GenPlantCommIntegration gpi on gpi.Refnum=d.Refnum and gpi.StreamId = 'FreshPyroFeed'
LEFT JOIN fact.GenPlantLogistics gpl on gpl.Refnum=d.Refnum and gpl.StreamId=gpi.StreamId and gpl.CurrencyRpt = 'USD'
JOIN calc.DivisorsFeed df on df.Refnum=d.Refnum and df.FactorSetId = @FactorSetId and df.StreamId = 'PlantFeed'
WHERE d.Refnum = @Refnum

DECLARE @IntegrationEthylene_Pcnt real, @IntegrationPropylene_Pcnt real, @IntegrationOther_Pcnt real, @LogisticsEthylene_USDperMT real, @LogisticsPropylene_USDperMT real
Select 
  @IntegrationEthylene_Pcnt		= SUM(CASE WHEN df.ComponentId = 'C2H4' THEN gpi.Integration_Pcnt ELSE 0 END)
, @IntegrationPropylene_Pcnt	= SUM(CASE WHEN df.ComponentId = 'C3H6' THEN gpi.Integration_Pcnt ELSE 0 END)
, @IntegrationOther_Pcnt		= SUM(CASE WHEN df.ComponentId = 'ProdHVCOther' THEN gpi.Integration_Pcnt ELSE 0 END)
FROM dbo.Divisors d LEFT JOIN fact.GenPlantCommIntegration gpi on gpi.Refnum=d.Refnum 
LEFT JOIN calc.DivisorsProduction df on df.Refnum=d.Refnum and df.FactorSetId = @FactorSetId
	AND ((df.ComponentId = 'C2H4' and gpi.StreamId = 'Ethylene') OR (df.ComponentId = 'C3H6' and gpi.StreamId = 'Propylene') OR (df.ComponentId = 'ProdHVCOther' and gpi.StreamId = 'Prod'))
WHERE d.Refnum=@Refnum

Select 
  @LogisticsEthylene_USDperMT	= SUM(CASE WHEN acc.StreamId = 'Ethylene' THEN gpl.Amount_Cur ELSE 0 END) 
, @LogisticsPropylene_USDperMT	= SUM(CASE WHEN acc.StreamId = 'Propylene' THEN gpl.Amount_Cur ELSE 0 END) 
FROM dbo.Divisors d CROSS APPLY  (Values ('Ethylene'), ('Propylene')) acc(StreamId)
LEFT JOIN fact.GenPlantLogistics gpl on gpl.Refnum=d.Refnum and gpl.CurrencyRpt = 'USD' and gpl.StreamId=acc.StreamId
LEFT JOIN calc.DivisorsProduction df on df.Refnum=d.Refnum and df.FactorSetId = @FactorSetId
	AND ((df.ComponentId = 'C2H4' and acc.StreamId = 'Ethylene') OR (df.ComponentId = 'C3H6' and acc.StreamId = 'Propylene'))
WHERE d.Refnum=@Refnum

DECLARE @EthyleneCarrier real,@ExtractButdiene real,@ExtractAromatics real,@MTBE real,@Isobutylene real,@IsobutyleneHighPurity real,@Butene1 real,
	@Isoprene real,@CycloPentadiene real,@OtherC5 real,@PolyPlantShare real,@OlefinsDerivatives real,@IntegrationRefinery real,
	@IntegrationOther real, @IntegrationNone real
SELECT
  @EthyleneCarrier		= EthyleneCarrier_Bit * 100.0
, @ExtractButdiene		= ExtractButadiene_Bit * 100.0
, @ExtractAromatics		= ExtractAromatics_Bit * 100.0
, @MTBE					= MTBE_Bit * 100.0
, @Isobutylene			= Isobutylene_Bit * 100.0
, @IsobutyleneHighPurity = IsobutyleneHighPurity_Bit * 100.0
, @Butene1				= Butene1_Bit * 100.0
, @Isoprene				= Isoprene_Bit * 100.0
, @CycloPentadiene		= CycloPentadiene_Bit * 100.0
, @OtherC5				= OtherC5_Bit * 100.0
, @PolyPlantShare		= PolyPlantShare_Bit * 100.0
, @OlefinsDerivatives	= OlefinsDerivatives_Bit * 100.0
, @IntegrationRefinery	= IntegrationRefinery_Bit * 100.0
FROM fact.GenPlant gp WHERE gp.Refnum=@Refnum

SELECT @IntegrationOther = ISNULL(@OlefinsDerivatives,0)
SELECT @IntegrationNone = CASE WHEN ISNULL(@IntegrationRefinery,0) + ISNULL(@PolyPlantShare,0) + ISNULL(@IntegrationOther,0) > 0 THEN 0.0 ELSE 100.0 END

DECLARE @OSIMPrep_Hrs real
SELECT @OSIMPrep_Hrs = g.OSIMPrep_Hrs From fact.GenPlant g where g.Refnum = @Refnum

DECLARE @Startup_Year real, @PlantDensity_SG real
select @Startup_Year = ca.StartUp_Year from fact.CapacityAttributes ca where ca.Refnum = @Refnum
select @PlantDensity_SG = p.PlantDensity_SG from calc.PeerGroupFeedClass p where p.Refnum = @Refnum and p.FactorSetId = @FactorSetId

DELETE FROM dbo.Facilities WHERE Refnum = @Refnum
Insert into dbo.Facilities (Refnum
, FracFeedCnt
, PyroFurnCnt
, TowerQuenchCnt
, QWaterSysCnt
, TowerGasCnt
, CompGasCnt
, TowerCausticCnt
, DryerProcessCnt
, MethSysCnt
, TowerDemethanizerCnt
, TowerDeethanizerCnt
, AcetylCnt
, TowerEthyleneCnt
, TowerDepropanizerCnt
, TowerPropyleneCnt
, TowerDebutanizerCnt
, CompRefrigPropyleneCnt
, CompRefrigEthyleneCnt
, CompRefrigMethaneCnt
, BoilFiredSteamCnt
, ElecGenDistCnt
, SteamSuperHeatCnt
, TowerCoolCnt
, HydroPurCnt
, EthyLiqCnt
, TowerPyroGasHTCnt
, RGRCnt
, VesselOthCnt
, FacilitiesCnt
, SingleTrain_pcnt
, DualTrain_pcnt
, ThreeTrain_pcnt
, EDCk_FracFeed
, EDCk_FeedStockMix
, EDCk_PyroFurn
, EDCk_Comp
, EDCk_SuppTot
, EDCk_Utly
, EDCk_HydroPur
, EDCk_TowerPyroGasHT
, EDCk
, PyroFurnSpare_Pcnt
, CGCompPower_BHP
, CGCompStages_Count
, CGCompAge_Years
, PRCompPower_BHP
, PRCompAge_Years
, ERCompPower_BHP
, ERCompAge_Years
, MRCompPower_BHP
, MRCompAge_Years
, BoilHPPressure_PSIg
, BoilHPRate_kLbHr
, BoilLPPressure_PSIg
, BoilLPRate_kLbHr
, ElecGen_MW
, NHYTFeedRate_kBsd
, NHYTFeedProcessed_kMT
, NHYTFeedPressure_PSIG
, CompGasHPToRefrigHPRatio
, RV_ISBL
, RVAdj_FeedFlex
, RVAdj_FeedPrep
, RVAdj_Boilers
, RVAdj_OtherOffSite
, RVAdj_FeedSuppl
, RVAdj_DualTrain
, RVAdj_Owner
, RV_USGC
, RV_LocFact
, RV
, IntegrationFeed_Pcnt
, IntegrationEthylene_Pcnt
, IntegrationPropylene_Pcnt
, IntegrationOther_Pcnt
, LogisticsFeed_USDperMT
, LogisticsEthylene_USDperMT
, LogisticsPropylene_USDperMT
, EthyleneCarrier
, ExtractButdiene
, ExtractAromatics
, MTBE
, Isobutylene
, IsobutyleneHighPurity
, Butene1
, Isoprene
, CycloPentadiene
, OtherC5
, PolyPlantShare
, OlefinsDerivatives
, IntegrationRefinery
, IntegrationOther
, IntegrationNone
, Startup_Year
, PlantDensity_SG 
, OSIMPrep_Hrs
)

SELECT 
Refnum = @Refnum
, FracFeedCnt = @FracFeedCnt
, PyroFurnCnt = @PyroFurnCnt
, TowerQuenchCnt = @TowerQuenchCnt
, QWaterSysCnt = @QWaterSysCnt
, TowerGasCnt = @TowerGasCnt
, CompGasCnt = @CompGasCnt
, TowerCausticCnt = @TowerCausticCnt
, DryerProcessCnt = @DryerProcessCnt
, MethSysCnt = @MethSysCnt
, TowerDemethanizerCnt = @TowerDemethanizerCnt
, TowerDeethanizerCnt = @TowerDeethanizerCnt
, AcetylCnt = @AcetylCnt
, TowerEthyleneCnt = @TowerEthyleneCnt
, TowerDepropanizerCnt = @TowerDepropanizerCnt
, TowerPropyleneCnt = @TowerPropyleneCnt
, TowerDebutanizerCnt = @TowerDebutanizerCnt
, CompRefrigPropyleneCnt = @CompRefrigPropyleneCnt
, CompRefrigEthyleneCnt = @CompRefrigEthyleneCnt
, CompRefrigMethaneCnt = @CompRefrigMethaneCnt
, BoilFiredSteamCnt = @BoilFiredSteamCnt
, ElecGenDistCnt = @ElecGenDistCnt
, SteamSuperHeatCnt = @SteamSuperHeatCnt
, TowerCoolCnt = @TowerCoolCnt
, HydroPurCnt = @HydroPurCnt
, EthyLiqCnt = @EthyLiqCnt
, TowerPyroGasHTCnt = @TowerPyroGasHTCnt
, RGRCnt = @RGRCnt
, VesselOthCnt = @VesselOthCnt
, FacilitiesCnt = @FacilitiesCnt
, SingleTrain_pcnt = @SingleTrain_pcnt
, DualTrain_pcnt = @DualTrain_pcnt
, ThreeTrain_pcnt = @ThreeTrain_pcnt
, EDCk_FracFeed = @EDCk_FracFeed
, EDCk_FeedStockMix = @EDCk_FeedStockMix
, EDCk_PyroFurn = @EDCk_PyroFurn
, EDCk_Comp = @EDCk_Comp
, EDCk_SuppTot = @EDCk_SuppTot
, EDCk_Utly = @EDCk_Utly
, EDCk_HydroPur = @EDCk_HydroPur
, EDCk_TowerPyroGasHT = @EDCk_TowerPyroGasHT
, EDCk = @EDCk
, PyroFurnSpare_Pcnt = @PyroFurnSpare_Pcnt
, CGCompPower_BHP = @CGCompPower_BHP
, CGCompStages_Count = @CGCompStages_Count
, CGCompAge_Years = @CGCompAge_Years
, PRCompPower_BHP = @PRCompPower_BHP
, PRCompAge_Years = @PRCompAge_Years
, ERCompPower_BHP = @ERCompPower_BHP
, ERCompAge_Years = @ERCompAge_Years
, MRCompPower_BHP = @MRCompPower_BHP
, MRCompAge_Years = @MRCompAge_Years
, BoilHPPressure_PSIg = @BoilHPPressure_PSIg
, BoilHPRate_kLbHr = @BoilHPRate_kLbHr
, BoilLPPressure_PSIg = @BoilLPPressure_PSIg
, BoilLPRate_kLbHr = @BoilLPRate_kLbHr
, ElecGen_MW = @ElecGen_MW
, NHYTFeedRate_kBsd = @NHYTFeedRate_kBsd
, NHYTFeedProcessed_kMT = @NHYTFeedProcessed_kMT
, NHYTFeedPressure_PSIG = @NHYTFeedPressure_PSIG 
, CompGasHPToRefrigHPRatio = @CompGasHPToRefrigHPRatio
, RV_ISBL = @RV_ISBL
, RVAdj_FeedFlex = @RVAdj_FeedFlex
, RVAdj_FeedPrep = @RVAdj_FeedPrep
, RVAdj_Boilers = @RVAdj_Boilers
, RVAdj_OtherOffSite = @RVAdj_OtherOffSite
, RVAdj_FeedSuppl = @RVAdj_FeedSuppl
, RVAdj_DualTrain = @RVAdj_DualTrain
, RVAdj_Owner = @RVAdj_Owner
, RV_USGC = @RV_USGC
, RV_LocFact = @RV_LocFact
, RV = @RV
, IntegrationFeed_Pcnt = @IntegrationFeed_Pcnt
, IntegrationEthylene_Pcnt = @IntegrationEthylene_Pcnt
, IntegrationPropylene_Pcnt = @IntegrationPropylene_Pcnt
, IntegrationOther_Pcnt = @IntegrationOther_Pcnt
, LogisticsFeed_USDperMT = @LogisticsFeed_USDperMT
, LogisticsEthylene_USDperMT = @LogisticsEthylene_USDperMT
, LogisticsPropylene_USDperMT = @LogisticsPropylene_USDperMT
, EthyleneCarrier = @EthyleneCarrier
, ExtractButdiene = @ExtractButdiene
, ExtractAromatics = @ExtractAromatics
, MTBE = @MTBE
, Isobutylene = @Isobutylene
, IsobutyleneHighPurity = @IsobutyleneHighPurity
, Butene1 = @Butene1
, Isoprene = @Isoprene
, CycloPentadiene = @CycloPentadiene
, OtherC5 = @OtherC5
, PolyPlantShare = @PolyPlantShare
, OlefinsDerivatives = @OlefinsDerivatives
, IntegrationRefinery = @IntegrationRefinery
, IntegrationOther = @IntegrationOther
, IntegrationNone = @IntegrationNone
, Startup_Year = @Startup_Year
, PlantDensity_SG = @PlantDensity_SG
, OSIMProep_Hrs = @OSIMPrep_Hrs

SET NOCOUNT OFF

















