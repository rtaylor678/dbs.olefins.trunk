﻿



CREATE              PROC [dbo].[spReliability](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @TotLostOpp real
SELECT @TotLostOpp = SUM(LostProdOpp) 
FROM dbo.ReliabilityCalc r WHERE r.Refnum=@Refnum and r.DataType = 'TotLossMT'

DELETE FROM dbo.Reliability WHERE Refnum = @Refnum
INSERT into dbo.Reliability (Refnum
, DataType
, TurnAround_Pcnt
, OperError_Pcnt
, IntFeedInterrupt_Pcnt
, FurnaceProcess_Pcnt
, FFP_Pcnt
, Transition_Pcnt
, AcetyleneConv_Pcnt
, ProcessOther_Pcnt
, Compressor_Pcnt
, OtherRotating_Pcnt
, FurnaceMech_Pcnt
, Corrosion_Pcnt
, ElecDist_Pcnt
, ControlSys_Pcnt
, OtherFailure_Pcnt
, ExtElecFailure_Pcnt
, ExtOthUtilFailure_Pcnt
, IntElecFailure_Pcnt
, IntSteamFailure_Pcnt
, IntOthUtilFailure_Pcnt
, UnPlanned_Pcnt
, PlantRelated_Pcnt
, FeedMix_Pcnt
, Severity_Pcnt
, Demand_Pcnt
, ExtFeedInterrupt_Pcnt
, CapProject_Pcnt
, Strikes_Pcnt
, OtherNonOp_Pcnt
, OtherTot_Pcnt
, LostProdOpp_Pcnt
, PcntOfTot
)

SELECT Refnum = @Refnum
, DataType					= CASE r.DataType	WHEN 'DTLossMT' THEN 'DTLoss' 
												WHEN 'SDLossMT' THEN 'SDLoss' 
												ELSE 'TotLoss' END
, TurnAround_Pcnt			= TurnAround/r.EthyleneCpbyAvg_kMT * 100 / 1000
, OperError_Pcnt			= OperError/r.EthyleneCpbyAvg_kMT * 100 / 1000
, IntFeedInterrupt_Pcnt		= IntFeedInterrupt/r.EthyleneCpbyAvg_kMT * 100 / 1000
, FurnaceProcess_Pcnt		= FurnaceProcess/r.EthyleneCpbyAvg_kMT * 100 / 1000
, FFP_Pcnt					= FFP/r.EthyleneCpbyAvg_kMT * 100 / 1000
, Transition_Pcnt			= Transition/r.EthyleneCpbyAvg_kMT * 100 / 1000
, AcetyleneConv_Pcnt		= AcetyleneConv/r.EthyleneCpbyAvg_kMT * 100 / 1000
, ProcessOther_Pcnt			= ProcessOther/r.EthyleneCpbyAvg_kMT * 100 / 1000
, Compressor_Pcnt			= Compressor/r.EthyleneCpbyAvg_kMT * 100 / 1000
, OtherRotating_Pcnt		= OtherRotating/r.EthyleneCpbyAvg_kMT * 100 / 1000
, FurnaceMech_Pcnt			= FurnaceMech/r.EthyleneCpbyAvg_kMT * 100 / 1000
, Corrosion_Pcnt			= Corrosion/r.EthyleneCpbyAvg_kMT * 100 / 1000
, ElecDist_Pcnt				= ElecDist/r.EthyleneCpbyAvg_kMT * 100 / 1000
, ControlSys_Pcnt			= ControlSys/r.EthyleneCpbyAvg_kMT * 100 / 1000
, OtherFailure_Pcnt			= OtherFailure/r.EthyleneCpbyAvg_kMT * 100 / 1000
, ExtElecFailure_Pcnt		= ExtElecFailure/r.EthyleneCpbyAvg_kMT * 100 / 1000
, ExtOthUtilFailure_Pcnt	= ExtOthUtilFailure/r.EthyleneCpbyAvg_kMT * 100 / 1000
, IntElecFailure_Pcnt		= IntElecFailure/r.EthyleneCpbyAvg_kMT * 100 / 1000
, IntSteamFailure_Pcnt		= IntSteamFailure/r.EthyleneCpbyAvg_kMT * 100 / 1000
, IntOthUtilFailure_Pcnt	= IntOthUtilFailure/r.EthyleneCpbyAvg_kMT * 100 / 1000
, UnPlanned_Pcnt			= UnPlanned/r.EthyleneCpbyAvg_kMT * 100 / 1000
, PlantRelated_Pcnt			= PlantRelated/r.EthyleneCpbyAvg_kMT * 100 / 1000
, FeedMix_Pcnt				= FeedMix/r.EthyleneCpbyAvg_kMT * 100 / 1000
, Severity_Pcnt				= Severity/r.EthyleneCpbyAvg_kMT * 100 / 1000
, Demand_Pcnt				= Demand/r.EthyleneCpbyAvg_kMT * 100 / 1000
, ExtFeedInterrupt_Pcnt		= ExtFeedInterrupt/r.EthyleneCpbyAvg_kMT * 100 / 1000
, CapProject_Pcnt			= CapProject/r.EthyleneCpbyAvg_kMT * 100 / 1000
, Strikes_Pcnt				= Strikes/r.EthyleneCpbyAvg_kMT * 100 / 1000
, OtherNonOp_Pcnt			= OtherNonOp/r.EthyleneCpbyAvg_kMT * 100 / 1000
, OtherTot_Pcnt				= OtherTot/r.EthyleneCpbyAvg_kMT * 100 / 1000
, LostProdOpp_Pcnt			= LostProdOpp/r.EthyleneCpbyAvg_kMT * 100 / 1000
, PcntOfTot					= LostProdOpp / @TotLostOpp * 100
FROM dbo.ReliabilityCalc r 
WHERE r.Refnum=@Refnum AND r.DataType in ('DTLossMT','SDLossMT','TotLossMT')
and r.EthyleneCpbyAvg_kMT > 0 AND @TotLostOpp > 0

SET NOCOUNT OFF


















