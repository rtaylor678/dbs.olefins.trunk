﻿
CREATE PROCEDURE [dbo].[Insert_GapProductPurity]
(
	@Refnum			VARCHAR(25),
	@FactorSetId	VARCHAR(12)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [dbo].[GapProductPurity]
		(
			[Refnum],
			[Hydrogen_WtPcnt],
			[Methane_WtPcnt],
			[EthyleneCG_WtPcnt],
			[EthylenePG_WtPcnt],
			[PropyleneCG_WtPcnt],
			[PropylenePG_WtPcnt],
			[PropyleneRG_WtPcnt],
			[PropaneC3Resid_WtPcnt]
		)
		SELECT
			p.[Refnum],
			p.[Hydrogen],
			p.[Methane],
			p.[EthyleneCG],
			p.[EthylenePG],
			p.[PropyleneCG],
			p.[PropylenePG],
			p.[PropyleneRG],
			p.[PropaneC3Resid]
		FROM (
			SELECT
				c.[Refnum],
				c.[StreamId],
				c.[Component_WtPcnt]
			FROM [fact].[CompositionQuantity]	c
			WHERE	c.[Refnum] = @Refnum
				AND	c.[StreamId] IN ('EthyleneCG', 'EthylenePG', 'Hydrogen', 'Methane', 'PropyleneCG', 'PropylenePG', 'PropyleneRG', 'PropaneC3Resid')
			) u
			PIVOT (MAX(u.[Component_WtPcnt]) FOR u.[StreamId] IN
				([EthyleneCG], [EthylenePG], [Hydrogen], [Methane], [PropyleneCG], [PropylenePG], [PropyleneRG], [PropaneC3Resid]
				)
			) p;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
