﻿CREATE VIEW [inter].[PricingPPFCFuelGasEnergy]
WITH SCHEMABINDING
AS
SELECT
	p.[FactorSetId],
	p.[AnnDateKey]	[CalDateKey],
	p.[Refnum],
	'PPFCFuelGas'				[AccountId],
	'PPFCFuelGas'				[StreamId],
	p.[LHValue_MBtu],
	SUM(p.[Quantity_kMT])		[Quantity_kMT],
	p.[LHValue_MBtu] / SUM(p.[Quantity_kMT]) / 2.2046	[LHValue_MBtuLb]
FROM [inter].[PricingPPFCFuelGas] p
GROUP BY
	p.[FactorSetId],
	p.[AnnDateKey],
	p.[Refnum],
	p.[LHValue_MBtu];
