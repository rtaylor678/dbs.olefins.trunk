﻿CREATE TABLE [stgFact].[SimulationParameters_Pyps] (
    [Refnum]              VARCHAR (25)       NOT NULL,
    [AdjButane]           VARCHAR (50)       NULL,
    [ErrorStream]         VARCHAR (14)       NULL,
    [FeedId_Raffinate]    INT                NULL,
    [FeedId_Heavy]        INT                NULL,
    [FeedId_Condensate]   INT                NULL,
    [FeedId_Atmospheric]  INT                NULL,
    [FeedId_Vacuum]       INT                NULL,
    [FeedId_Hydrotreated] INT                NULL,
    [FeedId_OthLiq1]      INT                NULL,
    [FeedId_OthLiq2]      INT                NULL,
    [FeedId_OthLiq3]      INT                NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_SimulationParameters_Pyps_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_SimulationParameters_Pyps_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_SimulationParameters_Pyps_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_SimulationParameters_Pyps_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SimulationParameters_Pyps] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_SimulationParameters_Pyps_u]
	ON [stgFact].[SimulationParameters_Pyps]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[SimulationParameters_Pyps]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[SimulationParameters_Pyps].Refnum		= INSERTED.Refnum;

END;