﻿CREATE TABLE [stgFact].[ProdLoss] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Category]       VARCHAR (3)        NOT NULL,
    [CauseID]        VARCHAR (20)       NOT NULL,
    [Description]    VARCHAR (250)      NULL,
    [PrevDTLoss]     REAL               NULL,
    [PrevSDLoss]     REAL               NULL,
    [PrevTotLoss]    REAL               NULL,
    [DTLoss]         REAL               NULL,
    [SDLoss]         REAL               NULL,
    [TotLoss]        REAL               NULL,
    [AnnDTLoss]      REAL               NULL,
    [AnnSDLoss]      REAL               NULL,
    [AnnTotLoss]     REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_ProdLoss_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdLoss_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdLoss_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdLoss_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_ProdLoss] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Category] ASC, [CauseID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_ProdLoss_u]
	ON [stgFact].[ProdLoss]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[ProdLoss]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[ProdLoss].Refnum		= INSERTED.Refnum
		AND	[stgFact].[ProdLoss].Category	= INSERTED.Category
		AND	[stgFact].[ProdLoss].CauseID		= INSERTED.CauseID;

END;