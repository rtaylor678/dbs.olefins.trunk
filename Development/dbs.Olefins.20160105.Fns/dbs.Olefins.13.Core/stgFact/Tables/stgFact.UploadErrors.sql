﻿CREATE TABLE [stgFact].[UploadErrors] (
    [FileType]           VARCHAR (25)       NOT NULL,
    [Refnum]             VARCHAR (18)       NOT NULL,
    [FilePath]           VARCHAR (512)      NOT NULL,
    [Sheet]              VARCHAR (128)      NULL,
    [Range]              VARCHAR (128)      NULL,
    [CellText]           NVARCHAR (128)     NULL,
    [ProcedureName]      VARCHAR (128)      NULL,
    [TableName]          VARCHAR (128)      NULL,
    [ParameterName]      VARCHAR (128)      NULL,
    [ErrorNotes]         VARCHAR (MAX)      NULL,
    [SystemErrorMessage] VARCHAR (MAX)      NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_UploadErrors_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_stgFact_UploadErrors_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_stgFact_UploadErrors_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_stgFact_UploadErrors_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_UploadErrors] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FileType] ASC, [tsModified] ASC)
);


GO
CREATE TRIGGER [stgFact].[t_UploadErrors_u]
	ON [stgFact].[UploadErrors]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[UploadErrors]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[UploadErrors].[FileType]		= INSERTED.[FileType]
		AND	[stgFact].[UploadErrors].[Refnum]		= INSERTED.[Refnum]
		AND	[stgFact].[UploadErrors].[FilePath]		= INSERTED.[FilePath];

END;