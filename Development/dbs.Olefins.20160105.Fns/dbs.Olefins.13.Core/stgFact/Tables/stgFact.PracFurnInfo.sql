﻿CREATE TABLE [stgFact].[PracFurnInfo] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdID]     VARCHAR (20)       NOT NULL,
    [Run]            REAL               NULL,
    [Inj]            VARCHAR (5)        NULL,
    [Pretreat]       CHAR (1)           NULL,
    [AntiFoul]       CHAR (1)           NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PracFurnInfo_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracFurnInfo_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracFurnInfo_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracFurnInfo_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PracFurnInfo] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PracFurnInfo_u]
	ON [stgFact].[PracFurnInfo]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PracFurnInfo]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PracFurnInfo].Refnum			= INSERTED.Refnum
		AND	[stgFact].[PracFurnInfo].FeedProdID		= INSERTED.FeedProdID;

END;