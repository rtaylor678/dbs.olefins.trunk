﻿CREATE PROCEDURE [stgFact].[Insert_MetaEnergy]
(
	@Refnum		VARCHAR (25),
	@EnergyType	VARCHAR (12),

	@Qtr1			REAL		= NULL,
	@Qtr2			REAL		= NULL,
	@Qtr3			REAL		= NULL,
	@Qtr4			REAL		= NULL,
	@Total			REAL		= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[MetaEnergy]([Refnum], [EnergyType], [Qtr1], [Qtr2], [Qtr3], [Qtr4], [Total])
	VALUES(@Refnum, @EnergyType, @Qtr1, @Qtr2, @Qtr3, @Qtr4, @Total);

END;
