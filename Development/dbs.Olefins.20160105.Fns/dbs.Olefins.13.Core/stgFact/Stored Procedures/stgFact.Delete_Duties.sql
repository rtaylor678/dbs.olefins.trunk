﻿CREATE PROCEDURE [stgFact].[Delete_Duties]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Duties]
	WHERE [Refnum] = @Refnum;

END;