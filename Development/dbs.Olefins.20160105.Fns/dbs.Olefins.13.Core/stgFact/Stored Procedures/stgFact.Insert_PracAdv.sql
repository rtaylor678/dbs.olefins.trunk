﻿CREATE PROCEDURE [stgFact].[Insert_PracAdv]
(
	@Refnum   VARCHAR (25),

	@Tbl9001  REAL     = NULL,
	@Tbl9002  REAL     = NULL,
	@Tbl9003  CHAR (1) = NULL,
	@Tbl9004  REAL     = NULL,
	@Tbl9005  CHAR (1) = NULL,
	@Tbl9006  CHAR (1) = NULL,
	@Tbl9007  CHAR (1) = NULL,
	@Tbl9008  CHAR (1) = NULL,
	@Tbl9009  REAL     = NULL,
	@Tbl9009a REAL     = NULL,
	@Tbl9010  CHAR (1) = NULL,
	@Tbl9011  REAL     = NULL,
	@Tbl9012  REAL     = NULL,
	@Tbl9013  REAL     = NULL,
	@Tbl9014  INT      = NULL,
	@Tbl9015  INT      = NULL,
	@Tbl9016  REAL     = NULL,
	@Tbl9017  REAL     = NULL,
	@Tbl9018  CHAR (1) = NULL,
	@Tbl9019  CHAR (1) = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PracAdv]([Refnum], [Tbl9001], [Tbl9002], [Tbl9003], [Tbl9004], [Tbl9005], [Tbl9006], [Tbl9007], [Tbl9008], [Tbl9009], [Tbl9009a], [Tbl9010], [Tbl9011], [Tbl9012], [Tbl9013], [Tbl9014], [Tbl9015], [Tbl9016], [Tbl9017], [Tbl9018], [Tbl9019])
	VALUES(@Refnum, @Tbl9001, @Tbl9002, @Tbl9003, @Tbl9004, @Tbl9005, @Tbl9006, @Tbl9007, @Tbl9008, @Tbl9009, @Tbl9009a, @Tbl9010, @Tbl9011, @Tbl9012, @Tbl9013, @Tbl9014, @Tbl9015, @Tbl9016, @Tbl9017, @Tbl9018, @Tbl9019);

END;