﻿CREATE PROCEDURE [stgFact].[Delete_OlefinsData]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN
	
	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		EXECUTE [stgFact].[Delete_Capacity]					@Refnum;
		EXECUTE [stgFact].[Delete_CapGrowth]				@Refnum;
		EXECUTE [stgFact].[Delete_CapitalExp]				@Refnum;
		EXECUTE [stgFact].[Delete_Composition]				@Refnum;
		EXECUTE [stgFact].[Delete_Duties]					@Refnum;
		EXECUTE [stgFact].[Delete_EnergyQnty]				@Refnum;
		EXECUTE [stgFact].[Delete_Environ]					@Refnum;
		EXECUTE [stgFact].[Delete_Facilities]				@Refnum;
		EXECUTE [stgFact].[Delete_FeedFlex]					@Refnum;
		EXECUTE [stgFact].[Delete_FeedPlan]					@Refnum;
		EXECUTE [stgFact].[Delete_FeedQuality]				@Refnum;
		EXECUTE [stgFact].[Delete_FeedResources]			@Refnum;
		EXECUTE [stgFact].[Delete_FeedSelections]			@Refnum;
		EXECUTE [stgFact].[Delete_FurnRely]					@Refnum;
		EXECUTE [stgFact].[Delete_Inventory]				@Refnum;
		EXECUTE [stgFact].[Delete_Maint]					@Refnum;
		EXECUTE [stgFact].[Delete_Maint01]					@Refnum;
		EXECUTE [stgFact].[Delete_MaintMisc]				@Refnum;
		EXECUTE [stgFact].[Delete_MetaEnergy]				@Refnum;
		EXECUTE [stgFact].[Delete_MetaFeedProd]				@Refnum;
		EXECUTE [stgFact].[Delete_MetaMisc]					@Refnum;
		EXECUTE [stgFact].[Delete_MetaOpEx]					@Refnum;
		EXECUTE [stgFact].[Delete_Misc]						@Refnum;
		EXECUTE [stgFact].[Delete_OpEx]						@Refnum;
		EXECUTE [stgFact].[Delete_OpExMiscDetail]			@Refnum;
		EXECUTE [stgFact].[Delete_Pers]						@Refnum;
		EXECUTE [stgFact].[Delete_PolyFacilities]			@Refnum;
		EXECUTE [stgFact].[Delete_PolyMatlBalance]			@Refnum;
		EXECUTE [stgFact].[Delete_PolyOpEx]					@Refnum;
		EXECUTE [stgFact].[Delete_Prac]						@Refnum;
		EXECUTE [stgFact].[Delete_PracAdv]					@Refnum;
		EXECUTE [stgFact].[Delete_PracControls]				@Refnum;
		EXECUTE [stgFact].[Delete_PracFurnInfo]				@Refnum;
		EXECUTE [stgFact].[Delete_PracMPC]					@Refnum;
		EXECUTE [stgFact].[Delete_PracOnlineFact]			@Refnum;
		EXECUTE [stgFact].[Delete_PracOrg]					@Refnum;
		EXECUTE [stgFact].[Delete_PracTA]					@Refnum;
		EXECUTE [stgFact].[Delete_ProdLoss]					@Refnum;
		EXECUTE [stgFact].[Delete_ProdLoss_Availability]	@Refnum;
		EXECUTE [stgFact].[Delete_ProdQuality]				@Refnum;
		EXECUTE [stgFact].[Delete_Quantity]					@Refnum;
		EXECUTE [stgFact].[Delete_TADT]						@Refnum;
		EXECUTE [stgFact].[Delete_TotMaintCost]				@Refnum;
		EXECUTE [stgFact].[Delete_TSort]					@Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;