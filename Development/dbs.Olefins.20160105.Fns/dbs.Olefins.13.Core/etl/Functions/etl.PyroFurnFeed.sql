﻿
CREATE FUNCTION etl.PyroFurnFeed(
	@StreamId			VARCHAR(42)
	)
RETURNS VARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	RETURN CASE @StreamId
	WHEN 'Butane'	THEN 'Naphtha'
	WHEN 'Light'	THEN 'Propane'
	WHEN 'EPMix'	THEN 'Ethane'
	WHEN 'LPG'		THEN 'Ethane'
	WHEN 'Liquid'	THEN 'Naphtha'
	WHEN 'HTGasoil'	THEN 'LiqHeavy'
	WHEN 'EpLT390'	THEN 'LiqHeavy'
	WHEN 'EpGT390'	THEN 'LiqHeavy'
	WHEN 'Diesel'	THEN 'LiqHeavy'
	ELSE @StreamId
	END
	
END;