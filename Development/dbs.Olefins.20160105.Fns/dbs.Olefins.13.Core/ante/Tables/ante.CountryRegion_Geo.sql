﻿CREATE TABLE [ante].[CountryRegion_Geo] (
    [CountryId]      CHAR (3)           NOT NULL,
    [RegionId]       VARCHAR (5)        NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_CountryRegion_Geo_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_CountryRegion_Geo_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_CountryRegion_Geo_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_CountryRegion_Geo_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_CountryRegion_Geo] PRIMARY KEY CLUSTERED ([CountryId] ASC),
    CONSTRAINT [FK_CountryRegion_Geo_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_CountryRegion_Geo_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId])
);


GO

CREATE TRIGGER [ante].[t_CountryRegion_Geo_u]
	ON [ante].[CountryRegion_Geo]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[CountryRegion_Geo]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[CountryRegion_Geo].[CountryId]		= INSERTED.[CountryId];

END;
