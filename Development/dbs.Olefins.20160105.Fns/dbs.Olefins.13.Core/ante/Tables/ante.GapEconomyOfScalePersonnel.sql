﻿CREATE TABLE [ante].[GapEconomyOfScalePersonnel] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [PersId]         VARCHAR (34)       NOT NULL,
    [Intercept]      REAL               NOT NULL,
    [Exponent]       REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_GapEconomyOfScalePersonnel_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_GapEconomyOfScalePersonnel_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_GapEconomyOfScalePersonnel_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_GapEconomyOfScalePersonnel_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GapEconomyOfScalePersonnel] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PersId] ASC),
    CONSTRAINT [FK_GapEconomyOfScalePersonnel_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_GapEconomyOfScalePersonnel_u]
	ON [ante].[GapEconomyOfScalePersonnel]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[GapEconomyOfScalePersonnel]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[GapEconomyOfScalePersonnel].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[GapEconomyOfScalePersonnel].[PersId]		= INSERTED.[PersId];

END;
