﻿CREATE TABLE [sim].[QueueComplete]
(
	[QueueId]			INT					NOT	NULL	CONSTRAINT [FK_QueueComplete_QueueAdd]			REFERENCES [sim].[QueueAdd]([QueueId]),

	[Complete]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_QueueComplete_Complete]			DEFAULT(SYSDATETIMEOFFSET()),
	[ItemsProcessed]	INT					NOT	NULL,
	[ErrorCount]		INT					NOT	NULL,

	[tsModified]		DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_QueueComplete_tsModified]		DEFAULT (SYSDATETIMEOFFSET()),
	[tsModifiedHost]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueComplete_tsModifiedHost]	DEFAULT (HOST_NAME()),
	[tsModifiedUser]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueComplete_tsModifiedUser]	DEFAULT (SUSER_SNAME()),
	[tsModifiedApp]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueComplete_tsModifiedApp]		DEFAULT (APP_NAME()),

	CONSTRAINT [PK_QueueComplete] PRIMARY KEY CLUSTERED([QueueId] ASC)
);