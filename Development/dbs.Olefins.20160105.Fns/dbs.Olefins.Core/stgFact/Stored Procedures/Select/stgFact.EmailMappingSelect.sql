﻿CREATE PROCEDURE [stgFact].[EmailMappingSelect]
	
	@StudyYear int,
	@WorkbookName NVARCHAR(50)

AS
	SELECT [ExcelMappingId]
      ,[StudyYear]
      ,[WorkbookName]
      ,[WorksheetName]
      ,[Active]
  FROM [stgFact].[ExcelMapping]
  WHERE 
		[ExcelMapping].StudyYear = @StudyYear
		AND [ExcelMapping].WorkbookName = @WorkbookName

RETURN 0
