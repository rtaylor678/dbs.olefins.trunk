﻿CREATE PROCEDURE [stgFact].[EmailMappingSelectByStudyYear]
	
	@StudyYear int,
	@Active bit


AS
	SELECT [ExcelMappingId]
      ,[StudyYear]
      ,[WorkbookName]
      ,[WorksheetName]
	  ,[DataStartCol]
	  ,[DataEndCol]
	  ,[DataStartRow]
	  ,[DataEndRow]
	  ,[TableRefCol]
	  ,[ColFldAndValue]
	  ,[ReadType]
      ,[Active]
  FROM [stgFact].[ExcelMapping]
  WHERE 
		[ExcelMapping].StudyYear = @StudyYear
		AND [ExcelMapping].Active = @Active



RETURN 0

GO
