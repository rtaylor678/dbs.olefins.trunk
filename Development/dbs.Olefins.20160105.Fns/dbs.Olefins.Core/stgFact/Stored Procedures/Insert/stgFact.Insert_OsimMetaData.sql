﻿CREATE PROCEDURE [stgFact].[Insert_OsimMetaData]
(
	@Refnum				VARCHAR(25),

	@VersionOS			VARCHAR(128)	= NULL,
	@VersionExcel		VARCHAR(128)	= NULL,
	@VersionVbe			VARCHAR(128)	= NULL,
	@LocaleId			INT				= NULL,

	@DateTimeOsim		SMALLDATETIME	= NULL,
	@DateTimeTemplate	SMALLDATETIME	= NULL,

	@RefnumCurrent		VARCHAR(25),
	@RefnumHistory		VARCHAR(25)		= NULL,

	@StudyYearCurrent	SMALLINT,
	@StudyYearHistory	SMALLINT		= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[OsimMetaData]
	(
		[Refnum],
		[VersionOS],
		[VersionExcel],
		[VersionVbe],
		[LocaleId],
		[DateTimeOsim],
		[DateTimeTemplate],
		[RefnumCurrent],
		[RefnumHistory],
		[StudyYearCurrent],
		[StudyYearHistory]
	)
	VALUES
	(
		@Refnum,
		@VersionOS,
		@VersionExcel,
		@VersionVbe,
		@LocaleId,
		@DateTimeOsim,
		@DateTimeTemplate,
		@RefnumCurrent,
		@RefnumHistory,
		@StudyYearCurrent,
		@StudyYearHistory
	);

END;