﻿CREATE PROCEDURE [stgFact].[Insert_OpExMiscDetail]
(
	@Refnum      VARCHAR (25),
	@OpExId      VARCHAR (6),

	@Description VARCHAR (100) = NULL,
	@Amount      REAL          = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO [stgFact].[OpExMiscDetail]([Refnum], [OpExId], [Description], [Amount])
	VALUES(@Refnum, @OpExId, @Description, @Amount);

END;