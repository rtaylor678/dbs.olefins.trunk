﻿CREATE PROCEDURE [stgFact].[Insert_TotMainCost]
(
	@Refnum            VARCHAR (25),

	@DirMaintMatl      REAL     = NULL,
	@DirMaintLabor     REAL     = NULL,
	@DirTAMatl         REAL     = NULL,
	@DirTALabor        REAL     = NULL,
	@AllocOHMaintMatl  REAL     = NULL,
	@AllocOHMaintLabor REAL     = NULL,
	@AllocOHTAMatl     REAL     = NULL,
	@AllocOHTALabor    REAL     = NULL,
	@TotalMaintMatl    REAL     = NULL,
	@TotalMaintLabor   REAL     = NULL,
	@TotalTAMatl       REAL     = NULL,
	@TotalTALabor      REAL     = NULL,
	@DirMaintTot       REAL     = NULL,
	@DirTATot          REAL     = NULL,
	@AllocOHMaintTot   REAL     = NULL,
	@AllocOHTATot      REAL     = NULL,
	@TotRoutMaint      REAL     = NULL,
	@TotTAMaint        REAL     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[TotMaintCost]([Refnum], [DirMaintMatl], [DirMaintLabor], [DirTAMatl], [DirTALabor], [AllocOHMaintMatl], [AllocOHMaintLabor], [AllocOHTAMatl], [AllocOHTALabor], [TotalMaintMatl], [TotalMaintLabor], [TotalTAMatl], [TotalTALabor], [DirMaintTot], [DirTATot], [AllocOHMaintTot], [AllocOHTATot], [TotRoutMaint], [TotTAMaint])
	VALUES(@Refnum, @DirMaintMatl, @DirMaintLabor, @DirTAMatl, @DirTALabor, @AllocOHMaintMatl, @AllocOHMaintLabor, @AllocOHTAMatl, @AllocOHTALabor, @TotalMaintMatl, @TotalMaintLabor, @TotalTAMatl, @TotalTALabor, @DirMaintTot, @DirTATot, @AllocOHMaintTot, @AllocOHTATot, @TotRoutMaint, @TotTAMaint);

END;
