﻿CREATE PROCEDURE [stgFact].[Insert_FacilitiesCoolingWater]
(
	@Refnum					VARCHAR (25),

	@FanHorsepower_bhp		REAL			= NULL,
	@PumpHorsepower_bhp		REAL			= NULL,
	@FlowRate_TonneDay		REAL			= NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FacilitiesCoolingWater]
	(
		[Refnum],
		[FanHorsepower_bhp],
		[PumpHorsepower_bhp],
		[FlowRate_TonneDay]
	)
	VALUES
	(
		@Refnum,
		@FanHorsepower_bhp,
		@PumpHorsepower_bhp,
		@FlowRate_TonneDay
	);

END;