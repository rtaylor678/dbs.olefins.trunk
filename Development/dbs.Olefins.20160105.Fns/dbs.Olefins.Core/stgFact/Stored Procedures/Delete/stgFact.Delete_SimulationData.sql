﻿CREATE PROCEDURE [stgFact].[Delete_SimulationData]
(
	@Refnum					VARCHAR (25),
	@SimModelId				VARCHAR (12)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DELETE FROM [stgFact].[YieldCompositionStream]
		WHERE	[Refnum]		= @Refnum
			AND [SimModelId]	= @SimModelId;

		DELETE FROM [stgFact].[EnergyConsumptionStream]
		WHERE	[Refnum]		= @Refnum
			AND [SimModelId]	= @SimModelId;

		DELETE FROM [stgFact].[YieldCompositionPlant]
		WHERE	[Refnum]		= @Refnum
			AND [SimModelId]	= @SimModelId;

		DELETE FROM [stgFact].[EnergyConsumptionPlant]
		WHERE	[Refnum]		= @Refnum
			AND [SimModelId]	= @SimModelId;

		DELETE FROM [stgFact].[SimulationParameters_Pyps]
		WHERE	[Refnum]		= @Refnum
			AND	@SimModelId		= 'PYPS';

		DELETE FROM [stgFact].[SimulationSeverity_Pyps]
		WHERE	[Refnum]		= @Refnum
			AND	@SimModelId		= 'PYPS';

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
