﻿CREATE PROCEDURE [stgFact].[ExcelMappingDeleteById]
	
	@ExcelMappingId  int
	
AS
	UPDATE [stgFact].ExcelMapping
	SET Active = 0
	WHERE ExcelMapping.ExcelMappingId = @ExcelMappingId

RETURN 0