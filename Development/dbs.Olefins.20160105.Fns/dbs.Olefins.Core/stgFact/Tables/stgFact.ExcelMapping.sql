﻿CREATE TABLE [stgFact].[ExcelMapping]
(
	[ExcelMappingId] INT IDENTITY(1,1) NOT NULL, 
    [StudyYear] INT NOT NULL, 
    [WorkbookName] NVARCHAR(50) NOT NULL, 
    [WorksheetName] NVARCHAR(32) NOT NULL, 
	[DataStartCol] NVARCHAR(2) NOT NULL, 
	[DataEndCol] NVARCHAR(2) NOT NULL,
	[DataStartRow] INT NOT NULL,
	[DataEndRow] INT NOT NULL,
	[TableRefCol] NVARCHAR(2) NOT NULL,
	[ColFldAndValue] NVARCHAR(2) NOT NULL,
	[ReadType]  NVARCHAR(15) NOT NULL,
    [Active] BIT NOT NULL DEFAULT 1

CONSTRAINT [PK_stgFact_ExcelMapping] PRIMARY KEY CLUSTERED 
(
	[ExcelMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]