﻿CREATE TABLE [stgFact].[FeedQuality] (
    [Refnum]               VARCHAR (25)       NOT NULL,
    [FeedProdId]           VARCHAR (20)       NOT NULL,
    [Methane]              REAL               NULL,
    [Ethane]               REAL               NULL,
    [Ethylene]             REAL               NULL,
    [Propane]              REAL               NULL,
    [Propylene]            REAL               NULL,
    [nButane]              REAL               NULL,
    [iButane]              REAL               NULL,
    [Isobutylene]          REAL               NULL,
    [Butene1]              REAL               NULL,
    [Butadiene]            REAL               NULL,
    [nPentane]             REAL               NULL,
    [iPentane]             REAL               NULL,
    [nHexane]              REAL               NULL,
    [iHexane]              REAL               NULL,
    [Septane]              REAL               NULL,
    [Octane]               REAL               NULL,
    [CO2]                  REAL               NULL,
    [Hydrogen]             REAL               NULL,
    [Sulfur]               REAL               NULL,
    [CoilOutletPressure]   REAL               NULL,
    [SteamHCRatio]         REAL               NULL,
    [CoilOutletTemp]       REAL               NULL,
    [EthyleneYield]        REAL               NULL,
    [PropylEthylRatio]     REAL               NULL,
    [FeedConv]             REAL               NULL,
    [PropylMethaneRatio]   REAL               NULL,
    [ConstrYear]           INT                NULL,
    [ResTime]              REAL               NULL,
    [SulfurPPM]            REAL               NULL,
    [ASTMethod]            VARCHAR (10)       NULL,
    [IBP]                  REAL               NULL,
    [D5pcnt]               REAL               NULL,
    [D10Pcnt]              REAL               NULL,
    [D30Pcnt]              REAL               NULL,
    [D50Pcnt]              REAL               NULL,
    [D70Pcnt]              REAL               NULL,
    [D90Pcnt]              REAL               NULL,
    [D95Pcnt]              REAL               NULL,
    [EP]                   REAL               NULL,
    [NParaffins]           REAL               NULL,
    [IsoParaffins]         REAL               NULL,
    [Naphthenes]           REAL               NULL,
    [Olefins]              REAL               NULL,
    [Aromatics]            REAL               NULL,
    [SG]                   REAL               NULL,
    [OthLiqFeedDESC]       VARCHAR (50)       NULL,
    [OthLiqFeedPriceBasis] REAL               NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_FeedQuality_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedQuality_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedQuality_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedQuality_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_FeedQuality] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_FeedQuality_u]
	ON [stgFact].[FeedQuality]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[FeedQuality]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[FeedQuality].Refnum		= INSERTED.Refnum
		AND	[stgFact].[FeedQuality].FeedProdId	= INSERTED.FeedProdId;

END;