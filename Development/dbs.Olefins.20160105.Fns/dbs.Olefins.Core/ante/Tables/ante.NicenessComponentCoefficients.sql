﻿CREATE TABLE [ante].[NicenessComponentCoefficients] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [Coefficient]    REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessComponentCoefficients_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_NicenessComponentCoefficients_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_NicenessComponentCoefficients_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_NicenessComponentCoefficients_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_NicenessComponentCoefficients] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
    CONSTRAINT [FK_NicenessComponentCoefficients_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_NicenessComponentCoefficients_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_NicenessComponentCoefficients_u]
	ON [ante].[NicenessComponentCoefficients]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessComponentCoefficients]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[NicenessComponentCoefficients].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[NicenessComponentCoefficients].[ComponentId]	= INSERTED.[ComponentId];

END;
