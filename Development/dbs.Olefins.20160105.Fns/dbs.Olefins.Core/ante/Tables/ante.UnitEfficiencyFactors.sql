﻿CREATE TABLE [ante].[UnitEfficiencyFactors] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [UnitId]         VARCHAR (42)       NOT NULL,
    [EfficiencyId]   VARCHAR (42)       NOT NULL,
    [Coefficient]    REAL               NOT NULL,
    [Exponent]       REAL               NOT NULL,
    [ValueMin]       REAL               NOT NULL,
    [ValueMax]       REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_UnitEfficiencyFactors_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_UnitEfficiencyFactors_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_UnitEfficiencyFactors_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_UnitEfficiencyFactorss_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_UnitEfficiencyFactors] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [UnitId] ASC, [EfficiencyId] ASC),
    CONSTRAINT [CK_UnitEfficiencyFactors_ValueMin_ValueMax] CHECK ([ValueMin]<=[ValueMax]),
    CONSTRAINT [FK_UnitEfficiencyFactors_Efficiency_LookUp] FOREIGN KEY ([EfficiencyId]) REFERENCES [dim].[Efficiency_LookUp] ([EfficiencyId]),
    CONSTRAINT [FK_UnitEfficiencyFactors_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_UnitEfficiencyFactors_Unit_LookUp] FOREIGN KEY ([UnitId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId])
);


GO

CREATE TRIGGER [ante].[t_UnitEfficiencyFactors_u]
	ON [ante].[UnitEfficiencyFactors]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[UnitEfficiencyFactors]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[UnitEfficiencyFactors].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[UnitEfficiencyFactors].[UnitId]			= INSERTED.[UnitId]
		AND	[ante].[UnitEfficiencyFactors].[EfficiencyId]	= INSERTED.[EfficiencyId];

END;
