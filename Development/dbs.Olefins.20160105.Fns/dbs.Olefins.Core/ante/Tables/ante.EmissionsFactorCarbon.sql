﻿CREATE TABLE [ante].[EmissionsFactorCarbon] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [EmissionsId]    VARCHAR (42)       NOT NULL,
    [CEF_MTCO2_MBtu] REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsFactorCarbon_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorCarbon_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorCarbon_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorCarbon_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EmissionsFactorCarbon] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EmissionsId] ASC),
    CONSTRAINT [FK_EmissionsFactorCarbon_Emissions_LookUp] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsFactorCarbon_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_EmissionsFactorCarbon_u]
	ON [ante].[EmissionsFactorCarbon]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EmissionsFactorCarbon]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EmissionsFactorCarbon].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[EmissionsFactorCarbon].EmissionsId	= INSERTED.EmissionsId;

END;