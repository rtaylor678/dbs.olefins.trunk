﻿


Create   PROC reports.[spProcessBatch](@Batch varchar(10))
AS
DECLARE @GroupId varchar(25)
DECLARE creports CURSOR LOCAL FAST_FORWARD
FOR	SELECT GroupId FROM reports.ReportGroups WHERE Batch = @Batch
OPEN creports
FETCH NEXT FROM creports INTO @GroupId
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC reports.spALL @GroupId
	FETCH NEXT FROM creports INTO @GroupId
END
CLOSE creports
DEALLOCATE creports
