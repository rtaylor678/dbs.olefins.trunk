﻿


CREATE              PROC [reports].[spPersonnel](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.Personnel WHERE GroupId = @GroupId
Insert into reports.Personnel (GroupId
, DataType
, DivisorValue
, OccProcProc
, OccProcUtil
, OccProcOffSite
, OccProcTruckRail
, OccProcMarine
, OccProcRelief
, OccProcTraining
, OccProcOther
, OccProc
, OccMaintRoutine
, OccMaintTaAnnRetube
, OccMaintTaMaint
, OccMaintInsp
, OccMaintShop
, OccMaintPlan
, OccMaintWarehouse
, OccMaintTraining
, OccMaint
, OccAdminLab
, OccAdminAcct
, OccAdminPurchase
, OccAdminSecurity
, OccAdminClerical
, OccAdminOther
, OccAdmin
, OccPlantSite
, MpsProc
, MpsMaintRoutine
, MpsMaintTaAnnRetube
, MpsMaintTaMaint
, MpsMaintShop
, MpsMaintPlan
, MpsMaintWarehouse
, MpsMaint
, MpsAdminLab
, MpsTechProc
, MpsTechEcon
, MpsTechRely
, MpsTechInsp
, MpsTechAPC
, MpsTechEnviron
, MpsTechOther
, MpsTech
, MpsAdminAcct
, MpsAdminMIS
, MpsAdminPurchase
, MpsAdminSecurity
, MpsAdminOther
, MpsAdmin
, MpsPlantSite
, MpsAdminGenPurchase
, MpsAdminGenOther
, MpsSubTotal
, PlantTotal
, OCCAbsence_Pcnt
, MpsAbsence_Pcnt
, MaintRoutine
, MaintTaAnnRetube
, MaintTaMaint
, MaintInsp
, MaintShop
, MaintPlan
, MaintWarehouse
, MaintTraining
, Maint
, NonMaint
, nmOCCPlantSite
, nmMpsTech
, nmMpsSubTotal
, nmMpsPlantSite
)

SELECT GroupId = @GroupId
, DataType					= p.DataType
, DivisorValue				= [$(DbGlobal)].dbo.WtAvg(p.DivisorValue,1)
, OccProcProc = [$(DbGlobal)].dbo.WtAvg(OccProcProc, p.DivisorValue)
, OccProcUtil = [$(DbGlobal)].dbo.WtAvg(OccProcUtil, p.DivisorValue)
, OccProcOffSite = [$(DbGlobal)].dbo.WtAvg(OccProcOffSite, p.DivisorValue)
, OccProcTruckRail = [$(DbGlobal)].dbo.WtAvg(OccProcTruckRail, p.DivisorValue)
, OccProcMarine = [$(DbGlobal)].dbo.WtAvg(OccProcMarine, p.DivisorValue)
, OccProcRelief = [$(DbGlobal)].dbo.WtAvg(OccProcRelief, p.DivisorValue)
, OccProcTraining = [$(DbGlobal)].dbo.WtAvg(OccProcTraining, p.DivisorValue)
, OccProcOther = [$(DbGlobal)].dbo.WtAvg(OccProcOther, p.DivisorValue)
, OccProc = [$(DbGlobal)].dbo.WtAvg(OccProc, p.DivisorValue)
, OccMaintRoutine = [$(DbGlobal)].dbo.WtAvg(OccMaintRoutine, p.DivisorValue)
, OccMaintTaAnnRetube = [$(DbGlobal)].dbo.WtAvg(OccMaintTaAnnRetube, p.DivisorValue)
, OccMaintTaMaint = [$(DbGlobal)].dbo.WtAvg(OccMaintTaMaint, p.DivisorValue)
, OccMaintInsp = [$(DbGlobal)].dbo.WtAvg(OccMaintInsp, p.DivisorValue)
, OccMaintShop = [$(DbGlobal)].dbo.WtAvg(OccMaintShop, p.DivisorValue)
, OccMaintPlan = [$(DbGlobal)].dbo.WtAvg(OccMaintPlan, p.DivisorValue)
, OccMaintWarehouse = [$(DbGlobal)].dbo.WtAvg(OccMaintWarehouse, p.DivisorValue)
, OccMaintTraining = [$(DbGlobal)].dbo.WtAvg(OccMaintTraining, p.DivisorValue)
, OccMaint = [$(DbGlobal)].dbo.WtAvg(OccMaint, p.DivisorValue)
, OccAdminLab = [$(DbGlobal)].dbo.WtAvg(OccAdminLab, p.DivisorValue)
, OccAdminAcct = [$(DbGlobal)].dbo.WtAvg(OccAdminAcct, p.DivisorValue)
, OccAdminPurchase = [$(DbGlobal)].dbo.WtAvg(OccAdminPurchase, p.DivisorValue)
, OccAdminSecurity = [$(DbGlobal)].dbo.WtAvg(OccAdminSecurity, p.DivisorValue)
, OccAdminClerical = [$(DbGlobal)].dbo.WtAvg(OccAdminClerical, p.DivisorValue)
, OccAdminOther = [$(DbGlobal)].dbo.WtAvg(OccAdminOther, p.DivisorValue)
, OccAdmin = [$(DbGlobal)].dbo.WtAvg(OccAdmin, p.DivisorValue)
, OccPlantSite = [$(DbGlobal)].dbo.WtAvg(OccPlantSite, p.DivisorValue)
, MpsProc = [$(DbGlobal)].dbo.WtAvg(MpsProc, p.DivisorValue)
, MpsMaintRoutine = [$(DbGlobal)].dbo.WtAvg(MpsMaintRoutine, p.DivisorValue)
, MpsMaintTaAnnRetube = [$(DbGlobal)].dbo.WtAvg(MpsMaintTaAnnRetube, p.DivisorValue)
, MpsMaintTaMaint = [$(DbGlobal)].dbo.WtAvg(MpsMaintTaMaint, p.DivisorValue)
, MpsMaintShop = [$(DbGlobal)].dbo.WtAvg(MpsMaintShop, p.DivisorValue)
, MpsMaintPlan = [$(DbGlobal)].dbo.WtAvg(MpsMaintPlan, p.DivisorValue)
, MpsMaintWarehouse = [$(DbGlobal)].dbo.WtAvg(MpsMaintWarehouse, p.DivisorValue)
, MpsMaint = [$(DbGlobal)].dbo.WtAvg(MpsMaint, p.DivisorValue)
, MpsAdminLab = [$(DbGlobal)].dbo.WtAvg(MpsAdminLab, p.DivisorValue)
, MpsTechProc = [$(DbGlobal)].dbo.WtAvg(MpsTechProc, p.DivisorValue)
, MpsTechEcon = [$(DbGlobal)].dbo.WtAvg(MpsTechEcon, p.DivisorValue)
, MpsTechRely = [$(DbGlobal)].dbo.WtAvg(MpsTechRely, p.DivisorValue)
, MpsTechInsp = [$(DbGlobal)].dbo.WtAvg(MpsTechInsp, p.DivisorValue)
, MpsTechAPC = [$(DbGlobal)].dbo.WtAvg(MpsTechAPC, p.DivisorValue)
, MpsTechEnviron = [$(DbGlobal)].dbo.WtAvg(MpsTechEnviron, p.DivisorValue)
, MpsTechOther = [$(DbGlobal)].dbo.WtAvg(MpsTechOther, p.DivisorValue)
, MpsTech = [$(DbGlobal)].dbo.WtAvg(MpsTech, p.DivisorValue)
, MpsAdminAcct = [$(DbGlobal)].dbo.WtAvg(MpsAdminAcct, p.DivisorValue)
, MpsAdminMIS = [$(DbGlobal)].dbo.WtAvg(MpsAdminMIS, p.DivisorValue)
, MpsAdminPurchase = [$(DbGlobal)].dbo.WtAvg(MpsAdminPurchase, p.DivisorValue)
, MpsAdminSecurity = [$(DbGlobal)].dbo.WtAvg(MpsAdminSecurity, p.DivisorValue)
, MpsAdminOther = [$(DbGlobal)].dbo.WtAvg(MpsAdminOther, p.DivisorValue)
, MpsAdmin = [$(DbGlobal)].dbo.WtAvg(MpsAdmin, p.DivisorValue)
, MpsPlantSite = [$(DbGlobal)].dbo.WtAvg(MpsPlantSite, p.DivisorValue)
, MpsAdminGenPurchase = [$(DbGlobal)].dbo.WtAvg(MpsAdminGenPurchase, p.DivisorValue)
, MpsAdminGenOther = [$(DbGlobal)].dbo.WtAvg(MpsAdminGenOther, p.DivisorValue)
, MpsSubTotal = [$(DbGlobal)].dbo.WtAvg(MpsSubTotal, p.DivisorValue)
, PlantTotal = [$(DbGlobal)].dbo.WtAvg(PlantTotal, p.DivisorValue)
, OCCAbsence_Pcnt = [$(DbGlobal)].dbo.WtAvg(OCCAbsence_Pcnt, p.PlantTotal)
, MpsAbsence_Pcnt = [$(DbGlobal)].dbo.WtAvg(MpsAbsence_Pcnt, p.PlantTotal)
, MaintRoutine = [$(DbGlobal)].dbo.WtAvg(MaintRoutine, p.DivisorValue)
, MaintTaAnnRetube = [$(DbGlobal)].dbo.WtAvg(MaintTaAnnRetube, p.DivisorValue)
, MaintTaMaint = [$(DbGlobal)].dbo.WtAvg(MaintTaMaint, p.DivisorValue)
, MaintInsp = [$(DbGlobal)].dbo.WtAvg(MaintInsp, p.DivisorValue)
, MaintShop = [$(DbGlobal)].dbo.WtAvg(MaintShop, p.DivisorValue)
, MaintPlan = [$(DbGlobal)].dbo.WtAvg(MaintPlan, p.DivisorValue)
, MaintWarehouse = [$(DbGlobal)].dbo.WtAvg(MaintWarehouse, p.DivisorValue)
, MaintTraining = [$(DbGlobal)].dbo.WtAvg(MaintTraining, p.DivisorValue)
, Maint = [$(DbGlobal)].dbo.WtAvg(Maint, p.DivisorValue)
, NonMaint = [$(DbGlobal)].dbo.WtAvg(_NonMaint, p.DivisorValue)
, nmOCCPlantSite = [$(DbGlobal)].dbo.WtAvg(_nmOCCPlantSite, p.DivisorValue)
, nmMpsTech = [$(DbGlobal)].dbo.WtAvg(_nmMpsTech, p.DivisorValue)
, nmMpsSubTotal = [$(DbGlobal)].dbo.WtAvg(_nmMpsSubTotal, p.DivisorValue)
, nmMpsPlantSite = [$(DbGlobal)].dbo.WtAvg(_nmMpsPlantSite, p.DivisorValue)
FROM @MyGroup r JOIN dbo.Personnel p on p.Refnum=r.Refnum 
GROUP BY p.DataType

SET NOCOUNT OFF

















