﻿CREATE PROCEDURE [Console].[GetIssues]
      @RefNum varchar(18),
      @Completed nvarchar(1)
AS
BEGIN

      SELECT * FROM Val.Checklist
      WHERE Refnum = dbo.FormatRefNum('20' + @RefNum,0) 
      AND (@Completed = 'A' OR Completed = @Completed)

END
