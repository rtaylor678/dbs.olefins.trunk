﻿CREATE PROCEDURE [Console].[InsertValidationNotes]

	@RefNum varchar(18),
	@Notes text

AS
BEGIN

	INSERT INTO Val.Notes (Refnum, ValidationNotes) VALUES (@RefNum, @Notes)

END
