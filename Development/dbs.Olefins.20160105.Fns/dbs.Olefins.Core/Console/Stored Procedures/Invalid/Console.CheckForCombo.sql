﻿CREATE PROC [Console].[CheckForCombo]
	@RefNum varchar(18)
	
AS
	
	SELECT cb1.IndivRefnum FROM ComboRefs cb1 
	JOIN ComboRefs cb2 on cb1.ComboRefnum = cb2.ComboRefnum 
	WHERE cb2.IndivRefnum IN (SELECT SMALLREFNUM FROM TSort WHERE SMALLREFNUM = @RefNum) 
	and 
	cb1.IndivRefnum NOT IN (SELECT SMALLREFNUM FROM TSort WHERE SMALLREFNUM = @RefNum)
