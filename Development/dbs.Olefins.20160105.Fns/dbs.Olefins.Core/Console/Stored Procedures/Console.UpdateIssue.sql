﻿CREATE PROCEDURE [Console].[UpdateIssue] 

	@Refnum varchar(18),
	@IssueTitle nvarchar(20),
	@Status nvarchar(1),
	@UpdatedBy nvarchar(3)

AS
BEGIN
DECLARE @UpdateDate datetime
SET @UpdateDate = GETDATE()

IF @Status = 'N'
	BEGIN
		SET @UpdatedBy = null
		SET @UpdateDate = null
	END
UPDATE Val.Checklist
SET Completed = @Status, SetTime = @UpdateDate,SetBy = @UpdatedBy
Where Refnum = dbo.FormatRefNum('20' + @Refnum,0) AND IssueId = @IssueTitle
END
