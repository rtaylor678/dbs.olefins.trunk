﻿CREATE PROCEDURE [Console].[GetContactInfo]
	@Refnum varchar(18)
AS
BEGIN
	SELECT	[ContactTypeId]
      ,[NameFull]
      ,[NameTitle]
      ,[AddressPOBox]
      ,[AddressStreet]
      ,[AddressCity]
      ,[AddressState]
      ,[AddressCountry]
      ,[AddressCountryId]
      ,[AddressPostalCode]
      ,[NumberVoice]
      ,[NumberFax]
      ,[NumberMobile]
      ,[eMail]
			
			 
	FROM fact.TSortContact
	WHERE Refnum = '20' + @Refnum OR Refnum = dbo.FormatRefNum('20' + @Refnum, 0)
END
