﻿CREATE PROCEDURE [Console].[GetValidationFiles] 
	@RefNum varchar(18)
AS
BEGIN
	SELECT DISTINCT vf.* 
	FROM ValidationFiles vf INNER JOIN TSort t ON t.StudyYear = vf.StudyYear
	WHERE t.Refnum = @RefNum 
END
