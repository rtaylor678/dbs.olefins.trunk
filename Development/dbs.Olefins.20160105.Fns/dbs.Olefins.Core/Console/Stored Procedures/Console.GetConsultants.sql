﻿CREATE PROCEDURE [Console].[GetConsultants]
	@StudyYear smallint,
	@Study char(3),
	@Refnum varchar(18)

AS
BEGIN

SELECT Consultant FROM [TSort] 
WHERE Consultant is not null and StudyYear =  @StudyYear and SmallRefnum =  @Refnum  
ORDER BY SmallRefnum ASC
END
