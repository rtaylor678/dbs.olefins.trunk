﻿CREATE VIEW [cons].[SubscriptionAssetsTrends]
WITH SCHEMABINDING
AS
SELECT
	[Refnum]	= p.[Refnum],
	[TrendNum]	= c.[Refnum],
	[TrendYear]	= c.[_StudyYear],
	p.[SubscriberAssetName],
	p.[SubscriberAssetDetail],
	[CoLoc] = RTRIM(p.[SubscriberAssetName]) + ' - ' +  RTRIM(p.[SubscriberAssetDetail])
FROM [cons].[SubscriptionsAssets]	p
JOIN [cons].[SubscriptionsAssets]	c
	ON	c.[StudyId]			= p.[StudyId]
	AND	c.[AssetId]			= p.[AssetId]
	AND	c.[ScenarioName]	= p.[ScenarioName]
WHERE	p.[Refnum]	IN (SELECT [Refnum] FROM [cons].[RefList]);
