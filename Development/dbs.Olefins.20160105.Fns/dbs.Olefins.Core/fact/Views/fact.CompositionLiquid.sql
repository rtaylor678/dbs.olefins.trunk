﻿CREATE VIEW [fact].[CompositionLiquid]
WITH SCHEMABINDING
AS
SELECT
	[u].[Refnum],
	[u].[CalDateKey],
	[u].[StreamId],
	[u].[StreamDescription],
	[HasPiano] = 1,
	[u].[P],
	[u].[I],
	[u].[A],
	[u].[N],
	[u].[O],
	[u].[H2],
	[ParaffinINRatio]			= CASE WHEN [u].[P] <> 0.0 THEN [u].[I] / [u].[P] END
FROM (
	SELECT
		[c].[Refnum],
		[c].[CalDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
		[c].[ComponentId],
		[c].[Component_WtPcnt]
	FROM [fact].[CompositionQuantity] [c]
	WHERE [c].[ComponentId] IN ('P', 'I', 'A', 'N', 'O', 'H2')
		AND	[c].[Component_WtPcnt] > 0.0
	) [t]
	PIVOT (
	MAX([t].[Component_WtPcnt]) FOR [t].[ComponentId] IN ([P], [I], [A], [N], [O], [H2])
	) [u]
WHERE	[u].[P] > 0.0
	OR	[u].[I] > 0.0
	OR	[u].[A] > 0.0
	OR	[u].[N] > 0.0
	OR	[u].[O] > 0.0;