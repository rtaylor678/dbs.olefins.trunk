﻿
CREATE VIEW fact.ReliabilityPyroFurnOpExPivot
WITH SCHEMABINDING
AS
SELECT
	  p.Refnum
	, p.CalDateKey
	, p.FurnId
	, p.CurrencyRpt
	, p.MaintMajor			[MaintMajor_Cur]
	, p.MaintRetube			[MaintRetube_Cur]
	, p.MaintRetubeLabor	[MaintRetubeLabor_Cur]
	, p.MaintRetubeMatl		[MaintRetubeMatl_Cur]
FROM(
	SELECT
		  r.Refnum
		, r.CalDateKey
		, r.FurnId
		, r.AccountId
		, r.CurrencyRpt
		, r.Amount_Cur
	FROM fact.ReliabilityPyroFurnOpEx r
	) u
	PIVOT (
	MAX(u.Amount_Cur) FOR AccountId IN(
		  MaintMajor
		, MaintRetube
		, MaintRetubeLabor
		, MaintRetubeMatl
		)
	) p;