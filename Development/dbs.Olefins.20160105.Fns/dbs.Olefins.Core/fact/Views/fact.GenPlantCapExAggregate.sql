﻿CREATE VIEW fact.GenPlantCapExAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	b.AccountId,
	c.CurrencyRpt,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c.Amount_Cur
		WHEN '-' THEN - c.Amount_Cur
		END
		)						[Amount_Cur],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c.Budget_Cur
		WHEN '-' THEN - c.Budget_Cur
		END
		)						[Budget_Cur]
FROM dim.Account_Bridge			b
INNER JOIN fact.GenPlantCapEx	c
	ON	c.AccountId = b.DescendantId
GROUP BY
	b.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	b.AccountId,
	c.CurrencyRpt;