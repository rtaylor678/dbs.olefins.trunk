﻿CREATE TABLE [fact].[GenPlantFeedFlex] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [StreamId]         VARCHAR (42)       NOT NULL,
    [Capability_Pcnt]  REAL               NOT NULL,
    [Demonstrate_Pcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantFeedFlex_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_GenPlantFeedFlex_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_GenPlantFeedFlex_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_GenPlantFeedFlex_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantFeedFlex] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantFeedFlex_Capability_Pcnt] CHECK ([Capability_Pcnt]>=(0.0) AND [Capability_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantFeedFlex_Demonstrate_Pcnt] CHECK ([Demonstrate_Pcnt]>=(0.0) AND [Demonstrate_Pcnt]<=(100.0)),
    CONSTRAINT [FK_GenPlantFeedFlex_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantFeedFlex_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_GenPlantFeedFlex_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_GenPlantFeedFlex_ReplVal]
    ON [fact].[GenPlantFeedFlex]([StreamId] ASC)
    INCLUDE([Refnum], [CalDateKey], [Capability_Pcnt], [Demonstrate_Pcnt]);


GO

CREATE TRIGGER [fact].[t_GenPlantFeedFlex_u]
	ON [fact].[GenPlantFeedFlex]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantFeedFlex]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantFeedFlex].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlantFeedFlex].StreamId		= INSERTED.StreamId
		AND [fact].[GenPlantFeedFlex].CalDateKey	= INSERTED.CalDateKey;

END;