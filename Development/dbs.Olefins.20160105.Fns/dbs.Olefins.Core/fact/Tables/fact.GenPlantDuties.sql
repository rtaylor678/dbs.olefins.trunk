﻿CREATE TABLE [fact].[GenPlantDuties] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantDuties_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_GenPlantDuties_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_GenPlantDuties_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_GenPlantDuties_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantDuties] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantDuties_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_GenPlantDuties_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_GenPlantDuties_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantDuties_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_GenPlantDuties_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_GenPlantDuties_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantDuties_u]
	ON [fact].[GenPlantDuties]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantDuties]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantDuties].Refnum			= INSERTED.Refnum
		AND [fact].[GenPlantDuties].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[GenPlantDuties].AccountId		= INSERTED.AccountId
		AND [fact].[GenPlantDuties].StreamId		= INSERTED.StreamId
		AND [fact].[GenPlantDuties].CalDateKey		= INSERTED.CalDateKey;

END;