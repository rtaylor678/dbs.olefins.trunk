﻿CREATE TABLE [fact].[MaintExpense] (
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [AccountId]           VARCHAR (42)       NOT NULL,
    [CompMaintHours_Pcnt] REAL               NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_MaintExpense_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_MaintExpense_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_MaintExpense_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_MaintExpense_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MaintExpense] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_MaintExpense_CompMaintHours_Pcnt] CHECK ([CompMaintHours_Pcnt]>=(0.0) AND [CompMaintHours_Pcnt]<=(100.0)),
    CONSTRAINT [FK_MaintExpense_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_MaintExpense_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MaintExpense_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_MaintExpense_u]
	ON [fact].[MaintExpense]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[MaintExpense]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[MaintExpense].Refnum		= INSERTED.Refnum
		AND [fact].[MaintExpense].AccountId		= INSERTED.AccountId
		AND [fact].[MaintExpense].CalDateKey	= INSERTED.CalDateKey;

END;