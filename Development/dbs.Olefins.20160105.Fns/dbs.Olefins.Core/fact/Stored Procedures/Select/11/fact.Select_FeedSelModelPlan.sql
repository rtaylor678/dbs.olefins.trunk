﻿CREATE PROCEDURE [fact].[Select_FeedSelModelPlan]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (7)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[PlanId],
		[f].[Daily_Bit],
			[Daily_X]			= CASE WHEN [f].[Daily_Bit]			= 1 THEN 'X' END,
		[f].[BiWeekly_Bit],
			[BiWeekly_X]		= CASE WHEN [f].[BiWeekly_Bit]		= 1 THEN 'X' END,
		[f].[Weekly_Bit],
			[Weekly_X]			= CASE WHEN [f].[Weekly_Bit]		= 1 THEN 'X' END,
		[f].[BiMonthly_Bit],
			[BiMonthly_X]		= CASE WHEN [f].[BiMonthly_Bit]		= 1 THEN 'X' END,
		[f].[Monthly_Bit],
			[Monthly_X]			= CASE WHEN [f].[Monthly_Bit]		= 1 THEN 'X' END,
		[f].[LessMonthly_Bit],
			[LessMonthly_X]		= CASE WHEN [f].[LessMonthly_Bit]	= 1 THEN 'X' END,
			[Monitored_Bit] = [f].[_Monitored_Bit]
	FROM
		[fact].[FeedSelModelPlan]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;