﻿CREATE PROCEDURE [fact].[Select_FeedSelLogistics]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 11 (3)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[FacilityId],
		[f].[Delivery_Pcnt]
	FROM
		[fact].[FeedSelLogistics]	[f]
	WHERE	[f].[Delivery_Pcnt]	> 0.0
		AND	[f].[Refnum]		= @Refnum

END;