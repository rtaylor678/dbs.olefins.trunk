﻿CREATE PROCEDURE [fact].[Select_ApcOnLinePcnt]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 9-4, Table 9-5, Table 9-6 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[ApcId],
		[f].[OnLine_Pcnt]
	FROM
		[fact].[ApcOnLinePcnt]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;