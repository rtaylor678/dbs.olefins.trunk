﻿CREATE PROCEDURE [fact].[Select_Apc]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 9-1, Table 9-2, Table 9-3, Table 9-4 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[Loop_Count],
		[f].[ClosedLoopAPC_Pcnt],
		[f].[ApcMonitorId],
		[f].[ApcAge_Years],

		[f].[OffLineLinearPrograms_Bit],
			[OffLineLinearPrograms_X]			= CASE WHEN [OffLineLinearPrograms_Bit]			= 1 THEN 'X' END,
		[f].[OffLineNonLinearSimulators_Bit],
			[OffLineNonLinearSimulators_X]		= CASE WHEN [OffLineNonLinearSimulators_Bit]	= 1 THEN 'X' END,
		[f].[OffLineNonLinearInput_Bit],
			[OffLineNonLinearInput_X]			= CASE WHEN [OffLineNonLinearInput_Bit]			= 1 THEN 'X' END,
		[f].[OnLineClosedLoop_Bit],
			[OnLineClosedLoop_X]				= CASE WHEN [OnLineClosedLoop_Bit]				= 1 THEN 'X' END,

		[f].[OffLineComparison_Days],
		[f].[OnLineComparison_Days],
		[f].[PriceUpdateFreq_Days],

		[f].[ClosedLoopModelCokingPred_Bit],
			[ClosedLoopModelCokingPred_YN]		= CASE WHEN [ClosedLoopModelCokingPred_Bit]		= 1 THEN 'Y' ELSE 'N' END,

		[f].[OptimizationOffLine_Mnths],
		[f].[OptimizationPlanFreq_Mnths],
		[f].[OptimizationOnLineOper_Mnths],
		[f].[OnLineInput_Count],
		[f].[OnLineVariable_Count],
		[f].[OnLineOperation_Pcnt],
		[f].[OnLinePointCycles_Count],

		[f].[PyroFurnModeledIndependent_Bit],
			[PyroFurnModeledIndependent_YN]		= CASE WHEN [PyroFurnModeledIndependent_Bit]	= 1 THEN 'Y' ELSE 'N' END,
		[f].[OnLineAutoModeSwitch_Bit],
			[OnLineAutoModeSwitch_YN]			= CASE WHEN [OnLineAutoModeSwitch_Bit]			= 1 THEN 'Y' ELSE 'N' END,

		[f].[DynamicResponse_Count],
		[f].[StepTestSimple_Days],
		[f].[StepTestComplex_Days],
		[f].[StepTestUpdateFreq_Mnths],
		[f].[AdaptiveAlgorithm_Count],
		[f].[EmbeddedLP_Count],
		[f].[MaxVariablesManipulated_Count],
		[f].[MaxVariablesConstraint_Count],
		[f].[MaxVariablesControlled_Count]
	FROM
		[fact].[Apc]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;