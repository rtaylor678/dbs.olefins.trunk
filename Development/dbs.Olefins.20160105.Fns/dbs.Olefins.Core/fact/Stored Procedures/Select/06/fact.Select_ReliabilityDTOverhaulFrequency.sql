﻿CREATE PROCEDURE [fact].[Select_ReliabilityDTOverhaulFrequency]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-6
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[FacilityId],
		[f].[Frequency_Years]
	FROM
		[fact].[ReliabilityDTOverhaulFrequency]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;