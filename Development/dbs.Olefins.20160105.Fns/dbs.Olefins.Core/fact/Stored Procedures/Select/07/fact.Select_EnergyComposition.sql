﻿CREATE PROCEDURE [fact].[Select_EnergyComposition]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 7
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[ComponentId],
		[f].[Component_WtPcnt]
	FROM
		[fact].[EnergyComposition]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;