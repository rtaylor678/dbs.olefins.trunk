﻿CREATE PROCEDURE [fact].[Select_PpfcLHValue]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 3
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[LHValue_Imp]	=	[f].[LHValue_MBtuLb],
			[LHValue_Met]	=	[f].[_LHValue_GJkMT]
	FROM
		[fact].[QuantityLHValue]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;