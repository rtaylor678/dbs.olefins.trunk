﻿CREATE PROCEDURE [fact].[Select_CapacityAggregate]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FactorSetId	VARCHAR(12);

	SELECT
		@FactorSetId	= [f].[FactorSetId]
	FROM
		[fact].[TSortClient]	[t]
	INNER JOIN
		[ante].[FactorSetsInStudyYear]	[f]
			ON	[f].[_StudyYear]		= [t].[_DataYear]
			AND	[f].[BaseFactorSet_Bit]	= 1
	WHERE
		[t].[Refnum]	= @Refnum

	--	Table 1-1
	SELECT
		[f].[FactorSetId],
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[Capacity_kMT],
		[f].[Capacity_MTd],
		[f].[Stream_MTd],
		[f].[Record_MTd],
		[f].[CapacityRecord_Pcnt],

			[AbsCapacity_kMT]	= ABS([f].[Capacity_kMT]),
			[AbsCapacity_MTd]	= ABS([f].[Capacity_MTd]),
			[AbsStream_MTd]		= ABS([f].[Stream_MTd]),
			[AbsRecord_MTd]		= ABS([f].[Record_MTd])
	FROM
		[fact].[CapacityAggregate]	[f]
	WHERE	[f].[Refnum]		= @Refnum
		AND	[f].[FactorSetId]	= @FactorSetId;

END;