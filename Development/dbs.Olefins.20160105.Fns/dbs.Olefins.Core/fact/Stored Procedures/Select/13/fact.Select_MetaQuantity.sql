﻿CREATE PROCEDURE [fact].[Select_MetaQuantity]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 13 (1)
	SELECT
		[u].[Refnum],
		[u].[StreamId],
		[u].[Q1],
		[u].[Q2],
		[u].[Q3],
		[u].[Q4]
	FROM (
		SELECT
			[f].[Refnum],
				[Qtr]	 = 'Q' + CONVERT(CHAR(1), [c].[CalQtr]),
			[f].[StreamId],
			[f].[Quantity_kMT]
		FROM
			[fact].[MetaQuantity]	[f]
		INNER JOIN
			[dim].[Calendar_LookUp]	[c]
				ON	[c].[CalDateKey]	= [f].[CalDateKey]
		WHERE	[f].[Refnum]		= @Refnum
			AND	[f].[Quantity_kMT]	> 0.0
		) [p]
		PIVOT(
			MAX([p].[Quantity_kMT]) FOR [p].[Qtr] IN ([Q1], [Q2], [Q3], [Q4])
		) [u];

END;