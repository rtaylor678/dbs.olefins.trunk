﻿CREATE PROCEDURE [fact].[Select_MetaEnergy]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 13 (1)
	SELECT
		[u].[Refnum],
		[u].[AccountId],
		[u].[Q1],
		[u].[Q2],
		[u].[Q3],
		[u].[Q4]
	FROM (
		SELECT
				[Qtr]	 = 'Q' + CONVERT(CHAR(1), [c].[CalQtr]),
			[f].[Refnum],
			[f].[AccountId],
				[LHV]	= CASE WHEN [t].[UomId] = 'US' THEN [f].[LHValue_MBTU] ELSE [f].[_LHValue_GJ] END
		FROM
			[fact].[MetaEnergy]			[f]
		INNER JOIN
			[dim].[Calendar_LookUp]		[c]
				ON	[c].[CalDateKey]	= [f].[CalDateKey]
		INNER JOIN
			[fact].[TSortClient]		[t]
				ON	[t].[Refnum]		= [f].[Refnum]
		WHERE
			[f].[Refnum]	=	@Refnum
		) [p]
		PIVOT(
			MAX([p].[LHV]) FOR [p].[Qtr] IN ([Q1], [Q2], [Q3], [Q4])
		) [u];

END;