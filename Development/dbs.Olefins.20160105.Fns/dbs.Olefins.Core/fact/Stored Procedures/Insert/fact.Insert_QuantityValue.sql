﻿CREATE PROCEDURE [fact].[Insert_QuantityValue]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.QuantityValue(Refnum, CalDateKey, StreamId, StreamDescription, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.FeedProdId)							[StreamId]
			, ISNULL(u.StreamDescription, u.FeedProdId)					[StreamDescription]
			, 'USD'
			, Amount_Cur
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)		[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, p.FeedProdId
				, etl.ConvStreamDescription(q.FeedProdId, p.OthLiqFeedDESC, q.MiscProd1, q.MiscProd2, q.MiscFeed) [StreamDescription]
				, p.OthLiqFeedPriceBasis								[Amount_Cur]
			FROM stgFact.FeedQuality p
			INNER JOIN stgFact.Quantity q
				ON	q.Refnum = p.Refnum
				AND q.FeedProdId = p.FeedProdId
				AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
			INNER JOIN stgFact.TSort t
				ON t.Refnum = p.Refnum
			WHERE p.OthLiqFeedPriceBasis > 0.0
			AND t.Refnum = @sRefnum
			) u;

		INSERT INTO fact.QuantityValue(Refnum, CalDateKey, StreamId, StreamDescription, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.FeedProdId)							[StreamId]
			, ISNULL(u.StreamDescription, u.FeedProdId)					[StreamDescription]
			, 'USD'
			, Amount_Cur
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, p.FeedProdId
				, etl.ConvStreamDescription(q.FeedProdId, NULL, q.MiscProd1, q.MiscProd2, q.MiscFeed) [StreamDescription]
				, ISNULL(p.H2Value, ISNULL(p.OthProd1Value, p.OthProd2Value))
																		[Amount_Cur]
			FROM stgFact.ProdQuality p
			INNER JOIN stgFact.Quantity q
				ON	q.Refnum = p.Refnum
				AND q.FeedProdId = p.FeedProdId
				AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
			INNER JOIN stgFact.TSort t		ON t.Refnum = p.Refnum
			WHERE (p.OthProd1Value > 0.0 OR p.OthProd2Value > 0.0 OR p.H2Value > 0.0)
			AND t.Refnum = @sRefnum
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;