﻿CREATE PROCEDURE [fact].[Insert_EnergySteam]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO fact.EnergySteam(Refnum, CalDateKey, AccountId, Amount_kMT, Pressure_Barg, Temp_C)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvEnergyID(e.EnergyType)						[AccountId]
			, CASE WHEN (t.UOM = 1 AND t.StudyYear >= 2007 AND t.Refnum LIKE '%[0-9][0-9]PCH[0-9][0-9]%')
				THEN	[$(DbGlobal)].dbo.UnitsConv(ABS(e.Amount), 'MLB', 'KMT')
				ELSE	ABS(e.Amount)
				END
			, [$(DbGlobal)].dbo.UnitsConv(e.PSIG, 'PSIG', 'BARG')

			, CASE WHEN (t.UOM = 1 AND t.StudyYear >= 2007 AND t.Refnum LIKE '%[0-9][0-9]PCH[0-9][0-9]%')
				THEN	[$(DbGlobal)].dbo.UnitsConv(e.Temperature, 'TF', 'TC')
				ELSE	e.Temperature
				END
		FROM stgFact.EnergyQnty e
		INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
		WHERE	e.Amount <> 0.0
			AND	etl.ConvEnergyID(RTRIM(LTRIM(e.EnergyType))) IN (SELECT b.DescendantId FROM dim.Account_Bridge b WHERE b.AccountId IN ('ExportSteam', 'PurSteam'))
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;




	--SET NOCOUNT ON;

	--DECLARE	@Refnum		VARCHAR(25);
	--DECLARE	@sRefnum	VARCHAR(10);
	--DECLARE	@StudyId	VARCHAR(3)	= 'PCH';

	--DECLARE Refnums CURSOR FAST_FORWARD	FOR	
	--SELECT
	--	[Refnum]	= LTRIM(RTRIM(l.[Refnum])),
	--	[sRefnum]	= RIGHT(LTRIM(RTRIM(l.[Refnum])), 8)
	--FROM
	--	[cons].[RefList] l WHERE l.[ListId] = '11PCH+Late'

	--OPEN Refnums;

	--FETCH NEXT FROM Refnums INTO @Refnum, @sRefnum;

	--WHILE (@@FETCH_STATUS = 0)
	--BEGIN

	--	DELETE FROM [fact].[EnergySteam] WHERE [Refnum] = @Refnum;
	--	EXECUTE [fact].[Insert_EnergySteam] @Refnum, @sRefnum, @StudyId

	--	FETCH NEXT FROM Refnums INTO @Refnum, @sRefnum;

	--END;

	--CLOSE		Refnums;
	--DEALLOCATE	Refnums;
