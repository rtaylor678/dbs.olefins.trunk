﻿CREATE PROCEDURE [fact].[Insert_PersAbsence]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.PersAbsence(Refnum, CalDateKey, PersId, Absence_Pcnt)
		SELECT
			  etl.ConvRefnum(u.Refnum, t.StudyYear, @StudyId)					[Refnum]
			, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
			, etl.ConvPersId(u.PersId)
			, CASE WHEN u.Pcnt > 1.0 THEN u.Pcnt / 100.0 ELSE u.Pcnt END * 100.0	[Pcnt]
		FROM(
			SELECT
				  m.Refnum
				, m.OCCAbsence	[OCC]
				, m.MpsAbsence	[Mps]
			FROM stgFact.Misc m
			) p
			UNPIVOT (
			Pcnt FOR PersId IN(
				  [OCC]
				, [Mps]
				)
			) u
		INNER JOIN stgFact.TSort t ON t.Refnum = u.Refnum
		WHERE t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;