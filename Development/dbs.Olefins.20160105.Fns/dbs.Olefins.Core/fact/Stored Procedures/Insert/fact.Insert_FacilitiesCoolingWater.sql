﻿CREATE PROCEDURE [fact].[Insert_FacilitiesCoolingWater]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesCoolingWater
		(
			Refnum,
			CalDateKey,
			FacilityId,
			FanHorsepower_bhp,
			PumpHorsepower_bhp,
			FlowRate_TonneDay
		)
		SELECT
			[Refnum]				= @Refnum,
			[CalDateKey]			=  etl.ConvDateKey(t.StudyYear),
			x.FacilityId,
			f.FanHorsepower_bhp,
			f.PumpHorsepower_bhp,
			f.FlowRate_TonneDay
		FROM
			stgFact.FacilitiesCoolingWater f
		INNER JOIN	
			stgFact.TSort			t
				ON t.Refnum			= f.Refnum
		LEFT OUTER JOIN
			fact.Facilities			x
				ON	x.Refnum		= @Refnum
				AND	x.FacilityId	= 'TowerCool'
		WHERE	t.Refnum			= @sRefnum
			AND	x.FacilityId IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;