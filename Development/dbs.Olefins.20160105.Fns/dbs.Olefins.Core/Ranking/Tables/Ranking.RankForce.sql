﻿CREATE TABLE [Ranking].[RankForce] (
    [ListId]     VARCHAR (42)  NOT NULL,
    [RankBreak]  VARCHAR (128) NOT NULL,
    [BreakValue] VARCHAR (12)  NOT NULL,
    [NumTiles]   TINYINT       NOT NULL,
    CONSTRAINT [PK_RankForce_1] PRIMARY KEY CLUSTERED ([ListId] ASC, [RankBreak] ASC, [BreakValue] ASC) WITH (FILLFACTOR = 70)
);

