﻿CREATE TABLE [dim].[ReliabilityServiceLevel_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [ServiceLevelId] VARCHAR (14)        NOT NULL,
    [ParentId]       VARCHAR (14)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_ReliabilityServiceLevel_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityServiceLevel_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityServiceLevel_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityServiceLevel_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_ReliabilityServiceLevel_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityServiceLevel_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ServiceLevelId] ASC),
    CONSTRAINT [CR_ReliabilityServiceLevel_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ReliabilityServiceLevel_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityServiceLevel_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ReliabilityServiceLevel_LookUp] ([ServiceLevelId]),
    CONSTRAINT [FK_ReliabilityServiceLevel_Parent_LookUp_ServiceLevel] FOREIGN KEY ([ServiceLevelId]) REFERENCES [dim].[ReliabilityServiceLevel_LookUp] ([ServiceLevelId]),
    CONSTRAINT [FK_ReliabilityServiceLevel_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ReliabilityServiceLevel_Parent] ([FactorSetId], [ServiceLevelId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityServiceLevel_Parent_u]
ON [dim].[ReliabilityServiceLevel_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityServiceLevel_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityServiceLevel_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityServiceLevel_Parent].[ServiceLevelId]		= INSERTED.[ServiceLevelId];

END;