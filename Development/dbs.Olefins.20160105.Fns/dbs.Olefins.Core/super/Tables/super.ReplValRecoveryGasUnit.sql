﻿CREATE TABLE [super].[ReplValRecoveryGasUnit] (
    [Refnum]             VARCHAR (25)       NOT NULL,
    [CalDateKey]         INT                NOT NULL,
    [ComponentId]        VARCHAR (42)       NOT NULL,
    [Quantity_kMT]       REAL               NOT NULL,
    [RecoveryMultiplier] REAL               NOT NULL,
    [Superseede]         BIT                NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_super_ReplValRecoveryGasUnit_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_super_ReplValRecoveryGasUnit_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_super_ReplValRecoveryGasUnit_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_super_ReplValRecoveryGasUnit_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_super_ReplValRecoveryGasUnit] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_super_ReplValRecoveryGasUnit_Multiplier] CHECK ([RecoveryMultiplier]>(0.0)),
    CONSTRAINT [CR_super_ReplValRecoveryGasUnit_Quantity_kMT] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [FK_super_ReplValRecoveryGasUnit_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_super_ReplValRecoveryGasUnit_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId])
);


GO

CREATE TRIGGER [super].[t_ReplValRecoveryGasUnit_u]
	ON [super].[ReplValRecoveryGasUnit]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [super].[ReplValRecoveryGasUnit]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[super].[ReplValRecoveryGasUnit].Refnum				= INSERTED.Refnum
		AND	[super].[ReplValRecoveryGasUnit].CalDateKey			= INSERTED.CalDateKey
		AND	[super].[ReplValRecoveryGasUnit].ComponentId		= INSERTED.ComponentId
		
END;