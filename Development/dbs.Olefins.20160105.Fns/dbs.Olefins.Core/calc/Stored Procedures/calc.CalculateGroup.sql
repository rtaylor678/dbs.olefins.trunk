﻿CREATE PROCEDURE [calc].[CalculateGroup]
(
	@FactorSetId	VARCHAR(12) = NULL,
	@GroupId		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY
	
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO cons.VerifyTSort_Group (calc.CalculatePlant)';
		PRINT @ProcedureDesc;

		EXECUTE	[cons].[VerifyTSort_Group]			@GroupId;

		DECLARE @fpl	[calc].[FoundationPlantList];

		INSERT INTO @fpl
		(
			[FactorSetId],
			[FactorSetName],
			[FactorSet_AnnDateKey],
			[FactorSet_QtrDateKey],
			[FactorSet_QtrDate],
			[Refnum],
			[Plant_AnnDateKey],
			[Plant_QtrDateKey],
			[Plant_QtrDate],
			[CompanyId],
			[AssetId],
			[AssetName],
			[SubscriberCompanyName],
			[PlantCompanyName],
			[SubscriberAssetName],
			[PlantAssetName],
			[CountryId],
			[StateName],
			[EconRegionId],
			[UomId],
			[CurrencyFcn],
			[CurrencyRpt],
			[AssetPassWord_PlainText],
			[StudyYear],
			[DataYear],
			[Consultant],
			[StudyYearDifference],
			[CalQtr]
		)
		SELECT
			fpl.[FactorSetId],
			fpl.[FactorSetName],
			fpl.[FactorSet_AnnDateKey],
			fpl.[FactorSet_QtrDateKey],
			fpl.[FactorSet_QtrDate],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			fpl.[Plant_QtrDateKey],
			fpl.[Plant_QtrDate],
			fpl.[CompanyId],
			fpl.[AssetIdPri],
			fpl.[AssetName],
			fpl.[SubscriberCompanyName],
			fpl.[PlantCompanyName],
			fpl.[SubscriberAssetName],
			fpl.[PlantAssetName],
			fpl.[CountryId],
			fpl.[StateName],
			fpl.[EconRegionId],
			fpl.[UomId],
			fpl.[CurrencyFcn],
			fpl.[CurrencyRpt],
			fpl.[AssetPassWord_PlainText],
			fpl.[StudyYear],
			fpl.[DataYear],
			fpl.[Consultant],
			fpl.[StudyYearDifference],
			fpl.[CalQtr]
		FROM [calc].[FoundationPlantListSource]						fpl
		WHERE	CHARINDEX(@GroupId, fpl.Refnum) >= 1
			AND	fpl.FactorSetId = ISNULL(@FactorSetId, fpl.FactorSetId);

		EXECUTE [calc].[Delete_CalculatePlant]		@GroupId, @fpl;

		EXECUTE [fact].[Delete_Osim_Group]			@GroupId, @fpl;

		EXECUTE [fact].[Insert_Osim_Group]			@GroupId, @fpl;

		EXECUTE [calc].[Insert_CalculatePlant]		@GroupId, @fpl;
		--
		--	Each 'Refnum' needs to have tables made
		--
		SET NOCOUNT ON		
		DECLARE @qry varchar(1000), @Refnum varchar(25)		
		DECLARE RefnumCursor CURSOR FOR 		
			select distinct Refnum from @fpl
		OPEN RefnumCursor		
		FETCH NEXT FROM RefnumCursor INTO @Refnum		
		WHILE (@@FETCH_STATUS <> -1)		
		BEGIN		
			IF (@@FETCH_STATUS <> -2)	
			BEGIN		
				exec MakeTables @Refnum	
			END				
			FETCH NEXT FROM RefnumCursor INTO @Refnum				
		END					
		DEALLOCATE RefnumCursor					
		set Nocount OFF		
		--
		--	Each 'Group' needs to have reports made
		--
		SET NOCOUNT ON		
		DECLARE RefnumCursor CURSOR FOR 		
			select distinct AssetId from @fpl
		OPEN RefnumCursor		
		FETCH NEXT FROM RefnumCursor INTO @Refnum		
		WHILE (@@FETCH_STATUS <> -1)		
		BEGIN		
			IF (@@FETCH_STATUS <> -2)	
			BEGIN		
				exec reports.spALL @Refnum	
			END				
			FETCH NEXT FROM RefnumCursor INTO @Refnum				
		END					
		DEALLOCATE RefnumCursor					
		set Nocount OFF		
		
	END TRY
	BEGIN CATCH
		
		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;