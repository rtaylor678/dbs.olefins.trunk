﻿CREATE PROCEDURE [calc].[Insert_MaintExpenseLaborMatl_MaintOpExAgg]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[MaintExpenseLaborMatl]([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [AccountId], [Amount_Cur])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[CurrencyRpt],
			a.[AccountId],
			a.[AmountAvg_Cur]
		FROM @fpl								fpl
		INNER JOIN [fact].[MaintOpExAggregate]	a
			ON	a.[Refnum]		= fpl.[Refnum]
			AND	a.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	a.[AccountId]	IN ('MaintContractLabor', 'MaintContractMatl', 'MaintEquip', 'MaintMatl')
			AND	a.[AmountAvg_Cur] <> 0.0
		WHERE	fpl.[CalQtr] = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;