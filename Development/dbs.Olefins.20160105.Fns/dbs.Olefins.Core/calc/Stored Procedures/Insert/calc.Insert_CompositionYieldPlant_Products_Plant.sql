﻿CREATE PROCEDURE [calc].[Insert_CompositionYieldPlant_Products_Plant]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @SuppGasOil';
		PRINT @ProcedureDesc;

		DECLARE @SuppGasOil_PyroFuelOil		TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Product_kMT]		REAL					NULL,
			[ToAlloc_kMT]		REAL					NULL,

			[_Allocated_kMT]	AS CONVERT(REAL,	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),

			[_Remainder_kMT]	AS CONVERT(REAL,	COALESCE([ToAlloc_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),
			[_Pyrolysis_kMT]	AS CONVERT(REAL,	COALESCE([Product_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END)
		);

		INSERT INTO @SuppGasOil_PyroFuelOil([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Product_kMT], [ToAlloc_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			'PyroFuelOil',
			p.[Component_kMT],
			sqa.[Quantity_kMT]
		FROM @fpl											fpl
		LEFT OUTER JOIN [inter].[YIRProduct]				p
			ON	p.[FactorSetId]		= fpl.[FactorSetId]
			AND	p.[Refnum]			= fpl.[Refnum]
			AND	p.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	p.[ComponentId]		= 'PyroFuelOil'
		LEFT OUTER JOIN [fact].[StreamQuantityAggregate]	sqa	WITH (NOEXPAND)
			ON	sqa.[FactorSetId]	= fpl.[FactorSetId]
			AND	sqa.[Refnum]		= fpl.[Refnum]
			AND	sqa.[StreamId]		= 'SuppGasOil'
		WHERE	fpl.[CalQtr]		= 4
			AND(p.[Component_kMT]	> 0.0
			OR	sqa.[Quantity_kMT]	> 0.0);

		DECLARE @SuppGasOil_PyroGasOil		TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Product_kMT]		REAL					NULL,
			[ToAlloc_kMT]		REAL					NULL,

			[_Allocated_kMT]	AS CONVERT(REAL,	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),

			[_Remainder_kMT]	AS CONVERT(REAL,	COALESCE([ToAlloc_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),
			[_Pyrolysis_kMT]	AS CONVERT(REAL,	COALESCE([Product_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END)
		);

		INSERT INTO @SuppGasOil_PyroGasOil([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Product_kMT], [ToAlloc_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			'PyroGasOil',
			p.[Component_kMT],
			r.[_Remainder_kMT]
		FROM @fpl											fpl
		LEFT OUTER JOIN [inter].[YIRProduct]				p
			ON	p.[FactorSetId]		= fpl.[FactorSetId]
			AND	p.[Refnum]			= fpl.[Refnum]
			AND	p.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	p.[ComponentId]		= 'PyroGasOil'
		LEFT OUTER JOIN @SuppGasOil_PyroFuelOil				r
			ON	r.[FactorSetId]		= fpl.[FactorSetId]
			AND	r.[Refnum]			= fpl.[Refnum]
			AND	r.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	r.[ComponentId]		= 'PyroFuelOil'
		WHERE	fpl.[CalQtr]		= 4
			AND(p.[Component_kMT]	> 0.0
			OR	r.[_Remainder_kMT]	> 0.0);

		DECLARE @SuppGasOil_PyroGasoline	TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Allocated_kMT]		REAL				NOT	NULL
		);

		INSERT INTO @SuppGasOil_PyroGasoline([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Allocated_kMT])
		SELECT
			p.[FactorSetId],
			p.[Refnum],
			p.[CalDateKey],
			'PyroGasoline',	
			p.[_Remainder_kMT]
		FROM	@SuppGasOil_PyroGasOil		p
		WHERE	p.[_Remainder_kMT]	> 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @SuppWashOil';
		PRINT @ProcedureDesc;

		DECLARE @SuppWashOil_PyroFuelOil	TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Product_kMT]		REAL					NULL,
			[ToAlloc_kMT]		REAL					NULL,

			[_Allocated_kMT]	AS CONVERT(REAL,	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),

			[_Remainder_kMT]	AS CONVERT(REAL,	COALESCE([ToAlloc_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),
			[_Pyrolysis_kMT]	AS CONVERT(REAL,	COALESCE([Product_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END)
		);

		INSERT INTO @SuppWashOil_PyroFuelOil([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Product_kMT], [ToAlloc_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			'PyroFuelOil',
			p.[_Pyrolysis_kMT],
			sqa.[Quantity_kMT]
		FROM @fpl											fpl
		LEFT OUTER JOIN @SuppGasOil_PyroFuelOil				p
			ON	p.[FactorSetId]		= fpl.[FactorSetId]
			AND	p.[Refnum]			= fpl.[Refnum]
			AND	p.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	p.[ComponentId]		= 'PyroFuelOil'
		LEFT OUTER JOIN [fact].[StreamQuantityAggregate]	sqa	WITH (NOEXPAND)
			ON	sqa.[FactorSetId]	= fpl.[FactorSetId]
			AND	sqa.[Refnum]		= fpl.[Refnum]
			AND	sqa.[StreamId]		= 'SuppWashOil'
		WHERE fpl.[CalQtr]			= 4
			AND(p.[_Pyrolysis_kMT]	> 0.0
			OR	sqa.[Quantity_kMT]	> 0.0);

		DECLARE @SuppWashOil_PyroGasOil		TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Product_kMT]		REAL					NULL,
			[ToAlloc_kMT]		REAL					NULL,

			[_Allocated_kMT]	AS CONVERT(REAL,	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),

			[_Remainder_kMT]	AS CONVERT(REAL,	COALESCE([ToAlloc_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),
			[_Pyrolysis_kMT]	AS CONVERT(REAL,	COALESCE([Product_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END)
		);

		INSERT INTO @SuppWashOil_PyroGasOil([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Product_kMT], [ToAlloc_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			'PyroGasOil',
			p.[_Pyrolysis_kMT],
			r.[_Remainder_kMT]
		FROM @fpl											fpl
		LEFT OUTER JOIN @SuppGasOil_PyroGasOil				p
			ON	p.[FactorSetId]		= fpl.[FactorSetId]
			AND	p.[Refnum]			= fpl.[Refnum]
			AND	p.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	p.[ComponentId]		= 'PyroGasOil'
		LEFT OUTER JOIN @SuppWashOil_PyroFuelOil			r
			ON	r.[FactorSetId]		= fpl.[FactorSetId]
			AND	r.[Refnum]			= fpl.[Refnum]
			AND	r.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	r.[ComponentId]		= 'PyroFuelOil'
		WHERE fpl.[CalQtr]			= 4
			AND(p.[_Pyrolysis_kMT]	> 0.0
			OR	r.[_Remainder_kMT]	> 0.0);

		DECLARE @SuppWashOil_PyroGasoline	TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Allocated_kMT]		REAL				NOT	NULL
		);

		INSERT INTO @SuppWashOil_PyroGasoline([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Allocated_kMT])
		SELECT
			p.[FactorSetId],
			p.[Refnum],
			p.[CalDateKey],
			'PyroGasoline',	
			p.[_Remainder_kMT]
		FROM	@SuppWashOil_PyroGasOil		p
		WHERE	p.[_Remainder_kMT]	> 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @SuppOther';
		PRINT @ProcedureDesc;

		DECLARE @SuppOther_PyroFuelOil	TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Product_kMT]		REAL					NULL,
			[ToAlloc_kMT]		REAL					NULL,

			[_Allocated_kMT]	AS CONVERT(REAL,	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),

			[_Remainder_kMT]	AS CONVERT(REAL,	COALESCE([ToAlloc_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),
			[_Pyrolysis_kMT]	AS CONVERT(REAL,	COALESCE([Product_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END)
		);

		INSERT INTO @SuppOther_PyroFuelOil([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Product_kMT], [ToAlloc_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			'PyroFuelOil',
			p.[_Pyrolysis_kMT],
			SUM(c.[Component_kMT])
		FROM @fpl											fpl
		LEFT OUTER JOIN @SuppWashOil_PyroFuelOil			p
			ON	p.[FactorSetId]		= fpl.[FactorSetId]
			AND	p.[Refnum]			= fpl.[Refnum]
			AND	p.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	p.[ComponentId]		= 'PyroFuelOil'
		LEFT OUTER JOIN [calc].[CompositionStream]			c
			ON	c.[FactorSetId]		= fpl.[FactorSetId]
			AND	c.[Refnum]			= fpl.[Refnum]
			AND	c.[SimModelId]		= 'Plant'
			AND	c.[OpCondId]		= 'PlantFeed'
			AND	c.[StreamId]		= 'SuppOther'
			AND	c.[ComponentId]		= 'PyroFuelOil'
		WHERE fpl.[CalQtr]			= 4
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			p.[ComponentId],
			c.[StreamId],
			p.[_Pyrolysis_kMT]
		HAVING	p.[_Pyrolysis_kMT]		> 0.0
			OR	SUM(c.[Component_kMT])	> 0.0;

		DECLARE @SuppOther_PyroGasOil		TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Product_kMT]		REAL					NULL,
			[ToAlloc_kMT]		REAL					NULL,

			[_Allocated_kMT]	AS CONVERT(REAL,	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),

			[_Remainder_kMT]	AS CONVERT(REAL,	COALESCE([ToAlloc_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END),
			[_Pyrolysis_kMT]	AS CONVERT(REAL,	COALESCE([Product_kMT], 0.0)
												-	CASE WHEN COALESCE([Product_kMT], 0.0) > COALESCE([ToAlloc_kMT], 0.0) THEN COALESCE([ToAlloc_kMT], 0.0) ELSE COALESCE([Product_kMT], 0.0) END)
		);

		INSERT INTO @SuppOther_PyroGasOil([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Product_kMT], [ToAlloc_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			'PyroGasOil',
			p.[_Pyrolysis_kMT],
			r.[_Remainder_kMT]
		FROM @fpl											fpl
		LEFT OUTER JOIN @SuppWashOil_PyroGasOil				p
			ON	p.[FactorSetId]		= fpl.[FactorSetId]
			AND	p.[Refnum]			= fpl.[Refnum]
			AND	p.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	p.[ComponentId]		= 'PyroGasOil'
		LEFT OUTER JOIN @SuppOther_PyroFuelOil				r
			ON	r.[FactorSetId]		= fpl.[FactorSetId]
			AND	r.[Refnum]			= fpl.[Refnum]
			AND	r.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	r.[ComponentId]		= 'PyroFuelOil'
		WHERE fpl.[CalQtr]			= 4
			AND(p.[_Pyrolysis_kMT]	> 0.0
			OR	r.[_Remainder_kMT]	> 0.0);

		DECLARE @SuppOther_PyroGasoline	TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Allocated_kMT]		REAL				NOT	NULL
		);

		INSERT INTO @SuppOther_PyroGasoline([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Allocated_kMT])
		SELECT
			p.[FactorSetId],
			p.[Refnum],
			p.[CalDateKey],
			'PyroGasoline',	
			p.[_Remainder_kMT]
		FROM	@SuppOther_PyroGasOil		p
		WHERE	p.[_Remainder_kMT]	> 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Distribution';
		PRINT @ProcedureDesc;

		DECLARE @Distribution	TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Distribution_kMT]	REAL				NOT	NULL	CHECK([Distribution_kMT] >= 0.0)
		);

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[_Allocated_kMT]
		FROM @SuppGasOil_PyroFuelOil	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[_Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[_Allocated_kMT]
		FROM @SuppGasOil_PyroGasOil	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[_Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[Allocated_kMT]
		FROM @SuppGasOil_PyroGasoline	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[_Allocated_kMT]
		FROM @SuppWashOil_PyroFuelOil	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[_Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[_Allocated_kMT]
		FROM @SuppWashOil_PyroGasOil	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[_Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[Allocated_kMT]
		FROM @SuppWashOil_PyroGasoline	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[_Allocated_kMT]
		FROM @SuppOther_PyroFuelOil	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[_Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[_Allocated_kMT]
		FROM @SuppOther_PyroGasOil	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[_Allocated_kMT]	> 0.0;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT 
			t.[FactorSetId],
			t.[Refnum],
			t.[CalDateKey],
			t.[ComponentId],
			t.[Allocated_kMT]
		FROM @SuppOther_PyroGasoline	t
		WHERE	t.[ComponentId]	IS NOT NULL
			AND	t.[Allocated_kMT]	> 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @Distribution (PyroGasoline)';
		PRINT @ProcedureDesc;

		INSERT INTO @Distribution([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			s.[ComponentId],
			s.[Component_kMT]
		FROM @fpl											fpl
		INNER JOIN [inter].[YIRSupplemental]				s
			ON	s.[FactorSetId]		= fpl.[FactorSetId]
			AND	s.[Refnum]			= fpl.[Refnum]
			AND	s.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	s.[ComponentId]		= 'PyroGasoline'
		WHERE	fpl.[CalQtr]		= 4
			AND	s.[ComponentId]	IS NOT NULL
			AND	s.[Component_kMT]	> 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO @DistributionAggregate';
		PRINT @ProcedureDesc;

		DECLARE @DistributionAggregate	TABLE
		(
			[FactorSetId]		VARCHAR(12)			NOT	NULL	CHECK([FactorSetId] <> ''),
			[Refnum]			VARCHAR(25)			NOT NULL	CHECK([Refnum] <> ''),
			[CalDateKey]		INT					NOT	NULL	CHECK([CalDateKey] > 19000101 AND [CalDateKey] < 99991231),

			[ComponentId]		VARCHAR(42)			NOT	NULL	CHECK([ComponentId] <> ''),
			[Distribution_kMT]	REAL				NOT	NULL	CHECK([Distribution_kMT] >= 0.0)
		);

		INSERT INTO @DistributionAggregate([FactorSetId], [Refnum], [CalDateKey], [ComponentId], [Distribution_kMT])
		SELECT
			d.[FactorSetId],
			d.[Refnum],
			d.[CalDateKey],
			d.[ComponentId],
			SUM(d.[Distribution_kMT])
		FROM @Distribution d
		WHERE	d.[ComponentId] IS NOT NULL
		GROUP BY
			d.[FactorSetId],
			d.[Refnum],
			d.[CalDateKey],
			d.[ComponentId]
		HAVING SUM(d.[Distribution_kMT]) > 0.0;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.CompositionYieldPlant';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[CompositionYieldPlant](FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId,
			Component_kMT, Component_WtPcnt, Component_Supp_kMT)
		SELECT
			t.[FactorSetId],
			t.[Refnum],
			t.[Plant_QtrDateKey],
			'Plant',
			'OSOP',
			t.[RecycleId],
			t.[ComponentId],
			t.[Component_kMT],
			t.[Component_kMT] / t.[Total_kMT] * 100.0,
			t.[Supplemtal_kMT]
		FROM (
			SELECT
				t.[FactorSetId],
				t.[Refnum],
				t.[Plant_QtrDateKey],
				r.[RecycleId],
				t.[ComponentId],
				t.[Component_kMT],
				SUM(t.[Component_kMT]) OVER(PARTITION BY t.[FactorSetId], t.[Refnum], t.[Plant_QtrDateKey]) [Total_kMT],
				t.[Supplemtal_kMT]
			FROM (
				SELECT
					tsq.[FactorSetId],
					tsq.[Refnum],
					tsq.[Plant_QtrDateKey],
					cb.[ComponentId]														[ComponentId],
					ISNULL(p.[Component_kMT], 0.0) - (ISNULL(s.[Component_kMT], 0.0) * COALESCE(100.0 - qsr.[Recycled_WtPcnt], 100.0) / 100.0)
													- COALESCE(d.[Distribution_kMT], 0.0)	[Component_kMT],
					ISNULL(s.[Component_kMT], 0.0) * COALESCE(100.0 - qsr.[Recycled_WtPcnt], 100.0) / 100.0	+ COALESCE(d.[Distribution_kMT], 0.0)
																							[Supplemtal_kMT]
				FROM @fpl											tsq
				CROSS JOIN [dim].[Component_LookUp]					cb
				LEFT OUTER JOIN [inter].[YIRProduct]				p
					ON	p.[FactorSetId]		= tsq.[FactorSetId]
					AND	p.[Refnum]			= tsq.[Refnum]
					AND	p.[CalDateKey]		= tsq.[Plant_QtrDateKey]
					AND	p.[ComponentId]		= cb.[ComponentId]
				LEFT OUTER JOIN [inter].[YIRSupplemental]			s
					ON	s.[FactorSetId]		= tsq.[FactorSetId]
					AND	s.[Refnum]			= tsq.[Refnum]
					AND	s.[CalDateKey]		= tsq.[Plant_QtrDateKey]
					AND	s.[ComponentId]		= cb.[ComponentId]
					AND	s.[ComponentId]		NOT IN ('PyroFuelOil', 'PyroGasOil', 'PyroGasoline')
				LEFT OUTER JOIN [fact].[QuantitySuppRecycled]		qsr
					ON	qsr.[Refnum]		= tsq.[Refnum]
					AND	qsr.[ComponentId]	= cb.[ComponentId]
				LEFT OUTER JOIN @DistributionAggregate				d
					ON	d.[FactorSetId]		= tsq.[FactorSetId]
					AND	d.[Refnum]			= tsq.[Refnum]
					AND	d.[ComponentId]		= cb.[ComponentId]
				WHERE	p.[ComponentId] IS NOT NULL
					OR	s.[ComponentId] IS NOT NULL
				) t
			INNER JOIN [calc].[PlantRecycles]						r
				ON	r.[FactorSetId] = t.[FactorSetId]
				AND	r.[Refnum]		= t.[Refnum]
			) t
		WHERE	t.[Total_kMT]		<> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;