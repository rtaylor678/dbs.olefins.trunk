﻿CREATE TABLE [fact].[Capacity] (
    [SubmissionId]   INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [Capacity_kMT]   FLOAT (53)         NULL,
    [StreamDay_MTSD] FLOAT (53)         NOT NULL,
    [Record_MTSD]    FLOAT (53)         NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Capacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Capacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Capacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Capacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Capacity] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamId] ASC),
    CONSTRAINT [CR_Capacity_Quantity_kMT_MinIncl_0.0] CHECK ([Capacity_kMT]>=(0.0)),
    CONSTRAINT [CR_Capacity_Record_MTd_MinIncl_0.0] CHECK ([Record_MTSD]>=(0.0)),
    CONSTRAINT [CR_Capacity_StreamDay_MTSD_MinIncl_0.0] CHECK ([StreamDay_MTSD]>=(0.0)),
    CONSTRAINT [FK_Capacity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Capacity_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_Capacity_u]
	ON [fact].[Capacity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Capacity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[Capacity].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[fact].[Capacity].[StreamId]		= INSERTED.[StreamId];

END;