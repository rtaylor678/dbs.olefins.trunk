﻿CREATE PROCEDURE [ante].[Merge_MapFactorFracType]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapFactorFracType] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_StreamId('EPMix'),		dim.Return_FactorId('TowerDeethanizer')),
		(@MethodologyId, dim.Return_StreamId('LPG'),		dim.Return_FactorId('TowerDepropanizer')),
		(@MethodologyId, dim.Return_StreamId('Condensate'),	dim.Return_FactorId('TowerNAPS')),
		(@MethodologyId, dim.Return_StreamId('Naphtha'),	dim.Return_FactorId('TowerNAPS')),
		(@MethodologyId, dim.Return_StreamId('LiqHeavy'),	dim.Return_FactorId('TowerNAPS'))
	)
	AS Source([MethodologyId], [StreamId], [FactorId])
	ON	Target.[MethodologyId]		= Source.[MethodologyId]
	AND	Target.[StreamId]			= Source.[StreamId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [FactorId])
		VALUES([MethodologyId], [StreamId], [FactorId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;