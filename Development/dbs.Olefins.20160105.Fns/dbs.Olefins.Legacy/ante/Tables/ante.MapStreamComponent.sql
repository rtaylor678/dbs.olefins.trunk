﻿CREATE TABLE [ante].[MapStreamComponent] (
    [MethodologyId]    INT                NOT NULL,
    [StreamId]         INT                NOT NULL,
    [ComponentId]      INT                NOT NULL,
    [Component_WtPcnt] FLOAT (53)         NOT NULL,
    [Recovered_WtPcnt] FLOAT (53)         NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_MapStreamComponent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)     CONSTRAINT [DF_MapStreamComponent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)     CONSTRAINT [DF_MapStreamComponent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)     CONSTRAINT [DF_MapStreamComponent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapStreamComponent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC),
    CONSTRAINT [CR_MapStreamComponent_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_MapStreamComponent_Recovered_WtPcnt] CHECK ([Recovered_WtPcnt]>=(0.0) AND [Recovered_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_MapStreamComponent_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_MapStreamComponent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_MapStreamComponent_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_MapStreamComponent_Production]
    ON [ante].[MapStreamComponent]([MethodologyId] DESC, [StreamId] ASC, [ComponentId] ASC)
    INCLUDE([Component_WtPcnt]) WHERE ([ComponentId] IN ((5), (6), (7), (8), (15), (16), (17), (18), (19), (21), (24), (162), (163), (164), (165), (166)));


GO

CREATE TRIGGER [ante].[t_MapStreamComponent_u]
	ON [ante].[MapStreamComponent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapStreamComponent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapStreamComponent].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapStreamComponent].[StreamId]		= INSERTED.[StreamId];

END;