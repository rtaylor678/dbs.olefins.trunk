﻿CREATE TABLE [ante].[ComponentEnergy] (
    [MethodologyId]        INT                NOT NULL,
    [ComponentId]          INT                NOT NULL,
    [StandardEnergy_BtuLb] FLOAT (53)         NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_ComponentEnergy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (128)     CONSTRAINT [DF_ComponentEnergy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (128)     CONSTRAINT [DF_ComponentEnergy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (128)     CONSTRAINT [DF_ComponentEnergy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ComponentEnergy] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC),
    CONSTRAINT [CR_ComponentEnergy_StandardEnergy_BtuLb_MinIncl_0.0] CHECK ([StandardEnergy_BtuLb]>=(0.0)),
    CONSTRAINT [FK_ComponentEnergy_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_ComponentEnergy_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId])
);


GO

CREATE TRIGGER [ante].[t_ComponentEnergy_u]
	ON [ante].[ComponentEnergy]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ComponentEnergy]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[ComponentEnergy].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[ComponentEnergy].[ComponentId]			= INSERTED.[ComponentId];

END;