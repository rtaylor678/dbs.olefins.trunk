﻿CREATE TABLE [ante].[NicenessComponents] (
    [MethodologyId]  INT                NOT NULL,
    [ComponentId]    INT                NOT NULL,
    [Coefficient]    FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessComponents_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_NicenessComponents_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_NicenessComponents_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_NicenessComponents_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_NicenessComponents] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC),
    CONSTRAINT [CR_NicenessComponents_Coefficient_MinIncl_0.0] CHECK ([Coefficient]>=(0.0)),
    CONSTRAINT [FK_NicenessComponents_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_NicenessComponents_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId])
);


GO

CREATE TRIGGER [ante].[t_NicenessComponents_u]
	ON [ante].[NicenessComponents]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessComponents]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[NicenessComponents].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[NicenessComponents].[ComponentId]	= INSERTED.[ComponentId];

END;