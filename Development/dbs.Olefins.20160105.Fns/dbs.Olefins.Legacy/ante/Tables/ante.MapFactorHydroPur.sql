﻿CREATE TABLE [ante].[MapFactorHydroPur] (
    [MethodologyId]  INT                NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [FactorId]       INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapFactorHydroPur_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_MapFactorHydroPur_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_MapFactorHydroPur_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_MapFactorHydroPur_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapFactorHydroPur] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FacilityId] ASC),
    CONSTRAINT [FK_MapFactorHydroPur_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_MapFactorHydroPur_Factor_LookUp] FOREIGN KEY ([FactorId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_MapFactorHydroPur_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId])
);


GO

CREATE TRIGGER [ante].[t_MapFactorHydroPur_u]
	ON [ante].[MapFactorHydroPur]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorHydroPur]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorHydroPur].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorHydroPur].[FacilityId]		= INSERTED.[FacilityId];

END;