﻿CREATE TABLE [stage].[FacilitiesElecGeneration] (
    [SubmissionId]   INT                NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [Capacity_MW]    FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesElecGeneration_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesElecGeneration] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [FacilityId] ASC),
    CONSTRAINT [CR_FacilitiesElecGeneration_Capacity_MW_MinIncl_0.0] CHECK ([Capacity_MW]>=(0.0)),
    CONSTRAINT [FK_FacilitiesElecGeneration_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesElecGeneration_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [stage].[t_FacilitiesElecGeneration_u]
	ON [stage].[FacilitiesElecGeneration]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[FacilitiesElecGeneration]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[FacilitiesElecGeneration].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[FacilitiesElecGeneration].[FacilityId]		= INSERTED.[FacilityId];

END;