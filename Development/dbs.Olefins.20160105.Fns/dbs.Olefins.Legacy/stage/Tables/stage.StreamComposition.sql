﻿CREATE TABLE [stage].[StreamComposition] (
    [SubmissionId]     INT                NOT NULL,
    [StreamNumber]     INT                NOT NULL,
    [ComponentId]      INT                NOT NULL,
    [Component_WtPcnt] FLOAT (53)         NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_StreamComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamComposition] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MaxIncl_100.0] CHECK ([Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MinIncl_0.0] CHECK ([Component_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_StreamComposition_StreamNumber] CHECK ([StreamNumber]>(0)),
    CONSTRAINT [FK_StreamComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StreamComposition_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [stage].[t_StreamComposition_u]
	ON [stage].[StreamComposition]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamComposition]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamComposition].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[StreamComposition].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[stage].[StreamComposition].[ComponentId]	= INSERTED.[ComponentId];

END;