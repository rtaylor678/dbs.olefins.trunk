﻿CREATE TABLE [stage].[StreamRecovered] (
    [SubmissionId]     INT                NOT NULL,
    [StreamNumber]     INT                NOT NULL,
    [Recovered_WtPcnt] FLOAT (53)         NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_StreamRecovered_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamRecovered] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC),
    CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MaxIncl_100.0] CHECK ([Recovered_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MinIncl_0.0] CHECK ([Recovered_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_StreamRecovered_StreamNumber] CHECK ([StreamNumber]>(0)),
    CONSTRAINT [FK_StreamRecovered_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [stage].[t_StreamRecovered_u]
	ON [stage].[StreamRecovered]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamRecovered]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamRecovered].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[stage].[StreamRecovered].[StreamNumber]		= INSERTED.[StreamNumber];

END;