﻿CREATE PROCEDURE [web].[Update_CompanyPermissions]
(
	@CompanyPermissionID	INT = NULL,
	@CompanyId				INT = NULL,
	@UserId					INT = NULL,
	@Active					BIT = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @JoinId			INT	= @CompanyPermissionID;
	DECLARE @LoginId		INT	= @UserId;

	IF (@JoinId IS NULL)
	SELECT @JoinId = jcl.[JoinId] FROM [auth].[JoinCompanyLogin] jcl WHERE jcl.[CompanyId] = @CompanyId AND jcl.[LoginId] = @LoginId;

	IF (@JoinId IS NOT NULL)
    BEGIN
		EXECUTE [auth].[Update_JoinCompanyLogin] @JoinId, @CompanyId, @LoginId, @Active;
    END
    ELSE
	BEGIN
		EXECUTE [auth].[Insert_JoinCompanyLogin] @CompanyId, @LoginId;
    END;

	SELECT @JoinId;
	RETURN @JoinId;

END;