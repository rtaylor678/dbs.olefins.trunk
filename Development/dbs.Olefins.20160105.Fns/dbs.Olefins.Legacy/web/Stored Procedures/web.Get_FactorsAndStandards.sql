﻿CREATE PROCEDURE [web].[Get_FactorsAndStandards]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (@MethodologyId IS NULL)
	SET @MethodologyId = IDENT_CURRENT('[ante].[Methodology]');

	DECLARE @Capacity_Fresh				FLOAT;
	DECLARE @Capacity_Supp				FLOAT;
	DECLARE @Capability_Ethylene		FLOAT;
	DECLARE @Capability_Olefins			FLOAT;
	DECLARE @EiiStandardEnergy_BtuHvc	FLOAT;

	SELECT
		@Capacity_Fresh		= c.[_PlantCapacity_MTSD],
		@Capacity_Supp		= c.[_SupplementalInfer_MTSD]
	FROM [calc].[Get_Capacity](@MethodologyId, @SubmissionId)  c;

	SELECT
		@EiiStandardEnergy_BtuHvc = s.[EIIStdEnergy_BtuHvc]
	FROM [calc].[Get_StandardEnergy_BtuHvc](@MethodologyId, @SubmissionId)	s;

	SELECT
		@Capability_Ethylene	= c.[Ethylene],
		@Capability_Olefins		= c.[ProdOlefins]
	FROM [calc].[Get_Utilization](@MethodologyId, @SubmissionId) c;

	SELECT
		s.[MethodologyId],
		p.[PlantId],
		p.[PlantName],
		s.[SubmissionId],
		z.[SubmissionName],
		z.[_SubmissionNameFull]								[SubmissionNameFull],

		COALESCE(@Capacity_Fresh, 0.0)						[Capacity_Fresh],
		COALESCE(@Capacity_Supp, 0.0)						[Capacity_Supp],

		COALESCE(@EiiStandardEnergy_BtuHvc, 0.0)			[EiiStandardEnergy_BtuHvc],
		COALESCE(@Capability_Ethylene, 0.0)					[Capability_Ethylene],
		COALESCE(@Capability_Olefins, 0.0)					[Capability_Olefins],

		COALESCE(s.[EIIStdEnergyAuxiliary], 0.0)			[EIIStdEnergyAuxiliary],
		COALESCE(s.[EIIStdEnergyReduction], 0.0)			[EIIStdEnergyReduction],
		COALESCE(s.[EIIStdEnergyStdConfigEandP], 0.0)		[EIIStdEnergyStdConfigEandP],
		COALESCE(s.[EIIStdEnergySuppFeedsEandP], 0.0)		[EIIStdEnergySuppFeedsEandP],
		COALESCE(s.[EIIStdEnergyTotal], 0.0)				[EIIStdEnergyTotal],
		COALESCE(s.[EIIStdEnergyUtilities], 0.0)			[EIIStdEnergyUtilities],

		COALESCE(s.[MESAuxiliary], 0.0)						[MESAuxiliary],
		COALESCE(s.[MESReduction], 0.0)						[MESReduction],
		COALESCE(s.[MESStdConfigEandP], 0.0)				[MESStdConfigEandP],
		COALESCE(s.[MESSuppFeedsEandP], 0.0)				[MESSuppFeedsEandP],
		COALESCE(s.[MESTotal], 0.0)							[MESTotal],
		COALESCE(s.[MESUtilities], 0.0)						[MESUtilities],

		COALESCE(s.[NonEnergyCESAuxiliary], 0.0)			[NonEnergyCESAuxiliary],
		COALESCE(s.[NonEnergyCESReduction], 0.0)			[NonEnergyCESReduction],
		COALESCE(s.[NonEnergyCESStdConfigEandP], 0.0)		[NonEnergyCESStdConfigEandP],
		COALESCE(s.[NonEnergyCESSuppFeedsEandP], 0.0)		[NonEnergyCESSuppFeedsEandP],
		COALESCE(s.[NonEnergyCESTotal], 0.0)				[NonEnergyCESTotal],
		COALESCE(s.[NonEnergyCESUtilities], 0.0)			[NonEnergyCESUtilities],

		COALESCE(s.[PESMaintenanceAuxiliary], 0.0)			[PESMaintenanceAuxiliary],
		COALESCE(s.[PESMaintenanceReduction], 0.0)			[PESMaintenanceReduction],
		COALESCE(s.[PESMaintenanceStdConfigEandP], 0.0)		[PESMaintenanceStdConfigEandP],
		COALESCE(s.[PESMaintenanceSuppFeedsEandP], 0.0)		[PESMaintenanceSuppFeedsEandP],
		COALESCE(s.[PESMaintenanceTotal], 0.0)				[PESMaintenanceTotal],
		COALESCE(s.[PESMaintenanceUtilities], 0.0)			[PESMaintenanceUtilities],

		COALESCE(s.[PESNonMaintenanceAuxiliary], 0.0)		[PESNonMaintenanceAuxiliary],
		COALESCE(s.[PESNonMaintenanceReduction], 0.0)		[PESNonMaintenanceReduction],
		COALESCE(s.[PESNonMaintenanceStdConfigEandP], 0.0)	[PESNonMaintenanceStdConfigEandP],
		COALESCE(s.[PESNonMaintenanceSuppFeedsEandP], 0.0)	[PESNonMaintenanceSuppFeedsEandP],
		COALESCE(s.[PESNonMaintenanceTotal], 0.0)			[PESNonMaintenanceTotal],
		COALESCE(s.[PESNonMaintenanceUtilities], 0.0)		[PESNonMaintenanceUtilities],

		COALESCE(s.[PESTotalAuxiliary], 0.0)				[PESTotalAuxiliary],
		COALESCE(s.[PESTotalReduction], 0.0)				[PESTotalReduction],
		COALESCE(s.[PESTotalStdConfigEandP], 0.0)			[PESTotalStdConfigEandP],
		COALESCE(s.[PESTotalSuppFeedsEandP], 0.0)			[PESTotalSuppFeedsEandP],
		COALESCE(s.[PESTotalTotal], 0.0)					[PESTotalTotal],
		COALESCE(s.[PESTotalUtilities], 0.0)				[PESTotalUtilities],

		COALESCE(s.[TotalkEDCAuxiliary], 0.0)				[TotalkEDCAuxiliary],
		COALESCE(s.[TotalkEDCReduction], 0.0)				[TotalkEDCReduction],
		COALESCE(s.[TotalkEDCStdConfigEandP], 0.0)			[TotalkEDCStdConfigEandP],
		COALESCE(s.[TotalkEDCSuppFeedsEandP], 0.0)			[TotalkEDCSuppFeedsEandP],
		COALESCE(s.[TotalkEDCTotal], 0.0)					[TotalkEDCTotal],
		COALESCE(s.[TotalkEDCUtilities], 0.0)				[TotalkEDCUtilities]
	FROM (
		SELECT
			s.[MethodologyId],
			s.[SubmissionId],
			rms.[Report_Prefix] + rmp.[Report_Suffix]	[FieldName],
			s.[StandardValue]
		FROM [calc].[Standards_Aggregate]	s		WITH (NOEXPAND)
		INNER JOIN [ante].[ReportMap_Standard]		rms
			ON	rms.[MethodologyId]	= s.[MethodologyId]
			AND	rms.[StandardId]	= s.[StandardId]
		INNER JOIN [ante].[ReportMap_ProcessUnit]	rmp
			ON	rmp.[MethodologyId]	= s.[MethodologyId]
			AND	rmp.[ProcessUnitId]	= s.[ProcessUnitId]
		WHERE	s.[MethodologyId]	= @MethodologyId
			AND	s.[SubmissionId]	= @SubmissionId
		) u
	PIVOT(MAX(u.[StandardValue])
		FOR u.[FieldName] IN (
			[EIIStdEnergyAuxiliary],
			[EIIStdEnergyReduction],
			[EIIStdEnergyStdConfigEandP],
			[EIIStdEnergySuppFeedsEandP],
			[EIIStdEnergyTotal],
			[EIIStdEnergyUtilities],

			[MESAuxiliary],
			[MESReduction],
			[MESStdConfigEandP],
			[MESSuppFeedsEandP],
			[MESTotal],
			[MESUtilities],

			[NonEnergyCESAuxiliary],
			[NonEnergyCESReduction],
			[NonEnergyCESStdConfigEandP],
			[NonEnergyCESSuppFeedsEandP],
			[NonEnergyCESTotal],
			[NonEnergyCESUtilities],

			[PESMaintenanceAuxiliary],
			[PESMaintenanceReduction],
			[PESMaintenanceStdConfigEandP],
			[PESMaintenanceSuppFeedsEandP],
			[PESMaintenanceTotal],
			[PESMaintenanceUtilities],

			[PESNonMaintenanceAuxiliary],
			[PESNonMaintenanceReduction],
			[PESNonMaintenanceStdConfigEandP],
			[PESNonMaintenanceSuppFeedsEandP],
			[PESNonMaintenanceTotal],
			[PESNonMaintenanceUtilities],

			[PESTotalAuxiliary],
			[PESTotalReduction],
			[PESTotalStdConfigEandP],
			[PESTotalSuppFeedsEandP],
			[PESTotalTotal],
			[PESTotalUtilities],

			[TotalkEDCAuxiliary],
			[TotalkEDCReduction],
			[TotalkEDCStdConfigEandP],
			[TotalkEDCSuppFeedsEandP],
			[TotalkEDCTotal],
			[TotalkEDCUtilities]	
		)
	) s
	INNER JOIN [fact].[Submissions]				z
		ON z.[SubmissionId]		= s.[SubmissionId]
	INNER JOIN [auth].[JoinPlantSubmission]		jps
		ON	jps.[SubmissionId]	= s.[SubmissionId]
	INNER JOIN [auth].[Plants]					p
		ON	p.[PlantId]			= jps.[PlantId];

END;