﻿CREATE PROCEDURE [Console].[UpdateValidationNotes]

	@RefNum dbo.Refnum,
	@Notes text

AS
BEGIN
IF datalength(@Notes) > 0 
	UPDATE ValidationNotes SET ValidationNotes = @Notes
	WHERE Refnum=@RefNum 
	
ELSE
	INSERT INTO ValidationNotes (RefNum, ValidationNotes) Values(@RefNum,@Notes)

END
