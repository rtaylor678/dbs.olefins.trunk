﻿CREATE PROCEDURE [Console].[GetRefNumsByConsultant]
@Consultant nvarchar(3),
@Study varchar(3),
@StudyYear nvarchar(4)

AS
BEGIN

	SELECT RefNum, Coloc FROM dbo.TSort WHERE Consultant=@Consultant AND StudyYear = @StudyYear

END

