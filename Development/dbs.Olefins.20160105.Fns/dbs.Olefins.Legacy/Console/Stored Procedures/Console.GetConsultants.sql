﻿CREATE PROCEDURE [Console].[GetConsultants]
	@StudyYear dbo.StudyYear,
	@Study dbo.Study,
	@RefNum dbo.Refnum

AS
BEGIN

SELECT Consultant FROM [TSort] 
WHERE StudyYear =  @StudyYear and RefNum =  @RefNum  
ORDER BY RefNum ASC
END
