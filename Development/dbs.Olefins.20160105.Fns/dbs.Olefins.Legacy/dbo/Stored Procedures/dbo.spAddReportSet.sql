﻿


CREATE  PROCEDURE spAddReportSet 
	@ReportSetName varchar(30),
	@CreateDate SmallDateTime = NULL ,
	@JobNo varchar(10) = NULL , 
	@Owner varchar(5) = NULL ,
	@Description varchar(100) = NULL , 
	@FileName varchar(100) = NULL
AS
IF @CreateDate IS NULL 
	SELECT @CreateDate = CONVERT(SmallDateTime, GetDate())
INSERT INTO ReportSets (ReportSetName, CreateDate, JobNo, 
	Owner, Description, FileName)
VALUES (@ReportSetName, @CreateDate, @JobNo, 
	@Owner, @Description, @FileName)


