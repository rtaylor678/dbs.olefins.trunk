﻿
/****** Object:  Stored Procedure dbo.spRankInsert    Script Date: 4/18/2003 4:32:54 PM ******/

/****** Object:  Stored Procedure dbo.spRankInsert    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.spRankInsert    Script Date: 04/13/2000 8:32:50 AM ******/
/****** Object:  Stored Procedure dbo.spRankInsert;1    Script Date: 8/8/97 8:26:09 AM ******/
CREATE PROCEDURE spRankInsert;1
@Refnum Refnum,
@RefListNo integer,
@BreakID integer,
@BreakValue varchar(12), 
@RankVariableID integer,
@Rank smallint,
@Percentile real,
@Tile Tinyint,
@Value float
AS
DECLARE @RankSpecsId integer
EXEC spAddRankSpecs @RefListNo, @BreakID, @BreakValue, @RankVariableID, @RankSpecsId OUTPUT
EXEC spRankInsert;2 @Refnum, @RankSpecsId, @Rank, @Percentile, @Tile, @Value

