﻿

/****** Object:  Stored Procedure dbo.spCheckReportSetOwner    Script Date: 4/18/2003 4:32:53 PM ******/


/****** Object:  Stored Procedure dbo.spCheckReportSetOwner    Script Date: 12/28/2001 7:34:42 AM ******/
CREATE  PROCEDURE spCheckReportSetOwner
	@ReportSetID integer = NULL, @ReportSetName varchar(30) = NULL,
	@CurrUser varchar(30) = NULL
AS
DECLARE @Result smallint
DECLARE @UName varchar(30), @UGroup varchar(30)
IF @ReportSetID IS NULL AND @ReportSetName IS NULL

BEGIN
	RAISERROR('You must supply a ReportSet ID or Name.', 16, -1)
	RETURN -102
END
ELSE BEGIN
	IF @ReportSetID IS NULL
	BEGIN
		SELECT @ReportSetID = ReportSetID
		FROM ReportSets
		WHERE ReportSetName = @ReportSetName
		IF @ReportSetID IS NULL
			RETURN 1
		IF @CurrUser IS NULL
			SELECT @UName = User_Name(), @UGroup = name 
			FROM sysusers
			WHERE uid = (SELECT gid from sysusers
					WHERE name = User_Name())
		ELSE
			SELECT @UName = @CurrUser, @UGroup = name 
			FROM sysusers
			WHERE uid = (SELECT gid from sysusers
					WHERE name = @CurrUser)
		IF @UName <> 'dbo' AND NOT EXISTS (SELECT * FROM sysmembers m INNER JOIN sysusers u ON u.uid = m.memberuid
					INNER JOIN sysusers g ON g.uid = m.groupuid
					WHERE g.name IN ('db_owner','Developer', 'Datagrp')
					AND u.name = @UName)
		BEGIN
			IF NOT EXISTS (SELECT * FROM ReportSets
				WHERE ReportSetID = @ReportSetID AND
				(Owner = @UName OR Owner IS NULL))
					RETURN -101

		END
/*
		IF EXISTS (SELECT * FROM ReportQueue
				WHERE ReportSetID = @ReportSetID)
			RETURN -105
		ELSE
*/
			RETURN 1
	END
END


