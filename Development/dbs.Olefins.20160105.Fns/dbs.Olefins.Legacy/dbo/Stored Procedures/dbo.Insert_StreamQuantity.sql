﻿CREATE PROCEDURE [dbo].[Insert_StreamQuantity]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamTypeId			INT,
	@Quantity_kMT			FLOAT
)
AS
BEGIN

	IF(@Quantity_kMT >= 0.0)
	EXECUTE [stage].[Insert_StreamQuantity] @SubmissionId, @StreamNumber, @StreamTypeId, @Quantity_kMT;

END;
