﻿CREATE TABLE [dbo].[FeedProdVal] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [FeedProdID] VARCHAR (20)   NOT NULL,
    [TotVal]     REAL           NULL,
    [UnitVal]    REAL           NULL,
    CONSTRAINT [PK___FeedProdVal] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

