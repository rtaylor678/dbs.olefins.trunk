﻿
/****** Object:  View dbo.DataDictionary    Script Date: 4/18/2003 4:32:51 PM ******/

CREATE  VIEW DataDictionary AS
	SELECT o.ObjectName, c.ColumnName, c.ColumnID, c.ObjectID, o.SectionID,
		SectionName = (SELECT SectionName FROM [$(DbOlefinsGlobal)].dbo.DD_Sections s WHERE s.SectionID = o.SectionID),
		o.HelpID as ObjectHelpID, o.ObjectDesc, o.KeyFields,
		o.TableType, o.RptSortOrder, o.DescFields, c.StoredUnits,
		c.DisplayUnits, c.HelpID as ColumnHelpID, c.LookupID
	FROM (dbo.DD_Objects o INNER JOIN dbo.DD_Columns c
	on o.ObjectID = c.ObjectID)

