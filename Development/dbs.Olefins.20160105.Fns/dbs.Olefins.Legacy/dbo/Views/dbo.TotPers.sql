﻿
/****** Object:  View dbo.TotPers    Script Date: 4/18/2003 4:32:51 PM ******/

/****** Object:  View dbo.TotPers    Script Date: 12/28/2001 7:34:24 AM ******/
create view TotPers as 
	SELECT Refnum, SectionID, SUM(NumPers) AS TotPers,
		SUM(STH) AS TotSTHrs,
		SUM(OvtHours) AS TotOvtHrs,
		SUM(Contract) as TotContractHrs
		FROM Pers
		GROUP BY Refnum, SectionID
