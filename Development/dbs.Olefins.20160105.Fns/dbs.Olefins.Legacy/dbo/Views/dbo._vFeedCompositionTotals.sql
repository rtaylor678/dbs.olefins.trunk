﻿
/****** Object:  View dbo._vFeedCompositionTotals    Script Date: 4/18/2003 4:32:51 PM ******/



/****** Object:  View dbo._vFeedCompositionTotals    Script Date: 12/28/2001 7:34:24 AM ******/
CREATE   VIEW _vFeedCompositionTotals AS 



SELECT Refnum, FeedProdID, ISNULL(Methane,0) + ISNULL(Ethane,0) + ISNULL(Ethylene,0) + ISNULL(Propane,0) +
ISNULL(Propylene,0) + ISNULL(nButane,0) + ISNULL(iButane,0) + ISNULL(Isobutylene,0)  + ISNULL(Butene1,0) + ISNULL(Butadiene,0) +
ISNULL(nPentane,0) + ISNULL(iPentane,0) + ISNULL(nHexane,0) + ISNULL(iHexane,0) + ISNULL(Septane,0) + ISNULL(Octane,0) +
ISNULL(CO2,0) + ISNULL(Hydrogen,0) + ISNULL(Sulfur,0) AS TotCompLight,
ISNULL(NParaffins,0) + ISNULL(IsoParaffins,0) + ISNULL(Naphthenes,0) + ISNULL(Olefins,0) + ISNULL(Aromatics,0) AS TotCompLiquid
FROM FeedQuality





