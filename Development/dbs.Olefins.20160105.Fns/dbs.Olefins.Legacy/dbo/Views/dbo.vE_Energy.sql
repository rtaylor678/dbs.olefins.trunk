﻿
/****** Object:  View dbo.vE_Energy    Script Date: 8/17/2006 6:55:19 AM ******/

/****** Object:  View dbo.vE_Energy    Script Date: 8/3/2006 7:13:31 AM ******/
/**These are the fields used to calculate E_Score.**/
CREATE   VIEW [dbo].vE_Energy
AS
SELECT e.Refnum,  [FG_MonitorPcnt],  [FG_Optimize], [FG_Drain], [FG_FlowMeter]
, [AI_Optimize], [AI_LeakRepair], [AI_CleanBurner]
, [Drft_OnlinePcnt], [Drft_MinAirLeak], [Drft_MnlOptimize], [Drft_AutoControl]
, [Brnr_Replace], [Brnrr_MngSys], [Brnr_Maint], [Brnr_Test]
, [SC_COT_C], [SC_CalorimeterPcnt], [SC_EffluentPcnt], [SC_EffluentFdbackPcnt]
, [SC_SIPPcnt], [SC_SIPFdbackPcnt], [SC_MPCPcnt], [SC_ComparePcnt], [SC_TubeMetal_C]
, [SteamOffline], [SteamMPCPcnt], [SteamRealtime]
, [TLEUpgrade], [TLESecondaryPcnt], [TLEHeatRecover]
, [Decoke_CO2Pcnt], [Decoke_CO2Target]
, [HPS_Monitor], [HPS_BlowdownConduct], [HPS_Blowdown_C]
, [Qnch_Clean], [Qnch_Qcalc], [Qnch_OilViscosity], [Qnch_Fractionator], [Qnch_OvhdTemp_C], [Qnch_BottomTemp_C]
, [HEP_MonitorQCalc], [HEP_CleanPcnt]
, [RS_H2CH4Online], [CW_Optimize], [CW_SPO],  [CW_Measure]
, [Dist_OnlinePcnt], [Dist_SPC]
, [DT36_LightKey], [DT36_HeavyKey], [DT36_AdvMultiDC], [DT36_OnlineDDC]
, [DT37_LightKey], [DT37_HeavyKey], [DT37_AdvMultiDC], [DT37_OnlineDDC]
, [PolyEthyl_LightKey], [PolyEthyl_HeavyKey], [PolyEthyl_AdvMultiDC], [PolyEthyl_OnlineDDC]
, [LPD_LightKey], [LPD_HeavyKey], [LPD_AdvMultiDC], [LPD_OnlineDDC]
, [HPD_LightKey], [HPD_HeavyKey], [HPD_AdvMultiDC], [HPD_OnlineDDC]
, [PolyPropyl_LightKey], [PolyPropyl_HeavyKey], [PolyPropyl_AdvMultiDC], [PolyPropyl_OnlineDDC]
, [LPDeB_LightKey], [LPDeB_HeavyKey], [LPDeB_AdvMultiDC], [LPDeB_OnlineDDC]
, [MBLR_SoftwarePcnt], [MBLR_ValueMnl], [MBLR_Flare]
, [EM_Monitor], [EM_Balance], [EM_OfflinePerf], [EM_OperatorFdback], [EM_Mng], [EM_Integrate], [EM_HIP]
, [Cogen], [ZeroSteamVent], [CondensateRecovPcnt], [StreamTrapPcnt]
, [OEC_PRR], [UnitEnergyPerf], [OperatorTrain]
, 'ISP'=SUM(ISP), 'EMS'=SUM(EMS), 'TargetEFF'=SUM(TargetEFF), 'WheelClean'=SUM(WheelClean)
, 'WheelCoating'=SUM(WheelCoating), 'PCE'=SUM(PCE), 'TDS_PSIG'=SUM(TDS_PSIG), 'TDSTemp'=SUM(TDSTemp)
, 'TDCTemp'=SUM(TDCTemp), 'TargetVSC'=SUM(TargetVSC), 'FreqVSC'=SUM(FreqVSC), 'MaintainVSC'=SUM(MaintainVSC)
, 'Kickbacks'=SUM(Kickbacks), 'AvgKickbacks_Pcnt'=SUM(AvgKickbacks_Pcnt) 
, 'LoopOptimize'=SUM(LoopOptimize), 'HEPerf'=SUM(HEPerf), 'HEService'=SUM(HEService) 
FROM E_Energy e INNER JOIN vE_CompressorTotals c ON c.Refnum = e.Refnum
GROUP BY e.Refnum, [FG_MonitorPcnt],  [FG_Optimize], [FG_Drain], [FG_FlowMeter]
, [AI_Optimize], [AI_LeakRepair], [AI_CleanBurner]
, [Drft_OnlinePcnt], [Drft_MinAirLeak], [Drft_MnlOptimize], [Drft_AutoControl]
, [Brnr_Replace], [Brnrr_MngSys], [Brnr_Maint], [Brnr_Test]
, [SC_COT_C], [SC_CalorimeterPcnt], [SC_EffluentPcnt], [SC_EffluentFdbackPcnt]
, [SC_SIPPcnt], [SC_SIPFdbackPcnt], [SC_MPCPcnt], [SC_ComparePcnt], [SC_TubeMetal_C]
, [SteamOffline], [SteamMPCPcnt], [SteamRealtime]
, [TLEUpgrade], [TLESecondaryPcnt], [TLEHeatRecover]
, [Decoke_CO2Pcnt], [Decoke_CO2Target]
, [HPS_Monitor], [HPS_BlowdownConduct], [HPS_Blowdown_C]
, [Qnch_Clean], [Qnch_Qcalc], [Qnch_OilViscosity], [Qnch_Fractionator], [Qnch_OvhdTemp_C], [Qnch_BottomTemp_C]
, [HEP_MonitorQCalc], [HEP_CleanPcnt]
, [RS_H2CH4Online], [CW_Optimize], [CW_SPO],  [CW_Measure]
, [Dist_OnlinePcnt], [Dist_SPC]
, [DT36_LightKey], [DT36_HeavyKey], [DT36_AdvMultiDC], [DT36_OnlineDDC]
, [DT37_LightKey], [DT37_HeavyKey], [DT37_AdvMultiDC], [DT37_OnlineDDC]
, [PolyEthyl_LightKey], [PolyEthyl_HeavyKey], [PolyEthyl_AdvMultiDC], [PolyEthyl_OnlineDDC]
, [LPD_LightKey], [LPD_HeavyKey], [LPD_AdvMultiDC], [LPD_OnlineDDC]
, [HPD_LightKey], [HPD_HeavyKey], [HPD_AdvMultiDC], [HPD_OnlineDDC]
, [PolyPropyl_LightKey], [PolyPropyl_HeavyKey], [PolyPropyl_AdvMultiDC], [PolyPropyl_OnlineDDC]
, [LPDeB_LightKey], [LPDeB_HeavyKey], [LPDeB_AdvMultiDC], [LPDeB_OnlineDDC]
, [MBLR_SoftwarePcnt], [MBLR_ValueMnl], [MBLR_Flare]
, [EM_Monitor], [EM_Balance], [EM_OfflinePerf], [EM_OperatorFdback], [EM_Mng], [EM_Integrate], [EM_HIP]
, [Cogen], [ZeroSteamVent], [CondensateRecovPcnt], [StreamTrapPcnt]
, [OEC_PRR], [UnitEnergyPerf], [OperatorTrain]



