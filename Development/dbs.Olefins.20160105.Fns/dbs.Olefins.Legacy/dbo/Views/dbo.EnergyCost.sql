﻿
-- TOOK THIS OUT 2/8/2007 + ISNULL(o.PurOth,0) 
--
--  Not sure where this is used, just wanted it to be consistent

CREATE   View dbo.EnergyCost
AS
SELECT t.Refnum, NetEnergyCost_MTHVC = SUM(ISNULL(o.PurElec,0) + 
 ISNULL(o.PurSteam,0) + ISNULL(o.PurFG,0) +                         
 ISNULL(o.PurLiquid,0) + ISNULL(ft.PPCFGCost,0) + 
 ISNULL(ft.PPCOthFuelCost,0) + ISNULL(o.SteamExports,0) + 
 ISNULL(o.PowerExports,0)) / p.HVChemDiv
FROM TSort t INNER JOIN Opex o ON t.Refnum = o.Refnum
INNER JOIN FinancialTot ft ON t.Refnum = ft.Refnum INNER JOIN Production p 
 ON p.Refnum = o.Refnum
GROUP BY t.Refnum, p.HVChemDiv



