﻿CREATE TABLE [dim].[Component_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [ComponentId]        INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Component_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Component_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Component_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Component_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Component_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Component_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_Component_Bridge_ComponentId] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Component_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Component_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Component_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Component_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [ComponentId]) REFERENCES [dim].[Component_Parent] ([MethodologyId], [ComponentId]),
    CONSTRAINT [FK_Component_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[Component_Parent] ([MethodologyId], [ComponentId])
);


GO

CREATE TRIGGER [dim].[t_Component_Bridge_u]
	ON [dim].[Component_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Component_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Component_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Component_Bridge].[ComponentId]		= INSERTED.[ComponentId]
		AND	[dim].[Component_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;