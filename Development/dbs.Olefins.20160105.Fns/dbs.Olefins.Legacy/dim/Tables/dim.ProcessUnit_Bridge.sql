﻿CREATE TABLE [dim].[ProcessUnit_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [ProcessUnitId]      INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ProcessUnit_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ProcessUnit_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ProcessUnit_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ProcessUnit_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ProcessUnit_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ProcessUnit_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ProcessUnitId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_ProcessUnit_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId]),
    CONSTRAINT [FK_ProcessUnit_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_ProcessUnit_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_ProcessUnit_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [ProcessUnitId]) REFERENCES [dim].[ProcessUnit_Parent] ([MethodologyId], [ProcessUnitId]),
    CONSTRAINT [FK_ProcessUnit_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[ProcessUnit_Parent] ([MethodologyId], [ProcessUnitId]),
    CONSTRAINT [FK_ProcessUnit_Bridge_StandardId] FOREIGN KEY ([ProcessUnitId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])
);


GO

CREATE TRIGGER [dim].[t_ProcessUnit_Bridge_u]
	ON [dim].[ProcessUnit_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnit_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[ProcessUnit_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[ProcessUnit_Bridge].[ProcessUnitId]		= INSERTED.[ProcessUnitId]
		AND	[dim].[ProcessUnit_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;