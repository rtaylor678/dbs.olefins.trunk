﻿CREATE TABLE [auth].[LoginsPassWords] (
    [LoginId]        INT                NOT NULL,
    [pSalt]          BINARY (512)       NOT NULL,
    [pWord]          VARBINARY (512)    NOT NULL,
    [ValidFrom]      DATETIMEOFFSET (7) CONSTRAINT [DF_LoginsPassWords_ValidFrom] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [ValidTo]        DATETIMEOFFSET (7) NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_LoginsPassWords_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_LoginsPassWords_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_LoginsPassWords_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_LoginsPassWords_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_UsersPasswords] PRIMARY KEY CLUSTERED ([LoginId] ASC, [ValidFrom] DESC),
    CONSTRAINT [CL_LoginsPassWords_pWord] CHECK (datalength([pWord])>=(64)),
    CONSTRAINT [CV_UsersPassWords_ValidDates] CHECK ([ValidTo]>=[ValidFrom]),
    CONSTRAINT [FK_LoginsPassWords_LoginAttributes] FOREIGN KEY ([LoginId]) REFERENCES [auth].[Logins] ([LoginId])
);


GO
CREATE NONCLUSTERED INDEX [IX_LoginsPassWords_Current]
    ON [auth].[LoginsPassWords]([LoginId] ASC, [pWord] ASC) WHERE ([ValidTo] IS NULL);


GO

CREATE TRIGGER [auth].[t_LoginsPassWords_u]
	ON [auth].[LoginsPassWords]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[LoginsPassWords]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[LoginsPassWords].[LoginId]		= INSERTED.[LoginId]
		AND	[auth].[LoginsPassWords].[ValidFrom]	= INSERTED.[ValidFrom];

END;