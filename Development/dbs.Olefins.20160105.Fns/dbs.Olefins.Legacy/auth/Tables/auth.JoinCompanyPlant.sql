﻿CREATE TABLE [auth].[JoinCompanyPlant] (
    [JoinId]         INT                IDENTITY (1, 1) NOT NULL,
    [CompanyId]      INT                NOT NULL,
    [PlantId]        INT                NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_JoinCompanyPlant_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_JoinCompanyPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_JoinCompanyPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_JoinCompanyPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_JoinCompanyPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_JoinCompanyPlant] PRIMARY KEY CLUSTERED ([JoinId] ASC),
    CONSTRAINT [FK_JoinCompanyPlant_Companies] FOREIGN KEY ([CompanyId]) REFERENCES [auth].[Companies] ([CompanyId]),
    CONSTRAINT [FK_JoinCompanyPlant_Plants] FOREIGN KEY ([PlantId]) REFERENCES [auth].[Plants] ([PlantId]),
    CONSTRAINT [UK_JoinCompanyPlant_CompanyPlant] UNIQUE NONCLUSTERED ([CompanyId] ASC, [PlantId] ASC),
    CONSTRAINT [UK_JoinCompanyPlant_Plant] UNIQUE NONCLUSTERED ([PlantId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinCompanyPlant_Active]
    ON [auth].[JoinCompanyPlant]([CompanyId] ASC, [PlantId] ASC) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_JoinCompanyPlant_u]
	ON [auth].[JoinCompanyPlant]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinCompanyPlant]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinCompanyPlant].[JoinId]	= INSERTED.[JoinId];

END;