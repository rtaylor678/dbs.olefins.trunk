﻿CREATE TABLE [calc].[StandardEnergy_PyrolysisHvc] (
    [MethodologyId]            INT                NOT NULL,
    [SubmissionId]             INT                NOT NULL,
    [ContainedProduct_Dur_kMT] FLOAT (53)         NOT NULL,
    [Recovered_Dur_kMT]        FLOAT (53)         NULL,
    [PyrolysisProduct_Dur_kMT] FLOAT (53)         NOT NULL,
    [PyrolysisProduct_Dur_klb] FLOAT (53)         NOT NULL,
    [HvcYieldDivisor_kLbDay]   FLOAT (53)         NOT NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]             ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_PyrolysisHvc] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_ContainedProduct_Dur_kMT_MinIncl_0.0] CHECK ([ContainedProduct_Dur_kMT]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_HvcYieldDivisor_kLbDay_MinIncl_0.0] CHECK ([HvcYieldDivisor_kLbDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_PyrolysisProduct_Dur_klb_MinIncl_0.0] CHECK ([PyrolysisProduct_Dur_klb]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_PyrolysisProduct_Dur_kMT_MinIncl_0.0] CHECK ([PyrolysisProduct_Dur_kMT]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_Recovered_Dur_kMT_MinIncl_0.0] CHECK ([Recovered_Dur_kMT]>=(0.0)),
    CONSTRAINT [FK_StandardEnergy_PyrolysisHvc_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StandardEnergy_PyrolysisHvc_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_StandardEnergy_PyrolysisHvc_u]
	ON [calc].[StandardEnergy_PyrolysisHvc]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_PyrolysisHvc]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_PyrolysisHvc].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_PyrolysisHvc].[SubmissionId]		= INSERTED.[SubmissionId];

END;