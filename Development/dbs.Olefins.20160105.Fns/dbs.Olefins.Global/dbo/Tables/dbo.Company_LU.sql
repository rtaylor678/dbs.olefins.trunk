﻿CREATE TABLE [dbo].[Company_LU] (
    [CompanyID]   CHAR (12) NOT NULL,
    [CompanyName] CHAR (40) NOT NULL,
    CONSTRAINT [PK_Company_LU_1__22] PRIMARY KEY CLUSTERED ([CompanyID] ASC)
);

