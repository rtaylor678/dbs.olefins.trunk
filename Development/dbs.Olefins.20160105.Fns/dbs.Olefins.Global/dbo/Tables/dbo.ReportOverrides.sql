﻿CREATE TABLE [dbo].[ReportOverrides] (
    [Refnum]  VARCHAR (15) NOT NULL,
    [UnitID]  VARCHAR (15) CONSTRAINT [DF_ReportOverrides_UnitID] DEFAULT ('') NOT NULL,
    [VarName] VARCHAR (50) NOT NULL,
    [Value]   VARCHAR (50) NULL,
    CONSTRAINT [PK_ReportOverrides] PRIMARY KEY CLUSTERED ([Refnum] ASC, [UnitID] ASC, [VarName] ASC) WITH (FILLFACTOR = 70)
);

