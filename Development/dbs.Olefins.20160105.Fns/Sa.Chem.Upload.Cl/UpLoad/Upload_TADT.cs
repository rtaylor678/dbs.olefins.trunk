﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadTADT(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const UInt32 cBeg = 6;
			const UInt32 cEnd = 8;

			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				string cT06_03 = (includeSpace) ? "Table 6-3" : "Table6-3";
				wks = wkb.Worksheets[cT06_03];

				for (c = cBeg; c <= cEnd; c++)
				{
					try
					{
						r = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_TADT]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@Refnum			CHAR (9),
								cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

								//@TrainID			TINYINT,
								cmd.Parameters.Add("@TrainID", SqlDbType.Int).Value = (byte)(c - 5);

								#region Train Turnaround

								//@PctTotEthylCap	REAL     = NULL
								r = 6;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@PctTotEthylCap", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@PlanInterval		REAL     = NULL
								r = 7;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@PlanInterval", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@PlanDT			REAL     = NULL
								r = 8;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@PlanDT", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@ActInterval		REAL     = NULL
								r = 9;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@ActInterval", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@ActDT			REAL     = NULL
								r = 10;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@ActDT", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@TACost			REAL     = NULL
								r = 11;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@TACost", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@TAManHrs			REAL     = NULL
								r = 15;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@TAManHrs", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@TADate			DATETIME = NULL
								r = 18;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@TADate", SqlDbType.DateTime).Value = ReturnDateTime(rng); }

								//@TAMini			CHAR (3) = NULL
								r = 20;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@TAMini", SqlDbType.VarChar, 3).Value = ReturnString(rng, 3); }

								#endregion

								cmd.ExecuteNonQuery();
							}

							if (StudyYear >= 2015)
							{
								using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_TADTMini]", cn))
								{
									cmd.CommandType = CommandType.StoredProcedure;

									//@Refnum			CHAR (9),
									cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

									//@TrainID			TINYINT,
									cmd.Parameters.Add("@TrainID", SqlDbType.Int).Value = (byte)(c - 5);


									//@MiniTaDowntime_Hrs			REAL	= NULL,
									r = 21;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@MiniTaDowntime_Hrs", SqlDbType.Float).Value = ReturnFloat(rng); }

									//@MiniTaCost_USD				REAL	= NULL,
									r = 22;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@MiniTaCost_USD", SqlDbType.Float).Value = ReturnFloat(rng); }

									//@MiniTaMaintWork_ManHrs		REAL	= NULL,
									r = 23;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@MiniTaMaintWork_ManHrs", SqlDbType.Float).Value = ReturnFloat(rng); }

									//@MiniTa_Date					REAL	= NULL
									r = 24;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@MiniTa_Date", SqlDbType.Float).Value = ReturnFloat(rng); }

									cmd.ExecuteNonQuery();
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadTADT", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_TADT]", ex);
					}
				}
				cn.Close();
			}
			rng = null;
			wks = null;
		}
	}
}