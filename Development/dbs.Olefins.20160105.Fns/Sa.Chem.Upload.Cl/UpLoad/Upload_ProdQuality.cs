﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadProdQuality(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			const UInt32 c = 9;

			object[] itm;

			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				string cT03_01 = (includeSpace) ? "Table 3" : "Table3";
				wks = wkb.Worksheets[cT03_01];

				#region Mole Percent

				itm = new object[2]
				{
					new object[2] { "Hydrogen", 9 },
					new object[2] { "FuelGasSales", 11 }
				};

				foreach (object[] s in itm)
				{
					try
					{
						r = Convert.ToUInt32(s[1]);

						using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdQuality]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum		CHAR (9)
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID    VARCHAR (20)
							cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = (string)s[0];

							//@MolePcnt      REAL         = NULL
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@MolePcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@H2Value       REAL         = NULL
							if (Convert.ToString(s[0]) == "Hydrogen")
							{
								r = 10;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@H2Value", SqlDbType.Float).Value = ReturnFloat(rng); }
							}

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdQuality", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdQuality]", ex);
					}
				}

				#endregion

				#region Weight Percent

				itm = new object[6]
				{
					new object[2] { "Ethylene", 13 },
					new object[2] { "EthyleneCG", 14 },
					new object[2] { "Propylene", 15 },
					new object[2] { "PropyleneCG", 16 },
					new object[2] { "PropyleneRG", 17 },
					new object[2] { "PropaneC3", 18 }
				};

				foreach (object[] s in itm)
				{
					try
					{
						r = Convert.ToUInt32(s[1]);

						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdQuality]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@Refnum			CHAR (9)
								cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

								//@FeedProdID		VARCHAR (20)
								cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = (string)s[0];

								//@WtPcnt			REAL         = NULL
								cmd.Parameters.Add("@WtPcnt", SqlDbType.Float).Value = ReturnFloat(rng);

								cmd.ExecuteNonQuery();
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdQuality", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdQuality]", ex);
					}
				}

				#endregion

				#region PPFC

				try
				{
					if (RangeHasValue(wks.Cells[31, c]) || RangeHasValue(wks.Cells[32, c]))
					{
						using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdQuality]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum			CHAR (9)
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID		VARCHAR (20)
							cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = "PPCFuel";

							//@HeatValue     REAL         = NULL
							r = 31;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng))
							{
								//Done: Convert from GJ/MT to Btu/Lb
								if (ReturnFloat(rng) <= 200f)
								{
									cmd.Parameters.Add("@HeatValue", SqlDbType.Float).Value = ReturnFloat(rng) * 429.945894799096f;
								}
								else
								{
									cmd.Parameters.Add("@HeatValue", SqlDbType.Float).Value = ReturnFloat(rng);
								}
							}

							//@H2Content     REAL         = NULL
							r = 32;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@H2Content", SqlDbType.Float).Value = ReturnFloat(rng); }

							cmd.ExecuteNonQuery();
						}
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdQuality", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdQuality]", ex);
				}

				itm = new object[5] {
				new object[2] { "PPCFuel_H2", 32 },
				new object[2] { "PPCFuel_CH4", 33 },
				new object[2] { "PPCFuel_ETH", 34 },
				new object[2] { "PPCFuel_Other", 35 },
				new object[2] { "PPCFuel_Inert", 36 },
			};

				foreach (object[] s in itm)
				{
					try
					{
						r = Convert.ToUInt32(s[1]);

						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdQuality]", cn);
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum			CHAR (9)
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID		VARCHAR (20)
							cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = Convert.ToString(s[0]);

							//@WtPcnt			REAL         = NULL
							cmd.Parameters.Add("@WtPcnt", SqlDbType.Float).Value = ReturnFloat(rng);

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdQuality", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdQuality]", ex);
					}
				}

				#endregion

				#region Other Product Value

				try
				{
					r = 37;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng))
					{
						using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdQuality]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum			CHAR (9)
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID		VARCHAR (20)
							cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = "OthProd1";

							//@OthProd1Value			REAL         = NULL
							cmd.Parameters.Add("@OthProd1Value", SqlDbType.Float).Value = ReturnFloat(rng);

							cmd.ExecuteNonQuery();
						}
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdQuality", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdQuality]", ex);
				}

				try
				{
					r = 38;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng))
					{
						using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdQuality]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum			CHAR (9)
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID		VARCHAR (20)
							cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = "OthProd2";

							//@OthProd2Value			REAL         = NULL
							cmd.Parameters.Add("@OthProd2Value", SqlDbType.Float).Value = ReturnFloat(rng);

							cmd.ExecuteNonQuery();
						}
					}

				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdQuality", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdQuality]", ex);
				}

				#endregion

				cn.Close();
			}
			rng = null;
			wks = null;
		}
	}
}