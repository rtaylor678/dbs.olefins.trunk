﻿--EXECUTE [Ranking].[RankRefList] '13PCH+late';

DECLARE @Refnum				VARCHAR(18) = '2013PCH029P';
DECLARE @RankVariable		SYSNAME		= 'APCImplIndex';
DECLARE @FactorSetId		VARCHAR(12)	= '2013'


SELECT [a].[Refnum], [a].[APCImplIndex], [a].[APCOnLineIndex] FROM [dbo].[APC] [a] WHERE [a].[Refnum] = @Refnum;

EXECUTE [dbo].[spGENSUM] @Refnum, @FactorSetId;
SELECT [g].[Refnum], [g].[APCImplIndex], [g].[APCOnLineIndex] FROM [dbo].[GENSUM] [g] WHERE [g].[Refnum] = @Refnum;

SELECT * FROM [Ranking].[RefineryValues] [r] WHERE [r].[Refnum] = @Refnum AND [r].[RankVariable] = @RankVariable;
--EXECUTE [Ranking].[GetRanking]    @Refnum, '13PCH+Late', 'TotalList', @RankVariable;














