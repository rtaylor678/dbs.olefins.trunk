﻿INSERT INTO [dbo].[Queue]
(
	[Refnum],
	[Program],
	[Consultant],
	[FilePath]
)
SELECT
	[Refnum]		= '20' + [t].[Refnum],
	[Program]		= 'UploadOSIM',
	[Consultant]	= 'RRH',
	[FilePath]		= 'K:\Study\Olefins\2013\Correspondence\' + [t].[Refnum] + '\OSIM20' + [t].[Refnum] + '.xls'
FROM (VALUES
--('13PCH029'),
--('13PCH029P')
--('13PCH010'),
--('13PCH099'),
--('13PCH154'),
--('13PCH167'),
--('13PCH196'),
--('13PCH201R'),
--('13PCH205'),
--('13PCH206'),
--('13PCH208'),
--('13PCH209'),
--('13PCH209A')
--('13PCH225'),
--('13PCH228')
('13PCH233')
) [t]([Refnum])
LEFT OUTER JOIN
	[dbo].[Queue] [q]
		ON	[q].[Refnum]	= '20' + [t].[Refnum]
		AND	[q].[Program]	= 'UploadOSIM'
WHERE
	[q].[Refnum] IS NULL;


SELECT * FROM [dbo].[Queue];





--EXECUTE [calc].[Delete_CalculatePlant] '2013PCH109';
--EXECUTE [calc].[Delete_CalculatePlant] '2013PCH134';
--EXECUTE [calc].[Delete_CalculatePlant] '2013PCH176';
--EXECUTE [calc].[Delete_CalculatePlant] '2013PCH186';

--SELECT * FROM [fact].[Capacity] WHERE [Refnum] = '2013PCH109'

--EXECUTE [fact].[Delete_OSIM] '2013PCH109';
--EXECUTE [fact].[Delete_OSIM] '2013PCH134';
--EXECUTE [fact].[Delete_OSIM] '2013PCH176';
--EXECUTE [fact].[Delete_OSIM] '2013PCH186';

--EXECUTE [stgFact].[Delete_OlefinsData] '13PCH109';
--EXECUTE [stgFact].[Delete_OlefinsData] '13PCH134';
--EXECUTE [stgFact].[Delete_OlefinsData] '13PCH176';
--EXECUTE [stgFact].[Delete_OlefinsData] '13PCH186';

