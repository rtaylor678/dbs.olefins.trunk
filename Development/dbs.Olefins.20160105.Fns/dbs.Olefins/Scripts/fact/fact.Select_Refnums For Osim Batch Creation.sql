﻿ALTER PROCEDURE [fact].[Select_Refnums]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		[t].[Refnum],
		[t].[PreviousRefnum]
	FROM
		[fact].[PlantNameHistoryLimit](2009)	[t]
	WHERE	[t].[StudyYear]	=	2015
		AND	[t].[Refnum] IN ('2015PCH006','2015PCH143','2015PCH040','2015PCH194','2015PCH007','2015PCH140');
		--AND	[t].[Refnum] IN ('2015PCH134');

END;