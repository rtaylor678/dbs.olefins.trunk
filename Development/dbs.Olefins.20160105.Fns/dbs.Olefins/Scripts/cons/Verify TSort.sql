﻿
DECLARE @Refnum			VARCHAR(18) = '2013PCH029';
DECLARE @sRefnum		VARCHAR(18) = RIGHT(@Refnum, LEN(@Refnum) - 2);
DECLARE @FactorSetID	VARCHAR(4)	= '2013'; -- LEFT(@Refnum, 4);
DECLARE @ProcedureDesc	NVARCHAR(4000);





		DECLARE @AssetId	VARCHAR(5)	= [etl].[ConvAssetPCH](@Refnum);
		DECLARE @StudyId	VARCHAR(4)	= [etl].[ConvRefnumStudy](@Refnum);


		SELECT
			t.CompanyId,
			t.CoName,
			t.Co,
			etl.ConvCompanyId(t.CompanyId, t.CoName, t.Co)
		FROM
			[stgFact].[TSort] t
		WHERE
			[t].[Refnum] = @sRefnum
