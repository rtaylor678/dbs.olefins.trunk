﻿--Use this script to add records to create a new study

DECLARE	@CalDateKey		INT			= 20151231;
DECLARE	@StudyId		VARCHAR(4)	= 'PCH';
DECLARE	@StudyYear		INT			= 2015;
DECLARE	@FactorSetId	VARCHAR(4)	= CONVERT(CHAR(4), @StudyYear);

--SET NOCOUNT ON;

DECLARE @Subscriptions TABLE
(
	[Refnum]				VARCHAR(12)			NULL	CHECK([Refnum] <> ''),
	[CompanyNameReport]		VARCHAR(42)		NOT	NULL	CHECK([CompanyNameReport] <> ''),
	[AssetNameReport]		VARCHAR(128)	NOT	NULL	CHECK([AssetNameReport] <> ''),
	[AssetIDPri]			VARCHAR(30)		NOT	NULL	CHECK([AssetIDPri] <> ''),
	[CompanyId]				VARCHAR(24)		NOT	NULL	CHECK([CompanyId] <> ''),

	[Coordinator]			VARCHAR(48)			NULL	CHECK([Coordinator] <> ''),
	[EMail]					VARCHAR(254)		NULL	CHECK([EMail] <> ''),
	[CompanyPassWord]		VARCHAR(128)		NULL	CHECK([CompanyPassWord] <> ''),

	[CountryId]				VARCHAR(4)			NULL	CHECK([CountryId] <> ''),
	[Co]					AS [CompanyNameReport],
	[Loc]					AS [AssetNameReport],

	PRIMARY KEY CLUSTERED ([AssetIDPri] ASC),
	UNIQUE NONCLUSTERED ([CompanyId] ASC, [Loc] ASC)
);

INSERT INTO @Subscriptions
(
	[Refnum],
	[CompanyNameReport],
	[AssetNameReport],
	[AssetIDPri],
	[CompanyId],
	[Coordinator],
	[EMail],
	[CompanyPassWord],
	[CountryId]
)
SELECT 
	[t].[Refnum],
	[t].[CompanyNameReport],
	[t].[AssetNameReport],
	[t].[AssetIDPri],
	[t].[CompanyId],
	[t].[Coordinator],
	[t].[EMail],
	[t].[CompanyPassWord],
		[CountryId]	= COALESCE([a].[CountryId], [t].[CountryId])
FROM (VALUES
--('2013PCH115', 'BASF SE', 'ANTWERP', '115', 'BASF', 'Dr. Jelan Kuhn', 'jelan.kuhn@basf.com', 'LU_2015_@q', NULL),
--('2013PCH003', 'BASF SE', 'LUDWIGSHAFEN', '003', 'BASF', 'Dr. Jelan Kuhn', 'jelan.kuhn@basf.com', 'LU_2015_@q', NULL),
--('2013PCH162', 'BASF TOTAL', 'PORT ARTHUR BASF', '162', 'BASF TOTAL', 'George Franklin', 'george.franklin@basf.com', 'btpportarthur', NULL),
--('2013PCH196', 'BASF YPC', 'NANJING BASF YPC', '196', 'BASF YPC', 'Zhao Jianqing', 'zhaojq@basf-ypc.com.cn', 'BASFYPCCBPC', NULL),
--('2013PCH077', 'BOREALIS', 'PORVOO', '077', 'Borealis', NULL, NULL, NULL, NULL),
--('2013PCH028', 'BOREALIS', 'STENUNGSUND', '028', 'Borealis', NULL, NULL, NULL, NULL),
--('2013PCH163', 'BOROUGE', 'RUWAIS #1', '163', 'Borouge', NULL, NULL, NULL, NULL),
--('2013PCH233', 'BOROUGE', 'RUWAIS #2', '233', 'Borouge', NULL, NULL, NULL, NULL),
--(NULL, 'CARMEL OLEFINS', 'HAIFA', '126', 'CARMEL', 'Rotem Oren', 'orotem@bazan.co.il', '526161355', NULL),
--('2013PCH234', 'Chandra Asri', 'CILEGON', '234', 'ChandraAsri', NULL, NULL, NULL, NULL),
--('2013PCH004', 'CP CHEM', 'CEDAR BAYOU', '004', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', NULL),
--('2013PCH005', 'CP CHEM', 'PORT ARTHUR', '005', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', NULL),
--('2013PCH184', 'CP CHEM', 'QCHEM MESAIEED', '184', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', NULL),
--('2013PCH222', 'CP CHEM', 'RAS LAFFAN', '222', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', NULL),
--('2013PCH23A', 'CP CHEM', 'SWEENY 22', '23A', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', NULL),
--('2013PCH23B', 'CP CHEM', 'SWEENY 24', '23B', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', NULL),
--('2013PCH23C', 'CP CHEM', 'SWEENY 33', '23C', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', NULL),
--('2013PCH197', 'CSPC', 'HUIZHOU', '197', 'CSPC', 'Song Jinquan', 'song.jinquan@cnoocshell.com', 'd@tatrans5#', NULL),
--('2013PCH097', 'DOW', 'BAHIA BLANCA 1', '097', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH165', 'DOW', 'BAHIA BLANCA 2', '165', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH112', 'DOW', 'BOEHLEN', '112', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH057', 'DOW', 'FREEPORT 7', '057', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH099', 'DOW', 'FREEPORT 8', '099', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH100', 'DOW', 'FT SASKATCHEWAN', '100', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH058', 'DOW', 'PLAQUEMINE 2', '058', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH059', 'DOW', 'PLAQUEMINE 3', '059', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH082', 'DOW', 'ST CHARLES', '082', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH045', 'DOW', 'TARRAGONA', '045', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH046', 'DOW', 'TERNEUZEN 1', '046', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH047', 'DOW', 'TERNEUZEN 2', '047', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH164', 'DOW', 'TERNEUZEN 3', '164', 'DOW', 'Jim Benton', 'bentonjh@dow.com', 'dow13plants', NULL),
--('2013PCH092', 'DUPONT', 'ORANGE', '092', 'du Pont', 'Teresa Gerber', 'Teresa.L.Gerber-1@dupont.com', 'dupont2015', NULL),
--('2013PCH148', 'EQUATE', 'KUWAIT OLEFINS 1', '148', 'Equate', 'Vangala Sarma', 'vangalss@equate.com', 'avinashdara', NULL),
--('2013PCH215', 'EQUATE', 'KUWAIT OLEFINS 2', '215', 'Equate', 'Vangala Sarma', 'vangalss@equate.com', 'avinashdara', NULL),
--('2013PCH012', 'EXXONMOBIL', 'BATON ROUGE', '012', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--('2013PCH011', 'EXXONMOBIL', 'BAYTOWN BOP', '011', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--('2013PCH133', 'EXXONMOBIL', 'BAYTOWN BOP-X', '133', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--('2013PCH018', 'EXXONMOBIL', 'BEAUMONT', '018', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--('2013PCH015', 'EXXONMOBIL', 'FIFE', '015', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--('2013PCH013', 'EXXONMOBIL', 'NDG', '013', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--('2013PCH076', 'EXXONMOBIL', 'SARNIA', '076', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--('2013PCH159', 'EXXONMOBIL', 'SINGAPORE', '159', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', NULL),
--(NULL, 'EXXONMOBIL', 'SINGAPORE 2', '241', 'EXXONMOBIL', 'Colby Jones', 'colby.f.jones@exxonmobil.com', 'EMCC2015XOM', 'SGP'),
--('2013PCH029', 'FLINT HILLS RESOURCES', 'PORT ARTHUR', '029', 'FlintHillsResources', NULL, NULL, NULL, NULL),
--('2013PCH029P', 'FLINT HILLS RESOURCES', 'PORT ARTHUR', '029', 'FlintHillsResources', NULL, NULL, NULL, NULL),
--('2013PCH119', 'FORMOSA', 'POINT COMFORT', '119', 'Formosa', NULL, NULL, NULL, NULL),
--(NULL, 'FPCC', 'MAILIAO OL1', '237', 'FPCC', 'Chun-Lung Lee', 'chun-lunglee@fpcc.com.tw', 'N000014092', 'CHN'),
--(NULL, 'FPCC', 'MAILIAO OL2', '238', 'FPCC', 'Chun-Lung Lee', 'chun-lunglee@fpcc.com.tw', 'N000014092', 'CHN'),
--(NULL, 'FPCC', 'MAILIAO OL3', '239', 'FPCC', 'Chun-Lung Lee', 'chun-lunglee@fpcc.com.tw', 'N000014092', 'CHN'),
--('2013PCH209', 'FREP', 'FUJIAN', '209', 'FREP', 'Heping Le', 'liheping@fjrep.com', 'OlefinFREP', NULL),
--('2013PCH209A', 'FREP', 'FUJIAN', '209', 'FREP', NULL, NULL, NULL, NULL),
--(NULL, 'GIE COGEFY', 'FEYZIN', '071', 'GIE COGEFY', 'Patrice Bruneau', 'patrice.bruneau@total.com', 'gerancevapo2015', 'FRA'),
--('2013PCH175', 'HANWHA TOTAL', 'DAESAN', '175', 'Samsung', 'In-Yeob Lee', 'inyeob.lee@hanwha-total.com', 'topooda100', NULL),
--('2013PCH223', 'Indian Oil', 'PANIPAT', '223', 'IOCL', NULL, NULL, NULL, NULL),
--('2013PCH198', 'IRPC', 'RAYONG', '198', 'IRPC', NULL, NULL, NULL, NULL),
--('2013PCH157', 'JX NIPPON', 'KAWASAKI', '157', 'Nippon', NULL, NULL, NULL, NULL),
--('2013PCH217', 'LUKOIL', 'STAVROLEN', '217', 'Lukoil', NULL, NULL, NULL, NULL),
--('2013PCH025', 'LYONDELLBASELL', 'BERRE OB2', '025', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH16A', 'LYONDELLBASELL', 'CHANNELVIEW OP-1', '16A', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH16B', 'LYONDELLBASELL', 'CHANNELVIEW OP-2', '16B', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH088', 'LYONDELLBASELL', 'CLINTON', '088', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH036', 'LYONDELLBASELL', 'CORPUS CHRISTI', '036', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH127', 'LYONDELLBASELL', 'LAPORTE', '127', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH087', 'LYONDELLBASELL', 'MORRIS', '087', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH094', 'LYONDELLBASELL', 'MUNCHSMUNSTER', '094', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH055', 'LYONDELLBASELL', 'WESSELING OM4', '055', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH056', 'LYONDELLBASELL', 'WESSELING OM6', '056', 'LyondellBasell', NULL, NULL, NULL, NULL),
--('2013PCH228', 'MOC', 'MAP TA PHUT', '228', 'MOC', 'Kasidid Asumpinpong', 'kasidida@scg.co.th', 'MOC2015SCG', NULL),
--('2013PCH205', 'MOL', 'TISZAUJVAROS 1', '205', 'TVK', 'Zoltan Pomichal', 'zpomichal@mol.hu', 'TSC1-TSC2Study', NULL),
--('2013PCH206', 'MOL', 'TISZAUJVAROS 2', '206', 'TVK', 'Zoltan Pomichal', 'zpomichal@mol.hu', 'TSC1-TSC2Study', NULL),
--(NULL, 'MITSUBISHI', 'KASHIMA', '147', 'Mitsubishi', 'Toshiyuki Endo', '4103520@cc.m-kagaku.co.jp', 'TEMITSU410', 'JPN'),
--(NULL, 'NOVA', 'CORUNNA', '022', 'Nova', 'Ted Salari', 'ted.salari@novachem.com', 'solomonsurvey', NULL),
--(NULL, 'NOVA', 'JOFFRE 1', '01A', 'Nova', 'Ted Salari', 'ted.salari@novachem.com', 'solomonsurvey', NULL),
--(NULL, 'NOVA', 'JOFFRE 2', '01B', 'Nova', 'Ted Salari', 'ted.salari@novachem.com', 'solomonsurvey', NULL),
--(NULL, 'NOVA', 'JOFFRE 3', '181', 'Nova', 'Ted Salari', 'ted.salari@novachem.com', 'solomonsurvey', NULL),
--('2013PCH033', 'OMV', 'BURGHAUSEN', '033', 'OMV', 'Thomas Svolba', 'thomas.svolba@omv.com', '2015petromv', NULL),
--('2013PCH020', 'OMV', 'SCHWECHAT', '020', 'OMV', 'Thomas Svolba', 'thomas.svolba@omv.com', '2015petromv', NULL),
--('2013PCH053', 'PCS', 'SINGAPORE 1', '053', 'PCS', NULL, NULL, 'pcsoy2015', NULL),
--('2013PCH144', 'PCS', 'SINGAPORE 2', '144', 'PCS', NULL, NULL, 'pcsoy2015', NULL),
--(NULL, 'PETRONAS', 'OPTIMAL OLEFINS', '210', 'PETRONAS', NULL, NULL, NULL, 'MYS'),
--(NULL, 'PETRONAS', 'ETHYLENE MALAYSIA', '123', 'PETRONAS', NULL, NULL, NULL, 'MYS'),
--('2013PCH208', 'PKN', 'LITVINOV', '208', 'PKN', NULL, NULL, NULL, NULL),
--('2013PCH199', 'PKN', 'PLOCK', '199', 'PKN', 'Leszek Slawinski', 'leszek.slawinski@orlen.pl', 'Poland@2015', NULL),
--('2013PCH083', 'PTT', 'MAP TA PHUT', '083', 'PTT', NULL, NULL, NULL, NULL),
--('2013PCH113', 'PTT', 'MAP TA PHUT #2', '113', 'PTT', NULL, NULL, NULL, NULL),
--('2013PCH190', 'PTT', 'MAP TA PHUT #2-2', '190', 'PTT', NULL, NULL, NULL, NULL),
--('2013PCH221', 'PTT', 'MAP TA PHUT #3', '221', 'PTT', NULL, NULL, NULL, NULL),
--('2013PCH121', 'Qapco', 'MESAIEED', '121', 'QAPCO', NULL, NULL, NULL, NULL),
--('2013PCH168', 'RELIANCE', 'GANDHAR', '168', 'Reliance', 'Deepesh Bhojwani', 'deepesh.bhojwani@ril.com', 'ecnailer2015', NULL),
--('2013PCH122', 'RELIANCE', 'HAZIRA', '122', 'Reliance', 'Deepesh Bhojwani', 'deepesh.bhojwani@ril.com', 'ecnailer2015', NULL),
--('2013PCH131', 'RELIANCE', 'NAGOTHANE', '131', 'Reliance', 'Deepesh Bhojwani', 'deepesh.bhojwani@ril.com', 'ecnailer2015', NULL),
--('2013PCH167', 'RELIANCE', 'VADODARA', '167', 'Reliance', 'Deepesh Bhojwani', 'deepesh.bhojwani@ril.com', 'ecnailer2015', NULL),
--('2013PCH041', 'REPSOL', 'PUERTOLLANO', '041', 'REPSOL', NULL, NULL, NULL, NULL),
--('2013PCH078', 'REPSOL', 'SINES', '078', 'REPSOL', 'Luis Casado Padilla', 'lcasadop@repsol.com', 'Kalimera15', NULL),
--('2013PCH079', 'REPSOL', 'TARRAGONA', '079', 'REPSOL', 'Luis Casado Padilla', 'lcasadop@repsol.com', 'Kalimera15', NULL),
--('2013PCH145', 'ROC', 'RAYONG', '145', 'ROC', 'Kasidid Asumpinpong', 'kasidida@scg.co.th', 'MOC2015SCG', NULL),
--('2013PCH176R', 'Rollover', 'OITA', '176', 'Rollover', NULL, NULL, NULL, NULL),
--('2013PCH109R', 'Rollover', 'TOKUYAMA', '109', 'Rollover', NULL, NULL, NULL, NULL),
--('2013PCH134R', 'Rollover', 'ULSAN 2', '134', 'Rollover', NULL, NULL, NULL, NULL),
--('2013PCH186R', 'Rollover', 'YEOSU', '186', 'Rollover', NULL, NULL, NULL, NULL),
--('2013PCH201R', 'Rollover', 'YEOSU NCC2', '201', 'Rollover', NULL, NULL, NULL, NULL),
--('2013PCH095', 'RUHR OEL', 'GELSENKIRCHEN 3', '095', 'RuhrOel', 'Michael Bacioi', 'michael.bacioi@bpge.de', 'solole2015', NULL),
--('2013PCH096', 'RUHR OEL', 'GELSENKIRCHEN 4', '096', 'RuhrOel', 'Michael Bacioi', 'michael.bacioi@bpge.de', 'solole2015', NULL),
--('2013PCH084', 'SABIC', 'GELEEN OLEFINS 3', '084', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH085', 'SABIC', 'GELEEN OLEFINS 4', '085', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH172', 'SABIC', 'KEMYA KOP', '172', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH169', 'SABIC', 'PK1', '169', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH170', 'SABIC', 'PK2', '170', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH171', 'SABIC', 'PK3', '171', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH141', 'SABIC', 'SADAF', '141', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH231', 'SABIC', 'SAUDI KAYAN', '231', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH230', 'SABIC', 'SHARQ', '230', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH191', 'SABIC', 'UNITED', '191', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH068', 'SABIC', 'WILTON 6', '068', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH173', 'SABIC', 'YANPET 1', '173', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH174', 'SABIC', 'YANPET 2', '174', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH232', 'SABIC', 'YANSAB', '232', 'SABIC', NULL, NULL, NULL, NULL),
--('2013PCH106', 'SASOL NA', 'WESTLAKE', '106', 'Sasol NA', 'Mike Milanowski', 'mike.milanowski@us.sasol.com', 'SasolSol15', NULL),
--('2013PCH195', 'SECCO', 'SHANGHAI', '195', 'SECCO', 'Huang Xuefeng', 'huang.xuefeng@secco.com.cn', 'S@S2015ole', NULL),
--('2013PCH091', 'SHELL', 'DEER PARK', '091', 'SHELL', 'Rob Loonen', 'rob.loonen@shell.com', 'SDSsol2015', NULL),
--('2013PCH026', 'SHELL', 'MOERDIJK', '026', 'SHELL', NULL, NULL, NULL, NULL),
--('2013PCH089', 'SHELL', 'NORCO GO1', '089', 'SHELL', 'Rob Loonen', 'rob.loonen@shell.com', 'SDSsol2015', NULL),
--('2013PCH235', 'SHELL', 'PULAU BUKOM', '235', 'SHELL', 'Rob Loonen', 'rob.loonen@shell.com', 'SDSsol2015', NULL),
--('2013PCH043', 'SHELL', 'RHEINLAND', '043', 'SHELL', NULL, NULL, NULL, NULL),
--(NULL, 'SHOWA DENKO', 'OITA', '176', 'ShowaDenko', 'Yoshio Kawabata', 'kawabata.yoshio.xhanx@showadenko.com', 'sdk_c12_11', NULL),
--('2013PCH213', 'SIBUR', 'NIZHNI NOVGOROD', '213', 'Sibur', NULL, NULL, NULL, NULL),
--('2013PCH214', 'SIBUR', 'TOMSK', '214', 'Sibur', NULL, NULL, NULL, NULL),
--('2013PCH152', 'SINOPEC', 'MAOMING', '152', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', NULL),
--('2013PCH153', 'SINOPEC', 'QILU', '153', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', NULL),
--('2013PCH128', 'SINOPEC', 'SHANGHAI2#', '128', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', NULL),
--(NULL, 'SINOPEC', 'WUHAN', '240', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', 'CHN'),
--('2013PCH149', 'SINOPEC', 'YANGZI', '149', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', NULL),
--('2013PCH154', 'SINOPEC', 'YANSHAN', '154', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', NULL),
--('2013PCH225', 'SINOPEC', 'ZHENHAI', '225', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', NULL),
--('2013PCH224', 'SINOPEC', 'ZHONGSHA', '224', 'Sinopec', 'Liu Ranbing', 'liurb.edri@sinopec.com', 'yxjx2015', NULL),
--('2013PCH207', 'SLOVNAFT', 'BRATISLAVA', '207', 'SLOVNAFT', 'Lukas Gasparovic', 'lukas.gasparovic@slovnaft.sk', 'kl76ip80ca', NULL),
--('2013PCH998', 'Solomon', 'SINGAPORE', '998', 'Solomon', NULL, NULL, NULL, NULL),
--('2013PCH212', 'Tasnee', 'AL JUBAIL', '212', 'Tasnee', NULL, NULL, NULL, NULL),
--('2013PCH105', 'TONEN', 'KAWASAKI', '105', 'TONEN', 'Takashi Yanagi', 'takashi.yanagi@tonengeneral.co.jp', '2015cracker', NULL),
--('2013PCH50B', 'TOTAL', 'ANTWERP NC2', '50B', 'Total', NULL, NULL, NULL, NULL),
--('2013PCH049', 'TOTAL', 'ANTWERP NC3', '049', 'Total', NULL, NULL, NULL, NULL),
--('2013PCH042', 'TOTAL', 'GONFREVILLE', '042', 'Total', NULL, NULL, NULL, NULL),
--('2013PCH111', 'VERSALIS', 'BRINDISI', '111', 'VERSALIS', NULL, NULL, NULL, NULL),
--('2013PCH070', 'VERSALIS', 'DUNKERQUE', '070', 'VERSALIS', NULL, NULL, NULL, NULL),
--('2013PCH074', 'VERSALIS', 'PORTO MARGHERA', '074', 'VERSALIS', NULL, NULL, NULL, NULL),
--('2013PCH010', 'VERSALIS', 'PRIOLO', '010', 'VERSALIS', NULL, NULL, NULL, NULL),
--(NULL, 'WESTLAKE', 'SULPHUR 1', '118', 'Westlake', 'John Casey', 'jcasey@westlake.com', 'Westlake2015', NULL),
--(NULL, 'WESTLAKE', 'SULPHUR 2', '179', 'Westlake', 'John Casey', 'jcasey@westlake.com', 'Westlake2015', NULL),
--(NULL, 'WILLIAMS', 'GEISMAR', '039', 'Williams', 'Parker Tucker', 'parker.tucker@williams.com', 'Razorbacks', NULL),
--('2013PCH200', 'YNCC', 'YEOSU NCC1', '200', 'YNCC', NULL, NULL, NULL, NULL),
--('2013PCH201', 'YNCC', 'YEOSU NCC2', '201', 'YNCC', NULL, NULL, NULL, NULL)

--(NULL, 'SK', 'ULSAN 2', '134', 'SK', 'Soojeong Choi', 'sj.choi@sk.com', 'skgc201601', NULL)

----Braskem 20160129
--('2011PCH006', 'BRASKEM', 'CAMACARI 1', '006', 'BRASKEM', 'Thaís Camargo', 'thais.camargo@braskem.com', 'SOLBRK2015', NULL),
--('2011PCH143', 'BRASKEM', 'CAMACARI 2', '143', 'BRASKEM', 'Thaís Camargo', 'thais.camargo@braskem.com', 'SOLBRK2015', NULL),
--('2011PCH040', 'BRASKEM', 'CAPUAVA', '040', 'BRASKEM', 'Thaís Camargo', 'thais.camargo@braskem.com', 'SOLBRK2015', NULL),
--('2011PCH194', 'BRASKEM', 'DUQUE DE CAXIAS', '194', 'BRASKEM', 'Thaís Camargo', 'thais.camargo@braskem.com', 'SOLBRK2015', NULL),
--('2011PCH007', 'BRASKEM', 'TRIUNFO 1', '007', 'BRASKEM', 'Thaís Camargo', 'thais.camargo@braskem.com', 'SOLBRK2015', NULL),
--('2011PCH140', 'BRASKEM', 'TRIUNFO 2', '140', 'BRASKEM', 'Thaís Camargo', 'thais.camargo@braskem.com', 'SOLBRK2015', NULL)

--FORMOSA 20160212
--('2013PCH119', 'FORMOSA', 'POINT COMFORT 1', '119', 'Formosa', 'Troy Hardegree', 'troyh@ftpc.fpcusa.com', 'fpcolefins', NULL),
--SB commented out - this was here and uncommented(NULL, 'FORMOSA', 'POINT COMFORT 2', '242', 'Formosa', 'Troy Hardegree', 'troyh@ftpc.fpcusa.com', 'fpcolefins', 'USA')

--SB New 2016-03-17
----VERSALIS 20160317
--('2015PCH111', 'VERSALIS', 'BRINDISI', '111', 'VERSALIS', 'Michela Frisieri', 'MICHELA.FRISIERI@VERSALIS.ENI.COM', 'SOLO2015MBF', NULL),
--('2015PCH070', 'VERSALIS', 'DUNKERQUE', '070', 'VERSALIS', 'Michela Frisieri', 'MICHELA.FRISIERI@VERSALIS.ENI.COM', 'SOLO2015MBF', NULL),
--('2015PCH074', 'VERSALIS', 'PORTO MARGHERA', '074', 'VERSALIS', 'Michela Frisieri', 'MICHELA.FRISIERI@VERSALIS.ENI.COM', 'SOLO2015MBF', NULL),
--('2015PCH010', 'VERSALIS', 'PRIOLO', '010', 'VERSALIS', 'Michela Frisieri', 'MICHELA.FRISIERI@VERSALIS.ENI.COM', 'SOLO2015MBF', NULL)

--for SASOL POLYMERS
--('2015PCH218', 'SASOL POLYMERS', 'SECUNDA', '218', 'SASOL POLYMERS', 'Cindy Mestry', 'cindy.mestry@sasol.com', 'Polymers123', NULL)

--SB NAPHTACHIMIE 2016-05-26
('2015PCH075', 'NAPHTACHIMIE', 'LAVERA', '075', 'NAPHTACHIMIE', 'Thomas Mignot', 'thomas.mignot@naphtachimie.com', 'SolomonNC15', NULL)


--also note --   '([t].[Refnum] NOT IN'  -below

) [t](
	[Refnum],
	[CompanyNameReport],
	[AssetNameReport],
	[AssetIDPri],
	[CompanyId],
	[Coordinator],
	[EMail],
	[CompanyPassWord],
	[CountryId]
)
LEFT OUTER JOIN
	[cons].[Assets]		[a]
		ON	[t].[AssetIDPri]	= [a].[AssetIdPri]
		AND	[a].[AssetIDSec]	IS NULL
WHERE
	([t].[Refnum] NOT IN
	(
		'2013PCH234',
		'2013PCH029P',
		'2013PCH209A',
		'2013PCH223',
		'2013PCH208',
		'2013PCH121',
		'2013PCH041',
		'2013PCH176R',
		'2013PCH109R',
		'2013PCH134R',
		'2013PCH186R',
		'2013PCH201R',
		'2013PCH026',
		'2013PCH043',
		'2013PCH212',
		'2013PCH074'
	)
	OR [t].[Refnum] IS NULL);
--===================================================
INSERT INTO [cons].[Assets]
(
	[AssetIDPri],
	[AssetName],
	[AssetDetail],
	[CountryID]
)
SELECT
	[s].[AssetIDPri],
		[AssetName]		= COALESCE([s].[AssetNameReport], [s].[Loc]),
		[AssetDetail]	= COALESCE([s].[AssetNameReport], [s].[Loc]),
	[s].[CountryId]
FROM
	@Subscriptions		[s]
LEFT OUTER JOIN
	[cons].[Assets]		[a]
		ON	[a].[AssetIDPri]	= [s].[AssetIDPri]
WHERE
	[a].[AssetIDPri]	IS NULL;
--===================================================
INSERT INTO [dim].[Company_LookUp]
(
	[CompanyId],
	[CompanyName],
	[CompanyDetail]
)
SELECT DISTINCT
	[s].[CompanyId],
		[CompanyName]	= COALESCE([s].[Co], [s].[CompanyId]),
		[CompanyDetail]	= COALESCE([s].[Co], [s].[CompanyId])
FROM
	@Subscriptions				[s]
LEFT OUTER JOIN
	[dim].[Company_LookUp]		[c]
		ON	[c].[CompanyId]	= [s].[CompanyId]
WHERE
	[c].[CompanyId]	IS NULL;
--===================================================
MERGE [cons].[SubscriptionsCompanies]	AS [t]
USING
(
	SELECT DISTINCT
		[s].[CompanyId],
			[CalDateKey]				= @CalDateKey,
			[StudyID]					= @StudyId,
			[SubscriberCompanyName]		= [s].[Co],
			[SubscriberCompanyDetail]	= [s].[Co],
			[CompanyNameReport]			= [s].[Co],
		[s].[CompanyPassWord]
	FROM
		@Subscriptions				[s]
)										AS [s]
ON	(	[t].[CompanyID]		= [s].[CompanyID]
	AND	[t].[CalDateKey]	= [s].[CalDateKey]
	AND	[t].[StudyID]		= [s].[StudyID])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[SubscriberCompanyName],
	[SubscriberCompanyDetail],
	[CompanyNameReport],
	[PassWord_PlainText]
)
VALUES
(
	[s].[CompanyId],
	[s].[CalDateKey],
	[s].[StudyId],
	[s].[SubscriberCompanyName],
	[s].[SubscriberCompanyDetail],
	[s].[CompanyNameReport],
	[s].[CompanyPassWord]
)
WHEN MATCHED AND
	(	[t].[SubscriberCompanyName]		<> [s].[SubscriberCompanyName]
	OR	[t].[SubscriberCompanyDetail]	<> [s].[SubscriberCompanyDetail]
	OR	[t].[CompanyNameReport]			<> [s].[CompanyNameReport]
	OR	[t].[PassWord_PlainText]		<> [s].[CompanyPassWord]
	OR	[t].[SubscriberCompanyName]		IS NULL
	OR	[t].[SubscriberCompanyDetail]	IS NULL
	OR	[t].[CompanyNameReport]			IS NULL
	OR	[t].[PassWord_PlainText]		IS NULL) THEN
UPDATE
SET
	[t].[SubscriberCompanyName]		= [s].[SubscriberCompanyName],
	[t].[SubscriberCompanyDetail]	= [s].[SubscriberCompanyDetail],
	[t].[CompanyNameReport]			= [s].[CompanyNameReport],
	[t].[PassWord_PlainText]		= [s].[CompanyPassWord];
--===================================================
MERGE [cons].[SubscriptionsAssets]		AS [t]
USING
(
	SELECT
		[s].[CompanyId],
			[CalDateKey]				= @CalDateKey,
			[StudyID]					= @StudyId,
		[s].[AssetIDPri],
			[SubscriberAssetName]		= [s].[Loc],
			[SubscriberAssetDetail]		= [s].[Loc],
		[s].[Loc]
	FROM
		@Subscriptions				[s]
)										AS [s]
ON	(	[t].[CompanyId]		= [s].[CompanyId]
	AND	[t].[CalDateKey]	= [s].[CalDateKey]
	AND	[t].[StudyID]		= [s].[StudyID]
	AND	[t].[AssetID]		= [s].[AssetIDPri])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[AssetID],
	[SubscriberAssetName],
	[SubscriberAssetDetail],
	[AssetNameReport]
)
VALUES
(
	[s].[CompanyID],
	[s].[CalDateKey],
	[s].[StudyID],
	[s].[AssetIDPri],
	[s].[SubscriberAssetName],
	[s].[SubscriberAssetDetail],
	[s].[Loc]
)
WHEN MATCHED AND 
	(	[t].[CompanyId]		<> [s].[CompanyId]
	OR	[t].[CalDateKey]	<> [s].[CalDateKey]
	OR	[t].[StudyID]		<> [s].[StudyID]
	OR	[t].[AssetID]		<> [s].[AssetIDPri]
	OR	[t].[CompanyId]		IS NULL
	OR	[t].[CalDateKey]	IS NULL
	OR	[t].[StudyID]		IS NULL
	OR	[t].[AssetID]		IS NULL) THEN
UPDATE
SET
	[t].[CompanyId]		= [s].[CompanyId],
	[t].[CalDateKey]	= [s].[CalDateKey],
	[t].[StudyID]		= [s].[StudyID],
	[t].[AssetID]		= [s].[AssetIDPri];
--===================================================
MERGE [cons].[TSortSolomon]				AS [t]
USING
(
	SELECT
			[Refnum]		= CONVERT(CHAR(4), @StudyYear) + @StudyId + [s].[AssetIDPri],
			[CalDateKey]	= @CalDateKey,
			[FactorSetId]	= @FactorSetId,
		[s].[CompanyId]
	FROM
		@Subscriptions	[s]
)											AS [s]
ON	([t].[Refnum]	= [s].[Refnum])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[Refnum],
	[CalDateKey],
	[FactorSetId],
	[ContactCode]
)
VALUES
(
	[s].[Refnum],
	[s].[CalDateKey],
	[s].[FactorSetId],
	[s].[CompanyId]
)
WHEN MATCHED AND 
	(	[t].[CalDateKey]	<> [s].[CalDateKey]
	OR	[t].[FactorSetId]	<> [s].[FactorSetId]
	OR	[t].[ContactCode]	<> [s].[CompanyId]
	OR	[t].[CalDateKey]	IS NULL
	OR	[t].[FactorSetId]	IS NULL
	OR	[t].[ContactCode]	IS NULL) THEN
UPDATE
SET
	[t].[CalDateKey]	= [s].[CalDateKey],
	[t].[FactorSetId]	= [s].[FactorSetId],
	[t].[ContactCode]	= [s].[CompanyId];
--===================================================
MERGE [dbo].[CoContactInfo]				AS [t]
USING
(
	SELECT DISTINCT
		[s].[CompanyId],
			[StudyYear]		= @StudyYear,
			[ContactType]	= 'COORD',
			[FirstName]		= LEFT([s].[Coordinator], CHARINDEX(' ', [s].[Coordinator]) - 1),
			[LastName]		= RIGHT([s].[Coordinator], LEN([s].[Coordinator]) - CHARINDEX(' ', [s].[Coordinator])),
		[s].[EMail]
	FROM
		@Subscriptions			[s]
)										AS [s]
ON	(	[t].[ContactCode]	= [s].[CompanyId]
	AND	[t].[StudyYear]		= [s].[StudyYear]
	AND	[t].[ContactType]	= [s].[ContactType])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[ContactCode],
	[StudyYear],
	[ContactType],
	[FirstName],
	[LastName],
	[Email]
)
VALUES
(
	[s].[CompanyId],
	[s].[StudyYear],
	[s].[ContactType],
	[s].[FirstName],
	[s].[LastName],
	[s].[Email]
)
WHEN MATCHED AND
	(	[t].[FirstName]	<> [s].[FirstName]
	OR	[t].[LastName]	<> [s].[LastName]
	OR	[t].[Email]		<> [s].[Email]
	OR	[t].[FirstName]	IS NULL
	OR	[t].[LastName]	IS NULL
	OR	[t].[Email]		IS NULL) THEN
UPDATE
SET
	[FirstName]	= [s].[FirstName],
	[LastName]	= [s].[LastName],
	[Email]		= [s].[Email];
--===================================================
DECLARE @CheckList TABLE
(
	[IssueID]			CHAR(8)				NOT	NULL	CHECK([IssueID] <> ''),
	[IssueTitle]		CHAR(50)			NOT	NULL	CHECK([IssueTitle] <> ''),
	[IssueText]			TEXT				NOT	NULL,
	PRIMARY KEY CLUSTERED ([IssueID] ASC),
	UNIQUE NONCLUSTERED([IssueTitle] ASC)
);

INSERT INTO @CheckList
(
	[IssueID],
	[IssueTitle],
	[IssueText]
)
SELECT
	[t].[IssueID],
	[t].[IssueTitle],
	[t].[IssueText]
FROM (VALUES
('A1', 'OSIM received (JSJ)', 'Check when OSIM is received, acknowledged, dragged to Consol (Triggers OSIM is ready to unpack)'),
('A2', 'OSIM unpacked (Prog)', 'Check when OSIM is unpacked, uploaded, calced (Triggers OSIM is ready for PYPS/SPSL & DV)'),
('A3', 'DV Consultant assigned (JSJ)', 'Check when DV Con assignment is recorded in Console for plant.'),
('C1', 'Scrubber has been run (DV)', 'Check after you have run Scrubber.'),
('G1', 'EDC/EII reviewed (DV)', 'Check when EDC/EII/*EI have been reviewed.'),
('G2', 'Indicator Trends (DV)', 'Trends have been sent for comment.'),
('P1', 'PYPS is ready to run (DV)', 'Check when the data required for PYPS is complete and you are ready for JSJ to run PYPS.'),
('P2', 'PYPS is ready to be uploaded (JSJ)', 'Check after a sucessful PYPS run and you wish to have PYPS uploaded and CALCS initiated.'),
('S1', 'SPSL is ready to run (DV)', 'Check when the data required for SPSL is complete and you are ready for JSJ to run SPSL.'),
('S2', 'SPSL is ready to be uploaded (JSJ)', 'Check after a sucessful SPSL run and you wish to have SPSL uploaded and CALCS initiated.'),
('V1', 'First stage validation complete (DV)', 'Check after first stage validation is complete'),
('V2', 'Validation Polishing completed (DV)', 'Check when polishing and ranging are completed'),
('V3', 'Final validation completed (DV-2/PM)', 'Check when final validation is completed')
) [t] ( [IssueID], [IssueTitle], [IssueText])
--------------------------------------------------------------------------------------------
MERGE [val].[Checklist]					AS [t]
USING
(
	SELECT
		[Refnum]		= CONVERT(CHAR(4), @StudyYear) + @StudyId + [s].[AssetIDPri],
	[c].[IssueID],
	[c].[IssueTitle],
	[c].[IssueText],
		[PostedBy]		= '-',
		[PostedTime]	= SYSDATETIME(),
		[Completed]		= 'N',
		[SetBy]			= NULL,
		[SetTime]		= NULL

	FROM
		@Subscriptions		[s]
	CROSS JOIN
		@CheckList			[c]
)										AS [s]
ON	(	[t].[Refnum]	= [s].[Refnum]
	AND	[t].[IssueID]	= [s].[IssueID])
WHEN NOT MATCHED BY TARGET THEN
INSERT 
(
	[Refnum],
	[IssueID],
	[IssueTitle],
	[IssueText],
	[PostedBy],
	[PostedTime],
	[Completed],
	[SetBy],
	[SetTime]	
)
VALUES
(
	[s].[Refnum],
	[s].[IssueID],
	[s].[IssueTitle],
	[s].[IssueText],
	[s].[PostedBy],
	[s].[PostedTime],
	[s].[Completed],
	[s].[SetBy],
	[s].[SetTime]
)
WHEN MATCHED AND ([t].[IssueTitle] <> [s].[IssueTitle]) THEN
UPDATE
SET
	[IssueTitle]	= [s].[IssueTitle],
	[IssueText]		= [s].[IssueText];

--For testing
--SELECT 
--	[Subscriptions]	= (SELECT COUNT(1) FROM @Subscriptions),
--	[Tsort]			= (SELECT COUNT(1) FROM [OlefinsDev20151102].[cons].[TSort] [t] WHERE [t].[StudyYear] = @StudyYear);
