﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSelModelPenalty(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string penaltyId;
			RangeReference rr;

			Dictionary<string, RangeReference> feedSelPenalty = FeedSelPenaltyDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSelModelPenalty]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								penaltyId = rdr.GetString(rdr.GetOrdinal("PenaltyID"));

								if (feedSelPenalty.TryGetValue(penaltyId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;
									c = rr.col;
									AddRangeValues(wks, r, c, rdr, "Penalty_YN");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSelModelPenalty", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSelModelPenalty]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> FeedSelPenaltyDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("FeedSlateChange", new RangeReference(T11_01, 156, 10));
			d.Add("Reliability", new RangeReference(T11_01, 157, 10));
			d.Add("Envrionment", new RangeReference(T11_01, 158, 10));
			d.Add("FurnRunLength", new RangeReference(T11_01, 159, 10));
			d.Add("Maintenance", new RangeReference(T11_01, 160, 10));
			d.Add("Inventory", new RangeReference(T11_01, 161, 10));

			return d;
		}
	}
}