﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSelPractices(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string streamId;

			Dictionary<string, int> feedSelStream = FeedSelStreamColDictionary();
			Dictionary<string, int> feedSelPrac = FeedSelPracRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + T11_01];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSelPractices]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));

								if (feedSelStream.TryGetValue(streamId, out c))
								{
									c = c + 3;

									foreach (KeyValuePair<string, int> entry in feedSelPrac)
									{
										r = entry.Value;
										AddRangeValues(wks, r, c, rdr, entry.Key);
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSelPractices", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSelPractices]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> FeedSelPracRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Mix_Y", 51);
			d.Add("Blend_Y", 53);
			d.Add("Fractionation_Y", 54);
			d.Add("HydroTreat_Y", 55);
			d.Add("Purification_Y", 57);

			d.Add("MinConsidDensity_SG", 62);
			d.Add("MaxConsidDensity_SG", 63);
			d.Add("MinActualDensity_SG", 64);
			d.Add("MaxActualDensity_SG", 65);

			return d;
		}
	}
}