﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ApcExistance(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string apcId;
			RangeReference rr;

			Dictionary<string, RangeReference> apcExistance = ApcExistanceDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ApcExistance]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								apcId = rdr.GetString(rdr.GetOrdinal("ApcId"));

								if(apcExistance.TryGetValue(apcId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;

									c = 8;
									AddRangeValues(wks, r, c, rdr, "Mpc_X");

									c = 10;
									AddRangeValues(wks, r, c, rdr, "Sep_X");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ApcExistance", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ApcExistance]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> ApcExistanceDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("ApcStartStop", new RangeReference(T09_04, 42, 0));
			d.Add("ApcDecokeAutomatic", new RangeReference(T09_04, 44, 0));
			d.Add("ApcDecokeOperator", new RangeReference(T09_04, 47, 0));
			d.Add("ApcThroughputIndivid", new RangeReference(T09_04, 49, 0));
			d.Add("ApcThroughputTotal", new RangeReference(T09_04, 53, 0));

			d.Add("ApcClosedLoop", new RangeReference(T09_05, 11, 0));
			d.Add("ApcCoilOutlet", new RangeReference(T09_05, 14, 0));
			d.Add("ApcFiringConstraint", new RangeReference(T09_05, 16, 0));
			d.Add("ApcExcessAir", new RangeReference(T09_05, 19, 0));
			d.Add("ApcFiringDuty", new RangeReference(T09_05, 22, 0));
			d.Add("ApcBalancing", new RangeReference(T09_05, 24, 0));
			d.Add("ApcSteamHydrocarbon", new RangeReference(T09_05, 26, 0));
			d.Add("ApcRecycleFlowRate", new RangeReference(T09_05, 28, 0));

			d.Add("ApcSuction", new RangeReference(T09_05, 38, 0));
			d.Add("ApcSurgeMargin", new RangeReference(T09_05, 40, 0));
			d.Add("ApcCompLoad", new RangeReference(T09_05, 44, 0));
			d.Add("ApcSurge", new RangeReference(T09_05, 46, 0));
			d.Add("ApcCooling", new RangeReference(T09_05, 49, 0));
			d.Add("ApcCompConstraint", new RangeReference(T09_05, 53, 0));

			d.Add("ApcHotWater", new RangeReference(T09_06, 10, 0));
			d.Add("ApcPyroGasBP", new RangeReference(T09_06, 13, 0));
			d.Add("ApcStrippingSteamRatio", new RangeReference(T09_06, 15, 0));

			d.Add("ApcDemethComposition", new RangeReference(T09_06, 27, 0));
			d.Add("ApcDemethThroughput", new RangeReference(T09_06, 29, 0));
			d.Add("ApcDeethComposition", new RangeReference(T09_06, 32, 0));
			d.Add("ApcDeethThroughput", new RangeReference(T09_06, 34, 0));
			d.Add("ApcFracEthyleneComposition", new RangeReference(T09_06, 36, 0));
			d.Add("ApcFracEthyleneThroughput", new RangeReference(T09_06, 38, 0));
			d.Add("ApcRefrig", new RangeReference(T09_06, 41, 0));
			d.Add("ApcRefrigCompSurgeMargin", new RangeReference(T09_06, 44, 0));
			d.Add("ApcRefrigCompSurge", new RangeReference(T09_06, 47, 0));
			d.Add("ApcC3Recovery", new RangeReference(T09_06, 49, 0));
			d.Add("ApcC3RecoveryThroughput", new RangeReference(T09_06, 51, 0));
			d.Add("ApcSteamNaphthaComposition", new RangeReference(T09_06, 53, 0));
			d.Add("ApcSteamNaphthaThroughput", new RangeReference(T09_06, 56, 0));
			d.Add("ApcPressureMin", new RangeReference(T09_06, 58, 0));

			return d;
		}
	}
}