﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		//	0				1			2					3					4
		//Refnum		CompanyId		AssetName		NameFull			NameTitle
		//2013PCH011	ExxonMobil		BAYTOWN BOP		Brandon Jonas		Strategic Business Analyst

		//	5				6								7				8				9					10					11				12				13
		//AddressPOBox	AddressStreet					AddressCity		AddressState	AddressCountryId	AddressPostalCode	NumberVoice		NumberFax		eMail
		//NULL			3525 Decker Drive Office 415	Baytown			Texas			USA					77520				281-834-6333	281-834-6414	brandon.e.jonas@exxonmobil.com

		//	14						15									16				17							18
		//PricingName			PrincingEMail						DataName		DataEMail						UomID
		//Nathan Michalik		nathan.j.michalik@exxonmobil.com	Kermit J Dow	kermit.j.dow@exxonmobil.com		US

		//	19				20					21
		//StudyYear		PreviousRefnum		PreviousStudyYear
		//2013			2011PCH011			2011

		public void PlantName(Excel.Workbook wkb, string tabNamePrefix, string refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 4;

			string pRefnum;
			int uom;

			Dictionary<string, string> cCurrency = CurrencyCountryDictionary();

			try
			{
				wks = wkb.Worksheets["PlantName"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_PlantName]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								AddRangeValues(wks, 6, c, rdr, "CompanyID");
								AddRangeValues(wks, 7, c, rdr, "AssetName");

								AddRangeValues(wks, 12, c, rdr, "NameFull");
								AddRangeValues(wks, 13, c, rdr, "NameTitle");
								AddRangeValues(wks, 14, c, rdr, "AddressPOBox");
								AddRangeValues(wks, 15, c, rdr, "AddressStreet");
								AddRangeValues(wks, 16, c, rdr, "AddressCity");
								AddRangeValues(wks, 17, c, rdr, "AddressState");
								AddRangeValues(wks, 18, c, rdr, "AddressCountryId");
								AddRangeValues(wks, 19, c, rdr, "AddressPostalCode");
								AddRangeValues(wks, 20, c, rdr, "NumberVoice");
								AddRangeValues(wks, 21, c, rdr, "NumberFax");
								AddRangeValues(wks, 22, c, rdr, "eMail");
								AddRangeValues(wks, 23, c, rdr, "PricingName");
								AddRangeValues(wks, 24, c, rdr, "PrincingEMail");
								AddRangeValues(wks, 25, c, rdr, "DataName");
								AddRangeValues(wks, 26, c, rdr, "DataEMail");

								rng = wks.Cells[29, c];
								rng.Value2 = refnum.Substring(2);

								rng = wks.Cells[46, c];
								rng.Value2 = refnum;

								if (!rdr.IsDBNull(rdr.GetOrdinal("PreviousRefnum")))
								{
									pRefnum = rdr.GetString(rdr.GetOrdinal("PreviousRefnum")).Trim(); ;

									rng = wks.Cells[30, c];
									rng.Value2 = pRefnum.Substring(2);

									rng = wks.Cells[47, c];
									rng.Value2 = pRefnum;

									rng = wks.Cells[49, c];
									rng.Value2 = rdr.GetValue(rdr.GetOrdinal("PreviousStudyYear"));
								}

								rng = wks.Cells[48, c];
								rng.Value2 = rdr.GetValue(rdr.GetOrdinal("StudyYear"));

								string countryName;
								string currencyId = rdr.GetString(rdr.GetOrdinal("CurrencyFcn")).Trim();

								if (cCurrency.TryGetValue(currencyId, out countryName))
								{
									wks = wkb.Worksheets[tabNamePrefix + "4"];
									wks.Cells[4, 6].Value2 = countryName;
								}

								uom = (rdr.GetString(rdr.GetOrdinal("UomId")).Trim() == "US") ? 1 : 2;
								wks = wkb.Worksheets["Conv"];
								wks.Cells[2, 2].Value2 = uom;
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "PopulatePlantName", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_PlantName]", ex);
			}
			finally
			{
				wkb.Parent.CalculateFull();
				rng = null;
				wks = null;
			}
		}

		private struct RangeReference
		{
			public string sheet;
			public string rng;

			public int row;
			public int col;

			public RangeReference(string sheetName, int rowNumber, int colNumber)
			{
				this.sheet = sheetName;
				this.row = rowNumber;
				this.col = colNumber;
				this.rng = string.Empty;
			}
		}
	}
}