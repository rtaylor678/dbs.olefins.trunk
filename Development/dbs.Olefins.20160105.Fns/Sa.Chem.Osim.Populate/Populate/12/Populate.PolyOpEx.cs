﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void PolyOpEx(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string traindId;
			string accountId;
			RangeReference rr;

			Dictionary<string, int> polyCol = PolyTrainColDictionary();
			Dictionary<string, RangeReference> polyOpEx = PolyOpExDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_PolyOpEx]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								traindId = rdr.GetString(rdr.GetOrdinal("AssetId"));
								traindId = traindId.Substring(traindId.Length - 2);
								
								if (polyCol.TryGetValue(traindId, out c))
								{
									accountId = rdr.GetString(rdr.GetOrdinal("AccountId"));

									if (polyOpEx.TryGetValue(accountId, out rr))
									{
										wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
										r = rr.row;
										AddRangeValues(wks, r, c, rdr, "Amount_Cur");
									}
								}
							}
						}
					}
					cn.Close();
				};
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "PolyOpEx", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_PolyOpEx]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> PolyOpExDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("Maint", new RangeReference(T12_01, 18, 0));
			d.Add("TACurrExp", new RangeReference(T12_01, 19, 0));
			d.Add("OtherNonVol", new RangeReference(T12_01, 20, 0));

			d.Add("Catalysts", new RangeReference(T12_01, 23, 0));
			d.Add("ChemAdd", new RangeReference(T12_01, 24, 0));
			d.Add("ChemOth", new RangeReference(T12_01, 25, 0));
			d.Add("Royalties", new RangeReference(T12_01, 26, 0));
			d.Add("PurchasedEnergy", new RangeReference(T12_01, 27, 0));
			d.Add("PurOth", new RangeReference(T12_01, 28, 0));
			d.Add("PackagingMatlSvcs", new RangeReference(T12_01, 29, 0));
			d.Add("OtherVol", new RangeReference(T12_01, 30, 0));

			return d;
		}
	}
}