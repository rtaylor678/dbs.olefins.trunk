﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityTrainTurnaround(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			int trainId;
			string schedId;
			int offset;

			Dictionary<int, int> relTrain = RelTrainDictionary();
			Dictionary<string, int> plannedOffset = RelPlannedOffsetDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-3"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityTrainTurnaround]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								trainId = rdr.GetInt32(rdr.GetOrdinal("TrainId"));
								
								if(relTrain.TryGetValue(trainId, out c))
								{
									r = 6;
									AddRangeValues(wks, r, c, rdr, "Capacity_Pcnt", 100.0);
		
									schedId = rdr.GetString(rdr.GetOrdinal("SchedId"));

									if (plannedOffset.TryGetValue(schedId, out offset))
									{
										r = 7;
										AddRangeValues(wks, r + offset, c, rdr, "Interval_Mnths");

										r = 8;
										AddRangeValues(wks, r + offset, c, rdr, "DownTime_Hrs");

										r = 11;
										AddRangeValues(wks, r, c, rdr, "TurnAroundCost_MCur");

										r = 15;
										AddRangeValues(wks, r, c, rdr, "TurnAround_ManHrs");

										r = 18;
										AddRangeValues(wks, r, c, rdr, "TurnAround_Date");

										r = 20;
										AddRangeValues(wks, r, c, rdr, "MiniTA_YN");

										r = 21;
										AddRangeValues(wks, r, c, rdr, "MiniTA_DownTime_Hrs");

										r = 22;
										AddRangeValues(wks, r, c, rdr, "MiniTACost_MCur");

										r = 23;
										AddRangeValues(wks, r, c, rdr, "MiniTA_ManHrs");

										r = 24;
										AddRangeValues(wks, r, c, rdr, "MiniTA_Date");
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityTrainTurnaround", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityTrainTurnaround]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<int, int> RelTrainDictionary()
		{
			Dictionary<int, int> d = new Dictionary<int, int>();

			d.Add(1, 6);
			d.Add(2, 7);
			d.Add(3, 8);

			return d;
		}

		private Dictionary<string, int> RelPlannedOffsetDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("P", 0);
			d.Add("A", 2);

			return d;
		}
	}
}