﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityCriticalPath(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string facilityId;

			Dictionary<string, int> oppLossPathRow = OppLossCritPathDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-2"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityCriticalPath]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								facilityId = rdr.GetString(rdr.GetOrdinal("FacilityID"));

								if (oppLossPathRow.TryGetValue(facilityId, out r))
								{
									AddRangeValues(wks, r, 5, rdr, "CritPath_X");
									AddRangeValues(wks, r, 6, rdr, "FacilityName");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityCriticalPath", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityCriticalPath]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> OppLossCritPathDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("CompGas", 11);
			d.Add("CompRefrig", 12);
			d.Add("PyroFurnTLE", 13);
			d.Add("Exchanger", 14);
			d.Add("Vessel", 15);
			d.Add("PyroFurn", 16);
			d.Add("Utilities", 17);
			d.Add("CritPathCapProj", 18);
			d.Add("CritPathOther", 19);

			return d;
		}
	}
}