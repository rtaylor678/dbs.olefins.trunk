﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ReliabilityOppLoss(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string oppLossID;
			int currentYear;

			Dictionary<string, int> oppLossRow = OppLossRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "6-1"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ReliabilityOppLoss]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								oppLossID = rdr.GetString(rdr.GetOrdinal("OppLossID"));

								if (oppLossRow.TryGetValue(oppLossID, out r))
								{
									currentYear = rdr.GetInt32(rdr.GetOrdinal("CurrentYear"));

									AddRangeValues(wks, r, 6 + (currentYear * 2), rdr, "DownTimeLoss_MT");
									AddRangeValues(wks, r, 7 + (currentYear * 2), rdr, "SlowDownLoss_MT");

									AddRangeValues(wks, r, 2, rdr, "OppLossName");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ReliabilityOppLoss", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ReliabilityOppLoss]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> OppLossRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("TurnAround", 10);

			d.Add("OperError", 12);
			d.Add("IntFeedInterrupt", 13);

			d.Add("FFPCause1", 15);
			d.Add("FFPCause2", 16);
			d.Add("FFPCause3", 17);
			d.Add("FFPCause4", 18);

			d.Add("Transition", 19);
			d.Add("AcetyleneConv", 20);

			d.Add("ProcessOther1", 23);
			d.Add("ProcessOther2", 24);
			d.Add("ProcessOther3", 25);
			d.Add("ProcessOther4", 26);

			d.Add("FurnaceProcess", 27);

			d.Add("Compressor", 29);
			d.Add("OtherRotating", 30);
			d.Add("FurnaceMech", 31);
			d.Add("Corrosion", 33);
			d.Add("ElecDist", 34);
			d.Add("ControlSys", 35);
			d.Add("OtherFailure", 36);

			d.Add("ExtElecFailure", 38);
			d.Add("ExtOthUtilFailure", 39);
			d.Add("IntElecFailure", 40);
			d.Add("IntSteamFailure", 41);
			d.Add("IntOthUtilFailure", 42);

			d.Add("Demand", 44);
			d.Add("ExtFeedInterrupt", 45);
			d.Add("CapProject", 47);
			d.Add("Strikes", 48);
			d.Add("FeedMix", 49);
			d.Add("Severity", 50);

			d.Add("OtherNonOp1", 52);
			d.Add("OtherNonOp2", 53);
			d.Add("OtherNonOp3", 54);
			d.Add("OtherNonOp4", 55);

			return d;
		}
	}
}