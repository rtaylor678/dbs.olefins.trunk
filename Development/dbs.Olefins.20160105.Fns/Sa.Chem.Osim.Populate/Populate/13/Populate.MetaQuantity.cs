﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MetaQuantity(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string streamId;
			RangeReference rr;

			Dictionary<string, RangeReference> metaQuant = MetaQuantDictionary();
			Dictionary<string, int> metaCol = MetaColDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MetaQuantity]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamID"));

								if (metaQuant.TryGetValue(streamId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;

									foreach (KeyValuePair<string, int> entry in metaCol)
									{
										c = entry.Value;
										AddRangeValues(wks, r, c, rdr, entry.Key);
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MetaQuantity", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MetaQuantity]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> MetaQuantDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("MetaOCEthylene", new RangeReference(T13_01, 15, 0));
			d.Add("MetaOCButene1", new RangeReference(T13_01, 16, 0));
			d.Add("MetaOCButene2", new RangeReference(T13_01, 17, 0));
			d.Add("MetaOCButanes", new RangeReference(T13_01, 18, 0));
			d.Add("MetaOCOther", new RangeReference(T13_01, 19, 0));

			d.Add("MetaPurchEthylene", new RangeReference(T13_01, 20, 0));
			d.Add("MetaPurchButene1", new RangeReference(T13_01, 21, 0));
			d.Add("MetaPurchButene2", new RangeReference(T13_01, 22, 0));
			d.Add("MetaPurchButane", new RangeReference(T13_01, 23, 0));
			d.Add("MetaPurchOther", new RangeReference(T13_01, 24, 0));

			d.Add("MetaProdPropylenePG", new RangeReference(T13_01, 31, 0));
			d.Add("MetaProdUnReactLightPurge", new RangeReference(T13_01, 32, 0));
			d.Add("MetaProdUnReactButane", new RangeReference(T13_01, 33, 0));
			d.Add("MetaProdC5", new RangeReference(T13_01, 34, 0));

			return d;
		}

		private Dictionary<string, int> MetaColDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Q1", 6);
			d.Add("Q2", 7);
			d.Add("Q3", 8);
			d.Add("Q4", 9);

			return d;
		}
	}
}