﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MetaEnergy(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string accountId;
			RangeReference rr;

			Dictionary<string, RangeReference> metaEnergy = MetaEnergyDictionary();
			Dictionary<string, int> metaCol = MetaColDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MetaEnergy]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("AccountId"));

								if (metaEnergy.TryGetValue(accountId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;

									foreach (KeyValuePair<string, int> entry in metaCol)
									{
										c = entry.Value;
										AddRangeValues(wks, r, c, rdr, entry.Key);
									}
								}
							}
						}
					}
					cn.Close();
				};
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MetaEnergy", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MetaEnergy]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}


		private Dictionary<string, RangeReference> MetaEnergyDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("MetaEnergy", new RangeReference(T13_01, 53, 0));

			return d;
		}
	}
}