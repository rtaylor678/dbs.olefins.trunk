﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void GenPlantEnergy(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			const int c = 5;

			Dictionary<string, int> genEnergy = GenPlantEnergyRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "8-4"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_GenPlantEnergy]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								foreach (KeyValuePair<string, int> entry in genEnergy)
								{
									r = entry.Value;
									AddRangeValues(wks, r, c, rdr, entry.Key);
								}

								r = 61;
								AddRangeValues(wks, r, c, rdr, "QuenchHeatMatl_Pcnt", 100.0);

								r = 64;
								AddRangeValues(wks, r, c, rdr, "QuenchDilution_Pcnt", 100.0);
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "GenPlantEnergy", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_GenPlantEnergy]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> GenPlantEnergyRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("CoolingWater_MTd", 36);
			d.Add("CoolingWaterSupplyTemp_C", 39);
			d.Add("CoolingWaterReturnTemp_C", 40);
			d.Add("FinFanHeatAbsorb_kBTU", 43);
			d.Add("CompGasEfficiency_Pcnt", 46);
			d.Add("CalcTypeID", 49);
			d.Add("SteamExtratRate_kMTd", 52);
			d.Add("SteamCondenseRate_kMTd", 53);
			d.Add("ExchangerHeatDuty_BTU", 58);
			
			return d;
		}
	}
}