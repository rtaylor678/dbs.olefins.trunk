﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MaintExpense(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string accountId;
			RangeReference rr;

			Dictionary<string, RangeReference> maintExp = MaintExpDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MaintExpense]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("AccountId"));

								if (maintExp.TryGetValue(accountId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;
									c = rr.col;
									AddRangeValues(wks, r, c, rdr, "CompMaintHours_Pcnt", 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MaintExpense", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MaintExpense]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> MaintExpDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("MaintCorrEmergancy", new RangeReference(T10_02, 36, 9));
			d.Add("MaintCorrProcess", new RangeReference(T10_02, 37, 9));
			d.Add("MaintPrevCond", new RangeReference(T10_02, 39, 9));
			d.Add("MaintPrevCondAdd", new RangeReference(T10_02, 40, 9));
			d.Add("MaintPrevSys", new RangeReference(T10_02, 41, 9));
			d.Add("MaintPrevSysAdd", new RangeReference(T10_02, 42, 9));

			return d;
		}
	}
}