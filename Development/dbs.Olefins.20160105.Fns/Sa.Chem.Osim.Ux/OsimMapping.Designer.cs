﻿namespace Sa.Chem
{
    partial class OsimMapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStudyYear = new System.Windows.Forms.Label();
            this.dgMapping = new System.Windows.Forms.DataGridView();
            this.gbMapping = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtWorkbookNameInsert = new System.Windows.Forms.TextBox();
            this.pnlEditAdd = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEndCol = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtStartCol = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtWorksheetNameInsert = new System.Windows.Forms.TextBox();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).BeginInit();
            this.gbMapping.SuspendLayout();
            this.pnlEditAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblStudyYear
            // 
            this.lblStudyYear.AutoSize = true;
            this.lblStudyYear.Location = new System.Drawing.Point(87, 9);
            this.lblStudyYear.Name = "lblStudyYear";
            this.lblStudyYear.Size = new System.Drawing.Size(0, 13);
            this.lblStudyYear.TabIndex = 0;
            // 
            // dgMapping
            // 
            this.dgMapping.AllowUserToAddRows = false;
            this.dgMapping.AllowUserToDeleteRows = false;
            this.dgMapping.AllowUserToOrderColumns = true;
            this.dgMapping.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgMapping.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMapping.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgMapping.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgMapping.Location = new System.Drawing.Point(3, 16);
            this.dgMapping.MultiSelect = false;
            this.dgMapping.Name = "dgMapping";
            this.dgMapping.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgMapping.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMapping.Size = new System.Drawing.Size(1066, 323);
            this.dgMapping.TabIndex = 18;
            // 
            // gbMapping
            // 
            this.gbMapping.Controls.Add(this.dgMapping);
            this.gbMapping.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMapping.Location = new System.Drawing.Point(12, 12);
            this.gbMapping.Name = "gbMapping";
            this.gbMapping.Size = new System.Drawing.Size(1072, 342);
            this.gbMapping.TabIndex = 19;
            this.gbMapping.TabStop = false;
            this.gbMapping.Text = "Mapping";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(73, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Workbook Name :";
            // 
            // txtWorkbookNameInsert
            // 
            this.txtWorkbookNameInsert.Location = new System.Drawing.Point(199, 14);
            this.txtWorkbookNameInsert.Name = "txtWorkbookNameInsert";
            this.txtWorkbookNameInsert.Size = new System.Drawing.Size(244, 20);
            this.txtWorkbookNameInsert.TabIndex = 20;
            // 
            // pnlEditAdd
            // 
            this.pnlEditAdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEditAdd.Controls.Add(this.btnClose);
            this.pnlEditAdd.Controls.Add(this.btnSave);
            this.pnlEditAdd.Controls.Add(this.label5);
            this.pnlEditAdd.Controls.Add(this.textBox1);
            this.pnlEditAdd.Controls.Add(this.label6);
            this.pnlEditAdd.Controls.Add(this.textBox2);
            this.pnlEditAdd.Controls.Add(this.label3);
            this.pnlEditAdd.Controls.Add(this.txtEndCol);
            this.pnlEditAdd.Controls.Add(this.label2);
            this.pnlEditAdd.Controls.Add(this.txtStartCol);
            this.pnlEditAdd.Controls.Add(this.label1);
            this.pnlEditAdd.Controls.Add(this.txtWorksheetNameInsert);
            this.pnlEditAdd.Controls.Add(this.label4);
            this.pnlEditAdd.Controls.Add(this.txtWorkbookNameInsert);
            this.pnlEditAdd.Location = new System.Drawing.Point(263, 360);
            this.pnlEditAdd.Name = "pnlEditAdd";
            this.pnlEditAdd.Size = new System.Drawing.Size(585, 197);
            this.pnlEditAdd.TabIndex = 22;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(149, 161);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(368, 161);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 32;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(275, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "End Row :";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(405, 100);
            this.textBox1.MaxLength = 4;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(42, 20);
            this.textBox1.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(72, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Start Row :";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(201, 100);
            this.textBox2.MaxLength = 4;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(42, 20);
            this.textBox2.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(275, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "End Column Name :";
            // 
            // txtEndCol
            // 
            this.txtEndCol.Location = new System.Drawing.Point(405, 69);
            this.txtEndCol.MaxLength = 2;
            this.txtEndCol.Name = "txtEndCol";
            this.txtEndCol.Size = new System.Drawing.Size(42, 20);
            this.txtEndCol.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(72, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Start Column Name :";
            // 
            // txtStartCol
            // 
            this.txtStartCol.Location = new System.Drawing.Point(201, 69);
            this.txtStartCol.MaxLength = 2;
            this.txtStartCol.Name = "txtStartCol";
            this.txtStartCol.Size = new System.Drawing.Size(42, 20);
            this.txtStartCol.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(73, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Worksheet Name :";
            // 
            // txtWorksheetNameInsert
            // 
            this.txtWorksheetNameInsert.Location = new System.Drawing.Point(200, 41);
            this.txtWorksheetNameInsert.MaxLength = 32;
            this.txtWorksheetNameInsert.Name = "txtWorksheetNameInsert";
            this.txtWorksheetNameInsert.Size = new System.Drawing.Size(141, 20);
            this.txtWorksheetNameInsert.TabIndex = 22;
            // 
            // btnInsert
            // 
            this.btnInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsert.Location = new System.Drawing.Point(656, 382);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(122, 23);
            this.btnInsert.TabIndex = 34;
            this.btnInsert.Text = "Add Record";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(314, 382);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(122, 23);
            this.btnDelete.TabIndex = 35;
            this.btnDelete.Text = "Delete Record";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(489, 382);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(122, 23);
            this.btnUpdate.TabIndex = 36;
            this.btnUpdate.Text = "Update Record";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // OsimMapping
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 579);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.pnlEditAdd);
            this.Controls.Add(this.gbMapping);
            this.Controls.Add(this.lblStudyYear);
            this.Name = "OsimMapping";
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).EndInit();
            this.gbMapping.ResumeLayout(false);
            this.pnlEditAdd.ResumeLayout(false);
            this.pnlEditAdd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStudyYear;
        private System.Windows.Forms.DataGridView dgMapping;
        private System.Windows.Forms.GroupBox gbMapping;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtWorkbookNameInsert;
        private System.Windows.Forms.Panel pnlEditAdd;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEndCol;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtStartCol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWorksheetNameInsert;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;

    }
}