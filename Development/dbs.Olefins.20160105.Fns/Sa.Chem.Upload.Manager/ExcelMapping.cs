﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace Sa.Chem.Upload.Manager
{
    [Serializable]
    public class ExcelMapping
    {

        public int ExcelMappingId { get; set; }
        public int StudyYear { get; set; }
        public string WorkbookName { get; set; }
        public string WorksheetName { get; set; }
        public string DataStartCol { get; set; }
        public string DataEndCol  { get; set; }
        public int DataStartRow { get; set; }
        public int DataEndRow { get; set; }
        public string TableRefCol { get; set; }
        public string ColFieldAndValue { get; set; }
        public string ReadType { get; set; }
        public bool Active { get; set; }

        public const string ReadTypeColumn = "Col";
        public const string ReadTypeRow = "Row";


        public List<ExcelMapping> SelectAll(bool active, string connectionString)
        {
            List<ExcelMapping> excelMappingList = new List<ExcelMapping>();

            try
            {

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[stgFact].[EmailMappingSelectAll]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                       
                        cmd.Parameters.Add("@Active", SqlDbType.Bit).Value = active;

                        conn.Open();

                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
                        {

                            if (rdr.HasRows)
                            {

                                while (rdr.Read())
                                {
                                    ExcelMapping em = new ExcelMapping();

                                    em.ExcelMappingId = rdr.GetInt32(rdr.GetOrdinal("ExcelMappingId"));
                                    em.WorkbookName = rdr.GetString(rdr.GetOrdinal("WorkbookName"));
                                    em.WorksheetName = rdr.GetString(rdr.GetOrdinal("WorksheetName"));
                                    em.DataStartCol = rdr.GetString(rdr.GetOrdinal("DataStartCol"));
                                    em.DataEndCol = rdr.GetString(rdr.GetOrdinal("DataEndCol"));
                                    em.DataStartRow = rdr.GetInt32(rdr.GetOrdinal("DataStartRow"));
                                    em.DataEndRow = rdr.GetInt32(rdr.GetOrdinal("DataEndRow"));
                                    em.TableRefCol = rdr.GetString(rdr.GetOrdinal("TableRefCol"));
                                    em.ColFieldAndValue = rdr.GetString(rdr.GetOrdinal("ColFieldAndValue"));
                                    em.ReadType = rdr.GetString(rdr.GetOrdinal("ReadType"));
                                    em.Active = rdr.GetBoolean(rdr.GetOrdinal("Active"));

                                    excelMappingList.Add(em);

                                }
                            }

                            rdr.Close();
                        }


                        conn.Close();
                    }
                }

                return excelMappingList;
            }
            catch (Exception ex)
            {
                string error = ex.Message;

                //Do some logging
                return null;
            }


        }
       
        public List<ExcelMapping> SelectByStudyYear(int studyYear, bool active, string connectionString)
        {
            List<ExcelMapping> excelMappingList = new List<ExcelMapping>();

            try
            {

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[stgFact].[EmailMappingSelectByStudyYear]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@StudyYear", SqlDbType.Int, 4).Value  = studyYear;
                        cmd.Parameters.Add("@Active", SqlDbType.Bit).Value = active;

                        conn.Open();

                        using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
                        {

                            if (rdr.HasRows)
                            {

                                while (rdr.Read())
                                {
                                    ExcelMapping em = new ExcelMapping();

                                    em.ExcelMappingId = rdr.GetInt32(rdr.GetOrdinal("ExcelMappingId"));
                                    em.WorkbookName = rdr.GetString(rdr.GetOrdinal("WorkbookName"));
                                    em.WorksheetName = rdr.GetString(rdr.GetOrdinal("WorksheetName"));
                                    em.DataStartCol = rdr.GetString(rdr.GetOrdinal("DataStartCol"));
                                    em.DataEndCol = rdr.GetString(rdr.GetOrdinal("DataEndCol"));
                                    em.DataStartRow = rdr.GetInt32(rdr.GetOrdinal("DataStartRow"));
                                    em.DataEndRow = rdr.GetInt32(rdr.GetOrdinal("DataEndRow"));
                                    em.TableRefCol = rdr.GetString(rdr.GetOrdinal("TableRefCol"));
                                    em.ColFieldAndValue = rdr.GetString(rdr.GetOrdinal("ColFieldAndValue"));
                                    em.ReadType = rdr.GetString(rdr.GetOrdinal("ReadType"));
                                    em.Active = rdr.GetBoolean(rdr.GetOrdinal("Active"));

                                    excelMappingList.Add(em);

                                }
                            }

                            rdr.Close();
                        }
                        
                        
                        conn.Close();
                    }
                }

                return excelMappingList;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                
                //Do some logging
                return null;
            }

            
        }

        public int Insert(int studyYear, string workbookName, string worksheetName, string startCol, string endCol, int startRow, int endRow, 
                                  string tableRefCol, string colFieldAndValue, string readType, bool active, string connectionString)
        {
            List<ExcelMapping> excelMappingList = new List<ExcelMapping>();
            int retVal = 0;

            try
            {

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[stgFact].[EmailMappingInsert]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@StudyYear", SqlDbType.Int, 4).Value = studyYear;
                        cmd.Parameters.Add("@WorkbookName", SqlDbType.NVarChar, 50).Value = workbookName;
                        cmd.Parameters.Add("@WorksheetName", SqlDbType.NVarChar, 32).Value = worksheetName;
                        cmd.Parameters.Add("@DataStartCol", SqlDbType.NVarChar, 2).Value = startCol;
                        cmd.Parameters.Add("@DataEndCol", SqlDbType.NVarChar, 2).Value = endCol;
                        cmd.Parameters.Add("@DataStartRow", SqlDbType.Int, 4).Value = startRow;
                        cmd.Parameters.Add("@DataEndRow", SqlDbType.Int, 4).Value = endRow;
                        cmd.Parameters.Add("@TableRefCol", SqlDbType.NVarChar, 2).Value = tableRefCol;
                        cmd.Parameters.Add("@ColFieldAndValue", SqlDbType.NVarChar, 2).Value = colFieldAndValue;
                        cmd.Parameters.Add("@ReadType", SqlDbType.NVarChar, 4).Value = readType;      
                        cmd.Parameters.Add("@Active", SqlDbType.Bit).Value = active;

                        conn.Open();

                        retVal = cmd.ExecuteNonQuery();

                        conn.Close();
                    }
                }

                return retVal;
            }
            catch (Exception ex)
            {
                string error = ex.Message;

                //Do some logging
                return -1;
            }


        }

        public int Delete(int excelMappingId, string connectionString)
        {
            List<ExcelMapping> excelMappingList = new List<ExcelMapping>();
            int retVal = 0;

            try
            {

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[stgFact].[EmailMappingDeleteById]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@ExcelMappingId", SqlDbType.Int, 4).Value = excelMappingId;
                       
                        conn.Open();

                        retVal = cmd.ExecuteNonQuery();

                        conn.Close();
                    }
                }

                return retVal;
            }
            catch (Exception ex)
            {
                string error = ex.Message;

                //Do some logging
                return -1;
            }


        }
    
        public int Delete(string workbookName, string connectionString)
        {
            List<ExcelMapping> excelMappingList = new List<ExcelMapping>();
            int retVal = 0;

            try
            {

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[stgFact].[EmailMappingDeleteByWorkbookName]", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@WorkbookName", SqlDbType.Int, 4).Value = workbookName;

                        conn.Open();

                        retVal = cmd.ExecuteNonQuery();

                        conn.Close();
                    }
                }

                return retVal;
            }
            catch (Exception ex)
            {
                string error = ex.Message;

                //Do some logging
                return -1;
            }
        }

        //public int Delete(int studyYear, string connectionString)
        //{
        //    List<ExcelMapping> excelMappingList = new List<ExcelMapping>();
        //    int retVal = 0;

        //    try
        //    {

        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {
        //            using (SqlCommand cmd = new SqlCommand("[stgFact].[EmailMappingDelete]", conn))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;

        //                cmd.Parameters.Add("@StudyYear", SqlDbType.Int, 4).Value = studyYear;

        //                conn.Open();

        //                retVal = cmd.ExecuteNonQuery();

        //                conn.Close();
        //            }
        //        }

        //        return retVal;
        //    }
        //    catch (Exception ex)
        //    {
        //        string error = ex.Message;

        //        //Do some logging
        //        return -1;
        //    }
        //}

    }
}
