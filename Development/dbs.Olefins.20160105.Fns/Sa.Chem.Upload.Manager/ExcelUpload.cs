﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace Sa.Chem.Upload.Manager
{
    public class ExcelUpload
    {
        public void DoProcess(string mappingWorkbookPath, string workbookPath, int studyYear, bool active, string connectionString)
        {
            ExcelMapping excelMapping = new ExcelMapping();
            DataTable mappingDataTable = new DataTable();

            
             Excel.Application excelApp = null;
             Excel.Workbook excelWorkbook = null;

             Excel.Application excelAppMapping = null;
             Excel.Workbook excelWorkbookMapping = null;

            try
            {
                OpenExcelWorkbooks(ref excelApp, ref excelWorkbook, ref excelAppMapping, ref excelWorkbookMapping, workbookPath, mappingWorkbookPath);

                List<ExcelMapping> excelMappingList = excelMapping.SelectByStudyYear(studyYear, active, connectionString);

                if (excelMappingList != null)
                {
                    foreach (ExcelMapping em in excelMappingList)
                    {
                        mappingDataTable = CreateMappingDatableFromExcelWorksheet(ref excelAppMapping, ref excelWorkbookMapping, em.WorksheetName, 
                                                                                        em.DataStartCol, em.DataEndCol, em.DataStartRow, em.DataEndRow, em.TableRefCol, em.ColFieldAndValue);

                        GetExcelDataAndInsert(ref excelApp, ref excelWorkbook, mappingDataTable, em);

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void OpenExcelWorkbooks(ref Excel.Application excelApp, ref Excel.Workbook excelWorkbook, ref Excel.Application excelAppMapping, 
                                                            ref Excel.Workbook excelWorkbookMapping, string workbookPath, string mappingWorkbookPath)
        {
            try
            {

                //Workbook
                excelApp = new Excel.Application();
                excelApp.Visible = false;
                //WorkBooks.open(string Filename, object UpdateLinks, object ReadOnly, object Format, object Password, object WriteResPassword,  object ReadOnlyRecommend, 
                //                  object Origin, object Delimiter,  object Editable, object Notify, object Converter, object AddToMru, object Local, object CorruptLoad )   
                excelWorkbook = excelApp.Workbooks.Open(workbookPath, 0, false, 5, "", "", true,
                                                                          Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                

                //Mapping Workbook
                excelAppMapping = new Excel.Application();
                excelAppMapping.Visible = false;


                //WorkBooks.open(string Filename, object UpdateLinks, object ReadOnly, object Format, object Password, object WriteResPassword,  object ReadOnlyRecommend, 
                //                  object Origin, object Delimiter,  object Editable, object Notify, object Converter, object AddToMru, object Local, object CorruptLoad )   
                excelWorkbookMapping = excelAppMapping.Workbooks.Open(mappingWorkbookPath, 0, false, 5, "", "", true,
                                                                            Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        private void GetExcelDataAndInsert(ref Excel.Application excelApp, ref Excel.Workbook excelWorkbook, DataTable dataTableMapping, ExcelMapping excelMapping)
        {
          
            try
            {
               
           
                Excel.Sheets excelSheets = excelWorkbook.Worksheets;
                Excel.Worksheet excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(excelMapping.WorksheetName);

                //int hits = 0; removed 7.18.18 to clear warnings

                foreach (DataRow row in dataTableMapping.Rows)
                {
                    int colIndex = 0;
                    //Console.WriteLine(row["ImagePath"]);
                    foreach (DataColumn col in dataTableMapping.Columns)
                    {
                        string value = Convert.ToString(row[col]);

                        //if (
                        
                        //if (value.Length > 0) hits = hits + 1;
                        //colIndex = colIndex + 1;
                    }
                }

                //int totalHits = hits;
                //for (int i = excelWorkbook.DataStartRow; i < excelWorkbook.DataEndRow + 1; i++)
                //{

                //    DataRow dataRow = dataTableMapping.Rows[i];
                //    //Excel.Range excelCells = (Excel.Range)excelWorksheet.get_Range(excelMapping.DataStartCol + Convert.ToString(i), excelMapping.DataEndCol + Convert.ToString(i));

                //    //for (int j = 1; j < dataTableMapping.Columns.Count; j++)
                //    //{
                //    //    if (
                //    //    Excel.Range cell = excelCells.Item[j];
                //    //    string cellValue = cell.Value2;
                //    //}
                    
                //}            
            }
            catch (Exception ex)
            {              
               string error = ex.Message;
                //throw ex;
            }
            finally
            {
               
            }

        }


        private DataTable CreateMappingDatableFromExcelWorksheet(ref Excel.Application excelApp, ref Excel.Workbook excelWorkbook, string worksheetName, 
                                                                    string startCol, string endCol, int startRow, int endRow, string tableRefCol, string colFieldAndValue)
        {

            DataTable dataTableMapping = new DataTable();
                 
            try
            {
                    
                //string values2 = string.Empty;
                AddDataTableColumns(ref dataTableMapping);

                excelApp = new Excel.Application();
                excelApp.Visible = false;

                Excel.Sheets excelSheets = excelWorkbook.Worksheets;
                Excel.Worksheet excelWorksheet = (Excel.Worksheet)excelSheets.get_Item(worksheetName);

                if (excelWorksheet == null)
                {
                    throw new Exception("Worksheet Name: " + worksheetName + " not found. Please correct.");
                }

                for (int i = startRow; i < endRow + 1; i++)
                {

                    DataRow dr = dataTableMapping.NewRow();

                    Excel.Range excelCells = (Excel.Range)excelWorksheet.get_Range(startCol + Convert.ToString(i), endCol + Convert.ToString(i));
                   

                    for (int j = 1; j < excelCells.Count + 1; j++)
                    {
                        Excel.Range cell = excelCells.Item[j];
                                     
                        //For testing
                        //values2 = values2 + dr[j].ToString() + "; ";
                        dr[j] = cell.Value2;        
                        
                    }

                    Excel.Range excelTableRefCell = (Excel.Range)excelWorksheet.get_Range(tableRefCol + Convert.ToString(i));
                    Excel.Range excelWhereClauseCell = (Excel.Range)excelWorksheet.get_Range(colFieldAndValue + Convert.ToString(i));   

                    dr[excelCells.Count + 1] = excelTableRefCell.Value2;
                    dr[excelCells.Count + 2] = excelWhereClauseCell.Value2;

                    dataTableMapping.Rows.Add(dr);
                }

                return dataTableMapping;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return null;
               
                //throw ex;
            }
            finally
            {
                CloseExcel(ref excelApp, ref excelWorkbook);
            }

        }

        private void AddDataTableColumns(ref DataTable dataTableMapping)
        {
            for (int i = 0; i < 26; i++)
            {
                dataTableMapping.Columns.Add(ColumnName(i), typeof(string));
            }
        }

        private string ColumnName(int index)
        {
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return alphabet[index].ToString();

        }

        public static void CloseExcel(ref Excel.Application xla, ref Excel.Workbook wkb)
        {
            if (wkb != null)
            {
                wkb.Close(false);
                Marshal.FinalReleaseComObject(wkb);
                wkb = null;
            }

            if (xla != null)
            {
                xla.Quit();
                Marshal.FinalReleaseComObject(xla);
                xla = null;
            }

            System.GC.WaitForPendingFinalizers();
            System.GC.Collect();
            System.GC.WaitForFullGCComplete();
        }

        //private string 
    }
}
