﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.Chem.Upload.Manager
{
    [Serializable]
    public class Service
    {
        public List<ExcelMapping> SelectMappingByStudyYear(int studyYear, bool active, string connectionString)
        {
            
            try
            {
                ExcelMapping excelMapping = new ExcelMapping();
                return excelMapping.SelectByStudyYear(studyYear, active, connectionString);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                return null;
            }
        }
    }
}
